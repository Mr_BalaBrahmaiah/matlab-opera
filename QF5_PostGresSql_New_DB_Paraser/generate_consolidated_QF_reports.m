function generate_consolidated_QF_reports()

[AssetClass_ColNames,AssetClass_tbl_Data] = read_from_database('asset_class_table',0); %% Common AssetClass for All BU's
Unique_AssetClass = AssetClass_tbl_Data(:,1);

Header = [{'Month','M_DATEKEY'},upper(Unique_AssetClass)',{'TOTAL QF'}];

%%

[OutErrorMsg,QF1_Data] = generate_QF_Reports('qf1',Unique_AssetClass);
[OutErrorMsg,QF2_Data] = generate_QF_Reports('qf2',Unique_AssetClass);
[OutErrorMsg,QF3_Data] = generate_QF_Reports('qf3',Unique_AssetClass);
[OutErrorMsg,QF4_Data] = generate_QF_Reports('qf4',Unique_AssetClass);

ConsolidateData = [QF1_Data;QF2_Data;QF3_Data;QF4_Data];

% UniqueFields = {'Month','M_DATEKEY'};
% SumFields = {'BOND','COMMODITY','CURRENCY','CURRENCY','ENERGY','INDEX','INDUSTRIAL MATERIAL','METAL','MONEY MARKET','TOTAL QF'};
UniqueFields = Header(2);
SumFields = Header(3:end);
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,ConsolidateReportData] = consolidatedata(Header, ConsolidateData,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);

Summary_Data = [OutputFields;ConsolidateReportData];

OutXLSFileName = getXLSFilename('QF_Consolidated _Reports');
OutXLSFileName =fullfile(pwd,OutXLSFileName);

% xlswrite(OutXLSFileName,[Header;QF1_Data],'QF1');
% xlswrite(OutXLSFileName,[Header;QF2_Data],'QF2');
% xlswrite(OutXLSFileName,[Header;QF3_Data],'QF3');
% xlswrite(OutXLSFileName,[Header;QF4_Data],'QF4');
% xlswrite(OutXLSFileName,Summary_Data,'Summary');

%% Remove Month Col and Unique Market Date

for i = 1 : 4
    if(i == 1)
        QF_Data = QF1_Data(:,2:end);
        BusinessUnit = 'QF1';
    elseif(i == 2)
        QF_Data = QF2_Data(:,2:end);
        BusinessUnit = 'QF2';
    elseif(i == 3)
        QF_Data = QF3_Data(:,2:end);
        BusinessUnit = 'QF3';
    else
        QF_Data = QF4_Data(:,2:end);
        BusinessUnit = 'QF4';
    end
    
    UniqueFields = {'M_DATEKEY'};
    SumFields = Header(3:end);
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,ConsolidateReportData] = consolidatedata(Header(2:end), QF_Data,...
        UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    QF_Data_MonthUnique.(BusinessUnit) = [OutputFields;ConsolidateReportData];
    
end

Temp_QF1 = cell(size(QF1_Data,1),1);
Temp_QF1(:,:) = {'QF1'};
QF1_Data = [QF1_Data,Temp_QF1];

Temp_QF2 = cell(size(QF2_Data,1),1);
Temp_QF2(:,:) = {'QF2'};
QF2_Data = [QF2_Data,Temp_QF2];

Temp_QF3 = cell(size(QF3_Data,1),1);
Temp_QF3(:,:) = {'QF3'};
QF3_Data = [QF3_Data,Temp_QF3];

Temp_QF4 = cell(size(QF4_Data,1),1);
Temp_QF4(:,:) = {'QF4'};
QF4_Data = [QF4_Data,Temp_QF4];

Header = [Header,{'Business Unit'}];

ConsolidateData = [QF1_Data;QF2_Data;QF3_Data;QF4_Data];

UniqueFields = {'Business Unit','Month','M_DATEKEY'};
SumFields = Header(3:end-1);
OutputFields = [UniqueFields,SumFields];
[OutputFields,ConsolidateReportData] = consolidatedata(Header, ConsolidateData,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);

xlswrite(OutXLSFileName,QF_Data_MonthUnique.QF1,'QF1');
xlswrite(OutXLSFileName,QF_Data_MonthUnique.QF2,'QF2');
xlswrite(OutXLSFileName,QF_Data_MonthUnique.QF3,'QF3');
xlswrite(OutXLSFileName,QF_Data_MonthUnique.QF4,'QF4');
xlswrite(OutXLSFileName,Summary_Data,'Summary'); %% Remove Month Column
xlswrite(OutXLSFileName,[OutputFields;ConsolidateReportData],'Detailed View');

%%
try
    xls_delete_sheets(OutXLSFileName);
catch
end


end
