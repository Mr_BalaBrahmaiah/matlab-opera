function Main_PostgresSql_Global(SqlQuery,InBUName,InputDates)

%%
% [DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_all_portfolio',0); %% 'trades_today_all_portfolio' %% 'trades_history_all_portfolio'

% Out = strtok(DBData(:,1),' ');
% UniqueDate = unique(Out);
try
global ObjDB ;
ObjDB = connect_to_database;

%% SQL

ViewName = 'helper_quandfund_subpf_mapping_view';  %% helper_settle_pricing_subportfoliovalues_view
[MappingColNames,MappingData] = read_from_database(ViewName,0);

if strcmpi(MappingData,'No Data')
    errordlg('No Data in helper_quandfund_subpf_mapping_view view',InBUName);
end

%% PostgreSQL

% SqlQuery = 'select * from invenio_read_only.trades_history_all_portfolio where trade_date::text like ''2016-11-09%'' and portfolio_name = ''QF1''';

[DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_qf5_portfolio',0,SqlQuery); %% 'trades_history_all_portfolio' %% trades_history_qf5_portfolio

if strcmpi(DBData,'No Data')
    errordlg('No Data in trades_history_qf5_portfolio view',InBUName);
end

%%% Newly added code (18-10-2019)
%%% fetch data from counterparty_identification_account_qf5 table
sql_query = 'select * from counterparty_identification_account_qf5';
[CPIA_FieldNames,CPIA_Data] = Fetch_DB_Data(ObjDB,sql_query); 

if strcmpi(CPIA_Data,'No Data')
    errordlg('No Data in counterparty_identification_account_qf5 table',InBUName);
end

CPIA_Data_Tbl = cell2table(CPIA_Data,'VariableNames',CPIA_FieldNames);  %% Convert to table

%%% Mapping Process
[row,~] = size(DBData);
Subportfolio_Col =  cellfun(@(V) strcmpi('sub_portfolio',V), DBFieldNames);
broker_name_Col =  cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);
account_Col =  cellfun(@(V) strcmpi('account',V), DBFieldNames);
Overall_Out_Err_Msg = [];
for i = 1: 1: row
    cur_subportfolio = DBData(i,Subportfolio_Col);
    cur_broker_name = DBData(i,broker_name_Col);
    cur_account = DBData(i,account_Col);
    
    Index_opera_counterparty = strcmpi(cur_subportfolio,CPIA_Data_Tbl.subportfolio) & strcmpi(cur_broker_name,CPIA_Data_Tbl.traders_db_broker) & ...
                            strcmpi(cur_account,CPIA_Data_Tbl.account_no);
    if any(Index_opera_counterparty)               
        opera_counterparty(i,:) = CPIA_Data_Tbl.opera_counterparty(Index_opera_counterparty);
    else
        opera_counterparty(i,:) = {''};
        Out_Err_Msg = {['No mapping found with counterparty_identification_account_qf5 for corresponding subportfolio: ',char(cur_subportfolio),' & broker_name: ',char(cur_broker_name),' & account: ',char(cur_account)]};
        Overall_Out_Err_Msg = [Overall_Out_Err_Msg ; Out_Err_Msg];
    end
end

DBData(:,broker_name_Col) = opera_counterparty;
%%% end code

%% Preprocessing for PostgreSQL Data

if(length(DBData)>1)
    
    if(strcmpi(InBUName,'QF5'))
        Identifier_Col =  cellfun(@(V) strcmpi('identifier',V), DBFieldNames);
        Include_Dot_Prod_Index = cellStrfind(DBData(:,Identifier_Col),{'.'});
        DBData_Dot = DBData(Include_Dot_Prod_Index,:);
        DBData(Include_Dot_Prod_Index,:) = [];
        
        Unique_Identifier = unique(DBData_Dot(:,Identifier_Col)) ;
        Subportfolio_Col =  cellfun(@(V) strcmpi('sub_portfolio',V), DBFieldNames);
        Unique_Subportfolio_Data = unique(DBData_Dot(:,Subportfolio_Col));
        
        Subportfolio_Product_MapTbl = ['subportfolio_product_mapping_table_',char(InBUName)];
        [aa,bb] = Fetch_DB_Data(ObjDB,[],Subportfolio_Product_MapTbl);
        Subportfolio_Product_MapTbl_Data = cell2dataset([aa;bb]);
        
        Missing_Identifier = [];
        Overall_NettedTRData_Dot = [];
        Overall_ErrorData_Dot = [];
        Overall_Total_Data_Dot = [];
        
        for k = 1:size(Unique_Subportfolio_Data,1)
            Current_Subportfolio = Unique_Subportfolio_Data(k);
            Matched_Subportfolio_Index  = strcmpi(Current_Subportfolio,DBData_Dot(:,Subportfolio_Col));
            
            Current_DBData_Dot = DBData_Dot(Matched_Subportfolio_Index,:);
            
            Instrument_Data = cell(size(Current_DBData_Dot,1),1);
            Portfolio_Data = cell(size(Current_DBData_Dot,1),1);
            ProductCode_Data = cell(size(Current_DBData_Dot,1),1);
            
            Unique_Identifier = unique(Current_DBData_Dot(:,Identifier_Col)) ;
            
            for iPF = 1:size(Unique_Identifier,1)
                Current_Identifier = Unique_Identifier(iPF);
                
                try
                    
                    %%
                    IdxSubportfolio_Identifier = strcmpi(Subportfolio_Product_MapTbl_Data.instrument,Current_Identifier) & strcmpi(Subportfolio_Product_MapTbl_Data.subportfolio,Current_Subportfolio);
                    if(isempty(find(IdxSubportfolio_Identifier)))
                        Missing_Identifier = [Missing_Identifier ; [Current_Subportfolio,Current_Identifier] ];
                        continue;
                    end
                    
                    
                    IdxPF = strcmpi(Current_Identifier,Current_DBData_Dot(:,Identifier_Col)) & strcmpi(Current_Subportfolio,Current_DBData_Dot(:,Subportfolio_Col));
                    
                    Instrument_Data(IdxPF,:) = Subportfolio_Product_MapTbl_Data.instrument(IdxSubportfolio_Identifier);
                    Portfolio_Data(IdxPF,:) = Subportfolio_Product_MapTbl_Data.subportfolio(IdxSubportfolio_Identifier);
                    ProductCode_Data(IdxPF,:) = Subportfolio_Product_MapTbl_Data.product_code(IdxSubportfolio_Identifier);
                    
                catch ME
                    Missing_Identifier = [Missing_Identifier ; [Current_Subportfolio,Current_Identifier] ];
                    continue;
                end
            end
            
            [NettedTRData_Dot,ErrorData_Dot,Total_Data_Dot,TRHeader] = Get_TR_Format_QF5(InBUName,DBFieldNames,Current_DBData_Dot,Instrument_Data,Portfolio_Data,ProductCode_Data) ;
            
            if(~isempty(NettedTRData_Dot))
                Overall_NettedTRData_Dot = [Overall_NettedTRData_Dot ; NettedTRData_Dot(2:end,:)];
            end
            if(~isempty(ErrorData_Dot))
                Overall_ErrorData_Dot = [Overall_ErrorData_Dot ; ErrorData_Dot(2:end,:) ];
            end
            if(~isempty(Total_Data_Dot))
                Overall_Total_Data_Dot = [Overall_Total_Data_Dot; Total_Data_Dot(2:end,:)];
            end
            
            NettedTRData_Dot = [];
            ErrorData_Dot = [];
            Total_Data_Dot = [];
            
            fprintf('Finsihed : %d : %s : %d\n',k,char(Current_Subportfolio),size(Unique_Subportfolio_Data,1));
            
        end
        
        if(~isempty(Missing_Identifier))
            warndlg([upper(InBUName),' Identifier ',Missing_Identifier(:,2)',' which is not available in subportfolio_product_mapping_table_qf5']);
            %             warndlg([upper(InBUName),' Identifier ',Missing_Identifier(:,1)',' ',Missing_Identifier(:,2)',' which is not available in subportfolio_product_mapping_table_qf5']);
        end
        
        NettedTRData_Dot = [TRHeader ; Overall_NettedTRData_Dot];
        ErrorData_Dot = [TRHeader ; Overall_ErrorData_Dot];
        Total_Data_Dot = [TRHeader ;Overall_Total_Data_Dot];
    end
    
    %% Existing Code
    if(strcmpi(InBUName,'QF5'))
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
            'OptType','Prem','CounterParty','Initial Lots',...
            'CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','Source',...
            'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp','Equity_Trade_Type'};
    else
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
            'OptType','Prem','CounterParty','Initial Lots',...
            'CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','Source',...
            'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};
    end
    
    
    if(~isempty(DBData))
        
        Identifier_Col =  cellfun(@(V) strcmpi('identifier',V), DBFieldNames);
        Identifier = DBData(:,Identifier_Col);
        
        [ProductCode_Month, BBG_Code] = strtok(Identifier,' ');
        
        IdxSingle = find(cellfun(@(s) (length(s)<=1), ProductCode_Month));
        Change_BBG_Code = BBG_Code(IdxSingle);
        [Change_Month_code,Remain_BBG_Code] = strtok(Change_BBG_Code,' ');
        
        BBG_Code(IdxSingle) = Remain_BBG_Code;
        BBG_Code = strtrim(BBG_Code);
        
        Change_ProductCoode = ProductCode_Month(IdxSingle);
        
        ProductCode_Month(IdxSingle) = cellstr([char(Change_ProductCoode),char(Change_Month_code)]);
        
        Month_Code = cellfun(@(x) x(end-1:end), ProductCode_Month, 'UniformOutput', false);   %% Get Month Code Only
        
        ProductCode = ProductCode_Month;
        for i = 1 : length(ProductCode)
            ProductCode{i}((end-1:end)) = [];             %% Get Product Code Only
        end
        
        %% Mapping SQL Data vs PostgersSQL Data
        
        Map_ProdCol =  cellfun(@(V) strcmpi('product_code',V), MappingColNames);
        Map_ProductCode = MappingData(:,Map_ProdCol);
        
        Map_BBGCol =  cellfun(@(V) strcmpi('bbg_code',V), MappingColNames);
        Map_BBGCode = MappingData(:,Map_BBGCol);
        
        Map_DBProductCol =  cellfun(@(V) strcmpi('db_product_code',V), MappingColNames);
        Map_DBProductCode = MappingData(:,Map_DBProductCol);
        
        DBProduct_Code = cell(length(ProductCode),1);          %% Create Empty cell Array
        DBProduct_Code(:,:) = {' '};
        
        for k = 1 : length(ProductCode)
            Current_ProductCode = strtrim(ProductCode{k});
            Current_BBGCode = strtrim(BBG_Code{k});
            Map_DBProductCode = strtrim(Map_DBProductCode);
            
            MatchedIndex = find(strcmpi(Current_ProductCode, Map_ProductCode) & strcmpi(Current_BBGCode, Map_BBGCode));
            
            if(~isempty(MatchedIndex))
                DBProduct_Code(k,1) = cellstr(strtrim(Map_DBProductCode(MatchedIndex)));
            end
            
        end
        
        DBPortfolio_Col = cellfun(@(V) strcmpi('portfolio_name',V), DBFieldNames);
        DBPortfolio = strtrim(DBData(:,DBPortfolio_Col));   %% Get Portfolio Name from PostgreSQL DB
        
        %% Mapping QF1 Support Portfolio vs DB Product Code and DB Portfolio For Getting Instrument
        
        %     InBUName = 'qf1';
        SqlQuery2 = 'select instrument,subportfolio,product_code from subportfolio_product_mapping_table where product_code not like ''%USD%''';
        [Tbl_DBFieldNames,Tbl_DBData] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery2,lower(InBUName));
        
        if(length(Tbl_DBData)>1)
            
            Tbl_InstrumentCol =  cellfun(@(V) strcmpi('instrument',V), Tbl_DBFieldNames);
            Tbl_Instrument = strtrim(Tbl_DBData(:,Tbl_InstrumentCol));
            
            Tbl_ProductCol =  cellfun(@(V) strcmpi('product_code',V), Tbl_DBFieldNames);
            Tbl_ProductCode = strtrim(Tbl_DBData(:,Tbl_ProductCol));
            
            Tbl_PortfolioCol =  cellfun(@(V) strcmpi('subportfolio',V), Tbl_DBFieldNames);
            Tbl_Portfolio = strtrim(Tbl_DBData(:,Tbl_PortfolioCol));
            
            Instrument_Code = cell(length(DBProduct_Code),1);
            Instrument_Code(:,:) = {' '};
            
            %%  Transaction Number
            %         ObjDB = connect_to_database;
            
            TempTradeId = '';                               %% Transaction Number
            try
                [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
                if strcmpi(OutErrorMsg,'No Errors')
                    % TradeIdPrefix = 'TR-FY16-17-';
                    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
                    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
                    TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
                    
                    if(isempty(TempTradeId))
                        TempTradeId = 0;      %% Default
                    end
                end
            catch
                TempTradeId = '';
            end
            %%
            
            Missing_ProductCode = [];
            for i = 1 : length(DBProduct_Code)
                
                Current_ProductCode = strtrim(DBProduct_Code{i});
                Current_Portfolio = strtrim(DBPortfolio{i});
                
                MatchedIndex = find(strcmpi(Current_ProductCode, Tbl_ProductCode) & strcmpi(Current_Portfolio, Tbl_Portfolio));
                
                if(~isempty(MatchedIndex))
                    Instrument_Code(i,1) = cellstr(strtrim(Tbl_Instrument(MatchedIndex)));
                else
                    Missing_ProductCode = [Missing_ProductCode;Current_ProductCode];
                end
            end
            
            if(~isempty(Missing_ProductCode))
                warndlg([upper(InBUName),' Product Code ',Missing_ProductCode',' which is not available in subportfolio_product_mapping_table_',lower(InBUName)]);
            end
            
            %% Excel Sheet Write
            
            %     TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
            %         'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
            %         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
            %         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
            %         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
            
            
            
            Temp = cell(length(DBProduct_Code),1);
            Temp(:,:) = num2cell(NaN); %% Temp(:,:) = {''};  %% Temp(:,:) = num2cell(NaN);
            
            BuySell_Col = cellfun(@(V) strcmpi('trade_side_name',V), DBFieldNames);
            BuySell = strtrim(DBData(:,BuySell_Col));
            Trade_Type = BuySell ;
            % Buy_Index = (~cellfun(@isempty,regexp(BuySell,'Buy')));   %% Changing in BuySell Variable Buy into bought & Sell into sold
            % Sell_Index = (~cellfun(@isempty,regexp(BuySell,'Sell')));
            Buy_Index = cellStrfind(lower(BuySell),{'buy'});
            Sell_Index = cellStrfind(lower(BuySell),{'sell'});
            BuySell(Buy_Index) = {'Bought'};
            BuySell(Sell_Index) = {'Sold'};
            
            Lots_Col = cellfun(@(V) strcmpi('trade_qty',V), DBFieldNames);  %% Actice Lots ans Initial Lots
            Lots_Data = DBData(:,Lots_Col);
            IdxSell = strcmpi(BuySell,'Sold');
            Lots_Data(IdxSell) = num2cell(cell2mat(Lots_Data(IdxSell)) .* -1);
            
            Maturity_Trades = strcat(Month_Code,'.',Month_Code);
            
            OptType = cell(length(DBProduct_Code),1);
            OptType(:,:) = {'future'};
            
            Premium_Col = cellfun(@(V) strcmpi('trade_price',V), DBFieldNames);  %% Avg Premium and Prem
            Premium = DBData(:,Premium_Col);
            
            CounterParty_Col = cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);   %% CounterParty and Exe-Broker as same
            CounterParty = DBData(:,CounterParty_Col);
            
            TraderName_Col = cellfun(@(V) strcmpi('trader_name',V), DBFieldNames);
            TraderName = DBData(:,TraderName_Col);
            
            TransactionDate_Col = cellfun(@(V) strcmpi('trade_date',V), DBFieldNames);
            ActualTransactionDate = DBData(:,TransactionDate_Col);
            [TransactionDate,Timing] = strtok(ActualTransactionDate);
            
            % TransactionDate = cellfun(@(V) datestr(V), TransactionDate,'UniformOutput',false);
            % TransactionDate = cellfun(@(V) x2mdate(V), TransactionDate,'UniformOutput',false);   %% m2xdate %% x2mdate
            % formatOut = 'yyyy-mm-dd';
            % datestr(TransactionDate,formatOut)
            
            Source = cell(length(DBProduct_Code),1);   %% Get Database Name
            Source(:,:) = {'TraderDB'};
            
            Exe_Type = cell(length(DBProduct_Code),1);   %% Get Database Name
            Exe_Type(:,:) = {'Elec'};
            
            Rates = cell(length(DBProduct_Code),1);   %% Get Database Name
            Rates(:,:) = {'Normal'};
            
            %% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType
            
            [~,~,RawData] = xlsread('Mapping_derivativeType.xlsx'); % 'Mapping_derivativeType.xls' % 'Mapping_fxfuture.xls'
            Header = RawData(1,:);
            Data = RawData(2:end,:);
            
            for k = 1 : length(Data)
                Current_Instrument = strtrim(Data{k,1});
                Current_OptType = strtrim(Data{k,2});
                MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
                
                if(~isempty(MatchedIndex))
                    OptType(MatchedIndex,1) = cellstr(Current_OptType);
                end
                
            end
            
            
            %% WRITE EXCEL SHEET
            
            %     RowData = [Instrument_Code,DBPortfolio,BuySell,Lots_Data,Maturity_Trades,Temp,OptType,Premium,CounterParty,Temp,Lots_Data,Temp,Premium,...
            %         Temp,Temp,Temp,Temp,TransactionDate,Temp,Temp,Source,Temp,Temp,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];
            
            if(strcmpi(InBUName,'QF5'))
                RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
                    Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp,TraderName,ActualTransactionDate,Trade_Type];
            else
                RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
                    Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp,TraderName,ActualTransactionDate];
            end
            
            Total_Data = [TRHeader;RowData];
            
            if(strcmpi(InBUName,'QF5'))
                UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date','Equity_Trade_Type'};
            else
                UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date'};
            end
            SumFields = {'Initial Lots'};
            NettedTRData = calc_netted_data(TRHeader, RowData,TRHeader,UniqueFields,SumFields,[]);
            
            IdxEmpty = find(cellfun(@(s) (length(s)<=1), NettedTRData(:,1)));  %%  Find Empty Instrument
            ErrorData = NettedTRData(IdxEmpty,:);   %% Move Empty Rows to Errors Sheet
            ErrorData = [TRHeader;ErrorData];       %% Add Header
            
            NettedTRData(IdxEmpty,:) = [];      %% Find Empty Instrument and Remove Empty rows in NettedTRData
            
            if ~isempty(TempTradeId)
                PosTrnId = strcmpi('TRN.Number',TRHeader);
                PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
                NumRows = size(NettedTRData,1) - 1; % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
                NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
                TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
                TradeId = strrep(TradeId,' ','');  %% Remove Spaces
                NettedTRData(2:end,PosTrnId) = TradeId;
                NettedTRData(2:end,PosParentTrnId) = TradeId;
            end
            
            %% Changing Instrument KMA to KMA1
            
            if(size(NettedTRData,1) > 1)
                
                Instrument_Col = strcmpi('Instrument',TRHeader);
                Changed_Instrument = strrep( NettedTRData(2:end,Instrument_Col) ,'KMA INDEX FUTURE' ,'KMA1 INDEX FUTURE');
                NettedTRData(2:end,1) = Changed_Instrument ;
                
            end
            
        else
            errordlg('No Data in Support Portfolio DB',InBUName);
            
        end
        
    else
        NettedTRData = TRHeader;
        ErrorData = TRHeader;
        Total_Data = TRHeader; %% Row_Data is Empty
    end
    
    %%  If QF5 Concatenate the With and Without Identifiers Data
    
    if(strcmpi(InBUName,'QF5'))
        NettedTRData = [NettedTRData ; NettedTRData_Dot(2:end,:)] ;
        
        NettedTRData = NettedTRData(2:end,:);
        OptType_Index = cellStrfind_exact(TRHeader,{'OptType'});
        EquitySwap_Index = cellStrfind_exact(NettedTRData(:,OptType_Index),{'equity_swap'});
        EquityFuture_Index = cellStrfind_exact(NettedTRData(:,OptType_Index),{'equity','future'});
        
        TRNNum_Col = cellStrfind_exact(TRHeader,{'TRN.Number'});
        ParTRNNum_Col = cellStrfind_exact(TRHeader,{'Parent Transaction Number'});
        if(~isempty(EquitySwap_Index))
            [OutErrorMsg,LastTradeId,TradeID]= getLastTradeId_RES_QF5(InBUName,NettedTRData(EquitySwap_Index,:));
            NettedTRData(EquitySwap_Index,TRNNum_Col) = TradeID;
            NettedTRData(EquitySwap_Index,ParTRNNum_Col) = TradeID;
        end
        
        if(~isempty(EquityFuture_Index))
            [TradeID , Last_DBTradeId] = getTradeID_4_Cell_Array(NettedTRData(EquityFuture_Index,:),InBUName);
            NettedTRData(EquityFuture_Index,TRNNum_Col) = TradeID;
            NettedTRData(EquityFuture_Index,ParTRNNum_Col) = TradeID;
        end
        
        NettedTRData = [TRHeader;NettedTRData];
        
        
        ErrorData = [ErrorData ; ErrorData_Dot(2:end,:)];
        Total_Data = [Total_Data ; Total_Data_Dot(2:end,:)] ;
    end
    
    %% Write & Delete Excel Sheet
    %         TimeStamp = char(strrep(datestr(now,'HH:MM:SS'),':','_'));
    %         XLSFileName = [InBUName,'_Trade_Dump_',InputDates,'_',TimeStamp,'.xlsx'];
    XLSFileName = getXLSFilename(strcat(upper(char(InBUName)),'_Trade_Dump')) ;
    
    if(exist(XLSFileName))
        delete(XLSFileName);
        xlswrite(XLSFileName,NettedTRData,'Netted-TRData');
        xlswrite(XLSFileName,ErrorData,'Errors');
        xlswrite(XLSFileName,Total_Data,'OverallData');
        
        if(~isempty(Missing_Identifier))
            xlswrite(XLSFileName,[{'Portfolio','Identifier'} ;Missing_Identifier ],'Missing_Identifier');
        end
        xls_delete_sheets([pwd filesep XLSFileName]);
    else
        xlswrite(XLSFileName,NettedTRData,'Netted-TRData');
        xlswrite(XLSFileName,ErrorData,'Errors');
        xlswrite(XLSFileName,Total_Data,'OverallData');
        
        if(~isempty(Missing_Identifier))
            xlswrite(XLSFileName,[{'Portfolio','Identifier'} ;Missing_Identifier ],'Missing_Identifier');
        end
        xls_delete_sheets([pwd filesep XLSFileName]);
    end
    %%% Newly added code
    if ~isempty(Overall_Out_Err_Msg)
        xlswrite(XLSFileName,Overall_Out_Err_Msg,'Error_subPortfolio');
    end
    
    % Make Active Sheet
    SheetName = 'Netted-TRData';
    try
        Full_Path = [pwd filesep XLSFileName];
        xls_change_activesheet(Full_Path,SheetName);  %% Full Path Necessary..!!!!
    catch
    end
    
    
else
    
    errordlg(['No ',InBUName,' Data in DB'],InBUName);
    
end

catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end


end





