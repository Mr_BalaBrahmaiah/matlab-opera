try
    
    ObjDB = connect_to_database;
    
    %% Input Data
    InBUName = 'qf5';
    [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
    InXLSFilename = fullfile(pathname,filename);
    
    [~,~,RawData] = xlsread(InXLSFilename,'OverallData') ;
    
    TRHeader = RawData(1,:);
    RowData =  RawData(2:end,:);
    
    hWaitbar = waitbar(0,'Please wait...');
    
    %% Netted Data
    UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date','Equity_Trade_Type'};
    SumFields = {'Initial Lots'};
    NettedTRData = calc_netted_data(TRHeader, RowData,TRHeader,UniqueFields,SumFields,[]);
    
    waitbar(0.4,hWaitbar,'Consolidate the Data ','Name','Please wait...');
    
    %% RES TradeID  Finding
    
    waitbar(0.6,hWaitbar,'RES TradeID  Finding ','Name','Please wait...');
    
    NettedTRData(1,:) = []; 
    
    [OutErrorMsg,LastTradeId,TradeId] = getLastTradeId_RES_QF5(InBUName,NettedTRData);
    
    if(~isempty(TradeId{1}) ) %% && ~isempty(cell2mat(TradeId))
        
        TRN_TradeID_Col = cellStrfind_exact(TRHeader , {'TRN.Number'}) ;
        Parent_TradeID_Col = cellStrfind_exact(TRHeader , {'Parent Transaction Number'}) ;
        NettedTRData(:,TRN_TradeID_Col) = TradeId ;
        NettedTRData(:,Parent_TradeID_Col) = TradeId ;
        
        xlswrite(InXLSFilename,[TRHeader ; NettedTRData],'Netted-TRData');
        
    else
        xlswrite(InXLSFilename,[TRHeader ; NettedTRData],'Netted-TRData');
        errordlg('Unable to find RES-TradeID','Warning Error');
    end
    
    %%
    
    waitbar(1,hWaitbar,'Process Finished','Name','Finished');
    
    msgbox('Process Finished','Finish');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    
end