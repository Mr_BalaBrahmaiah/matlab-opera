function [OutErrorMsg,Total_Data] = generate_QF_Reports(InBUName,Unique_AssetClass)

OutErrorMsg = {'No errors'};

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        User_Want_Col = {'asset_class','contract_month','settlement_date','mtm_usd'};
        Matched_Col = cellStrfind_Perfect(ColNames,User_Want_Col);
        % Matched_Col = [51;24;3;42];
        Filterd_Data = Data(:,Matched_Col);
        
        ContractMonth_Index =  strcmpi(User_Want_Col,'contract_month');
        
        %% Find Blank Contract Month
        % IdxEmpty = find(cellfun(@(s) (length(s)>1), Filterd_Data(:,ContractMonth_Index)));
        % Filterd_Data = Filterd_Data(IdxEmpty,:); %% Remove Empty ContractMonth Fields and corresponding all column
        
        IdxEmpty = find(cellfun(@(s) (length(s)<1), Filterd_Data(:,ContractMonth_Index))); %% Blank to Temporary Identity
        Filterd_Data(IdxEmpty,ContractMonth_Index) = {'blank'};
        
        %%
        ContractMonth_Data = Filterd_Data(:,ContractMonth_Index);
        Unique_ContractMonth = unique(ContractMonth_Data);
        
        %% Getting Asset Class
        AssetClass_Index = strcmpi(User_Want_Col,'asset_class');
        
        %         AssetClass_Data = Filterd_Data(:,AssetClass_Index); %% Particular BU Asset Class
        %         Unique_AssetClass = unique(AssetClass_Data);
        
        %         [AssetClass_ColNames,AssetClass_tbl_Data] = read_from_database('asset_class_table',0); %% Common Asset Class for All BU's
        %         Unique_AssetClass = AssetClass_tbl_Data(:,1);
        
        %%
        
        SettlementDate_Index =  strcmpi(User_Want_Col,'settlement_date');
        Mtm_Usd_Index =  strcmpi(User_Want_Col,'mtm_usd');
        
        count = 1;
        
        for i = 1 : length(Unique_ContractMonth)
            
            Current_ContractMonth = Unique_ContractMonth{i};
            
            Matched_ContractMonth_Index = find(strcmpi(ContractMonth_Data,Current_ContractMonth));
            
            Matched_AssetClass_Data = Filterd_Data(Matched_ContractMonth_Index,AssetClass_Index);
            
            CM{count,1} = Current_ContractMonth;
            
            for ii = 1 : length(Unique_AssetClass)
                
                Current_AssetClass = Unique_AssetClass{ii};
                Matched_AssetClass_Index = Matched_ContractMonth_Index(find(strcmpi(Matched_AssetClass_Data,Current_AssetClass)));
                
                if(~isempty(Matched_AssetClass_Index))
                    Matched_SetlementDate = unique(Filterd_Data(Matched_AssetClass_Index,SettlementDate_Index));
                    Matched_MtmUsd = sum(cell2mat(Filterd_Data(Matched_AssetClass_Index,Mtm_Usd_Index)));
                    
                    TempData = Filterd_Data(Matched_AssetClass_Index,:); %% For Verification
                    
                    EOD_Date(count,1) = Matched_SetlementDate;
                    AssetClass(count,ii) = num2cell(Matched_MtmUsd);
                    
                else
                    AssetClass(count,ii) = num2cell(0);
                end
                
                
            end
            
            count = count + 1;
        end
        
        %% Find NaN Fields from Matrix and replace 0 because for sum all Rows of Matrix
        [Index] = isnan(cell2mat(AssetClass)) ;
        AssetClass(Index) = num2cell(0);
 
        %%
        [RowSum,~] = cell2sum_Row_Col(AssetClass);
        Combined_Data = [CM,EOD_Date,AssetClass,RowSum];
        
        %         Header = [{'Month','M_DATEKEY'},Unique_AssetClass',{'TOTAL QF'}];
        %         Total_Data = [Header;Combined_Data];
        
        Total_Data = Combined_Data;
        
    else
        warndlg('No Data in DB');
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
end