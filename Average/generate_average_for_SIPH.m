function [OutData , ErrorMsg] = generate_average_for_SIPH

% clc;
% clear;

ErrorMsg = {'No Error'};

ObjDB = connect_to_database;

hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');

try
    %% Read From Database
    
    Sys_VDate = fetch(ObjDB,'select value_date from valuation_date_table');
    
    [DB_FieldNames,DB_Data] = read_from_database('helper_calculate_avg_till_now_view_cfs',0);
    
    [View_FiledsNames,View_Data] = Fetch_DB_Data(ObjDB,[],'helper_security_average_till_now_monitoring_view');
    SecurityID_Col = cellStrfind_exact(View_FiledsNames,{'security_id'});
    FutureID_Col = cellStrfind_exact(View_FiledsNames,{'future_id'});
    ValueDate_Col = cellStrfind_exact(View_FiledsNames,{'value_date'});
    
    %% Get Holidays
    % [~,~,RawData] = xlsread('holidays_list_table.xlsx');
    % RawData_Header = RawData(1,:);
    % RawData = RawData(2:end,:);
    
    [RawData_Header,RawData] = read_from_database('holidays_list_table',0);
    Holidays_Col = find(cellfun(@(V) strcmpi('holidays',V), RawData_Header));
    Holiday_Data = RawData(:,Holidays_Col);
    
    HolidayVec = char(Holiday_Data);
    
    %%
    waitbar(0.25,hWaitbar,'Processing...');
    
    if( ~isempty(DB_Data) )
        
        StartDate_Col = find(cellfun(@(V) strcmpi('start_date',V), DB_FieldNames));
        EndDate_Col = find(cellfun(@(V) strcmpi('end_date',V), DB_FieldNames));
        
        UserWant_Col = cellStrfind_Perfect(DB_FieldNames,{'future_id','fx_spot_id'});  %% Get User Want Col
        UserWant_Data = DB_Data(:,UserWant_Col);
        
        Rate = cell(size(UserWant_Data,1),1) ; %% Empty cell for Rate  It will Fill From DB
        
        UserWant_Header = {'future_id','rate','fx_rate','fx_spot_id'};
        UserWant_Data = [UserWant_Data(:,1), Rate ,Rate,UserWant_Data(:,2)];
        
        
        fid = fopen('Error_Log_Missing_Price.log','wt');
        
        %%
        Total_Data = [];
        OutData = [];
        bb = {''};  %% Error Settle Value SQL Query
        cc = {''};  %% Error FX Rate SQL Query
        
        Total_Security_Data = [];
        
        for iRow = 1 : size(DB_Data,1)
            
            Current_FutureID = strtrim(DB_Data(iRow,1));
            
            StartDate = DB_Data{iRow,StartDate_Col};
            EndDate  = DB_Data{iRow,EndDate_Col};
            
            Today  = fetch(ObjDB,'select settlement_date from valuation_date_table') ; %% datestr(today,'yyyy-mm-dd');
            if( datenum(EndDate) >= datenum(Today) )
                EndDate = Today;
            else
                EndDate = EndDate;
            end
            
            %% TODO - Change the below -2 to -1 later %% datenum(today-2)
            
            VBusDays = busdays(datenum(StartDate), datenum(EndDate),'daily',HolidayVec); %% Where Start and Expiry are the input column names
            if ~isempty(VBusDays)
                
                Maturity = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
                
                %             Maturity_Str = convertCell2Char(Maturity);
                %             Maturity_Str = strrep(Maturity_Str,',',''',''');
                %             SqlQuery_SettleDate = ['select value_date,settle_date from product_busdays_table where product_code = ''',char(Current_ProductCode),''' and value_date in (''',Maturity_Str,''') ' ];
                %             [DateCol,DateData] = Fetch_DB_Data(ObjDB,SqlQuery_SettleDate);
                %             ValueDates = DateData(:,1);
                %             SettleDates = DateData(:,2);
                
                Temp = cell(length(Maturity),size(UserWant_Data,2)); %% Continuesly Append
                %             ValueDates = cellstr(datestr(busdate(VBusDays,1,HolidayVec),'yyyy-mm-dd'));
                
                
                for ii = 1 : length(Maturity)
                    %  fprintf('%d : %d\n',iRow,ii);
                    SqlQuery1 = ['select settle_value from underlying_settle_value_table where settlement_date = ''',Maturity{ii},''' and underlying_id = ''',UserWant_Data{iRow,1},''''];
                    SettlePrice = fetch(ObjDB, SqlQuery1);
                    
                    SqlQuery2 = ['select settle_value from underlying_settle_value_table where settlement_date = ''',Maturity{ii},''' and underlying_id = ''',UserWant_Data{iRow,4},''''];
                    FX_Rate = fetch(ObjDB, SqlQuery2);
                    
                    if isempty(SettlePrice)
                        SettlePrice = num2cell(0);
                        Missing_ID_Date = [UserWant_Data{iRow,1}, ' ' , Maturity{ii}] ;
                        fprintf(fid,'%s\n' ,Missing_ID_Date);
                        bb = [bb;{SqlQuery1}] ;%% Verification
                    end
                    if isempty(FX_Rate)
                        FX_Rate = num2cell(0);
                        Missing_FX_Rate = [UserWant_Data{iRow,4}, ' ' , Maturity{ii}] ;
                        fprintf(fid,'%s\n' ,Missing_FX_Rate);
                        cc = [cc;{SqlQuery2}] ;%% Verification
                    end
                    
                    UserWant_Data(iRow,2) = SettlePrice;
                    UserWant_Data(iRow,3) = FX_Rate ;
                    
                    Temp(ii,1) = UserWant_Data(iRow,1);
                    Temp(ii,2) = UserWant_Data(iRow,2);
                    Temp(ii,3) = UserWant_Data(iRow,3);
                    Temp(ii,4) = UserWant_Data(iRow,4);
                    
                    %  Temp(ii,:) = UserWant_Data(iRow,:);
                    
                end
                
                %%
                if( sum(cellfun(@(V) any(isempty(V(:))), Temp(:,2))) || sum(cellfun(@(V) any(isnan(V(:))), Temp(:,3))) )
                    warndlg(' Fx Rate Column is NaN or Empty');
                    break;
                end
            else
                Temp = cell(1,size(UserWant_Data,2));
                Temp(1,1) = UserWant_Data(iRow,1);
                Temp(1,2) = {0};
                Temp(1,3) = {0};
                Temp(1,4) = UserWant_Data(iRow,4);
                
                Maturity = Today;
            end
            %%
            waitbar(0.5,hWaitbar,'Processing...');
            
            EUR_Rate = num2cell( cell2mat(Temp(:,2))./ cell2mat(Temp(:,3)) ) ; %% Divison (Rate / Fx_Rate)
            Total_Data = [Total_Data ; [Temp , Maturity , EUR_Rate] ];
            
            %   AvgTillNow = sum(cell2mat(Total_Data(:,end)))/size(Total_Data,1);
            AvgTillNow = nansum(cell2mat(EUR_Rate)/size(EUR_Rate,1));
            
            RowOut = [DB_Data(iRow,1),AvgTillNow] ;
            OutData = [OutData;RowOut] ;
            
            Matched_Rows = strcmpi(View_Data(:,FutureID_Col),Current_FutureID) & strcmpi(View_Data(:,ValueDate_Col),Sys_VDate) ;
            Security_Data = View_Data(Matched_Rows,[ValueDate_Col;SecurityID_Col]); %% FutureID_Col
            Temp_Cell = cell(size(Security_Data,1),1);
            Temp_Cell(:,1) = { AvgTillNow };
            
            Total_Security_Data = [Total_Security_Data;[Security_Data,Temp_Cell]];
            
            
        end
        
        Total_Security_Header = {'value_date','security_id','avg_till_now'}; %% 'future_id'
        
        OutXLSName = getXLSFilename('Average_Sheet_');
        
        xlswrite(OutXLSName,[Total_Security_Header;Total_Security_Data], 'Security Avg Till Now');
        xlswrite(OutXLSName,[DB_FieldNames;DB_Data], 'INPUT');
        xlswrite(OutXLSName,[[UserWant_Header,{'Maturity','EUR Rate'}] ; Total_Data], 'Maturity Formation');
        xlswrite(OutXLSName,OutData, 'OUTPUT');
        xlswrite(OutXLSName,bb, 'Settle Price Empty');
        xlswrite(OutXLSName,cc, 'Fx Rate Empty');
        
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        xls_change_activesheet(fullfile(pwd,OutXLSName),'Security Avg Till Now');
        
        
    else
        msgbox('No Data in DB');
        
    end
    
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

waitbar(1,hWaitbar,'Process Finished');
msgbox('Process Complete','Finish');

