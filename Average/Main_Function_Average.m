clc;
clear;

ErrorMsg = {'No Error'};

try
    %% Read From Database
    
    [DB_FieldNames,DB_Data] = read_from_database('helper_calculate_avg_till_now_view',0);
    
    %% Get Holidays
    % [~,~,RawData] = xlsread('holidays_list_table.xlsx');
    % RawData_Header = RawData(1,:);
    % RawData = RawData(2:end,:);
    
    ObjDB = connect_to_database;
    
    [RawData_Header,RawData] = read_from_database('holidays_list_table',0);
    Holidays_Col = find(cellfun(@(V) strcmpi('holidays',V), RawData_Header));
    Holiday_Data = RawData(:,Holidays_Col);
    
    HolidayVec = char(Holiday_Data);
    
    %%
    if( ~isempty(DB_Data) )
        
        StartDate_Col = find(cellfun(@(V) strcmpi('start_date',V), DB_FieldNames));
        EndDate_Col = find(cellfun(@(V) strcmpi('end_date',V), DB_FieldNames));
        
        UserWant_Col = cellStrfind_Perfect(DB_FieldNames,{'future_id','rate','fx_rate'});  %% Get User Want Col
        UserWant_Data = DB_Data(:,UserWant_Col);
        
        Total_Data = [];
        OutData = [];
        for iRow = 1 : size(DB_Data,1)
            
            StartDate = DB_Data{iRow,StartDate_Col};
            EndDate  = DB_Data{iRow,EndDate_Col};
            
            %% TODO - Change the below -2 to -1 later
            VBusDays = busdays(datenum(StartDate), datenum(today-2),'daily',HolidayVec); %% Where Start and Expiry are the input column names
            if isempty(VBusDays)
                continue;
            end
            Maturity = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
            
            Temp = cell(length(Maturity),size(UserWant_Data,2)); %% Continuesly Append
            ValueDates = cellstr(datestr(busdate(VBusDays,1,HolidayVec),'yyyy-mm-dd'));
            for ii = 1 : length(Maturity)
                SqlQuery = ['select settle_value from underlying_settle_value_table where value_date = ''',ValueDates{ii},''' and underlying_id = ''',UserWant_Data{iRow,1},''''];
                SettlePrice = fetch(ObjDB, SqlQuery);
                UserWant_Data(iRow,2) = SettlePrice;
                
                Temp(ii,1) = UserWant_Data(iRow,1);
                Temp(ii,2) = UserWant_Data(iRow,2);
                Temp(ii,3) = UserWant_Data(iRow,3);
                
                %             Temp(ii,:) = UserWant_Data(iRow,:);
                
            end
            
            EUR_Rate = num2cell( cell2mat(Temp(:,2))./ cell2mat(Temp(:,3)) ) ; %% Divison (Rate / Fx_Rate)
            Total_Data = [Total_Data ; [Temp , Maturity , EUR_Rate] ];
            
            AvgTillNow = sum(cell2mat(Total_Data(:,end)))/size(Total_Data,1);
            RowOut = [DB_Data(iRow,1),AvgTillNow] ;
            OutData = [OutData;RowOut] ;
            
        end
        
    else
        msgbox('No Data in DB');
        
    end
    
catch ME
   errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end