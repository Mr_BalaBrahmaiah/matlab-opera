function dblIdxFound = cellStrfind(ceaString, ceaPattern)
% Finds the indices of the occurences of the search pattern in
% the given string 
% This function internally uses strfind function to fnd the indices, but
% this function overcomes the below limitations of 'strfind' function in
% matlab
% Benefits over inbuilt function strfind:
%       - strfind function doesnot allow the search pattern as a cell array
%       - when the input string is of cellArray type, then the Return index 
%         from strfind is also cellArray, which is not compatible to find
%         the indices
%% Syntax:
% =======  
%   dblIdxFound = cellStrfind(ceaString, ceaPattern)
% 
%% Input Arguments:
% ================
%   ceaString  - <cellArray> or <charArray> of String
%   ceaPattern - <cellArray> or <charArray> of Search Patterns
% 
%% Return Values:
% ==============
%   dblIdxFound - <double array> of the indices of the occurences of Search
%   pattern in the given string
% 
%% Examples:
% ==========
%% Example 1
% when String is cellArray and Pattern is Char array
%
%   string = {'Test1', 'Test2', 'Test3' };
%   pattern = 'Test3';
%	result = cellStrfind(string,pattern)
%   result =  3
%
%% Example 2
% when both string and patterns are cellArrays
%
%   string = {'Test1', 'Test2', 'Test3' };
%   pattern = {'Test3','Test1'};
%  result = cellStrfind(string,pattern)
%  result =
%      3
%      1
% 
%% Example 3
% when both string and patterns are cellArrays
%
% string = {'Test1', 'Test2', 'Test3' };
% pattern = {'Test3','Test5'};
% result = cellStrfind(string,pattern)
% result =
%      3
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/03/11 07:02:19 $
%  $Revision: 1.2 $
%
dblIdxFound = [];

if iscell(ceaPattern)
    numPatterns = length(ceaPattern);
    for iCell = 1:numPatterns
        % Finds the occurances of each pattern in the given string
        ceaIndexFound = strfind(ceaString,ceaPattern{iCell});
        dblIdxFound = [dblIdxFound;findIndex(ceaIndexFound)];
    end
else    
    ceaIndexFound = strfind(ceaString,ceaPattern);
    dblIdxFound = findIndex(ceaIndexFound);
end

function dblIdxFound = findIndex(ceaIndexFound)
% Converts the cell array of indices to double array of indices

if iscell(ceaIndexFound)
    if ~isempty(ceaIndexFound)
        % Finds the non-zero indices in input cell array of indices
        dblIdxFound = find(cellfun(@isempty,ceaIndexFound)==0);
    else
        dblIdxFound = [];
    end
else
    dblIdxFound = ceaIndexFound;
end