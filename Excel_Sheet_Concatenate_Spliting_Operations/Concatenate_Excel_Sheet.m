function Concatenate_Excel_Sheet()

%%% Input is Folder

% clc
% close all
% clear

SheetName = 'Sheet1';

%%
TargetDir = uigetdir('','Choose Input Folder');
TargetFiles = dir(TargetDir);

TargetFiles(strcmpi('.',{TargetFiles.name})) = [];
TargetFiles(strcmpi('..',{TargetFiles.name})) = [];
TargetFiles(strcmpi('Thumbs.db',{TargetFiles.name})) = [];

%% Make Output Folder

Date_Str_2 = strrep(datestr(now),':','_'); %% datestr(today,Date_Format)
Output_FolderPath = [pwd filesep 'zz_Error_Files'];
Destination_Path = [Output_FolderPath filesep strcat('Unale to Read ',Date_Str_2)];

if(~exist(Destination_Path))
    mkdir(Destination_Path);
else
    [status, message, messageid] = rmdir(Destination_Path,'s') ;
    while(status)
        [status, message, messageid] = rmdir(Destination_Path,'s') ;
    end
    mkdir(Destination_Path);
end


%%
OverallData = [];

for i = 1 : length(TargetFiles)
    
    fprintf('%d : %d\n',i,length(TargetFiles));
    
    try
        Current_XLS_File = [TargetDir filesep TargetFiles(i).name];
        
        [~,~,RawData] = xlsread(Current_XLS_File,SheetName);
        
        Header = RawData(1,:);
        RowData = RawData(2:end,:);
        
        %% Temporary Purpose for adding File Value date
        %         Split_Str = strsplit(TargetFiles(i).name,'_');
        %         CurrentFile_ValueDate = Split_Str(end-1);
        %
        %         ValueDate_Data = cell(size(RowData,1),1);
        %         ValueDate_Data(:,:) = CurrentFile_ValueDate;
        %
        %         RowData = [ValueDate_Data , RowData(:,1:9)];
        %         Header = [{'Value_Date'},Header];
        
        %%
        OverallData = [OverallData;RowData];
        
    catch
        copyfile(Current_XLS_File,Destination_Path);
        continue;
    end
    
end

Split_Str = strsplit(TargetDir,'\');
Excel_FileName = [char(Split_Str(end)),'.xlsx'] ;
xlswrite(Excel_FileName,[Header;OverallData]);

msgbox(Excel_FileName,'Finish');

