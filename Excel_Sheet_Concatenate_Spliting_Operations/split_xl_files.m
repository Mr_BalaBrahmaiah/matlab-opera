function split_xl_files

[filename, pathname] = uigetfile( ...
    {'*.xlsx';'*.xls'}, ...
    'Pick a file');
InXLSFilename = fullfile(pathname,filename);

%% Output Path

Split_Str = strsplit(InXLSFilename,'\') ;
Output_Folder = strrep(char(Split_Str(end)),'.xlsx','') ;

Output_Path = [pwd filesep 'Output' filesep strrep(char(Split_Str(end)),'.xlsx','')] ;

if(~exist(Output_Path))
    mkdir(Output_Path);
else
    [status, ~, ~] = rmdir(Output_Path,'s') ;
    while(status)
        [status, ~, ~] = rmdir(Output_Path,'s') ;
    end
    mkdir(Output_Path);
end

%%
[~,~,RawData] = xlsread(InXLSFilename);
Header = RawData(1,:);
Data   = RawData(2:end,:);

[~,StrFilename,StrExt] = fileparts(InXLSFilename);

[NumRows,NumCols] = size(Data);

LimitLines = 40000;

if NumRows > LimitLines
    NumFiles = ceil(NumRows/LimitLines);
    
    for iFile = 1:NumFiles
        
        NewFilename = [Output_Path filesep StrFilename,'_',num2str(iFile),'.xlsx'];
        
        if iFile ~= NumFiles
            xlswrite(NewFilename,[Header;Data(1:LimitLines,:)]);
            Data(1:LimitLines,:) = [];
        else
            xlswrite(NewFilename,[Header;Data(1:end,:)]);
            Data(1:end,:) = [];
        end
        
    end
    
end

msgbox(Output_Path,'Finish');







