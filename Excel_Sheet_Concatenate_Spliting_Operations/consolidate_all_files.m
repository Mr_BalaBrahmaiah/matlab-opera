[filename, pathname] = uigetfile( ...
    {'*.xlsx';'*.*'}, ...
    'Pick a file','MultiSelect', 'on');

StrPattern = strrep(datestr(now),' ','_');
StrPattern = strrep(StrPattern,':','-');
OutFilename = ['Consolidated_OutFile_',StrPattern,'.xlsx'];

StartingFileName  = 'DIRReport_Summary_';  % change the file name in this line
SheetName = 'Pnp Deal Dump'; % change the sheet name here

if pathname ~= 0
    StrFilenames = strrep(filename,StartingFileName,'');
    StrFilenames = StrFilenames';
    StrFilenames = strrep(StrFilenames,'.xlsx','');
      
    [Dates,Times] = strtok(StrFilenames,'_');
    Times = strrep(Times,'_','');
    Times = strrep(Times,'-',':');
    
    OutData = [];
    for iFile = 1:length(filename)
        disp(iFile);
        Fullfilename = fullfile(pathname,filename{iFile});
        [~,~,RawData ] = xlsread(Fullfilename,SheetName); 
        TRData = cell2dataset(RawData);
        
        %replace the below fieldnames based on file
        ValueDate = repmat(Dates(iFile),size(TRData.subportfolio));
        UpdatedTimestamp = repmat(Times(iFile),size(TRData.subportfolio));
        
        RowData = [ValueDate,UpdatedTimestamp,TRData.portfolio,TRData.subportfolio,TRData.tid,TRData.product_code,TRData.contract_month,num2cell(TRData.mtm_usd)];
        
        OutData = [OutData;RowData];
    end
    
%     TempData = sortrows(OutData,[9,10]);    
%     xlswrite(OutFilename,dataset2cell(TempData));

xlswrite(OutFilename,OutData);
    
%     OutFilename = 'Consolidated_Traders_report_2015.csv';
%     customcsvwrite(OutFilename,OutData(2:end,:),OutData(1,:));
    
end