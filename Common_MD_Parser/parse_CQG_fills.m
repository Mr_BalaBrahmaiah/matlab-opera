function OutData = parse_CQG_fills(varargin)

try
    OutData = [];
    MissingAccountNo = {};
    MissingID = [];
    
    if nargin == 1
        CQG_FilePath = varargin{1};
    else
        if nargin == 2
            InPathname = varargin{2};
        else
            InPathname = '';
        end
        [CQG_FileName,CQG_PathName]  = uigetfile('*.*','Select the CQG Fills file',InPathname);
        %         [CQG_MappingFileName,CQG_MappingPathName]  = uigetfile('*.*','Select the CQG Mapping file',InPathname);
        
        if isnumeric(CQG_FileName) && CQG_FileName==0
            errordlg('XTR Fills File not selected!','Missing Deals Parser');
            return;
        else
            CQG_FilePath = fullfile(CQG_PathName,CQG_FileName);
            %             CQG_Map_FilePath = fullfile(CQG_MappingPathName,CQG_MappingFileName);
        end
    end
    
    %% Read EXCEL SHEETS
    [~,~,CQG_Data] =xlsread(CQG_FilePath);
    [~,~,CQG_Map_Data] =xlsread('MissingDeals_Master.xlsx','CQG Fills Conversion');  %% CQG_Map_FilePath
    
    %% User want Columns Only
    User_Header = {'Trades_AccountName','Trades_ContractName','Trades_FCMName','Trades_Price',...
        'Trades_Side','Trades_Size','Trades_TradeDate'};
    
    for i = 1 : length(User_Header)
        
        Matched_Field = find(strcmpi(User_Header{i},CQG_Data));
        Matched_Field = Matched_Field(1);
        [~,ColNum] = ind2sub(size(CQG_Data),Matched_Field);
        Required_Field = CQG_Data(:,ColNum);
        Required_Field = Required_Field(2:end,:);
        
        CurrVarname = cell2mat(User_Header(i));
        InputData.(CurrVarname)= Required_Field;
        
    end
    
    %% Get Traders Contract Name
    Traders_ContractName_Field = find(strcmpi('Trades_ContractName',CQG_Data));
    Traders_ContractName_Field = Traders_ContractName_Field(1);
    [~,ColNum] = ind2sub(size(CQG_Data),Traders_ContractName_Field);
    Traders_ContractName = CQG_Data(:,ColNum);
    Traders_ContractName = Traders_ContractName(2:end,:);
    
    Month_Trades = cellfun(@(x) x(end-2:end), Traders_ContractName, 'UniformOutput', false);
    Parsed_ContractName = regexprep(Traders_ContractName,'F.US.','');
    
    Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
    MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
    
    %% Remopve NaN Fields in CQG Mapping Data
    Header = CQG_Map_Data(1,:);
    Row_find_NaN = CQG_Map_Data(:,1);
    
    Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);
    CQG_Map_Data(:,Nan_Col(:)) = [];
    
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_NaN);
    CQG_Map_Data(Nan_Row(:),:) = [];
    
    %% Get CQG Mapping
    CQG_ProductCode_Field = find(strcmpi('ProductCode',CQG_Map_Data));
    CQG_ProductCode_Field = CQG_ProductCode_Field(1);
    [~,MapColNum] = ind2sub(size(CQG_Map_Data),CQG_ProductCode_Field);
    CQG_ProductCode = CQG_Map_Data(:,MapColNum);
    CQG_ProductCode = CQG_ProductCode(2:end,:);
    CQG_ProductCode = regexprep(CQG_ProductCode,'F.US.','');
    
    CQG_AccounNo = CQG_Map_Data(2:end,1);
    
    CQG_Product = cell(size(Parsed_ContractName));
    Matching_ID = zeros(size(Parsed_ContractName,1),1);
    
    for iCode = 1:length(CQG_ProductCode)
        Idx = find((~cellfun(@isempty,regexp(Parsed_ContractName,CQG_ProductCode{iCode}))) & strcmpi(CQG_AccounNo{iCode},InputData.Trades_AccountName));
        if(~isempty(Idx))
            Matching_ID(Idx,1) = Idx;
            CQG_Product(Idx) = CQG_ProductCode(iCode);  %% Remove Empty CQG ProductCode
        end
    end
    
    %     CQG_FillHeader = {'Trades_AccountName','Trades_ContractName','Parsed_ContractName','CQG_Product','Month','Trades_FCMName','Trades_Price',...
    %         'Trades_Side','Trades_Size','Trades_TradeDate'};
    %     CQG_FillData = [InputData.Trades_AccountName,InputData.Trades_ContractName,Parsed_ContractName,CQG_Product,Month_Trades,...
    %         InputData.Trades_FCMName,InputData.Trades_Price,InputData.Trades_Side,InputData.Trades_Size,InputData.Trades_TradeDate];
    
    %% CQG File
    
    AccProdMapping = CQG_Map_Data;
    
    %%
    AccProdMapping = AccProdMapping(2:end,:);
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN         = cellfun(@isnan,AccProdMapping(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    AccProdMapping(IdxNaNRows,:) = [];
    
    AccNumber = AccProdMapping(:,1);
    XTRProductCode = AccProdMapping(:,2);
    Desc    = AccProdMapping(:,3);
    PremDivBy8 = cell2mat(AccProdMapping(:,4));
    PriceConversion = AccProdMapping(:,5);
    RefXTRAccNum =  AccProdMapping(:,6);
    
    NewTradePrice = InputData.Trades_Price;
    Lots = cell2mat(InputData.Trades_Size);
    
    %   TempYearMonth = cellstr(datestr(datenum(YearMonth,'dd-mm-yyyy'),'mmm-yy'));
    if isdeployed
        YearMonth = cellstr(datestr(InputData.Trades_TradeDate,'dd-mm-yyyy'));
    else
        YearMonth = InputData.Trades_TradeDate;
    end
    NewYM = char(YearMonth);
    TempYearMonth = cellstr(datestr(cellstr(NewYM(:,1:end-5)),'mmm-yy'));
    YearMonthCode = cell(size(TempYearMonth));
    Maturity = cell(size(TempYearMonth));
    AccountNumber = cell(size(TempYearMonth));
    Description = cell(size(TempYearMonth));
    Strike = cell(size(TempYearMonth));
    
    for i = 1:length(CQG_AccounNo)
        %         YrMon = TempYearMonth{i};
        %         ProdCode = Parsed_ContractName{i};
        IdxProd = (~cellfun(@isempty,regexp(Parsed_ContractName,CQG_ProductCode{i}))) & strcmpi(CQG_AccounNo{i},InputData.Trades_AccountName) ;
        % find((~cellfun(@isempty,regexp(Parsed_ContractName,ProdCode))) & strcmpi(CQG_AccounNo{iCode},InputData.Trades_AccountName));
        YrMon = TempYearMonth(IdxProd);
        
        if isempty(find(IdxProd))
            MissingAccountNo = [MissingAccountNo,InputData.Trades_AccountName{i}];
            MissingID = [MissingID,i];
            IsError = 1;
            
            Maturity(IdxProd,1) = {''};
            AccountNumber(IdxProd,1) = {''};
            Description(IdxProd,1) = {''};
            YearMonthCode(IdxProd,1) = {''};
            NewTradePrice(IdxProd) = {0};
            continue;
        end
        YrMon = char(YrMon(1));
        YearMonthCode(IdxProd,1) = cellstr([MonthCode{strncmpi(YrMon,Month,3)},YrMon(end)]);
        Maturity(IdxProd,1) = cellstr(strcat(char(YearMonthCode(find(IdxProd))),'.',char(YearMonthCode(find(IdxProd)))));
        AccountNumber(IdxProd,1) = AccNumber(i); %#ok<*AGROW>
        Description(IdxProd,1) = Desc(i);
        if PremDivBy8(i)
            Temp   = num2str(InputData.Trades_Price{IdxProd});
            TempNo = str2num(Temp(1:end-1)); %#ok<*ST2NM>
            NewTradePrice{IdxProd} = TempNo + (str2num(Temp(end))/8);
        end
        
        aa = find(IdxProd);
        for k = 1 : length(find(IdxProd))
            %         NewTradePrice(IdxProd) = {[NewTradePrice{IdxProd}] * PriceConversion{i}};
            NewTradePrice(aa(k)) = {NewTradePrice{aa(k)} * PriceConversion{i}};
        end
        if strcmpi(InputData.Trades_Side(IdxProd),'Short')
            Lots(IdxProd) = -(Lots(IdxProd));
        end
    end
    
    if isdeployed
        TradeDate  = cellstr(datestr(InputData.Trades_TradeDate,'dd-mm-yyyy'));
        Side = InputData.Trades_Side;
    else
        TradeDate  = InputData.Trades_TradeDate;
        Side = InputData.Trades_Side;
    end
    
    
    if IsError
        ErrorAccnum = unique(MissingAccountNo);
        warndlg(['CQG Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(in CQG-Fills Conversion and in CQG-TR Format Master sheets) to get these account deals'],'MD Parser');
        TradeDate(MissingID) = [];
        Side(MissingID) = [];
        Lots(MissingID) = [];
        Strike(MissingID) = [];
        CQG_Product(MissingID) = [];
        YearMonthCode(MissingID) = [];
        Maturity(MissingID) =  [];
        AccountNumber(MissingID) =  [];
        Description(MissingID) = [];
        NewTradePrice(MissingID) =  [];
        InputData.Trades_AccountName(MissingID) = [];
        InputData.Trades_FCMName(MissingID) = [];
        Month_Trades(MissingID) = [];     %% YearMonthCode
    end
    
    ProductType = cellstr(repmat('future',size(Side)));
    Long_Index = (~cellfun(@isempty,regexp(Side,'Long')));   %% Changing in Side Variable Long into bought & Short into sold
    Short_Index = (~cellfun(@isempty,regexp(Side,'Short')));
    Side(Long_Index) = {'bought'};
    Side(Short_Index) = {'sold'};
    
    %%% No need to use Month & Maturity Variable in CQG Fills %% Month Should be two Charcter
    Month_Trades =char(Month_Trades);
    Month_Trades(:,2) = '';
    Month_Trades = cellstr(Month_Trades);
    Maturity_Trades = strcat(Month_Trades,'.',Month_Trades);
    
    OutData      = [AccountNumber, TradeDate, Side, num2cell(Lots), Month_Trades, Description, Strike, ProductType, NewTradePrice,...
        Maturity_Trades,InputData.Trades_AccountName,InputData.Trades_FCMName];
    OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType',...
        'TradePrice','Maturity','CQGAccountNumber','CounterParty'};
    Output       = [OutputHeader;OutData];
    
    %     waitbar(1,hWaitbar);
    
    if exist('TradeDate','var')
        OutXLSFileName = ['CQG_Fills_',TradeDate{1},'.xls'];
    else
        OutXLSFileName = ['CQG_Fills_',datestr(today,'ddmmyyyy'),'.xls'];
    end
    OutXLSFileName = strrep(OutXLSFileName,'/','_');
    xlswrite(OutXLSFileName,Output,'CQG_Fills');
    OutXLSFileName = fullfile(pwd,OutXLSFileName);
    try
        xls_delete_sheets(OutXLSFileName);
    catch
    end
    %     delete(hWaitbar);
    msgbox(['XTR Fills statement is saved in ',char(OutXLSFileName)],'Missing Deals Parser');
    
catch ME
    errordlg(ME.message,'Missing Deals Parser');
end
end


