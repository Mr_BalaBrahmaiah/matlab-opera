function [CounterParty_Changed_NettedTRData,Not_MatchedPortfolio] = Changing_CounterParty_Using_MasterSheet(Type_Name,NettedTRData,Master_Path)

CounterParty_Changed_NettedTRData = [];

ConsoNew_NettedTRData = [];
Matched_Portfolio = [];
Not_MatchedPortfolio = [];
try
    if strcmpi(Type_Name,'agf')
        [~,~,ConsoMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(Type_Name,'usg')
        [~,~,ConsoMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    end
        
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), ConsoMapping(:,1)); %% Nan_Row = find(Nan_Row);
    ConsoMapping(Nan_Row(:),:) = [];
    ConsoMapping = ConsoMapping(2:end,:);
    
    Unique_NettedTR = unique(NettedTRData(2:end,2))';
    Unique_ConsoNetted = unique(ConsoMapping(:,1))';
    Portfolio_MatchedIndex = cellStrfind_Exact(Unique_NettedTR,Unique_ConsoNetted);
    
    Unique_NettedTR(Portfolio_MatchedIndex) = [];
    
    if(~isempty(Unique_NettedTR))
        warndlg(['Conso Mapping Portfolio',Unique_NettedTR,' Which is not available in Master File(in Conso Mapping). Please update this account number in master file(in Conso Mapping Sheet) to get these account deals'],'MD Parser');
    end
    
    ConsoHeader = NettedTRData(1,:);
    Conso_NettedTRData = NettedTRData(2:end,:);
    
    %                         ConsoNew_NettedTRData = [];
    %                         Not_MatchedPortfolio = [];
    %                         Matched_Portfolio = [];
    
    NettedTRData_Portfolio = strtrim(Conso_NettedTRData(:,2));
    NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,8));   %% If Column 25 or 27
    % NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,9)); %% If Column 32
    
    for k = 1 : length(ConsoMapping(:,1))
        ConsoMapping_Portfolio = strtrim(ConsoMapping{k,1});
        ConsoMapping_Counterparty = strtrim(ConsoMapping{k,2});
        
        MatchedIndex = find(strcmpi(ConsoMapping_Portfolio, NettedTRData_Portfolio) & ...
            strcmpi(ConsoMapping_Counterparty, NettedTRData_CounterParty));
        
        if(~isempty(MatchedIndex))
            Temp_Conso = Conso_NettedTRData(MatchedIndex,:);
            %             Temp_Conso(:,2) = {strtrim(ConsoMapping{k,3})};
            Temp_Conso(:,8) =  {strtrim(ConsoMapping{k,3})};
            ConsoNew_NettedTRData = [ConsoNew_NettedTRData ; Temp_Conso];
            
            Temp_Conso = [];
            
            Matched_Portfolio =  [Matched_Portfolio ; {ConsoMapping_Portfolio}];
            
            % Conso_NettedTRData(MatchedIndex,2) = {strtrim(ConsoMapping{k,3})};
            % Conso_NettedTRData(MatchedIndex,8) = {strtrim(ConsoMapping{k,4})};  %% If Column 25 25 or 27
            % Conso_NettedTRData(MatchedIndex,9) = {strtrim(ConsoMapping{k,4})};  %% If Column 32
            
        else
            
            Not_MatchedPortfolio = [Not_MatchedPortfolio ; {ConsoMapping_Portfolio}];
        end
        
    end
    
    %% 
    CounterParty_Changed_NettedTRData = [ConsoHeader ; ConsoNew_NettedTRData] ;
        
catch ME
    OutErrorMsg = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end