function OutData = parse_murex_extract(varargin)

try
    OutData = [];
    
    if nargin == 1
        MurexExtractFile = varargin{1};
    else
        [FileName,PathName]  = uigetfile('*.xlsx','Select the Murex Extract file');
        
        if isnumeric(FileName) && FileName==0
            errordlg('Murex Extract File not selected!','Missing Deals Parser');
            return;
        else
            MurexExtractFile = fullfile(PathName,FileName);
        end
    end
    
    if ~isempty(MurexExtractFile)
        [~,~,RawData ] = xlsread(MurexExtractFile);
    else
        return;
    end
    
    ErrorLogFile = ['FF_MD_LogFile_',datestr(today,'ddmmyyyy'),'.txt'];
    fid = fopen(ErrorLogFile,'wt');
    
    hWaitbar = waitbar(0.1,'Please wait, extracting data from Murex Extract file ...');
    
    Header = RawData(1,:);
    AllData = RawData(2:end,:);
    
    PosTradeDate   = strcmpi('TRN.Date',Header);
    TempPosInstrument  = strcmpi('DSP_LABEL',Header);
    IdxInstrument = find(TempPosInstrument);
    if length(IdxInstrument) > 1 % since two fields with DSP_LABEL is present
        PosInstrument = IdxInstrument(1);
    end
    PosSide        = strcmpi('B/S',Header);
    PosQuantity    = strcmpi('NOMINAL 0',Header);
    PosMaturity    = strcmpi('MATURITY',Header);
    PosStrike      = strcmpi('STRIKE',Header);
    PosTradePrice  = strcmpi('RATE 0',Header);
    PosProductType = strcmpi('C/P',Header);
    PosPortfolio   = strcmpi('LABEL',Header);
    TempPosCP      = strcmpi('COUNTERPART',Header);
    IdxCP = find(TempPosCP);
    if length(IdxCP) > 1 % since two fields with DSP_LABEL is present
        PosCounterparty = IdxInstrument(end);
    end
    
    [~,~,AccProdMapping] = xlsread('MissingDeals_Master.xlsx','Murex Extract Conversion');
    AccProdMapping  = AccProdMapping(2:end,:);
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN         = cellfun(@isnan,AccProdMapping(:,3),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    AccProdMapping(IdxNaNRows,:) = [];
    
    RefInstrument   = AccProdMapping(:,1);
    RefDescription  = AccProdMapping(:,2);
    RefAccNum       = AccProdMapping(:,3);
    RefPortfolio    = AccProdMapping(:,4);
    PriceFac = cell2mat(AccProdMapping(:,5));
    StrikeFac= cell2mat(AccProdMapping(:,6));
    
    Counterparty  = AllData(:,PosCounterparty);
    IdxValidRows = [];
    IdxNaN         = cellfun(@isnan,Counterparty,'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if ~any(IdxNaN{iR})
            IdxValidRows = [IdxValidRows; iR];
        end
    end
    Data          = AllData(IdxValidRows,:);
    
    try
        TradeDate = cellstr(datestr(datenum(Data(:,PosTradeDate),'dd/mm/yyyy'),'dd-mm-yyyy'));
    catch
        TradeDate = Data(:,PosTradeDate);
    end
    Instrument    = Data(:,PosInstrument);
    Side          = Data(:,PosSide);
    Quantity      = Data(:,PosQuantity);
    Maturity      = strtok(Data(:,PosMaturity),'.');
    Strike        = Data(:,PosStrike);
    TradePrice    = Data(:,PosTradePrice);
    ProductType   = Data(:,PosProductType);
    Portfolio     = Data(:,PosPortfolio);
    
    Description   = cell(size(Instrument));
    AccountNumber = cell(size(Instrument));
    NewStrike     = cell(size(Instrument));
    NewTradePrice = cell(size(Instrument));
    NewProductType = repmat(cellstr('future'),size(ProductType));        
    
    NumProds = length(RefInstrument);
    for iCode = 1:NumProds
        IdxInstrumentMatch = logical(zeros(size(Instrument)));
        IdxPortfolioMatch  = logical(zeros(size(Instrument)));      
        
        IdxInstrumentMatch(cellStrfind(Instrument,RefInstrument{iCode})) = 1;
        IdxPortfolioMatch(strmatch(RefPortfolio{iCode},Portfolio)) = 1;
        IdxProd = IdxInstrumentMatch & IdxPortfolioMatch;
        if ~isempty(IdxProd)
            Description(IdxProd)   = RefDescription(iCode);
            AccountNumber(IdxProd) = RefAccNum(iCode);
            NewStrike(IdxProd)     = num2cell(cell2mat(Strike(IdxProd)) .* StrikeFac(iCode));
            NewTradePrice(IdxProd) = num2cell(cell2mat(TradePrice(IdxProd)) .* PriceFac(iCode));
        end
    end
    
    IdxDescNotFound  = cellfun(@isempty,Description);
    IdxAccNoNotFound = cellfun(@isempty,AccountNumber);
    if any(IdxDescNotFound)
        InstrErrorStr = ['Description is not available for instrument ''',convertCell2Char(Instrument(IdxDescNotFound)),''' in Master XLS file. These trades will not be considered appropriately in MD process!'];
        fprintf(fid,'%s\n',InstrErrorStr);
        Description(IdxDescNotFound) = {''};          
    end
    if any(IdxAccNoNotFound)
        InstrErrorStr = ['AccountNumber is not available for instrument ''',convertCell2Char(Instrument(IdxAccNoNotFound)),''' in Master XLS file. These trades will not be considered appropriately in MD process!'];
        fprintf(fid,'%s\n',InstrErrorStr);
        AccountNumber(IdxAccNoNotFound) = {''}; 
    end
    
    fclose(fid);
     
    IdxPut = strcmpi('p',ProductType);
    IdxCall = strcmpi('c',ProductType);
    NewProductType(IdxCall) = {'call'};
    NewProductType(IdxPut) = {'put'};
    
    IdxSell = strcmpi('S',Side);
    Quantity = cell2mat(Quantity);
    Quantity(IdxSell) = -(Quantity(IdxSell));
    Lots = num2cell(Quantity);
    
    OutData      = [AccountNumber, TradeDate, Side, Lots, Maturity, Description, NewStrike, NewProductType, NewTradePrice];
    OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice'};
    Output       = [OutputHeader;OutData];
    
    
    if exist('TradeDate','var')
        OutXLSFileName = ['Murex_Extract_',TradeDate{1},'.xls'];
    else
        OutXLSFileName = ['Murex_Extract_',datestr(today,'ddmmyyyy'),'.xls'];
    end
    
    waitbar(1,hWaitbar);
    
    xlswrite(OutXLSFileName,Output,'Murex_Extract');
    OutXLSFileName = fullfile(pwd,OutXLSFileName);
    xls_delete_sheets(OutXLSFileName);
    delete(hWaitbar);
    msgbox(['Murex Extract statement is saved in ',char(OutXLSFileName)],'Missing Deals Parser');
catch ME
    errordlg(ME.message,'Missing Deals Parser');
end
end
