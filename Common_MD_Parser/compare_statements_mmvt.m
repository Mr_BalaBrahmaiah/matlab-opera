 function TPData = compare_statements_mmvt(UniqueAccNo,RefTrades,CompTrades,ErrorMessage,CompString)
        
        TPData = [];
        
        
        hWaitbar = waitbar(0,'Please wait,comparing the statements to find the missing deals ...');
        for iAccNo = 1:length(UniqueAccNo)
            try
                Accnum = UniqueAccNo(iAccNo);
                waitbar(iAccNo/length(UniqueAccNo),hWaitbar);
                % compare in buy trades
                % filter by buy/sell
                
                %brokers
                IdxRefTrades = ismember(cell2mat(RefTrades(:,PosAccNo)),Accnum);
                IdxCompTrades = ismember(cell2mat(CompTrades(:,PosAccNo)),Accnum);
                
                RefYM  = unique(RefTrades(IdxRefTrades,PosYearMonth));
                for iYM = 1:length(RefYM)
                    IdxRefYM = zeros(size(IdxRefTrades));     IdxRefYM   = logical(IdxRefYM);
                    IdxCompYM = zeros(size(IdxCompTrades));   IdxCompYM = logical(IdxCompYM);
                    
                    YrMon = RefYM{iYM};
                    IdxRefYM(IdxRefTrades) = strcmpi(YrMon,RefTrades(IdxRefTrades,PosYearMonth));
                    TradePrices = unique(cell2mat(RefTrades(IdxRefYM,PosTradePrice)));
                    
                    IdxCompYM(IdxCompTrades) = strcmpi(YrMon,CompTrades(IdxCompTrades,PosYearMonth));
                    
                    for iTP = 1:length(TradePrices)
                        IdxRefTP = zeros(size(IdxRefTrades));   IdxRefTP = logical(IdxRefTP); %#ok<*LOGL>
                        
                        IdxCompTP = zeros(size(IdxCompTrades));   IdxCompTP = logical(IdxCompTP);
                        IdxCompFut = zeros(size(IdxCompTrades));  IdxCompFut = logical(IdxCompFut);
                        IdxCompCall = zeros(size(IdxCompTrades)); IdxCompCall = logical(IdxCompCall);
                        IdxCompPut = zeros(size(IdxCompTrades));  IdxCompPut = logical(IdxCompPut);
                        
                        InRef1 = cell2mat(RefTrades(IdxRefYM,PosTradePrice));
                        InRef2 = TradePrices(iTP) .* ones(size(InRef1));
                        IdxRefTP(IdxRefYM)   =  arrayfun(@isnumericequal,InRef1,InRef2);
                        
                        InComp1 = cell2mat(CompTrades(IdxCompYM,PosTradePrice));
                        InComp2 = TradePrices(iTP) .* ones(size(InComp1));
                        IdxCompTP(IdxCompYM) =  arrayfun(@isnumericequal,InComp1,InComp2);
                        
                        Premium = TradePrices(iTP);
                        TradeDates = unique(RefTrades(IdxRefTP,PosTradeDate));
                        
                        for iTRDate = 1:length(TradeDates)
                            IdxRefFut = zeros(size(IdxRefTrades));  IdxRefFut = logical(IdxRefFut);
                            IdxRefCall = zeros(size(IdxRefTrades)); IdxRefCall = logical(IdxRefCall);
                            IdxRefPut = zeros(size(IdxRefTrades));  IdxRefPut = logical(IdxRefPut);
                            IdxRefDate = zeros(size(IdxRefTrades)); IdxRefDate = logical(IdxRefDate);
                            IdxCompDate = zeros(size(IdxCompTrades)); IdxCompDate =logical(IdxCompDate);
                            
                            TradeDate = TradeDates{iTRDate};
                            IdxRefDate(IdxRefTP)   = strcmpi(TradeDate,RefTrades(IdxRefTP,PosTradeDate));
                            IdxCompDate(IdxCompTP) = strcmpi(TradeDate,CompTrades(IdxCompTP,PosTradeDate));
                            
                            % check in Futures
                            RefLots = NaN; CompLots = NaN;
                            IdxRefFut(IdxRefDate) = strcmpi('future',RefTrades(IdxRefDate,PosProdType));
                            if any(IdxRefFut)
                                RefLots = sum(cell2mat(RefTrades(IdxRefFut,PosQuantity)));
                                Desc = [RefYM{iYM},' ',char(unique(RefTrades(IdxRefFut,PosDesc)))];
                            end
                            if ~isempty(find(IdxCompDate))
                                IdxCompFut(IdxCompTP) = strcmpi('future',CompTrades(IdxCompDate,PosProdType));
                                if any(IdxCompFut)
                                    CompLots = sum(cell2mat(CompTrades(IdxCompFut,PosQuantity)));
                                end
                            end
                            TPData = [TPData;construct_output(IdxRefFut)];
                            
                            %check in vanilla_call
                            RefLots = NaN; CompLots = NaN;
                            IdxRefCall(IdxRefDate) = strcmpi('call',RefTrades(IdxRefDate,PosProdType));
                            IdxCompCall(IdxCompDate) = strcmpi('call',CompTrades(IdxCompDate,PosProdType));
                            if any(IdxRefCall)
                                CallStrike = unique(cell2mat(RefTrades(IdxRefCall,PosStrike)),'stable');
                                AllCallStrike = cell2mat(RefTrades(IdxRefCall,PosStrike));
                                for iStrike = 1:length(CallStrike)
                                    IdxRefStrikeCall  = zeros(size(IdxRefTrades));  IdxRefStrikeCall = logical(IdxRefStrikeCall);
                                    IdxCompStrikeCall = zeros(size(IdxCompTrades)); IdxCompStrikeCall = logical(IdxCompStrikeCall);
                                    
                                    CurrStrike                     =  CallStrike(iStrike) .* ones(size(AllCallStrike));
                                    IdxRefStrikeCall(IdxRefCall)   =  arrayfun(@isnumericequal,AllCallStrike,CurrStrike);
                                    
                                    RefLots = sum(cell2mat(RefTrades(IdxRefStrikeCall,PosQuantity)));
                                    Desc = ['Call-',num2str(CallStrike(iStrike)),' ',RefYM{iYM},' ',char(unique(RefTrades(IdxRefCall,PosDesc)))];
                                    if any(IdxCompCall)
                                        AllCallCompStrike = cell2mat(CompTrades(IdxCompCall,PosStrike));
                                        CurrStrike        = CallStrike(iStrike) .* ones(size(AllCallCompStrike));
                                        IdxCompStrikeCall(IdxCompCall) =  arrayfun(@isnumericequal,AllCallCompStrike,CurrStrike);
                                        if any(IdxCompStrikeCall)
                                            CompLots  = sum(cell2mat(CompTrades(IdxCompStrikeCall,PosQuantity)));
                                        end
                                    end
                                    TPData = [TPData;construct_output(IdxRefCall)]; %#ok<*AGROW>
                                end
                            end
                            
                            
                            % check in vanilla_put
                            RefLots = NaN; CompLots = NaN;
                            IdxRefPut(IdxRefDate) = strcmpi('put',RefTrades(IdxRefDate,PosProdType));
                            IdxCompPut(IdxCompDate) = strcmpi('put',CompTrades(IdxCompDate,PosProdType));
                            if any(IdxRefPut)
                                PutStrike = unique(cell2mat(RefTrades(IdxRefPut,PosStrike)),'stable');
                                AllPutStrike = cell2mat(RefTrades(IdxRefPut,PosStrike));
                                for iStrike = 1:length(PutStrike)
                                    IdxRefStrikePut  = zeros(size(IdxRefTrades));  IdxRefStrikePut = logical(IdxRefStrikePut);
                                    IdxCompStrikePut = zeros(size(IdxCompTrades)); IdxCompStrikePut = logical(IdxCompStrikePut);
                                    
                                    CurrStrike                 =  PutStrike(iStrike) .* ones(size(AllPutStrike));
                                    IdxRefStrikePut(IdxRefPut) =  arrayfun(@isnumericequal,AllPutStrike,CurrStrike);
                                    
                                    RefLots = sum(cell2mat(RefTrades(IdxRefStrikePut,PosQuantity)));
                                    Desc = ['Put-',num2str(PutStrike(iStrike)),' ',RefYM{iYM},' ',char(unique(RefTrades(IdxRefPut,PosDesc)))];
                                    if any(IdxCompPut)
                                        AllPutCompStrike = cell2mat(CompTrades(IdxCompPut,PosStrike));
                                        CurrStrike       = PutStrike(iStrike) .* ones(size(AllPutCompStrike));
                                        IdxCompStrikePut(IdxCompPut) =  arrayfun(@isnumericequal,AllPutCompStrike,CurrStrike);
                                        if any(IdxCompStrikePut)
                                            CompLots  = sum(cell2mat(CompTrades(IdxCompStrikePut,PosQuantity)));
                                        end
                                    end
                                    TPData = [TPData;construct_output(IdxRefPut)]; %#ok<*AGROW>
                                end
                            end
                            
                            
                        end
                    end
                end
                
            catch
                ErrorStr = ['Error in Missing Deals in Account Number : ',Accnum,' found in ',CompString,' contract month ',YrMon,' premium ',num2str(Premium),' trade date ',TradeDate];
                fprintf(fid,'%s\n',ErrorStr);
                errordlg('Unknown error occured in MD!Please check the log file','Missing Deals Parser');
                continue;
            end
        end
        
        delete(hWaitbar);
        
        function TPData = construct_output(Idx)
            TPData = [];
            if any(Idx) && ~isnumericequal(RefLots,CompLots)
                if isnan(CompLots)
                    ErrorMsg = ErrorMessage;
                elseif ~isnumericequal(RefLots,CompLots)
                    ErrorMsg = LotErrorMsg;
                end
                
                if strmatch('Broker',ErrorMsg)
                    if ~ismember(cellStrfind(RefDesc, char(unique(RefTrades(Idx,PosDesc)))), find(ismember(RefAccNo, Accnum)) ) % if the description and account no does not match, then throw it as wrong confirmation
                        ErrorMsg = {'Wrong trade confirmation in Trade.xls!'};
                    end
                    TPData = [TradeDate,Accnum,num2cell(CompLots),Desc,num2cell(Premium),num2cell(RefLots),ErrorMsg];
                elseif strmatch('Missing',ErrorMsg) %#ok<*MATCH2>
                    if ~ismember(cellStrfind(RefDesc, char(unique(RefTrades(Idx,PosDesc)))), find(ismember(RefAccNo, Accnum)) ) % if the description and account no does not match, then throw it as wrong confirmation
                        ErrorMsg = {'Wrong trade confirmation in Broker Statement!'};
                    end
                    TPData = [TradeDate,Accnum,num2cell(RefLots),Desc,num2cell(Premium),num2cell(CompLots),ErrorMsg];
                else
                    if strcmpi(CompString,'FromBroker')
                        TPData = [TradeDate,Accnum,num2cell(RefLots),Desc,num2cell(Premium),num2cell(CompLots),ErrorMsg];
                    end
                end
            end
        end
    end
