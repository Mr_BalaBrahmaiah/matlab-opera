function [OutData,BrokerData,XTRFillsData,TraderSnapsData,StatementDate,Not_GettingPrice,fid] = generate_missing_deals(Type_Name,XTR_File_Path,BrokerStatement,TSFileNames,Multi_PnP_Excel_Path, Option_BrokerTRData)

% try

OutData= [];

Master_Path = [pwd filesep 'Master Files' filesep];

% read the data from broker statement and % read the data from traders snaps
if(strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
    if(~isempty(BrokerStatement))
        [BrokerData,StatementDate,InitPathName] = parse_broker_statement(BrokerStatement,Type_Name);
    else
        BrokerData = [];
        StatementDate = [];
        InitPathName = '';
    end
    if(~isempty(TSFileNames))
        TraderSnapsData = parse_traders_snaps(TSFileNames,Type_Name);
    else
        TraderSnapsData = [];
    end
    
    if(~isempty(Multi_PnP_Excel_Path))
        TraderSnapsData_Multi_PnP = parse_traders_multi_PNP_snaps(Multi_PnP_Excel_Path,Type_Name);
        
        TraderSnapsData = [TraderSnapsData ; TraderSnapsData_Multi_PnP];
    else
        TraderSnapsData_Multi_PnP = [];
    end
    
else
    BrokerData = [];
    TraderSnapsData =[];
    StatementDate = [];
end

if(strcmpi(Type_Name,'cop'))
    if(~isempty(BrokerStatement))
        [BrokerData,StatementDate,InitPathName] = parse_broker_statement(BrokerStatement,Type_Name);
    else
        BrokerData = [];
        StatementDate = [];
        InitPathName = '';
    end
end
% read the data from XTR fills file
if( strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg') || strcmpi(Type_Name,'orx') || strcmpi(Type_Name,'cop') )
    [XTRFillsData,Not_GettingPrice] = parse_xtr_fills([],pwd,Type_Name,XTR_File_Path);
    
else
    XTRFillsData = [];
end


%%

% StatementDate = '';
ErrorLogFile = ['Invenio_MD_LogFile_',char(StatementDate),'.txt'];
fid = fopen(ErrorLogFile,'wt');
% StatementDate = datestr(today-1,'dd-mm-yyyy');

if( ~isempty(BrokerStatement) && (strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'cop') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg')) )
    
    % % Header = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice'};
    PosAccNo = 1;
    PosTradeDate = 2;
    PosSide = 3;
    PosQuantity = 4;
    PosYearMonth = 5;
    PosDesc = 6;
    PosStrike = 7;
    PosProdType = 8;
    PosTradePrice = 9;
    PosMaturity  =10;
    
    % used only in traders snaps
    PosExeBroker = 11;
    PosClearer = 12;
    
    % begin the missing deals process only if both broker and traders data are
    % available
    XTRFills = [XTRFillsData ; Option_BrokerTRData];
    if ~isempty(BrokerData) || (~isempty(XTRFills) || ~isempty(TraderSnapsData))
        if(~isempty(BrokerData))
            BrokerData(:,PosAccNo) = num2cell(str2num(char(BrokerData(:,PosAccNo))));
        else
            BrokerData = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice','Maturity'};
        end
        if ~isempty(TraderSnapsData)
            % consider the required snaps from Traders snaps data
            % since futures are taken from XTRader files, they need to be ignored in
            % traders snaps
            IdxRemove = ~(strncmpi('wells',TraderSnapsData(:,PosClearer),length('wells')) & strcmpi('XTrader',TraderSnapsData(:,PosExeBroker)) & strcmpi('Future',TraderSnapsData(:,PosProdType)));
            IdxData = strncmpi('wells',TraderSnapsData(:,PosClearer),length('wells')) & IdxRemove;
            TradersData = TraderSnapsData(IdxData,1:10);
            
            % If the traded price is not given in Traders Snaps data, assume it
            % as zero and take it for MD process
            TempTradersPrice = cell2mat(TradersData(:,PosTradePrice));
            TempTradersPrice(isnan(TempTradersPrice)) = 0;
            TradersData(:,PosTradePrice) = num2cell(TempTradersPrice);
            
            % check if traders snaps or xtr fills file contain data that are not of
            % statement date. if so, inform it and ignore those entries from missing
            % deal process
            IdxTRS = ~ismember(TradersData(:,PosTradeDate),StatementDate);
            if any(IdxTRS)
                try
                    TradeDesc = unique(TradersData(IdxTRS,PosDesc));
                    TradeDesc = TradeDesc';
                    warndlg(['Traders snaps file contain ',TradeDesc,' trades that are not done on ',char(StatementDate),' .Those trades will not be considered for Missing Deals Process and in TR dump!'],'Missing Deals Parser');
                catch
                    warndlg(['Traders snaps file contain trades that are not done on ',char(StatementDate),' .Those trades will not be considered for Missing Deals Process and in TR dump!'],'Missing Deals Parser');
                end
                TradersData(IdxTRS,:) =[];
                try
                    % Remove those lines from TR dump as well
                    IdxTRDump =  ~ismember(TraderSnapsData(:,PosTradeDate),StatementDate);
                    TraderSnapsData(IdxTRDump,:) =[];
                    % If the traded price is not given in Traders Snaps data, assume it
                    % as zero and take it for TR dump as well
                    TempTradersPrice = cell2mat(TraderSnapsData(:,PosTradePrice));
                    TempTradersPrice(isnan(TempTradersPrice)) = 0;
                    TraderSnapsData(:,PosTradePrice) = num2cell(TempTradersPrice);
                catch
                end
            end
        else
            TradersData = [];
        end
        
        if ~isempty(XTRFills)
            % check if xtr fills file contain data that are not of
            % statement date. if so, inform it and ignore those entries from missing
            % deal process and in tr dump
            IdxXTR = ~ismember(XTRFills(:,PosTradeDate),StatementDate);
            if any(IdxXTR)
                try
                    TradeDesc = unique(XTRFills(IdxXTR,PosDesc));
                    TradeDesc = TradeDesc';
                    warndlg(['XTR Fills file contain ',TradeDesc,' trades that are not done on ',char(StatementDate),' .Those trades will not be considered for Missing Deals Process and in TR dump!'],'Missing Deals Parser');
                catch
                    warndlg(['XTR Fills file contain trades that are not done on ',char(StatementDate),' .Those trades will not be considered for Missing Deals Process and in TR dump!'],'Missing Deals Parser');
                end
                XTRFills(IdxXTR,:) =[];
            end
            TempXTRFillsData = XTRFills(:,1:10); % since the last field XTR account number is not needed in MD
        else
            TempXTRFillsData = [];
        end
        % cumulative traders data
        InputTRData = [TempXTRFillsData;TradersData];
        if(isempty(InputTRData))
            InputTRData = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType',...
                'TradePrice','Maturity','Trader Name','Transaction Date Time','XTRAccountNumber','CounterParty'} ; %% XTR Fills Header
        end
        
        % read the TR master file
        if(strcmpi(Type_Name,'cfs'))
            [~,~,ProductMaster] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'TR Format Master');
        elseif(strcmpi(Type_Name,'cop'))
            [~,~,ProductMaster] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'TR Format Master');
        elseif(strcmpi(Type_Name,'agf'))
            [~,~,ProductMaster] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'TR Format Master');
        elseif(strcmpi(Type_Name,'usg'))
            [~,~,ProductMaster] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'TR Format Master');
        else
        end
        
        ProductMaster = ProductMaster(2:end,:);
        % To remove the lines of xl whcih contains NaN in all columns; the data
        % which gets removed from xl, but the row remains still
        IdxNaNRows = [];
        IdxNaN         = cellfun(@isnan,ProductMaster(:,4),'UniformOutput',false);
        for iRNaN = 1:length(IdxNaN)
            if any(IdxNaN{iRNaN})
                IdxNaNRows = [IdxNaNRows; iRNaN];
            end
        end
        ProductMaster(IdxNaNRows,:) = [];
        RefAccNo   = cell2mat(ProductMaster(:,4));
        RefDesc    = ProductMaster(:,3);
        
        % generate the missing deals by comparing broker and traders data
        BrokerBuyTrades  = BrokerData(strcmpi(BrokerData(:,PosSide),'B'),:);
        TRBuyTrades   = InputTRData(strcmpi(InputTRData(:,PosSide),'B'),:);
        
        BrokerSellTrades = BrokerData(strcmpi(BrokerData(:,PosSide),'S'),:);
        TRSellTrades  = InputTRData(strcmpi(InputTRData(:,PosSide),'S'),:);
        
        LotErrorMsg = {'Lot Error. Is it valid?'};
        MDErrorMsg  = {'Missing Deal in Trade.xls. Valid?'};
        BrokerMsg   = {'Broker Not Confirmed the Deal. Valid?'};
        
        % comparision from Broker To Trader
        BrokerAccNo = unique(cell2mat(BrokerData(:,PosAccNo)));
        % buy trades
        OutData = [OutData; compare_statements(BrokerAccNo,BrokerBuyTrades,TRBuyTrades,MDErrorMsg,'FromBroker')];
        % sell trades
        OutData = [OutData; compare_statements(BrokerAccNo,BrokerSellTrades,TRSellTrades,MDErrorMsg,'FromBroker')];
        % OutData = [OutData; compare_statements(BrokerAccNo,BrokerData,InputTRData,MDErrorMsg)];
        
        % comparision from Trader to Broker
        TradersAccNo = unique(cell2mat(InputTRData(:,PosAccNo)));
        % buy trades
        OutData = [OutData; compare_statements(TradersAccNo,TRBuyTrades,BrokerBuyTrades,BrokerMsg,'FromTrader')];
        % sell trades
        OutData = [OutData; compare_statements(TradersAccNo,TRSellTrades,BrokerSellTrades,BrokerMsg,'FromTrader')];
        % OutData = [OutData; compare_statements(TradersAccNo,InputTRData,BrokerData,BrokerMsg)];
        
        if ~isempty(OutData)
            % Replace NaN with Nil in the position of Lots
            Idx = cellfun(@isnan,OutData(:,3));
            OutData(Idx,3) = {'Nil'};
            Idx = cellfun(@isnan,OutData(:,6));
            OutData(Idx,6) = {'Nil'};
            
            
            Header1 = {'Current Missing deals :-',[],[],[],'Entries per',[],'Remarks by',[]};
            Header2 = {'Trade Date','Brokers Confirmation','Buy / (Sell) Lots','Details','Trade Price','Trade.xls','Risk Back Office','Traders Initials & Response'};
            OutData = [OutData cell(size(OutData,1),1)];
            %       OutData = sortrows(OutData,2);
            
            % add blank rows after every account number
            OutMDData = [];
            out_acc = unique(cell2mat(OutData(:,2)));
            for iNum = 1:length(out_acc)
                Idx = ismember(cell2mat(OutData(:,2)),out_acc(iNum));
                TempData = OutData(Idx,:);
                BlankRow = cell(1,size(OutData,2));
                OutMDData = [OutMDData;BlankRow;TempData];
            end
            
            %       Output = [Header1;Header2;OutData];
            Output = [Header1;Header2;OutMDData];
            
            TitleDate = char(StatementDate);
            
            
            % OutXLSFileName = ['Missing_Deals_',datestr(today,'dd-mmm-yyyy'),'.xls'];
            OutXLSFileName = [upper(char(Type_Name)),'_','Missing_Deals_',TitleDate,'.xls'];
            xlswrite(OutXLSFileName,Output,TitleDate);
            OutXLSFileName = fullfile(pwd,OutXLSFileName);
            xls_delete_sheets(OutXLSFileName);
            msgbox(['Missing Deals are saved in ',char(OutXLSFileName)],'Missing Deals Parser');
        else
            msgbox('No missing deals found!','Missing Deals Parser');
        end
    end
    % catch ME
    %     errordlg('Unknown error occured in MD!','Missing Deals Parser');
    % end
    
end

    function TPData = compare_statements(UniqueAccNo,RefTrades,CompTrades,ErrorMessage,CompString)
        
        TPData = [];
        
        
        hWaitbar = waitbar(0,'Please wait,comparing the statements to find the missing deals ...');
        for iAccNo = 1:length(UniqueAccNo)
            try
                Accnum = UniqueAccNo(iAccNo);
                waitbar(iAccNo/length(UniqueAccNo),hWaitbar);
                % compare in buy trades
                % filter by buy/sell
                
                %brokers
                IdxRefTrades = ismember(cell2mat(RefTrades(:,PosAccNo)),Accnum);
                IdxCompTrades = ismember(cell2mat(CompTrades(:,PosAccNo)),Accnum);
                
                Unique_Product = unique(RefTrades(IdxRefTrades,6));
                for k = 1 : length(Unique_Product)
                    Current_Product = Unique_Product(k);
                    
                    IdxRefTrades = ismember(cell2mat(RefTrades(:,PosAccNo)),Accnum) & strcmpi(RefTrades(:,6),Current_Product) ;
                    IdxCompTrades = ismember(cell2mat(CompTrades(:,PosAccNo)),Accnum) & strcmpi(CompTrades(:,6),Current_Product) ;
                    
                    RefYM  = unique(RefTrades(IdxRefTrades,PosYearMonth));
                    for iYM = 1:length(RefYM)
                        IdxRefYM = zeros(size(IdxRefTrades));     IdxRefYM   = logical(IdxRefYM);
                        IdxCompYM = zeros(size(IdxCompTrades));   IdxCompYM = logical(IdxCompYM);
                        
                        YrMon = RefYM{iYM};
                        IdxRefYM(IdxRefTrades) = strcmpi(YrMon,RefTrades(IdxRefTrades,PosYearMonth));
                        TradePrices = unique(cell2mat(RefTrades(IdxRefYM,PosTradePrice)));
                        
                        IdxCompYM(IdxCompTrades) = strcmpi(YrMon,CompTrades(IdxCompTrades,PosYearMonth));
                        
                        for iTP = 1:length(TradePrices)
                            IdxRefTP = zeros(size(IdxRefTrades));   IdxRefTP = logical(IdxRefTP); %#ok<*LOGL>
                            
                            IdxCompTP = zeros(size(IdxCompTrades));   IdxCompTP = logical(IdxCompTP);
                            IdxCompFut = zeros(size(IdxCompTrades));  IdxCompFut = logical(IdxCompFut);
                            IdxCompCall = zeros(size(IdxCompTrades)); IdxCompCall = logical(IdxCompCall);
                            IdxCompPut = zeros(size(IdxCompTrades));  IdxCompPut = logical(IdxCompPut);
                            
                            InRef1 = cell2mat(RefTrades(IdxRefYM,PosTradePrice));
                            InRef2 = TradePrices(iTP) .* ones(size(InRef1));
                            IdxRefTP(IdxRefYM)   =  arrayfun(@isnumericequal,InRef1,InRef2);
                            
                            InComp1 = cell2mat(CompTrades(IdxCompYM,PosTradePrice));
                            InComp2 = TradePrices(iTP) .* ones(size(InComp1));
                            IdxCompTP(IdxCompYM) =  arrayfun(@isnumericequal,InComp1,InComp2);
                            
                            Premium = TradePrices(iTP);
                            TradeDates = unique(RefTrades(IdxRefTP,PosTradeDate));
                            
                            for iTRDate = 1:length(TradeDates)
                                IdxRefFut = zeros(size(IdxRefTrades));  IdxRefFut = logical(IdxRefFut);
                                IdxRefCall = zeros(size(IdxRefTrades)); IdxRefCall = logical(IdxRefCall);
                                IdxRefPut = zeros(size(IdxRefTrades));  IdxRefPut = logical(IdxRefPut);
                                IdxRefDate = zeros(size(IdxRefTrades)); IdxRefDate = logical(IdxRefDate);
                                IdxCompDate = zeros(size(IdxCompTrades)); IdxCompDate =logical(IdxCompDate);
                                
                                TradeDate = TradeDates{iTRDate};
                                IdxRefDate(IdxRefTP)   = strcmpi(TradeDate,RefTrades(IdxRefTP,PosTradeDate));
                                IdxCompDate(IdxCompTP) = strcmpi(TradeDate,CompTrades(IdxCompTP,PosTradeDate));
                                
                                % check in Futures
                                RefLots = NaN; CompLots = NaN;
                                IdxRefFut(IdxRefDate) = strcmpi('future',RefTrades(IdxRefDate,PosProdType));
                                if any(IdxRefFut)
                                    RefLots = sum(cell2mat(RefTrades(IdxRefFut,PosQuantity)));
                                    Desc = [RefYM{iYM},' ',char(unique(RefTrades(IdxRefFut,PosDesc)))];
                                end
                                if ~isempty(find(IdxCompDate))
                                    IdxCompFut(IdxCompTP) = strcmpi('future',CompTrades(IdxCompDate,PosProdType));
                                    if any(IdxCompFut)
                                        CompLots = sum(cell2mat(CompTrades(IdxCompFut,PosQuantity)));
                                    else
                                        CompLots = NaN;
                                    end
                                end
                                TPData = [TPData;construct_output(IdxRefFut)];
                                
                                %check in vanilla_call
                                RefLots = NaN; CompLots = NaN;
                                IdxRefCall(IdxRefDate) = strcmpi('call',RefTrades(IdxRefDate,PosProdType));
                                IdxCompCall(IdxCompDate) = strcmpi('call',CompTrades(IdxCompDate,PosProdType));
                                if any(IdxRefCall)
                                    CallStrike = unique(cell2mat(RefTrades(IdxRefCall,PosStrike)),'stable');
                                    AllCallStrike = cell2mat(RefTrades(IdxRefCall,PosStrike));
                                    for iStrike = 1:length(CallStrike)
                                        IdxRefStrikeCall  = zeros(size(IdxRefTrades));  IdxRefStrikeCall = logical(IdxRefStrikeCall);
                                        IdxCompStrikeCall = zeros(size(IdxCompTrades)); IdxCompStrikeCall = logical(IdxCompStrikeCall);
                                        
                                        CurrStrike                     =  CallStrike(iStrike) .* ones(size(AllCallStrike));
                                        IdxRefStrikeCall(IdxRefCall)   =  arrayfun(@isnumericequal,AllCallStrike,CurrStrike);
                                        
                                        RefLots = sum(cell2mat(RefTrades(IdxRefStrikeCall,PosQuantity)));
                                        Desc = ['Call-',num2str(CallStrike(iStrike)),' ',RefYM{iYM},' ',char(unique(RefTrades(IdxRefCall,PosDesc)))];
                                        if any(IdxCompCall)
                                            AllCallCompStrike = cell2mat(CompTrades(IdxCompCall,PosStrike));
                                            CurrStrike        = CallStrike(iStrike) .* ones(size(AllCallCompStrike));
                                            IdxCompStrikeCall(IdxCompCall) =  arrayfun(@isnumericequal,AllCallCompStrike,CurrStrike);
                                            if any(IdxCompStrikeCall)
                                                CompLots  = sum(cell2mat(CompTrades(IdxCompStrikeCall,PosQuantity)));
                                            else
                                                CompLots = NaN;
                                            end
                                        end
                                        TPData = [TPData;construct_output(IdxRefCall)]; %#ok<*AGROW>
                                    end
                                end
                                
                                
                                % check in vanilla_put
                                RefLots = NaN; CompLots = NaN;
                                IdxRefPut(IdxRefDate) = strcmpi('put',RefTrades(IdxRefDate,PosProdType));
                                IdxCompPut(IdxCompDate) = strcmpi('put',CompTrades(IdxCompDate,PosProdType));
                                if any(IdxRefPut)
                                    PutStrike = unique(cell2mat(RefTrades(IdxRefPut,PosStrike)),'stable');
                                    AllPutStrike = cell2mat(RefTrades(IdxRefPut,PosStrike));
                                    for iStrike = 1:length(PutStrike)
                                        IdxRefStrikePut  = zeros(size(IdxRefTrades));  IdxRefStrikePut = logical(IdxRefStrikePut);
                                        IdxCompStrikePut = zeros(size(IdxCompTrades)); IdxCompStrikePut = logical(IdxCompStrikePut);
                                        
                                        CurrStrike                 =  PutStrike(iStrike) .* ones(size(AllPutStrike));
                                        IdxRefStrikePut(IdxRefPut) =  arrayfun(@isnumericequal,AllPutStrike,CurrStrike);
                                        
                                        RefLots = sum(cell2mat(RefTrades(IdxRefStrikePut,PosQuantity)));
                                        Desc = ['Put-',num2str(PutStrike(iStrike)),' ',RefYM{iYM},' ',char(unique(RefTrades(IdxRefPut,PosDesc)))];
                                        if any(IdxCompPut)
                                            AllPutCompStrike = cell2mat(CompTrades(IdxCompPut,PosStrike));
                                            CurrStrike       = PutStrike(iStrike) .* ones(size(AllPutCompStrike));
                                            IdxCompStrikePut(IdxCompPut) =  arrayfun(@isnumericequal,AllPutCompStrike,CurrStrike);
                                            if any(IdxCompStrikePut)
                                                CompLots  = sum(cell2mat(CompTrades(IdxCompStrikePut,PosQuantity)));
                                            else
                                                CompLots = NaN;
                                            end
                                        end
                                        TPData = [TPData;construct_output(IdxRefPut)]; %#ok<*AGROW>
                                    end
                                end
                                
                                
                            end
                        end
                    end
                end
            catch ME
                ErrorStr = ['Error in Missing Deals in Account Number : ',Accnum,' found in ',CompString,' contract month ',YrMon,' premium ',num2str(Premium),' trade date ',TradeDate];
                fprintf(fid,'%s\n',ErrorStr);
                errordlg('Unknown error occured in MD!Please check the log file','Missing Deals Parser');
                continue;
            end
            
        end
        
        delete(hWaitbar);
        
        function TPData = construct_output(Idx)
            TPData = [];
            if any(Idx) && ~isnumericequal(RefLots,CompLots)
                if isnan(CompLots)
                    ErrorMsg = ErrorMessage;
                elseif ~isnumericequal(RefLots,CompLots)
                    ErrorMsg = LotErrorMsg;
                end
                
                if strmatch('Broker',ErrorMsg)
                    if ~ismember(cellStrfind(RefDesc, char(unique(RefTrades(Idx,PosDesc)))), find(ismember(RefAccNo, Accnum)) ) % if the description and account no does not match, then throw it as wrong confirmation
                        ErrorMsg = {'Wrong trade confirmation in Trade.xls!'};
                    end
                    TPData = [TradeDate,Accnum,num2cell(CompLots),Desc,num2cell(Premium),num2cell(RefLots),ErrorMsg];
                elseif strmatch('Missing',ErrorMsg) %#ok<*MATCH2>
                    if ~ismember(cellStrfind(RefDesc, char(unique(RefTrades(Idx,PosDesc)))), find(ismember(RefAccNo, Accnum)) ) % if the description and account no does not match, then throw it as wrong confirmation
                        ErrorMsg = {'Wrong trade confirmation in Broker Statement!'};
                    end
                    TPData = [TradeDate,Accnum,num2cell(RefLots),Desc,num2cell(Premium),num2cell(CompLots),ErrorMsg];
                else
                    if strcmpi(CompString,'FromBroker')
                        TPData = [TradeDate,Accnum,num2cell(RefLots),Desc,num2cell(Premium),num2cell(CompLots),ErrorMsg];
                    end
                end
            end
        end
    end

    function IsEqualVal = isnumericequal(Num1,Num2)
        IsEqualVal = abs(Num1 - Num2) <= 0.0000000001;
    end


end