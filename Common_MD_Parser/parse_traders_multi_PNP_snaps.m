function OutData = parse_traders_multi_PNP_snaps(TSFileNames,Type_Name)


Master_Path = [pwd filesep 'Master Files' filesep];

OutData = [];

%%
if isempty(TSFileNames)
    return;
end

%%

ObjDB = connect_to_database;
Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');

% if nargin == 1
%     DefPathName = varargin{1};
% else
%     DefPathName = '';
% end
%
% [FileName,PathName]  = uigetfile('*.xlsx','Select the Traders snapshot files','Multiselect','on',DefPathName);
%
% if isnumeric(FileName) && FileName==0
%     errordlg('Traders snapshot File not selected!','Missing Deals Parser');
%     return;
% else
%     TSFileNames = fullfile(cellstr(PathName),cellstr(FileName));
NumFiles = length(TSFileNames);
if(strcmpi(Type_Name,'cfs'))
    [~,~,AccProdMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'Traders Snaps Conversion');
elseif(strcmpi(Type_Name,'agf'))
    [~,~,AccProdMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Traders Snaps Conversion');
elseif(strcmpi(Type_Name,'usg'))
    [~,~,AccProdMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Traders Snaps Conversion');
else
    
end
AccProdMapping = AccProdMapping(2:end,:);
% To remove the lines of xl whcih contains NaN in all columns; the data
% which gets removed from xl, but the row remains still
IdxNaNRows = [];
IdxNaN  = cellfun(@isnan,AccProdMapping(:,1),'UniformOutput',false);
for iR = 1:length(IdxNaN)
    if any(IdxNaN{iR})
        IdxNaNRows = [IdxNaNRows; iR];
    end
end
AccProdMapping(IdxNaNRows,:) = [];

AccNumber = AccProdMapping(:,1);
TSCode = AccProdMapping(:,2);
Desc    = AccProdMapping(:,3);

hWaitbar = waitbar(0,'Please wait, extracting data from Traders snapshot files ...');
for iFile =1:NumFiles
    waitbar(iFile/NumFiles,hWaitbar);
    try
        [~,~,RawData ] =xlsread(TSFileNames{iFile},'Positions');
        
        DateIdx = find(strcmpi('Today',RawData));
        DateIdx = DateIdx(1);
        [RowNum,ColNum] = ind2sub(size(RawData),DateIdx);
        
        try
            TempDate  = RawData(RowNum,ColNum+1) ;
            
            if isdeployed
                [System_Date_Format,~] = get_System_Date_Format();
                if(strcmpi(System_Date_Format,'mm/dd/yyyy'))
                    TempDate = cellstr(datestr(datenum(TempDate,System_Date_Format),'dd-mm-yyyy'));
                end
            end
            
        catch
            TempDate  = cellstr(datestr(Settle_Date,'dd-mm-yyyy'));
            % TempDate  = cellstr(datestr(today-1,'dd-mm-yyyy'));
        end
        
        RawData = RawData(:,1:25);
        
        
        StartIdx = find(strcmpi('Date',RawData));
        if isempty(StartIdx)
            StartIdx = find(strcmpi('Portfolio',RawData));
        end
        
        ProductData =[];
        
        Header = strtrim(RawData(StartIdx,:));
        
        PosProduct     = strcmpi('Product',Header);
        PosQuantity    = strcmpi('Lots',Header);
        PosYearMonth   = strcmpi('Month',Header);
        PosStrike      = strcmpi('Strike',Header);
        PosProdType    = strcmpi('Put/Call/Fut',Header);
        PosTradePrice  = strcmpi('Prem/Rate',Header);
        PosExeBroker   = strcmpi('Exe-Broker',Header); % 8
        PosClearer     = strcmpi('Clearer/CP',Header);% 9
        PosExeType     = strcmpi('Exe Type',Header); % 10
        PosRates       = strcmpi('Rates',Header); %11
        PosSpclRate    = strcmpi('Spcl. Rate /lot',Header); %12
        PosFutSource   = strcmpi('Future Source',Header); %13
        
        
        AllData        = RawData(StartIdx+1:end,:);
        Product        = AllData(:,PosProduct);
        
        %% Get Perfect Matched Product Code
        
        Perfect_FileName = strsplit(TSFileNames{iFile},'\');
        Perfect_ProductName = strsplit(char(Perfect_FileName(end)),'_');
        Perfect_ProductName = Perfect_ProductName(1:4)';
        None_Index = strcmpi(Perfect_ProductName,'None');
        Perfect_ProductName(None_Index) = []; %% Remove None Index
        
        Matched_ProductCode_Index = cellStrfind_Exact(Product,Perfect_ProductName);
        if(~isempty(find(Matched_ProductCode_Index, 1)))
            AllData = AllData(Matched_ProductCode_Index,:);
        else
            continue;
        end
        
        %%
        IdxValidRows = [];
        IdxNaN         = cellfun(@isnan,AllData(:,PosProduct),'UniformOutput',false);
        for iR = 1:length(IdxNaN)
            if ~any(IdxNaN{iR})
                IdxValidRows = [IdxValidRows; iR];
            end
        end
        
        Data = AllData(IdxValidRows,:);
        
        if ~isempty(Data)
            
            Productcode    = Data(:,PosProduct);
            Quantity       = Data(:,PosQuantity);
            Strike         = Data(:,PosStrike);
            ProductType    = Data(:,PosProdType);
            YearMonth      = upper(strtok(Data(:,PosYearMonth),'.'));
            TradePrice     = Data(:,PosTradePrice);
            ExeBroker      = Data(:,PosExeBroker);
            Clearer        = Data(:,PosClearer);
            ExeType        = Data(:,PosExeType);
            Rates          = Data(:,PosRates);
            SpclRate       = Data(:,PosSpclRate);
            Maturity       = Data(:,PosYearMonth);
            FutSource      = Data(:,PosFutSource);
            
            TradeDate     = repmat(TempDate,size(Productcode));
            
            Side           = cell(size(Quantity));
            Side(cell2mat(Quantity) <= 0) = {'S'};
            Side(cell2mat(Quantity) > 0) = {'B'};
            
            AccountNumber = {}; Description ={};
            for i =1:length(Productcode)
                ProdCode = Productcode{i};
                AccountNumber(i,1) = AccNumber(strcmpi(ProdCode,TSCode)); %#ok<*AGROW>
                Description(i,1) = Desc(strcmpi(ProdCode,TSCode));
            end
            
            ProductData  = [AccountNumber, TradeDate, Side, Quantity, YearMonth, Description, Strike, ProductType, TradePrice,Maturity,ExeBroker,Clearer,ExeType,Rates,SpclRate,FutSource];
        end
        OutData = [OutData;ProductData];
        
    catch ME
        errordlg(['Unknown error occured while parsing the Traders snaps file - ''',TSFileNames{iFile},'''. This snaps file will be skipped!'],'MD-Traders Snaps Error');
        continue;
    end
end

if ~isempty(OutData)
    OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice','Maturity','ExeBroker','Clearer','ExeType','Rates','SpclRate','FutureSource'};
    Output       = [OutputHeader;OutData];
    
    if exist('TradeDate','var')
        
        OutXLSFileName = strrep(getXLSFilename([upper(char(Type_Name)),'_Multi_PnP_Trader_snaps']),'_TR_Dump','');
        %         TimeStamp = strrep(strrep(datestr(datetime,'HH:MM:SS'),' ','_'),':','-');
        %         OutXLSFileName = [upper(char(Type_Name)),'_Multi_PnP_Trader_snaps_',datestr(datenum(TradeDate{1},23)),'_',TimeStamp,'.xlsx'];%% In Matlab it shows next month date wrong Conversion
    else
        TimeStamp = strrep(strrep(datestr(datetime,'HH:MM:SS'),' ','_'),':','-');
        OutXLSFileName = [upper(char(Type_Name)),'_Multi_PnP_Trader_snaps_',datestr(today,'ddmmyyyy'),'_',TimeStamp,'.xlsx'];
    end
    
    %     Output(2:end,2) = cellstr(datestr(Output(2:end,2),'dd-mm-yyyy')); %% For StatementDate Matching from BrokerStatement
    xlswrite(OutXLSFileName,Output,'Multi_PnP');
    OutXLSFileName = fullfile(pwd,OutXLSFileName);
    
    try
        xls_delete_sheets(OutXLSFileName);
    catch
    end
    delete(hWaitbar);
    msgbox(['Traders snapshots are saved in ',char(OutXLSFileName)],'Missing Deals Parser');
end
end
% end