function [Option_NettedTRData , Option_BrokerTRData] = generate_Option_XTR_Fills(InBUName , Current_XTR_File_Path)

try    
    Master_Path = [pwd filesep 'Master Files' filesep];
    
    Option_NettedTRData = [];
    Option_BrokerTRData = [];
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%%% XTR Traders Data 
    [~,~,RawData] = xlsread(char(Current_XTR_File_Path));  %% input data

    RawData(:,1) = [];  %%% delete first col
    Raw_Data = RawData(2:end,:);
    if isempty(Raw_Data)        %%% Checking condition? data is found or not
        warndlg('No data found in selected Traders file!') ;
        return;
    end

    %%%% Remove Blanks colomns @ Strike
    IdxNaNRows = [];
    IdxNaN = cellfun(@isnan,RawData(:,9),'UniformOutput',false);
    for iRNaN = 1:length(IdxNaN)
        if any(IdxNaN{iRNaN})
            IdxNaNRows = [IdxNaNRows; iRNaN];
        end
    end
    RawData(IdxNaNRows,:) = [];
    RawData_Header = RawData(1,:);
    RawData_set = cell2dataset(RawData);
    
    %%% Filter the data using Alias_Master Table
    [~,~,Alias_Master_Data] = xlsread([Master_Path filesep 'XTRFills_Broker_Alias_Master.xlsx']);
    Alias_Master_Data = cell2dataset(Alias_Master_Data);
    
    if isempty(Alias_Master_Data)           %%% Checking condition? data is found or not
        warndlg('No data found in XTRFills_Broker_Alias_Master file!') ;
        return;
    end

    %%% Check Alias
    Raw_Alias_Data = RawData_set(contains(RawData_set.Alias,unique(Alias_Master_Data.Alias),'ignoreCase',true),:);
    
    if isempty(Raw_Alias_Data)
        warndlg(['No ',upper(char(InBUName)),' Trades for Corresponding Date']) ;
        return;
    end 

    %%% Header and Data
    RawData_Alias_Data = Raw_Alias_Data;

    %%% Read Missing Deals Master File
    if(strcmpi(InBUName,'cfs'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR Fills Conversion Options');
    elseif(strcmpi(InBUName,'agf'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR Fills Conversion Options');
    elseif(strcmpi(InBUName,'usg'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR Fills Conversion Options');
    else
        warndlg([upper(char(InBUName)),'_MissingDeals_Master-Option.xlsx is not found']) ;
        return;
    end

    AccProdMapping_Data = AccProdMapping(2:end,:);
    if isempty(AccProdMapping_Data)           %%% Checking condition? data is found or not
        warndlg('No data found in MissingDeals_Master-Option file!') ;
        return;
    end

    %%%  Remove NaN Fields
    Header = AccProdMapping(1,:);
    NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
    AccProdMapping(:,NaN_Col(:)) = [];

    Row_find_Nan = AccProdMapping(:,4);
    NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
    AccProdMapping(NaN_Row(:),:) = [];

    Master_Header = AccProdMapping(1,:);
    AccProdMapping = AccProdMapping(2:end,:);

    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN    = cellfun(@isnan,AccProdMapping(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    AccProdMapping(IdxNaNRows,:) = [];

    AccProdMapping = cell2dataset([Master_Header ; AccProdMapping]);
    
    %%% Mapping the two tables (XTR Fills Conversion Options and Input RawData
    %%% Table and get the matched data based on Account number and product code)
    [r1,~] = size(RawData_Alias_Data);
    Raw_AccPro_Data_Group=[]; Missing_Account_Number = [];
    for i1=1:1:r1
        field_product=RawData_Alias_Data.Product(i1);
        field_Acc_numb=RawData_Alias_Data.AccountNumber(i1);         
        Indx=find(strcmpi(field_Acc_numb,AccProdMapping.XTRAccountNo) & strcmpi(field_product,AccProdMapping.ProductCode));

        if isempty(Indx)
            Missing_Account_Number = unique([Missing_Account_Number,field_Acc_numb]);
            continue;
        end

        Raw_Matched_Data = AccProdMapping(Indx,:);

        if ~isempty(Raw_Matched_Data)
            index=find(strcmp(Raw_Matched_Data.XTRAccountNo,RawData_Alias_Data.AccountNumber));
            if (index)
                Unique_Group_Data=dataset2cell(RawData_Alias_Data(i1,:));
                Matched_RawData1=[Raw_Matched_Data.AccountNo Raw_Matched_Data.Description Raw_Matched_Data.PremiumDivBy8 Raw_Matched_Data.PriceConversion Unique_Group_Data(2:end,:)];
                Raw_AccPro_Data_Group=vertcat(Raw_AccPro_Data_Group,Matched_RawData1);
            end
        end          
    end

    if isempty(Raw_AccPro_Data_Group)
        warndlg(['No ',upper(char(InBUName)),' Trades are matching with MissingDeals_Master-Option.xlsx-XTR Fills Conversion Options file']) ;
        return;
    end

    ErrorAccnum = (unique(Missing_Account_Number));
    if(strcmpi(InBUName,'agf'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' OPTION-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(in XTR-Fills Conversion and in XTR-TR Format Master sheets) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' OPTION-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end

        elseif(strcmpi(InBUName,'cfs'))

        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' OPTION-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' OPTION-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end

        elseif(strcmpi(InBUName,'usg'))

        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' OPTION-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' OPTION-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
    end

    %%%% Header and Data for after matching MissingDeals_Master-Option Table
    Raw_Master_Header = [Master_Header(1) Master_Header(3) Master_Header(4) Master_Header(5) RawData_Header];
    Raw_AccPro_Master_Data = [Raw_Master_Header ; Raw_AccPro_Data_Group];

    Raw_Master_Data_All = cell2dataset(Raw_AccPro_Master_Data);

    %%% User required data fields
    Raw_Master_user_Data = [num2cell(Raw_Master_Data_All.AccountNo), Raw_Master_Data_All.Description ,num2cell(Raw_Master_Data_All.PremiumDivBy8), num2cell(Raw_Master_Data_All.PriceConversion), ...
                        Raw_Master_Data_All.Broker, Raw_Master_Data_All.Alias, Raw_Master_Data_All.Product, Raw_Master_Data_All.Contract, ...
                        num2cell(Raw_Master_Data_All.Strike), Raw_Master_Data_All.C_P, Raw_Master_Data_All.ProductType, Raw_Master_Data_All.ExpDate, ...
                        Raw_Master_Data_All.B_S, num2cell(Raw_Master_Data_All.Qty), num2cell(Raw_Master_Data_All.Price), Raw_Master_Data_All.AccountNumber, ...
                        Raw_Master_Data_All.RiskAccount, Raw_Master_Data_All.ExchDate, Raw_Master_Data_All.ExchTime, Raw_Master_Data_All.TrdID, ...
                        Raw_Master_Data_All.Username, Raw_Master_Data_All.OrderDate, Raw_Master_Data_All.OrderTime];

    %%% User required header fields
    Raw_Master_user_Header = {'AccountNo','Description', 'PremiumDivBy8', 'PriceConversion', 'Broker', 'Alias', 'Product', ...
                                'Contract', 'Strike', 'C_P', 'ProductType', 'ExpDate', 'B_S', 'Qty', 'Price', 'AccountNumber', ...
                                'RiskAccount', 'ExchDate', 'ExchTime', 'TrdID', 'Username', 'OrderDate', 'OrderTime'};
    Raw_Master_user_Data = cell2dataset([Raw_Master_user_Header ; Raw_Master_user_Data]);
    
    %%% Product Master @ XTR
    if(strcmpi(InBUName,'cfs'))
        [~,~,ProductMaster] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'agf'))
        [~,~,ProductMaster] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'usg'))
        [~,~,ProductMaster] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    else
        warndlg([upper(char(InBUName)),'_MissingDeals_Master-Option.xlsx is not found']) ;
        return;
    end

    ProductMaster_Data = ProductMaster(2:end,:);
    if isempty(ProductMaster_Data)           %%% Checking condition? data is found or not
        warndlg(['No data found in ',upper(char(InBUName)),'_MissingDeals_Master-Option.xlsx-XTR-TR Format Master File!']) ;
        return;
    end

    %%%  Remove NaN Fields
    Header = ProductMaster(1,:);
    NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
    ProductMaster(:,NaN_Col(:)) = [];

    Row_find_Nan = ProductMaster(:,4);
    NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
    ProductMaster(NaN_Row(:),:) = [];

    Pro_Master_Header1 = ProductMaster(1,:);
    ProductMaster = ProductMaster(2:end,:);

    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN  = cellfun(@isnan,ProductMaster(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    ProductMaster(IdxNaNRows,:) = [];

    ProductMaster = cell2dataset([Pro_Master_Header1 ; ProductMaster]);
    
    %%% Mapping the two tables (XTR-TR Format Master and Input RawData
    %%% Table and get the matched data)
    Raw_Product_Data=[];
    [rr,~]=size(Raw_Master_user_Data);
    for i1=1:1:rr
        field_Acc_numb1=Raw_Master_user_Data.AccountNumber(i1);  
        field_descr=Raw_Master_user_Data.Description(i1);
        Indx1=strcmpi(field_Acc_numb1,ProductMaster.AccountNumber) & strcmpi(field_descr,ProductMaster.ProductDescription);
        Raw_pro_Matched_Data = ProductMaster(Indx1,:);
        if ~isempty(Raw_pro_Matched_Data)
            Master_user_want_Data = [Raw_pro_Matched_Data.Portfolio Raw_pro_Matched_Data.Instrument Raw_pro_Matched_Data.ProductCode Raw_pro_Matched_Data.Strike_div Raw_pro_Matched_Data.Prem_div];
            Unique_Group_Data1=dataset2cell(Raw_Master_user_Data(i1,:));
            Raw_pro_Matched_Data1=[Master_user_want_Data Unique_Group_Data1(2:end,:)];
            Raw_Product_Data=vertcat(Raw_Product_Data,Raw_pro_Matched_Data1);
        end  
    end

    if isempty(Raw_Product_Data)
        warndlg(['No ',upper(char(InBUName)),' Trades are matching with MissingDeals_Master-Option.xlsx-XTR-TR Format Master File!']) ;
        return;
    end 

    %%%% Header and Data for after matching Product_Master Table
    Raw_pro_Master_Header=[Pro_Master_Header1(1) Pro_Master_Header1(2) Pro_Master_Header1(5) Pro_Master_Header1(6) Pro_Master_Header1(7) Raw_Master_user_Header];
    Raw_Pro_Master_TR_Data = [Raw_pro_Master_Header ; Raw_Product_Data];

    %consolidatedata -- Lots (Qty)
    ResetOptionTRData =Raw_Pro_Master_TR_Data;
    ResetOptionTRData_Header =  ResetOptionTRData(1,:);
    ResetOptionTRData = ResetOptionTRData(2:end,:);

    UniqueFields = {'AccountNo','Portfolio','Instrument', 'Account Number','ProductCode','Description','PremiumDivBy8','Price Conversion','Broker','Strike','C_P','Exp Date','B_S','Price','Trd ID'}; 
    SumFields = {'Qty'};
    OutputFields = ResetOptionTRData_Header ;
    WeightedAverageFields = [];
    [ResetOptionTRData_Header,ResetOptionTRData_Consolidate] = consolidatedata(ResetOptionTRData_Header, ResetOptionTRData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    %%% final user required data 
    Final_OptionTRData = [ResetOptionTRData_Header;ResetOptionTRData_Consolidate];
    
    %%% Counterparty Mapping
    if strcmpi(char(InBUName),'agf')
        [~,~,ConsoMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'usg')
        [~,~,ConsoMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'cfs')
        [~,~,ConsoMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    end   
    
   Nan_Row =  cellfun(@(V) any(isnan(V(:))), ConsoMapping(:,1)); %% Nan_Row = find(Nan_Row);
   ConsoMapping(Nan_Row(:),:) = [];
   ConsoMapping = ConsoMapping(2:end,:);
    
    Unique_NettedTR = unique(Final_OptionTRData(2:end,1))';
    Unique_ConsoNetted = unique(ConsoMapping(:,1))';
    Portfolio_MatchedIndex = cellStrfind_Exact(Unique_NettedTR,Unique_ConsoNetted);
    
    Unique_NettedTR(Portfolio_MatchedIndex) = [];
    
    if(~isempty(Unique_NettedTR))
        warndlg(['Conso Mapping Portfolio',Unique_NettedTR,' Which is not available in Master File(in Conso Mapping). Please update this account number in master file(in Conso Mapping Sheet) to get these account deals'],'MD Parser');
    end
    
    ConsoHeader = Final_OptionTRData(1,:);
    Conso_NettedTRData = Final_OptionTRData(2:end,:);
    
    NettedTRData_Portfolio = strtrim(Conso_NettedTRData(:,1));
    NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,10));   %% If Column 25 or 27
    ConsoNew_NettedTRData = [];
    Matched_Portfolio = [];
    Not_MatchedPortfolio = [];
    for k = 1 : length(ConsoMapping(:,1))
        ConsoMapping_Portfolio = strtrim(ConsoMapping{k,1});
        ConsoMapping_Counterparty = strtrim(ConsoMapping{k,2});
        
        MatchedIndex = find(strcmpi(ConsoMapping_Portfolio, NettedTRData_Portfolio) & ...
            strncmpi(ConsoMapping_Counterparty, NettedTRData_CounterParty,length(ConsoMapping_Counterparty)));
        
        if(~isempty(MatchedIndex))
            Temp_Conso = Conso_NettedTRData(MatchedIndex,:);
            Temp_Conso(:,10) =  {strtrim(ConsoMapping{k,3})};
            ConsoNew_NettedTRData = [ConsoNew_NettedTRData ; Temp_Conso];
            
            Temp_Conso = [];
            Matched_Portfolio =  [Matched_Portfolio ; {ConsoMapping_Portfolio}];
            
        else
            Not_MatchedPortfolio = [Not_MatchedPortfolio ; {ConsoMapping_Portfolio}];
        end 
    end
    
    Conso_OptionTRData = [ConsoHeader ; ConsoNew_NettedTRData] ;
    OptionTRData = cell2dataset(Conso_OptionTRData);
    
    %%%% vol_id_table
    %[~,~,VolIdMapping] = xlsread('vol_id_table.xlsx');
    SqlQuery = 'select * from vol_id_table';
    ObjDB = connect_to_database;
    [VolIdMapping_Header,VolIdMapping_Data] = Fetch_DB_Data(ObjDB,SqlQuery);

    VolIdMapping = [VolIdMapping_Header;VolIdMapping_Data];

    %%% Final User Requirement Process
    [row,~]=size(OptionTRData);
    for i = 1:1:row
        Option_Portfolio(i,1) = OptionTRData.Portfolio(i);
        Option_Counterparty(i,1) = OptionTRData.Broker(i); 

        instrument = OptionTRData.Instrument(i);
        Option_Instrument(i,1) = cellstr([char(instrument),'-O']);

        if(strcmpi(InBUName,'cfs'))
            if strncmpi(Option_Counterparty,'wells',5)
                if (cellStrfind(Option_Portfolio,'RMS-'))
                    %Counterparty(i,1) = {'WELLS FARGO RMS'};
                    ExeBroker(i,1) = {'WELLS FARGO RMS'};
                    Source(i,1) = {'XTR Fills'};
                    ExeType(i,1) ={'Elec'};
                    Rates(i,1) = {'Normal'};
                else
                    %Counterparty(i,1) = {'WELLS FARGO'};
                    ExeBroker(i,1) = {'WELLS FARGO'};
                    Source(i,1) = {'XTR Fills'};
                    ExeType(i,1) ={'Elec'};
                    Rates(i,1) = {'Normal'}; 
                    try
                        if isempty(char(ExeBroker))
                            ExeBroker(i,1) = {'WELLS FARGO QSVT'};
                        end
                    catch
                    end
                end
            end

        elseif (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg'))
            if strncmpi(Option_Counterparty,'wells',5)
                if (cellStrfind(Option_Portfolio,'ALPHA-'))
                    %Counterparty(i,1) = {'WELLS FARGO ALPHA'};
                    ExeBroker(i,1) = {'WELLS FARGO'};
                    Source(i,1) = {'XTR Fills'};
                    ExeType(i,1) ={'Elec'};
                    Rates(i,1) = {'Normal'};

                else
                    %Counterparty(i,1) = {'WELLS FARGO'};
                    ExeBroker(i,1) = {'WELLS FARGO'};
                    Source(i,1) = {'XTR Fills'};
                    ExeType(i,1) ={'Elec'};
                    Rates(i,1) = {'Normal'};
                    try
                        if isempty(char(ExeBroker))
                            ExeBroker(i,1) = {'WELLS FARGO QSVT'};
                        end
                    catch
                    end
                end
            end
        end

        Buy_sell = OptionTRData.B_S(i);
        if strcmpi(Buy_sell,'S')
            BuySell(i,1) = {'Sold'};
        elseif strcmpi(Buy_sell,'B')
            BuySell(i,1) = {'Bought'};
        else
            BuySell(i,1) = {''};
        end

        Option_type = OptionTRData.C_P(i);
        if strcmpi(Option_type,'C')
            ProductType(i,1) = {'CALL'};
            OptType(i,1) = {'Vanilla_Call'};
        elseif strcmpi(Option_type,'P')
            ProductType(i,1) = {'PUT'};
            OptType(i,1) = {'Vanilla_Put'};
        else
            OptType(i,1) = {''};  
        end

        Option_productcode=OptionTRData.ProductCode(i);
        Option_Contract=OptionTRData.Contract(i);
        Option_Expiry=OptionTRData.ExpDate(i);
        Option_TradePrice=OptionTRData.Price(i);
        Option_PriceConversion=OptionTRData.PriceConversion(i);

        Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
        MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};

       Contr_NewYM = char(Option_Contract);

        if length(Contr_NewYM) == 10
            CYrMon = char(cellstr(datestr(datetime(Contr_NewYM),'mmm-yy')));
        else
            Year= Contr_NewYM(:,1:5);
            CYrMon = char(strcat({Year(:,1:3)},{'-'},{Year(:,4:5)}));
        end

        ProdCode = char(strtrim(Option_productcode));
        Exp_Year_Month = datestr(datetime(Option_Expiry),'yyyy-mm-dd');

        Contr_YearMonthCode = cellstr([MonthCode{strncmpi(CYrMon,Month,3)},CYrMon(end)]);
        Year_Month(i,1) = Contr_YearMonthCode;
        
        if length(ProdCode) == 1
            ticker=strcat(ProdCode,{' '},Contr_YearMonthCode);
        else
            ticker=strcat(ProdCode,Contr_YearMonthCode);
        end

        %%%  Remove NaN Fields
        volid_Header = VolIdMapping(1,:);
        NaN_Col =  cellfun(@(V) any(isnan(V(:))), volid_Header);  %% Nan_Col = find(Nan_Col);
        VolIdMapping(:,NaN_Col(:)) = [];

        Row_find_Nan = VolIdMapping(:,4);
        NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
        VolIdMapping(NaN_Row(:),:) = [];

        VolId_Header = VolIdMapping(1,:);
        VolIdMapping = VolIdMapping(2:end,:);
        % To remove the lines of xl whcih contains NaN in all columns; the data
        % which gets removed from xl, but the row remains still
        IdxNaNRows = [];
        IdxNaN    = cellfun(@isnan,VolIdMapping(:,1),'UniformOutput',false);
        for iR = 1:length(IdxNaN)
            if any(IdxNaN{iR})
                IdxNaNRows = [IdxNaNRows; iR];
            end
        end
        VolIdMapping(IdxNaNRows,:) = [];

        bbg_opt_ticker = VolIdMapping(:,4);
        expiry_date = VolIdMapping(:,5);

        %%% Comparing BBG-OpT-Ticker and EXP-Date
        IdxProd = find(strcmpi(ticker,bbg_opt_ticker) & strcmpi(Exp_Year_Month,expiry_date));

        %%% Corresponding Vol-id
        vol_id = VolIdMapping(IdxProd,2);

        vol_id_splits = split(vol_id,' ');
        if length(vol_id_splits) == 4
            Option_Month(i,1)=cellstr(char(vol_id_splits(4,1)));
        elseif length(vol_id_splits) == 3
            Option_Month(i,1)=cellstr(char(vol_id_splits(3,1)));
        else
            Option_Month(i,1)={''};  
        end

        Opt_mont = char(Option_Month(i));
        if length(Opt_mont) == 7
            Opt_mont(2) = [];
            Opt_mont(5) = [];
            broker_opt_month(i,1) = cellstr(Opt_mont);
        else
           broker_opt_month(i,1) = cellstr(Opt_mont);
        end
            
        %%%% Primium Calculations  %%%% Start
        PremiumDivBy8 = OptionTRData.PremiumDivBy8(i);
        if PremiumDivBy8 == 0
            NewTradePrice = Option_TradePrice .* Option_PriceConversion;
        else 
            %%%% Primium Calculations  %%%% Start
            Temp   = num2str(Option_TradePrice);
            TempNo = str2num(Temp(1:end-1)); %#ok<*ST2NM>
            if isempty(TempNo)
                TempNo=0;
            end        
            NewTradePrice = TempNo + (str2num(Temp(end))/8);
            NewTradePrice = NewTradePrice * Option_PriceConversion;
        end

        Premium_div = OptionTRData.Prem_div(i);
        TempPremium = NewTradePrice;
        if ~isnan(TempPremium) && isnumeric(TempPremium)
            TempPremium = TempPremium ./ Premium_div;
        end

        Strike = OptionTRData.Strike(i);
        StrikeConv = OptionTRData.Strike_div(i);

       if PremiumDivBy8 == 0
            Temp   = Strike;
            Strike = Temp * Option_PriceConversion;
       else
           Strike = Strike;
       end
          
        TempStrike = Strike;
        if ~isnan(TempStrike) && isnumeric(TempStrike)
            TempStrike = TempStrike ./ StrikeConv;
        end

        if isempty(TempStrike)
            Option_Strike(i,1) = num2cell(NaN);
        else
            Option_Strike(i,1) = num2cell(TempStrike);
        end

        if isempty(TempPremium) || isnan(TempPremium)
            Option_Premium(i,1) = num2cell(0);
        else
            Option_Premium(i,1) = num2cell(TempPremium);
        end
    end

    %%Transaction Number
    TempTradeId = '';
    try
    [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
    if strcmpi(OutErrorMsg,'No Errors')
        ObjDB = connect_to_database;
        SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
        TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
        TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));

        if(isempty(TempTradeId))
            TempTradeId = 0;      %% Default
        end
    end
    catch
        TempTradeId = '';
    end

    NumRows1 = size(OptionTRData,1); % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
    NTradeId = (TempTradeId+1:1:TempTradeId+NumRows1)';
    TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
    TradeId = strrep(TradeId,' ','');  %% Remove Spaces

    TRN_Number	= TradeId;
    Parent_Transaction_Number	= TradeId;

    Idxsold = strcmpi(BuySell,'Sold');
    IdxBrought = strcmpi(BuySell,'Bought');
    Counterparty = OptionTRData.Broker;
    Initial_Lots	= OptionTRData.Qty;

    Initial_Lots(Idxsold) = Initial_Lots(Idxsold) .* -1;
    Initial_Lots(IdxBrought) = Initial_Lots(IdxBrought) .* 1;

    ExchDate = OptionTRData.ExchDate;
    if isdeployed
        Transaction_Date = cellstr(datetime(ExchDate));
    else
        Transaction_Date = ExchDate;
    end
    Trader_Name	= OptionTRData.TrdID;            %Trd ID

    OrderDate = OptionTRData.OrderDate;
    OrderTime = OptionTRData.OrderTime;

    Traded_DateTimestamp = strcat(OrderDate,{' '},OrderTime);  %Order Date & Order Time

    CONTRACTNB	= cell(size(Option_Instrument,1),1);
    Maturity	= cell(size(Option_Instrument,1),1);
    Barrier_Type	= cell(size(Option_Instrument,1),1);
    Barrier	= cell(size(Option_Instrument,1),1);
    Averaging_Start_Date	= cell(size(Option_Instrument,1),1);
    Averaging_End_Date	= cell(size(Option_Instrument,1),1);
    Spcl_Rate_lot	= cell(size(Option_Instrument,1),1);
    Other_Remarks	= cell(size(Option_Instrument,1),1);
    OTC_DAILY_EOD_LEG	= cell(size(Option_Instrument,1),1);

    if strcmpi(InBUName,'cfs')
        CONTRACTNB = Counterparty;
        OTC_DAILY_EOD_LEG = repmat({'EOD'},size(Option_Instrument,1),1);
    end
    
%%%   Final user TR_Dump Header and Data
%     Option_TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Initial Lots',...
%                     'CONTRACTNB','Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source',...
%                     'Averaging Start Date','Averaging End Date','Parent Transaction Number','Exe-Broker','Exe Type','Rates',...
%                     'Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'}; 


    Option_TradersTRData  = [Option_Instrument, Option_Portfolio, BuySell, Option_Month, Option_Strike, OptType, Option_Premium, Counterparty, ...
                            num2cell(Initial_Lots), CONTRACTNB, Maturity, Barrier_Type, Barrier, Transaction_Date, TRN_Number, Source, Averaging_Start_Date, ...
                            Averaging_End_Date, Parent_Transaction_Number, ExeBroker, ExeType, Rates, Spcl_Rate_lot, Other_Remarks, OTC_DAILY_EOD_LEG, ...
                            Trader_Name, Traded_DateTimestamp];    

    Option_NettedTRData =  Option_TradersTRData;
    
    
    Option_BrokerTRData  = [num2cell(OptionTRData.AccountNo), Transaction_Date, OptionTRData.B_S, num2cell(Initial_Lots), Year_Month, OptionTRData.Description, ...
            Option_Strike, ProductType, num2cell(OptionTRData.Price), broker_opt_month, Trader_Name, Traded_DateTimestamp, OptionTRData.AccountNumber, Counterparty];  

catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end

