function [AGF_Conso_Netted] = mapping_4_changing_OptType(AGF_Conso_Netted)

Master_Path = [pwd filesep 'Master Files' filesep];

%% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType

Instrument_Code = AGF_Conso_Netted(:,1);   %% Get Instrument Column

% MatchedIndex = find(strncmpi('CME', Instrument_Code,3));  %% Don't Need for Loop

[~,~,RawData] = xlsread([Master_Path filesep 'Mapping_fxfuture.xlsx']);
% Header = RawData(1,:);
Data = RawData(2:end,:);

for k = 1 : length(Data)
    Current_Instrument = strtrim(Data{k,1});
    Current_OptType = strtrim(Data{k,2});
    MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
    
    if(~isempty(MatchedIndex))
        AGF_Conso_Netted(MatchedIndex,6) = cellstr(Current_OptType); %% If 25 Column
        %  AGF_Conso_Netted(MatchedIndex,7) = cellstr(Current_OptType); %% If 32 Column
    end
    
end

end