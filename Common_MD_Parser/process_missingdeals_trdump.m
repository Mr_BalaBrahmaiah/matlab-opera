function process_missingdeals_trdump

[~,BrokerData,XTRFillsData,TraderSnapsData,StatementDate,fid] = generate_missing_deals;

if isempty(BrokerData) &&  isempty(XTRFillsData) && isempty(TraderSnapsData)
    errordlg('No files selected for broker statement, traders snaps and XTR fills!');
    return;
end

ObjDB = connect_to_database;
DBData = fetch(ObjDB,'select subportfolio,portfolio from subportfolio_portfolio_table');
RefDBSubportfolio = DBData(:,1);
RefDBPortfolio = DBData(:,2);

% read the TR master file
% this is applicable only for Broker statement and Traders snaps
[~,~,ProductMaster] = xlsread('MissingDeals_Master.xlsx','TR Format Master');
ProductMaster = ProductMaster(2:end,:);
% To remove the lines of xl whcih contains NaN in all columns; the data
% which gets removed from xl, but the row remains still
IdxNaNRows = [];
IdxNaN         = cellfun(@isnan,ProductMaster(:,4),'UniformOutput',false);
for iRNaN = 1:length(IdxNaN)
    if any(IdxNaN{iRNaN})
        IdxNaNRows = [IdxNaNRows; iRNaN];
    end
end
ProductMaster(IdxNaNRows,:) = [];

RefPortfolio  = ProductMaster(:,1);
RefInstrument = ProductMaster(:,2);
RefDesc    = ProductMaster(:,3);
RefAccNo   = ProductMaster(:,4);
RefProdCode = ProductMaster(:,5);

StrikeConv = cell2mat( ProductMaster(:,6));
PremiumConv = cell2mat( ProductMaster(:,7));
RefDirPortfolio = ProductMaster(:,8);

% create the TR format dump file
OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice','Maturity Month'};
TSOutputHeader = [OutputHeader,'Source','Clearer'];
TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
    'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
    'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
    'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
    'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot'};
PosAccNo = 1;
PosTradeDate = 2;
PosSide = 3;
PosQuantity = 4;
PosYearMonth = 5;
PosDesc = 6;
PosStrike = 7;
PosProdType = 8;
PosTradePrice = 9;
PosMaturity = 10;

% used only in traders snaps
PosExeBroker = 11;
PosClearer = 12;
PosExeType = 13;
PosRates = 14;
PosSpclRate = 15;

% construct the TR format for broker statement data
if ~isempty(BrokerData)
    BrokerTRData      = construct_TR_format(BrokerData,'Broker');
else
    BrokerTRData = [];
end

% construct the TR format for traders snaps data
if ~isempty(TraderSnapsData)
    IdxSnapsData = strncmpi('wells',TraderSnapsData(:,PosClearer),length('wells')) & ...
        strcmpi('XTrader',TraderSnapsData(:,PosExeBroker)) & strcmpi('Future',TraderSnapsData(:,PosProdType));
    TraderSnapsData(IdxSnapsData,:) = [];
    TraderSnapsTRData = construct_TR_format(TraderSnapsData,'TS');
    UpdTradersSnapsData = TraderSnapsData;
    UpdTradersSnapsData(:,11:end) = [];
    TRSSource = repmat(cellstr('Traders Snaps'),size(UpdTradersSnapsData,1),1);
    TRSClearer = TraderSnapsData(:,PosClearer);
    UpdTradersSnapsData = [UpdTradersSnapsData TRSSource TRSClearer];
else
    UpdTradersSnapsData = TraderSnapsData;
    TraderSnapsTRData = [];
end

% read the TR master file
% this is applicable only for XTR fills data
[~,~,ProductMaster] = xlsread('MissingDeals_Master.xlsx','XTR-TR Format Master');

%%  Remove NaN Fields
Header = ProductMaster(1,:);
NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
ProductMaster(:,NaN_Col(:)) = [];

Row_find_Nan = ProductMaster(:,4);
NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
ProductMaster(NaN_Row(:),:) = [];

%%
ProductMaster = ProductMaster(2:end,:);
% To remove the lines of xl whcih contains NaN in all columns; the data
% which gets removed from xl, but the row remains still
IdxNaNRows = [];
IdxNaN         = cellfun(@isnan,ProductMaster(:,4),'UniformOutput',false);
for iRNaN = 1:length(IdxNaN)
    if any(IdxNaN{iRNaN})
        IdxNaNRows = [IdxNaNRows; iRNaN];
    end
end
ProductMaster(IdxNaNRows,:) = [];

RefPortfolio  = ProductMaster(:,1);
RefInstrument = ProductMaster(:,2);
RefDesc    = ProductMaster(:,3);
RefAccNo   = ProductMaster(:,4);
RefProdCode = ProductMaster(:,5);

StrikeConv = cell2mat( ProductMaster(:,6));
PremiumConv = cell2mat( ProductMaster(:,7));

% construct the TR format for XTRFills data
if ~isempty(XTRFillsData)
    XTRFillsData(:,1) = XTRFillsData(:,11); % to assign the XTR account no to Account number field
    XTRFillsData = XTRFillsData(:,1:10);
    XTRFillsTRData    = construct_TR_format(XTRFillsData,'XTR');
    XTRSource = repmat(cellstr('XTR Fills'),size(XTRFillsData,1),1);
    XTRClearer = repmat(cellstr('WELLS FARGO'),size(XTRFillsData,1),1);
    XTRFillsData = [XTRFillsData XTRSource XTRClearer];
else
    XTRFillsTRData = [];
end

fclose(fid);

TradersData   = [XTRFillsData;UpdTradersSnapsData];
TradersTRData = [XTRFillsTRData;TraderSnapsTRData];

OutXLSFileName = ['TR_Dump_',char(StatementDate),'.xls'];

if(~isempty(BrokerData))
    % write broker data
    xlswrite(OutXLSFileName,[OutputHeader;BrokerData],'Broker Statement');
    % write broker tr format data
    xlswrite(OutXLSFileName,[TRHeader;BrokerTRData],'Broker - TR format');
end
% write traders data
xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills + TR Snaps');
% write traders tr format data
xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR+TR Snaps - TR format');

try
    if ~isempty(TradersTRData)
        PosSource = strcmpi('Source',TRHeader);
        SourceData = TradersTRData(:,PosSource);
        IdxXTR = strcmpi('XTR Fills',SourceData);
        XTRNetData = TradersTRData(IdxXTR,:);
        if ~isempty(XTRNetData) % to check if XTR fills file is selected or not
            UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date'};
            SumFields = {'Active Lots','Initial Lots'};
            NettedXTRData = calc_netted_data(TRHeader, XTRNetData,TRHeader,UniqueFields,SumFields,[]);
            NettedTRData = [NettedXTRData;TradersTRData(~IdxXTR,:)];
            
            % NaN_Row =  cellfun(@(V) any(isnan(V(:))), NettedTRData(:,2)); %% My Changing
            % NettedTRData(NaN_Row(:),:) = [];
            
            % write netted traders tr format data
            xlswrite(OutXLSFileName,NettedTRData,'Netted TR Data-XTR+TR Snaps');
        end
        %% Conso Mapping
        [~,~,ConsoMapping] = xlsread('MissingDeals_Master.xlsx','Conso Mapping');
        ConsoMapping = ConsoMapping(2:end,:);
        ConsoHeader = NettedTRData(1,:);
        NettedTRData = NettedTRData(2:end,:);
        
        NettedTRData_Portfolio = strtrim(NettedTRData(:,2));
        NettedTRData_CounterParty = strtrim(NettedTRData(:,9));
        
        for k = 1 : length(ConsoMapping(:,1))
            ConsoMapping_Portfolio = strtrim(ConsoMapping{k,1});
            ConsoMapping_Counterparty = strtrim(ConsoMapping{k,2});
            
            MatchedIndex = find(strcmpi(ConsoMapping_Portfolio, NettedTRData_Portfolio) & ...
                strcmpi(ConsoMapping_Counterparty, NettedTRData_CounterParty));
            
            if(~isempty(MatchedIndex))
                NettedTRData(MatchedIndex,2) = {strtrim(ConsoMapping{k,3})};
                NettedTRData(MatchedIndex,9) = {strtrim(ConsoMapping{k,4})};
            end
            
        end
        
        xlswrite(OutXLSFileName,[ConsoHeader;NettedTRData],'Conso Netted');
        
    end
catch ME
    disp(ME.message);
end
OutXLSFileName = fullfile(pwd,OutXLSFileName);
xls_delete_sheets(OutXLSFileName);
msgbox(['TR dump is saved in ',char(OutXLSFileName)],'Missing Deals Parser');


    function OutData = construct_TR_format(InData,Name)
        % construct the broker data in TR format
        NumRows = size(InData,1);
        OutData = [];
        for iR = 1:NumRows
            AccNum = InData{iR,PosAccNo};
            Description = InData{iR,PosDesc};
            if strcmpi(Name,'XTR')
                IdxAccNo = strcmpi(AccNum,RefAccNo) & strcmpi(Description,RefDesc);
            else
                IdxAccNo = ismember(cell2mat(RefAccNo),AccNum) & strcmpi(Description,RefDesc);
            end
            Instrument = unique(RefInstrument(IdxAccNo));
            Portfolio = unique(RefPortfolio(IdxAccNo));
            ProductCode = char(unique(RefProdCode(IdxAccNo)));
            BuySell = InData(iR,PosSide);
            if strcmpi(BuySell,'B')
                BuySell = {'Bought'};
            elseif strcmpi(BuySell,'S')
                BuySell = {'Sold'};
            end
            Lots = InData(iR,PosQuantity);
            Month = InData(iR,PosMaturity);
            Maturity = num2cell(NaN);
            Strike = InData(iR,PosStrike);
            OptType = lower(InData{iR,PosProdType});
            Premium = InData(iR,PosTradePrice);
            TradeDate = InData(iR,PosTradeDate);
            ExeBroker = num2cell(NaN);
            ExeType = num2cell(NaN);
            Rates = num2cell(NaN);
            SpclRate = num2cell(NaN);
            if strcmpi(Name,'Broker')
                Source = {''};
                Counterparty = {'WELLS FARGO'};
            elseif strcmpi(Name,'XTR') % for traders
                Source = {'XTR Fills'};
                Counterparty = {'WELLS FARGO'};
                ExeBroker = {'WELLS FARGO'};
                ExeType ={'Elec'};
                Rates = {'Normal'};
            else
                Source = {'Traders Snaps'};
                if isnan(InData{iR,PosClearer})
                    ErrorStr = ['Clearer is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    Counterparty = {''};
                else
                    Counterparty = upper(InData(iR,PosClearer));
                    if strcmpi(Counterparty,'Dir Book')
                        Counterparty = unique(RefDirPortfolio(IdxAccNo));
                    end
                end
                if isnan(InData{iR,PosExeBroker})
                    ErrorStr = ['ExeBroker is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    ExeBroker = {''};
                else
                    ExeBroker = upper(InData(iR,PosExeBroker));
                    if strcmpi(ExeBroker,'Dir Book')
                        ExeBroker = unique(RefDirPortfolio(IdxAccNo));
                    end
                end
                if strcmpi(ExeBroker,'XTrader')
                    ExeBroker = {'WELLS FARGO'};
                end
                
                if isnan(InData{iR,PosExeType})
                    ErrorStr = ['ExeType is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    ExeType = {''};
                else
                    ExeType = InData(iR,PosExeType);
                end
                
                if isnan(InData{iR,PosRates})
                    ErrorStr = ['Rates is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    Rates = {''};
                else
                    Rates = InData(iR,PosRates);
                end
                
                SpclRate = InData(iR,PosSpclRate);
            end
            
            if strcmpi(OptType,'future')
                OptType = {'Future'};
                Strike = num2cell(NaN);
            else
                if strcmpi(OptType ,'call')
                    OptType = {'Vanilla_Call'};
                elseif strcmpi(OptType ,'put')
                    OptType = {'Vanilla_Put'};
                end
                if ~isempty(Instrument)
                    Instrument = cellstr([char(Instrument),'-O']);
                end
            end
            Temp = num2cell(NaN);
            
            if isempty(Month)
                Month = num2cell(NaN);
            end
            if isempty(Maturity)
                Maturity = num2cell(NaN);
            end
            
            if isempty(Instrument) || isempty(Portfolio)
                InstrErrorStr = ['Portfolio/Instrument information is not available for account ''',num2str(AccNum),''' in Master XLS file. Portfolio/Instrument will be empty for the corresponding trades in TR dump!'];
                fprintf(fid,'%s\n',InstrErrorStr);
                if isempty(Instrument)
                    Instrument = num2cell(NaN);
                end
                if isempty(Portfolio)
                    Portfolio = num2cell(NaN);
                end
            end
            try
                TempStrike = cell2mat(Strike);
                TempPremium = cell2mat(Premium);
                if ~isnan(TempStrike) && isnumeric(TempStrike)
                    TempStrike = TempStrike ./ StrikeConv(IdxAccNo);
                end
                if ~isnan(TempPremium) && isnumeric(TempPremium)
                    TempPremium = TempPremium ./ PremiumConv(IdxAccNo);
                end
                if isempty(TempStrike)
                    Strike = num2cell(NaN);
                else
                    Strike = num2cell(TempStrike);
                end
                if isempty(TempPremium) || isnan(TempPremium)
                    Premium = num2cell(0);
                else
                    Premium = num2cell(TempPremium);
                end
            catch
            end
            % TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
            %             'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
            %         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
            %         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
            %         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot'}
            
            IdxDBPortfolio = strcmpi(Portfolio,RefDBSubportfolio);
            if strncmpi(Counterparty,'wells',5)
                if (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'DIR'))
                    Counterparty = {'WELLS FARGO ARB'};
                    ExeBroker = {'WELLS FARGO ARB'};
                elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'TEC'))
                    Counterparty = {'WELLS FARGO TEC'};
                    ExeBroker = {'WELLS FARGO TEC'};
                elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'QSVT'))
                    Counterparty = {'WELLS FARGO QSVT'};
                    ExeBroker = {'WELLS FARGO QSVT'};
                end
            end
            RowData  = [Instrument,Portfolio,BuySell,Lots,Month,Strike,...
                OptType,Premium,Counterparty,Temp,Lots,...
                Temp,Premium,Temp,Maturity,Temp,Temp,TradeDate,Temp,Temp,Source,Temp,...
                Temp,Temp,Temp,Temp,ExeBroker,ExeType,Rates,SpclRate];
            
            OutData = [OutData ; RowData]; %#ok<*AGROW>
        end
    end

end