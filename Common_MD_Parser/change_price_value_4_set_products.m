function [RawData_Header , RowData ,Output_Header,Output_RowData,Not_GettingPrice] = change_price_value_4_set_products(RawData,Output)

Master_Path = [pwd filesep 'Master Files' filesep];

Not_GettingPrice = 0 ;


ObjDB = connect_to_database;

try
    RawData_Header = RawData(1,:);
    RowData =  RawData(2:end,:);
    
    %% Get SET Products from Input RawData
    Product_Header_Col = find(cellfun(@(V) strcmpi('Product',V), RawData_Header));
    Price_Header_Col = find(cellfun(@(V) strcmpi('Price',V), RawData_Header));
    SettleDate_Header_Col = find(cellfun(@(V) strcmpi('Exch Date',V), RawData_Header));
    
    SettleDate_Input = unique(RowData(:,SettleDate_Header_Col));
    
    [System_Date_Format,System_Date] = get_System_Date_Format();
    SettleDate = datestr(datenum(SettleDate_Input,System_Date_Format) , 'yyyy-mm-dd');
    
    
    Product_Data = RowData(:,Product_Header_Col); %% Full Column from Input
    
    Matched_Index = find(cellfun('length',Product_Data)>4);  %% For Find Set Product
    
    Find_Set_Index = cellStrfind_Exact( cellfun(@(x) x(end-2:end), Product_Data(Matched_Index), 'UniformOutput', false) , {'set'}) ;  %% Get 'Set' index of  Last 3 Charcter Product
    
    Product_Set_Index = Matched_Index(Find_Set_Index);
    
    Product_Set_Data = RowData(Product_Set_Index,:);
    
    if(~isempty(Product_Set_Data))
        
        %% Get ORYX Master File
        [~,~,XTR_Fills_Sheet] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
        Header = XTR_Fills_Sheet(1,:);
        Row_find_Nan = XTR_Fills_Sheet(:,1);
        Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
        XTR_Fills_Sheet(:,Nan_Col(:)) = [];
        
        Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
        XTR_Fills_Sheet(Nan_Row(:),:) = [];
        
        [~,~,XTR_TR_Sheet] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
        Header = XTR_TR_Sheet(1,:);
        Row_find_Nan = XTR_TR_Sheet(:,1);
        Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
        XTR_TR_Sheet(:,Nan_Col(:)) = [];
        
        Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
        XTR_TR_Sheet(Nan_Row(:),:) = [];
        
        %%%
        XTR_Fills_Sheet_Header =  XTR_Fills_Sheet(1,:);
        XTR_Fills_Sheet_Data = XTR_Fills_Sheet(2:end,:);
        
        XTR_TR_Sheet_Header =  XTR_TR_Sheet(1,:);
        XTR_TR_Sheet_Data = XTR_TR_Sheet(2:end,:);
        
        %% ORYX Master XTR Fills Sheet and XTR-TR-Master Sheet Mapping
        
        ProductCode_TickSize = cell(size(XTR_TR_Sheet_Data,1),3);
        ProductCode_TickSize(:,:) = {' '};
        
        for i = 1 : length(XTR_TR_Sheet_Data(:,1))
            Map_Account_Num = strtrim( num2str(XTR_TR_Sheet_Data{i,4}) );
            Map_Description = strtrim(XTR_TR_Sheet_Data{i,3});
            Invenio_Product_Code = char(strtrim(XTR_TR_Sheet_Data(i,8)));
            
            XTR_Fills_Account_Num = cellfun(@(s) (num2str(s)), XTR_Fills_Sheet_Data(:,1),'UniformOutput',false);
            XTR_Fills_Description = XTR_Fills_Sheet_Data(:,3);
            
            MatchedIndex = find(strcmpi(Map_Account_Num, XTR_Fills_Account_Num) & strcmpi(Map_Description, XTR_Fills_Description));
            
            if(~isempty(MatchedIndex))
                
                ProductCode_TickSize(MatchedIndex,1) = strtrim(XTR_Fills_Sheet_Data(MatchedIndex,2)); %% Get ProductCode
                ProductCode_TickSize(MatchedIndex,2) = cellstr(Invenio_Product_Code);
                
                SqlQuery = ['select tick_size from product_master_table where invenio_product_code = ''',Invenio_Product_Code,''''];
                %                 ObjDB = connect_to_database;
                Tick_size = fetch(ObjDB,SqlQuery);
                
                ProductCode_TickSize(MatchedIndex,3) = Tick_size;  %% Get Tick Size from DB
                
            end
            
        end
        
        ProductCode_TickSize_Header = ['ProductCode','Invenio ProductCode','Tick Size'];
        
        
        %% Get Maturity Month for Set Product Only
        
        PosYearMonth   = strcmpi('Contract',RawData_Header);
        Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
        MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
        
        if isdeployed
            YearMonth = cellstr(datestr(Product_Set_Data(:,PosYearMonth),'dd-mm-yyyy'));
        else
            YearMonth = Product_Set_Data(:,PosYearMonth);
        end
        
        NewYM = char(YearMonth);
        TempYearMonth = cellstr(datestr(cellstr(NewYM(:,1:end-5)),'mmm-yy'));
        
        %%
        %         ObjDB = connect_to_database;
        
        ProductCode_TickSize_SettleValue = cell(size(Product_Set_Data,1),1);
        
        for i = 1 : size(Product_Set_Data,1)
            
            Current_ProductCode = Product_Set_Data(i,Product_Header_Col) ;
            Matched_ProdCode = cellStrfind_Exact(ProductCode_TickSize(:,1),Current_ProductCode);
            
            if(~isempty(Matched_ProdCode))
                Current_ProductData = ProductCode_TickSize(Matched_ProdCode,1);
                Current_Invenio_ProductCode = ProductCode_TickSize(Matched_ProdCode,2);
                Current_Product_TickSize = ProductCode_TickSize(Matched_ProdCode,3);
                
                
                YrMon = TempYearMonth{i};
                YearMonthCode = cellstr([MonthCode{strncmpi(YrMon,Month,3)},YrMon(end)]);
                
                if(length(char(Current_Invenio_ProductCode)) ==4)
                    Underlying_ID = [char(unique(Current_Invenio_ProductCode)),'  ',char(YearMonthCode)];
                else
                    Underlying_ID = [char(unique(Current_Invenio_ProductCode)),' ',char(YearMonthCode)];
                end
                
                SqlQuery = ['select settle_value from underlying_settle_value_table where underlying_id = ',['''' Underlying_ID ''''],' and settlement_date = ',['''' char(SettleDate) '''']];
                
                Settle_Value = fetch(ObjDB,SqlQuery);
                
                if(isempty(Settle_Value))
                    Not_GettingPrice = 1;
                end
                
                Input_Price =  Product_Set_Data(i,Price_Header_Col);
                
                
                %   Product_Set_Data(i,Price_Header_Col) = {(cell2mat(Input_Price)*cell2mat(Current_Product_TickSize)) + cell2mat(Settle_Value)};
                Product_Set_Data(i,Price_Header_Col) = {nansum( (cell2mat(Input_Price)) + cell2mat(Settle_Value) )};
                
            end
            
        end
        
        %% Change the Price Value of Input
        
        RowData(Product_Set_Index,:) = Product_Set_Data ;
        
        Output_Header = Output(1,:);
        Output_RowData = Output(2:end,:);
        TradePrice_Output_Col = find(cellfun(@(V) strcmpi('TradePrice',V), Output_Header));
        
        Output_RowData(Product_Set_Index,TradePrice_Output_Col) = RowData(Product_Set_Index,Price_Header_Col);
        
        Output = [Output_Header;Output_RowData];
        
    else
        
        Output_Header = Output(1,:);
        Output_RowData = Output(2:end,:);
        
    end
    
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    return;
end


