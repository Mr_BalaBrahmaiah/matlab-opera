function [OutData,StatementDate,PathName] = parse_broker_statement(varargin)

Master_Path = [pwd filesep 'Master Files' filesep];

try
    % try
    OutData = []; StatementDate = '';PathName = '';
    
    if nargin == 2
        BrokerStatement = varargin{1};
        Type_Name = varargin{2};
    end
    %     else
    %         [FileName,PathName]  = uigetfile('*.txt','Select the Broker Text file statement');
    %
    %         if isnumeric(FileName) && FileName==0
    %             errordlg('Broker Text file statement not selected!','Missing Deals Parser');
    %             PathName = '';
    %             return;
    %         else
    %             BrokerStatement = fullfile(PathName,FileName);
    %         end
    %     end
    
    if ~isempty(BrokerStatement)
        RawData = textread(BrokerStatement,'%s','delimiter','\n','whitespace',''); %#ok<*REMFF1>
    else
        return;
    end
    
    hWaitbar = waitbar(0,'Please wait, extracting data from Broker statement ...');
    
    % CHANGE FOR NEW FORMAT
    %   Start_Of_Data = cellStrfind(RawData,'T  R  A  D  E   A  C  T  I  V  I  T  Y');
    Start_Of_Data = cellStrfind(RawData,'C  O  N  F  I  R  M  A  T  I  O  N');
    End_Of_Data = cellStrfind(RawData,{'O   P   E   N      P  O  S  I  T  I  O  N  S','P  U  R  C  H  A  S  E      &      S  A  L  E',...
        'MEMO OPTIONS OFFSETTING INFORMATION','OPTIONS HAVE EXPIRED'});
    
    NewData10 = []; NewData11 = []; NewData12 = [];
    NewData13 = []; NewData14 = []; NewData15 = [];
    NewData9 = [];
    
    Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
    MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
    
    AccNum = cellStrfind(RawData,'ACCOUNT NUMBER:');
    StmtDate = cellStrfind(RawData,'STATEMENT DATE:');
    StatementDate = strtrim(strrep(strtrim(RawData(StmtDate(1))),'STATEMENT DATE:',''));
    StatementDate = strtrim(strrep(StatementDate,'550 S. TRYON STREET','')); % CHANGE FOR NEW FORMAT
    StatementDate = datestr(StatementDate,'dd-mm-yyyy');
    
    for iS = 1:length(Start_Of_Data)
        waitbar(iS/length(Start_Of_Data),hWaitbar);
        SD = Start_Of_Data(iS) -10 : Start_Of_Data(iS);
        LineAccNum = SD(ismember(SD,AccNum));
        % CHANGE FOR NEW FORMAT
        AccountNumber = strrep(strtrim(RawData(LineAccNum)),'ACCOUNT NUMBER: L ','');
        %       AccountNumber = strrep(AccountNumber,' BNE01','');
        AccountNumber = strtrim(strrep(AccountNumber,'WELLS FARGO SECURITIES, LLC.                                         ACCOUNT NUMBER: B US01',''));
        
        if isempty(AccountNumber)
            SD = Start_Of_Data(iS) -20 : Start_Of_Data(iS);
            LineAccNum = SD(ismember(SD,AccNum));
            % CHANGE FOR NEW FORMAT
            AccountNumber = strrep(strtrim(RawData(LineAccNum)),'ACCOUNT NUMBER: L ','');
            %       AccountNumber = strrep(AccountNumber,' BNE01','');
            AccountNumber = strtrim(strrep(AccountNumber,'WELLS FARGO SECURITIES, LLC.                                         ACCOUNT NUMBER: B US01',''));
        end
        
        NewEndOfData = End_Of_Data(End_Of_Data > Start_Of_Data(iS));
        [~,Idx] = min(NewEndOfData - Start_Of_Data(iS));
        
        Data = RawData(Start_Of_Data(iS):NewEndOfData(Idx));
        
        NumLines = length(Data);
        for i = 5:NumLines-2
            TempData = strsplit(strtrim(Data{i}),' ');
            if isempty(strfind(TempData{1},'/'))
                continue
            end
            
            Pos1 = 5; Pos2 = 7;
            if ~isempty(strfind(TempData{2},'/'))
                TempData(2) = [];
                Pos1 = 7; Pos2 = 9;
            end
            if length(TempData)>=9 && length(TempData)<=16
                A = regexp(Data{i}, '\s+|\S+', 'match');
                if length(A{Pos1}) > length(A{Pos2})
                    BuyOrSell = 'S';
                else
                    BuyOrSell = 'B';
                end
            end
            
            IsValidRow = 1;
            switch(length(TempData))
                case 9
                    NewData9 = [NewData9;TempData];
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    YearMonth = [TempData{4},TempData{5}];
                    Desc      = cellstr([TempData{6},' ',TempData{7}]);
                    Strike    = num2cell(NaN);
                    ProductType = cellstr('FUTURE');
                    TradePrice = num2cell(str2num(TempData{8}));
                case 10
                    NewData10 = [NewData10;TempData];
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    YearMonth = [TempData{4},TempData{5}];
                    Desc      = cellstr([TempData{6},' ',TempData{7}]);
                    Strike    = num2cell(NaN);
                    ProductType = cellstr('FUTURE');
                    if  strcmpi(TempData{6},'cattle')
                        TradePrice = num2cell(str2num(TempData{8}) + (0.01 * str2num(TempData{9})));
                    else
                        TradePrice = num2cell(str2num(TempData{9}));
                    end
                case 11
                    NewData11 = [NewData11;TempData];
                    %                     if ~isempty(strfind(TempData{2},'/'))
                    %                         TempData(2) = [];
                    %                     end
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    YearMonth = [TempData{4},TempData{5}];
                    % check if the 8th position is also desc, like in BEAN OIL or
                    % BEAN MEAL
                    if isempty(str2num(TempData{8})) ... % only for strings
                            || strcmpi(TempData{7},'robusta') ...
                            || strcmpi(TempData{7},'cotton')
                        Desc      = cellstr([TempData{6},' ',TempData{7},' ',TempData{8}]);
                    else
                        Desc      = cellstr([TempData{6},' ',TempData{7}]);
                    end
                    Strike    = num2cell(NaN);
                    ProductType = cellstr('FUTURE');
                    % check if the fraction value is present for Traded Price ,
                    % like in Corn or Wheat
                    if isempty(strfind(TempData{9},'.')) % tradedprice not in position 9
                        TradePrice = num2cell(str2num(TempData{10}));
                    else
                        if isempty(str2num(TempData{10}))
                            TradePrice = num2cell(str2num(TempData{9}));
                        else
                            TradePrice = num2cell(str2num(TempData{9}) + (0.01 * str2num(TempData{10})));     %#ok<*ST2NM>
                        end
                    end
                case {12,13}
                    if length(TempData) == 12
                        NewData12 = [NewData12;TempData];
                    else
                        NewData13 = [NewData13;TempData];
                    end
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    if strcmpi(TempData{4},'call') || strcmpi(TempData{4},'put') %options
                        ProductType = cellstr(TempData{4});
                        YearMonth = [TempData{5},TempData{6}];
                        if strcmpi(TempData{8},'rbusta') || strcmpi(TempData{8},'robusta')... % to get LIF ROBUSTA 10
                                || strcmpi(TempData{8},'lean')
                            Desc      = cellstr([TempData{7},' ',TempData{8},' ',TempData{9}]);
                            Strike    = num2cell(str2num(TempData{10}));
                            TradePrice = num2cell(str2num(TempData{12}));
                        elseif strcmpi(TempData{7},'cattle')
                            Desc      = cellstr(TempData{7});
                            Strike    = num2cell(str2num(TempData{8}));
                            if ~isempty(str2num(TempData{11}))
                                TradePrice = num2cell(str2num(TempData{10}) + (0.01 * str2num(TempData{11})));
                            else
                                TradePrice = num2cell(str2num(TempData{10}));
                            end
                        else
                            Desc      = cellstr([TempData{7},' ',TempData{8}]);
                            Strike    = num2cell(str2num(TempData{9}));
                            TradePrice = num2cell(str2num(TempData{11}));
                            if isempty(Strike) && ischar(TempData{9})
                                Desc      = cellstr([TempData{7},' ',TempData{8},' ',TempData{9}]);
                                Strike    = num2cell(str2num(TempData{10}));
                                TradePrice = num2cell(str2num(TempData{12}));
                            end
                        end
                    else % futures
                        ProductType = cellstr('FUTURE');
                        YearMonth = [TempData{4},TempData{5}];
                        Desc      = cellstr([TempData{6},' ',TempData{7},' ',TempData{8}]);
                        Strike    = num2cell(NaN);
                        TradePrice = num2cell(str2num(TempData{10}) + (0.01 * str2num(TempData{11})));     %#ok<*ST2NM>
                    end
                case 14
                    NewData14 = [NewData14;TempData];
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    ProductType = cellstr(TempData{4});
                    YearMonth = [TempData{5},TempData{6}];
                    % check if the 8th position is also desc, like in BEAN OIL or
                    % BEAN MEAL
                    if isempty(str2num(TempData{9})) || strcmpi(TempData{8},'cottn')% only for strings
                        Desc      = cellstr([TempData{7},' ',TempData{8},' ',TempData{9}]);
                        Strike    = num2cell(str2num(TempData{10}));
                    else
                        Desc      = cellstr([TempData{7},' ',TempData{8}]);
                        Strike    = num2cell(str2num(TempData{9}));
                    end
                    % check if the fraction value is present for Traded Price
                    if isempty(strfind(TempData{11},'.')) % tradedprice in position 12
                        TradePrice = num2cell(str2num(TempData{12}));
                    else
                        TradePrice = num2cell(str2num(TempData{11}) + (0.01 * str2num(TempData{12})));     %#ok<*ST2NM>
                    end
                case 15
                    NewData15 = [NewData15;TempData];
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    ProductType = cellstr(TempData{4});
                    YearMonth = [TempData{5},TempData{6}];
                    Desc      = cellstr([TempData{7},' ',TempData{8},' ',TempData{9}]);
                    Strike    = num2cell(str2num(TempData{10}));
                    TradePrice = num2cell(str2num(TempData{12}) + (0.01 * str2num(TempData{13})));
                    if(isempty(Strike))
                        Strike    = num2cell(str2num(TempData{11}));
                        TradePrice = num2cell(str2num(TempData{13}));
                    end
                    
                case 16
                    TradeDate = TempData(1);
                    Quantity = num2cell(str2double(TempData{3}));
                    ProductType = cellstr(TempData{4});
                    YearMonth = [TempData{5},TempData{6}];
                    Desc      = cellstr([TempData{7},' ',TempData{8},' ',TempData{9}]);
                    Strike    = num2cell(str2num(TempData{11}));
                    TradePrice = num2cell(str2num(TempData{13}) + (0.01 * str2num(TempData{14})));
                otherwise
                    IsValidRow = 0;
                    continue
            end
            
            if IsValidRow
                YearMonthCode = cellstr([MonthCode{strncmpi(YearMonth,Month,3)},YearMonth(end)]);
                Maturity = YearMonthCode;
                char_end_1 = YearMonth(end-1); %% Newly added line on 2020-01-08, for fixing year
                NewTradeDate = strsplit(char(TradeDate),'/');  NewTradeDate{3} = [char_end_1,NewTradeDate{3}];
                %NewTradeDate = strsplit(char(TradeDate),'/'); NewTradeDate{3} = ['1',NewTradeDate{3}]; %%% old line
                TradeDate = strjoin(NewTradeDate,'/');
                FinalRowData = [AccountNumber, datestr(TradeDate,'dd-mm-yyyy'), BuyOrSell, Quantity, YearMonthCode, Desc, Strike, ProductType, TradePrice,Maturity];
                if length(FinalRowData) == 10
                    OutData      = [OutData; FinalRowData];
                end
            end
        end
        
    end
    
    TradesOutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice','Maturity'};
    
    if(strcmpi(Type_Name,'cfs'))
        [~,~,ConversionFac] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'Broker Statement Conversion');
    elseif(strcmpi(Type_Name,'cop'))
        [~,~,ConversionFac] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'Broker Statement Conversion');
    elseif(strcmpi(Type_Name,'agf'))
        [~,~,ConversionFac] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Broker Statement Conversion');
    elseif(strcmpi(Type_Name,'usg'))
        [~,~,ConversionFac] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Broker Statement Conversion');
    else
    end
    ConversionFac = ConversionFac(2:end,:);
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN         = cellfun(@isnan,ConversionFac(:,1),'UniformOutput',false);
    for iRNaN = 1:length(IdxNaN)
        if any(IdxNaN{iRNaN})
            IdxNaNRows = [IdxNaNRows; iRNaN];
        end
    end
    ConversionFac(IdxNaNRows,:) = [];
    
    ConvDesc = ConversionFac(:,1);
    PriceFac = cell2mat(ConversionFac(:,2));
    StrikeFac = cell2mat(ConversionFac(:,3));
    TRDesc = ConversionFac(:,4);
    PosStrike = 7; PosTradePrice = 9;PosSide = 3; PosQuantity = 4; PosDesc = 6;
    
    for i = 1:length(ConvDesc)
        Idx = strcmpi(ConvDesc{i},OutData(:,PosDesc));
        
        if any(Idx)
            OutData(Idx,PosStrike) = num2cell(cell2mat(OutData(Idx,PosStrike)) .* StrikeFac(i));
            OutData(Idx,PosTradePrice) = num2cell(cell2mat(OutData(Idx,PosTradePrice)) .* PriceFac(i));
            OutData(Idx,PosDesc) = TRDesc(i);
        end
    end
    Idx = strcmpi('S',OutData(:,PosSide));
    OutData(Idx,PosQuantity) = num2cell(-(cell2mat(OutData(Idx,PosQuantity))));
    %   OutData(:,PosQuantity) = num2cell(abs(cell2mat(OutData(:,PosQuantity))));
    TradesOutput = [TradesOutputHeader;OutData];
    
    Start_Of_Data = cellStrfind(RawData,'F  O  R  E  I  G  N    E  X  C  H  A  N  G  E    A  C  T  I  V  I  T  Y');
    End_Of_Data = cellStrfind(RawData,{'F  O  R  E  I  G  N    E  X  C  H  A  N  G  E    P  O  S  I  T  I  O  N  S',...
        'O   P   E   N      P  O  S  I  T  I  O  N  S'});
    OutFxData = [];
    for iS = 1:length(Start_Of_Data)
        
        SD = Start_Of_Data(iS) -10 : Start_Of_Data(iS);
        LineAccNum = SD(ismember(SD,AccNum));
        if isempty(LineAccNum)
            SD = Start_Of_Data(iS) -20 : Start_Of_Data(iS)+20;
            LineAccNum = SD(ismember(SD,AccNum));
        end
        AccountNumber = strrep(strtrim(RawData(LineAccNum)),'ACCOUNT NUMBER: L ','');
        AccountNumber = strrep(AccountNumber,' BNE01','');
        
        NewEndOfData = End_Of_Data(End_Of_Data > Start_Of_Data(iS));
        [~,Idx] = min(NewEndOfData - Start_Of_Data(iS));
        
        Data = RawData(Start_Of_Data(iS):NewEndOfData(Idx));
        
        NumLines = length(Data);
        for i = 5:NumLines-2
            TempData = strsplit(strtrim(Data{i}),' ');
            if isempty(strfind(TempData{1},'/'))
                continue
            else
                TradeDate = TempData(1);
                ValueDate = TempData(2);
                BuyOrSell = TempData(3);
                Quantity = TempData(5);
                TradePrice = num2cell(str2num(TempData{6}));
                Desc = cellstr([TempData{4},'/',TempData{7}]);
                Strike    = num2cell(NaN);
                ProductType = cellstr('FXD');
                FxRowData = [AccountNumber,TradeDate,BuyOrSell,Quantity,ValueDate,Desc,Strike,ProductType,TradePrice];
                OutFxData   = [OutFxData; FxRowData]; %#ok<*AGROW>
            end
        end
    end
    if ~isempty(OutFxData)
        FxOutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice'};
        FxOutput = [FxOutputHeader;OutFxData];
    end
    delete(hWaitbar);
    
    TimeStamp = strrep(strrep(datestr(datetime,'HH:MM:SS'),' ','_'),':','-');
    XLFileName = [upper(char(Type_Name)),'_','BrokerStatement_',char(StatementDate),'_',TimeStamp,'.xlsx'];
    xlswrite(XLFileName,TradesOutput,'Trades');
    if ~isempty(OutFxData)
        xlswrite(XLFileName,FxOutput,'FxTrades');
    end
    XLFileName = fullfile(pwd,XLFileName);
    try
        xls_delete_sheets(XLFileName);
    catch
    end
    msgbox(['XLS Broker statement is saved in ',char(XLFileName)],'Missing Deals Parser');
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    warndlg(errorMessage);
    
    OutData = '';
    StatementDate = '';
    PathName = '';
end
end



