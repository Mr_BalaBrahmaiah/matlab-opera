function varargout = MDParser(varargin)
% MDPARSER MATLAB code for MDParser.fig
%      MDPARSER, by itself, creates a new MDPARSER or raises the existing
%      singleton*.
%
%      H = MDPARSER returns the handle to a new MDPARSER or the handle to
%      the existing singleton*.
%
%      MDPARSER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MDPARSER.M with the given input arguments.
%
%      MDPARSER('Property','Value',...) creates a new MDPARSER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MDParser_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MDParser_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MDParser

% Last Modified by GUIDE v2.5 28-May-2014 15:08:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MDParser_OpeningFcn, ...
                   'gui_OutputFcn',  @MDParser_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MDParser is made visible.
function MDParser_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MDParser (see VARARGIN)

% Choose default command line output for MDParser
handles.output = hObject;

handles.BrokerStatement = '';
handles.XTRFileName = '';
handles.MurexExtractFile = '';

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MDParser wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MDParser_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edtBrokerStmt_Callback(hObject, eventdata, handles)
% hObject    handle to edtBrokerStmt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtBrokerStmt as text
%        str2double(get(hObject,'String')) returns contents of edtBrokerStmt as a double


% --- Executes during object creation, after setting all properties.
function edtBrokerStmt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtBrokerStmt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbBrokerStmt.
function pbBrokerStmt_Callback(hObject, eventdata, handles)
% hObject    handle to pbBrokerStmt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName]  = uigetfile('*.txt','Select the Broker Text file statement');

if isnumeric(FileName) && FileName==0
    errordlg('Broker Text file statement not selected!','Missing Deals Parser');    
    return;
else
    BrokerStatement = fullfile(PathName,FileName);
    set(handles.edtBrokerStmt,'String',BrokerStatement);
    handles.BrokerStatement = BrokerStatement;    
end
guidata(hObject,handles);

function edtXTRFills_Callback(hObject, eventdata, handles)
% hObject    handle to edtXTRFills (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtXTRFills as text
%        str2double(get(hObject,'String')) returns contents of edtXTRFills as a double


% --- Executes during object creation, after setting all properties.
function edtXTRFills_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtXTRFills (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbXTRFills.
function pbXTRFills_Callback(hObject, eventdata, handles)
% hObject    handle to pbXTRFills (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName]  = uigetfile('*.csv','Select the XTR Fills file');

if isnumeric(FileName) && FileName==0
    errordlg('XTR Fills File not selected!','Missing Deals Parser');    
else
    XTRFileName = fullfile(PathName,FileName);
    set(handles.edtXTRFills,'String',XTRFileName);
    handles.XTRFileName = XTRFileName;       
    set(handles.pbGenerateMD,'enable','on');
end
handles.MurexExtractFile = '';
set(handles.edtMurexExtract,'String','');
guidata(hObject,handles);

function edtMurexExtract_Callback(hObject, eventdata, handles)
% hObject    handle to edtMurexExtract (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtMurexExtract as text
%        str2double(get(hObject,'String')) returns contents of edtMurexExtract as a double


% --- Executes during object creation, after setting all properties.
function edtMurexExtract_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtMurexExtract (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbMurexExtract.
function pbMurexExtract_Callback(hObject, eventdata, handles)
% hObject    handle to pbMurexExtract (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName]  = uigetfile('*.xlsx','Select the Murex Extract file');

if isnumeric(FileName) && FileName==0
    errordlg('Murex Extract File not selected!','Missing Deals Parser');    
else
    MurexExtractFile = fullfile(PathName,FileName);
    set(handles.edtMurexExtract,'String',MurexExtractFile);
    handles.MurexExtractFile = MurexExtractFile;     
    set(handles.pbGenerateMD,'enable','on');
end
handles.XTRFileName = '';
set(handles.edtXTRFills,'String','');
guidata(hObject,handles);

% --- Executes on button press in rbXTR.
function rbXTR_Callback(hObject, eventdata, handles)
% hObject    handle to rbXTR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbXTR
if get(handles.rbXTR,'Value')    
    set(handles.edtXTRFills,'enable','on');
    set(handles.pbXTRFills,'enable','on');
    set(handles.rbMurexExtract,'Value',0);
    set(handles.edtMurexExtract,'enable','off');
    set(handles.pbMurexExtract,'enable','off');    
end
guidata(hObject,handles);

% --- Executes on button press in rbMurexExtract.
function rbMurexExtract_Callback(hObject, eventdata, handles)
% hObject    handle to rbMurexExtract (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbMurexExtract
if get(handles.rbMurexExtract,'Value')    
    set(handles.edtMurexExtract,'enable','on');
    set(handles.pbMurexExtract,'enable','on');
    set(handles.rbXTR,'Value',0);    
    set(handles.edtXTRFills,'enable','off');
    set(handles.pbXTRFills,'enable','off');   
end
guidata(hObject,handles);

% --- Executes on button press in pbGenerateMD.
function pbGenerateMD_Callback(hObject, eventdata, handles)
% hObject    handle to pbGenerateMD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
generate_missing_deals_Murex(handles.BrokerStatement,handles.XTRFileName,handles.MurexExtractFile)
