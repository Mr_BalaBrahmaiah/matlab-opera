function [OutErrorMsg,LastTradeId,TradeIdWithoutPrefix]= getLastTradeId(InBUName)

OutErrorMsg = {'No errors'};
LastTradeId = {''};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view'; 
    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));

    if strcmpi(InBUName,'qf5')
        TableName =['deal_ticket_table_with_latest_versions_view_',InBUName];
        SqlQuery = ['select mid(trade_id,12) from ',TableName,' where trade_id like ''',TradeIdPrefix,'%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);
    else
        TableName =['cloned_deal_ticket_table_',InBUName];
        SqlQuery = ['select mid(trade_id,12) from ',TableName,' where trade_id like ''',TradeIdPrefix,'%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);
    end
    
    TradeIdWithoutPrefix = max(str2num(char(DBTradeId)));    
    
    LastTradeId = cellstr([TradeIdPrefix,num2str(TradeIdWithoutPrefix)]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end