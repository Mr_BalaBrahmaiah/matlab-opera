function [OutXLSFileName , Future_NettedTRData] = process_missingdeals_trdump_updated(Type_Name,XTR_File_Path, BrokerStatement ,TSFileNames ,Multi_PnP_Excel_Path, Option_BrokerTRData)

%%% Old function
%%% process_missingdeals_trdump_updated(Type_Name,XTR_File_Path, BrokerStatement ,TSFileNames ,Multi_PnP_Excel_Path)

%%% New Function
%%% [OutXLSFileName , Future_NettedTRData] = process_missingdeals_trdump_updated(Type_Name,XTR_File_Path, BrokerStatement ,TSFileNames ,Multi_PnP_Excel_Path)

% com.sun.java.swing.plaf.windows.WindowsLookAndFeel;  %% For Enable Borders in MATLAB EXECUTABLE
%
% % InputOptions = {'cfs','agf','qf1','qf2','qf3','qf4','No thank you'};
% InputOptions = {'agf','orx','No thank you'};
% DefaultSelection = InputOptions{end};
% UserSelction = ButtonChoice4User(InputOptions, 'Please Choose the Business File Format', DefaultSelection,...
%     'Business Unit');
% Type_Name = InputOptions{UserSelction};
% fprintf( 'User selection : "%s"\n', InputOptions{UserSelction});

%%
Future_NettedTRData = [];
% OutXLSFileName = ['TR_Dump_',char(StatementDate),'.xls'];
OutXLSFileName = getXLSFilename(upper(Type_Name));

Master_Path = [pwd filesep 'Master Files' filesep];

%%
[System_Date_Format,System_Date] = get_System_Date_Format();

if isdeployed
    if(strcmpi(System_Date_Format,'dd-mm-yyyy'))
        DateNum_Conv = 'mm/dd/yyyy';
        %         msgbox(System_Date_Format);
    else
        DateNum_Conv = 'dd-mm-yyyy';
        %         msgbox(System_Date_Format);
    end
else
    DateNum_Conv = System_Date_Format;
    %     msgbox(System_Date_Format);
end

DB_Date_Format = 'yyyy-mm-dd';

%%
if(strcmpi(Type_Name,'No thank you'))
    errordlg('You Must be Selected anyone of the Business Unit...!');
    return;
else
    [~,BrokerData,XTRFillsData,TraderSnapsData,StatementDate,Not_GettingPrice,fid] = generate_missing_deals(Type_Name,XTR_File_Path,BrokerStatement,TSFileNames,Multi_PnP_Excel_Path, Option_BrokerTRData);
    
    
    if(Not_GettingPrice) %% From ORYX Set Product
        close all force;
        warndlg(['Please Upload the Market Prices to calculate premium and then run the ',upper(Type_Name),' Parser'],upper(Type_Name));
        return;
    end
    
    if(isempty(BrokerData) &&  isempty(XTRFillsData) && isempty(TraderSnapsData))
        % errordlg('No files selected for broker statement, traders snaps and XTR Fills!'); %% It is Common Parser
        close all force;
        errordlg(['No ',Type_Name,'Trades found in XTR Fills File !']);  %% It is AGF Parser
        
        return;
    else
        
        %% create the TR format dump file
        OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType', 'TradePrice','Maturity Month'};
        BrokerHeader = [OutputHeader,'Trader Name','Traded DateTimestamp'];
        TSOutputHeader = [OutputHeader,'Trader Name','Traded DateTimestamp','Source','Clearer'];
        
        %  OLD HEADER   %     TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
        %     %         'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
        %     %         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
        %     %         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
        %     %         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        %     %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
            'OptType','Prem','CounterParty','Initial Lots',...
            'CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','Source',...
            'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};
        PosAccNo = 1;
        PosTradeDate = 2;
        PosSide = 3;
        PosQuantity = 4;
        PosYearMonth = 5;
        PosDesc = 6;
        PosStrike = 7;
        PosProdType = 8;
        PosTradePrice = 9;
        PosMaturity = 10;
        
        % used only in traders snaps
        PosExeBroker = 11;
        PosClearer = 12;
        PosExeType = 13;
        PosRates = 14;
        PosSpclRate = 15;
        
        PosFutureSource = 16;
        
        %%
        ObjDB = connect_to_database;
        Tablename = ['subportfolio_portfolio_table_',Type_Name];
        SqlQuery = ['select subportfolio,portfolio from ',Tablename];
        DBData = fetch(ObjDB,SqlQuery);
        RefDBSubportfolio = DBData(:,1);
        RefDBPortfolio = DBData(:,2);
        
        % read the TR master file
        % this is applicable only for Broker statement and Traders snaps
        if( strcmpi(Type_Name,'cfs') ||  strcmpi(Type_Name,'cop') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
            if(strcmpi(Type_Name,'cfs'))
                [~,~,ProductMaster] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'TR Format Master');
            elseif(strcmpi(Type_Name,'cop'))
                [~,~,ProductMaster] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'TR Format Master'); %% COP
            elseif(strcmpi(Type_Name,'agf'))
                [~,~,ProductMaster] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'TR Format Master'); %% AGF
            elseif(strcmpi(Type_Name,'usg'))
                [~,~,ProductMaster] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'TR Format Master'); %% USG
            else
                
            end
            
            ProductMaster = ProductMaster(2:end,:);
            % To remove the lines of xl whcih contains NaN in all columns; the data
            % which gets removed from xl, but the row remains still
            IdxNaNRows = [];
            IdxNaN         = cellfun(@isnan,ProductMaster(:,4),'UniformOutput',false);
            for iRNaN = 1:length(IdxNaN)
                if any(IdxNaN{iRNaN})
                    IdxNaNRows = [IdxNaNRows; iRNaN];
                end
            end
            ProductMaster(IdxNaNRows,:) = [];
            
            RefPortfolio  = ProductMaster(:,1);
            RefInstrument = ProductMaster(:,2);
            RefDesc    = ProductMaster(:,3);
            RefAccNo   = ProductMaster(:,4);
            RefProdCode = ProductMaster(:,5);
            
            StrikeConv = cell2mat( ProductMaster(:,6));
            PremiumConv = cell2mat( ProductMaster(:,7));
            RefDirPortfolio = ProductMaster(:,8);
            
            % construct the TR format for broker statement data
            if ~isempty(BrokerData)
                Temp_Broker = cell(size(BrokerData,1),2);
                BrokerData = [BrokerData ,Temp_Broker];
                BrokerTRData      = construct_TR_format(BrokerData,'Broker',char(Type_Name));
            else
                BrokerTRData = [];
            end
            
            % construct the TR format for traders snaps data
            if ~isempty(TraderSnapsData)
                IdxSnapsData = strncmpi('wells',TraderSnapsData(:,PosClearer),length('wells')) & ...
                    strcmpi('XTrader',TraderSnapsData(:,PosExeBroker)) & strcmpi('Future',TraderSnapsData(:,PosProdType));
                TraderSnapsData(IdxSnapsData,:) = [];
                TraderSnapsTRData = construct_TR_format(TraderSnapsData,'TS',char(Type_Name));
                UpdTradersSnapsData = TraderSnapsData;
                UpdTradersSnapsData(:,11:end) = [];
                TRSSource = repmat(cellstr('Traders Snaps'),size(UpdTradersSnapsData,1),1);
                TRSClearer = TraderSnapsData(:,PosClearer);
                UpdTradersSnapsData = [UpdTradersSnapsData TRSSource TRSClearer];
            else
                UpdTradersSnapsData = TraderSnapsData;
                TraderSnapsTRData = [];
            end
            
        else
            UpdTradersSnapsData = [];
            TraderSnapsTRData = [];
        end
        
        
        %% read the TR master file
        try
            if ~isempty(XTRFillsData) || ~isempty(BrokerData)
                
                % this is applicable only for XTR fills data
                if(strcmpi(Type_Name,'cfs'))
                    [~,~,ProductMaster] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
                elseif(strcmpi(Type_Name,'agf'))
                    [~,~,ProductMaster] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
                elseif(strcmpi(Type_Name,'orx'))
                    [~,~,ProductMaster] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
                elseif(strcmpi(Type_Name,'usg'))
                    [~,~,ProductMaster] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
                else
                    [~,~,ProductMaster] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
                end
                
                %%%  Remove NaN Fields
                Header = ProductMaster(1,:);
                NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);
                ProductMaster(:,NaN_Col(:)) = [];
                
                Row_find_NaN = ProductMaster(:,4);
                NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_NaN);
                ProductMaster(NaN_Row(:),:) = [];
                
                %%%
                ProductMaster = ProductMaster(2:end,:);
                % To remove the lines of xl whcih contains NaN in all columns; the data
                % which gets removed from xl, but the row remains still
                IdxNaNRows = [];
                IdxNaN         = cellfun(@isnan,ProductMaster(:,4),'UniformOutput',false);
                for iRNaN = 1:length(IdxNaN)
                    if any(IdxNaN{iRNaN})
                        IdxNaNRows = [IdxNaNRows; iRNaN];
                    end
                end
                ProductMaster(IdxNaNRows,:) = [];
                
                RefPortfolio  = ProductMaster(:,1);
                RefInstrument = ProductMaster(:,2);
                RefDesc    = ProductMaster(:,3);
                RefAccNo   = ProductMaster(:,4);
                RefProdCode = ProductMaster(:,5);
                
                StrikeConv = cell2mat( ProductMaster(:,6));
                PremiumConv = cell2mat( ProductMaster(:,7));
                
                % construct the TR format for XTRFills data
                if ~isempty(XTRFillsData)
                    XTRClearer = XTRFillsData(:,14);
                    XTRFillsData(:,1) = XTRFillsData(:,13); % to assign the XTR account no to Account number field
                    XTRFillsData = XTRFillsData(:,1:12);
                    
                    %% Transaction Number
                    TempTradeId = '';
                    try
                        [OutErrorMsg,DBTradeId]= getLastTradeId(Type_Name);
                        if strcmpi(OutErrorMsg,'No Errors')
                            % TradeIdPrefix = 'TR-FY16-17-';
                            SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
                            TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
                            TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
                            
                            if(isempty(TempTradeId))
                                TempTradeId = 0;      %% Default
                            end
                        end
                    catch
                        TempTradeId = '';
                    end
                    %%
                    
                    XTRFillsTRData    = construct_TR_format([XTRFillsData , XTRClearer],'XTR',Type_Name); %% New Argument is TypeName
                    
                    XTRSource = repmat(cellstr('XTR Fills'),size(XTRFillsData,1),1);
                    %     XTRClearer = repmat(cellstr('WELLS FARGO'),size(XTRFillsData,1),1);
                    XTRFillsData = [XTRFillsData XTRSource XTRClearer];
                    
                else
                    XTRFillsTRData = [];
                end
                
                fclose(fid);
                
                UpdTradersSnapsData = [UpdTradersSnapsData , cell(size(UpdTradersSnapsData,1),2) ];
                TradersData   = [XTRFillsData;UpdTradersSnapsData];
                TradersTRData = [XTRFillsTRData;TraderSnapsTRData];
                
                if( ~isempty(BrokerData) && ( strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'cop') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg') )  )    %% Broker Data Only for CFS and COP
                    % write broker data
                    xlswrite(OutXLSFileName,[BrokerHeader;BrokerData],'Broker Statement'); %% OutputHeader  %% BrokerHeader
                    % write broker tr format data
                    xlswrite(OutXLSFileName,[TRHeader;BrokerTRData],'Broker - TR format');
                end
                
                % write traders data
                %                         xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills + TR Snaps'); %% Change The Order of Writing
                % write traders tr format data
                %             xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR+TR Snaps - TR format');
                
                %% Netted XTR-TR Data and Conso Netted Sheet Fileds
                if ~isempty(TradersTRData)
                    PosSource = strcmpi('Source',TRHeader);
                    SourceData = TradersTRData(:,PosSource);
                    
                    IdxXTR = strcmpi('XTR Fills',SourceData);
                    
                    XTRNetData = TradersTRData(IdxXTR,:);
                    if ~isempty(XTRNetData) % to check if XTR fills file is selected or not
                        UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date'};
                        SumFields = {'Active Lots','Initial Lots'};
                        NettedXTRData = calc_netted_data(TRHeader, XTRNetData,TRHeader,UniqueFields,SumFields,[]);
                        NettedTRData = [NettedXTRData;TradersTRData(~IdxXTR,:)];
                        
                        % NaN_Row =  cellfun(@(V) any(isnan(V(:))), NettedTRData(:,2)); %% My Changing
                        % NettedTRData(NaN_Row(:),:) = [];
                        
                        
                        if(strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
                            Netted_Header = NettedTRData(1,:);
                            NettedTRData = NettedTRData(2:end,:);
                            CounterParty_Col = cellStrfind_Exact(Netted_Header,{'CounterParty'}) ;
                            Not_Uploded_Index = ~strncmpi(NettedTRData(:,CounterParty_Col),'WELLS FARGO',length('WELLS FARGO'));
                            Not_Uploaded_Data = [Netted_Header ; NettedTRData(Not_Uploded_Index,:)];
                            NettedTRData(Not_Uploded_Index,:) = [] ; %% Remove Not_Uploaded_Data in Netted Data
                            
                            NettedTRData = [Netted_Header ; NettedTRData];
                        end
                        
                        if ~isempty(TempTradeId)
                            PosTrnId = strcmpi('TRN.Number',TRHeader);
                            PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
                            NumRows1 = size(NettedTRData,1) - 1; % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
                            NTradeId = (TempTradeId+1:1:TempTradeId+NumRows1)';
                            TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
                            TradeId = strrep(TradeId,' ','');  %% Remove Spaces
                            NettedTRData(2:end,PosTrnId) = TradeId;
                            NettedTRData(2:end,PosParentTrnId) = TradeId;
                        end
                        
                        [NettedTRData] = mapping_4_changing_OptType(NettedTRData) ;
                        
                        if isdeployed
                            TransactionDate_Col = cellStrfind_Exact(TRHeader,{'Transaction Date'});
                            %                             msgbox(NettedTRData{2,TransactionDate_Col});
                            NettedTRData(2:end,TransactionDate_Col) = cellstr(datestr(datenum( NettedTRData(2:end,TransactionDate_Col),DateNum_Conv),1));
                            xlswrite(OutXLSFileName,NettedTRData,'Netted TR Data-XTR'); %% 'Netted TR Data-XTR+TR Snaps with ConsoNetted'

                            Future_NettedTRData = NettedTRData;   %%% Newly added line
                            %save Future_NettedTRData Future_NettedTRData   
                            
                            if(strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
                                Not_Uploaded_Data(2:end,TransactionDate_Col) = cellstr(datestr(datenum( Not_Uploaded_Data(2:end,TransactionDate_Col),DateNum_Conv),1));
                                xlswrite(OutXLSFileName,Not_Uploaded_Data,'Not_Uploaded_Data');
                                OutXLSFileName_Not_Upload = getXLSFilename(strcat(upper(Type_Name),'_Not_Uploaded_Data'));
                                xlswrite(OutXLSFileName_Not_Upload,Not_Uploaded_Data);
                            end
                            xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills');
                            xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR Fills-TR format');
                            %                             msgbox('Finish','Exe');
                        else
                            % write netted traders tr format data
                            xlswrite(OutXLSFileName,NettedTRData,'Netted TR Data-XTR'); %% Seperate Netted TR Data-XTR+TR Snaps Excel Sheet
                            
                            Future_NettedTRData = NettedTRData;   %%% Newly added line
                            %save Future_NettedTRData Future_NettedTRData 
                            
                            if(strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
                                xlswrite(OutXLSFileName,Not_Uploaded_Data,'Not_Uploaded_Data');
                                OutXLSFileName_Not_Upload = getXLSFilename(strcat(upper(Type_Name),'_Not_Uploaded_Data'));
                                xlswrite(OutXLSFileName_Not_Upload,Not_Uploaded_Data);
                            end
                            xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills');
                            xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR Fills-TR format');
                        end
                        
                    end
                    %% Conso Mapping Only for AGF Type
                    %If user want Conso Portfolio for AGF  using if condition like this if(~isempty(NettedTRData)) && strcmp(Type_Name,'agf')
                    % No Need Conso Portfolio for AGF using if(~isempty(ConsoNew_NettedTRData))
                    ConsoNew_NettedTRData = [];
                    Not_MatchedPortfolio = [];
                    Matched_Portfolio = [];
                    if(~isempty(ConsoNew_NettedTRData)) %% (~isempty(NettedTRData)) && strcmp(Type_Name,'agf')
                        if strcmpi(Type_Name,'agf')
                        [~,~,ConsoMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Conso Mapping');
                        elseif strcmpi(Type_Name,'usg')
                            [~,~,ConsoMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Conso Mapping');
                        else
                            
                        end
                            
                        Nan_Row =  cellfun(@(V) any(isnan(V(:))), ConsoMapping(:,1)); %% Nan_Row = find(Nan_Row);
                        ConsoMapping(Nan_Row(:),:) = [];
                        ConsoMapping = ConsoMapping(2:end,:);
                        
                        Unique_NettedTR = unique(NettedTRData(2:end,2))';
                        Unique_ConsoNetted = unique(ConsoMapping(:,1))';
                        Portfolio_MatchedIndex = cellStrfind_Exact(Unique_NettedTR,Unique_ConsoNetted);
                        
                        Unique_NettedTR(Portfolio_MatchedIndex) = [];
                        
                        if(~isempty(Unique_NettedTR))
                            warndlg(['Conso Mapping Portfolio',Unique_NettedTR,' Which is not available in Master File(in Conso Mapping). Please update this account number in master file(in Conso Mapping Sheet) to get these account deals'],'MD Parser');
                        end
                        
                        ConsoHeader = NettedTRData(1,:);
                        Conso_NettedTRData = NettedTRData(2:end,:);
                        
                        %                         ConsoNew_NettedTRData = [];
                        %                         Not_MatchedPortfolio = [];
                        %                         Matched_Portfolio = [];
                        
                        NettedTRData_Portfolio = strtrim(Conso_NettedTRData(:,2));
                        NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,8));   %% If Column 25 or 27
                        % NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,9)); %% If Column 32
                        
                        for k = 1 : length(ConsoMapping(:,1))
                            ConsoMapping_Portfolio = strtrim(ConsoMapping{k,1});
                            ConsoMapping_Counterparty = strtrim(ConsoMapping{k,2});
                            
                            MatchedIndex = find(strcmpi(ConsoMapping_Portfolio, NettedTRData_Portfolio) & ...
                                strcmpi(ConsoMapping_Counterparty, NettedTRData_CounterParty));
                            
                            if(~isempty(MatchedIndex))
                                Temp_Conso = Conso_NettedTRData(MatchedIndex,:);
                                Temp_Conso(:,2) = {strtrim(ConsoMapping{k,3})};
                                Temp_Conso(:,8) =  {strtrim(ConsoMapping{k,4})};
                                ConsoNew_NettedTRData = [ConsoNew_NettedTRData ; Temp_Conso];
                                
                                Temp_Conso = [];
                                
                                Matched_Portfolio =  [Matched_Portfolio ; {ConsoMapping_Portfolio}];
                                
                                % Conso_NettedTRData(MatchedIndex,2) = {strtrim(ConsoMapping{k,3})};
                                % Conso_NettedTRData(MatchedIndex,8) = {strtrim(ConsoMapping{k,4})};  %% If Column 25 25 or 27
                                % Conso_NettedTRData(MatchedIndex,9) = {strtrim(ConsoMapping{k,4})};  %% If Column 32
                                
                            else
                                
                                Not_MatchedPortfolio = [Not_MatchedPortfolio ; {ConsoMapping_Portfolio}];
                            end
                            
                        end
                        
                        
                        %%
                        TempTradeId = NTradeId(end);   %% Get from NettedTRData
                        
                        if ~isempty(TempTradeId)
                            PosTrnId = strcmpi('TRN.Number',TRHeader);
                            PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
                            NumRows1 = size(ConsoNew_NettedTRData,1);
                            NTradeId = (TempTradeId+1:1:TempTradeId+NumRows1)';
                            TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
                            TradeId = strrep(TradeId,' ','');  %% Remove Spaces
                            ConsoNew_NettedTRData(:,PosTrnId) = TradeId;
                            ConsoNew_NettedTRData(:,PosParentTrnId) = TradeId;
                        end
                    end
                    %%
                    %  xlswrite(['RMO_',OutXLSFileName],[ConsoHeader;Conso_NettedTRData],'Conso Netted');
                    %  OutXLSFileName1 = fullfile(pwd,['RMO_',OutXLSFileName]);  %% Seperate RMO Excel Sheet
                    %  xls_delete_sheets(OutXLSFileName1);
                    if(strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
                        AGF_Conso_Netted = [NettedTRData;ConsoNew_NettedTRData];
                        
                        [AGF_Conso_Netted] = mapping_4_changing_OptType(AGF_Conso_Netted) ;
                        
                        [AGF_Conso_Netted,Not_MatchedPortfolio] = Changing_CounterParty_Using_MasterSheet(Type_Name,AGF_Conso_Netted,Master_Path);%% No Need Conso Portfolio CounterParty Chanaged
                        if(~isempty(Not_MatchedPortfolio))
                            %errordlg([upper(Type_Name),{' '},char(Not_MatchedPortfolio),{' '},' Counterparty Mapping Not Available'],'Missing Deals Parser');
                        end
                        
                        PosTrnId = strcmpi('TRN.Number',AGF_Conso_Netted(1,:));
                        PosParentTrnId = strcmpi('Parent Transaction Number',AGF_Conso_Netted(1,:));
                        [TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(AGF_Conso_Netted(2:end,:),Type_Name) ;
                        AGF_Conso_Netted(2:end,PosTrnId) = TradeId;
                        AGF_Conso_Netted(2:end,PosParentTrnId) = TradeId;  %% Trade ID Change
                        
                        if isdeployed
                            TransactionDate_Col = cellStrfind_Exact(TRHeader,{'Transaction Date'});
                            %                             msgbox(AGF_Conso_Netted{2,TransactionDate_Col});
                            AGF_Conso_Netted(2:end,TransactionDate_Col) = cellstr(datestr(datenum( AGF_Conso_Netted(2:end,TransactionDate_Col),DateNum_Conv),1));
                            xlswrite(OutXLSFileName,AGF_Conso_Netted,'Netted TR Data-XTR'); %% 'Netted TR Data-XTR+TR Snaps with ConsoNetted'
                            
                            Future_NettedTRData = AGF_Conso_Netted;   %%% Newly added line
                            %save Future_NettedTRData Future_NettedTRData 
                            
                            % write traders data
                            %                             TransactionDate_Col = cellStrfind_Exact(TSOutputHeader,{'Traded DateTimestamp'});
                            %                             TradersData(:,TransactionDate_Col) = cellstr(datestr(datenum(TradersData(:,TransactionDate_Col),DateNum_Conv),1));
                            xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills');
                            
                            % write traders tr format data
                            %                             TransactionDate_Col = cellStrfind_Exact(TRHeader,{'Transaction Date'});
                            %                             TradersTRData(:,TransactionDate_Col) = cellstr(datestr(datenum(TradersTRData(:,TransactionDate_Col),System_Date_Format),1));
                            xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR Fills-TR format');
                            
                            %                             msgbox('Finish','Exe');
                        else
                            % 'Netted TR Data-XTR+TR Snaps with ConsoNetted
                            xlswrite(OutXLSFileName,AGF_Conso_Netted,'Netted TR Data-XTR'); %% 'Netted TR Data-XTR+TR Snaps with ConsoNetted'
                            
                            Future_NettedTRData = AGF_Conso_Netted;   %%% Newly added line
                            %save Future_NettedTRData Future_NettedTRData 
                            
                            % write traders data
                            xlswrite(OutXLSFileName,[TSOutputHeader;TradersData],'XTR Fills');
                            
                            % write traders tr format data
                            xlswrite(OutXLSFileName,[TRHeader;TradersTRData],'XTR Fills-TR format');
                            
                        end
                        % Make Active Sheet
                        SheetName = 'Netted TR Data-XTR';
                        try
                            Full_Path = [pwd filesep OutXLSFileName];
                            xls_change_activesheet(Full_Path,SheetName);  %% Full Path Necessary..!!!!
                        catch
                        end
                        
                    end
                end
            end
        catch ME
            %             disp(ME.message);
            errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
                ME.stack(1).name, ME.stack(1).line, ME.message);
            fprintf(1, '%s\n', errorMessage);
            
        end
        
        OutXLSFileName = fullfile(pwd,OutXLSFileName);
        SheetName = 'Netted TR Data-XTR';
        try
            xls_delete_sheets(OutXLSFileName);
            xls_change_activesheet(OutXLSFileName,SheetName);
        catch
        end
        %         msgbox([upper(Type_Name),' TR dump is saved in ',char(OutXLSFileName)],'Missing Deals Parser');  %%Show File Saving Path
        
    end
end
%%
    function OutData = construct_TR_format(InData,Name,Type_Name)
        
        if( strcmpi(Type_Name,'cfs') ||  strcmpi(Type_Name,'cop') || strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))%% For Broker Empty Instrument and Portfolio
            if(strcmpi(Type_Name,'cfs'))
                [~,~,ProductMaster_Temp] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'TR Format Master');
            elseif(strcmpi(Type_Name,'agf'))
                [~,~,ProductMaster_Temp] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'TR Format Master');
            elseif(strcmpi(Type_Name,'usg'))
                [~,~,ProductMaster_Temp] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'TR Format Master');
            else
                [~,~,ProductMaster_Temp] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'TR Format Master'); %% COP
            end
            ProductMaster_Temp_Header = ProductMaster_Temp(1,:);
            ProductMaster_Temp = ProductMaster_Temp(2:end,:);
        end
        
        % construct the broker data in TR format
        NumRows = size(InData,1);
        OutData = [];
        for iR = 1:NumRows
            
            AccNum = InData{iR,PosAccNo};
            Description = InData{iR,PosDesc};
            if strcmpi(Name,'XTR')
                IdxAccNo = strcmpi(AccNum,RefAccNo) & strcmpi(Description,RefDesc);
            else
                if(strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
                    if ischar(AccNum)
                        IdxAccNo = ismember(cell2mat(RefAccNo),str2double(AccNum)) & strcmpi(Description,RefDesc);
                    else
                        IdxAccNo = ismember(cell2mat(RefAccNo),AccNum) & strcmpi(Description,RefDesc);
                    end
                else
                    IdxAccNo = ismember(cell2mat(RefAccNo),AccNum) & strcmpi(Description,RefDesc);
                end
            end
            
            if(strcmpi(Type_Name,'orx'))
                try
                    IdxAccNo = cellfun(@(V) ismember(AccNum,V), RefAccNo) & strcmpi(Description,RefDesc);
                catch
                    IdxAccNo = strcmpi(AccNum,RefAccNo) & strcmpi(Description,RefDesc);
                end
            end
            
            Instrument = unique(RefInstrument(IdxAccNo));
            Portfolio = unique(RefPortfolio(IdxAccNo));
            ProductCode = char(unique(RefProdCode(IdxAccNo)));
            BuySell = InData(iR,PosSide);
            if strcmpi(BuySell,'B')
                BuySell = {'Bought'};
            elseif strcmpi(BuySell,'S')
                BuySell = {'Sold'};
            end
            Lots = InData(iR,PosQuantity);
            Month = InData(iR,PosMaturity);
            Maturity = num2cell(NaN);
            Strike = InData(iR,PosStrike);
            OptType = lower(InData{iR,PosProdType});
            Premium = InData(iR,PosTradePrice);
            TradeDate = InData(iR,PosTradeDate);
            ExeBroker = num2cell(NaN);
            ExeType = num2cell(NaN);
            Rates = num2cell(NaN);
            SpclRate = num2cell(NaN);
            
            if strcmpi(Name,'TS')
                TraderName = num2cell(NaN);         %% Add New Column
                TransactionDate_Time = num2cell(NaN);
            else
                TraderName = InData(iR,11);                 %% Add New Column
                TransactionDate_Time = InData(iR,12);
            end
            
            FutureSource = num2cell(NaN);
            if strcmpi(Name,'Broker')
                Source = {''};
                Counterparty = {'WELLS FARGO'};
            elseif strcmpi(Name,'XTR')  && (strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg')) % for traders
                Source = {'XTR Fills'};
                Counterparty = {'WELLS FARGO'};
                ExeBroker = {'WELLS FARGO'};
                ExeType ={'Elec'};
                Rates = {'Normal'};
            elseif strcmpi(Name,'XTR')  && strcmpi(Type_Name,'cfs') % for traders
                Source = {'XTR Fills'};
                Counterparty = {'WELLS FARGO'};
                ExeBroker = {'WELLS FARGO'};
                ExeType ={'Elec'};
                Rates = {'Normal'};
            elseif strcmpi(Name,'XTR')  && strcmpi(Type_Name,'cop') % for traders
                Source = {'XTR Fills'};
                Counterparty = {'WELLS FARGO'};
                ExeBroker = {'WELLS FARGO'};
                ExeType ={'Elec'};
                Rates = {'Normal'};
            elseif strcmpi(Name,'XTR') && strcmpi(Type_Name,'orx') % for traders
                Source = {'XTR Fills'};
                Counterparty = InData(iR,13) ;%% {'MACQUARIE'};
                ExeBroker = InData(iR,13) ;%% {'MACQUARIE'};
                ExeType ={'Elec'};
                Rates = {'Normal'};
                
            else
                Source = {'Traders Snaps'};
                if isnan(InData{iR,PosClearer})
                    ErrorStr = ['Clearer is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    Counterparty = {''};
                else
                    Counterparty = upper(InData(iR,PosClearer));
                    if strcmpi(Counterparty,'Dir Book')
                        Counterparty = unique(RefDirPortfolio(IdxAccNo));
                    end
                end
                if isnan(InData{iR,PosExeBroker})
                    ErrorStr = ['ExeBroker is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    ExeBroker = {''};
                else
                    ExeBroker = upper(InData(iR,PosExeBroker));
                    if strcmpi(ExeBroker,'Dir Book')
                        ExeBroker = unique(RefDirPortfolio(IdxAccNo));
                    end
                end
                if strcmpi(ExeBroker,'XTrader')
                    ExeBroker = {'WELLS FARGO'};
                end
                
                if isnan(InData{iR,PosExeType})
                    ErrorStr = ['ExeType is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    ExeType = {''};
                else
                    ExeType = InData(iR,PosExeType);
                end
                
                if isnan(InData{iR,PosRates})
                    ErrorStr = ['Rates is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    fprintf(fid,'%s\n',ErrorStr);
                    Rates = {''};
                else
                    Rates = InData(iR,PosRates);
                end
                
                SpclRate = InData(iR,PosSpclRate);
                
                
                if isnan(InData{iR,PosFutureSource})
                    %                     ErrorStr = ['FutureSource is empty in Traders Snaps for product ''',ProductCode,''' This column will be assumed as blank in TR dump and in netting.To get the correct dump, fill the correct value and rerun the parser'];
                    %                     fprintf(fid,'%s\n',ErrorStr);
                    FutureSource= {''};
                else
                    FutureSource = InData(iR,PosFutureSource);
                end
                
            end
            
            if strcmpi(OptType,'future')
                OptType = {'Future'};
                Strike = num2cell(NaN);
            else
                if strcmpi(OptType ,'call')
                    OptType = {'Vanilla_Call'};
                elseif strcmpi(OptType ,'put')
                    OptType = {'Vanilla_Put'};
                end
                if ~isempty(Instrument)
                    Instrument = cellstr([char(Instrument),'-O']);
                end
            end
            Temp = num2cell(NaN);
            
            if isempty(Month)
                Month = num2cell(NaN);
            end
            if isempty(Maturity)
                Maturity = num2cell(NaN);
            end
            
            if isempty(Instrument) || isempty(Portfolio)
                InstrErrorStr = ['Portfolio/Instrument information is not available for account ''',num2str(AccNum),''' in Master XLS file. Portfolio/Instrument will be empty for the corresponding trades in TR dump!'];
                fprintf(fid,'%s\n',InstrErrorStr);
                if isempty(Instrument)
                    Matched_AccNum = find(cell2mat(ProductMaster_Temp(:,4))==str2double(AccNum));
                    if(Matched_AccNum)
                        Instrument = ProductMaster_Temp(Matched_AccNum,2); %% Instrument Col
                    else
                        Instrument = num2cell(NaN);
                        %                         Instrument = {''};
                    end
                end
                if isempty(Portfolio)
                    Matched_AccNum = find(cell2mat(ProductMaster_Temp(:,4))==str2double(AccNum));
                    if(Matched_AccNum)
                        Portfolio = ProductMaster_Temp(Matched_AccNum,1); %% Portfolio Col
                    else
                        Portfolio = num2cell(NaN);
                        %                         Portfolio = {''};
                    end
                end
            end
            %%% TODO - BELOW CODE TO BE REMOVED WHEN DEALS ARE ABLE TO BE
            %%% UPLOADED DIRECTLY INTO THE SYSTEM
            try
                TempStrike = cell2mat(Strike);
                TempPremium = cell2mat(Premium);
                if ~isnan(TempStrike) && isnumeric(TempStrike)
                    TempStrike = TempStrike ./ StrikeConv(IdxAccNo);
                end
                if ~isnan(TempPremium) && isnumeric(TempPremium)
                    TempPremium = TempPremium ./ PremiumConv(IdxAccNo);
                end
                if isempty(TempStrike)
                    Strike = num2cell(NaN);
                else
                    Strike = num2cell(TempStrike);
                end
                if isempty(TempPremium) || isnan(TempPremium)
                    Premium = num2cell(0);
                else
                    Premium = num2cell(TempPremium);
                end
            catch
            end
            %%% END OF TODO - CODE TO BE REMOVED
            %             % TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
            %             %             'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
            %             %         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
            %             %         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
            %             %         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            %             %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot'}
            %  NEW HEADER
            %              TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
            %             'OptType','Prem','CounterParty','Initial Lots',...
            %             'CONTRACTNB','Maturity','Barrier Type',...
            %             'Barrier','Transaction Date','TRN.Number','Source',...
            %             'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            %             'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% OLD Lines %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %             IdxDBPortfolio = strcmpi(Portfolio,RefDBSubportfolio);
            %             if strncmpi(Counterparty,'wells',5)
            %                 if (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'DIR'))
            %                     Counterparty = {'WELLS FARGO ARB'};
            %                     ExeBroker = {'WELLS FARGO ARB'};
            %                 elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'TEC'))
            %                     Counterparty = {'WELLS FARGO TEC'};
            %                     ExeBroker = {'WELLS FARGO TEC'};
            %                 elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'QSVT'))
            %                     Counterparty = {'WELLS FARGO QSVT'};
            %                     ExeBroker = {'WELLS FARGO QSVT'};
            %                 end
            %             end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            IdxDBPortfolio = strcmpi(Portfolio,RefDBSubportfolio);
            if strncmpi(Counterparty,'wells',5)
                if (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'DIR'))
                    Counterparty = {'WELLS FARGO ARB'};
                    ExeBroker = {'WELLS FARGO ARB'};
                elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'TEC'))
                    Counterparty = {'WELLS FARGO TEC'};
                    ExeBroker = {'WELLS FARGO TEC'};
                elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'RMS-'))
                    Counterparty = {'WELLS FARGO RMS'};
                    ExeBroker = {'WELLS FARGO RMS'};
                    ContractNB = {'WELLS FARGO RMS'};
                    OTC_Leg = {'EOD'};
                    
                elseif (cellStrfind(RefDBPortfolio(IdxDBPortfolio),'QSVT'))
                    Counterparty = {'WELLS FARGO QSVT'};
                    try
                        if isempty(char(ExeBroker))
                            ExeBroker = {'WELLS FARGO QSVT'};
                        end
                    catch
                    end
                end
            end
            
            % %% OLD ROW DATA
            % %                         RowData  = [Instrument,Portfolio,BuySell,Lots,Month,Strike,...
            % %                             OptType,Premium,Counterparty,Temp,Lots,...
            % %                             Temp,Premium,Temp,Maturity,Temp,Temp,TradeDate,Temp,Temp,Source,Temp,...
            % %                             Temp,Temp,Temp,Temp,ExeBroker,ExeType,Rates,SpclRate,Temp,Temp];
            
            if(exist('OTC_Leg','var'))
                RowData  = [Instrument,Portfolio,BuySell,Month,Strike,...
                    OptType,Premium,Counterparty,Lots,...
                    ContractNB,Maturity,Temp,Temp,TradeDate,Temp,Source,...
                    Temp,Temp,Temp,ExeBroker,ExeType,Rates,SpclRate,FutureSource,OTC_Leg,TraderName,TransactionDate_Time];
                clear ContractNB OTC_Leg;
            else
                RowData  = [Instrument,Portfolio,BuySell,Month,Strike,...
                    OptType,Premium,Counterparty,Lots,...
                    Temp,Maturity,Temp,Temp,TradeDate,Temp,Source,...
                    Temp,Temp,Temp,ExeBroker,ExeType,Rates,SpclRate,FutureSource,Temp,TraderName,TransactionDate_Time];
            end
            
            OutData = [OutData ; RowData]; %#ok<*AGROW>
            
        end
    end

end