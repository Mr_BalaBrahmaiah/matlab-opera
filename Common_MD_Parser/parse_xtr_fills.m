function [OutData,Not_GettingPrice] = parse_xtr_fills(varargin)

Not_GettingPrice = 0;
Master_Path = [pwd filesep 'Master Files' filesep];
OutData = [];

try
    OutData = [];
    MissingAccountNo = {};
    IsError = 0;
    MissingID = [];
    
    if nargin == 1
        XTFileName = varargin{1};
    else
        if nargin > 2
            InPathname = varargin{2};
            Type_Name = varargin{3};
            XTFileName = varargin{4};
        else
            InPathname = '';
            Type_Name = '';
            XTFileName = '';
        end
        %% Excel Path from Main GUI
        %         [FileName,PathName]  = uigetfile('*.csv',['Select the ', upper(Type_Name),' XTR Fills file'],InPathname);
        
        %         if isnumeric(FileName) && FileName==0
        %             errordlg('XTR Fills File not selected!','Missing Deals Parser');
        %             return;
        %         else
        %             XTFileName = fullfile(PathName,FileName);
        %         end
        
    end
    
    if ~isempty(XTFileName)
        Total_Data = [];
        for c = 1 : length(XTFileName)
            Current_XTR_File = char(XTFileName(c));
            [~,~,Current_Data ] =xlsread(Current_XTR_File);
            
            Alt_PriceCol = cellStrfind_Exact(Current_Data(1,:),{'Alt Price'}); %% Remove Extra Header
            if(~isempty(Alt_PriceCol))
                Current_Data(:,Alt_PriceCol) = [];
            end
            
            Header = Current_Data(1,:) ;
            Total_Data = [Total_Data ; Current_Data(2:end,:) ];
        end
        
        RawData = [Header ; Total_Data];
        
    else
        return;
    end
    
    %% XTR Fills Broker Master
    XTR_MasterFile = [Master_Path filesep 'XTRFills_Broker_Alias_Master.xlsx'];
    [~,~,XTR_MasterData] = xlsread(XTR_MasterFile);
    
    XTR_MasterHeader  = XTR_MasterData(1,:);
    XTR_MasterData = XTR_MasterData(2:end,:);
    
    BusinessGroup_Col = cellStrfind_Exact(XTR_MasterHeader,{'BusinessGroup'});
    Broker_Mas_Col = cellStrfind_Exact(XTR_MasterHeader,{'Broker'});
    Alias_Mas_Col = cellStrfind_Exact(XTR_MasterHeader,{'Alias'});
    
    Matched_BU = cellStrfind_Exact(XTR_MasterData(:,BusinessGroup_Col),{Type_Name});
    if(~isempty(Matched_BU))
        MasterBroker_Data = XTR_MasterData(Matched_BU,Broker_Mas_Col)  ;
        MasterAlias_Data = XTR_MasterData(Matched_BU,Alias_Mas_Col)  ;
    end
    
    
    %% Fetching AGRIFUNDS Rows Only for AGF
    if(strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'cfs') || strcmpi(Type_Name,'cop') || strcmpi(Type_Name,'orx') || strcmpi(Type_Name,'usg'))
        RawData_Header = RawData(1,:);
        RowData = RawData(2:end,:);
        
        Broker_Col = cellStrfind_Exact(RawData_Header,{'Broker'});
        Alias_Col = cellStrfind_Exact(RawData_Header,{'Alias'});
        
        Matched_Data = [];
        for i = 1 : length(MasterBroker_Data)
            Current_Broker = MasterBroker_Data(i);
            Current_Alias = MasterAlias_Data(i);
            
            if(strcmpi(Current_Alias,'all'))
                MatchedIndex = strncmpi(RowData(:,Broker_Col),Current_Broker,length(char(Current_Broker))) & cell2mat( cellfun(@(x)(isnumeric(x)),RowData(:,Alias_Col),'UniformOutput',false) );
            else
                MatchedIndex = strncmpi(RowData(:,Broker_Col),Current_Broker,length(char(Current_Broker))) & strncmpi(RowData(:,Alias_Col),Current_Alias,length(char(Current_Alias)));
            end
            
            Matched_Data = [Matched_Data ;  RowData(MatchedIndex,:) ];
            
        end
        
        RawData = [RawData_Header ; Matched_Data];
        
        if(isempty(Matched_Data))
            msgbox(['No ',char(Type_Name),' Trades Corresponding Date '],'parse XTR Fills');
            return;
        end
        
        
    end
    
    %% Fetching ORYX Rows Only for ORX
    %     if(strcmp(Type_Name,'orx'))
    
    %%**********************************************************************************
    %         RawData_Header = RawData(1,:);
    %         RowData = RawData(2:end,:);
    %
    %         Alias_Col = find(cellfun(@(V) strcmpi('Alias',V), RawData_Header)) ;
    %         Alias_Data = RowData(:,Alias_Col);
    %         Index = cellStrfind(Alias_Data,{'ORYX'});
    %
    %         RowData = RowData(Index,:);  %% Only ORYX Fileds
    
    %         AccountNumber_Col = find(cellfun(@(V) strcmpi('Account Number',V), RawData_Header)) ;
    %         AccountNumber_Data = RowData(:,AccountNumber_Col);
    %         %         Unique_AccountNumber = unique(cell2mat(AccountNumber_Data)) ;
    %
    %         [~,~,AccNumMapping] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'Broker Mapping');
    %         AccNum_Mapping_Header = AccNumMapping(1,:);
    %         AccNum_Mapping_Data = AccNumMapping(2:end,:);
    %
    %         AccountNumber_Header = find(cellfun(@(V) strcmpi('Account Number',V), AccNum_Mapping_Header)) ;
    %         CounterParty_Header = find(cellfun(@(V) strcmpi('CounterParty',V), AccNum_Mapping_Header)) ;
    %         Company_Header = find(cellfun(@(V) strcmpi('Company',V), AccNum_Mapping_Header)) ;
    %
    %
    %         Counter_Party = cell(size(AccountNumber_Data,1),1) ;
    %         Company = cell(size(AccountNumber_Data,1),1) ;
    %         for i = 1 : size(AccNum_Mapping_Data,1)
    %             %             Current_AccountNumber = cell2mat(AccNum_Mapping_Data(i,AccountNumber_Header));
    %             %             Matched_Index = find(cellfun(@(V) ismember(Current_AccountNumber,V), AccountNumber_Data));
    %
    %             Current_AccountNumber = num2str(cell2mat(AccNum_Mapping_Data(i,AccountNumber_Header))) ;%% AccNum_Mapping_Data(i,AccountNumber_Header);
    %             Numeric_Index = cell2mat(cellfun(@(x)(isnumeric(x)),AccountNumber_Data,'UniformOutput',false));
    %             AccountNumber_Data(Numeric_Index) = cellfun(@(x)(num2str(x)),AccountNumber_Data(Numeric_Index),'UniformOutput',false);
    %
    %             Matched_Index = strcmpi(char(Current_AccountNumber),AccountNumber_Data);
    %             if(~isempty(Matched_Index))
    %
    %                 Counter_Party(Matched_Index,1) = AccNum_Mapping_Data(i,CounterParty_Header);
    %                 Company(Matched_Index,1) = AccNum_Mapping_Data(i,Company_Header);
    %
    %             end
    %         end
    %
    %         RawData_Header = [RawData_Header,'Broker','Company'];
    %         RowData = [RowData,Counter_Party,Company];
    
    %         RawData = [RawData_Header ; RowData];
    
    %     end
    
    %%
    %     hWaitbar = waitbar(0.1,'Please wait, extracting data from XTR fills file ...');
    
    Header = RawData(1,:);
    
    Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
    MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
    
    PosProdType = strcmpi('Fill Type',Header);
    ProdType = RawData(:,PosProdType);
    IdxProdType = strcmpi(ProdType, 'future') | strcmpi(ProdType, 'future leg');
    Data = RawData(IdxProdType,:);
    Data(:,PosProdType) = {'future'};
    
    PosProduct     = strcmpi('Product',Header);
    PosTradeDate   = strcmpi('Exch Date',Header);
    PosSide        = strcmpi('B/S',Header);
    PosQuantity    = strcmpi('Qty',Header);
    PosYearMonth   = strcmpi('Contract',Header);
    PosStrike      = strcmpi('Strike',Header);
    PosTradePrice  = strcmpi('Price',Header);
    PosXTRAccount  = strcmpi('Account Number',Header);
    PosTraderName  = strcmpi('Trd ID',Header);
    PosOrderDate  = strcmpi('Order Date',Header);
    PosOrderTime  = strcmpi('Order Time',Header);
    
    Product       = Data(:,PosProduct); % to get acc no
    Side          = Data(:,PosSide);
    Quantity      = Data(:,PosQuantity);
    
    Strike        = Data(:,PosStrike);
    TradePrice    = Data(:,PosTradePrice);
    ProductType   = Data(:,PosProdType);
    XTRAccount    = Data(:,PosXTRAccount);
    
    TraderName    = Data(:,PosTraderName);
    OrderDate    = Data(:,PosOrderDate);
    OrderTime    = Data(:,PosOrderTime);   %% HH:MM:SS:XXX
    %     OrderTime = cellfun(@(x) x(1:end-4), OrderTime, 'UniformOutput', false); %% HH:MM:SS
    TransactionDate_Time = strcat(OrderDate,{' '},OrderTime);
    
    
    PosCounterParty = strcmpi('Broker',Header);   %% Get Broker Column for Counter Party
    CounterParty = Data(:,PosCounterParty);
    if(~isempty(CounterParty))
        WellsFargo_Index = (~cellfun(@isempty,regexp(CounterParty,'Wells Fargo Securities LLC')));
        CounterParty(WellsFargo_Index) = {'Wells Fargo'};
    end
    
    %% Read Missing Deals Master File
    if(strcmpi(Type_Name,'agf'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(Type_Name,'orx'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(Type_Name,'cfs'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(Type_Name,'usg'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    else
        [~,~,AccProdMapping] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    end
    
    %%  Remove NaN Fields
    Header = AccProdMapping(1,:);
    NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
    AccProdMapping(:,NaN_Col(:)) = [];
    
    Row_find_Nan = AccProdMapping(:,4);
    NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
    AccProdMapping(NaN_Row(:),:) = [];
    
    %%
    AccProdMapping = AccProdMapping(2:end,:);
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN         = cellfun(@isnan,AccProdMapping(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    AccProdMapping(IdxNaNRows,:) = [];
    
    AccNumber = AccProdMapping(:,1);
    XTRProductCode = AccProdMapping(:,2);
    Desc    = AccProdMapping(:,3);
    PremDivBy8 = cell2mat(AccProdMapping(:,4));
    PriceConversion = AccProdMapping(:,5);
    RefXTRAccNum =  AccProdMapping(:,6);
    
    NewTradePrice = TradePrice;
    Lots = cell2mat(Quantity);
    
    %%% Old logic  %% Other Systems
    if isdeployed
        YearMonth = cellstr(datestr(Data(:,PosYearMonth),'dd-mm-yyyy'));
    else
        YearMonth = Data(:,PosYearMonth);
    end
    NewYM = char(YearMonth);
    TempYearMonth = cellstr(datestr(cellstr(NewYM(:,1:end-5)),'mmm-yy'));
    
    %%% Newly added code %% run within MATLAB environment
%     YearMonth = Data(:,PosYearMonth);
%     TempYearMonth = cellstr(datestr(datenum(YearMonth,'dd-mm-yyyy'),'mmm-yy'));
    %%% End code
    
    Maturity = cell(size(TempYearMonth));
    for i = 1:length(TempYearMonth)
        YrMon = TempYearMonth{i};
        ProdCode = Product{i};
        if(strcmpi(Type_Name,'orx'))
            try
                IdxProd = cellfun(@(V) ismember(XTRAccount{i},V), RefXTRAccNum) & strcmpi(ProdCode,XTRProductCode);
            catch
                IdxProd = strcmpi(XTRAccount{i},RefXTRAccNum) & strcmpi(ProdCode,XTRProductCode);
            end
        else
            IdxProd = strcmpi(XTRAccount{i},RefXTRAccNum) & strcmpi(ProdCode,XTRProductCode);
        end
        
        if isempty(find(IdxProd))
            MissingAccountNo = [MissingAccountNo,XTRAccount{i}];
            MissingID = [MissingID,i];
            IsError = 1;
            
            Maturity(i,1) = {''};
            AccountNumber(i,1) = {''};
            Description(i,1) = {''};
            YearMonthCode(i,1) = {''};
            NewTradePrice{i} = 0;
            continue;
        end
        YearMonthCode(i,1) = cellstr([MonthCode{strncmpi(YrMon,Month,3)},YrMon(end)]);
        Maturity(i,1) = cellstr([YearMonthCode{i,1},'.',YearMonthCode{i,1}]);
        AccountNumber(i,1) = AccNumber(IdxProd); %#ok<*AGROW>
        Description(i,1) = Desc(IdxProd);
        if PremDivBy8(IdxProd)
            Temp   = num2str(TradePrice{i});
            TempNo = str2num(Temp(1:end-1)); %#ok<*ST2NM>
            NewTradePrice{i} = TempNo + (str2num(Temp(end))/8);
        end
        NewTradePrice{i} = NewTradePrice{i} * PriceConversion{IdxProd};
        if strcmpi(Side(i),'S')
            Lots(i) = -(Lots(i));
        end
    end
    
    if isdeployed
        TradeDate  = cellstr(datestr(Data(:,PosTradeDate),'dd-mm-yyyy'));
    else
        TradeDate  = Data(:,PosTradeDate);
    end
    
    %%
    if IsError
        
        if(strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
            ErrorAccnum = unique(MissingAccountNo);
            
            if(~isempty(ErrorAccnum))
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(in XTR-Fills Conversion and in XTR-TR Format Master sheets) to get these account deals'],upper(Type_Name));
            else
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(Type_Name));
            end
            
        end
        
        if(strcmpi(Type_Name,'orx'))
            ErrorAccnum = unique(cell2mat(MissingAccountNo));
            
            if(~isempty(ErrorAccnum))
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file contain the Account Number ',num2str(ErrorAccnum),' which is not available in Master file. Please update this account number in master file(ORYX Missing Deals Master) to get these account deals'],upper(Type_Name));
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file NEW PRODUCT FOUND ',unique(Product(MissingID))],upper(Type_Name));
            else
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(Type_Name));
            end
        end
        
        if(strcmpi(Type_Name,'cfs'))
            ErrorAccnum = unique(MissingAccountNo);
            
            if(~isempty(ErrorAccnum))
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(Type_Name));
                % warndlg([upper(Type_Name),' XTR Fills file NEW PRODUCT FOUND ',unique(Product(MissingID))],upper(Type_Name));
            else
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(Type_Name));
            end
        end
        
        if(strcmpi(Type_Name,'cop'))
            ErrorAccnum = unique(MissingAccountNo) ;%% unique(cell2mat(MissingAccountNo));
            
            if(~isempty(ErrorAccnum))
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(COP Missing Deals Master) to get these account deals'],upper(Type_Name));
                % warndlg([upper(Type_Name),' XTR Fills file NEW PRODUCT FOUND ',unique(Product(MissingID))],upper(Type_Name));
            else
                warndlg([upper(Type_Name),' FUTURE-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(Type_Name));
            end
        end
        
        TradeDate(MissingID) = [];
        Side(MissingID) = [];
        Lots(MissingID) = [];
        Strike(MissingID) = [];
        ProductType(MissingID) = [];
        YearMonthCode(MissingID) = [];
        Maturity(MissingID) =  [];
        AccountNumber(MissingID) =  [];
        Description(MissingID) = [];
        NewTradePrice(MissingID) =  [];
        XTRAccount(MissingID) = [];
        CounterParty(MissingID) = [];
        
        TraderName(MissingID) = [];
        TransactionDate_Time(MissingID) = [];
        
    else
        warndlg([upper(Type_Name),' FUTURE-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',], upper(Type_Name));
        
    end
    %%
    if(~isempty(AccountNumber) || ~isempty(XTRAccount))
        
        OutData      = [AccountNumber, TradeDate, Side, num2cell(Lots), YearMonthCode, Description, Strike, ProductType, NewTradePrice,Maturity,TraderName,TransactionDate_Time,XTRAccount,CounterParty];
        OutputHeader = {'AccountNumber', 'TradeDate', 'Side', 'Quantity', 'YearMonth', 'Description', 'Strike', 'ProductType',...
            'TradePrice','Maturity','Trader Name','Transaction Date Time','XTRAccountNumber','CounterParty'};
        
        Output       = [OutputHeader;OutData];
        
        %% Change the Price Value for SET Product Only  [( InputPrice * TickSize ) + SettleValue ]
        
        if(strcmpi(Type_Name,'orx'))
            [RawData_Header , RowData , OutputHeader , OutData,Not_GettingPrice] = change_price_value_4_set_products(RawData,Output);
        else
            Not_GettingPrice = 0;
        end
        
        %%
        %         waitbar(1,hWaitbar);
        %         if exist('TradeDate','var')
        %             OutXLSFileName = [upper(Type_Name),'_XTR_Fills_',TradeDate{1},'.xlsx'];
        %         else
        %             OutXLSFileName = [upper(Type_Name),'_XTR_Fills_',datestr(today,'ddmmyyyy'),'.xlsx'];
        %         end
        % OutXLSFileName = strrep(OutXLSFileName,'/','_');
        
        if(~exist('OutXLSFileName','var'))
            OutXLSFileName = getXLSName([upper(Type_Name),'_XTR_Fills']);
        end
        
        xlswrite(OutXLSFileName,[OutputHeader ; OutData],'XTR_Fills');  %% Output
        OutXLSFileName = fullfile(pwd,OutXLSFileName);
        try
            xls_delete_sheets(OutXLSFileName);
        catch
        end
        
        %         msgbox([upper(Type_Name),' XTR Fills statement is saved in ',char(OutXLSFileName)],'Missing Deals Parser');
        
    else
        if(strcmpi(Type_Name,'agf') || strcmpi(Type_Name,'usg'))
            msgbox('No AGF Trades Corresponding Date ','parse XTR Fills');
        end
        if(strcmpi(Type_Name,'orx'))
            msgbox('No ORX Trades Corresponding Date ','parse XTR Fills');
        end
        if(strcmpi(Type_Name,'cfs'))
            msgbox('No CFS Trades Corresponding Date ','parse XTR Fills');
        end
        if(strcmpi(Type_Name,'cop'))
            msgbox('No COP Trades Corresponding Date ','parse XTR Fills');
        end
    end
    
    %     waitbar(1,hWaitbar);
    %     delete(hWaitbar);
    
catch ME
    errordlg(ME.message,'Missing Deals Parser');
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end
end


