function [OutErrorMsg] =  Barrier_EOD_Report_Exe()

try
    OutErrorMsg = {'No Errors'};
    
    %%%
    Prompt        = {'Enter the BU Name:'};
    Name          = 'Inputs for Barrier EOD Report';
    Numlines      = 1;
    Defaultanswer = {'CFS'};  %%% default BU is 'CFS"
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('BU input was not given by user!');
        return;
    end
    
    InBUName = lower(InputDates(1));
    
    %%Fetch data from view
    ViewName = 'helper_barrier_underlying_view';
    [Security_Info_Table_Cols,Security_Info_Table_Data] = read_from_database(ViewName,0,'',char(InBUName));

    if strcmpi(Security_Info_Table_Data,'No Data') 
       errordlg ({['Data not Available in ',strcat(ViewName,'_',char(InBUName))]}) ;
       return;
    end
    
    XLSFileName = getXLSFilename(['barrier_underlying_report_',char(InBUName)]) ;
        
    xlswrite(XLSFileName,[Security_Info_Table_Cols;Security_Info_Table_Data],'Underlying_id-Data');
    xls_delete_sheets([pwd filesep XLSFileName]);
        
    msgbox('Process Complete','Finish');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end