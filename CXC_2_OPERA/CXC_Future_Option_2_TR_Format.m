function [TRHeader , RowData ,Market_Price_Index_Error ,Maturity_Error_Data] = CXC_Future_Option_2_TR_Format(CXC_Data , CXC_Action_Str)

    Market_Price_Index_Error = '';
    Maturity_Error_Data = '';

    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from vol_id_table
    SqlQuery = 'select vol_id,bbg_opt_ticker,expiry_date from vol_id_table';
    [~,DBData] = read_from_database('vol_id_table',0,SqlQuery);
    if strcmpi(DBData,'No Data') 
        warndlg('Data not Available in vol_id_table');
        return;
    end
    DBVolID = DBData(:,1);
    DBBBGOptTicker = DBData(:,2);
    DBExpiryDate = cellstr(datestr(datenum(DBData(:,3),'yyyy-mm-dd'),'yyyy'));

    %%% Fetch Data from cxc_instrument_mapping_table
    Instrument_Mapping_Table_Name = 'cxc_instrument_mapping_table';
    sql_cxc_instrument_mapping = ['select * from ',Instrument_Mapping_Table_Name,];
    [cxc_instrument_mapping_Cols,cxc_instrument_mapping_Data] = Fetch_DB_Data(ObjDB,sql_cxc_instrument_mapping);

    if strcmpi(cxc_instrument_mapping_Data,'No Data') 
        warndlg({['Data not Available in ',Instrument_Mapping_Table_Name,]}) ;
        return;
    end

    cxc_instrument_mapping_Tab_Data = cell2table(cxc_instrument_mapping_Data,'VariableNames',cxc_instrument_mapping_Cols);  %% Convert to table

    %%% Check if the Data is avaliable or not
    if (size(CXC_Data,1) >= 1)
        %%% Mapping the Instrument
        Index_instrument = ismember(CXC_Data.MARKET_PRICE_INDEX,cxc_instrument_mapping_Tab_Data.market_prc_index);
        CXC_View_Data = CXC_Data(Index_instrument,:);  %%% Final excat data

        %%% Missing Market_Price_Index List
        Market_Price_Index_Error = unique(CXC_Data.MARKET_PRICE_INDEX(~Index_instrument));  %%% 

        %%% get the user required columns
        % Portfolio
        Portfolio = CXC_View_Data.PROFIT_CENTER;
        
        % Buy_Sell
        LONG_SHORT = CXC_View_Data.LONG_SHORT;
        Bounght_Index = cellStrfind_exact(LONG_SHORT,{'Long'}) ;
        Sold_Index = cellStrfind_exact(LONG_SHORT,{'Short'}) ;
        Buy_Sell(Bounght_Index,1) = {'Bought'};
        Buy_Sell(Sold_Index,1) = {'Sold'};
        
        % InitialLot
        InitialLot_Data = CXC_View_Data.NO_OF_LOTS;
        InitialLot_Data(Bounght_Index,1) = (InitialLot_Data(Bounght_Index) .* 1);   
        InitialLot_Data(Sold_Index,1) = (InitialLot_Data(Sold_Index) .* -1);
        
        %OptType
        OptType = CXC_View_Data.CALL_PUT;
        Fut_Index = cellStrfind_exact(OptType,{''}) ;
        Call_Index = cellStrfind_exact(OptType,{'CALL'}) ;
        Put_Index = cellStrfind_exact(OptType,{'PUT'}) ;
        OptType(Fut_Index)  = {'Future'} ;
        OptType(Call_Index)  = {'vanilla_call'} ;
        OptType(Put_Index)  = {'vanilla_put'} ;

        %%% get the Instrument and Maturity_Code
        [row,~] = size(CXC_View_Data);
        Maturity_Error_Data = [];
        for i=1:1:row
            Matched_MP_Index = strcmpi(CXC_View_Data.MARKET_PRICE_INDEX(i) ,cxc_instrument_mapping_Tab_Data.market_prc_index);
            instrument_mapping_data = cxc_instrument_mapping_Tab_Data(Matched_MP_Index,:);

            Instrument(i,1) = instrument_mapping_data.instrument;
            price_conversion(i,1) = instrument_mapping_data.price_conversion;
            bbg_product_code = instrument_mapping_data.bbg_product_code;

            call_put = CXC_View_Data.CALL_PUT(i);
            if strcmpi(call_put,'')
                Maturity_Code(i,1) = strcat(CXC_View_Data.TMON(i),'.',CXC_View_Data.TMON(i)); 

            elseif (strcmpi(call_put,'call') || strcmpi(call_put,'put'))
                EXPIRY_DATE = CXC_View_Data.EXPIRY_DATE(i);
                Current_Expiry_Date = cellstr(datestr(datenum(EXPIRY_DATE,'yyyy-mm-dd'),'yyyy'));
                Month_Code = CXC_View_Data.TMON(i);
                cxc_bbg_ticker = strcat(bbg_product_code,Month_Code);

                BBG_Opt_Ticker_Index = find(strcmpi(cxc_bbg_ticker,DBBBGOptTicker) & strcmpi(Current_Expiry_Date,DBExpiryDate));

                if(~isempty(BBG_Opt_Ticker_Index))
                    Vol_ID = DBVolID(BBG_Opt_Ticker_Index , 1);
                    Split_Str =strsplit(char(Vol_ID),' ');  
                else
                    Split_Str = cxc_bbg_ticker;
                    Maturity_Error_Data = [Maturity_Error_Data ;cellstr(strcat(cxc_bbg_ticker,' CXC_BBG_Ticker not available in vol_id_table '))];
                end
                Maturity_Code(i,1) =  Split_Str(end) ;
            end
        end
        
    % Instrument
    instrument = Instrument;
    instrument(Call_Index) = strcat(instrument(Call_Index),'-O');
    instrument(Put_Index) = strcat(instrument(Put_Index),'-O');
    % Strike
    Strike = CXC_View_Data.STRIKE_PRICE;
    Strike(Fut_Index) = Strike(Fut_Index);
    Strike(Call_Index) = Strike(Call_Index) ./ price_conversion(Call_Index);
    Strike(Put_Index) = Strike(Put_Index) ./ price_conversion(Put_Index);
    %Premimum_Data
    Prem_Data = CXC_View_Data.UNIT_PRICE ./ price_conversion;

    %%% get the data for CounterParty&ExeBroker, TransactionDate,
    %%% Traded_DateTimestamp, Trader_Name, UniqueNumber and CounterpartyEntity
    CounterParty_Data = CXC_View_Data.BROKER;
    TransactionDate = CXC_View_Data.CONTRACT_DATE;
    Traded_DateTimestamp = CXC_View_Data.LASTUPDATE; %% datestr(CXC_View_Data.LASTUPDATE+datenum('30-Dec-1899'))
    Trader_Name = CXC_View_Data.OPERATION_USER;
    UniqueNumber_Data = CXC_View_Data.CONTRACT_REFERENCE; 
    CounterpartyEntity_Data = CXC_View_Data.CTR_BOOK; 
    ExeBroker_Data = CounterParty_Data;

    %%% Hard Code fields
    Source = repmat({'CXC'},size(CXC_View_Data,1),1); 
    ExeType = repmat({'Elec'},size(CXC_View_Data,1),1); 
    Rates = repmat({'Normal'},size(CXC_View_Data,1),1); 
    CXC_Action_Data = repmat(CXC_Action_Str,size(CXC_View_Data,1),1); %% CXC_Action is either NEW or MOD
 
    %%% Just fill with empty data
    TRN_Number = cell(size(CXC_View_Data,1),1);
    Parent_Trans_Number = cell(size(CXC_View_Data,1),1);
    Month1_code = cell(size(CXC_View_Data,1),1);
    CONTRACTNB = cell(size(CXC_View_Data,1),1);
    Maturity = cell(size(CXC_View_Data,1),1);
    Barrier_Type = cell(size(CXC_View_Data,1),1);
    Barrier = cell(size(CXC_View_Data,1),1);
    Averaging_Start_Date = cell(size(CXC_View_Data,1),1);
    Averaging_End_Date = cell(size(CXC_View_Data,1),1);
    Spcl_Rate_lot = cell(size(CXC_View_Data,1),1);
    Other_Remarks = CXC_View_Data.CONTRACT_STATUS;
    BrokerLot_Data = cell(size(CXC_View_Data,1),1);
    OTC_DAILY_EOD_LEG = cell(size(CXC_View_Data,1),1);

    %%% Required Header
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
        'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
    %%% Required Data
    RowData = [instrument, Portfolio, Buy_Sell, Maturity_Code, Month1_code, num2cell(Strike), OptType, num2cell(Prem_Data), CounterParty_Data,num2cell(InitialLot_Data), ...
                CONTRACTNB, Maturity, Barrier_Type, Barrier, TransactionDate,TRN_Number, Source, Averaging_Start_Date, Averaging_End_Date, ...
                Parent_Trans_Number, ExeBroker_Data, ExeType, Rates, Spcl_Rate_lot, Other_Remarks, OTC_DAILY_EOD_LEG, Trader_Name, Traded_DateTimestamp, ...
                UniqueNumber_Data, BrokerLot_Data, cellstr(CXC_Action_Data), CounterpartyEntity_Data] ;
    
    else %% if Data is empty
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
            'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
            'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};

        RowData = [];
    end
end
