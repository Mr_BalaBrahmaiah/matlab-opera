function varargout = CXC_2_OPERA(varargin)
% CXC_2_OPERA MATLAB code for CXC_2_OPERA.fig
%      CXC_2_OPERA, by itself, creates a new CXC_2_OPERA or raises the existing
%      singleton*.
%
%      H = CXC_2_OPERA returns the handle to a new CXC_2_OPERA or the handle to
%      the existing singleton*.
%
%      CXC_2_OPERA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CXC_2_OPERA.M with the given input arguments.
%
%      CXC_2_OPERA('Property','Value',...) creates a new CXC_2_OPERA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CXC_2_OPERA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CXC_2_OPERA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only
%      onek
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CXC_2_OPERA

% Last Modified by GUIDE v2.5 26-Apr-2018 14:56:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CXC_2_OPERA_OpeningFcn, ...
    'gui_OutputFcn',  @CXC_2_OPERA_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before CXC_2_OPERA is made visible.
function CXC_2_OPERA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CXC_2_OPERA (see VARARGIN)

% Choose default command line output for CXC_2_OPERA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

                %%%% CXC Project Program %%%%
%%% connect to CXC database
ObjDB_MS_SQL = connect_to_database_MS_SQL ;
MS_SQL_DB_Message = ObjDB_MS_SQL.Message;
if(isempty(MS_SQL_DB_Message))
    MS_SQL_DB_Message = 'MS_SQL Database is Connected';
end
set(handles.text11,'String',MS_SQL_DB_Message);

%%% Get system date
[System_Date_Format,~] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

%%% get the Opera date (Today Date - last 3 months date)
HolDates = {'2016-01-01','2017-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));
Opera_Date_Str = datestr(busdate(today-105,-1,HolidayVec),1);

%%% CXC date (StartDate and EndDate)
[CXC_StartDate_Str,CXC_EndDate_Str,~] = Get_User_Want_Business_Days([],2) ; %% NoDaysShift Default 2
CXC_StartDate_Str = datestr(datenum(CXC_StartDate_Str,DB_Date_Format),1);
CXC_EndDate_Str = datestr(datenum(CXC_EndDate_Str,DB_Date_Format),1);

%%% Set handles
set(handles.edit1,'String',Opera_Date_Str);
set(handles.edit2,'String',CXC_StartDate_Str);
set(handles.edit3,'String',CXC_EndDate_Str);

set(handles.edit1,'Enable','off');
set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group

handles.Opera_Date_Str = Opera_Date_Str;
handles.CXC_StartDate_Str = CXC_StartDate_Str;
handles.CXC_EndDate_Str = CXC_EndDate_Str;

Live_Only = get(handles.radiobutton1,'Value');
handles.Live_Only = Live_Only;

Live_Dead = get(handles.radiobutton2,'Value');
handles.Live_Dead = Live_Dead;

From_Database = get(handles.radiobutton5,'Value');
handles.From_Database = From_Database;

From_Excel = get(handles.radiobutton6,'Value');
handles.From_Excel = From_Excel;

guidata(hObject,handles);

% --- Outputs from this function are returned to the command line.
function varargout = CXC_2_OPERA_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

PLM = get(handles.checkbox1,'Value');
if(PLM)
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
else
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
end

handles.PLM = PLM;
guidata(hObject,handles);

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

RBR = get(handles.checkbox2,'Value');
if(RBR)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox3,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox3,'Enable','on');
end

handles.RBR = RBR;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

EDL = get(handles.checkbox3,'Value');
if(EDL)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
end

handles.EDL = EDL;
guidata(hObject,handles);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

CXC_StartDate_Str = get(handles.edit2,'String');
handles.CXC_StartDate_Str = CXC_StartDate_Str;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

CXC_EndDate_Str = get(handles.edit3,'String');
handles.CXC_EndDate_Str = CXC_EndDate_Str;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global PLM;global RBR; global EDL;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[jObj] = Job_Start();
drawnow;

From_Database = get(handles.radiobutton5,'Value');
From_Excel = get(handles.radiobutton6,'Value');

ObjDB = connect_to_database;

%%% get the selected Date's by user
Opera_Date_Str = handles.Opera_Date_Str;
Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');
CXC_StartDate_Str = handles.CXC_StartDate_Str;
CXC_StartDate_Str = datestr(CXC_StartDate_Str,'yyyy-mm-dd');
CXC_EndDate_Str = handles.CXC_EndDate_Str;
CXC_EndDate_Str = datestr(CXC_EndDate_Str,'yyyy-mm-dd');

%%% get the selected BU by user
PLM = get(handles.checkbox1,'Value');
RBR = get(handles.checkbox2,'Value');
EDL = get(handles.checkbox3,'Value');

if(datenum(CXC_StartDate_Str))
    %%Only Run Fast Seven Days
    if(PLM || RBR || EDL)
        
        BU_Names = {'plm','rbr','edl'} ; %% Hard Code
        User_Select_Portfolio = {PLM,RBR,EDL};
        Selected_BU_Name = BU_Names(logical(cell2mat(User_Select_Portfolio)));
        
        %%% fetch data from cxc_book_mapping_table
        book_mapping_Table_Name = 'cxc_book_mapping_table';
        sql_cxc_bk_mapping = ['select * from ',book_mapping_Table_Name,' where opera_bu = ''',char(Selected_BU_Name),''' '];
        [cxc_bk_mapping_Cols,cxc_bk_mapping_Data] = Fetch_DB_Data(ObjDB,sql_cxc_bk_mapping);

        if strcmpi(cxc_bk_mapping_Data,'No Data') 
            warndlg({['Data not Available in ',book_mapping_Table_Name,]}) ;
            return;
        end

        cxc_bk_mapping_Tab_Data = cell2table(cxc_bk_mapping_Data,'VariableNames',cxc_bk_mapping_Cols);  %% Convert to table

        %%% fetch data from cxc_ignored_market_product_table
        Ignored_Market_Product_Table_Name = 'cxc_ignored_market_product_table';
        sql_cxc_ignored_market_product = ['select * from ',Ignored_Market_Product_Table_Name,];
        [ignored_market_product_Cols,ignored_market_product_Data] = Fetch_DB_Data(ObjDB,sql_cxc_ignored_market_product);

        if strcmpi(ignored_market_product_Data,'No Data') 
            warndlg({['Data not Available in ',Ignored_Market_Product_Table_Name,]}) ;
            return;
        end

        ignored_market_product_Tab_Data = cell2table(ignored_market_product_Data,'VariableNames',ignored_market_product_Cols);  %% Convert to table

        ctr_book_sql = ['''',strrep(convertCell2Char(cxc_bk_mapping_Tab_Data.cxc_book),',',''','''),''''];
        ignored_marketP= ['''',strrep(convertCell2Char(ignored_market_product_Tab_Data.market_price_index),',',''','''),''''];
        
        %%% Find the minimum date
        [Opera_Date_Str] = Find_Minimum_CXC_ViewDate(ctr_book_sql,ignored_marketP,CXC_StartDate_Str,CXC_EndDate_Str) ;
        set(handles.edit1,'String',Opera_Date_Str);
        Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');

        %%% get Output File name
        XLS_Name = strcat('CXC_2_OPERA_TR_Format_',upper(char(Selected_BU_Name)));
        OutXLSName = getXLSFilename(XLS_Name);
        xlswrite(OutXLSName,{''},'CXC_2_OPERA_TR_Format');
        
        %%% SQL Query's for deal_ticket_table view and CXC view's
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',char(Selected_BU_Name));
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Opera_Date_Str ,''' '];
        %SqlQuery_2 = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where ctr_book in (',ctr_book_sql,') and market_price_index not in (',ignored_marketP,') and ((CONTRACT_DATE >= ''',CXC_StartDate_Str,''' and CONTRACT_DATE <= ''',CXC_EndDate_Str,''') or (LASTUPDATE >= ''',CXC_StartDate_Str,''' and LASTUPDATE <= ''',CXC_EndDate_Str,'''))'] ;
        SqlQuery_2 = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where ctr_book in (',ctr_book_sql,') and market_price_index not in (',ignored_marketP,') and ((cast(CONTRACT_DATE as date) >= ''',CXC_StartDate_Str,''' and cast(CONTRACT_DATE as date) <= ''',CXC_EndDate_Str,''') or (cast(LASTUPDATE as date) >= ''',CXC_StartDate_Str,''' and cast(LASTUPDATE as date) <= ''',CXC_EndDate_Str,'''))'] ;
        SqlQuery_3 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

        %%% Fetch data from View's using above sql query's
        [DBFieldNames,DBData] = read_from_database(0,0,SqlQuery_1);
        if(From_Database)
            [CXC_FieldNames,CXC_Data] = read_from_database_MS_SQL([],SqlQuery_2);
        else %% if (From_Excel)
            %%% Read the CXC DUMP Input file from System (manually)
            [FileName,PathName] = uigetfile('*.*','Select the CXC Dump File');
            Excel_Path = [PathName,FileName];
            [~,~,CXCRawData] = xlsread(char(Excel_Path));  %% CXC Dump Input File
            CXC_FieldNames = CXCRawData(1,:);
            CXC_Data = CXCRawData(2:end,:);
        end
        
        [DBFieldNames_Del,DBData_Del] = read_from_database(0,0,SqlQuery_3); 
        
        if( size(DBData,2) > 1 )
            if(~strcmpi('No Data',CXC_Data ))
                %%% CXC Main Program Logic
                CXC_View_Data = cell2table(CXC_Data,'VariableNames',CXC_FieldNames);  %% Convert to table
                %%% Only taking the Confirmed and Fulfilled data
                Ind_Confirm_Fulfil = strcmpi(CXC_View_Data.CONTRACT_STATUS,'Confirmed') | strcmpi(CXC_View_Data.CONTRACT_STATUS,'Fulfilled');
                CXC_View_Data = CXC_View_Data(Ind_Confirm_Fulfil,:);
                
                if ~isempty(CXC_View_Data)
                    %%Compare CXC DB and OUR DB %% Get New Trades
                    OUR_UniqueNumber_Col = cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames) ;
                    CXC_UniqueNumber_Col = cellfun(@(V) strcmpi('CONTRACT_REFERENCE',V), CXC_FieldNames) ;
                    TradeID_Col = cellfun(@(V) strcmpi('trade_id',V), DBFieldNames) ;

                    OUR_Unique_Data  = DBData(:,OUR_UniqueNumber_Col);
                    CXC_Unique_Data = CXC_View_Data(:,CXC_UniqueNumber_Col);
                    
                    [Matched_UniqueNumber ,Location] = ismember(CXC_Unique_Data.CONTRACT_REFERENCE,OUR_Unique_Data);
                    CXC_Old_Data = CXC_View_Data(Matched_UniqueNumber,:);
                    CXC_New_Data = CXC_View_Data(~Matched_UniqueNumber,:);
                    if any(Matched_UniqueNumber)
                        Existing_TradeId = DBData(nonzeros(Location),TradeID_Col);
                    end
                else
                    warndlg('No Data for Confirmed(New) Trades');
                    CXC_New_Data = [];
                    CXC_Old_Data = [];
                end

                %%CXC View New Data 2 TR FORMAT
                if(exist('CXC_New_Data','var'))
                    [TRHeader , New_OutTRData , Market_Price_Index_Error_New , Maturity_Error_Data_New] = CXC_Future_Option_2_TR_Format(CXC_New_Data,'NEW') ;
                end

                %%CXC View Mod Data 2 TR FORMAT
                if(exist('CXC_Old_Data','var'))
                    [TRHeader , Mod_OutTRData ,Market_Price_Index_Error_Old , Maturity_Error_Data_Old] = CXC_Future_Option_2_TR_Format(CXC_Old_Data,'MOD') ;

                    if(size(Mod_OutTRData,2) > 1)
                        TRN_Number_Col = cellfun(@(V) strcmpi('TRN.Number',V), TRHeader) ;
                        Parent_TRN_Number_Col = cellfun(@(V) strcmpi('Parent Transaction Number',V), TRHeader) ;

                        Mod_OutTRData(:,TRN_Number_Col) = Existing_TradeId ;
                        Mod_OutTRData(:,Parent_TRN_Number_Col) = Existing_TradeId ;
                    end
                end

                %%% Filter the Voided data
                CONTRACT_STATUS3 = {'Voided','Voided (Not Posted)','Voided (Not Confirmed)'};
                Index_Voided = ismember(CXC_View_Data.CONTRACT_STATUS,CONTRACT_STATUS3);
                Cxc_Voided_Data = CXC_View_Data(Index_Voided,:);

                if ~isempty(Cxc_Voided_Data)
                    %%Compare CXC DB and OUR DB %% Get Del Trades
                    UNIQ_NO_Del_Col = cellfun(@(V) strcmpi('CONTRACT_REFERENCE',V), CXC_FieldNames) ;
                    Del_ID = Cxc_Voided_Data(:,UNIQ_NO_Del_Col);
                    OUR_UniqueNumber_Col = cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames_Del) ;

                    if(size(DBData_Del,2)>1)
                        Del_ID_Index  = cellStrfind_exact(DBData_Del(:,OUR_UniqueNumber_Col) , Del_ID) ;
                        Del_Data = DBData_Del(Del_ID_Index,:) ;  %% CXC Delete Trades
                    else
                        Del_ID_Index = [];
                    end

                    if(~isempty(Del_ID_Index))  
                        [DBFieldNames_Del ,  Del_Data] = Update_2_Database(Del_Data,char(Selected_BU_Name)) ; %% Update Table and Get Data From Table 
                    else
                        warndlg(SqlQuery_3,'Not Matching Delete Unique ID with deal_ticket_table_with_latest_versions_view');
                    end
                else
                    warndlg('No Data for Voided(Del) Trades');
                end
    
                %%% Combine the CXC New Data and CXC Modified Data
                Overall_New_TRData = New_OutTRData;
                Overall_Mod_TRData =  Mod_OutTRData;
                Overall_New_Mod_TRData = [Overall_New_TRData ; Overall_Mod_TRData];

                if isempty(Overall_New_Mod_TRData)
                    warndlg('No Data found in Future and Options in CXC_TRADE_DETAIL_FOR_OPERA View!','CXC_2_OPERA');
                    msgbox('Process Complete','Finish');
                    Job_Done(jObj);
                end
            
                %%% get the trade_id for only CXC New data
                [TradeId , ~] = getTradeID_4_Cell_Array(Overall_New_TRData,Selected_BU_Name);

                if ~strcmpi(TradeId , '')    %% if CXC New data is not empty           
                    TRN_TradeID_Col = cellStrfind_exact(TRHeader , {'TRN.Number'}) ;
                    Parent_TradeID_Col = cellStrfind_exact(TRHeader , {'Parent Transaction Number'}) ;
                    Overall_New_TRData(:,TRN_TradeID_Col) = TradeId ;
                    Overall_New_TRData(:,Parent_TradeID_Col) = TradeId ;

                    Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];               
                    xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'CXC_2_OPERA_TR_Format') ;               
                else  %% if CXC New data is empty 
                    Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];
                    xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'CXC_2_OPERA_TR_Format') ;
                end
     
                %%% Excel Write for CXC data
                if isempty(CXC_New_Data) 
                    xlswrite(OutXLSName , {'No Data'} , 'CXC_New_Data') ;
                else
                    xlswrite(OutXLSName , [CXC_FieldNames ;  table2cell(CXC_New_Data)] , 'CXC_New_Data') ;
                end

                if isempty(CXC_Old_Data) 
                    xlswrite(OutXLSName , {'No Data'} , 'CXC_Old_Data') ;
                else
                    xlswrite(OutXLSName , [CXC_FieldNames ;  table2cell(CXC_Old_Data)] , 'CXC_Old_Data') ;
                end

                if(exist('Del_Data','var') && (~isempty(Del_ID_Index)) )
                    xlswrite(OutXLSName , [DBFieldNames_Del ;  Del_Data] , 'CXC_Voided_Data') ; 
                end

                if(~isempty(Market_Price_Index_Error_New) || ~isempty(Market_Price_Index_Error_Old))
                    Market_Price_Index_Error = [Market_Price_Index_Error_New ; Market_Price_Index_Error_Old];
                    xlswrite(OutXLSName , Market_Price_Index_Error , 'Market Price Index Data') ;
                end

                if(~isempty(Maturity_Error_Data_New) || ~isempty(Maturity_Error_Data_Old))
                    Maturity_Error_Data = [Maturity_Error_Data_New ; Maturity_Error_Data_Old];
                    xlswrite(OutXLSName , Maturity_Error_Data , 'Maturity Error Data') ;
                end
    
                %%%% Delete Empty sheets and active the user required sheet
                OutXLSName = fullfile(pwd,OutXLSName) ;
                xls_delete_sheets(OutXLSName) ;
                xls_change_activesheet(OutXLSName , 'CXC_2_OPERA_TR_Format'); 

                msgbox('Process Complete','Finish');
            else
                warndlg('No Data found in CXC_TRADE_DETAIL_FOR_OPERA view for given Date ranges') ;  
            end 
        else
            warndlg(SqlQuery_1,'No Data Last 90 Days deal_ticket_table_with_latest_versions_view');
        end
   else   
        close(gcf);
        msgbox('Please Select the any one of the BU Name','Warning');
    end
else
    msgbox(['The Data can be retrieved only for Last 7 days.',{''},'Please choose the appropriate Date for Opera Start & End Date.'],'Warning');
end

Job_Done(jObj);
set(handles.pushbutton1,'Enable','on');


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

Live_Only = get(handles.radiobutton1,'Value');
Live_Dead = get(handles.radiobutton2,'Value');

try
    [BU_Name,Pr_Code] = Get_BU_Prcode(handles); %% User Selected BU
    [OutXLSName] = Get_OverAll_Dump(char(BU_Name),char(Pr_Code),Live_Only,Live_Dead);
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end

msgbox(['Overall Dump File is ',OutXLSName],'Finish');

Job_Done(jObj);

% --- Executes on button press in pushbutton3.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

Default_Name = 'deal_ticket_table_with_latest_versions_view_';
Default_Name_2 = 'cloned_deal_ticket_table_';

PLM = get(handles.checkbox1,'Value');
RBR = get(handles.checkbox2,'Value');
EDL = get(handles.checkbox3,'Value');

if (PLM || RBR || EDL)
        BU_Names = {'plm','rbr','edl'} ; %% Hard Code
        User_Select_Portfolio = {PLM,RBR,EDL};
        Business_Unit = BU_Names(logical(cell2mat(User_Select_Portfolio)));

    if(strcmpi(Business_Unit,'plm'))
        InBUName = 'plm';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'rbr'))
        InBUName = 'rbr';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'edl'))
        InBUName = 'edl';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    else
        
    end
    
    SqlQuery_1 = ['select count(*) from ',Table_Name,' where ops_unique IS NOT NULL'] ;
    SqlQuery_2 = ['select count(distinct(ops_unique)) from ',Table_Name];
    SqlQuery_3 = ['select count(*) from ',Table_Name_2,' where contract_number IS NULL'];
    
    ObjDB = connect_to_database;
    Count_1 = cell2mat(fetch(ObjDB,SqlQuery_1));
    Count_2 = cell2mat(fetch(ObjDB,SqlQuery_2));
    Count_3 = cell2mat(fetch(ObjDB,SqlQuery_3));
    close(ObjDB);
    delete(ObjDB);
    
    if(length(unique([Count_1;Count_2;Count_3])) == 1) %% Count_1 == Count_2
        %         msgbox(['       No Duplicate Entries Found in Opera       ',Business_Unit,' Latest Version Count : ',num2str(Count_1),' Distinct Ops Unique Count : ',num2str(Count_2)],'Count Matched');
        msgbox([['       No Duplicate Entries Found in Opera ;Count is Matched in ',char(InBUName)],{''}, SqlQuery_1,num2str(Count_1),SqlQuery_2,num2str(Count_2),SqlQuery_3,num2str(Count_3)],'Count Matched');
    else
        %         errordlg(['       Duplicate Entries Found in Opera       ',Business_Unit,' Latest Version Count : ',num2str(Count_1),' Distinct Ops Unique Count : ',num2str(Count_2)],'Count Mismatched');
        errordlg([['       No Duplicate Entries Found in Opera ;Count is MisMatched in ',char(InBUName)],{''}, SqlQuery_1,num2str(Count_1),SqlQuery_2,num2str(Count_2),SqlQuery_3,num2str(Count_3)],'Count Mismatched');
    end
    
else
    close(gcf);
    msgbox('Please Select the any one of the Business Unit','Warning');
end

Job_Done(jObj);


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% Live_Only = get(handles.radiobutton1,'Value');
% handles.Live_Only = Live_Only;
% guidata(hObject,handles);

% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

% Live_Dead = get(handles.radiobutton2,'Value');
% handles.Live_Dead = Live_Dead;
% guidata(hObject,handles);

% --- Executes on button press in radiobutton1.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% --- Executes on button press in radiobutton1.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

