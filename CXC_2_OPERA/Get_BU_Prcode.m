function [BU_Name,Business_Unit] = Get_BU_Prcode(handles)

PLM = get(handles.checkbox1,'Value');
RBR = get(handles.checkbox2,'Value');
EDL = get(handles.checkbox3,'Value');

if(PLM || RBR || EDL)
    
    ProductNames = {'PLM','RBR','EDL'}; %% Hard Code
    BU_Names = {'plm','rbr','edl'} ; %% Hard Code
    User_Select_Portfolio = {PLM,RBR,EDL};
    BU_Name = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
        
end