function [ OutData ] = parse_option_file_modified(FileName,BBGOptCode,BBG_ProdCode)
%
%  Matlab Version(s):   R2014b
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:24:36 $
%  $Revision: 1.1 $
%

% Read the rawdata from option dump file
RawData = textread(FileName,'%s','delimiter','\n');

% find the start and end of fields and data
Start_Of_Fields = find(strcmpi('START-OF-FIELDS',RawData));
End_Of_Fields = find(strcmpi('END-OF-FIELDS',RawData));
IdxFields = (Start_Of_Fields + 1) : (End_Of_Fields - 1);
FieldNames = RawData(IdxFields);

Start_Of_Data = find(strcmpi('START-OF-DATA',RawData));
End_Of_Data = find(strcmpi('END-OF-DATA',RawData));
IdxData = (Start_Of_Data + 1) : (End_Of_Data - 1);
InData = RawData(IdxData);

% clear the variables that will not be used anymore to avoid out of memory
% issues due to huge data handling
clear RawData; clear IdxFields; clear IdxData;

[~,InDataRem] = strtok(InData,'|'); % Removes the first column
[~,InDataRem] = strtok(InDataRem,'|'); % Removes the second column
[~,InData] = strtok(InDataRem,'|'); % Removes the third column

% data to be taken from 4th position to match with the order of  fields in the dump
Ticker = strtok(InData,'|');

% Exclude the last digits of ticker field as they indicate Month and C/P
FileOptCode = cellfun(@(x) x(1:end-1), Ticker, 'UniformOutput', false);   %% end-3

% Find the lines where Opt code from DB matches
IdxOptCode = cellStrfind_Perfect(strtrim(FileOptCode),BBGOptCode);
OptData = InData(IdxOptCode);

NumRows = size(OptData,1);
NumCols = length(FieldNames);

Data = cell(NumRows,NumCols);
tic
for iR = 1:NumRows
    RowData = strsplit(OptData{iR},'|');
    Data(iR,:) = RowData(2:end-1);
end
toc
OutData = cell2dataset([FieldNames';Data]);

clear Data;clear OptData;

%% Find the lines where Prod code from DB matches
FileProdCode = cellfun(@(x) x(1:end-3), Ticker, 'UniformOutput', false);

IdxProdCode = cellStrfind_Perfect(strtrim(FileProdCode),BBG_ProdCode);
OutData.FileTickers = unique(strtrim(Ticker(IdxProdCode)));

% NumRows = size(ProdData,1);
% NumCols = length(FieldNames);
% 
% Data = cell(NumRows,NumCols);
% tic
% for iR = 1:NumRows
% %     iR
%     RowData = strsplit(ProdData{iR},'|');
%     Data(iR,:) = RowData(2:end-1);
% end
% toc
% OutData_1 = cell2dataset([FieldNames';Data]);
% 
% clear InData;
% 
% clear Data;clear ProdData;

end










