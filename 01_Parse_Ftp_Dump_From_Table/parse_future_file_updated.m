function [OutData,InData,BarrierFuture_Header,BarrierFutureData] = parse_future_file_updated(FurturePrice_Header,FutureData,Mapping_Data,BBG_UndId) %% Mapping_FilePath i.e Excel Sheet


OutData = [];
InData = [];
BarrierFuture_Header = {'asset_class','underlying_id','settlement_date','open_price','high_price','low_price','settle_price','last_price'};
BarrierFutureData =[];

%%
% % Remove NaN Fields
% [~,~,FutureData] = xlsread(FileName);
%
% Header = FutureData(1,:);
% Row_find_Nan = FutureData(:,1);
%
% Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
% FutureData(:,Nan_Col(:)) = [];
%
% Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
% FutureData(Nan_Row(:),:) = [];

%%

% FutureData = FutureData(2:end,:);

% BBR_Code = cell(length(InData),1);          %% BBR_Code Column Adding
% BBR_Code(:,:) = {' '};
% BBR_Code(:,1) = {'Commodity'};
% InData = [InData,BBR_Code];                 %% Add BBR_Code Column

[Index] = Find_Index(FutureData,BBG_UndId);      %% Changing Area
Index = unique(Index);

% [~,~,Mapping_Data] = xlsread(Mapping_FilePath);      %% Mappping Data
Mapping_Data = Mapping_Data(2:end,:);

Future_Product_Code = strtrim(FutureData(:,3));

[aa,bb] = strtok(FutureData(:,1));
Remove_Space_Str = strtrim(bb(:,1));
Find_Max_Length = find(cellfun(@(s) (length(s)>6), Remove_Space_Str));
Extra_Length_Str = char(Remove_Space_Str(Find_Max_Length));
Extra_Length_Str = cellstr(Extra_Length_Str(:,4:end));
Remove_Space_Str(Find_Max_Length,:) = Extra_Length_Str;

BBR_Code = cell(length(Future_Product_Code),1);
BBR_Code(:,:) = {' '};

FileMaturities = cell(length(Future_Product_Code),1);           %% Find File Maturities for Missing Maturities
FileMaturities(:,:) = {' '};
[Month,~] = strtok(bb(Find_Max_Length,1));
aa(Find_Max_Length,1) = cellstr([char(aa(Find_Max_Length,1)),char(Month)]);
Month_Code = cellfun(@(x) x(end-1:end), aa, 'UniformOutput', false);

for i = 1 : length(Mapping_Data(:,1))
    Map_ProductCode = strtrim(Mapping_Data{i,1});
    Map_bbgCode = strtrim(Mapping_Data{i,2});
    Map_assetClass = strtrim(Mapping_Data{i,3});
    
    MatchedIndex = find(strcmpi(Map_ProductCode, Future_Product_Code) & strcmpi(Map_bbgCode, Remove_Space_Str));
    
    if(~isempty(MatchedIndex))
        BBR_Code(MatchedIndex,1) = cellstr(Map_assetClass);
        
        FileMaturities(MatchedIndex,1) = cellstr(strtrim(Mapping_Data{i,4}));
    end
    
end

IdxEmpty = find(cellfun(@(s) (length(s)<=1), FileMaturities)); %% Empty means Not in Mapping Table

Prodcode = FutureData(:,3);
FileMaturities_Final = cellstr(strcat(char(FileMaturities),{' '},Prodcode,{' '},char(Month_Code)));

%%

MonthNames = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
MonthNumber = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11';'12'};

Current_Year = datestr(today,'yyyy');
Current_Year_LastStr = str2double(Current_Year(end));
if Current_Year_LastStr == 0 %%% Newly added logic on 02-01-2020 (for handling last character is ZERO)
    Current_Year_AddStr = str2double(Current_Year(end-1));
else
    Current_Year_AddStr = str2double(Current_Year(end-1)) + 1;
end

ObjDB = connect_to_database;
Current_Month =  datestr(today,'mmm') ; %% datestr(char(fetch(ObjDB,'select settlement_date from valuation_date_table')),'mmm') ; %% datestr(today,'mmm') ;

New_Underlying_ID_Array = cell(size(FileMaturities_Final,1),1);

for i = 1 : size(FileMaturities_Final,1)
    Current_UndId = FileMaturities_Final(i,1) ;
    Last_Character_Underlying_ID = str2double(cell2mat( cellfun(@(x) x(end), Current_UndId, 'UniformOutput', false) )); %% Year
    Month_Character_Underlying_ID = cellfun(@(x) x(end-1), Current_UndId, 'UniformOutput', false); %% Month
    
    if(Last_Character_Underlying_ID == 0)
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end 
        
    elseif((Last_Character_Underlying_ID < Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end 
        
    elseif((Last_Character_Underlying_ID == Current_Year_LastStr))
        
        Underying_ID_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthCode , Month_Character_Underlying_ID))));
        Current_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthNames , {Current_Month} ) )));
        
        if(Underying_ID_MonthNumber < Current_MonthNumber)
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;
        end
    elseif((Last_Character_Underlying_ID > Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end  
    else
        
        New_Underlying_ID_Array(i,1) = Current_UndId;
    end
    
end

%%

BarrierFutureData = [BBR_Code,FileMaturities_Final,FutureData(:,5:end)];
BarrierFutureData = BarrierFutureData(Index,:);
% BarrierFutureData_Output.Header = BarrierFuture_Header;
% BarrierFutureData_Output.Data = BarrierFutureData;


FutureData(:,6:end) = []; %% For InData Column numbers are HardCoding here
InData = [FutureData,BBR_Code,FileMaturities_Final];                     %% Add Equity Data and Asset Class

FileMaturities_Final(IdxEmpty) = [];   %% Remove Empty Fields
New_Underlying_ID_Array(IdxEmpty) = [];

% ProdCod = InData(:,3);                 %% Find Empty Asset Product Row
% Type = InData(:,end);
% unique(ProdCod(cellfun(@isempty,strtrim(Type))))

% Header{1,6} = 'ASSET_CLASS'; %% for Verification write Excel Sheet
% xlswrite(['Add Asset Class',datestr(date,'yyyymmdd'),'.xlsx'],[Header;InData]);

OutData.FutureName = strtrim(InData(Index,2));
OutData.FuturePrice = cell2mat(InData(Index,4));
OutData.SettlementDate = InData(Index,5);
OutData.AssetClass = InData(Index,6);
OutData.ProductCode = strtrim(InData(Index,3));
OutData.FileMaturities = New_Underlying_ID_Array ; %% FileMaturities_Final;  %% strtrim(InData(Index,7));
OutData.InputFileMaturities = FileMaturities_Final ;

% OutData.FutureName = strtrim(InData(:,2));
% OutData.FuturePrice = cell2mat(InData(:,4));
% OutData.SettlementDate = InData(:,5);
% OutData.AssetClass = InData(:,6);



end


%%
function  [Index_my] = Find_Index(InData,str_match)
Index_my = [];

if ~iscellstr(str_match)
    str_match = cellstr(str_match);
end

parfor i = 1:size(InData,1)
    
    Current_Line = InData{i};
    [Index] = Find_Index_Sub(i,Current_Line,str_match);
    Index_my = [Index_my;Index];
    
end
end

%%

function [Index] = Find_Index_Sub(i,Current_Line,str_match)

Index = [];

parfor k = 1:length(str_match)
    
    %         idx = strncmp(InData{i},str_match{k},length(str_match{k}));
    
    if(strncmpi(Current_Line,str_match{k},length(str_match{k})))
        
        Index = [Index;i];
        
    end
end
end

