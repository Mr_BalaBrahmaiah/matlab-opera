function IsEqualVal = isnumericequal(Num1,Num2)
        IsEqualVal = abs(Num1 - Num2) <= 0.0000000001;
end