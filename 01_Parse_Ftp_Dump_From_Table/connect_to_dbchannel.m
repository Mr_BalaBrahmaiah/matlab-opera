function objDB = connect_to_dbchannel
% connect to the database configuration as provided in the
% DatabaseConfiguration file
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/01/23 07:18:53 $
%  $Revision: 1.1 $
%

   javaaddpath('mysql-connector-java-5.1.25-bin.jar');
  
    % Read the database configuration file to read the DB configuration
    % Default DB config values   
    
    DBName = 'operadbchannel';
    DBUserName = 'user';
    DBPassword = 'invenio@123';
    DBServerIP = 'SGTCX-OPRDB01';
    
    try
        fprintf('Using the database : %s : IP : %s\n',DBName,DBServerIP);
    catch
        disp('Error occured in reading the default configuration!');
    end
    
    objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);

end