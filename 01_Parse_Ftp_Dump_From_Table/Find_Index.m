function  [Index_my] = Find_Index(InData,str_match)
Index_my = [];

if ~iscellstr(str_match)
    str_match = cellstr(str_match);
end

parfor i = 1:size(InData,1)
    
    Current_Line = InData{i};
    [Index] = Find_Index_Sub(i,Current_Line,str_match);
    Index_my = [Index_my;Index];
    
end

%%  Sub Functions

function [Index] = Find_Index_Sub(i,Current_Line,str_match)

Index = [];

parfor k = 1:length(str_match)
    
    %         idx = strncmp(InData{i},str_match{k},length(str_match{k}));
    
    if(strncmp(Current_Line,str_match{k},length(str_match{k})))
        
        Index = [Index;i];
        
    end
end