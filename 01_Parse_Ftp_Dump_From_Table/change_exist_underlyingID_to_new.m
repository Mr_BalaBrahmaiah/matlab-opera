function [New_Underlying_ID_Array] = change_exist_underlyingID_to_new(Underlying_ID)


%%
MonthNames = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
MonthNumber = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11';'12'};

Current_Year = datestr(today,'yyyy');
Current_Year_LastStr = str2double(Current_Year(end));
if Current_Year_LastStr == 0 %%% Newly added logic on 02-01-2020 (for handling last character is ZERO)
    Current_Year_AddStr = str2double(Current_Year(end-1));
else
    Current_Year_AddStr = str2double(Current_Year(end-1)) + 1;
end

Current_Month = datestr(today,'mmm') ;

New_Underlying_ID_Array = cell(size(Underlying_ID,1),1);

for i = 1 : size(Underlying_ID,1)
    Current_UndId = Underlying_ID(i,1) ;
    Last_Character_Underlying_ID = str2double(cell2mat( cellfun(@(x) x(end), Current_UndId, 'UniformOutput', false) ));
    Month_Character_Underlying_ID = cellfun(@(x) x(end-1), Current_UndId, 'UniformOutput', false);
    
    if(Last_Character_Underlying_ID == 0)
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end 
        
    elseif((Last_Character_Underlying_ID < Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end 
        
    elseif((Last_Character_Underlying_ID == Current_Year_LastStr))
        
        Underying_ID_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthCode , Month_Character_Underlying_ID))));
        Current_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthNames , {Current_Month} ) )));
        
        if(Underying_ID_MonthNumber < Current_MonthNumber)
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
            
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;
        end
    elseif((Last_Character_Underlying_ID > Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
        char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
        Last_2_char = str2double(char_last_2(:,end-1:end));
        if isnan(Last_2_char)
            New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
        else
            New_Underlying_ID_Array(i,1) = Current_UndId;  
        end   
    else
        
        New_Underlying_ID_Array(i,1) = Current_UndId;
    end
    
end
