function parse_ftp_dump_updated_new_using_table
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/16 08:29:11 $
%  $Revision: 1.4 $
%
try
    %%
    
    DBName = 'operadbchannel';
    DBUserName = 'user';
    DBPassword = 'invenio@123';
    DBServerIP = 'SGTCX-OPRDB01';
    
    ObjDB_OperaDB_Channel = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
    
    %     aa = 1;
    %     while(aa)
    %         fprintf('Entering While Loop.....\n');
    %         ObjDB_OperaDB_Channel = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
    %         if(isempty(ObjDB_OperaDB_Channel.Message))
    %             aa = 0;
    %         end
    %     end
    
    %% UI Selection
    
    Selection_CellArray = {'OPTIONS','FUTURE','EQUITY','FX FUTURE','CASH EQUITY','FX-FORWARD','FX-OPTION'};  %%% nely added (FX-FORWARD) % FX-OPTION (27-11-2019)
    
    [theChosen_Cell, ~] = uicellect(Selection_CellArray,'MaxPer',15,'RowPix',40,'ColPix',250);
    
    Waitbar_Length = size(theChosen_Cell,1);
    
    %%
    parpool;
    tic;
    
    hWaitbar = waitbar(0,'Please wait...');
    
    Prompt        = {'Enter the Valuation Date(yyyy-mm-dd format):','Enter the Settlement Date for OptionFile - US products(yyyy-mm-dd format):','Enter the Settlement Date for OptionFile - UK products(yyyy-mm-dd format):','Enter the Settlement Date for OptionFile - ASIA products(yyyy-mm-dd format):','Enter the Settlement Date for FUTURE File products(yyyy-mm-dd format):'};
    Name          = 'Date Input for FTP Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(today,'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    DBValueDate    = InputDates(1);
    DBSettleDateUS = InputDates(2);
    DBSettleDateUK = InputDates(3);
    DBSettleDateAsia = InputDates(4);
    %     DBSettleDate = InputDates(5);
    
    if isempty(DBValueDate)
        DBValueDate  = {datestr(today,'yyyy-mm-dd')};
    end
    
    if isempty(DBSettleDateUS)
        DBSettleDateUS = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    if isempty(DBSettleDateUK)
        DBSettleDateUK = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    if isempty(DBSettleDateAsia)
        DBSettleDateAsia = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    
    if(find(strcmpi(theChosen_Cell,'OPTIONS')))
        [OptionFileNames,OptionPathName] = uigetfile('*.*','Select the Option Dump File (namr/eur/asia)','Multiselect','on');
    else
        OptionFileNames = 0;
        OptionPathName = 0;
    end
    
    if isnumeric(OptionPathName)
        DefPathName = pwd;
    else
        DefPathName = OptionPathName;
    end
    
    %% Choosing Future Input Files
    %     [FutureFileName,FuturePathName]  = uigetfile('*.*','Select the Future Dump File',DefPathName);
    %
    %     if isnumeric(FuturePathName)
    %         DefPathName = pwd;
    %     else
    %         DefPathName = FuturePathName;
    %     end
    %
    %     Exiting_ProdCode = 1;
    %     while(Exiting_ProdCode)
    %         if isnumeric(FutureFileName) && FutureFileName==0
    %             Exiting_ProdCode = 0;
    %             break;
    %         else
    %             Index = cellStrfind({FutureFileName},'FuturesSheet');
    %             if(Index)
    %                 Exiting_ProdCode = 0;
    %                 break;
    %             else
    %                 InputOptions = {'OK Continue'};
    %                 DefaultSelection = InputOptions{end};
    %                 UserSelction = ButtonChoice4User(InputOptions, 'Warning Information', DefaultSelection,...
    %                     'Please Choose Appropriate Future File');
    %                 [FutureFileName,FuturePathName]  = uigetfile('*.*','Select the Correct Future Dump File',pwd);
    %                 continue;
    %             end
    %
    %         end
    %     end
    
    %%  Choosing Equity Input Files
    %     [EquityFileName,EquityPathName]  = uigetfile('*.*','Select the Equity Dump File',DefPathName);
    %     Exiting_ProdCode = 1;
    %     while(Exiting_ProdCode)
    %         if isnumeric(EquityFileName) && EquityFileName==0
    %             Exiting_ProdCode = 0;
    %             break;
    %         else
    %             Index = cellStrfind({EquityFileName},'EqdBnd');
    %             if(Index)
    %                 Exiting_ProdCode = 0;
    %                 break;
    %             else
    %                 InputOptions = {'OK Continue'};
    %                 DefaultSelection = InputOptions{end};
    %                 UserSelction = ButtonChoice4User(InputOptions, 'Warning Information', DefaultSelection,...
    %                     'Please Choose Appropriate Equity File');
    %                 [EquityFileName,EquityPathName]  = uigetfile('*.*','Select the Correct Equity Dump File',DefPathName);
    %                 continue;
    %             end
    %
    %         end
    %     end
    %% Choosing FX Future Input File
    %     [FX_FutureFileName,FX_FuturePathName]  = uigetfile('*.*','Select the FX Future Dump File',DefPathName);
    %     Exiting_ProdCode = 1;
    %     while(Exiting_ProdCode)
    %         if isnumeric(FX_FutureFileName) && FX_FutureFileName==0
    %             Exiting_ProdCode = 0;
    %             break;
    %         else
    %             Index = cellStrfind({FX_FutureFileName},'FxFut');
    %             if(Index)
    %                 Exiting_ProdCode = 0;
    %                 break;
    %             else
    %                 InputOptions = {'OK Continue'};
    %                 DefaultSelection = InputOptions{end};
    %                 UserSelction = ButtonChoice4User(InputOptions, 'Warning Information', DefaultSelection,...
    %                     'Please Choose Appropriate FX Future File');
    %                 [FX_FutureFileName,FX_FuturePathName]  = uigetfile('*.*','Select the Correct FX Future Dump File',DefPathName);
    %                 continue;
    %             end
    %
    %         end
    %     end
    
    %% Choosing Cash Equity Input File
    %     [CashEquityFileName,CashEquityPathName]  = uigetfile('*.*','Select the Cash Equity Dump File',DefPathName);
    %     Exiting_ProdCode = 1;
    %     while(Exiting_ProdCode)
    %         if isnumeric(CashEquityFileName) && CashEquityFileName==0
    %             Exiting_ProdCode = 0;
    %             break;
    %         else
    %             Index = cellStrfind({CashEquityFileName},'CashEquity');
    %             if(Index)
    %                 Exiting_ProdCode = 0;
    %                 break;
    %             else
    %                 InputOptions = {'OK Continue'};
    %                 DefaultSelection = InputOptions{end};
    %                 UserSelction = ButtonChoice4User(InputOptions, 'Warning Information', DefaultSelection,...
    %                     'Please Choose Appropriate Cash Equity File');
    %                 [CashEquityFileName,CashEquityPathName]  = uigetfile('*.*','Select the Correct Cash Equity Future Dump File',DefPathName);
    %                 continue;
    %             end
    %
    %         end
    %     end
    
    %% Missing Maturities FileName
    FutureFileName = 0; %% DB Channel Purpose
    EquityFileName = 0;
    FX_FutureFileName = 0;
       
    if((isnumeric(FutureFileName) && FutureFileName==0) && (isnumeric(EquityFileName) && EquityFileName==0) && (isnumeric(FX_FutureFileName) && FX_FutureFileName==0))
        XlS_FileName_Missing_Maturity = [];
    else
        XlS_FileName_Missing_Maturity = ['Missing_Maturities_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'];
        XlS_FileName_Missing_Maturity = fullfile(pwd,XlS_FileName_Missing_Maturity);
    end

    %%     Mapping_FilePath = [pwd filesep 'Equity_Mapping_Files' filesep 'Mapping_Eqd&Bond.xlsx'];
    [MappingFieldNames,Mapping_Data] = read_from_database('asset_class_bbgcode_mapping_view',0); %% asset_class_bbgcode_mapping
    
    %%
    [DBFieldNames,DBData] = read_from_database('product_master_table',0);
    ProdMasterData = cell2dataset([DBFieldNames;DBData]);
    
    BBG_ProdCode = ProdMasterData.bbg_product_code;
    
    %%
    
    [~,Settlement_Date] = read_from_database('valuation_date_table',0,'SELECT settlement_date FROM valuation_date_table');
    SqlQuery = ['select * from underlying_list_table where expiry_date >=''',char(Settlement_Date),''' '];
    
    [DBFieldNames,DBData] = read_from_database('underlying_list_table',0,SqlQuery);
    FutDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_UndId = FutDataDB.bbg_underlying_id;
    UndId = FutDataDB.underlying_id;
    Asset_Class = FutDataDB.asset_class;
    DBProductcode = FutDataDB.invenio_product_code;
    %     DBCurrency = FutDataDB.currency;
    IdxEmpty = cellfun(@isempty,BBG_UndId);
    BBG_UndId(IdxEmpty) = [];
    UndId(IdxEmpty) = [];
    Asset_Class(IdxEmpty) = [];
    DBProductcode(IdxEmpty) = [];
    %     DBCurrency(IdxEmpty) = [];
    
    %     BBG_UndId = unique(BBG_UndId);                   %% check why give same ID multiple time %% New Change
    
    [DBFieldNames,DBData] = read_from_database('vol_id_table',0);
    VolDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_OptCode = VolDataDB.bbg_opt_ticker;
    DBVolId = VolDataDB.vol_id;
    
    % Ignoring the index and currency asset_classes as they are not needed
    % for option related calculations; if they are not excluded in this
    % step, they create the issue with redundant product codes
    SqlQuery = 'select invenio_product_code,option_tick_size from product_master_table where asset_class not in (''index'',''currency'',''bond'',''money market'')';
    [~,DBData] = read_from_database('product_master_table',0,SqlQuery);
    DBInvProductCode = DBData(:,1);
    DBOptionTickSize = cell2mat(DBData(:,2));
    
    
    %% Option dump parser
    if isnumeric(OptionFileNames) && OptionFileNames==0
        errordlg('Option Dump file not selected!','FTP Parser');
    else
        RequiredOptionFields = {'TICKER','OPT_PUT_CALL','OPT_UNDL_PX','OPT_STRIKE_PX',...
            'PX_LAST','PX_OPEN','PX_HIGH','PX_LOW','PX_VOLUME','OPT_OPEN_INT',...
            'OPT_UNDL_TICKER', ...
            'PX_SETTLE_LAST_DT','PX_SETTLE','OPT_EXPIRE_DT',...
            'PX_VOLUME_1D','SECURITY_TYP','CRNCY','MARKET_SECTOR_DES'};
        
        RequiredOptionFields = [RequiredOptionFields , {'FileTickers'}];
        
        if ~iscellstr(OptionFileNames)
            OptionFileNames = cellstr(OptionFileNames);
        end
        if ~iscellstr(OptionPathName)
            OptionPathName = cellstr(OptionPathName);
        end
        
        FileName = fullfile(OptionPathName,OptionFileNames);
        NumOptFiles = length(FileName);
        OptionData = [];
        waitbar(0.1,hWaitbar,'Please wait, parsing option dump file');
        for iFile = 1:NumOptFiles
            [ OutData ] = parse_option_file_modified(FileName{iFile},BBG_OptCode,BBG_ProdCode); %% BBG_ProdCode  %% BBG_OptCode
            if isempty(OutData)
                continue;
            end
            %             xlswrite(['raw_option_dump_',OptionFileNames{iFile},'.xlsx'],dataset2cell(OutData));
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            
            NA_Index = cellStrfind(OutData.PX_SETTLE_LAST_DT,{'N.A.',' '});
            OutData(NA_Index,:) = [];
            
            DumpSettleDate = cellstr(datestr(datenum(OutData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
            
            %%% Newly added function regexpi instead of strfind
            if ~isempty(regexpi(FileName{iFile},'namr'))        % Change % strfind to regexpi (case insensitive)
                DBSettleDate = DBSettleDateUS;
            elseif ~isempty(regexpi(FileName{iFile},'euro'))
                DBSettleDate = DBSettleDateUK;
            elseif ~isempty(regexpi(FileName{iFile},'asia'))
                DBSettleDate = DBSettleDateAsia;
            end
            
            IdxDate = ismember(DumpSettleDate,DBSettleDate);
            IdxComdty = strcmpi('Comdty',OutData.MARKET_SECTOR_DES); % to ignore the others like curncy and index market types, since they return irrelevant data
            IdxSecType = strcmpi('Physical commodity option.',OutData.SECURITY_TYP);
            %IdxSecType = strcmpi('Financial commodity option.',OutData.SECURITY_TYP);
            IdxFound = IdxDate & IdxComdty & IdxSecType;
            
            for iField = 1:length(RequiredOptionFields)
                TempField = RequiredOptionFields{iField};
                if isfield(OptionData,TempField)
                    OptionData.(TempField) = [OptionData.(TempField);OutData.(TempField)(IdxFound)];
                else
                    OptionData.(TempField) = OutData.(TempField)(IdxFound);
                end
            end
            fprintf('%d : %s\n',iFile,datestr(now)); %% For Check
            clear OutData;
            
        end
        
        OPT_IMPLIED_VOLATILITY_MID = cell(size(OptionData.TICKER,1),1);
        OptionData.OPT_IMPLIED_VOLATILITY_MID = OPT_IMPLIED_VOLATILITY_MID;
        
        SettlePrice  = str2double(OptionData.PX_SETTLE);
        Strike       = str2double(OptionData.OPT_STRIKE_PX);
        AssetPrice   = str2double(OptionData.OPT_UNDL_PX);
        % TODO - verify the below code
        AssetPrice(AssetPrice<0) = 0.1;
        Strike(Strike<0) = 0;
        SettlePrice(SettlePrice<0) = 0;
        % End of TODO
        
        % Settlement Price
        SecurityID = cell(size(OptionData.TICKER));
        VolID = cell(size(OptionData.TICKER));
        for iCode = 1:length(BBG_OptCode)
            IdxFound = strmatch(BBG_OptCode{iCode},OptionData.TICKER);
            
            for iIdx = 1:length(IdxFound)
                SecurityID{IdxFound(iIdx)} = [DBVolId{iCode},OptionData.OPT_PUT_CALL{IdxFound(iIdx)},' ',num2str(Strike(IdxFound(iIdx)))];
            end
            
            VolID(IdxFound) = DBVolId(iCode);
            
        end
        
        %% Find Missing Maturity Options
        
        Exiting_ProdCode = [];
        Missing_ProdCode_Data = [];
        OptionData.FileTickers = cellfun(@(x) x(1:end-1), OptionData.FileTickers, 'UniformOutput', false);
        IdxNonEmpty = cellfun(@(s) (length(s)>1), OptionData.FileTickers) ;
        OptionData.FileTickers = OptionData.FileTickers(IdxNonEmpty) ;
        
        NumFileTickers = length(OptionData.FileTickers);
        for iT = 1:NumFileTickers
            IdxFound = strmatch(OptionData.FileTickers{iT},BBG_OptCode);
            if(~isempty(IdxFound))
                Exiting_ProdCode = [Exiting_ProdCode ; OptionData.FileTickers(iT)];
            else
                Missing_ProdCode_Data = [Missing_ProdCode_Data ; OptionData.FileTickers(iT)];
            end
            
        end
        
        [~,Mapping_Data_1] = read_from_database('asset_class_bbgcode_mapping_view',0,'SELECT * FROM asset_class_bbgcode_mapping_view where asset_class not in (''index'',''currency'',''bond'',''money market'')');
        
        if(~isempty(Missing_ProdCode_Data))
            
            Missing_ProdCode = strtrim(cellfun(@(x) x(1:end-2), Missing_ProdCode_Data, 'UniformOutput', false)); %% Get Product Code Only
            
            AssetClass_Code = cell(size(Missing_ProdCode,1),3);
            AssetClass_Code(:,:) = {''};
            
            for i = 1 : length(Mapping_Data_1(:,1))
                Map_ProductCode = strtrim(Mapping_Data_1{i,1});
                Map_bbgCode = strtrim(Mapping_Data_1{i,2});
                Map_assetClass = strtrim(Mapping_Data_1{i,3});
                Map_assetClass_ShortCode = strtrim(Mapping_Data_1{i,4});
                
                MatchedIndex = find(strcmpi(Map_ProductCode, Missing_ProdCode)) ;
                
                if(~isempty(MatchedIndex))
                    AssetClass_Code(MatchedIndex,1) = cellstr(Map_assetClass);
                    AssetClass_Code(MatchedIndex,2) = cellstr([Map_assetClass_ShortCode,' ',Map_ProductCode]); %% Combined Shortcode with product Code
                    AssetClass_Code(MatchedIndex,3) = Missing_ProdCode_Data(MatchedIndex); %% BBG Opt Ticker
                end
                
            end
            
            Not_In_Map_Tbl = setdiff(Missing_ProdCode,Mapping_Data_1(:,1)) ;
            Not_In_Map_Tbl_Index = cellStrfind_exact(Missing_ProdCode,Not_In_Map_Tbl); %% Get Empty Index
            AssetClass_Code(Not_In_Map_Tbl_Index,:) = []; %% Remove Empty Index
            
            Temp_Cell = cell(size(AssetClass_Code,1),1);
            
            Missing_Maturity = [AssetClass_Code(:,1) ,Temp_Cell , AssetClass_Code(:,2) , AssetClass_Code(:,3), Temp_Cell,Temp_Cell,Temp_Cell];
            Missing_Maturity_OPT_Header = {'asset_class','vol_id','product_code','bbg_opt_ticker','expiry_date','updated_by','updated_timestamp'};
            
            OutXLSFile1 = getXLSFilename('Missing_Option_Maturities');
            xlswrite(OutXLSFile1 ,[Missing_Maturity_OPT_Header;Missing_Maturity]); %% 'Missing Option Maturities'
            
        end
        
        %%
        
        Value_Date = repmat(DBValueDate,size(SecurityID));
        DataSource = repmat(cellstr('bbg_dumpfile'),size(SecurityID));
        Settle_Date = cellstr(datestr(datenum(OptionData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
        SettlePriceHeader = {'Security ID','Value Date','Settlement Price',...
            'Data Source','Settlement Date',...
            'Last Price','Open Price','High Price','Low Price','Open Int',...
            'Volume','Volume 1D','Imp Vol Mid'};
        SettlePriceData = [SecurityID,Value_Date,num2cell(SettlePrice),...
            DataSource,Settle_Date,...
            num2cell(str2double(OptionData.PX_LAST)),num2cell(str2double(OptionData.PX_OPEN)),...
            num2cell(str2double(OptionData.PX_HIGH)),num2cell(str2double(OptionData.PX_LOW)),...
            num2cell(str2double(OptionData.OPT_OPEN_INT)),num2cell(str2double(OptionData.PX_VOLUME)),...
            num2cell(str2double(OptionData.PX_VOLUME_1D)),num2cell(str2double(OptionData.OPT_IMPLIED_VOLATILITY_MID)/100)];
        
        Empty_SettlePrice_Index = find(cellfun(@isempty,SettlePriceData));
        SettlePriceData(Empty_SettlePrice_Index,:) = [];     %% Remove Missing_Maturities_OPT_Not_Tbl Product Indexs
        
        %         xlswrite(['Settlement_Price_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettlePriceHeader;SettlePriceData]);
        
        %% Excel Write Options Outputs
        
        %         OutXLSFile1 = getXLSFilename('Missing_Option_Maturities');
        %         xlswrite(OutXLSFile1 ,[Missing_Maturity_OPT_Header;Missing_Maturity]); %% 'Missing Option Maturities'
        
        OutXLSFile3 = getXLSFilename('Settlement_Price_Table');
        xlswrite(OutXLSFile3 ,[SettlePriceHeader;SettlePriceData]);
        
        %%
        waitbar(0.4,hWaitbar,'Please wait, calculating settle vols');
        % Settlement Vol
        MaturityDate = datenum(OptionData.OPT_EXPIRE_DT,'yyyymmdd');
        OptionType = OptionData.OPT_PUT_CALL;
        IdxCall = strcmpi(OptionType,'C');
        IdxPut = strcmpi(OptionType,'P');
        OptionType = strrep(OptionType,'C','call');
        OptionType = strrep(OptionType,'P','put');
        
        OptionTickSize = zeros(size(OptionData.TICKER));
        for iCode = 1:length(BBG_UndId)
            Idx = strmatch(BBG_UndId{iCode},OptionData.OPT_UNDL_TICKER);
            IdxTS = strcmpi(DBProductcode(iCode),DBInvProductCode);
            
            if(~isempty(find(IdxTS)))  %% DBProductcode have All Asset class but DBInvProductCode have without Index and Currency
                OptionTickSize(Idx) = DBOptionTickSize(IdxTS);
            end
        end
        
        IntrinsicValue = zeros(size(OptionData.TICKER));
        TempZeroVar = zeros(size(OptionData.TICKER));
        ModOptionPrice = SettlePrice;
        IntrinsicValue(IdxCall) = AssetPrice(IdxCall) - Strike(IdxCall);
        IntrinsicValue(IdxPut)  = Strike(IdxPut) - AssetPrice(IdxPut);
        IntrinsicValue(IntrinsicValue<0) = 0;
        %         IdxDeepITM = (IntrinsicValue == SettlePrice);
        IdxDeepITM = isnumericequal(IntrinsicValue,SettlePrice);
        ModOptionPrice(IdxDeepITM) = SettlePrice(IdxDeepITM) + OptionTickSize(IdxDeepITM);
        IdxZeroOptPrice = isnumericequal(ModOptionPrice,TempZeroVar);
        ModOptionPrice(IdxZeroOptPrice) = OptionTickSize(IdxZeroOptPrice);
        
        % % %          Settle Vol/Price Correction:
        % % % Call:
        % % %                 AssetPrice - Strike >= 0.0
        % % %                                 If OptionPrice = AssetPrice - Strike
        % % %                                                 OptionPrice = OptionPrice+OptionTickSize
        % % %                 AssetPrice - Strike <0.0
        % % %                                 If OptionPrice = 0.0
        % % %                                                 OptionPrice = OptionTickSize
        % % %
        % % % Put:
        % % %                 Strike � AssetPrice <= 0.0
        % % %                                 If OptionPrice = Strike - AssetPrice
        % % %                                                 OptionPrice = OptionPrice+OptionTickSize
        % % %                 Strike � AssetPrice >0
        % % %                                 If OptionPrice = 0.0
        % % %                                                 OptionPrice = OptionTickSize
        
        
        
        %         TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today +1;
        TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today ; % TODO - added on 01-July-2014 for new TTM convention
        
        %         SettleVols = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, SettlePrice);
        SettleVols = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, ModOptionPrice);
        
        % Check the NaN vols and ignore them before updating in excel,
        % since they cannot be uploaded in database
        NonNanIdx  = ~(cellfun(@isnan,SettleVols));
        Value_Date = repmat(DBValueDate,size(SettleVols(NonNanIdx)));
        
        SettleVolHeader = {'Value Date','VolId','Strike','Call_Put_Id','Settle Vol'};
        SettleVolData = [Value_Date,VolID(NonNanIdx),num2cell(Strike(NonNanIdx)),OptionData.OPT_PUT_CALL(NonNanIdx),SettleVols(NonNanIdx)];
        
        Empty_SettleVolData_Index = find(cellfun(@isempty,SettleVolData(:,2))); %% Remove Empty Index after that "vanillaimpvolbybs" Function
        SettleVolData(Empty_SettleVolData_Index,:) = [];
        
        OutXLSFile4 = getXLSFilename('Settlement_Vol_Surf_Table');
        xlswrite(OutXLSFile4,[SettleVolHeader;SettleVolData]);
        
        waitbar(0.2,hWaitbar,'OPTIONS Dump Finished','Name','Finished');
        
    end % end of option dump file selection
    
    
    %     DBSettleDate = [DBSettleDateUS,DBSettleDateUK,DBSettleDateAsia];
    DBSettleDate = InputDates(5);
    
    %% Future dump parser
    
    if(isempty(find(strcmpi(theChosen_Cell,'FUTURE'))))
        errordlg('Future Dump file not selected!','MarketData Parser');
    else
        
        Future_Price_TableName = 'mktdata_future_price_table';
        SqlQuery_Future = ['select bbg_ticker,bbg_code,product_code,px_settle,mkt_date,px_open,px_high,px_low,px_settle,px_last from ',...
            Future_Price_TableName,' where mkt_date = ''',char(DBSettleDate),''' '];
        [FurturePrice_Header,FuturePrice_Data] = Fetch_DB_Data(ObjDB_OperaDB_Channel,SqlQuery_Future);
        if(strcmpi(FuturePrice_Data,'No Data'))
            errordlg(['Not Getting Data from ',Future_Price_TableName,' Please Check the DB'],'MarketData Parser');
        elseif(isempty(FuturePrice_Data))
            errordlg(['Not Getting Data from ',Future_Price_TableName,' Please Check the DB'],'MarketData Parser');
        else
            waitbar(0.8,hWaitbar,'Please wait, parsing future dump file');
            
            [FutureData,InData_FUT,BarrierFuture_Header,BarrierFutureData] = parse_future_file_updated(FurturePrice_Header,FuturePrice_Data,[MappingFieldNames;Mapping_Data],BBG_ProdCode);
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            % To find the invalid date entries that cannot be handled by
            % datenum function
            %%
            %         InvalidIdx = strcmpi(FutureData.SettlementDate,'N.A.') | strcmpi(FutureData.SettlementDate,'N.A') | ...
            %             strcmpi(FutureData.SettlementDate,'') | strcmpi(FutureData.SettlementDate,' ');
            %         FutureData.SettlementDate(InvalidIdx) = {'01/01/1900'};
            %         if isdeployed
            %             DumpSettleDate = cellstr(datestr(datenum(FutureData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
            %         else
            %             DumpSettleDate = cellstr(datestr(datenum(FutureData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
            %         end
            %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
            
            DumpSettleDate = FutureData.SettlementDate;
            IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
            
            if(~isempty(BarrierFutureData))
                BarrierFutureData(IdxDate,:) = [];
                BarrierFuture_FileName = getXLSFilename('barriers_future_price_table');
                
                Barrier_UnderlyingCol = cellStrfind_exact(BarrierFuture_Header,{'underlying_id'});
                [New_Underlying_ID_Array] = change_exist_underlyingID_to_new(BarrierFutureData(:,Barrier_UnderlyingCol));
                BarrierFutureData(:,Barrier_UnderlyingCol) = New_Underlying_ID_Array;
                xlswrite(BarrierFuture_FileName,[BarrierFuture_Header ; BarrierFutureData]);
            end
            
            
            %%
            % underlying settle value computation
            FutureData.FutureName(IdxDate) = [];
            FutureData.FuturePrice(IdxDate)= [];
            FutureData.AssetClass(IdxDate)= [];
            FutureData.ProductCode(IdxDate)= [];
            
            FutureID = cell(size(FutureData.FutureName));
            for iCode = 1 : length(BBG_UndId)
                %  Idx = strmatch(BBG_UndId{iCode},FutureData.FutureName); %#ok<*MATCH2>
                Idx = strcmpi(BBG_UndId{iCode},FutureData.FutureName) & strcmpi(Asset_Class{iCode},FutureData.AssetClass);
                FutureID(Idx) = UndId(iCode);
            end
            
            % Check FutureID or Asset Class  % FutureData.AssetClass
            IdxNonEmpty = cellfun(@(s) (length(s)>1), FutureData.AssetClass) & cellfun(@(s) (length(s)>1), FutureID);  % Product Code Match in Table & InputFiles not Asset Class
            
            FutureID  =  FutureID(IdxNonEmpty);
            FutureData.AssetClass = FutureData.AssetClass(IdxNonEmpty);        % Remove Empty Asset Class Fileds
            FutureData.SettlementDate = FutureData.SettlementDate(IdxNonEmpty);
            FutureData.FuturePrice = FutureData.FuturePrice(IdxNonEmpty);
            FutureData.FutureName = FutureData.FutureName(IdxNonEmpty);
            FutureData.ProductCode = FutureData.ProductCode(IdxNonEmpty);
            
            %%%%%%%%%%%%%%%%%%%%%%%%% Find Missing Maturity Data %%%%%%%%%%%%%%%%%%%%%%%%%
            if(sum(IdxDate)==0)
                Missing_Maturities_FUT = setdiff(FutureData.FileMaturities ,FutureID);  % Find Missing Maturities
                %         xlswrite(XlS_FileName_Missing_Maturity,Missing_Maturities_FUT,'Missing Future Maturities');
                
                if(~isempty(Missing_Maturities_FUT))
                    Missing_Index = cellStrfind_exact(FutureData.FileMaturities,Missing_Maturities_FUT) ; %%
                    Missing_Maturities_FUT = FutureData.InputFileMaturities(Missing_Index) ; %% Get Input FormatMissing Maturities
                    
                    [Missing_UnderlyingID_Data_FUT,Missing_SettleValue_Data_FUT] = Find_Missing_MaturityData_Common(InData_FUT,Missing_Maturities_FUT,DBValueDate);
                    
                    OutXLSFile5 = getXLSFilename('missing_maturities_future');
                    xlswrite(OutXLSFile5 ,Missing_UnderlyingID_Data_FUT ,'Missing_UnderlyingID_Data_FUT');
                    OutXLSFile6 = getXLSFilename('missing_underlying_settle_value_table');
                    xlswrite(OutXLSFile6,Missing_SettleValue_Data_FUT,'Missing_SettleValue_Data_FUT');
                    
                    
                    try
                        xls_delete_sheets([pwd filesep OutXLSFile5]);
                        xls_delete_sheets([pwd filesep OutXLSFile6]);
                    catch
                        
                    end
                    
                end
                
            else
                warndlg(['Please Select the Appropriate File as There are No Settle Prices for Future File : ',char(DBSettleDate)]) ;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % check for the redundant entries if at all present for the same
            % settlement date
            [~,UniqIdx,TempIdx] = unique(FutureID,'stable');
            TempVar = zeros(size(UniqIdx));
            Value_Date = repmat(DBValueDate,size(UniqIdx));
            % TODO - Below lines are to divide OR price by 100. Remove them
            % once it is handled in database
            IdxOR = strmatch('OR',FutureID(UniqIdx));
            %         FutPrice = str2double(FutureData.FuturePrice(UniqIdx));
            FutPrice = FutureData.FuturePrice(UniqIdx);
            FutPrice(IdxOR) = FutPrice(IdxOR)/100;
            % End of TODO
            UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
            %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(FutureData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
            UndSettleData = [FutureData.AssetClass(UniqIdx),FutureID(UniqIdx),Value_Date,num2cell(FutPrice),num2cell(TempVar),...
                num2cell(TempVar),FutureData.SettlementDate(UniqIdx)];
            
            % write in xls file
            OutXLSFile7 = getXLSFilename('underlying_settle_value_table');
            xlswrite(OutXLSFile7 ,[UndSettleHeader;UndSettleData]);
            
            % Generate the traders surface data for every business unit in
            % the below segment of code; activate for every BU whenever
            % requested
            %         BUNames = {'cfs','cop','agf','qf1','qf2','qf3','qf4','qf5'};
            
            %         BUNames = [];
            BUNames = {'cfs','ogp','usg','og2'};  %%% newly added BU Name = 'usg' %% remove agf & coc and add the ogp (05-07-2019)
            %%% added 'OG2' on 2020-01-24
            for iBU = 1:length(BUNames)
                InBUName = BUNames{iBU};
                try
                    %             [~,~,RawMapData] = xlsread('Suportfolio_Product_Mapping.xlsx');
                    %             subportfolio_mapping = RawMapData(2:end,:);
                    
                    SqlQuery = 'select distinct(subportfolio),product_code,directional_portfolio from subportfolio_product_mapping_table where product_code not like ''%USD%''';
                    [~,subportfolio_mapping] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery,InBUName);
                catch
                    errordlg('Product mapping data for subportfolio is not found! Hence underlying_traders_value file will not be created!');
                    return;
                end
                
                num_pf = length(subportfolio_mapping);
                
                UndSettleHeader_Traders = UndSettleHeader;  %% Changing Future Data For Traders table Purpose
                UndSettleData_Traders = UndSettleData;
                UndSettleHeader_Traders(:,1) = [];
                UndSettleHeader_Traders(:,end) = [];
                UndSettleData_Traders(:,1) = [];
                UndSettleData_Traders(:,end) = [];
                
                UndTradersData =[];
                for iP = 1:num_pf
                    pf_code = subportfolio_mapping{iP,2};
                    subportfolio = subportfolio_mapping(iP,1);
                    Index = strncmp(pf_code,UndSettleData_Traders(:,1),length(pf_code));
                    temp_portfolio = repmat(subportfolio,size(find(Index)));
                    temp_data = [temp_portfolio UndSettleData_Traders(Index,:)];
                    UndTradersData = [UndTradersData;temp_data]; %#ok<AGROW>
                    % to add data for directional portfolios
                    dir_portfolio = subportfolio_mapping{iP,3};
                    if ~isempty(dir_portfolio)
                        temp_dirportfolio = repmat(cellstr(dir_portfolio),size(find(Index)));
                        temp_dirdata = [temp_dirportfolio UndSettleData_Traders(Index,:)];
                        UndTradersData = [UndTradersData;temp_dirdata];
                    end
                end
                UndTradersHeader = [{'Asset Class','Subportfolio'},UndSettleHeader_Traders];
                
                [ShortCode_Array,Remain] = strtok(UndTradersData(:,2),' ');
                [ProductCode_Array,dd] = strtok(Remain,' ');
                [AssetClass_Array] = Get_Asset_Class_4_CellArray(ProductCode_Array,ShortCode_Array,[MappingFieldNames;Mapping_Data]);
                
                UndTradersData = [AssetClass_Array,UndTradersData] ;
                
                if(strcmpi(InBUName,'agf')) 
                    Subportfolio_Col = cellStrfind_exact(UndTradersHeader,{'Subportfolio'});
                    ZT_Portfolio_Index = strncmpi(UndTradersData(:,Subportfolio_Col),'ZT-',3);
                    UndTradersData = UndTradersData(ZT_Portfolio_Index,:);
                end
                % write in xls file
                xlswrite(['underlying_traders_value_table_',InBUName,'_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndTradersHeader;UndTradersData]);
            end
            %% End of TODO
            waitbar(0.4,hWaitbar,'FUTURE Dump Finished','Name','Finished');
        end
    end
    
    %% Equity File Parser
    if(isempty(find(strcmpi(theChosen_Cell,'EQUITY'))))
        errordlg('EQUITY Dump file not selected!','MarketData Parser');
    else
        
        Equity_Data_TableName = 'mktdata_index_price_table';
        SqlQuery_Equity = ['select * from ',Equity_Data_TableName,' where mkt_date = ''',char(DBSettleDate),''' '];
        [Equity_Header,Equity_Data] = Fetch_DB_Data(ObjDB_OperaDB_Channel,SqlQuery_Equity);
        if(strcmpi(Equity_Data,'No Data'))
            errordlg(['Not Getting Data from ',Equity_Data_TableName,' Please Check the DB'],'MarketData Parser');
        elseif(isempty(Equity_Data))
            errordlg(['Not Getting Data from ',Equity_Data_TableName,' Please Check the DB'],'MarketData Parser');
        else
            [EquityData,InData_EQD] = parse_equity_file(Equity_Data,[MappingFieldNames;Mapping_Data],BBG_UndId);
            
            if(isempty(EquityData.FutureName))
                errordlg('Equity Dump file Data not Found in DB!','FTP Parser');
            else
                %             InvalidIdx = strcmpi(EquityData.SettlementDate,'N.A.') | strcmpi(EquityData.SettlementDate,'N.A') | ...
                %                 strcmpi(EquityData.SettlementDate,'') | strcmpi(EquityData.SettlementDate,' ');
                %             EquityData.SettlementDate(InvalidIdx) = {'01/01/1900'};
                %             if isdeployed
                %                 DumpSettleDate = cellstr(datestr(datenum(EquityData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
                %             else
                %                 DumpSettleDate = cellstr(datestr(datenum(EquityData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
                %             end
                %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
                
                DumpSettleDate = EquityData.SettlementDate;
                IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
                
                % underlying settle value computation
                EquityData.FutureName(IdxDate) = [];
                EquityData.FuturePrice(IdxDate)= [];
                EquityData.AssetClass(IdxDate)= [];
                EquityData.ProductCode(IdxDate)= [];
                
                EquityID = cell(size(EquityData.FutureName));
                for iCode = 1:length(BBG_UndId)
                    %             Idx = strmatch(BBG_UndId{iCode},EquityData.FutureName); %#ok<*MATCH2>
                    Idx = strcmpi(BBG_UndId{iCode},EquityData.FutureName) & strcmpi(Asset_Class{iCode},EquityData.AssetClass);
                    EquityID(Idx) = UndId(iCode);
                end
                
                IdxNonEmpty = cellfun(@(s) (length(s)>1), EquityID);  %% Product Code Match in Table & InputFiles not Asset Class
                
                EquityID  =  EquityID(IdxNonEmpty);
                EquityData.AssetClass = EquityData.AssetClass(IdxNonEmpty);        %% Remove Empty Asset Class Fileds
                EquityData.SettlementDate = EquityData.SettlementDate(IdxNonEmpty);
                EquityData.FuturePrice = EquityData.FuturePrice(IdxNonEmpty);
                EquityData.FutureName = EquityData.FutureName(IdxNonEmpty);
                EquityData.ProductCode = EquityData.ProductCode(IdxNonEmpty);
                
                %%%%%%%%%%%%%%%%%%%%%%%%% Find Missing Maturity Data %%%%%%%%%%%%%%%%%%%%%%%%%
                if(sum(IdxDate)==0)
                    
                    Missing_Maturities_EQD = setdiff(EquityData.FileMaturities ,EquityID);  % Find Missing Maturities
                    %             xlswrite(XlS_FileName_Missing_Maturity,Missing_Maturities_EQD,'Missing Equity Maturities');
                    
                    if(~isempty(Missing_Maturities_EQD))
                        Missing_Index = cellStrfind_exact(EquityData.FileMaturities,Missing_Maturities_EQD) ; %%
                        Missing_Maturities_EQD = EquityData.InputFileMaturities(Missing_Index) ; %% Get Input FormatMissing Maturities
                        
                        [Missing_UnderlyingID_Data_EQD,Missing_SettleValue_Data_EQD] = Find_Missing_MaturityData_Common(InData_EQD,Missing_Maturities_EQD,DBValueDate);
                        
                        Missing_UnderlyingID_Data_EQD(2:end,2:3) = strrep(Missing_UnderlyingID_Data_EQD(2:end,2:3) , 'ID KM' ,'ID KM1');  %% Till June 8th,2017
                        Missing_SettleValue_Data_EQD(2:end,2) = strrep(Missing_SettleValue_Data_EQD(2:end,2) , 'ID KM' ,'ID KM1');
                        
                        OutXLSFile8 = getXLSFilename('missing_maturities_equity');
                        xlswrite(OutXLSFile8 ,Missing_UnderlyingID_Data_EQD ,'Missing_UnderlyingID_Data_EQD');
                        OutXLSFile9 = getXLSFilename('missing_underlying_equity_table_');
                        xlswrite(OutXLSFile9 ,Missing_SettleValue_Data_EQD ,'Missing_SettleValue_Data_EQD');
                        
                        try
                            xls_delete_sheets([pwd filesep OutXLSFile8]);
                            xls_delete_sheets([pwd filesep OutXLSFile9]);
                        catch
                            
                        end
                        
                    end
                    
                else
                    warndlg(['Please Select the Appropriate File as There are No Settle Prices for Equity File : ',char(DBSettleDate)]) ;
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%
                
                % check for the redundant entries if at all present for the same
                % settlement date
                [~,UniqIdx,~] = unique(EquityID,'stable');
                TempVar = zeros(size(UniqIdx));
                Value_Date = repmat(DBValueDate,size(UniqIdx));
                % TODO - Below lines are to divide OR price by 100. Remove them
                % once it is handled in database
                IdxOR = strmatch('OR',EquityID(UniqIdx));
                %         FutPrice = str2double(EquityData.FuturePrice(UniqIdx));
                EquityPrice = EquityData.FuturePrice(UniqIdx);
                EquityPrice(IdxOR) =  EquityPrice(IdxOR)/100;
                % End of TODO
                UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
                %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(EquityData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
                UndSettleData = [EquityData.AssetClass(UniqIdx),EquityID(UniqIdx),Value_Date,num2cell(EquityPrice),num2cell(TempVar),num2cell(TempVar),EquityData.SettlementDate(UniqIdx)];
                
                %% Changing Underlying ID KM1 to KM
                %             Underlying_Id_Col = cellStrfind_Perfect(UndSettleHeader,{'Underlying_Id'});
                %             KM_Index = cellStrfind(UndSettleData(:,Underlying_Id_Col),{'ID KM1'}) ;
                %
                %             if(~isempty(KM_Index))
                %                 KM_Data = UndSettleData(KM_Index,:) ;
                %
                %                 Changed_Instrument = strrep( KM_Data(:,Underlying_Id_Col) , 'ID KM1' ,'ID KM');
                %                 KM_Data(:,Underlying_Id_Col) = Changed_Instrument ;
                %
                %                 UndSettleData = [UndSettleData ; KM_Data] ;
                %
                %             end
                
                %%
                % write in xls file
                OutXLSFile10 = getXLSFilename('underlying_equity_table');
                xlswrite(OutXLSFile10 ,[UndSettleHeader;UndSettleData]);
            end
            waitbar(0.6,hWaitbar,'EQUITY Dump Finished','Name','Finished');
        end
        
    end
    
    %% FX Future File Parser
    
    if(isempty(find(strcmpi(theChosen_Cell,'FX FUTURE'))))
        errordlg('FX FUTURE Dump file not selected!','MarketData Parser');
    else
        
        FX_Future_Data_TableName = 'mktdata_fxfuture_price_table';
        SqlQuery_FX_Future = ['select * from ',FX_Future_Data_TableName,' where mkt_date = ''',char(DBSettleDate),''' '];
        [FX_Future_Header,FX_Future_Data] = Fetch_DB_Data(ObjDB_OperaDB_Channel,SqlQuery_FX_Future);
        if(strcmpi(FX_Future_Data,'No Data'))
            errordlg(['Not Getting Data from ',FX_Future_Data_TableName,' Please Check the DB'],'MarketData Parser');
        elseif(isempty(FX_Future_Data))
            errordlg(['Not Getting Data from ',FX_Future_Data_TableName,' Please Check the DB'],'MarketData Parser');
        else
            
            waitbar(0.8,hWaitbar,'Please wait, parsing FX future dump file');
            
            [FX_FutureData,InData_FX] = parse_fx_future_file(FX_Future_Data,[MappingFieldNames;Mapping_Data],BBG_UndId);
            
            if ( isempty(FX_FutureData.FutureName) && isempty(FX_FutureData.FuturePrice) )
                errordlg('FX Future Dump file is Does not have any Data  !','FTP Parser');
            else
                % ignore the records for whcih the settlement date does not
                % match with the user entered settlement date
                % To find the invalid date entries that cannot be handled by
                % datenum function
                %             InvalidIdx = strcmpi(FX_FutureData.SettlementDate,'N.A.') | strcmpi(FX_FutureData.SettlementDate,'N.A') | ...
                %                 strcmpi(FX_FutureData.SettlementDate,'') | strcmpi(FX_FutureData.SettlementDate,' ');
                %             FX_FutureData.SettlementDate(InvalidIdx) = {'01/01/1900'};
                %             if isdeployed
                %                 DumpSettleDate = cellstr(datestr(datenum(FX_FutureData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
                %             else
                %                 DumpSettleDate = cellstr(datestr(datenum(FX_FutureData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
                %             end
                %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
                
                DumpSettleDate = FX_FutureData.SettlementDate;
                IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
                
                % underlying settle value computation
                FX_FutureData.FutureName(IdxDate) = [];
                FX_FutureData.FuturePrice(IdxDate)= [];
                FX_FutureData.AssetClass(IdxDate)= [];
                FX_FutureData.ProductCode(IdxDate)= [];
                
                FX_FutureID = cell(size(FX_FutureData.FutureName));
                for iCode = 1:length(BBG_UndId)
                    %  Idx = strmatch(BBG_UndId{iCode},FutureData.FutureName); %#ok<*MATCH2>
                    Idx = strcmpi(BBG_UndId{iCode},FX_FutureData.FutureName) & strcmpi(Asset_Class{iCode},FX_FutureData.AssetClass);
                    FX_FutureID(Idx) = UndId(iCode);
                end
                
                IdxNonEmpty = cellfun(@(s) (length(s)>1), FX_FutureData.AssetClass);  %% Product Code Match in Table & InputFiles not Asset Class
                
                FX_FutureID  =  FX_FutureID(IdxNonEmpty);
                FX_FutureData.AssetClass = FX_FutureData.AssetClass(IdxNonEmpty);        %% Remove Empty Asset Class Fileds
                FX_FutureData.SettlementDate = FX_FutureData.SettlementDate(IdxNonEmpty);
                FX_FutureData.FuturePrice = FX_FutureData.FuturePrice(IdxNonEmpty);
                FX_FutureData.FutureName  = FX_FutureData.FutureName(IdxNonEmpty);
                FX_FutureData.ProductCode = FX_FutureData.ProductCode(IdxNonEmpty);
                
                %%%%%%%%%%%%%%%%%%%%%%%%% Find Missing Maturity Data %%%%%%%%%%%%%%%%%%%%%%%%%
                
                if(sum(IdxDate)==0)
                    
                    Missing_Maturities_Fx_FUT = setdiff(FX_FutureData.FileMaturities ,FX_FutureID);  % Find Missing Maturities
                    %             xlswrite(XlS_FileName_Missing_Maturity,Missing_Maturities_Fx_FUT,'Missing Fx_Future Maturities');
                    
                    if(~isempty(Missing_Maturities_Fx_FUT))
                        Missing_Index = cellStrfind_exact(FX_FutureData.FileMaturities,Missing_Maturities_Fx_FUT) ; %%
                        Missing_Maturities_Fx_FUT = FX_FutureData.InputFileMaturities(Missing_Index) ; %% Get Input FormatMissing Maturities
                        
                        [Missing_UnderlyingID_Data_FX,Missing_SettleValue_Data_FX] = Find_Missing_MaturityData_Common(InData_FX,Missing_Maturities_Fx_FUT,DBValueDate);
                        
                        OutXLSFile11 = getXLSFilename('missing_maturities_fx_future_');
                        xlswrite(OutXLSFile11 ,Missing_UnderlyingID_Data_FX ,'Missing_UnderlyingID_Data_FX');
                        OutXLSFile12 = getXLSFilename('missing_underlying_fx_future_table_');
                        xlswrite(OutXLSFile12 ,Missing_SettleValue_Data_FX ,'Missing_SettleValue_Data_FX');
                        
                        
                        try
                            xls_delete_sheets([pwd filesep OutXLSFile11]);
                            xls_delete_sheets([pwd filesep OutXLSFile12]);
                        catch
                            
                        end
                        
                    end
                    
                else
                    warndlg(['Please Select the Appropriate File as There are No Settle Prices for FX Future File : ',char(DBSettleDate)]) ;
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%$$$$$$$$$$$$$$$$$$$$$$$$$$$$%%%%%%%%%%%%%%%%%%%%%%%%%
                
                % check for the redundant entries if at all present for the same
                % settlement date
                [~,UniqIdx,~] = unique(FX_FutureID,'stable');
                FX_TempVar = zeros(size(UniqIdx));
                FX_Value_Date = repmat(DBValueDate,size(UniqIdx));
                % TODO - Below lines are to divide OR price by 100. Remove them
                % once it is handled in database
                FX_IdxOR = find(strncmp(FX_FutureID, 'OR', 2)); %%FX_IdxOR = strmatch('OR',FX_FutureID(UniqIdx));
                %         FutPrice = str2double(FutureData.FuturePrice(UniqIdx));
                FX_FutPrice = FX_FutureData.FuturePrice(UniqIdx);
                FX_FutPrice(FX_IdxOR) = FX_FutPrice(FX_IdxOR)/100;
                % End of TODO
                FX_UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
                %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(FutureData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
                FX_UndSettleData = [FX_FutureData.AssetClass(UniqIdx),FX_FutureID(UniqIdx),FX_Value_Date,num2cell(FX_FutPrice),num2cell(FX_TempVar),...
                    num2cell(FX_TempVar),FX_FutureData.SettlementDate(UniqIdx)];
                
                % write in xls file
                OutXLSFile13 = getXLSFilename('underlying_fx_future_table_');
                xlswrite(OutXLSFile13 ,[FX_UndSettleHeader;FX_UndSettleData]);
            end
            
            % Generate the traders surface data for every business unit in
            % the below segment of code; activate for every BU whenever
            % requested
            %         BUNames = {'cfs','cop','agf','qf1','qf2','qf3','qf4','qf5'};
            %         BUNames = [];
            BUNames = {'cfs','ogp','usg','og2'}; %% add ogp & usg (05-07-2019) & added OG2 on 2020-01-24
            [FX_UndTradersHeader,FX_UndTradersData] = Get_Underlying_Traders_Value_Table(BUNames,FX_UndSettleHeader,FX_UndSettleData,MappingFieldNames,Mapping_Data,DBValueDate) ;
            
            waitbar(0.8,hWaitbar,'FX FUTURE Dump Finished','Name','Finished');
            
        end
    end
    
    %% Cash Equity
    
    if(isempty(find(strcmpi(theChosen_Cell,'CASH EQUITY'))))
        errordlg('CASH EQUITY Dump file not selected!','MarketData Parser');
    else
        
        CashEquity_Data_TableName = 'mktdata_cashequity_price_table';
        SqlQuery_CashEquity = ['select * from ',CashEquity_Data_TableName,' where mkt_date = ''',char(DBSettleDate),''' '];
        [CashEquity_Header,CashEquity_Data] = Fetch_DB_Data(ObjDB_OperaDB_Channel,SqlQuery_CashEquity);
        if(strcmpi(CashEquity_Data,'No Data'))
            errordlg(['Not Getting Data from ',CashEquity_Data_TableName,' Please Check the DB'],'MarketData Parser');
        elseif(isempty(CashEquity_Data))
            errordlg(['Not Getting Data from ',CashEquity_Data_TableName,' Please Check the DB'],'MarketData Parser');
        else
            
            [CashEquity_Data] = parse_cashequity_file(CashEquity_Data,[MappingFieldNames;Mapping_Data],DBValueDate,DBSettleDate);
            
            CashEQD_UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
            
            if(~isempty(CashEquity_Data))
                OutXLSFile14 = getXLSFilename('underlying_cash_equity_table_');
                xlswrite(OutXLSFile14 ,[CashEQD_UndSettleHeader;CashEquity_Data]);
            else
                
            end
            
            waitbar(1,hWaitbar,'CASH EQUITY Dump Finished','Name','Finished');
            
        end
        
    end
    
    %%%% Newly added code
    %% FX-FORWARD
    
     if(isempty(find(strcmpi(theChosen_Cell,'FX-FORWARD'))))
        errordlg('FX-FORWARD Dump file not selected!','MarketData Parser');
     else
        ObjDB1 = connect_to_dbchannel;  %%% Conncet to OPERA Channel
        ObjDB = connect_to_database;
    
        %%Fetch data from view
        Fxforward_price_Table_Name = 'mktdata_fxforward_price_table ';
        SqlQuery_mktdata_fxforward_price = ['select bbg_ticker,px_last from ',char(Fxforward_price_Table_Name),' where mkt_date = ''',char(DBSettleDateUS),''' '] ;
        [mktdata_fxforward_price_Cols,mktdata_fxforward_price_Data] = Fetch_DB_Data(ObjDB1,SqlQuery_mktdata_fxforward_price);
    
        if strcmpi(mktdata_fxforward_price_Data,'No Data') 
           errordlg ({['Data not Available in ',char(Fxforward_price_Table_Name)]}) ;
           return;
        end
         
        fxforward_price_data = cell2table(mktdata_fxforward_price_Data,'VariableNames',mktdata_fxforward_price_Cols);  %% Convert to table
        fxforward_price_data.bbg_ticker= strrep(fxforward_price_data.bbg_ticker,'  ',' ');   % remove spaces @ bbg_ticker
        
        %%% added on 06-02-2020, For USDINR, remove + symbol in bbg_ticker
        %%% filed
        fxforward_price_data.bbg_ticker = strrep(fxforward_price_data.bbg_ticker,'+','');   % remove + @ bbg_ticker
        %%% Currency_Spot_Mapping
        Currency_bbg_Table_Name = 'currency_bbg_mapping_table';
        SqlQuery_Currency_bbg_Table = ['select currency_pair,bbg_ticker from ',char(Currency_bbg_Table_Name),' order by order_number'];
        [Currency_bbg_Table_Cols,Currency_bbg_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Currency_bbg_Table);

        if strcmpi(Currency_bbg_Table_Data,'No Data')
             errordlg({['No Data found in ',char(Currency_bbg_Table_Name)]});
            return;
        end
        
        Currency_bbg_mapping_Data = cell2table(Currency_bbg_Table_Data,'VariableNames',Currency_bbg_Table_Cols);  %% Convert to table
        
        %%% User required data
        Source = repmat({'BGN'},size(Currency_bbg_Table_Data,1),1);  %% source
        Settlement_date = repmat(DBSettleDateUS,size(Currency_bbg_Table_Data,1),1);  %% settlement date
        Currency_pair = Currency_bbg_mapping_Data.currency_pair;  %%% currency pair
        BBG_Code = Currency_bbg_mapping_Data.bbg_ticker;  %%% BBG Ticker
        Index_ticker = strcmpi(Currency_bbg_mapping_Data.bbg_ticker,'BZLABZLA BGN Curncy');
        BBG_Code(Index_ticker) = {'BZLABZLA Index'};
        
        %%% Maturity
        Maturity = Currency_bbg_mapping_Data.bbg_ticker;
        pat = {'\d'};
        Numeric_Index = regexp(Maturity, pat);
        Index_empty = cell2mat(cellfun(@(s) isempty(s),Numeric_Index,'UniformOutput',0)); 
        Maturity(~Index_empty) = cellfun(@(s) (s(4:6)),Maturity(~Index_empty),'UniformOutput',0);
        Maturity(Index_empty) = {'Spot'};
        Maturity = strtrim(Maturity);
         
        %%% To get the Harcoded_Price and Maturity_Date
        [row, ~] = size(Currency_bbg_mapping_Data); Final_Harcoded_Price = []; Final_Maturity_Date = [];
        for i=1:1:row
            Current_bbg_ticker = Currency_bbg_mapping_Data.bbg_ticker(i);
            Index_match = strcmpi(Current_bbg_ticker,fxforward_price_data.bbg_ticker);
            
            %%% %%% Maturity Date
            Single_Maturity=char(Maturity(i));
            if strcmpi(Single_Maturity,'Spot')
                Maturity_Date = cellstr(datestr(datenum(DBSettleDateUS,'yyyy-mm-dd'),'dd-mmm-yy'));
            else
                Last_ltr=Single_Maturity(end);
                First_ltr=str2double(Single_Maturity(1:end-1));
                if Last_ltr == 'W'
                    Maturity_Date = cellstr(datestr(addtodate(datenum(DBSettleDateUS,'yyyy-mm-dd'), First_ltr * 7, 'day'),'dd-mmm-yy'));
                elseif Last_ltr == 'M'
                    Maturity_Date = cellstr(datestr(addtodate(datenum(DBSettleDateUS,'yyyy-mm-dd'), First_ltr, 'month'),'dd-mmm-yy'));
                elseif Last_ltr == 'Y'
                    Maturity_Date = cellstr(datestr(addtodate(datenum(DBSettleDateUS,'yyyy-mm-dd'), First_ltr, 'year'),'dd-mmm-yy'));
                end
            end 
                
            if any(Index_match)
                Matched_Data = fxforward_price_data(Index_match,:);
                Harcoded_Price =  num2cell(Matched_Data.px_last);
            else
                Harcoded_Price = {[]};
            end
            
            Final_Harcoded_Price = vertcat(Final_Harcoded_Price,Harcoded_Price);
            Final_Maturity_Date = vertcat(Final_Maturity_Date,Maturity_Date); 
        end

        %%% Header and Data
        Fx_forward_Header = {'Currency_Pair', 'Source', 'Maturity', 'Maturity_Date', 'BBG_Code', 'Harcoded_Price', 'Settlement_date'};
        Fx_forward_Data = [Currency_pair, Source, Maturity, Final_Maturity_Date, BBG_Code, Final_Harcoded_Price, Settlement_date];

        %%% write the data into excel sheet
        OutXLSName = getXLSFilename('Fx_Forward_Report');
        xlswrite(OutXLSName,[Fx_forward_Header; Fx_forward_Data],'FX-FORWARD DATA');
        xls_delete_sheets(fullfile(pwd,OutXLSName));

     end
    %%% End Code
    
    %%%% Newly added code on 27-11-2019
    %% FX-OPTION
    
     if(isempty(find(strcmpi(theChosen_Cell,'FX-OPTION'))))
        errordlg('FX-OPTION Dump file not selected!','MarketData Parser');
     else
        ObjDB1 = connect_to_dbchannel;  %%% Conncet to OPERA DB Channel
        ObjDB = connect_to_database;
    
        %%Fetch data from mktdata_fxoption_price_table
        Fxoption_price_Table_Name = 'mktdata_fxoption_price_table';
        SqlQuery_mktdata_fxoption_price = ['select * from ',char(Fxoption_price_Table_Name),' where mkt_date = ''',char(DBSettleDateUS),''' '] ;
        [mktdata_fxoption_price_Cols,mktdata_fxoption_price_Data] = Fetch_DB_Data(ObjDB1,SqlQuery_mktdata_fxoption_price);
    
        if strcmpi(mktdata_fxoption_price_Data,'No Data') 
           errordlg ({['Data not Available in ',char(Fxoption_price_Table_Name)]}) ;
           return;
        end
         
        fxoption_price_data = cell2table(mktdata_fxoption_price_Data,'VariableNames',mktdata_fxoption_price_Cols);  %% Convert to table
        
        %%% Fetch data from currency_bbg_option_mapping_table
        Currency_bbg_Table_Name = 'currency_bbg_option_mapping_table';
        SqlQuery_Currency_bbg_Table = ['select currency_pair,forward_code,usd_yield,atm_price,riskreversal_price_25d,butterfly_price_25d from ',char(Currency_bbg_Table_Name),' order by order_number'];
        [Currency_bbg_Table_Cols,Currency_bbg_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Currency_bbg_Table);

        if strcmpi(Currency_bbg_Table_Data,'No Data')
             errordlg({['No Data found in ',char(Currency_bbg_Table_Name)]});
            return;
        end
        
        Currency_bbg_mapping_Data = cell2table(Currency_bbg_Table_Data,'VariableNames',Currency_bbg_Table_Cols);  %% Convert to table
        
        %%% User required data
        Settlement_date = repmat(DBSettleDateUS,size(Currency_bbg_Table_Data,1),1);  %% settlement date
        Currency_pair = Currency_bbg_mapping_Data.currency_pair;  %%% currency pair
        Forward_Code = Currency_bbg_mapping_Data.forward_code;  %%% forward_code
        Forward_Rate = repmat({[]},size(Currency_bbg_Table_Data,1),1); % Forward_Rate
         
        %%% Mapping procees for get usd_yield, atm_price,
        %%% riskreversal_price_25d and butterfly_price_25d
        [row, ~] = size(Currency_bbg_mapping_Data);
        Final_usd_yield = []; Final_atm_price = []; Final_rr_price_25d = []; Final_btf_price_25d = [];
        for i=1:1:row
            Current_currency_pair = Currency_bbg_mapping_Data.currency_pair(i);
            Current_forward_code = Currency_bbg_mapping_Data.forward_code(i);
            Current_usd_yield = Currency_bbg_mapping_Data.usd_yield(i);
            Current_atm_price = Currency_bbg_mapping_Data.atm_price(i);
            Current_rr_price_25d = Currency_bbg_mapping_Data.riskreversal_price_25d(i);
            Current_btf_price_25d = Currency_bbg_mapping_Data.butterfly_price_25d(i);
            
            %%% Condition for usd_yield
            Index_usd_yield = strcmpi(Current_currency_pair,fxoption_price_data.curr_pair) & strcmpi(Current_forward_code,fxoption_price_data.forward_code) & ...
                        strcmpi(Current_usd_yield,fxoption_price_data.bbg_ticker);
             
            if any(Index_usd_yield)
                usd_yield =  num2cell(fxoption_price_data.px_mid(Index_usd_yield));
            else
                usd_yield = {[]};
            end
            Final_usd_yield = vertcat(Final_usd_yield,usd_yield);
            
            %%% Condition for atm_price
            Index_atm_price = strcmpi(Current_currency_pair,fxoption_price_data.curr_pair) & strcmpi(Current_forward_code,fxoption_price_data.forward_code) & ...
                        strcmpi(Current_atm_price,fxoption_price_data.bbg_ticker);
             
            if any(Index_atm_price)
                atm_price =  num2cell(fxoption_price_data.px_mid(Index_atm_price));
            else
                atm_price = {[]};
            end
            Final_atm_price = vertcat(Final_atm_price,atm_price);
            
            %%% Condition for riskreversal_price_25d
            Index_rr_price_25d = strcmpi(Current_currency_pair,fxoption_price_data.curr_pair) & strcmpi(Current_forward_code,fxoption_price_data.forward_code) & ...
                        strcmpi(Current_rr_price_25d,fxoption_price_data.bbg_ticker);
             
            if any(Index_rr_price_25d)
                rr_price_25d =  num2cell(fxoption_price_data.px_mid(Index_rr_price_25d));
            else
                rr_price_25d = {[]};
            end
            Final_rr_price_25d = vertcat(Final_rr_price_25d,rr_price_25d);
            
            %%% Condition for butterfly_price_25d
            Index_btf_price_25d = strcmpi(Current_currency_pair,fxoption_price_data.curr_pair) & strcmpi(Current_forward_code,fxoption_price_data.forward_code) & ...
                        strcmpi(Current_btf_price_25d,fxoption_price_data.bbg_ticker);
             
            if any(Index_btf_price_25d)
                btf_price_25d =  num2cell(fxoption_price_data.px_mid(Index_btf_price_25d));
            else
                btf_price_25d = {[]};
            end
            Final_btf_price_25d = vertcat(Final_btf_price_25d,btf_price_25d);
        end

        %%% Header and Data
        Fx_option_Header = {'settlement_date', 'currency_pair', 'forward_code', 'forward_rate', 'usd_yield', 'atm_price', 'riskreversal_price_25d', 'butterfly_price_25d'};
        Fx_option_Data = [Settlement_date, Currency_pair, Forward_Code, Forward_Rate, Final_usd_yield, Final_atm_price, Final_rr_price_25d, Final_btf_price_25d];

        %%% write the data into excel sheet
        OutXLSName = getXLSFilename('Fx_Option_Report');
        xlswrite(OutXLSName,[Fx_option_Header; Fx_option_Data],'FX-OPTION DATA');
        xls_delete_sheets(fullfile(pwd,OutXLSName));

     end
    %%% End Code
    %%
    %     if(~isempty(XlS_FileName_Missing_Maturity))
    %         try
    %             xls_delete_sheets(XlS_FileName_Missing_Maturity);
    %         catch
    %         end
    %     end
    
    %%
    waitbar(1,hWaitbar,'Parser completed');
    delete(hWaitbar);
    delete(gcp('nocreate'));
    msgbox(['Files are saved successfully in the directory ',pwd],'FTP Parser');
    
    toc;
    
catch ME
    if ishandle(hWaitbar)
        delete(hWaitbar);
    end
    delete(gcp('nocreate'));
    %     errordlg(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
    errordlg( errorMessage) ;
    
end
end