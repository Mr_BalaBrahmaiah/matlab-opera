NumRows = size(InData,1);

Data = [];
for iR = 1:NumRows
    RowData = strsplit(InData{iR},'|');
    Data = [Data;RowData];
end

% data to be taken from 4th position to match with the order of  fields in the dump
Data = Data(:,4:end-1);