function [CashEquity_Data] = parse_cashequity_file(EQD_Data,Mapping_Data,DBValueDate,DBSettleDate)

% % Remove NaN Fields
% [~,~,EQD_Data] = xlsread(CashEquityPathName);
%
% Header = EQD_Data(1,:);
% Row_find_Nan = EQD_Data(:,1);
%
% Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
% EQD_Data(:,Nan_Col(:)) = [];
%
% Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
% EQD_Data(Nan_Row(:),:) = [];

%%
[System_Date_Format,~] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

%%
% EQD_Data = EQD_Data(2:end,:);

Mapping_Data = Mapping_Data(2:end,:);

EQD_Product_Code = strtrim(EQD_Data(:,2));
EQD_Price = EQD_Data(:,3);

Settlement_Date = EQD_Data(:,4);
InvalidIdx = strcmpi(Settlement_Date,'N.A.') | strcmpi(Settlement_Date,'N.A') | strcmpi(Settlement_Date,'') | strcmpi(Settlement_Date,' ');
Settlement_Date(InvalidIdx) = {'01/01/1900'};
EQD_Settlement_Date = cellstr(datestr(datenum(Settlement_Date,DB_Date_Format),DB_Date_Format)); %% System_Date_Format

%%

Asset_Class = cell(length(EQD_Product_Code),1);
Asset_Class(:,:) = {''};
Underlying_ID = cell(length(EQD_Product_Code),1);
Underlying_ID(:,:) = {''};

for i = 1 : length(Mapping_Data(:,1))
    Map_ProductCode = strtrim(Mapping_Data{i,1});
    Map_bbgCode = strtrim(Mapping_Data{i,2});
    Map_assetClass = strtrim(Mapping_Data{i,3});
    Map_ShortCode = strtrim(Mapping_Data{i,4});
    
    MatchedIndex = find(strcmpi(Map_ProductCode, EQD_Product_Code) ) ;
    
    if(~isempty(MatchedIndex))
        Asset_Class(MatchedIndex,1) = cellstr(Map_assetClass);
        Underlying_ID(MatchedIndex,1) = strcat(Map_ShortCode,{' '},Map_ProductCode);
    end
    
end

%%
Temp_Cell = cell(length(EQD_Product_Code),1);
Temp_Cell(:,:) = {0};
EQD_Value_Date = cell(length(EQD_Product_Code),1);
EQD_Value_Date(:,:) = {char(DBValueDate)};

CashEquity_Data = [Asset_Class,Underlying_ID,EQD_Value_Date,EQD_Price,Temp_Cell,Temp_Cell,EQD_Settlement_Date];

%%

IdxDate = ~ismember(EQD_Settlement_Date,DBSettleDate);
CashEquity_Data(IdxDate,:) = [];


end