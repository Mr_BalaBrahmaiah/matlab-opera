function [AssetClass_Array] = Get_Asset_Class_4_CellArray(ProductCode_Array,ShortCode_Array,Mapping_Data)

ProductCode_Array = strtrim(ProductCode_Array);

AssetClass_Array = cell(size(ProductCode_Array,1),1) ;

%%

MappingFieldNames = Mapping_Data(1,:);
Mapping_Data = Mapping_Data(2:end,:);

ProductCode_Col = cellStrfind_exact(MappingFieldNames,{'product_code'}) ;
AssetClass_Col = cellStrfind_exact(MappingFieldNames,{'asset_class'}) ;
ShortCode_Col = cellStrfind_exact(MappingFieldNames,{'short_code'}) ;

%%

for i = 1 : length(Mapping_Data(:,1))
    Map_ProductCode = strtrim(Mapping_Data{i,ProductCode_Col});    
    Map_AssetClass = strtrim(Mapping_Data{i,AssetClass_Col});
    Map_ShortCode = strtrim(Mapping_Data{i,ShortCode_Col});
    
    MatchedIndex = find(strcmpi(Map_ProductCode, ProductCode_Array) & strcmpi(Map_ShortCode, ShortCode_Array));
    
    if(~isempty(MatchedIndex))
        AssetClass_Array(MatchedIndex,1) = cellstr(Map_AssetClass);
    end
    
end

end