function OutData = parse_future_file(FileName,BBG_UndId)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:24:36 $
%  $Revision: 1.1 $
%

% RawData = textread(FileName,'%s','delimiter','\n');
% 
% % Below lines are needed to remove the lines where values are not fetched
% % from bloomberg
% IdxRemove = strmatch('SECTYP',RawData);
% RawData(IdxRemove) = [];
% 
% Start_Of_Data = strmatch('START-OF-DATA',RawData);
% End_Of_Data = strmatch('END-OF-DATA',RawData);
% IdxData = (Start_Of_Data + 1) : (End_Of_Data - 1);
% InData = RawData(IdxData);
% 
% clear RawData; clear IdxData;

%% Remove NaN Fields
[~,~,InData] = xlsread(FileName);

Header = InData(1,:);
Row_find_Nan = InData(:,1);

Nan_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col); 
InData(:,Nan_Col(:)) = [];

Nan_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row); 
InData(Nan_Row(:),:) = [];

%%

InData = InData(2:end,:);

SearchPattern = strjoin(strcat('^',BBG_UndId,'.')','|');

Index = regexp(InData(:,2),SearchPattern);
TempIndex = ~(cellfun(@isempty,Index));
 
OutData.FutureName = strtrim(InData(TempIndex,2));
OutData.FuturePrice = cell2mat(InData(TempIndex,4));
OutData.SettlementDate = InData(TempIndex,5);

% OutData.FutureName = InData(TempIndex,2);
% OutData.FuturePrice = InData(TempIndex,4);
% OutData.SettlementDate = InData(TempIndex,5);

end




