function varargout = fx_forward_option_report(varargin)
% FX_FORWARD_OPTION_REPORT MATLAB code for fx_forward_option_report.fig
%      FX_FORWARD_OPTION_REPORT, by itself, creates a new FX_FORWARD_OPTION_REPORT or raises the existing
%      singleton*.
%
%      H = FX_FORWARD_OPTION_REPORT returns the handle to a new FX_FORWARD_OPTION_REPORT or the handle to
%      the existing singleton*.
%
%      FX_FORWARD_OPTION_REPORT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FX_FORWARD_OPTION_REPORT.M with the given input arguments.
%
%      FX_FORWARD_OPTION_REPORT('Property','Value',...) creates a new FX_FORWARD_OPTION_REPORT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before fx_forward_option_report_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to fx_forward_option_report_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help fx_forward_option_report

% Last Modified by GUIDE v2.5 30-Dec-2019 12:29:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @fx_forward_option_report_OpeningFcn, ...
                   'gui_OutputFcn',  @fx_forward_option_report_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before fx_forward_option_report is made visible.
function fx_forward_option_report_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to fx_forward_option_report (see VARARGIN)

% Choose default command line output for fx_forward_option_report
handles.output = hObject;
myimage= imread('olam_logo.jpg');
axes(handles.axes1);
imshow(myimage);
set(handles.axes1,'Units','normalized');

objDB = connect_to_database;
DBData = fetch(objDB,'select settlement_date from valuation_date_table');
set(handles.text3,'string',DBData);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes fx_forward_option_report wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = fx_forward_option_report_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in fx_forward.
function fx_forward_Callback(hObject, eventdata, handles)
% hObject    handle to fx_forward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[jObj] = Job_Start();
drawnow;

fx_forward_interpolation

Job_Done(jObj);

% --- Executes on button press in pushbutton2.
function fx_option_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

fx_option_calculation

Job_Done(jObj);
