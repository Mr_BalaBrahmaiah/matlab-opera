
[filename, pathname] = uigetfile('*.xlsx', 'Select the FX-Option input File');
if isequal(filename,0)
    msgbox('User selected Cancel')
else
    fullfilepath=fullfile(pathname, filename);
    [~,~,raw]=xlsread(fullfilepath);
end

OutXLSName = getXLSFilename('fx_option_parser');

headers =raw(1,:);
data =raw(3:end,:);

id_forward =find(strcmpi(headers,'forward_code'));
id_price=find(strcmpi(headers,'atm_price'));
id_risk =find(strcmpi(headers,'riskreversal_price_25d'));
id_butterfly=find(strcmpi(headers,'butterfly_price_25d'));

curre_data= data(:,id_forward);
x=1:length(curre_data);
x=x';
required_data = data( :,[ id_price id_risk id_butterfly ]);
id_empty =cell2mat(cellfun(@isnan ,  required_data(:,2),'uni',0)) ;

if find(id_empty ==1) 
    required_data=[x cell2mat(required_data)];
    cal_data =required_data(id_empty,:);
    training_data =required_data(~id_empty,:);
    predit_price= interp1( training_data(:,1), training_data(:,2) , x(id_empty) ,'linear', 'extrap');
    predit_price1= interp1( training_data(:,1), training_data(:,3) , x(id_empty) ,'linear', 'extrap');
    predit_price2= interp1( training_data(:,1), training_data(:,4) , x(id_empty) ,'linear', 'extrap');    
    required_data(id_empty,2)=  predit_price;
    required_data(id_empty,3)=  predit_price1;
    required_data(id_empty,4)=  predit_price2;
    required_data(:,1)=  [];
    data(:,[ id_price id_risk id_butterfly ])=num2cell(required_data);
   data=[raw(1:2,:);data];
end

xlswrite(OutXLSName,data);

msgbox(['Files are saved successfully in the directory ',pwd],'FX-Option');
