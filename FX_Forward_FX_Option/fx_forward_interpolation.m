

[filename, pathname] = uigetfile('*.xlsx', 'Select the FX-Forward input File');
if isequal(filename,0)
    msgbox('User selected Cancel')
else
    fullfilepath=fullfile(pathname, filename);
    [~,~,raw]=xlsread(fullfilepath);
end

OutXLSName = getXLSFilename('fx_forward_parser');

headers =raw(1,:);
data =raw(2:end,:);

id_curr=strcmpi(headers,'Currency_Pair');
id_maturity =find(strcmpi(headers,'Maturity'));
id_price=find(strcmpi(headers,'Harcoded_Price'));
curre= data(:,id_curr);
curre=[curre;'spot'];
maturity_date =data(:,id_maturity);
maturity_date=[maturity_date;'spot'];
required_currency =cell((length(maturity_date)-1),1);

%%% replacing spot currency in to below currency
for m=1:length(maturity_date)-1
    current_id =maturity_date(m);
    if strcmpi(current_id ,'spot')
        if  strcmpi(maturity_date(m+1),'spot')
            required_currency(m,1)=curre(m);
        else
            required_currency(m,1)=curre(m+1);
        end
    else
        required_currency(m,1)=curre(m);
    end
end
data=[required_currency data];
headers =['new_curr' headers] ;
id_ncurr=strcmpi(headers,'new_curr');
unique_curr=unique(data(:,id_ncurr));

id_maturity =find(strcmpi(headers,'Maturity'));
id_price=find(strcmpi(headers,'Harcoded_Price'));

id_indexcol=find(cell2mat(cellfun(@isnan ,  data(:,id_price),'uni',0))) ;
id_indexcol =id_indexcol+1;

%%% loop for number of currency
for curr=1:size(unique_curr,1)
    
    current_currency=unique_curr(curr);
    id_current = strcmpi(data(:,id_ncurr),current_currency);
    required_data =data(  id_current,[id_maturity id_price]);
    id_empty =cell2mat(cellfun(@isnan ,  required_data(:,2),'uni',0)) ;
    
    if find(id_empty ==1)
        cal_data =required_data(id_empty,1);
        training_data =required_data(~id_empty,:);
        if size(training_data ,1)>=2
            x=1:1800;
            y=zeros(size(x,2),1);
            x=x';
            for d=1:size(training_data,1)
                week_cal=cell2mat(training_data(d,1));
                if strcmpi(week_cal(end),'t')
                    y(1)=cell2mat(training_data(d,2));
                elseif strcmpi(week_cal(end),'W')
                    required_number=str2double(week_cal(1:end-1))*7;
                    y(required_number)=cell2mat(training_data(d,2));
                elseif strcmpi(week_cal(end),'M')
                    required_number=round(str2double(week_cal(1:end-1))*30);
                    y(required_number)=cell2mat(training_data(d,2));
                elseif strcmpi(week_cal(end),'y')
                    required_number=round(str2double(week_cal(1:end-1))*360);
                    y(required_number)=cell2mat(training_data(d,2));
                end
            end
            % interpolation for data
            id_emptydata=ismember(y,0);
            traing_data1= [x(~id_emptydata) y(~id_emptydata)];
            predit_price= interp1(traing_data1(:,1), traing_data1(:,2), x(id_emptydata) ,'linear', 'extrap');
            
            y(id_emptydata)=predit_price;
            for k =1:size(required_data,1)
                curre_dat= cell2mat(required_data(k,1));
                if strcmpi(week_cal(end),'t')
                    y(1)=cell2mat(training_data(d,2));
                elseif strcmpi(  curre_dat(end),'W')
                    required_number=str2double( curre_dat(1:end-1))*7;
                    required_data(k,2)=num2cell(y(required_number));
                elseif strcmpi(  curre_dat(end),'M')
                    required_number=round(str2double( curre_dat(1:end-1))*30);
                    required_data(k,2)=num2cell(y(required_number));
                elseif strcmpi(curre_dat(end),'y')
                    required_number=round(str2double(curre_dat(1:end-1))*360);
                    required_data(k,2)=num2cell(y(required_number));
                end
                
            end
        else
            continue;
        end
    else
        continue;
    end
    data(id_current,id_maturity)=required_data(:,1);
    data(id_current,id_price)=required_data(:,2);
end

xlswrite(OutXLSName,[headers;data])
%%% filling color and making bold
for g =1:length(id_indexcol)
    range =['G' num2str(id_indexcol(g))];
    xlsfont(OutXLSName,'Sheet1',range,'interior',6,1,6,'fontstyle','bold');
end

msgbox(['Files are saved successfully in the directory ',pwd],'FX-Forward');
