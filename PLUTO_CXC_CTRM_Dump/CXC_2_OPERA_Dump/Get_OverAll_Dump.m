function [OutXLSName] = Get_OverAll_Dump(BU_Name,Pr_Code,Live_Only,Live_Dead)

%% Connect to DB
ObjDB = connect_to_database;
ObjDB_Oracle = connect_to_database_MS_SQL;

Default_OperaView = 'deal_ticket_maturity_view_' ;
ViewName = strcat(Default_OperaView,BU_Name);


Today_Date = today ; %% 736770 ; %% 736788 ; %% today %%736785;

%% Get Excel Name

OutXLSName = getXLSFilename(strcat('Overall_Dump_CXC_OPERA_',BU_Name));

if(strcmpi(BU_Name,'plm'))
    Recon_ReplaceStr = 'PLM-';
elseif(strcmpi(BU_Name,'rbr'))
    Recon_ReplaceStr = 'RBR-';
elseif(strcmpi(BU_Name,'edl'))
    Recon_ReplaceStr = 'EDL-';
else
    
end

%%
if(Live_Only)
    
    if(strcmpi(Pr_Code,'PLM'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    elseif(strcmpi(Pr_Code,'RBR'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''CJ%''  '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''CJ%'' '];
    elseif(strcmpi(Pr_Code,'EDL'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];

    else
        
    end
    
else %% Live & Dead
    
    if(strcmpi(Pr_Code,'PLM'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') '];
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
    elseif(strcmpi(Pr_Code,'RBR'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') '];
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and ops_unique not like ''CJ%'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and ops_unique not like ''CJ%'' '];
    elseif(strcmpi(Pr_Code,'EDL'))
        CXC_FUT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        CXC_OPT_SqlQuery = ['select * from CXC_TRADE_DETAIL_FOR_OPERA where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
        
    else
        
    end
    
end

%% Fetch Data

[CXC_FUT_FieldNames,CXC_FUT_DBData] = Fetch_DB_Data(ObjDB_Oracle,CXC_FUT_SqlQuery);
[CXC_OPT_FieldNames,CXC_OPT_DBData] = Fetch_DB_Data(ObjDB_Oracle,CXC_OPT_SqlQuery);

OPT_LOT_Col = cellStrfind_exact(CXC_FUT_FieldNames,{'LOT'});
LYADJ_OT_Col = cellStrfind_exact(CXC_FUT_FieldNames,{'LYADJ_OT'});
LYBROK_ADJ_Col = cellStrfind_exact(CXC_FUT_FieldNames,{'LYBROK_ADJ'});
FUT_UnitWise_Lots = num2cell(cell2mat(CXC_FUT_DBData(:,OPT_LOT_Col)) - cell2mat(CXC_FUT_DBData(:,LYADJ_OT_Col)));
FUT_BrokerWise_Lots = num2cell(cell2mat(CXC_FUT_DBData(:,OPT_LOT_Col)) - cell2mat(CXC_FUT_DBData(:,LYBROK_ADJ_Col)));
CXC_FUT_FieldNames = [CXC_FUT_FieldNames ,{'Calc_UnitWise_Lots','Calc_BrokerWise_Lots'}];
CXC_FUT_DBData = [CXC_FUT_DBData ,FUT_UnitWise_Lots , FUT_BrokerWise_Lots];

OPT_LOT_Col = cellStrfind_exact(CXC_OPT_FieldNames,{'LOT'});
LYBROK_ADJ_Col = cellStrfind_exact(CXC_OPT_FieldNames,{'LYBROK_ADJ'});
OPT_UnitWise_Lots = CXC_OPT_DBData(:,OPT_LOT_Col);
OPT_BrokerWise_Lots = num2cell(cell2mat(CXC_OPT_DBData(:,OPT_LOT_Col)) - cell2mat(CXC_OPT_DBData(:,LYBROK_ADJ_Col)));
CXC_OPT_FieldNames = [CXC_OPT_FieldNames , {'Calc_UnitWise_Lots','Calc_BrokerWise_Lots'}];
CXC_OPT_DBData = [CXC_OPT_DBData , OPT_UnitWise_Lots ,OPT_BrokerWise_Lots];

[Opera_FUT_FieldNames,Opera_FUT_DBData] = Fetch_DB_Data(ObjDB,Opera_FUT_SqlQuery);
[Opera_OPT_FieldNames,Opera_OPT_DBData] = Fetch_DB_Data(ObjDB,Opera_OPT_SqlQuery);

CXC_FUT_UniqueCol = cellStrfind_exact(CXC_FUT_FieldNames,{'UNIQ_NO'});
CXC_FUT_UniqueData = CXC_FUT_DBData(:,CXC_FUT_UniqueCol);

CXC_OPT_UniqueCol = cellStrfind_exact(CXC_OPT_FieldNames,{'UNIQ_NO'});
CXC_OPT_UniqueData = CXC_OPT_DBData(:,CXC_OPT_UniqueCol);

Opera_FUT_UniqueCol = cellStrfind_exact(Opera_FUT_FieldNames,{'ops_unique'});
Opera_FUT_UniqueData = Opera_FUT_DBData(:,Opera_FUT_UniqueCol);

Opera_OPT_UniqueCol = cellStrfind_exact(Opera_OPT_FieldNames,{'ops_unique'});
Opera_OPT_UniqueData = Opera_OPT_DBData(:,Opera_OPT_UniqueCol);

Overall_CXC_Data = [CXC_FUT_UniqueData ; CXC_OPT_UniqueData];
Overall_OPERA_Data = [Opera_FUT_UniqueData ; Opera_OPT_UniqueData];

%% Find Missing Trades
if(~isempty(Overall_CXC_Data) && ~isempty(Overall_OPERA_Data))
    CXC_NotExisting_OPERA = setdiff(Overall_CXC_Data,Overall_OPERA_Data);
    OPERA_NotExisting_CXC  = setdiff(Overall_OPERA_Data,Overall_CXC_Data);
else
    if(isempty(Overall_CXC_Data))
        Overall_CXC_Data = {''};
    end
    
    if(isempty(Overall_OPERA_Data))
        Overall_OPERA_Data = {''};
    end
    CXC_NotExisting_OPERA = setdiff(Overall_CXC_Data,Overall_OPERA_Data);
    OPERA_NotExisting_CXC  = setdiff(Overall_OPERA_Data,Overall_CXC_Data);
    
end


%% FUT Recon

CXC_FUT_Needed_Fileds = {'UNIQ_NO','UNIT_SD','Calc_UnitWise_Lots','Calc_BrokerWise_Lots'};
CXC_FUT_ReconData = CXC_FUT_DBData(:,cellStrfind_exact(CXC_FUT_FieldNames,CXC_FUT_Needed_Fileds));
UNIQ_NO_Col = cellStrfind_exact(CXC_FUT_Needed_Fileds,{'UNIQ_NO'});
CXC_NotExisting_OPERA_Index = cellStrfind_exact(CXC_FUT_ReconData(:,UNIQ_NO_Col),CXC_NotExisting_OPERA);


Opera_FUT_Needed_Fields = {'trade_id','ops_unique','original_lots','subportfolio','broker_lots'};
Opera_FUT_ReconData = Opera_FUT_DBData(:,cellStrfind_exact(Opera_FUT_FieldNames,Opera_FUT_Needed_Fields));
CXC_Unique_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'ops_unique'});
OPERA_NotExisting_CXC_Index = cellStrfind_exact(Opera_FUT_ReconData(:,CXC_Unique_Col),OPERA_NotExisting_CXC);

CXC_FUT_ReconData(CXC_NotExisting_OPERA_Index,:) = [];
Opera_FUT_ReconData(OPERA_NotExisting_CXC_Index,:) = [];

if(size(CXC_FUT_ReconData,1) == size(Opera_FUT_ReconData,1))
    
    UniqueNum_OrderIndex = cellStrfind_exact(CXC_FUT_ReconData(:,1),Opera_FUT_ReconData(:,2));
    CXC_FUT_ReconData = CXC_FUT_ReconData(UniqueNum_OrderIndex,:); %% Making Order for Subtraction
    
    CXC_UnitLots_Col = cellStrfind_exact(CXC_FUT_Needed_Fileds,{'Calc_UnitWise_Lots'});
    Opera_OriginalLots_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'original_lots'});
    FUT_Lots_Diff = num2cell(cell2mat(CXC_FUT_ReconData(:,CXC_UnitLots_Col)) - cell2mat(Opera_FUT_ReconData(:,Opera_OriginalLots_Col)));
    
    CXC_UnitSD_Col = cellStrfind_exact(CXC_FUT_Needed_Fileds,{'UNIT_SD'});
    Opera_Subportfolio_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'subportfolio'});
    Opera_FUT_ReconData(:,Opera_Subportfolio_Col) = strrep(Opera_FUT_ReconData(:,Opera_Subportfolio_Col),Recon_ReplaceStr,''); %% Remove Opera Extension if Cottton Like 'CT-' will be removed for Matching
    Subportfolio_Diff = num2cell(strcmpi(CXC_FUT_ReconData(:,CXC_UnitSD_Col) ,Opera_FUT_ReconData(:,Opera_Subportfolio_Col)));
    
    CXC_BrokerLots_Col = cellStrfind_exact(CXC_FUT_Needed_Fileds,{'Calc_BrokerWise_Lots'});
    Opera_BrokerLots_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'broker_lots'});
    FUT_BrokerLots_Diff = num2cell(cell2mat(CXC_FUT_ReconData(:,CXC_BrokerLots_Col)) - cell2mat(Opera_FUT_ReconData(:,Opera_BrokerLots_Col)));
    
    CXC_FUT_Needed_Fileds = [{'CXC','','',''} ; CXC_FUT_Needed_Fileds];
    Opera_FUT_Needed_Fields = [{'OPERA','','','',''} ; Opera_FUT_Needed_Fields];
    Recon_Header = [{'Recon','',''} ; {'OriginalLots_Difference','Subportfolio matching','BrokerLots_Difference'} ] ;
    
    Temp_Cell = cell(size(Opera_FUT_ReconData,1),2);
    Temp_Header = cell(2,size(Temp_Cell,2));
    
    FUT_Recon_Header = [CXC_FUT_Needed_Fileds , Temp_Header , Opera_FUT_Needed_Fields , Temp_Header , Recon_Header];
    FUT_Recon_OverallData = [CXC_FUT_ReconData ,Temp_Cell ,Opera_FUT_ReconData , Temp_Cell , FUT_Lots_Diff , Subportfolio_Diff , FUT_BrokerLots_Diff];
    
    xlswrite(OutXLSName,[FUT_Recon_Header ; FUT_Recon_OverallData],'FUT RECON');
else
    xlswrite(OutXLSName,{'CXC_FUT Count and Opera_FUT Count is Mismatched So Unable to Create FUT Recon Dump'},'FUT RECON');
end

%% OPTION Recon

CXC_OPT_Needed_Fileds = {'UNIQ_NO','UNIT_SD','Calc_UnitWise_Lots','Calc_BrokerWise_Lots'};
CXC_OPT_ReconData = CXC_OPT_DBData(:,cellStrfind_exact(CXC_OPT_FieldNames,CXC_OPT_Needed_Fileds));
UNIQ_NO_Col = cellStrfind_exact(CXC_OPT_Needed_Fileds,{'UNIQ_NO'});
CXC_NotExisting_OPERA_Index = cellStrfind_exact(CXC_OPT_ReconData(:,UNIQ_NO_Col),CXC_NotExisting_OPERA);

Opera_OPT_Needed_Fields = {'trade_id','ops_unique','original_lots','subportfolio','broker_lots'};
Opera_OPT_ReconData = Opera_OPT_DBData(:,cellStrfind_exact(Opera_OPT_FieldNames,Opera_OPT_Needed_Fields));
CXC_Unique_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'ops_unique'});
OPERA_NotExisting_CXC_Index = cellStrfind_exact(Opera_OPT_ReconData(:,CXC_Unique_Col),OPERA_NotExisting_CXC);

CXC_OPT_ReconData(CXC_NotExisting_OPERA_Index,:) = [];
Opera_OPT_ReconData(OPERA_NotExisting_CXC_Index,:) = [];

if(size(CXC_OPT_ReconData,1) == size(Opera_OPT_ReconData,1))
    
    UniqueNum_OrderIndex = cellStrfind_exact(CXC_OPT_ReconData(:,1),Opera_OPT_ReconData(:,2));
    CXC_OPT_ReconData = CXC_OPT_ReconData(UniqueNum_OrderIndex,:); %% Making Order for Subtraction
    
    CXC_UnitLots_Col = cellStrfind_exact(CXC_OPT_Needed_Fileds,{'Calc_UnitWise_Lots'});
    Opera_OriginalLots_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'original_lots'});
    OPT_Lots_Diff = num2cell(cell2mat(CXC_OPT_ReconData(:,CXC_UnitLots_Col)) - cell2mat(Opera_OPT_ReconData(:,Opera_OriginalLots_Col)));
    
    CXC_UnitSD_Col = cellStrfind_exact(CXC_OPT_Needed_Fileds,{'UNIT_SD'});
    Opera_Subportfolio_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'subportfolio'});
    Opera_OPT_ReconData(:,Opera_Subportfolio_Col) = strrep(Opera_OPT_ReconData(:,Opera_Subportfolio_Col),Recon_ReplaceStr,''); %% Remove Opera Extension if Cottton Like 'CT-' will be removed for Matching
    Subportfolio_Diff = num2cell(strcmpi(CXC_OPT_ReconData(:,CXC_UnitSD_Col) ,Opera_OPT_ReconData(:,Opera_Subportfolio_Col)));
    
    CXC_BrokerLots_Col = cellStrfind_exact(CXC_OPT_Needed_Fileds,{'Calc_BrokerWise_Lots'});
    Opera_BrokerLots_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'broker_lots'});
    OPT_BrokerLots_Diff = num2cell(cell2mat(CXC_OPT_ReconData(:,CXC_BrokerLots_Col)) - cell2mat(Opera_OPT_ReconData(:,Opera_BrokerLots_Col)));
    
    CXC_OPT_Needed_Fileds = [{'CXC','','',''} ; CXC_OPT_Needed_Fileds];
    Opera_OPT_Needed_Fields = [{'OPERA','','','',''} ; Opera_OPT_Needed_Fields];
    Recon_Header = [{'Recon','',''} ; {'OriginalLots_Difference','Subportfolio matching','BrokerLots_Difference'} ] ;
    
    Temp_Cell = cell(size(Opera_OPT_ReconData,1),2);
    Temp_Header = cell(2,size(Temp_Cell,2));
    
    OPT_Recon_Header = [CXC_OPT_Needed_Fileds , Temp_Header , Opera_OPT_Needed_Fields , Temp_Header , Recon_Header];
    OPT_Recon_OverallData = [CXC_OPT_ReconData ,Temp_Cell ,Opera_OPT_ReconData , Temp_Cell , OPT_Lots_Diff , Subportfolio_Diff, OPT_BrokerLots_Diff];
    
    xlswrite(OutXLSName,[OPT_Recon_Header ; OPT_Recon_OverallData],'OPT RECON');
else
    xlswrite(OutXLSName,{'CXC_OPT Count and Opera_OPT Count is Mismatched So Unable to Create OPT Recon Dump'},'OPT RECON');
end


%% Excel Write


xlswrite(OutXLSName,[CXC_FUT_FieldNames;CXC_FUT_DBData],'CXC_FUT_DUMP');
xlswrite(OutXLSName,[CXC_OPT_FieldNames;CXC_OPT_DBData],'CXC_OPT_DUMP');
xlswrite(OutXLSName,[Opera_FUT_FieldNames;Opera_FUT_DBData],'OPERA_FUT_DUMP');
xlswrite(OutXLSName,[Opera_OPT_FieldNames;Opera_OPT_DBData],'OPERA_OPT_DUMP');

if(~isempty(OPERA_NotExisting_CXC))
    xlswrite(OutXLSName,OPERA_NotExisting_CXC,'OPERA_NotExisting_CXC');
else
    xlswrite(OutXLSName,{'No Data OPERA and CXC Matched'},'OPERA_NotExisting_CXC');
end

if(~isempty(CXC_NotExisting_OPERA))
    
    xlswrite(OutXLSName,CXC_NotExisting_OPERA,'CXC_NotExisting_OPERA');
    
    CXC_FUT_Data = CXC_FUT_DBData(cellStrfind_exact(CXC_FUT_UniqueData,CXC_NotExisting_OPERA),:);
    if(~isempty(CXC_FUT_Data))
        %         [TRHeader_FUT,RowData_FUT] = Convert_CXC_FUT_2_TR_Format(CXC_FUT_FieldNames,CXC_FUT_Data,BU_Name);
        CXC_Action_Str = 'NEW';
        [TRHeader_FUT , RowData_FUT ] = CXC_Future_2_TR_Format(CXC_FUT_FieldNames , CXC_FUT_Data , CXC_Action_Str,BU_Name);
        xlswrite(OutXLSName,[TRHeader_FUT;RowData_FUT],'CXC_FUT_TR_Format');
    end
    
    
    CXC_OPT_Data = CXC_OPT_DBData(cellStrfind_exact(CXC_OPT_UniqueData,CXC_NotExisting_OPERA),:);
    if(~isempty(CXC_OPT_Data))
        %         [TRHeader_OPT,RowData_OPT] = Convert_CXC_OPT_2_TR_Format(CXC_OPT_FieldNames,CXC_OPT_Data,BU_Name);
        CXC_Action_Str = 'NEW';
        [TRHeader_OPT , RowData_OPT ] = CXC_Option_2_TR_Format(CXC_OPT_FieldNames , CXC_OPT_Data ,CXC_Action_Str ,BU_Name);
        xlswrite(OutXLSName,[TRHeader_OPT;RowData_OPT],'CXC_OPT_TR_Format');
    end
    
else
    xlswrite(OutXLSName,{'No Data CXC and OPERA Matched'},'CXC_NotExisting_OPERA');
    
end


OutXLSName = fullfile(pwd,OutXLSName) ;
xls_delete_sheets(OutXLSName) ;

end




%%
