function [Min_Opera_Date] = Find_Minimum_CXC_ViewDate(Prod_Code,ignored_marketP,Start_Date,End_Date)

% Prod_Code = 'CT';

ObjDB_MS_SQL = connect_to_database_MS_SQL ;

%%

SqlQuery1 = ['select cast(min(CONTRACT_DATE) as date) from CXC_TRADE_DETAIL_FOR_OPERA where ctr_book in (',Prod_Code,') and market_price_index not in (',ignored_marketP,') and CONTRACT_STATUS = ''','Confirmed',''' and ((cast(CONTRACT_DATE as date) >= ''',Start_Date,''' and cast(CONTRACT_DATE as date) <= ''',End_Date,''') or (cast(LASTUPDATE as date) >= ''',Start_Date,''' and cast(LASTUPDATE as date) <= ''',End_Date,'''))'] ;
SqlQuery2 = ['select cast(min(CONTRACT_DATE) as date) from CXC_TRADE_DETAIL_FOR_OPERA where ctr_book in (',Prod_Code,') and market_price_index not in (',ignored_marketP,') and CONTRACT_STATUS = ''','Fulfilled',''' and ((cast(CONTRACT_DATE as date) >= ''',Start_Date,''' and cast(CONTRACT_DATE as date) <= ''',End_Date,''') or (cast(LASTUPDATE as date) >= ''',Start_Date,''' and cast(LASTUPDATE as date) <= ''',End_Date,'''))'] ;
SqlQuery3 = ['select cast(min(CONTRACT_DATE) as date) from CXC_TRADE_DETAIL_FOR_OPERA where ctr_book in (',Prod_Code,') and market_price_index not in (',ignored_marketP,') and CONTRACT_STATUS in (''','Voided',''') and ((cast(CONTRACT_DATE as date) >= ''',Start_Date,''' and cast(CONTRACT_DATE as date) <= ''',End_Date,''') or (cast(LASTUPDATE as date) >= ''',Start_Date,''' and cast(LASTUPDATE as date) <= ''',End_Date,'''))'] ;

%%

Confirmed_Date = fetch(ObjDB_MS_SQL,SqlQuery1) ;
Fulfilled_Date = fetch(ObjDB_MS_SQL,SqlQuery2) ;
Voided_Date = fetch(ObjDB_MS_SQL,SqlQuery3) ;

All_Dates = [Confirmed_Date ; Fulfilled_Date ; Voided_Date] ;
All_Dates = All_Dates(cellfun(@(s) (length(s)>1), All_Dates)) ;  %% Remove Empty Date Fields

Min_All_Dates = min(datenum(All_Dates)) ;
Min_CXC_Date = datestr(Min_All_Dates,'dd-mmm-yyyy') ;

%%

[System_Date_Format,~] = get_System_Date_Format();

HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));

Opera_Date_Str = datestr(busdate(today-105,-1,HolidayVec),1);
Opera_Date_Num = datenum(Opera_Date_Str);

if(Opera_Date_Num < Min_All_Dates)
    Min_Opera_Date = Opera_Date_Str ;
else
    Min_Opera_Date = Min_CXC_Date ;
end

%%

close(ObjDB_MS_SQL) ;
delete(ObjDB_MS_SQL) ;

end
