function chrString = convertCell2Char(ceaString)
% Converts the cellarray of strings into character array of single line
% This function has to be used before INIWRITE, since the function doesnot 
% allow to write the cell array of strings or character array having more
% than one line
%
%% Syntax:
%  =======
%    chrString = convertCell2Char(ceaString)
% 
%% Input Arguments:
%  ================
%   ceaString - <cellArray> of Strings to be written in INIFile using
%   INIWRITE function from INIFileManager Library
% 
%% Return Values:
%  ==============
%  chrString - <charArray> of string which can be written in INIFile using
%  INIWRITE function from INIFileManager Library

chrString = '';

for iStr = 1:length(ceaString)
    if isempty(chrString)
        chrString = char(ceaString{iStr});
    else
        chrString = [chrString,',',char(ceaString{iStr})]; %#ok<AGROW>
    end
end