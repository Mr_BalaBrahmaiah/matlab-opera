function [OutErrorMsg,LastTradeId]= getLastTradeId(InBUName)

OutErrorMsg = {'No errors'};
LastTradeId = {''};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
    
    TableName =['cloned_deal_ticket_table_',InBUName];
    SqlQuery = ['select mid(trade_id,12) from ',TableName,' where trade_id like ''',TradeIdPrefix,'%'''];
    DBTradeId = fetch(ObjDB,SqlQuery);
    
    TempTradeNum = max(str2num(char(DBTradeId)));
    
    if isempty(TempTradeNum)
        TempTradeNum = 0;
    end
    
    LastTradeId = cellstr([TradeIdPrefix,num2str(TempTradeNum)]);
catch ME
    OutErrorMsg = cellstr(ME.message);
end