function [TRHeader , Overall_TRData ,New_OutTRData , Mod_OutTRData] = PLUTO_Future_2_Opera(Business_Unit,Date_Str,Start_Date,End_Date ,OutXLSName)


% Business_Unit = 'CT';
% Date_Str = '2017-01-01'; %%HolDates = {'2016-01-01','2017-01-01'};HolidayVec = datenum(HolDates,'yyyy-mm-dd'); Date_Str = datestr(busdate(today-90,-1,HolidayVec),'yyyy-mm-dd')
% Start_Date = datestr(today-2) ;
% End_Date = datestr(today);


TRHeader = '';
Overall_TRData ='';
New_OutTRData ='';
Mod_OutTRData = '';

Instrument_Error_Data_New = '';
Instrument_Error_Data_Mod = '';

%%
Commodity = 'FUT';

[SqlQuery_1,SqlQuery_2,SqlQuery_3,SqlQuery_4,SqlQuery_5,InBUName] = Find_Product_SQL_Queries(Business_Unit,Date_Str,Start_Date,End_Date,Commodity);

%% Fetch deal_ticket_table_with_latest_versions_view_cot
% SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM deal_ticket_table_with_latest_versions_view_cot where transaction_date >=''', Date_Str ,''' '];

[DBFieldNames,DBData] = read_from_database(0,0,SqlQuery_1);

%% Fetch PLUTO DB
if( size(DBData,2) > 1 )
    
    %     SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'',''ICUS'')' ];
    % SqlQuery  = 'select * from op_fut_new where pr_code=''CT'' and ex_code not in (''MTS'',''ICUS'') and ((trunc(deal_dt) >= ''15-Feb-2017'' and trunc(deal_dt) <= ''17-Feb-2017'') or ( trunc(sys_date) >= ''15-Feb-2017'' and trunc(sys_date) <= ''17-Feb-2017''))';
    
    [PLUTO_FieldNames_New,PLUTO_Data_New] = read_from_database_MS_SQL([],SqlQuery_2);
    Total_PLUTO_FieldNames_New = PLUTO_FieldNames_New;
    if(~strcmpi('No Data',PLUTO_Data_New ))
        
        %% Compare PLUTO DB and OUR DB %% Get New Trades
        OUR_UniqueNumber_Col = find(cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames)) ;
        PLUTO_UniqueNumber_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), PLUTO_FieldNames_New)) ;
        
        OUR_Unique_Data  = DBData(:,OUR_UniqueNumber_Col);
        PLUTO_Unique_Data = PLUTO_Data_New(:,PLUTO_UniqueNumber_Col);
        
        Old_Data = [];
        New_Data = [];
        for i = 1 :  size(PLUTO_Unique_Data)
            
            Current_PLUTO_UniqueNumber = PLUTO_Unique_Data(i) ;
            
            Matched_UniqueNumber = cellStrfind_exact(OUR_Unique_Data,Current_PLUTO_UniqueNumber);
            
            if(~isempty(Matched_UniqueNumber))
                
                Old_Data = [Old_Data ; PLUTO_Data_New(i,:)] ;
            else
                
                New_Data = [New_Data ; PLUTO_Data_New(i,:)] ;
            end
            
            
        end
        
    else
        
        warndlg(SqlQuery_2,'No Data OP_FUT_NEW') ;
        
        PLUTO_FieldNames_New = {'No Data'};
        New_Data = '';
        Old_Data = {''};
    end
    
    %% Fetch PLUTO Future Modified  and Compare with OUR DB
    
    %     SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'',''ICUS'')' ]
    [PLUTO_FieldNames_Mod,PLUTO_Data_Mod] = read_from_database_MS_SQL([],SqlQuery_3);
    if(~strcmpi('No Data',PLUTO_Data_Mod ))

        OUR_UniqueNumber_Col = find(cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames)) ;
        TradeID_Col = find(cellfun(@(V) strcmpi('trade_id',V), DBFieldNames)) ;
        PLUTO_UniqueNumber_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), PLUTO_FieldNames_Mod)) ;
        
        OUR_Unique_Data  = DBData(:,OUR_UniqueNumber_Col);
        PLUTO_Unique_Data = PLUTO_Data_Mod(:,PLUTO_UniqueNumber_Col);
        
        Modified_Old_Data = [];
        Modified_New_Data = [];
        Existing_TradeId = [];
        for i = 1 : size(PLUTO_Unique_Data)
            
            Current_PLUTO_UniqueNumber = PLUTO_Unique_Data(i) ;
            
            Matched_UniqueNumber = cellStrfind_exact(OUR_Unique_Data,Current_PLUTO_UniqueNumber);
            
            if(~isempty(Matched_UniqueNumber))
                
                Modified_Old_Data = [Modified_Old_Data ; PLUTO_Data_Mod(i,:)] ;
                Existing_TradeId = [Existing_TradeId ; DBData(Matched_UniqueNumber,TradeID_Col)] ;
            else
                
                Modified_New_Data = [Modified_New_Data ; PLUTO_Data_Mod(i,:)] ;
            end
            
            
        end
        
        Total_New_Data = [New_Data ; Modified_New_Data] ; %% OP_FUT_New and OP_FUT_Mod
        
    else
        warndlg(SqlQuery_3,'No Data OP_FUT_MOD') ;
        
        Total_New_Data = New_Data ; %% Modified_New_Data is empty
        
        PLUTO_FieldNames_Mod = {'No Data'};
        Modified_New_Data = {''};
        Modified_Old_Data = {''};
    end
    
    %% PLUTO Future New 2 TR FORMAT
    
    if(exist('Total_New_Data','var'))
        [TRHeader , New_OutTRData , Instrument_Error_Data_New , Maturity_Error_Data] = PLUTO_Future_2_TR_Format(Total_PLUTO_FieldNames_New , Total_New_Data,'NEW',InBUName) ;
    end
    
    
    %% PLUTO Future Modified 2 TR FORMAT
    if(exist('Modified_Old_Data','var'))
        [TRHeader , Mod_OutTRData ,Instrument_Error_Data_Mod , Maturity_Error_Data] = PLUTO_Future_2_TR_Format(PLUTO_FieldNames_Mod , Modified_Old_Data,'MOD',InBUName) ;
        
        if(size(Mod_OutTRData,2) > 1)
            TRN_Number_Col = find(cellfun(@(V) strcmpi('TRN.Number',V), TRHeader)) ;
            Parent_TRN_Number_Col = find(cellfun(@(V) strcmpi('Parent Transaction Number',V), TRHeader)) ;
            
            Mod_OutTRData(:,TRN_Number_Col) = Existing_TradeId ;
            Mod_OutTRData(:,Parent_TRN_Number_Col) = Existing_TradeId ;
            
            Overall_TRData = [New_OutTRData ; Mod_OutTRData] ;
            
        end
    end
    
    if(isempty(Overall_TRData))
        if(exist('New_OutTRData','var'))
            Overall_TRData = New_OutTRData ;
        else
            TRHeader = {'No Data'};
            Overall_TRData = {''} ;
        end
        
    end
    %% Fetch OP_FUT_Del
    %     SqlQuery_4 = 'select * from OP_FUT_DEL';
    %     SqlQuery = ['select * from OP_FUT_DEL where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'',''ICUS'')'] ;
    [PLUTO_FieldNames_Del,PLUTO_Data_Del] = read_from_database_MS_SQL([],SqlQuery_4);
    
    if(~strcmpi('No Data',PLUTO_Data_Del))
        
        UNIQ_NO_Del_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), PLUTO_FieldNames_Del)) ;
        %   Del_ID = {'X17567100008','X17567100007'};
        Del_ID = PLUTO_Data_Del(:,UNIQ_NO_Del_Col);
        
        %         SqlQuery_5 = 'SELECT * FROM dbsystemuat.deal_ticket_table_with_latest_versions_view_cot' ;
        [DBFieldNames_Del,DBData_Del] = read_from_database(0,0,SqlQuery_5);
        
        OUR_UniqueNumber_Col = find(cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames_Del)) ;
        
        if(size(DBData_Del,2)>1)
            Del_ID_Index  = cellStrfind_exact(DBData_Del(:,OUR_UniqueNumber_Col) , Del_ID) ;
            Del_Data = DBData_Del(Del_ID_Index,:) ;
            %         Del_Data = DBData_Del(Del_ID_Index,[1;36]) ; %% Trade_Id_Col and PLUTO_Unique_Col
        else
            Del_ID_Index = [];
        end
        
        if(~isempty(Del_ID_Index))
            
            [DBFieldNames_Del ,  Del_Data] = Update_2_Database(Del_Data,InBUName) ; %% Update Table and Get Data From Table
            
            %%%%% Manually %%%%%%%%%
            %             Zero_Col = {'original_lots','premium','lots_to_be_netted','broker_lots'} ; %% Get Delete ID  Deal Ticket Format
            %             Zero_Col_Index = cellStrfind_exact(DBFieldNames_Del,Zero_Col) ;
            %             Del_Data(:,Zero_Col_Index) = {0};
            %             PLUTO_Action_Col = cellStrfind_exact(DBFieldNames_Del,{'ops_action'});
            %             Del_Data(:,PLUTO_Action_Col) = {'Del'};
            
            %             [TRHeader_Del,RowData_Del] = Deal_Ticket_to_TR_Format(DBFieldNames_Del,Del_Data,Commodity); %% Get Delete ID  TR Format
            
        else
            warndlg(SqlQuery_5,'Not Matching Delete Unique ID with deal_ticket_table_with_latest_versions_view_cot');
        end
        
    else
        warndlg(SqlQuery_4,'No Data OP_FUT_DEL') ;
        
        TRHeader_Del = {'No Data'};
        RowData_Del = {''};
    end
    %% EXCEL WRITE
    
    %     OutXLSName = getXLSFilename('PLUTO_FUT_2_TR_Format_');
    
    %     xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'PLUTO_FUT_New_TR_Format') ;
    xlswrite(OutXLSName , [PLUTO_FieldNames_New ;  New_Data] , 'PLUTO_FUT_New_Data') ;
    xlswrite(OutXLSName , [PLUTO_FieldNames_New ;  Old_Data] , 'PLUTO_FUT_New_Old_Data') ;
    xlswrite(OutXLSName , [PLUTO_FieldNames_Mod ;  Modified_New_Data] , 'PLUTO_FUT_Modified_New_Data') ;
    xlswrite(OutXLSName , [PLUTO_FieldNames_Mod ;  Modified_Old_Data] , 'PLUTO_FUT_Modified_Old_Data') ;
    
    if(exist('Del_Data','var') && (~isempty(Del_ID_Index)) )
        xlswrite(OutXLSName , [DBFieldNames_Del ;  Del_Data] , 'PLUTO_FUT_Delete_Data') ; %% [TRHeader_Del ;  RowData_Del] %% [DBFieldNames_Del ;  Del_Data]
    end
    
    if(~isempty(Instrument_Error_Data_New) || ~isempty(Instrument_Error_Data_Mod))
        Instrument_Error_Data = [Instrument_Error_Data_New ; Instrument_Error_Data_Mod];
        xlswrite(OutXLSName , [PLUTO_FieldNames_New ;Instrument_Error_Data ], 'Instrument Error Data') ;
    end
    %     OutXLSName = fullfile(pwd,OutXLSName) ;
    %     xls_delete_sheets(OutXLSName) ;
    
    %% Deal Security
    
    %                 [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
    %                 InXLSFilename = fullfile(pathname,filename);
    %                 [OutErrorMsg,OutSecurityInfoFilename,OutDealTicketFilename] = deals_security_id_generation(InBUName,InXLSFilename) ;
    
    
else
    
    warndlg(SqlQuery_1,'No Data Last 90 Days deal_ticket_table_with_latest_versions_view_cot');
    
end


