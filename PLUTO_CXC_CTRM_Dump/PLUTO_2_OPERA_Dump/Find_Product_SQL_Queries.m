function [SqlQuery_1,SqlQuery_2,SqlQuery_3,SqlQuery_4,SqlQuery_5,InBUName] = Find_Product_SQL_Queries(Business_Unit,Date_Str,Start_Date,End_Date,Commodity)

%%

SqlQuery_1 = [];
SqlQuery_2 = [];
SqlQuery_3 = [];
SqlQuery_4 = [];
SqlQuery_5 = [];

%%
if(strcmpi(Commodity,'FUT'))
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'CC'))
      InBUName = 'coc';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_FUTURE_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    else        
    end
    
else  %% OPT
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'CC'))
      InBUName = 'coc';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName];
        SqlQuery_2 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','NEW',''' '];
        SqlQuery_3 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_Code = ''',Business_Unit,''' and RECORD_FLAG = ''','MOD',''' '];
        SqlQuery_4 = ['select * from VWGET_OPERA_OPTIONS_INTERFACE where pr_code = ''',Business_Unit,''' and RECORD_FLAG = ''','DEL',''' '];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

    else      
    end
    
end
end