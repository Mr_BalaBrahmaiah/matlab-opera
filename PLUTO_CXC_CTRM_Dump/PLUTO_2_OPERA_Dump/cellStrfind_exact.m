function [Index] = cellStrfind_exact(CellStr,CellPattern)


%% Example

% CellStr = {'group_type','instrument','subportfolio','maturity_date','active_lots'};
% CellPattern = {'group_type','subportfolio','active_lots'};

%%


Index = [];
for i = 1 : length(CellPattern)
    
    Current_Str = CellPattern{i};
    
    Index = [Index ; find(strcmpi(CellStr,Current_Str))];
end

end