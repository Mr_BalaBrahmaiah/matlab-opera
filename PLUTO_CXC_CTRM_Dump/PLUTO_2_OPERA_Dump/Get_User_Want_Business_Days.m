function [Start_Date,Expiry_Date,User_Want_Days] = Get_User_Want_Business_Days(varargin) %% NoDaysShift Default 3

%% Example
% Argument 1 : ValueDate
% Argument 2 : User Want Days

NoDaysShift = 3 ;

Start_Date = '';
Expiry_Date = '';
User_Want_Days = '';


%%
ObjDB = connect_to_database;

[System_Date_Format,~] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

%%
try
    
    if(isempty(varargin))
        Value_Settele_Date = fetch(ObjDB,'SELECT value_date,settlement_date FROM valuation_date_table');
        Current_ValueDate = Value_Settele_Date(1);
        Settlement_Date =  Value_Settele_Date(2);
        
    else
        if nargin == 1
            Current_ValueDate = varargin{1};
            
            if(strcmpi(System_Date_Format,DB_Date_Format))
                Current_ValueDate = datestr(datenum(Current_ValueDate,System_Date_Format),DB_Date_Format);
            end
            
        else   %% nargin 1 Empty
            %         Current_ValueDate = datestr(today,DB_Date_Format);
            Value_Settele_Date = fetch(ObjDB,'SELECT value_date,settlement_date FROM valuation_date_table');
            Current_ValueDate = Value_Settele_Date(1);
            Settlement_Date =  Value_Settele_Date(2);
        end
        
    end
    
    if nargin == 2
        NoDaysShift = varargin{2};
        SqlQuery_Date = ['select value_date from vdate_sdate_table where value_date <= ''',char(Current_ValueDate),''' order by value_date desc limit 0,',num2str(NoDaysShift),''];
    else
        SqlQuery_Date = ['select value_date from vdate_sdate_table where value_date <= ''',char(Current_ValueDate),''' order by value_date desc limit 0,',num2str(NoDaysShift),''];
    end
    
    
    
    %% Fetch Dates
    
    [~,User_Want_Days] = Fetch_DB_Data(ObjDB,SqlQuery_Date);
    Expiry_Date = User_Want_Days(1);
    Start_Date = User_Want_Days(end);
    
catch
    Start_Date = 'Error';
    Expiry_Date = 'Error';
    User_Want_Days = 'Error';
    
end



