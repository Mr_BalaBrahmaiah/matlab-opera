function [Month_Maturity , Maturity_Code ,Maturity_Error_Data] = Convert_Month_2_Maturity_Format(Month_Cell,BBR_Code_Data,Expiry_Date,Commodity)

Month_Format = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};

Maturity_Error_Data = '';

%%

if(strcmpi(Commodity,'FUT'))
    
    Unique_Month = unique(Month_Cell);
    Month_Maturity = cell(size(Month_Cell,1) , 1) ;
    
    for i = 1 : size(Unique_Month,1)
        
        Current_Month_Year = Unique_Month(i) ;

        %Current_Month_Only= cellfun(@(x) x(1:3), Current_Month_Year, 'UniformOutput', false);
        
        %%% Newly added lines
        Current_Month_Only= cellfun(@(x) x(1:2), Current_Month_Year, 'UniformOutput', false);  %% Change
        Current_Month_Only = strtrim(Current_Month_Only);
        formatOut = 'mmm';
        Current_Month_Only = {(datestr(datenum(Current_Month_Only,'mm'),formatOut))};
        %%% End
        
        Current_Year_Only = cellfun(@(x) x(end), Current_Month_Year, 'UniformOutput', false);
        
        Current_Month_Code = MonthCode( cellStrfind_exact(Month_Format,Current_Month_Only) );
        
        Current_Maturity_Code = strcat(Current_Month_Code , Current_Year_Only) ;
        
        Matched_Month_Index = cellStrfind_exact(Month_Cell , Current_Month_Year);
        
        Month_Maturity(Matched_Month_Index,1) =  Current_Maturity_Code ;
        
    end
    
    Maturity_Code = strcat(Month_Maturity,{'.'},Month_Maturity) ;
    
    
else
    
    %Unique_Month = unique(Month_Cell);
    Month_Maturity = cell(size(Month_Cell,1) , 1) ;
    Maturity_Error_Data = [];
    
    SqlQuery = 'select vol_id,bbg_opt_ticker,expiry_date from vol_id_table';
    [~,DBData] = read_from_database('vol_id_table',0,SqlQuery);
    DBVolID = DBData(:,1);
    DBBBGOptTicker = DBData(:,2);
    DBExpiryDate = cellstr(datestr(datenum(DBData(:,3),'yyyy-mm-dd'),'yyyy'));
    
    for i = 1 : size(Month_Cell,1)
        
        Current_Month_Year = Month_Cell(i) ;
        Current_BBR_Code = BBR_Code_Data(i);
        Current_Expiry_Date = cellstr(datestr(datenum(Expiry_Date(i),'dd/mm/yyyy'),'yyyy'));
        %Current_Month_Only= cellfun(@(x) x(1:3), Current_Month_Year, 'UniformOutput', false);
        
        %%% Newly added lines
        Current_Month_Only= cellfun(@(x) x(1:2), Current_Month_Year, 'UniformOutput', false);  %% Change
        Current_Month_Only = strtrim(Current_Month_Only);
        formatOut = 'mmm';
        Current_Month_Only = {(datestr(datenum(Current_Month_Only,'mm'),formatOut))};
        %%% End

        Current_Year_Only = cellfun(@(x) x(end), Current_Month_Year, 'UniformOutput', false);
        
        Current_Month_Code = MonthCode( cellStrfind_exact(Month_Format,Current_Month_Only) );
        
        Current_Maturity_Code = strcat(Current_Month_Code , Current_Year_Only) ;
        
        BBG_Opt_Ticker = strcat(Current_BBR_Code ,Current_Maturity_Code);
               
        %%
        %         SqlQuery = ['select vol_id from vol_id_table where bbg_opt_ticker = ''',char(BBG_Opt_Ticker),''' '];
        %         [DBFieldNames,Vol_ID] = read_from_database(0,0,SqlQuery);
        
        %         if(~isempty(char(Vol_ID)))
        %             Split_Str =strsplit(char(Vol_ID),' ');
        %         else
        %              Split_Str = BBG_Opt_Ticker;
        %         end
        
        %%
        
        %%% Comparing BBG-OpT-Ticker and EXP-Date
         %IdxProd = find(strcmpi(BBG_Opt_Ticker,DBBBGOptTicker) & strcmpi(Current_Expiry_Date,DBExpiryDate));

        %Matched_BBG_Opt_Ticker_Index = cellStrfind_exact(DBBBGOptTicker,BBG_Opt_Ticker) ;
        Matched_BBG_Opt_Ticker_Index = find(strcmpi(BBG_Opt_Ticker,DBBBGOptTicker) & strcmpi(Current_Expiry_Date,DBExpiryDate));
        
        if(~isempty(Matched_BBG_Opt_Ticker_Index))
            Vol_ID = DBVolID(Matched_BBG_Opt_Ticker_Index , 1);
            Split_Str =strsplit(char(Vol_ID),' ');
            
        else
            Split_Str = BBG_Opt_Ticker;
            
            Maturity_Error_Data = [Maturity_Error_Data ;cellstr(strcat(BBG_Opt_Ticker,' BBG_Opt_Ticker not available in vol_id_table '))];
        end
        
        
        Month_Maturity(i,1) =  Split_Str(end) ;
        
    end
    
    Maturity_Code = Month_Maturity ;
    
end




