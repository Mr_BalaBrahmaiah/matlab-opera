function varargout = PLUTO_2_OPERA(varargin)
% PLUTO_2_OPERA MATLAB code for PLUTO_2_OPERA.fig
%      PLUTO_2_OPERA, by itself, creates a new PLUTO_2_OPERA or raises the existing
%      singleton*.
%
%      H = PLUTO_2_OPERA returns the handle to a new PLUTO_2_OPERA or the handle to
%      the existing singleton*.
%
%      PLUTO_2_OPERA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLUTO_2_OPERA.M with the given input arguments.
%
%      PLUTO_2_OPERA('Property','Value',...) creates a new PLUTO_2_OPERA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PLUTO_2_OPERA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PLUTO_2_OPERA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only
%      onek
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PLUTO_2_OPERA

% Last Modified by GUIDE v2.5 26-Apr-2018 14:56:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PLUTO_2_OPERA_OpeningFcn, ...
    'gui_OutputFcn',  @PLUTO_2_OPERA_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PLUTO_2_OPERA is made visible.
function PLUTO_2_OPERA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PLUTO_2_OPERA (see VARARGIN)

% Choose default command line output for PLUTO_2_OPERA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PLUTO_2_OPERA wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% global Opera_Date_Str;global PLUTO_StartDate_Str;global PLUTO_EndDate_Str;

ObjDB_MS_SQL = connect_to_database_MS_SQL ;
MS_SQL_DB_Message = ObjDB_MS_SQL.Message;
if(isempty(MS_SQL_DB_Message))
    MS_SQL_DB_Message = 'MS_SQL Database is Connected';
end
set(handles.text11,'String',MS_SQL_DB_Message);

[System_Date_Format,~] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

HolDates = {'2016-01-01','2017-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));

Opera_Date_Str = datestr(busdate(today-90,-1,HolidayVec),1);

% PLUTO_StartDate_Str = datestr(busdate(today-1,-1,HolidayVec),'dd-mmm-yyyy');
% PLUTO_EndDate_Str = datestr(busdate(today+1,-1,HolidayVec),'dd-mmm-yyyy');
% NoDaysShift = 2;

[PLUTO_StartDate_Str,PLUTO_EndDate_Str,~] = Get_User_Want_Business_Days([],3) ;
PLUTO_StartDate_Str = datestr(datenum(PLUTO_StartDate_Str,DB_Date_Format),1);
PLUTO_EndDate_Str = datestr(datenum(PLUTO_EndDate_Str,DB_Date_Format),1);

set(handles.edit1,'String',Opera_Date_Str);
set(handles.edit2,'String',PLUTO_StartDate_Str);
set(handles.edit3,'String',PLUTO_EndDate_Str);

% set(handles.checkbox4,'Enable','off');
% set(handles.checkbox5,'Enable','off');
% set(handles.checkbox6,'Enable','off');
% set(handles.checkbox7,'Enable','off');

% set(handles.text2,'Visible','off');
% set(handles.edit1,'Visible','off');
set(handles.edit1,'Enable','off');

set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group

handles.Opera_Date_Str = Opera_Date_Str;
handles.PLUTO_StartDate_Str = PLUTO_StartDate_Str;
handles.PLUTO_EndDate_Str = PLUTO_EndDate_Str;

Live_Only = get(handles.radiobutton1,'Value');
handles.Live_Only = Live_Only;

Live_Dead = get(handles.radiobutton2,'Value');
handles.Live_Dead = Live_Dead;


guidata(hObject,handles);

% --- Outputs from this function are returned to the command line.
function varargout = PLUTO_2_OPERA_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

% global CT;

CT = get(handles.checkbox1,'Value');

if(CT)
    
    %     set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end


handles.CT = CT;
guidata(hObject,handles);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

% global CC;

CC = get(handles.checkbox2,'Value');

if(CC)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.CC = CC;
guidata(hObject,handles);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

% global CF;

CF = get(handles.checkbox3,'Value');

if(CF)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.CF = CF;
guidata(hObject,handles);

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4

GR = get(handles.checkbox4,'Value');

if(GR)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.GR = GR;
guidata(hObject,handles);

% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5

MP = get(handles.checkbox5,'Value');

if(MP)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.MP = MP;
guidata(hObject,handles);

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6

RB = get(handles.checkbox6,'Value');

if(RB)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.RB = RB;
guidata(hObject,handles);


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7

SG = get(handles.checkbox7,'Value');

if(SG)
    %      set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    
    %set(handles.PersentageText,'String','');
else
    %     set(handles.uibuttongroup1,'Visible','off'); %% If User Want Full Dump Button Group
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    
    %set(handles.PersentageText,'String','');
end

handles.SG = SG;
guidata(hObject,handles);

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

PLUTO_StartDate_Str = get(handles.edit2,'String');
handles.PLUTO_StartDate_Str = PLUTO_StartDate_Str;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

PLUTO_EndDate_Str = get(handles.edit3,'String');
handles.PLUTO_EndDate_Str = PLUTO_EndDate_Str;
guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% global Opera_Date_Str;global PLUTO_StartDate_Str;global PLUTO_EndDate_Str;   %% Global Varibales
global CT;global CC; global CF;global GR;global MP;global RB;global SG;
%
% Opera_Date_Str = get(handles.edit1,'String');
% Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');
% PLUTO_StartDate_Str = get(handles.edit2,'String');
% PLUTO_EndDate_Str = get(handles.edit3,'String');
% CT = get(handles.checkbox1,'Value');
% CC = get(handles.checkbox2,'Value');
% CF = get(handles.checkbox3,'Value');
% GR = get(handles.checkbox4,'Value');
% MP = get(handles.checkbox5,'Value');
% RB = get(handles.checkbox6,'Value');
% SG = get(handles.checkbox7,'Value');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[jObj] = Job_Start();
drawnow;

Opera_Date_Str = handles.Opera_Date_Str;
Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');
PLUTO_StartDate_Str = handles.PLUTO_StartDate_Str;
PLUTO_StartDate_Str = datestr(PLUTO_StartDate_Str,'yyyy-mm-dd');
PLUTO_EndDate_Str = handles.PLUTO_EndDate_Str;
PLUTO_EndDate_Str = datestr(PLUTO_EndDate_Str,'yyyy-mm-dd');

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(datenum(PLUTO_StartDate_Str))
    %% Only Run Fast Seven Days
    if(CT || CC || CF || GR || MP || RB || SG)
        
        ProductNames = {'CT','CC','CF','GR','MP','RB','SG'};            %% Hard Code
        BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
        
        User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
        
        Selected_BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
        Selected_ProductNames = ProductNames(logical(cell2mat(User_Select_Portfolio)));
        
        [Opera_Date_Str] = Find_Minimum_PLUTO_ViewDate(char(Selected_ProductNames),PLUTO_StartDate_Str,PLUTO_EndDate_Str) ;
        set(handles.edit1,'String',Opera_Date_Str);
        Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');
        
        %     close(gcf);
        %     hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');       %% Processing Waitbar
        %     set(handles.slider2,'value',0.25) ;
        %set(handles.PersentageText,'String', [num2str(0.25*100), ' %']);
        
        for i = 1 : length(Selected_ProductNames)
            
            Current_Product = char(Selected_ProductNames(i));
            Current_BU_Name = char(Selected_BU_Names(i));
            
            XLS_Name = strcat('PLUTO_2_Opera_TR_Format_',Current_BU_Name);
            OutXLSName = getXLSFilename(XLS_Name);
            xlswrite(OutXLSName,{''},'PLUTO_2_OPERA_TR_Format');
            
            [TRHeader , Overall_TRData_FUT ,New_FUT_OutTRData , Mod_FUT_OutTRData] = PLUTO_Future_2_Opera(Current_Product,Opera_Date_Str,PLUTO_StartDate_Str,PLUTO_EndDate_Str,OutXLSName) ;
            
            %         set(handles.slider2,'value',0.50) ;
            %set(handles.PersentageText,'String', [num2str(0.50*100), ' %']);
            
            [~ , Overall_TRData_OPT ,New_OPT_OutTRData , Mod_OPT_OutTRData] = PLUTO_Option_2_Opera(Current_Product,Opera_Date_Str,PLUTO_StartDate_Str,PLUTO_EndDate_Str,OutXLSName) ;
            
            %         set(handles.slider2,'value',0.75) ;
            %set(handles.PersentageText,'Value', 0.75*100);
            
            Overall_New_TRData = [New_FUT_OutTRData ; New_OPT_OutTRData];
            Overall_Mod_TRData =  [Mod_FUT_OutTRData ; Mod_OPT_OutTRData];
            
            if isempty(Overall_New_TRData)  %%% Nely added code
                 warndlg('RECORD_FLAG = NEW : No Data found in Future and Option!','PLUTO_2_OPERA');
                 msgbox('Process Complete','Finish');
                 Job_Done(jObj);
            end
            [TradeId , Last_TradeId] = getTradeID_4_Cell_Array(Overall_New_TRData,Current_BU_Name) ; %% Get Trade ID
            
            if(~isempty(TradeId) ) %% && ~isempty(cell2mat(TradeId)) %%% Change
                
                TRN_TradeID_Col = cellStrfind_exact(TRHeader , {'TRN.Number'}) ;
                Parent_TradeID_Col = cellStrfind_exact(TRHeader , {'Parent Transaction Number'}) ;
                Overall_New_TRData(:,TRN_TradeID_Col) = TradeId ;
                Overall_New_TRData(:,Parent_TradeID_Col) = TradeId ;
                
                Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];
                
                xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'PLUTO_2_OPERA_TR_Format') ;
                
            else
                Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];
                xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'PLUTO_2_OPERA_TR_Format') ;
            end
            
            OutXLSName = fullfile(pwd,OutXLSName) ;
            xls_delete_sheets(OutXLSName) ;
            xls_change_activesheet(OutXLSName , 'PLUTO_2_OPERA_TR_Format');
            
            %         waitbar(i/length(Selected_ProductNames),hWaitbar,[Current_Product,' Finished']);
            %         set(handles.slider2,'value',1) ;
            %set(handles.PersentageText,'String', [num2str(1*100), ' %']);
            
        end
        
        %     close(hWaitbar);
        
        msgbox('Process Complete','Finish');
        
        %     munlock('PLUTO_2_OPERA.m');
    else
        close(gcf);
        msgbox('Please Select the any one of the portfolio','Warning');
        
    end
    
else
    msgbox(['The Data can be retrieved only for Last 7 days.',{''},'Please choose the appropriate Date for Opera Start & End Date.'],'Warning');
end

Job_Done(jObj);
set(handles.pushbutton1,'Enable','on');


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

Default_Name = 'deal_ticket_table_with_latest_versions_view_';
Default_Name_2 = 'cloned_deal_ticket_table_';

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(CT || CC || CF || GR || MP || RB || SG)
    
    ProductNames = {'CT','CC','CF','GR','MP','RB','SG'}; %% Hard Code
    BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
    User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
    BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        Table_Name = strcat(Default_Name,InBUName);
        Table_Name_2 = strcat(Default_Name_2,InBUName);
    else
        
    end
    
    SqlQuery_1 = ['select count(*) from ',Table_Name,' where ops_unique IS NOT NULL'] ;
    SqlQuery_2 = ['select count(distinct(ops_unique)) from ',Table_Name];
    SqlQuery_3 = ['select count(*) from ',Table_Name_2,' where contract_number IS NULL'];
    
    ObjDB = connect_to_database;
    Count_1 = cell2mat(fetch(ObjDB,SqlQuery_1));
    Count_2 = cell2mat(fetch(ObjDB,SqlQuery_2));
    Count_3 = cell2mat(fetch(ObjDB,SqlQuery_3));
    close(ObjDB);
    delete(ObjDB);
    
    if(length(unique([Count_1;Count_2;Count_3])) == 1) %% Count_1 == Count_2
        %         msgbox(['       No Duplicate Entries Found in Opera       ',Business_Unit,' Latest Version Count : ',num2str(Count_1),' Distinct PLUTO Unique Count : ',num2str(Count_2)],'Count Matched');
        msgbox([['       No Duplicate Entries Found in Opera ;Count is Matched in ',char(InBUName)],{''}, SqlQuery_1,num2str(Count_1),SqlQuery_2,num2str(Count_2),SqlQuery_3,num2str(Count_3)],'Count Matched');
    else
        %         errordlg(['       Duplicate Entries Found in Opera       ',Business_Unit,' Latest Version Count : ',num2str(Count_1),' Distinct PLUTO Unique Count : ',num2str(Count_2)],'Count Mismatched');
        errordlg([['       No Duplicate Entries Found in Opera ;Count is MisMatched in ',char(InBUName)],{''}, SqlQuery_1,num2str(Count_1),SqlQuery_2,num2str(Count_2),SqlQuery_3,num2str(Count_3)],'Count Mismatched');
    end
    
else
    close(gcf);
    msgbox('Please Select the any one of the Business Unit','Warning');
end

Job_Done(jObj);


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

Live_Only = get(handles.radiobutton1,'Value');
Live_Dead = get(handles.radiobutton2,'Value');

try
    [BU_Name,Pr_Code] = Get_BU_Prcode(handles); %% User Selected BU
    [OutXLSName] = Get_OverAll_Dump(char(BU_Name),char(Pr_Code),Live_Only,Live_Dead);
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end

msgbox(['Overall Dump File is ',OutXLSName],'Finish');

Job_Done(jObj);

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% Live_Only = get(handles.radiobutton1,'Value');
% handles.Live_Only = Live_Only;
% guidata(hObject,handles);

% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

% Live_Dead = get(handles.radiobutton2,'Value');
% handles.Live_Dead = Live_Dead;
% guidata(hObject,handles);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;
try
    [Business_Unit,InBUName] = Get_PLUTO_Opera_BusinessUnit(handles);
    
    if(~isempty(InBUName))
        
        ObjDB_Oracle = connect_to_database_Oracle;
        
        %Input File
        [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
        InXLSFilename = fullfile(pathname,filename);
        [~,~,RawData] = xlsread(InXLSFilename);
        
        Header = RawData(1,:);
        RawData = RawData(2:end,:);
        
        UniqueID = RawData;
        
        %%% cell to Char
        UniqueID_Str = convertCell2Char(UniqueID);
        UniqueID_Str = strrep(UniqueID_Str,',',''',''');
        
        %%% Fetch Data
        
        SqlQuery_1 = ['select * from op_fut_dump where pr_code = ''',char(Business_Unit),''' and UNIQ_NO in (''',UniqueID_Str,''')'];
        SqlQuery_2 = ['select * from op_opt_dump where pr_code = ''',char(Business_Unit),''' and UNIQ_NO in (''',UniqueID_Str,''')'];
        
        [FUT_FiledNames,FUT_Data,ObjDB_ErrorMsg1] = Fetch_DB_Data(ObjDB_Oracle,SqlQuery_1);
        [OPT_FiledNames,OPT_Data,ObjDB_ErrorMsg2] = Fetch_DB_Data(ObjDB_Oracle,SqlQuery_2);
        
        if(~isempty(ObjDB_ErrorMsg1) || ~isempty(ObjDB_ErrorMsg2))
            ObjDB_ErrorMsg = unique([ObjDB_ErrorMsg1 ; ObjDB_ErrorMsg2]);
            errordlg(ObjDB_ErrorMsg);
        else
            
            FUT_FiledNames = [FUT_FiledNames,{'Group_Type'}];
            FUT_GroupType = cell(size(FUT_Data,1),1);
            FUT_GroupType(:,1) = {'FUT'};
            FUT_Data = [FUT_Data , FUT_GroupType];
            
            OPT_FiledNames = [OPT_FiledNames,{'Group_Type'}];
            OPT_GroupType = cell(size(OPT_Data,1),1);
            OPT_GroupType(:,1) = {'OFUT'};
            OPT_Data = [OPT_Data , OPT_GroupType];
            
            FUT_UniqueNo_Col = cellStrfind_exact(FUT_FiledNames,{'UNIQ_NO'});
            OPT_UniqueNo_Col = cellStrfind_exact(OPT_FiledNames,{'UNIQ_NO'});
            
            if(~strcmpi(FUT_Data(1,1),'No Data') && ~strcmpi(OPT_Data(1,1),'No Data'))
                Dump_UniqueID = [FUT_Data(:,FUT_UniqueNo_Col) ; OPT_Data(:,OPT_UniqueNo_Col)];
            else
                if(~strcmpi(FUT_Data(1,1),'No Data'))
                    Dump_UniqueID = FUT_Data(:,FUT_UniqueNo_Col);
                else
                    FUT_Data = cell(size(FUT_FiledNames,1),size(FUT_FiledNames,2));
                end
                
                if(~strcmpi(OPT_Data(1,1),'No Data'))
                    Dump_UniqueID = OPT_Data(:,OPT_UniqueNo_Col);
                else
                    OPT_Data = cell(size(OPT_FiledNames,1),size(OPT_FiledNames,2));
                end
            end
            
            if(exist('Dump_UniqueID','var'))
                Missing_UniqueID = setdiff(RawData,Dump_UniqueID);
                
                if(isempty(Missing_UniqueID))
                    Missing_UniqueID = {'No Missing ID'};
                end
                
                %%% TR Format Conversion
                PLUTO_Action_Str = 'NEW';
                [TRHeader , FUT_RowData ] = PLUTO_Future_2_TR_Format(FUT_FiledNames , FUT_Data , PLUTO_Action_Str,InBUName);
                [TRHeader , OPT_RowData ] = PLUTO_Option_2_TR_Format(OPT_FiledNames , OPT_Data , PLUTO_Action_Str,InBUName);
                
                %%% Excel Write
                XLSName = getXLSFilename('UniqueID_Dump');
                xlswrite(XLSName,[TRHeader ; FUT_RowData],'FUT TR Dump');
                xlswrite(XLSName,[TRHeader ; OPT_RowData],'OPT TR Dump');
                
                xlswrite(XLSName,[FUT_FiledNames ; FUT_Data],'FUT Dump');
                xlswrite(XLSName,[OPT_FiledNames ; OPT_Data],'OPT Dump');
                xlswrite(XLSName,Missing_UniqueID,'Missing ID in Dump');
                
                xls_delete_sheets(fullfile(pwd,XLSName));
                
            else
                errordlg('Not Getting Unique Data from PLUTO DB') ;
                
            end
            
        end
    else
        warndlg('Please Select anyone of the business Unit','Warning');
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    errordlg(errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

msgbox('Process Complete','Finish');
Job_Done(jObj);


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

ObjDB = connect_to_database;

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(CT || CC || CF || GR || MP || RB || SG)
    
    ProductNames = {'CT','CC','CF','GR','MP','RB','SG'}; %% Hard Code
    BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
    User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
    BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
    else
        
    end
    
    
    SqlQuery1 = ['select distinct(product_code) from subportfolio_product_mapping_table_',char(InBUName)];
    [~,ProductCodes] = Fetch_DB_Data(ObjDB,SqlQuery1);
    ProductCodes = strip(ProductCodes);
    
    if(~strcmpi(ProductCodes,'No Data'))
        
        [User_Select_ProductCode, theChosenIDX] = uicellect(ProductCodes,'Select ProductCode','Multi', 0);
        
        if(~isempty(User_Select_ProductCode))
            TableName = ['subportfolio_product_mapping_table_',char(InBUName)];
            SqlQuery2 = ['select distinct(instrument) from ',char(TableName),' where product_code=''',char(User_Select_ProductCode),''' '];
            [~,UniqueInstrument] = Fetch_DB_Data(ObjDB,SqlQuery2);
            if(~strcmpi(UniqueInstrument,'No Data'))
                msgbox(['Product Code ',char(User_Select_ProductCode),' mapped ','Instrument is ',{''},char(UniqueInstrument)]);
            else
                warndlg(SqlQuery2,'No Data');
            end
            
        else
            warndlg('Please Select Anyone of the Product Code','Nothing Selected');
        end
        
    else
        warndlg(SqlQuery1,'No Data');
    end
    
else
    warndlg('Please Select anyone of the business Unit','Warning');
end
Job_Done(jObj);


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

ObjDB = connect_to_database;

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(CT || CC || CF || GR || MP || RB || SG)
    
    ProductNames = {'CT','CC','CF','GR','MP','RB','SG'}; %% Hard Code
    BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
    User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
    BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        Table_Name = ['deal_ticket_table_with_latest_versions_view_',InBUName];
    else
        
    end
    
    SqlQuery = ['select count(*) from ',Table_Name,' where version_no>0 and ops_action=''NEW'' '];
    
    [ColNames,DBCount] = Fetch_DB_Data(ObjDB,SqlQuery);
    if( cell2mat(DBCount) > 0 )
        warndlg([SqlQuery,{''},'Count=',num2str(cell2mat(DBCount)),'MisMatch...!!!!!!'],'MisMatch');
    else
        warndlg([SqlQuery,{''},'Count=',num2str(cell2mat(DBCount)),{''},'No Such Trades in OPERA'],'Perfect');
        
    end
    
else
    warndlg('Please Select anyone of the business Unit','Warning');
end

Job_Done(jObj);
