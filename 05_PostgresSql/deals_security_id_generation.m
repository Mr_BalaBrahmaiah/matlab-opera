function [OutErrorMsg,OutSecurityInfoFilename,OutDealTicketFilename] = deals_security_id_generation(InBUName,InXLSFilename)
% to generate the deal_ticket, security_info table data
% data from the input data in xls file
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/05/15 07:27:27 $
%  $Revision: 1.9 $
%


% [filename, pathname] = uigetfile( ...
% {'*.xlsx';'*.xls'}, ...
% 'Pick a file');
% InXLSFilename = fullfile(pathname,filename);

try
    [~,~,RawData] = xlsread(InXLSFilename);
    
    warning('off', 'MATLAB:nonIntegerTruncatedInConversionToChar');
    
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN     = cellfun(@isnan,RawData(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    RawData(IdxNaNRows,:) = [];
    
    % check if nan is present in the whole column itself;
    % if its present, then cell2dataset will fail
    IdxNaNCols = [];
    IdxNaN1    = cellfun(@isnan,RawData(1,:),'UniformOutput',false);
    for iR = 1:length(IdxNaN1)
        if any(IdxNaN1{iR})
            IdxNaNCols = [IdxNaNCols; iR];
        end
    end
    RawData(:,IdxNaNCols) = [];
    
    DSData = cell2dataset(RawData);
    OutErrorMsg = {'No errors'};
    OutSecurityInfoFilename = {''};
    OutDealTicketFilename = {''};
    
    % check if the required headers available
    InputHeading = DSData.Properties.VarNames;
    
    % TODO - error handling for swap header and fx header to be added
    if ~ismember('Instrument',InputHeading)
        OutErrorMsg = {'Instrument column missing in input deals header!'};
        return;
    end
    if ~ismember('Portfolio',InputHeading)
        OutErrorMsg = {'Portfolio column missing in input deals header!'};
        return;
    end
    if ~ismember('BuySell',InputHeading)
        OutErrorMsg = {'BuySell column missing in input deals header!'};
        return;
    end
    if ~ismember('Month',InputHeading)
        OutErrorMsg = {'Month column missing in input deals header!'};
        return;
        
    else
        if(strcmpi(InBUName,'qf5'))
            Month_Col = cellStrfind_exact(InputHeading,{'Month'}); %% For QF5
            try
                NaN_Index = find(~cellfun(@(s) (length(s)>1), DSData.Month)); %% find(isnan(DSData.Month));
            catch
                NaN_Index = find(isnan(DSData.Month));
            end
            aa = cell(size(NaN_Index,1),1);
            aa(:,:) = {''};
            
            DSData = dataset2cell(DSData);
            DSData(1,:) = [];
            DSData(NaN_Index,Month_Col) = aa;
            
            DSData = cell2dataset([InputHeading ; DSData]);
        end
    end
    if ~ismember('Strike',InputHeading)
        OutErrorMsg = {'Strike column missing in input deals header!'};
        return;
    end
    if ~ismember('OptType',InputHeading)
        OutErrorMsg = {'OptType column missing in input deals header!'};
        return;
    end
    if ~ismember('Prem',InputHeading)
        OutErrorMsg = {'Prem column missing in input deals header!'};
        return;
    end
    if ~ismember('CounterParty',InputHeading)
        OutErrorMsg = {'CounterParty column missing in input deals header!'};
        return;
    end
    if ~ismember('InitialLots',InputHeading)
        OutErrorMsg = {'InitialLots column missing in input deals header!'};
        return;
    end
    if ~ismember('CONTRACTNB',InputHeading)
        OutErrorMsg = {'CONTRACTNB column missing in input deals header!'};
        return;
    end
    if ~ismember('Maturity',InputHeading)
        OutErrorMsg = {'Maturity column missing in input deals header!'};
        return;
    end
    if ~ismember('BarrierType',InputHeading)
        OutErrorMsg = {'BarrierType column missing in input deals header!'};
        return;
    end
    if ~ismember('Barrier',InputHeading)
        OutErrorMsg = {'Barrier column missing in input deals header!'};
        return;
    end
    if ~ismember('TransactionDate',InputHeading)
        OutErrorMsg = {'TransactionDate column missing in input deals header!'};
        return;
    end
    if ~ismember('TRN_Number',InputHeading)
        OutErrorMsg = {'TRN_Number column missing in input deals header!'};
        return;
    end
    if ~ismember('ParentTransactionNumber',InputHeading)
        OutErrorMsg = {'ParentTransactionNumber column missing in input deals header!'};
        return;
    end
    if ~ismember('Exe_Broker',InputHeading)
        OutErrorMsg = {'Exe_Broker column missing in input deals header!'};
        return;
    end
    if ~ismember('ExeType',InputHeading)
        OutErrorMsg = {'ExeType column missing in input deals header!'};
        return;
    end
    if ~ismember('Rates',InputHeading)
        OutErrorMsg = {'Rates column missing in input deals header!'};
        return;
    end
    if ~ismember('Spcl_Rate_lot',InputHeading)
        OutErrorMsg = {'Spcl_Rate_lot column missing in input deals header!'};
        return;
    end
    if ~ismember('OtherRemarks',InputHeading)
        OutErrorMsg = {'OtherRemarks column missing in input deals header!'};
        return;
    end
    if ~ismember('OTCDAILYEODLEG',InputHeading)
        OutErrorMsg = {'OTCDAILYEODLEG column missing in input deals header!'};
        return;
    end
    if isnumeric(DSData.Maturity)
        if ~all(isnan(DSData.Maturity))
            OutErrorMsg = {'Maturity column should be in date format(dd-mm-yyyy)!'};
            return;
        end
    end
    if isnumeric(DSData.TransactionDate)
        OutErrorMsg = {'TransactionDate column should be in date format(dd-mm-yyyy)!'};
        return;
    end
    %     if isnumeric(DSData.TRN_Number) && any(isnan(DSData.TRN_Number))
    %         OutErrorMsg = {'TRN_Number column should not be empty!'};
    %         return;
    %     end
    %     if isnumeric(DSData.ParentTransactionNumber) && any(isnan(DSData.ParentTransactionNumber))
    %         OutErrorMsg = {'ParentTransactionNumber column should not be empty!'};
    %         return;
    %     end
    
    %% TODO - Check if the BU is {'cot','coc','cof'}
    if(~isempty(cellStrfind_exact(InBUName,{'cot','coc','cof'} )))
        if ~ismember('OPSUnique',InputHeading)
            OutErrorMsg = {'OPSUnique column missing in input deals header!'};
            return;
        end
        
        if ~ismember('BROKERLOTS',InputHeading)
            OutErrorMsg = {'BROKERLOTS column missing in input deals header!'};
            return;
        end
        
        if ~ismember('OPSAction',InputHeading)
            OutErrorMsg = {'OPSAction column missing in input deals header!'};
            return;
        end
    end
    
    %% For QF5
    
    if(strcmpi(InBUName,'qf5'))
        if ~ismember('Equity_Trade_Type',InputHeading)
            OutErrorMsg = {'Equity_Trade_Type column missing in input deals header!'};
            return;
        end
    end
    
    %%
    if isnumeric(DSData.TRN_Number) && any(isnan(DSData.TRN_Number))  %% Changing
        %     OutErrorMsg = {'TRN_Number column should not be empty!'};
        %     return;
        
        TempTradeId = '';
        [~,DBTradeId]= getLastTradeId(InBUName);
        try
            % TradeIdPrefix = 'TR-FY16-17-';
            SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
            ObjDB = connect_to_database;
            TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
            TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
            
            if(isempty(TempTradeId))
                TempTradeId = 0;      %% Default
            end
            
            NumRows = size(DSData.TRN_Number,1);
            NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
            TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
            TradeId = strrep(TradeId,' ','');  %% Remove Spaces
            
            DSData.TRN_Number = TradeId;
            
        catch
        end
        
    end
    DSData.ParentTransactionNumber = DSData.TRN_Number;
    
    %%
    OutSecurityInfoFilename = getXLSFilename(['Security_info_table_',InBUName]);
    OutDealTicketFilename = getXLSFilename(['Deal_ticket_table_',InBUName]);
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    % read all required master data from database
    % get the subportfolio product mapping data like strike and premium mult
    % factor and the prodcut code to be used
    [ColNames,Data] = read_from_database('subportfolio_product_mapping_table',0,'',InBUName);
    DBProductMapData = cell2dataset([ColNames;Data]);
    
    % get the future expiry dates
    [ColNames,Data] = read_from_database('underlying_list_table',0);
    DBFutureData = cell2dataset([ColNames;Data]);
    
    % get the option expiry dates
    [ColNames,Data] = read_from_database('vol_id_table',0);
    DBOptionData = cell2dataset([ColNames;Data]);
    
    % get the deal security mapping data
    [ColNames,Data] = read_from_database('deal_ticket_security_id_generation_table',0);
    DBMiscData = cell2dataset([ColNames;Data]);
    
    [ColNames,Data] = read_from_database('valuation_date_table',0);
    RefDate = cell2dataset([ColNames;Data]);
    SettlementDate = RefDate.settlement_date{1};
    
    % connect to database object
    objDB = connect_to_database;
    
    % read the headers of deal_ticket_table and security_info_table
    TempTablename = ['deal_ticket_table','_',InBUName];
    DealTicketFields = gettablecolumnnames(objDB, TempTablename);
    TempTablename = ['security_info_table','_',InBUName];
    SecurityInfoFields = gettablecolumnnames(objDB, TempTablename);
    
    DBProdData = fetch(objDB,'select invenio_product_code,currency from product_master_table');
    DBProductCode = DBProdData(:,1);
    DBCurrency = DBProdData(:,2);
    
    DBMarketActionData = fetch(objDB,'select market_action,reversal_action,sign from market_action_sign_mapping_table');
    DBMarketAction = DBMarketActionData(:,1);
    DBReversalAction = DBMarketActionData(:,2);
    DBMarketActionSign = cell2mat(DBMarketActionData(:,3));
    
    % for security id generation
    try
        InOptTypes = lower(DSData.OptType);
    catch
        OutErrorMsg = {'OptType column missing in input deals header!'};
        disp(OutErrorMsg);
        return;
    end
    
    NumRows = length(InOptTypes);
    
    OutSecurityData = cell(NumRows,length(SecurityInfoFields));
    OutDealTicketData = cell(NumRows,length(DealTicketFields));
    
    DSSecurityData = cell2dataset([SecurityInfoFields;OutSecurityData]);
    DSDealData = cell2dataset([DealTicketFields;OutDealTicketData]);
    
    % if space is present in the counterparty
    for iC = 1:length(DSData.CounterParty)
        if ~isnan(DSData.CounterParty{iC})
            DSData.CounterParty{iC} = strtrim(DSData.CounterParty{iC});
        end
    end
    
    % find the number of directional deals that need to be booked
    CntrDirDealsData = 0;
    NumDirDeals = length(find(strncmpi('MM',DSData.CounterParty,2) |  strncmpi('RMS',DSData.CounterParty,3)));
    if NumDirDeals > 0
        OutDirDealsData = cell(NumDirDeals,length(DealTicketFields));
        DSDirDealsData  = cell2dataset([DealTicketFields;OutDirDealsData]);
    end
    
    OutErrorNum = [];
    OutLogMsg = {};
    OutDirErrorNum = [];
    OutDirLogMsg = {};
    
    % Iterate over the deals line by line to get the security and deals in db
    % format
    for iDeal = 1:length(InOptTypes)
        if ~isempty(strfind(DSData.Instrument{iDeal},'OR')) % donot remove -O in OR instrument
            % if this is done, the instrument name will be completely changed,
            % since in the actual instrument name AG-OR-SCE,
            % -O in the middle will also be replaced, resulting in
            % a wrong name
            TempInstrument = 'AG-OR -SCE';
        else
            TempInstrument = strrep(upper(DSData.Instrument{iDeal}),'-O',''); % since in db, we store only the base instrumnet name(for futures)
        end
        IdxProdCode = strcmpi(TempInstrument,DBProductMapData.instrument) & ...
            strcmpi(DSData.Portfolio{iDeal},DBProductMapData.subportfolio);
        if isempty(find(IdxProdCode))
            TmpMsg = ['Product Code could not be found for the combination ',TempInstrument,' and ',DSData.Portfolio{iDeal},' for TradeId ',DSData.TRN_Number{iDeal}];
            OutLogMsg = [OutLogMsg;TmpMsg];
            OutErrorNum = [OutErrorNum,iDeal];
        end
        ProductCode = DBProductMapData.product_code(IdxProdCode);
        
        % do the strike, premium, barrier value conversions only to adapt to the
        % format from TR file
        Strike = DSData.Strike(iDeal)* DBProductMapData.strike_factor(IdxProdCode);
        if isempty(Strike) || isnan(Strike)
            Strike = [];
        else
            DSSecurityData.strike(iDeal) = num2cell(Strike);
        end
        Premium = DSData.Prem(iDeal) * DBProductMapData.premium_factor(IdxProdCode);
        if isempty(Premium) || isnan(Premium)
            Premium = [];
        end
        BarrierLevel = DSData.Barrier(iDeal) * DBProductMapData.strike_factor(IdxProdCode);
        if isempty(BarrierLevel) || isnan(BarrierLevel)
            BarrierLevel = [];
        end
        
        ProductCode = char(ProductCode);
        try
            DSData.Month{iDeal} = upper(DSData.Month{iDeal});
        catch
        end
        try
            DSData.CounterParty{iDeal} = upper(DSData.CounterParty{iDeal});
        catch
        end
        try
            DSData.Portfolio{iDeal}    = upper(DSData.Portfolio{iDeal});
        catch
        end
        try
            DSData.Instrument{iDeal}   = upper(DSData.Instrument{iDeal});
        catch
        end
        try
            DSData.TRN_Number{iDeal}   = upper(DSData.TRN_Number{iDeal});
        catch
        end
        try
            DSData.CONTRACTNB{iDeal}   = upper(DSData.CONTRACTNB{iDeal});
        catch
        end
        
        % get the underlying future and option months
        [OptionMonth,FutureMonth] = strtok(DSData.Month{iDeal},'.'); %
        FutureMonth = strrep(FutureMonth,'.','');
        
        UndId = [ProductCode,FutureMonth];
        VolId = [ProductCode,DSData.Month{iDeal}];
        DSSecurityData.underlying1_id{iDeal} = UndId;
        DSSecurityData.derivative_type{iDeal} = InOptTypes{iDeal};
        DSSecurityData.settlement_type{iDeal} = 'physical_settled';
        
        IdxCurr = strcmpi(strtrim(ProductCode),DBProductCode);
        Currency = upper(DBCurrency(IdxCurr));
        if isempty(Currency)
            DSSecurityData.currency(iDeal) = {''};
        else
            DSSecurityData.currency(iDeal) = Currency;
        end
        
        DSSecurityData.contract_month{iDeal} = DSData.Month{iDeal};
        DSSecurityData.product_code{iDeal} = strtrim(ProductCode);
        
        IdxDerivtype = strcmpi(InOptTypes{iDeal},DBMiscData.derivative_type);
        DSSecurityData.settlement_source(iDeal) = DBMiscData.trade_type(IdxDerivtype);
        
        switch InOptTypes{iDeal}
            case {'future','fx_future','equity'} % e.g: CT N4, C  N4
                if strcmpi(InOptTypes{iDeal},'future')
                    DSDealData.trade_group_type{iDeal} = 'FUT';
                end
                if strcmpi(InOptTypes{iDeal},'fx_future')
                    DSDealData.trade_group_type{iDeal} = 'FXFUT';
                end
                if strcmpi(InOptTypes{iDeal},'equity')
                    DSDealData.trade_group_type{iDeal} = 'EQD';
                end
                DSSecurityData.security_id{iDeal} = [ProductCode,FutureMonth];
                IdxUndId = strcmpi(UndId,DBFutureData.underlying_id);
                % if the underlying future id exists in db ,then use that
                % future expiry date; if not, then just populate #N/A so that
                % the same gets updated in master table
                if isempty(find(IdxUndId))
                    DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                    TmpMsg = ['Underlying Future Id ',UndId,' does not exist in database'];
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                else
                    if isempty(DBFutureData.expiry_date{IdxUndId}) && ~strncmpi(ProductCode,'EQ',2)  % if the id exists, but if the expiry date is empty
                        DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                        TmpMsg = ['Expiry date is not present for the Underlying Future Id ',UndId,' in database'];
                        if ~ismember(iDeal,OutErrorNum)
                            OutLogMsg = [OutLogMsg;TmpMsg];
                            OutErrorNum = [OutErrorNum,iDeal];
                        end
                    else
                        DSSecurityData.maturity_date(iDeal) = DBFutureData.expiry_date(IdxUndId);
                    end
                end
            case {'vanilla_call','vanilla_put'} % e.g: BO F5.F5C 16, BO F5.F5P 18.5
                DSDealData.trade_group_type{iDeal} = 'OFUT';
                if strcmpi(InOptTypes{iDeal},'vanilla_call')
                    CallPutId = 'C ';
                else
                    CallPutId = 'P ';
                end
                
                % subportfolio like 'RMS%' and maturity is empty, then
                % exchange_traded
                % subportfolio like 'RMS%' and maturity is not empty, then OTC
                %             if strncmpi(DSData.Portfolio{iDeal},'RMS',3) && ...
                %                     (isempty(DSData.Maturity{iDeal}) || any(isnan(DSData.Maturity{iDeal})))
                try
                    TempMaturity = DSData.Maturity{iDeal};
                catch
                    TempMaturity = DSData.Maturity(iDeal);
                end
                if (isempty(TempMaturity)) || any(isnan(TempMaturity))
                    DSSecurityData.settlement_source{iDeal} = 'exchange_traded';
                    DSSecurityData.security_id{iDeal} = [ProductCode,DSData.Month{iDeal},CallPutId,num2str(Strike)];
                    IdxVolID = strcmpi(VolId,DBOptionData.vol_id);
                    % if the option id exists in db ,then use that
                    % option expiry date; if not, then just populate #N/A so that
                    % the same gets updated in master table
                    if isempty(find(IdxVolID))
                        DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                        TmpMsg = ['Vol Id ',VolId,' does not exist in database'];
                        if ~ismember(iDeal,OutErrorNum)
                            OutLogMsg = [OutLogMsg;TmpMsg];
                            OutErrorNum = [OutErrorNum,iDeal];
                        end
                    else
                        if isempty(DBOptionData.expiry_date{IdxVolID}) % if the id exists, but if the expiry date is empty
                            DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                            TmpMsg = ['Expiry date is not present for the Vol Id ',VolId,' in database'];
                            if ~ismember(iDeal,OutErrorNum)
                                OutLogMsg = [OutLogMsg;TmpMsg];
                                OutErrorNum = [OutErrorNum,iDeal];
                            end
                        else
                            DSSecurityData.maturity_date(iDeal) = DBOptionData.expiry_date(IdxVolID);
                        end
                    end
                else
                    DSSecurityData.settlement_source{iDeal} = 'otc';
                    MaturityDate = datestr(datenum(DSData.Maturity{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                    DSSecurityData.maturity_date{iDeal} = MaturityDate;
                    MaturityDate = strrep(MaturityDate,'-','');
                    MaturityMonth = [MaturityDate,'.',OptionMonth];
                    DSSecurityData.security_id{iDeal} = [ProductCode,MaturityMonth,CallPutId,num2str(Strike)];
                end
                DSSecurityData.vol1_id{iDeal} = VolId;
                
                if isempty(Strike) || Strike == 0
                    DSSecurityData.strike{iDeal} = '#N/A';
                    TmpMsg = 'Strike should not be zero or empty';
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                    
                end
                
            case {'spread_call','spread_put'} % e.g: C  N4.N4 C  Z4.Z4 C 10 , CT N4.N4 CT Z4.Z4 P -1.5
                DSDealData.trade_group_type{iDeal} = 'OFUT';
                %assuming 2 months - Month and Month1 are present for spread types
                if strcmpi(InOptTypes{iDeal},'spread_call')
                    CallPutId = 'C ';
                else
                    CallPutId = 'P ';
                end
                
                DSSecurityData.security_id{iDeal} = [ProductCode,DSData.Month{iDeal},' ',ProductCode,DSData.Month1{iDeal},' ',CallPutId,num2str(Strike)];
                IdxVolID = strcmpi(VolId,DBOptionData.vol_id);
                % if the option id exists in db ,then use that
                % option expiry date; if not, then just populate #N/A so that
                % the same gfets updated in master table
                if isempty(find(IdxVolID))
                    DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                    TmpMsg = ['Vol Id ',VolId,' does not exist in database'];
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                else
                    if isempty(DBOptionData.expiry_date{IdxVolID}) % if the id exists, but if the expiry date is empty
                        DSSecurityData.maturity_date(iDeal) = {'#N/A'};
                        TmpMsg = ['Expiry date is not present for the Vol Id ',VolId,' in database'];
                        if ~ismember(iDeal,OutErrorNum)
                            OutLogMsg = [OutLogMsg;TmpMsg];
                            OutErrorNum = [OutErrorNum,iDeal];
                        end
                    else
                        DSSecurityData.maturity_date(iDeal) = DBOptionData.expiry_date(IdxVolID);
                    end
                end
                DSSecurityData.vol1_id{iDeal} = VolId;
                DSSecurityData.vol2_id{iDeal} = [ProductCode,DSData.Month1{iDeal}];
                [~,FutureMonth1] = strtok(DSData.Month1{iDeal},'.'); %
                FutureMonth1 = strrep(FutureMonth1,'.','');
                Und2Id = [ProductCode,FutureMonth1];
                DSSecurityData.underlying2_id{iDeal} = Und2Id;
                
                if isempty(Strike) || Strike == 0
                    DSSecurityData.strike{iDeal} = '#N/A';
                    TmpMsg = 'Strike should not be zero or empty';
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                    
                end
            case {'euro_up_in_call','euro_up_in_put','euro_down_in_call','euro_down_in_put',...
                    'euro_up_out_call','euro_up_out_put','euro_down_out_call','euro_down_out_put',...
                    'amer_up_in_call','amer_up_in_put','amer_down_in_call','amer_down_in_put',...
                    'amer_up_out_call','amer_up_out_put','amer_down_out_call','amer_down_out_put'}
                DSDealData.trade_group_type{iDeal} = 'OTC';
                % e.g. C  20140701.U4 AUOC 468 565 0 20140701 20140701 20140701
                TradeDate =  datestr(datenum(DSData.TransactionDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                try
                    TempMaturity = DSData.Maturity{iDeal};
                catch
                    TempMaturity = DSData.Maturity(iDeal);
                end
                if isempty(TempMaturity) || any(isnan(TempMaturity))
                    IdxVolID = strcmpi(VolId,DBOptionData.vol_id);
                    if isempty(find(IdxVolID))
                        MaturityDate = '#N/A';
                        TmpMsg = ['Vol Id ',VolId,' does not exist in database'];
                        if ~ismember(iDeal,OutErrorNum)
                            OutLogMsg = [OutLogMsg;TmpMsg];
                            OutErrorNum = [OutErrorNum,iDeal];
                        end
                    else
                        if isempty(DBOptionData.expiry_date{IdxVolID}) % if the id exists, but if the expiry date is empty
                            MaturityDate = '#N/A';
                            TmpMsg = ['Expiry date is not present for the Vol Id ',VolId,' in database'];
                            if ~ismember(iDeal,OutErrorNum)
                                OutLogMsg = [OutLogMsg;TmpMsg];
                                OutErrorNum = [OutErrorNum,iDeal];
                            end
                        else
                            MaturityDate = char(DBOptionData.expiry_date(IdxVolID));
                        end
                    end
                else
                    MaturityDate = datestr(datenum(DSData.Maturity{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                end
                
                if isempty(BarrierLevel) || BarrierLevel == 0
                    BarrierLevel = '#N/A';
                    TmpMsg = 'Barrier Level should not be zero or empty';
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                end
                
                if isempty(Strike) || Strike == 0
                    DSSecurityData.strike{iDeal} = '#N/A';
                    TmpMsg = 'Strike should not be zero or empty';
                    if ~ismember(iDeal,OutErrorNum)
                        OutLogMsg = [OutLogMsg;TmpMsg];
                        OutErrorNum = [OutErrorNum,iDeal];
                    end
                    
                end
                
                DSSecurityData.maturity_date{iDeal} = MaturityDate;
                DSSecurityData.barrier1_obs_startdate{iDeal} = TradeDate;
                DSSecurityData.barrier1_obs_enddate{iDeal} = MaturityDate;
                DSSecurityData.barrier1_level{iDeal} = BarrierLevel;
                Rebate = 0; % assuming Rebate = 0
                DSSecurityData.barrier1_rebate(iDeal) = num2cell(Rebate);
                DSSecurityData.barrier_obs_type(iDeal) = {'intraday'};
                
                TradeDate = strrep(TradeDate,'-','');
                MaturityDate = strrep(MaturityDate,'-','');
                BarrierStartDate = TradeDate;
                BarrierEndDate = MaturityDate;
                MaturityMonth = [MaturityDate,'.',OptionMonth];
                
                % get the barrier short code form
                BarrierCode = DBMiscData.short_code{IdxDerivtype};
                DSSecurityData.security_id{iDeal} = [ProductCode,MaturityMonth,' ',BarrierCode,' ',...
                    num2str(Strike),' ',num2str(BarrierLevel),' ',num2str(Rebate),' ',...
                    TradeDate,' ',BarrierEndDate,' ',BarrierStartDate];
                
                DSSecurityData.vol1_id{iDeal} = VolId;
                if strncmpi(InOptTypes{iDeal},'euro',4)
                    DSSecurityData.vol2_id{iDeal} = VolId;
                end
                
            case {'avg_swap_cc','avg_swap_nc'}
                DSDealData.trade_group_type{iDeal} = 'SWAP';
                %             if strncmpi(ProductCode,'OR',2)
                %                 ProductCode = 'OR ';
                %             end
                AvgStartDate = datestr(datenum(DSData.AveragingStartDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                AvgEndDate =  datestr(datenum(DSData.AveragingEndDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                TradeDate =  datestr(datenum(DSData.TransactionDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                MaturityDate = datestr(datenum(DSData.Maturity{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
                
                DSSecurityData.maturity_date{iDeal} = MaturityDate;
                DSSecurityData.averaging_startdate{iDeal} = AvgStartDate;
                DSSecurityData.averaging_enddate{iDeal} = AvgEndDate;
                DSSecurityData.averaging_frequency{iDeal} = 'daily';
                DSSecurityData.quanto_type{iDeal} = 'division';
                
                AvgStartDate = strrep(AvgStartDate,'-','');
                AvgEndDate = strrep(AvgEndDate,'-','');
                TradeDate = strrep(TradeDate,'-','');
                MaturityDate = strrep(MaturityDate,'-','');
                
                if strcmpi(InOptTypes{iDeal},'avg_swap_cc') %  e.g: OR F5.F5 20141201 20141231 20140113  EUR
                    Currency = 'EUR';
                    DSSecurityData.security_id{iDeal} = [ProductCode,DSData.Month{iDeal},' ',AvgStartDate,' ', AvgEndDate,' ',TradeDate,' ',Currency];
                    DSSecurityData.currency{iDeal} = Currency;
                    DSSecurityData.underlying2_id{iDeal} = ['EURUSD ',MaturityDate,' BGN'];
                else %  e.g: OR F5.F5 20141201 20141231 20140113
                    DSSecurityData.security_id{iDeal} = [ProductCode,DSData.Month{iDeal},' ',AvgStartDate,' ', AvgEndDate,' ',TradeDate,];
                end
                DSData.InitialLots(iDeal) = DSData.InitialLots(iDeal) / 5;
                
            case {'fx_forward_df','fx_forward_ndf','fx_spot'}
                DSDealData.trade_group_type{iDeal} = 'FXD';
                % TODO - security id, underlying id, maturity id needs to be
                % created
                DSSecurityData.settlement_type{iDeal} = 'cash_settled';
        end
        
        %     if isnan(DSData.Spcl_Rate_lot(iDeal))
        %         DSData.Spcl_Rate_lot(iDeal) = 0;
        %     end
        DSDealData.deal_status{iDeal} = 'live';
        % to get the reversal market action and market action sign
        IdxMktAction = strcmpi(DSData.BuySell(iDeal),DBMarketAction);
        Lots = abs(DSData.InitialLots(iDeal)) .* DBMarketActionSign(IdxMktAction);
        
        TransactionDate = datestr(datenum(DSData.TransactionDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
        % check if the transaction date is same as settlement date or not;
        % add the error message if not equal
        %     if ~strcmpi(SettlementDate,TransactionDate)
        %         TmpMsg = 'Transaction date is not the same as settlement date';
        %         if ~ismember(iDeal,OutErrorNum)
        %             OutLogMsg = [OutLogMsg;TmpMsg];
        %             OutErrorNum = [OutErrorNum,iDeal];
        %         end
        %     end
        
        % create the deal ticket table entry
        DSDealData.subportfolio(iDeal)    = DSData.Portfolio(iDeal);
        
        try
            DSDealData.trade_id(iDeal)        = strtrim(DSData.TRN_Number(iDeal));
        catch
            DSDealData.trade_id(iDeal)        = DSData.TRN_Number(iDeal);
        end
        % Check if the trade_id is not empty
        if isempty(DSDealData.trade_id{iDeal}) || ...
                (isnumeric(DSDealData.trade_id{iDeal}) && isnan(DSDealData.trade_id{iDeal}))
            TmpMsg = 'Transaction Number should not be empty';
            if ~ismember(iDeal,OutErrorNum)
                OutLogMsg = [OutLogMsg;TmpMsg];
                OutErrorNum = [OutErrorNum,iDeal];
            end
        end
        
        DSDealData.parent_trade_id(iDeal) = DSData.ParentTransactionNumber(iDeal);
        % Check if the parent_trade_id is not empty
        if isempty(DSDealData.parent_trade_id{iDeal}) || ...
                (isnumeric(DSDealData.parent_trade_id{iDeal}) && isnan(DSDealData.parent_trade_id{iDeal}))
            TmpMsg = 'Parent Transaction Number should not be empty';
            if ~ismember(iDeal,OutErrorNum)
                OutLogMsg = [OutLogMsg;TmpMsg];
                OutErrorNum = [OutErrorNum,iDeal];
            end
        end
        
        DSDealData.transaction_date{iDeal} = datestr(datenum(DSData.TransactionDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
        DSDealData.security_id(iDeal)     = DSSecurityData.security_id(iDeal);
        DSDealData.market_action(iDeal)   = lower(DSData.BuySell(iDeal));
        DSDealData.original_lots(iDeal)   = num2cell(Lots);
        DSDealData.premium{iDeal}         = Premium;
        DSDealData.counterparty_parent(iDeal) = convertNaNtoNullValues(DSData.CounterParty(iDeal));
        DSDealData.execution_broker(iDeal) = convertNaNtoNullValues(DSData.Exe_Broker(iDeal));
        DSDealData.clearing_broker(iDeal) = convertNaNtoNullValues(DSData.CounterParty(iDeal));
        DSDealData.contract_number(iDeal) = convertNaNtoNullValues(DSData.CONTRACTNB(iDeal));
        DSDealData.version_no(iDeal) = num2cell(0);
        DSDealData.exe_type(iDeal) = convertNaNtoNullValues(DSData.ExeType(iDeal));
        DSDealData.brokerage_rate_type(iDeal) = convertNaNtoNullValues(DSData.Rates(iDeal));
        DSDealData.spcl_rate(iDeal) = convertNaNtoNullValues(DSData.Spcl_Rate_lot(iDeal));
        DSDealData.other_remarks(iDeal) = convertNaNtoNullValues(DSData.OtherRemarks(iDeal));
        DSDealData.instrument(iDeal)    = DSData.Instrument(iDeal);
        DSDealData.opt_periodicity(iDeal) = convertNaNtoNullValues(DSData.OTCDAILYEODLEG(iDeal));
        DSDealData.is_netting_done(iDeal) = num2cell(0);
        DSDealData.lots_to_be_netted(iDeal) = num2cell(Lots);
        
        %%
        %         DSDealData.ops_unique(iDeal) = DSData.OPSUnique(iDeal);
        %         DSDealData.broker_lots(iDeal) = num2cell(DSData.BROKERLOTS(iDeal));
        %         DSDealData.ops_action(iDeal) = DSData.OPSAction(iDeal);
        
        if( ismember('OPSUnique',fieldnames(DSData)))
            DSDealData.ops_unique(iDeal) = DSData.OPSUnique(iDeal);
        end
        if( ismember('BROKERLOTS',fieldnames(DSData)))
            DSDealData.broker_lots(iDeal) = num2cell(DSData.BROKERLOTS(iDeal));
        end
        if( ismember('OPSAction',fieldnames(DSData)))
            DSDealData.ops_action(iDeal) = DSData.OPSAction(iDeal);
        end
        if( ismember('Equity_Trade_Type',fieldnames(DSData)))
            DSDealData.equity_trade_type(iDeal) = DSData.Equity_Trade_Type(iDeal);
        end
        
        %%
        % generate the counterparty directional deals
        if (strncmpi('MM',DSData.CounterParty{iDeal},2) ||  strncmpi('RMS',DSData.CounterParty{iDeal},3))
            CntrDirDealsData = CntrDirDealsData + 1;
            
            DSDirDealsData.subportfolio(CntrDirDealsData)    = DSData.CounterParty(iDeal);
            try
                DSDirDealsData.parent_trade_id{CntrDirDealsData} = ['DUP-',strtrim(DSData.ParentTransactionNumber{iDeal})];
            catch
                DSDirDealsData.parent_trade_id{CntrDirDealsData} = '';
            end
            try
                DSDirDealsData.trade_id{CntrDirDealsData}        = ['DUP-',strtrim(DSData.TRN_Number{iDeal})];
            catch
                DSDirDealsData.trade_id{CntrDirDealsData}        = '';
            end
            DSDirDealsData.transaction_date{CntrDirDealsData} = datestr(datenum(DSData.TransactionDate{iDeal},'dd-mm-yyyy'),'yyyy-mm-dd');
            DSDirDealsData.security_id(CntrDirDealsData)     = DSSecurityData.security_id(iDeal);
            DSDirDealsData.market_action(CntrDirDealsData)   = DBReversalAction(IdxMktAction);
            DSDirDealsData.original_lots(CntrDirDealsData)   = num2cell(-(Lots));
            DSDirDealsData.premium{CntrDirDealsData}         = Premium;
            DSDirDealsData.counterparty_parent(CntrDirDealsData) = DSData.Portfolio(iDeal);
            DSDirDealsData.execution_broker(CntrDirDealsData) = convertNaNtoNullValues(DSData.Exe_Broker(iDeal));
            DSDirDealsData.clearing_broker(CntrDirDealsData) = {'NULL'};
            DSDirDealsData.contract_number(CntrDirDealsData) = convertNaNtoNullValues(DSData.CONTRACTNB(iDeal));
            DSDirDealsData.version_no(CntrDirDealsData) = num2cell(0);
            DSDirDealsData.exe_type(CntrDirDealsData) = convertNaNtoNullValues(DSData.ExeType(iDeal));
            DSDirDealsData.brokerage_rate_type(CntrDirDealsData) = convertNaNtoNullValues(DSData.Rates(iDeal));
            DSDirDealsData.spcl_rate(CntrDirDealsData) = convertNaNtoNullValues(DSData.Spcl_Rate_lot(iDeal));
            DSDirDealsData.other_remarks(CntrDirDealsData) = convertNaNtoNullValues(DSData.OtherRemarks(iDeal));
            DSDirDealsData.instrument(CntrDirDealsData)    = DSData.Instrument(iDeal);
            DSDirDealsData.opt_periodicity(CntrDirDealsData) = convertNaNtoNullValues(DSData.OTCDAILYEODLEG(iDeal));
            DSDirDealsData.is_netting_done(CntrDirDealsData) = num2cell(0);
            DSDirDealsData.lots_to_be_netted(CntrDirDealsData) = num2cell(-(Lots));
            
            %%
            %             DSDealData.ops_unique(iDeal) = DSData.OPSUnique(iDeal);
            %             DSDealData.broker_lots(iDeal) = num2cell(DSData.BROKERLOTS(iDeal));
            %             DSDealData.ops_action(iDeal) = DSData.OPSAction(iDeal);
            
            if( ismember('OPSUnique',fieldnames(DSData)))
                DSDirDealsData.ops_unique(CntrDirDealsData) = DSData.OPSUnique(iDeal);
            end
            if( ismember('BROKERLOTS',fieldnames(DSData)))
                DSDirDealsData.broker_lots(CntrDirDealsData) = num2cell(DSData.BROKERLOTS(iDeal));
            end
            if( ismember('OPSAction',fieldnames(DSData)))
                DSDirDealsData.ops_action(CntrDirDealsData) = DSData.OPSAction(iDeal);
            end
            if( ismember('Equity_Trade_Type',fieldnames(DSData)))
                DSDirDealsData.equity_trade_type(CntrDirDealsData) = DSData.Equity_Trade_Type(iDeal);
            end
            %%
            [IsErrorFound,IdxErrorFound] = ismember(iDeal,OutErrorNum);
            if IsErrorFound
                OutDirErrorNum = [OutDirErrorNum;CntrDirDealsData];
                OutDirLogMsg =[OutDirLogMsg;OutLogMsg(IdxErrorFound)];
            end
        end
    end
    
    if ~isempty(OutErrorNum)
        OutSecurityErrorHeader = ['Error Message',SecurityInfoFields];
        OutDealsErrorHeader = ['Error Message',DealTicketFields];
        
        OutSecurityErrorData = dataset2cell(DSSecurityData(OutErrorNum,:));
        OutSecurityErrorData = OutSecurityErrorData(2:end,:);
        OutDealsErrorData = dataset2cell(DSDealData(OutErrorNum,:));
        OutDealsErrorData = OutDealsErrorData(2:end,:);
        
        OutSecurityErrorData = [OutLogMsg,OutSecurityErrorData];
        OutDealsErrorData = [OutLogMsg,OutDealsErrorData];
        if ~isempty(OutDirErrorNum)
            OutDirDealsErrorData = dataset2cell(DSDirDealsData(OutDirErrorNum,:));
            OutDirDealsErrorData = OutDirDealsErrorData(2:end,:);
            
            OutDirDealsErrorData = [OutDirLogMsg,OutDirDealsErrorData];
            DSDirDealsData(OutDirErrorNum,:) = [];
        end
        
        DSSecurityData(OutErrorNum,:) = [];
        DSDealData(OutErrorNum,:) = [];
        
    end
    
    xlswrite(OutSecurityInfoFilename,dataset2cell(DSSecurityData),'Securities');
    if CntrDirDealsData > 0 % if directional deals are found
        DSDirDealsData = dataset2cell(DSDirDealsData);
        DSDealData = dataset2cell(DSDealData);
        % to avoid the header repeated from Directional deals data
        xlswrite(OutDealTicketFilename,[DSDealData;DSDirDealsData(2:end,:)],'Deals');
    else
        xlswrite(OutDealTicketFilename,dataset2cell(DSDealData),'Deals');
    end
    
    if ~isempty(OutErrorNum)
        xlswrite(OutSecurityInfoFilename,[OutSecurityErrorHeader;OutSecurityErrorData],'Errors');
        if ~isempty(OutDirErrorNum)
            xlswrite(OutDealTicketFilename,[OutDealsErrorHeader;OutDealsErrorData;OutDirDealsErrorData],'Errors');
        else
            xlswrite(OutDealTicketFilename,[OutDealsErrorHeader;OutDealsErrorData],'Errors');
        end
    end
    
    try
        DirDealsInfo = {'Number of input internal deals ',num2str(NumDirDeals);'Number of output DIR deals generated ',num2str(CntrDirDealsData)};
        xlswrite(OutDealTicketFilename,DirDealsInfo,'InternalDealsCount');
    catch
        disp('Error while writing DIR deals info!');
    end
    
    try
        xls_delete_sheets(fullfile(pwd,OutSecurityInfoFilename));
        xls_delete_sheets(fullfile(pwd,OutDealTicketFilename));
    catch
    end
    
    OutSecurityInfoFilename = cellstr(OutSecurityInfoFilename);
    OutDealTicketFilename = cellstr(OutDealTicketFilename);
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutSecurityInfoFilename = {''};
    OutDealTicketFilename = {''};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end

end

function OutValue = convertNaNtoNullValues(InValue)
if iscell(InValue)
    if ~isnumeric(cell2mat(InValue)) % cannot be a NaN or numbers
        OutValue = InValue;
    else % should be a NaN or numbers
        OutValue = {'NULL'};
    end
    return;
end
if isnan(InValue)
    OutValue = {'NULL'};
elseif  InValue == 0
    OutValue = {'NULL'};
else
    OutValue = cellstr(InValue);
end
end