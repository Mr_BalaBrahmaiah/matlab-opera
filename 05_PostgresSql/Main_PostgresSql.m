clc
clear
close all


% [DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_all_portfolio',0); %% 'trades_today_all_portfolio' %% 'trades_history_all_portfolio'

% Out = strtok(DBData(:,1),' ');
% UniqueDate = unique(Out);

%% SQL

ViewName = 'helper_quandfund_subpf_mapping_view';  %% helper_settle_pricing_subportfoliovalues_view
[MappingColNames,MappingData] = read_from_database(ViewName,0);

%% PostgreSQL

SqlQuery = 'select * from invenio_read_only.trades_history_all_portfolio where trade_date::text like ''2016-11-15%'' and portfolio_name = ''QF1''';
% SqlQuery = 'select * from invenio_read_only.trades_history_all_portfolio where trade_date::text like ''2016%'' and portfolio_name = ''QF1''';

[DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_all_portfolio',0,SqlQuery);

%% Preprocessing for PostgreSQL Data

if(length(DBData)>1)
    
    Identifier_Col =  cellfun(@(V) strcmpi('identifier',V), DBFieldNames);
    Identifier = DBData(:,Identifier_Col);
    
    [ProductCode_Month, BBG_Code] = strtok(Identifier,' ');
    
    IdxSingle = find(cellfun(@(s) (length(s)<=1), ProductCode_Month));
    Change_BBG_Code = BBG_Code(IdxSingle);
    [Change_Month_code,Remain_BBG_Code] = strtok(Change_BBG_Code,' ');
    
    BBG_Code(IdxSingle) = Remain_BBG_Code;
    BBG_Code = strtrim(BBG_Code);
    
    Change_ProductCoode = ProductCode_Month(IdxSingle);
    
    ProductCode_Month(IdxSingle) = cellstr([char(Change_ProductCoode),char(Change_Month_code)]);
    
    Month_Code = cellfun(@(x) x(end-1:end), ProductCode_Month, 'UniformOutput', false);
    
    ProductCode = ProductCode_Month;
    for i = 1 : length(ProductCode)
        ProductCode{i}((end-1:end)) = [];
    end
    
    %% Mapping SQL Data vs PostgersSQL Data
    
    Map_ProdCol =  cellfun(@(V) strcmpi('product_code',V), MappingColNames);
    Map_ProductCode = MappingData(:,Map_ProdCol);
    
    Map_BBGCol =  cellfun(@(V) strcmpi('bbg_code',V), MappingColNames);
    Map_BBGCode = MappingData(:,Map_BBGCol);
    
    Map_DBProductCol =  cellfun(@(V) strcmpi('db_product_code',V), MappingColNames);
    Map_DBProductCode = MappingData(:,Map_DBProductCol);
    
    DBProduct_Code = cell(length(ProductCode),1);
    DBProduct_Code(:,:) = {' '};
    
    for k = 1 : length(ProductCode)
        Current_ProductCode = strtrim(ProductCode{k});
        Current_BBGCode = strtrim(BBG_Code{k});
        Map_DBProductCode = strtrim(Map_DBProductCode);
        
        MatchedIndex = find(strcmpi(Current_ProductCode, Map_ProductCode) & strcmpi(Current_BBGCode, Map_BBGCode));
        
        if(~isempty(MatchedIndex))
            DBProduct_Code(k,1) = cellstr(strtrim(Map_DBProductCode(MatchedIndex)));
        end
        
    end
    
    DBPortfolio_Col = cellfun(@(V) strcmpi('portfolio_name',V), DBFieldNames);
    DBPortfolio = strtrim(DBData(:,DBPortfolio_Col));
    
    %% Mapping QF1 Support Portfolio vs DB Product Code and DB Portfolio
    
    InBUName = 'qf1';
    SqlQuery = 'select instrument,subportfolio,product_code from subportfolio_product_mapping_table where product_code not like ''%USD%''';
    [Tbl_DBFieldNames,Tbl_DBData] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery,InBUName);
    
    if(length(Tbl_DBData)>1)
        Tbl_InstrumentCol =  cellfun(@(V) strcmpi('instrument',V), Tbl_DBFieldNames);
        Tbl_Instrument = strtrim(Tbl_DBData(:,Tbl_InstrumentCol));
        
        Tbl_ProductCol =  cellfun(@(V) strcmpi('product_code',V), Tbl_DBFieldNames);
        Tbl_ProductCode = strtrim(Tbl_DBData(:,Tbl_ProductCol));
        
        Tbl_PortfolioCol =  cellfun(@(V) strcmpi('subportfolio',V), Tbl_DBFieldNames);
        Tbl_Portfolio = strtrim(Tbl_DBData(:,Tbl_PortfolioCol));
        
        Instrument_Code = cell(length(DBProduct_Code),1);
        Instrument_Code(:,:) = {' '};
        
        for i = 1 : length(DBProduct_Code)
            
            Current_ProductCode = strtrim(DBProduct_Code{i});
            Current_Portfolio = strtrim(DBPortfolio{i});
            
            MatchedIndex = find(strcmpi(Current_ProductCode, Tbl_ProductCode) & strcmpi(Current_Portfolio, Tbl_Portfolio));
            
            if(~isempty(MatchedIndex))
                Instrument_Code(i,1) = cellstr(strtrim(Tbl_Instrument(MatchedIndex)));
            else
                
            end
        end
        
        
        %% Excel Sheet Write
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
            'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
            'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
            'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
        
        %     TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
        %         'OptType','Prem','CounterParty','Initial Lots',...
        %         'CONTRACTNB','Maturity','Barrier Type',...
        %         'Barrier','Transaction Date','TRN.Number','Source',...
        %         'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
        
        Temp = cell(length(DBProduct_Code),1);
        Temp(:,:) = {' '};
        
        BuySell_Col = cellfun(@(V) strcmpi('trade_side_name',V), DBFieldNames);
        BuySell = strtrim(DBData(:,BuySell_Col));
        Buy_Index = (~cellfun(@isempty,regexp(BuySell,'Buy')));   %% Changing in BuySell Variable Buy into bought & Sell into sold
        Sell_Index = (~cellfun(@isempty,regexp(BuySell,'Sell')));
        BuySell(Buy_Index) = {'Bought'};
        BuySell(Sell_Index) = {'Sold'};
        
        Lots_Col = cellfun(@(V) strcmpi('trade_qty',V), DBFieldNames);  %% Actice Lots ans Initial Lots
        Lots_Data = DBData(:,Lots_Col);
        
        Maturity_Trades = strcat(Month_Code,'.',Month_Code);
        
        OptType = cell(length(DBProduct_Code),1);
        OptType(:,:) = {'future'};
        
        Premium_Col = cellfun(@(V) strcmpi('trade_price',V), DBFieldNames);  %% Avg Premium and Prem
        Premium = DBData(:,Premium_Col);
        
        CounterParty_Col = cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);   %% CounterParty and Exe-Broker as same
        CounterParty = DBData(:,CounterParty_Col);
        
        TransactionDate_Col = cellfun(@(V) strcmpi('trade_date',V), DBFieldNames);
        TransactionDate = DBData(:,TransactionDate_Col);
        [TransactionDate,Timing] = strtok(TransactionDate);
        % TransactionDate = cellfun(@(V) datestr(V), TransactionDate,'UniformOutput',false);
        % TransactionDate = cellfun(@(V) x2mdate(V), TransactionDate,'UniformOutput',false);   %% m2xdate %% x2mdate
        % formatOut = 'yyyy-mm-dd';
        % datestr(TransactionDate,formatOut)
        
        Source = cell(length(DBProduct_Code),1);   %% Get Database Name
        Source(:,:) = {'TraderDB'};
        
        Exe_Type = cell(length(DBProduct_Code),1);   %% Get Database Name
        Exe_Type(:,:) = {'Elec'};
        
        Rates = cell(length(DBProduct_Code),1);   %% Get Database Name
        Rates(:,:) = {'Normal'};
        
        %% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType
        
        [~,~,RawData] = xlsread('Mapping_fxfuture.xls');
        Header = RawData(1,:);
        Data = RawData(2:end,:);
        
        for k = 1 : length(Data)
            Current_Instrument = strtrim(Data{k,1});
            Current_OptType = strtrim(Data{k,2});
            MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
            
            if(~isempty(MatchedIndex))
                OptType(MatchedIndex,1) = cellstr(Current_OptType);
            end
            
        end
        
        
        %% WRITE EXCEL SHEET
        
        RowData = [Instrument_Code,DBPortfolio,BuySell,Lots_Data,Maturity_Trades,Temp,OptType,Premium,CounterParty,Temp,Lots_Data,Temp,Premium,...
            Temp,Temp,Temp,Temp,TransactionDate,Temp,Temp,Source,Temp,Temp,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];
        
        %     RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
        %         Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];
        
        Total_Data = [TRHeader;RowData];
        
        XLSFileName = ['QF1_Model_Dump_',char(TransactionDate(1)),'.xlsx'];
        if(exist(XLSFileName))
            delete(XLSFileName);
            xlswrite(XLSFileName,Total_Data);
        else
            xlswrite(XLSFileName,Total_Data);
        end
        
    else
        errordlg('No Data in Support Portfolio DB',InBUName);
        
    end
    
else
    
    errordlg('No Data in DB');
    
end








