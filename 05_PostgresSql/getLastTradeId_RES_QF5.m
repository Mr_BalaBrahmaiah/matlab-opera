function [OutErrorMsg,LastTradeId,TradeId]= getLastTradeId_RES_QF5(InBUName,CellArray)

OutErrorMsg = {'No errors'};
LastTradeId = {''};
TradeId = {''};

NumRows = size(CellArray,1);

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view'; 
    TradeIdPrefix = ['RES-',char(fetch(ObjDB,SqlQuery))];

     if strcmpi(InBUName,'qf5')
        TableName =['deal_ticket_table_with_latest_versions_view_',InBUName];
        SqlQuery = ['select mid(trade_id,16) from ',TableName,' where trade_id like ''',TradeIdPrefix,'%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);
    else
        TableName =['cloned_deal_ticket_table_',InBUName];
        SqlQuery = ['select mid(trade_id,16) from ',TableName,' where trade_id like ''',TradeIdPrefix,'%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);
    end
    
    TempTradeNum = max(str2num(char(DBTradeId)));
    if(isempty(TempTradeNum))
        TempTradeNum = 0;      %% Default
    end
        
    
    LastTradeId = cellstr([TradeIdPrefix,num2str(TempTradeNum)]);
    
    if ~isempty(TempTradeNum)
        NTradeId = (TempTradeNum+1 : 1 : TempTradeNum+NumRows)';
        TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
        TradeId = strrep(TradeId,' ','');  %% Remove Spaces
    end   
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    LastTradeId = '';
    TempTradeNum = '';    
end


