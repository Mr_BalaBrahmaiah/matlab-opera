function OutFileName = getXLSFilename(Reportname)

objDB = connect_to_database;
DBValueDate = fetch(objDB,'select value_date from valuation_date_table');
DBValueDate = datestr(datenum(DBValueDate,'yyyy-mm-dd'),'dd-mmm-yyyy');

CurrentDateTime = strsplit(datestr(now),' ');
Time = CurrentDateTime{2};

TempName = [char(Reportname),'_',DBValueDate,'_',Time];
TempName = strrep(TempName,':','-');
TempName = strrep(TempName,' ','_');
OutFileName =  [TempName,'.xlsx'];