function [NettedTRData,ErrorData,Total_Data,TRHeader] = Get_TR_Format_QF5(InBUName,DBFieldNames,DBData,Instrument_Code,DBPortfolio,DBProduct_Code)

global ObjDB ;

NettedTRData = [];
ErrorData = [];
Total_Data = [];

%%  Transaction Number
%         ObjDB = connect_to_database;

TempTradeId = '';                               %% Transaction Number
try
    [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
    if strcmpi(OutErrorMsg,'No Errors')
        % TradeIdPrefix = 'TR-FY16-17-';
        SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
        TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
        TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
        
        if(isempty(TempTradeId))
            TempTradeId = 0;      %% Default
        end
    end
catch
    TempTradeId = '';
end


%% Excel Sheet Write

%     TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
%         'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
%         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
%         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
%         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
%         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};

TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
    'OptType','Prem','CounterParty','Initial Lots',...
    'CONTRACTNB','Maturity','Barrier Type',...
    'Barrier','Transaction Date','TRN.Number','Source',...
    'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp','Equity_Trade_Type'};

Temp = cell(length(DBProduct_Code),1);
Temp(:,:) = num2cell(NaN); %% Temp(:,:) = {''};  %% Temp(:,:) = num2cell(NaN);

TradeType_Col = cellfun(@(V) strcmpi('trade_type',V), DBFieldNames);
Swap_Index = cellStrfind_exact(DBData(:,TradeType_Col),{'swap'});
DBPortfolio(Swap_Index,:) = strcat('RES-',DBPortfolio(Swap_Index,:));

BuySell_Col = cellfun(@(V) strcmpi('trade_side_name',V), DBFieldNames);
BuySell = strtrim(DBData(:,BuySell_Col));
Trade_Type = BuySell ;
% Buy_Index = (~cellfun(@isempty,regexp(BuySell,'Buy')));   %% Changing in BuySell Variable Buy into bought & Sell into sold
% Sell_Index = (~cellfun(@isempty,regexp(BuySell,'Sell')));
Buy_Index = cellStrfind(lower(BuySell),{'buy'});
Sell_Index = cellStrfind(lower(BuySell),{'sell'});
BuySell(Buy_Index) = {'Bought'};
BuySell(Sell_Index) = {'Sold'};

Lots_Col = cellfun(@(V) strcmpi('trade_qty',V), DBFieldNames);  %% Actice Lots ans Initial Lots
Lots_Data = DBData(:,Lots_Col);
IdxSell = strcmpi(BuySell,'Sold');
Lots_Data(IdxSell) = num2cell(cell2mat(Lots_Data(IdxSell)) .* -1);

Maturity_Trades = Temp ; %% strcat(Month_Code,'.',Month_Code);

OptType = cell(length(DBProduct_Code),1);
OptType(:,:) = {'future'};
Swap_Index = cellStrfind_exact(DBData(:,TradeType_Col),{'swap'});
Cash_Index = cellStrfind_exact(DBData(:,TradeType_Col),{'cash'});
OptType(Swap_Index,:) = {'equity_swap'};
OptType(Cash_Index,:) = {'equity'};

Premium_Col = cellfun(@(V) strcmpi('trade_price',V), DBFieldNames);  %% Avg Premium and Prem
Premium = DBData(:,Premium_Col);

CounterParty_Col = cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);   %% CounterParty and Exe-Broker as same
CounterParty = DBData(:,CounterParty_Col);

TraderName_Col = cellfun(@(V) strcmpi('trader_name',V), DBFieldNames);
TraderName = DBData(:,TraderName_Col);

TransactionDate_Col = cellfun(@(V) strcmpi('trade_date',V), DBFieldNames);
ActualTransactionDate = DBData(:,TransactionDate_Col);
[TransactionDate,Timing] = strtok(ActualTransactionDate);

% TransactionDate = cellfun(@(V) datestr(V), TransactionDate,'UniformOutput',false);
% TransactionDate = cellfun(@(V) x2mdate(V), TransactionDate,'UniformOutput',false);   %% m2xdate %% x2mdate
% formatOut = 'yyyy-mm-dd';
% datestr(TransactionDate,formatOut)

Source = cell(length(DBProduct_Code),1);   %% Get Database Name
Source(:,:) = {'TraderDB'};

Exe_Type = cell(length(DBProduct_Code),1);   %% Get Database Name
Exe_Type(:,:) = {'Elec'};

Rates = cell(length(DBProduct_Code),1);   %% Get Database Name
Rates(:,:) = {'Normal'};

%% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType

[~,~,RawData] = xlsread('Mapping_derivativeType.xlsx');
Header = RawData(1,:);
Data = RawData(2:end,:);

for k = 1 : length(Data)
    Current_Instrument = strtrim(Data{k,1});
    Current_OptType = strtrim(Data{k,2});
    MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
    
    if(~isempty(MatchedIndex))
        OptType(MatchedIndex,1) = cellstr(Current_OptType);
    end
    
end


%% WRITE EXCEL SHEET

%     RowData = [Instrument_Code,DBPortfolio,BuySell,Lots_Data,Maturity_Trades,Temp,OptType,Premium,CounterParty,Temp,Lots_Data,Temp,Premium,...
%         Temp,Temp,Temp,Temp,TransactionDate,Temp,Temp,Source,Temp,Temp,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];

RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
    Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp,TraderName,ActualTransactionDate,Trade_Type];

Total_Data = [TRHeader;RowData];

UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date','Equity_Trade_Type'};
SumFields = {'Initial Lots'};
NettedTRData = calc_netted_data(TRHeader, RowData,TRHeader,UniqueFields,SumFields,[]);

IdxEmpty = find(cellfun(@(s) (length(s)<=1), NettedTRData(:,1)));  %%  Find Empty Instrument
ErrorData = NettedTRData(IdxEmpty,:);   %% Move Empty Rows to Errors Sheet
ErrorData = [TRHeader;ErrorData];       %% Add Header

NettedTRData(IdxEmpty,:) = [];      %% Find Empty Instrument and Remove Empty rows in NettedTRData

%% Compute TradeID
% NettedTRData = NettedTRData(2:end,:);
% OptType_Index = cellStrfind_exact(TRHeader,{'OptType'});
% EquitySwap_Index = cellStrfind_exact(NettedTRData(:,OptType_Index),{'equity_swap'});
% EquityFuture_Index = cellStrfind_exact(NettedTRData(:,OptType_Index),{'equity','future'});
% 
% TRNNum_Col = cellStrfind_exact(TRHeader,{'TRN.Number'});
% ParTRNNum_Col = cellStrfind_exact(TRHeader,{'Parent Transaction Number'});
% if(~isempty(EquitySwap_Index))
%     [OutErrorMsg,LastTradeId,TradeID]= getLastTradeId_RES_QF5(InBUName,NettedTRData(EquitySwap_Index,:));
%     NettedTRData(EquitySwap_Index,TRNNum_Col) = TradeID;
%     NettedTRData(EquitySwap_Index,ParTRNNum_Col) = TradeID;
% end
% 
% if(~isempty(EquityFuture_Index))
%     [TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(NettedTRData(EquityFuture_Index,:),InBUName);
%     NettedTRData(EquityFuture_Index,TRNNum_Col) = TradeID;
%     NettedTRData(EquityFuture_Index,ParTRNNum_Col) = TradeID;
% end
% 
% NettedTRData = [TRHeader;NettedTRData];

%%%%%%%%%%% OLD %%%%%%%%%%%%%%%%%
% if ~isempty(TempTradeId)
%     PosTrnId = strcmpi('TRN.Number',TRHeader);
%     PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
%     NumRows = size(NettedTRData,1) - 1; % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
%     NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
%     TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
%     TradeId = strrep(TradeId,' ','');  %% Remove Spaces
%     NettedTRData(2:end,PosTrnId) = TradeId;
%     NettedTRData(2:end,PosParentTrnId) = TradeId;
% end

%% Changing Instrument KMA to KMA1

if(size(NettedTRData,1) > 1)
    
    Instrument_Col = strcmpi('Instrument',TRHeader);
    Changed_Instrument = strrep( NettedTRData(2:end,Instrument_Col) ,'KMA INDEX FUTURE' ,'KMA1 INDEX FUTURE');
    NettedTRData(2:end,1) = Changed_Instrument ;
    
end


