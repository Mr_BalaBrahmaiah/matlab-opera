function [DBFieldNames,DBData] = read_from_database(InTableName,IsDateNeeded,varargin)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 06:15:07 $
%  $Revision: 1.6 $
%

DBFieldNames = [];
DBData       = [];

try
    javaaddpath('mysql-connector-java-5.1.25-bin.jar');
    % Read the database configuration file to read the DB configuration
    % Default DB config values
    DBName = 'dbsystemuat';
    DBUserName = 'user';
    DBPassword = 'invenio@123';
    IsFromServer = 1;
    DBServerIP = '192.168.37.60';
    try
        DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        disp(['Using the database ',DBName]);
    catch
        disp('Error occured in reading the DatabaseConfiguration.txt, hence using the default configuration!');
    end
    
    %   objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.194');
    if IsFromServer
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
    else
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL');
    end
    
    if ~isempty(varargin) % if sql query is passed as an input
        IsQueryInput = 1;
    else
        IsQueryInput = 0;
    end
    if isconnection(objDB)
        if IsQueryInput
            SqlQuery = varargin{1};
            NumInputs = nargin - 2;
            if NumInputs == 1
                SqlQuery = varargin{1};
            elseif NumInputs == 2
                SqlQuery = varargin{1};
                BU       = lower(varargin{2});
                DBTableName = [InTableName,'_',BU];
                if isempty(SqlQuery)
                     SqlQuery = ['select * from `',char(DBTableName),'`'];
                else
                    SqlQuery = strrep(SqlQuery,InTableName,DBTableName);
                end
                
            end
            
        else            
            if IsDateNeeded
                ValuationDate = fetch(objDB,'select value_date from valuation_date_table');
                SqlQuery = ['select * from `',char(InTableName),'` where value_date = ''',char(ValuationDate),''''];
            else
                SqlQuery = ['select * from `',char(InTableName),'`'];
%                 SqlQuery = ['select * from `',char(InTableName),'` where vol1_id = ''C  N6.N6'''];% for CSO
% SqlQuery = ['select * from `',char(InTableName),'` where security_id like ''S%.X6 EUOC %20160622'' '];
%  SqlQuery = ['select * from `',char(InTableName),'` where p1_name = ''OR K5'''];
            end
        end
        
        curs = exec(objDB,SqlQuery);
        curs = fetch(curs);
        AttrObj = attr(curs);
        DBFieldNames = {AttrObj.fieldName};
        DBData = curs.Data;
        
        close(objDB);
    end
catch ME
    if isconnection(objDB)
         close(objDB);
    end
    errordlg(ME.message);    
end

end