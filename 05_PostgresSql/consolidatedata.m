function [OutputFields,ConsDeals] = consolidatedata(Inputheading, InputMatrix,varargin)

% ConsDeals = '';
try
    NumInputs = nargin - 2;
    
    if NumInputs > 0
        UniqueFields  = varargin{1};
        SumFields     = varargin{2};
        OutputFields  = varargin{3};
        WeightedAverageFields = varargin{4};
    else
        UniqueFields = [];
        SumFields = [];
        OutputFields = Inputheading;
        WeightedAverageFields = [];
    end
    
    if ~isempty(WeightedAverageFields)
        Values = WeightedAverageFields(:,1);
        Weights = WeightedAverageFields(:,2);
    else
        Values = ''; Weights = '';
    end
    
    warning('OFF','MATLAB:nonIntegerTruncatedInConversionToChar');
    
    if ~isempty(UniqueFields)
        ConcatenateStr = '';
        % Get the Input Matrix values
        for iInput = 1:length(UniqueFields)
            StrField = UniqueFields{iInput};
            Pos = strcmp(StrField, Inputheading);
            InValue =  getvalidarray(InputMatrix(:,Pos));
            if isnumeric(InValue)
                ConcatenateStr = strcat(ConcatenateStr,num2str(InValue));
            else
                ConcatenateStr = strcat(ConcatenateStr,upper(InValue));
            end
        end
        
        [~,L1IdxIn,L1IdxOut] = unique(upper(ConcatenateStr),'stable');
        
        ConsDeals = cell(length(L1IdxIn), length(OutputFields));
        ConsDeals(:,:) = {' '};
        
        newIdx = unique(L1IdxOut,'stable');
        
        for iIdx = 1:length(newIdx)
            AllIdx = find(L1IdxOut == newIdx(iIdx));
            Idx = AllIdx(1);
            
            for iOut = 1:length(OutputFields)
                StrField = OutputFields{iOut};
                Pos = strcmp(StrField, Inputheading);
                if ~isempty(SumFields) && ismember(StrField,SumFields)
                    InValue =  getvalidarray(InputMatrix(AllIdx,Pos));
                    if isnumeric(InValue)
                        %                     ConsDeals(iIdx,iOut) = num2cell(sum(InValue));
                        ConsDeals(iIdx,iOut) = num2cell(nansum(InValue)); % to provide the sum values even if NaN exists
                    else
                        ConsDeals(iIdx,iOut) = num2cell(NaN);
                    end
                elseif ~isempty(WeightedAverageFields) && ismember(StrField,Values)
                    WeightName = Weights(strcmpi(Values,StrField));
                    PosWeight = strcmp(WeightName, Inputheading);
                    InValue =  getvalidarray(InputMatrix(AllIdx,Pos));
                    InWeight = getvalidarray(InputMatrix(AllIdx,PosWeight));
                    if isnumeric(InValue) && isnumeric(InWeight)
                        %                     ConsDeals(iIdx,iOut) = num2cell(sum(InValue.*InWeight)./ sum(InWeight));
                        ConsDeals(iIdx,iOut) = num2cell(nansum(InValue.*InWeight)./ nansum(InWeight));
                    else
                        ConsDeals(iIdx,iOut) = num2cell(NaN);
                    end
                else
                    %                 InValue = getvalidarray(InputMatrix(Idx,Pos));
                    %                 if isnumeric(InValue)
                    %                     ConsDeals(iIdx,iOut) = num2cell(InValue);
                    %                 else
                    %                     ConsDeals(iIdx,iOut) = InValue;
                    %                 end
                    InValue = getvalidarray(InputMatrix(AllIdx,Pos));
                    if ~isnumeric(InValue)
                        InValue = upper(InValue);
                        TempValue = unique(InValue);
                    else
                        TempValue =  unique(round(InValue,8));
                    end
                    %                 TempValue = unique(InValue); %% unique(InValue); %% unique(round(InValue,4)) ;  %% num2cell(unique(round(cell2mat(InValue),4)))
                    if length(TempValue) > 1
                        if isnumeric(TempValue)
                            TempValue = NaN;
                        else
                            TempValue = {' '};
                        end
                    end
                    if isnumeric(TempValue)
                        ConsDeals(iIdx,iOut) = num2cell(TempValue);
                    else
                        ConsDeals(iIdx,iOut) = TempValue;
                    end
                end
                
                % weightedAverage
                %     OutWA = sum(values.*weights)./ sum(weights);
                %     Prem = sum(ActiveLots(AllIdx).*TradedPrice(AllIdx))./Lots;
                
            end
        end
    else
        ConsDeals = cell(size(InputMatrix,1), length(OutputFields));
        ConsDeals(:,:) = {' '};
        for iOut = 1:length(OutputFields)
            StrField = OutputFields{iOut};
            Pos = strcmp(StrField, Inputheading);
            InValue = getvalidarray(InputMatrix(:,Pos));
            if isnumeric(InValue)
                ConsDeals(:,iOut) = num2cell(InValue);
            else
                ConsDeals(:,iOut) = InValue;
            end
        end
    end
    
    if iscolumn(OutputFields)
        OutputFields = OutputFields';
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
end
end

