function PC_Name = get_PC_Name()

PC_Name = '';

[ret, PC_Name] = system('hostname');   

if ret ~= 0,
   if ispc
      PC_Name = getenv('COMPUTERNAME');
   else      
      PC_Name = getenv('HOSTNAME');      
   end
end

PC_Name = strtrim(PC_Name) ;

