function GrainSmart_2_Opera()

try
    %% Fetch GrainSmart Data
    
    [TR_Header,GrainSmart_ColNames,GrainSmart_DBData,OutTRFormat,Empty_Instrument_TRFormat,Empty_Instrument_DBData] = GrainSmart_2_Opera_TR_Format();
    
    if(isempty(GrainSmart_DBData) || strcmpi(GrainSmart_DBData,'No Data'))
        warndlg('No Data in GrainSmart DB','Check') ;
        return;
    end
    
    %% Fetch Opera Data
    ObjDB = connect_to_database ;
    
    SqlQuery = 'select * from deal_ticket_table_with_latest_versions_view_grn where counterparty_entity = ''GRAIN SMART'' ';
    [Opera_ColNames,Opera_DBData ] = Fetch_DB_Data(ObjDB,SqlQuery) ;
    Opera_DealTicTbl = cell2dataset([Opera_ColNames;Opera_DBData]);
    
    %% Find New Unique Trades
    
    New_OPS_UniqNo = setdiff(GrainSmart_DBData.trade_blotter_nbr,Opera_DealTicTbl.ops_unique);
    if(~isempty(New_OPS_UniqNo))
        New_OpsUniq_Index = sort(cellStrfind_exact(OutTRFormat.OPSUnique , New_OPS_UniqNo));
        
        New_Trade_TR_Format = OutTRFormat(New_OpsUniq_Index,:) ;
        
        CellArray = cell(size(New_Trade_TR_Format,1),1) ;
        [New_TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(CellArray,'grn');
        
        New_Trade_TR_Format.TRN_Number(:,:) = New_TradeId ; %% Change Trade ID
        New_Trade_TR_Format.ParentTransactionNumber(:,:) = New_TradeId ; %% Change Trade ID
        
    else
        New_Trade_TR_Format = '';
    end
    
    
    
    %% Find Modify Trades
    
    Mod_OPS_UniqNo = OutTRFormat.OPSUnique( cellStrfind_exact(OutTRFormat.OPSAction , {'MOD'}) );
    if(~isempty(Mod_OPS_UniqNo))
        Mod_OpsUniq_Index = sort(cellStrfind_exact(OutTRFormat.OPSUnique , Mod_OPS_UniqNo));
        
        Mod_Trade_TR_Format = OutTRFormat(Mod_OpsUniq_Index,:) ;
        
        Mod_TradeID = Opera_DealTicTbl.trade_id( cellStrfind_exact(Opera_DealTicTbl.ops_unique , Mod_OPS_UniqNo) );
        Mod_Trade_TR_Format.TRN_Number(:,:) = Mod_TradeID ; %% Change Trade ID
        Mod_Trade_TR_Format.ParentTransactionNumber(:,:) = Mod_TradeID ; %% Change Trade ID
    else
        Mod_Trade_TR_Format = '';
    end
    
    
    %% Find Delete Trades
    
    Del_OPS_UniqNo = OutTRFormat.OPSUnique( cellStrfind_exact(OutTRFormat.OPSAction , {'DEL'}) );
    if(~isempty(Del_OPS_UniqNo))
        Del_OpsUniq_Index = sort(cellStrfind_exact(OutTRFormat.OPSUnique , Del_OPS_UniqNo));
        
        Del_Trade_TR_Format = OutTRFormat(Del_OpsUniq_Index,:) ;
        
        Del_TradeID = Opera_DealTicTbl.trade_id( cellStrfind_exact(Opera_DealTicTbl.ops_unique , Del_OPS_UniqNo) );
        Del_Trade_TR_Format.TRN_Number(:,:) = Del_TradeID ; %% Change Trade ID
        Del_Trade_TR_Format.ParentTransactionNumber(:,:) = Del_TradeID ; %% Change Trade ID
        
        Del_Trade_TR_Format.InitialLots(:,:) = {0} ;
        Del_Trade_TR_Format.OPSAction(:,:) = {'DEL'} ;
        Del_Trade_TR_Format.Prem(:,:) = {0} ;
    else
        Del_Trade_TR_Format = '';
    end
    
    %% Overall TR Format
    
    Overall_TR_Format = [New_Trade_TR_Format ; Mod_Trade_TR_Format ; Del_Trade_TR_Format] ;
    
    %% Excel Write
    
    OutXLSName  = getXLSFilename('GrainSmart_2_Opera') ;
    
    if(~isempty(Overall_TR_Format))
        
        if(~isempty(Overall_TR_Format))
            xlswrite(OutXLSName,dataset2cell(Overall_TR_Format),'Overall_TR_Dump') ;
        end
        
        if(~isempty(GrainSmart_DBData))
            xlswrite(OutXLSName,dataset2cell(GrainSmart_DBData),'GrainSmart_DBData') ;
        end
        
        if(~isempty(OutTRFormat))
            xlswrite(OutXLSName,dataset2cell(OutTRFormat),'GrainSmart_TR_Format') ;
        end
        
        if(~isempty(New_Trade_TR_Format))
            xlswrite(OutXLSName,dataset2cell(New_Trade_TR_Format),'New_Trade_TR_Format') ;
        end
        
        if(~isempty(Mod_Trade_TR_Format))
            xlswrite(OutXLSName,dataset2cell(Mod_Trade_TR_Format),'Mod_Trade_TR_Format') ;
        end
        
        if(~isempty(Del_Trade_TR_Format))
            xlswrite(OutXLSName,dataset2cell(Del_Trade_TR_Format),'Del_Trade_TR_Format') ;
        end
        
        if(~isempty(Empty_Instrument_TRFormat))
            xlswrite(OutXLSName,dataset2cell(Empty_Instrument_DBData),'GrainSmart_Empty_Instrument') ;
            xlswrite(OutXLSName,dataset2cell(Empty_Instrument_TRFormat),'TR_Format_Empty_Instrument') ;
        end
        
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        xls_change_activesheet(fullfile(pwd,OutXLSName),'Overall_TR_Dump') ;
        
        %         PC_Name = get_PC_Name();
        %         if(~isempty(PC_Name))
        %             xls_move_to_folder(OutXLSName) ;
        %         end
    else
        xlswrite(OutXLSName,{'New and Modified Trades are not found'},'Overall_TR_Dump') ;
        msgbox('New and Modified Trades are not found','Check DB');
    end
    
catch ME
    OutErrorMsg = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end
end