function [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery,varargin)

DBFieldNames = {'No Data'};
DBData = {''};

%%

if(isempty(SqlQuery))
   NewQuery = ['select * from ',char(varargin)];
   SqlQuery =  NewQuery;
end

%%
try
curs = exec(ObjDB,SqlQuery);
curs = fetch(curs);
AttrObj = attr(curs);
DBFieldNames = {AttrObj.fieldName};
DBData = curs.Data;

catch ME
    DBFieldNames = {'Error Data'};
    DBData = {''};
end
end