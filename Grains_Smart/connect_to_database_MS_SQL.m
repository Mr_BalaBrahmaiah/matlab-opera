function ObjDB = connect_to_database_MS_SQL
% connect to the database configuration as provided in the
% DatabaseConfiguration file
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/01/23 07:18:53 $
%  $Revision: 1.1 $
%

try
    javaaddpath('sqljdbc4.jar');
    % Read the database configuration file to read the DB configuration
    % Default DB config values
    DBName = 'SSUSAPRD';
    DBUserName = 'OPERA';
    DBPassword = 'Olam$123';
    IsFromServer = 1;
    DBServerIP = '10.24.1.170';
    DBServerPort = 1433;
    try
        DBConfigData = textread('DatabaseConfiguration_MS_SQL.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'server_port');
        if ~isempty(IdxFound)
            DBServerPort = strsplit(char(DBConfigData(IdxFound)),'=');
            DBServerPort = char(DBServerPort(end));
            DBServerPort = str2double(DBServerPort);
        end
        %         disp(['Using the database ',DBName]);
        fprintf('Using the database : %s : IP : %s\n',DBName,DBServerIP);
    catch
        disp('Error occured in reading the DatabaseConfiguration_PostGresSQL.txt, hence using the default configuration!');
    end
    
    if IsFromServer
        ObjDB = database(DBName,DBUserName,DBPassword,'Vendor','Microsoft SQL Server','Server',DBServerIP,'PortNumber',DBServerPort);
    else
        ObjDB = database(DBName,DBUserName,DBPassword,'Vendor','Microsoft SQL Server','DriverType','thin');
    end
catch ME
    if isconnection(ObjDB)
        close(ObjDB);
    end
    errordlg(ME.message);
end
end