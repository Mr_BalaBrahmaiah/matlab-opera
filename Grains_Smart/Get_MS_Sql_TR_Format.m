% function [OutErrorMsg ,OutXLSName] = Get_MS_Sql_TR_Format()

OutErrorMsg = {'No Errors'};
OutXLSName = getXLSFilename('MS_SQL_TR_Format');

TR_Header = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
    'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date',...
    'Parent Transaction Number','Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG',...
    'Trader Name','Traded DateTimestamp','OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'} ;

%%
try
    %% Fetch SQL Data
    
    ObjDB = connect_to_database;
    
    Settle_Date =  {'2017-03-26'} ;%%fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    SqlQuery_1 = 'select * from grainsmart_price_conversion_table' ;
    [PriceConversion_ColNames,PriceConversion_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    PriceConversion_Tbl = cell2dataset([PriceConversion_ColNames;PriceConversion_Data]);
    
    SqlQuery_2 = 'select * from subportfolio_product_mapping_table_grn where subportfolio = ''GR-GRAIN-SMART'' ';
    [ProdCodeTbl_ColNames,ProdCodeTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_2);
    ProdCode_Mapping_Tbl = cell2dataset([ProdCodeTbl_ColNames;ProdCodeTbl_Data]);
    
    SqlQuery_3 = ['select * from vol_id_table where expiry_date >= ''',char(Settle_Date),''''];
    [VolID_ColNames,VolID_Data] = Fetch_DB_Data(ObjDB,SqlQuery_3);
    VolID_Tbl = cell2dataset([VolID_ColNames;VolID_Data]);
    
    %% Fetch MS SQL Data
    ObjDB_MS_SQL = connect_to_database_MS_SQL;
    
    ViewName = 'opera_derivativesnew_v' ;  %% opera_derivatives_v
    [ColNames,DBData] = Fetch_DB_Data(ObjDB_MS_SQL,[],ViewName);
    
    DBData = cell2dataset([ColNames ;DBData]) ;
    
    %% Make TR Format
    if(size(DBData,2) > 1)
        OutTRFormat = cell2dataset([TR_Header ; cell(size(DBData,1),size(TR_Header,2)) ]);
        for i = 1 : size(DBData,1)
            if(length(DBData.bot_commodity_symbol{i})==1)
                Current_ProdCode = strcat('CM',{' '},DBData.bot_commodity_symbol(i),{'  '} );
                Current_BBGOpt_Ticker = strcat(DBData.bot_commodity_symbol(i),{' '},DBData.EX_CODE_MONTH(i) ) ;
            else
                Current_ProdCode = strcat('CM',{' '},DBData.bot_commodity_symbol(i),{' '} );
                Current_BBGOpt_Ticker = strcat(DBData.bot_commodity_symbol(i),DBData.EX_CODE_MONTH(i) ) ;
            end
            
            Current_UniqueInstrument = unique(ProdCode_Mapping_Tbl.instrument(strcmpi(ProdCode_Mapping_Tbl.product_code,Current_ProdCode)));
            if(~isempty(Current_UniqueInstrument))
                OutTRFormat.Instrument(i) = Current_UniqueInstrument;
            else
                OutErrorMsg = cellstr(['Instrument is not availabe in Corresponding Product Code : ',char(Current_ProdCode)]);
                errordlg(['Instrument is not availabe in Corresponding Product Code : ',{' '},char(Current_ProdCode)]);
                %return;
            end
            
            OutTRFormat.Portfolio(i) = {'GR-GRAIN-SMART'} ; %% Hard Code ;
            
            if(strcmpi(DBData.buy_sell(i),'B'))
                OutTRFormat.BuySell(i) = {'Bought'} ;
            else
                OutTRFormat.BuySell(i) = {'Sold'} ;
            end
            
            if(strcmpi(DBData.put_call(i),'C') || strcmpi(DBData.put_call(i),'P') )
                if(strcmpi(DBData.put_call(i),'C'))
                    OutTRFormat.OptType(i) = {'Vanilla_Call'};
                else
                    OutTRFormat.OptType(i) = {'Vanilla_Put'};
                end
                Matched_Index_VolID = strcmpi(VolID_Tbl.bbg_opt_ticker,Current_BBGOpt_Ticker);
                Current_VolID = strsplit( char(VolID_Tbl.vol_id(Matched_Index_VolID)),' ') ;
                OutTRFormat.Month(i) = Current_VolID(end);
                
            else
                OutTRFormat.OptType(i) = {'Future'};
                OutTRFormat.Month(i) = strcat(DBData.EX_CODE_MONTH(i),{'.'},DBData.EX_CODE_MONTH(i)) ;
            end
            
            Matched_Price_Coneversion = find(strcmpi(PriceConversion_Tbl.product_code,Current_ProdCode));
            if(~isempty(Matched_Price_Coneversion))
                Current_PriceConv = PriceConversion_Tbl.price_conversion(Matched_Price_Coneversion);
                OutTRFormat.Strike(i) = num2cell( DBData.strike_price(i).* Current_PriceConv ) ;
                OutTRFormat.Prem(i) = num2cell( DBData.price(i).* Current_PriceConv );
            else
                OutErrorMsg = cellstr(['Price Conversion is not availabe in Corresponding Product Code : ',char(Current_ProdCode)]);
                %                 errordlg(['Price Conversion is not availabe in Corresponding Product Code : ',{' '},char(Current_ProdCode)]);
                %                 return;
            end
            
            OutTRFormat.CounterParty(i) = DBData.trading_broker_id(i);
            OutTRFormat.InitialLots(i) = num2cell(DBData.LOTS(i));
            OutTRFormat.TransactionDate(i) = DBData.trade_date(i);
            OutTRFormat.Source(i) = DBData.Source_system(i);
            OutTRFormat.Exe_Broker(i) = DBData.trading_broker_id(i);
            
            OutTRFormat.ExeType(i) = {'Elec'}; %% Hard Code ;
            OutTRFormat.Rates(i) = {'Normal'};%% Hard Code ;
            
            OutTRFormat.TraderName(i) = DBData.name(i);
            OutTRFormat.TradedDateTimestamp(i) = DBData.add_Date(i);
            OutTRFormat.OPSUnique(i) = DBData.trade_blotter_nbr(i);
            OutTRFormat.BROKERLOTS(i) = num2cell(DBData.LOTS(i));
            OutTRFormat.OPSAction(i) = DBData.TRD_FLG(i);
            
            OutTRFormat.CounterpartyEntity(i) = {'GRAIN SMART'} ; %% Hard Code ;
            
            
        end
        
        %% Check Empty Instrument and Remove
        
        Instrument_Empty_Index = cellfun(@(s) (isempty(s)), OutTRFormat.Instrument) ;
        Empty_Instrument_TRFormat = OutTRFormat(Instrument_Empty_Index,:);
        Empty_Instrument_DBData = DBData(Instrument_Empty_Index,:);
        
        OutTRFormat(Instrument_Empty_Index,:) = [];
        DBData(Instrument_Empty_Index,:) = [];
        
        %% Excel Sheet Write
        
        OutTRFormat = dataset2cell(OutTRFormat) ;
        
        xlswrite(OutXLSName,[TR_Header ; OutTRFormat(2:end,:)],'TR_Format') ;
        xlswrite(OutXLSName,dataset2cell(DBData),'Original Data');
        
        if(sum(Instrument_Empty_Index) > 0)
            xlswrite(OutXLSName,dataset2cell(Empty_Instrument_TRFormat),'Empty_Instrument_TR_Format');
            xlswrite(OutXLSName,dataset2cell(Empty_Instrument_DBData),'Empty_Instrument_Original');
        end
        
        
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        xls_change_activesheet(fullfile(pwd,OutXLSName),'TR_Format') ;
        
        PC_Name = get_PC_Name();
        if(~isempty(PC_Name))
            xls_move_to_folder(OutXLSName) ;
        end
        
        
    else
        OutErrorMsg = {['No Data in ',ViewName]};
    end
    
catch ME
    OutErrorMsg = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end