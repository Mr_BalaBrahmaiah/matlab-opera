function varargout = GrainSmart_2_Opera_GUI(varargin)
% GRAINSMART_2_OPERA_GUI MATLAB code for GrainSmart_2_Opera_GUI.fig
%      GRAINSMART_2_OPERA_GUI, by itself, creates a new GRAINSMART_2_OPERA_GUI or raises the existing
%      singleton*.
%k
%      H = GRAINSMART_2_OPERA_GUI returns the handle to a new GRAINSMART_2_OPERA_GUI or the handle to
%      the existing singleton*.
%
%      GRAINSMART_2_OPERA_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRAINSMART_2_OPERA_GUI.M with the given input arguments.
%
%      GRAINSMART_2_OPERA_GUI('Property','Value',...) creates a new GRAINSMART_2_OPERA_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GrainSmart_2_Opera_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GrainSmart_2_Opera_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GrainSmart_2_Opera_GUI

% Last Modified by GUIDE v2.5 30-Jan-2018 16:26:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GrainSmart_2_Opera_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @GrainSmart_2_Opera_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GrainSmart_2_Opera_GUI is made visible.
function GrainSmart_2_Opera_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GrainSmart_2_Opera_GUI (see VARARGIN)

% Choose default command line output for GrainSmart_2_Opera_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GrainSmart_2_Opera_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

ObjDB_MS_SQL = connect_to_database_MS_SQL;
if(isempty(ObjDB_MS_SQL.Message))
    set(handles.text4,'String','Microsoft SQL Database is Connected');
else
    set(handles.text4,'String','Microsoft SQL Database is not Connected');
end


% --- Outputs from this function are returned to the command line.
function varargout = GrainSmart_2_Opera_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;
try
    GrainSmart_2_Opera() ;
    
    msgbox('Process Finished','Finish');
    
    Job_Done(jObj);
catch ME
    OutErrorMsg = {ME.message};
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

try
    
    GrainSmart_2_Opera_Recon() ;
    
    msgbox('Process Finished','Finish');
    
    Job_Done(jObj);
catch ME
    OutErrorMsg = {ME.message};
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

try
    ObjDB_MS_SQL = connect_to_database_MS_SQL;
    
    ViewName = 'SSUSA_PRD_MASTER_V' ;
    [View_FieldNames,View_DBData] = Fetch_DB_Data(ObjDB_MS_SQL,[],ViewName);
    
    if(size(View_DBData,2) > 1)
        OutXLSName = getXLSFilename('SSUSA_PRD_MASTER_V');
        xlswrite(OutXLSName,[View_FieldNames;View_DBData]);
    else
        warndlg('Not Getting Data from SSUSA_PRD_MASTER_V view in GrainSmart')
    end
    
    msgbox('Process Finished','Finish');
    
    Job_Done(jObj);
catch ME
    OutErrorMsg = {ME.message};
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    
end
