function [TR_Header,ColNames,DBData,OutTRFormat,Empty_Instrument_TRFormat,Empty_Instrument_DBData] = GrainSmart_2_Opera_TR_Format(GrainSmart_Data)

OutErrorMsg = {'No Errors'};
OutXLSName = getXLSFilename('MS_SQL_TR_Format');

DBData = '';
OutTRFormat = '';
Empty_Instrument_TRFormat = '';
Empty_Instrument_DBData = '';


TR_Header = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
    'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date',...
    'Parent Transaction Number','Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG',...
    'Trader Name','Traded DateTimestamp','OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'} ;

ColNames ='';

Instrument_Not_Available = [];
PriceConv_Not_Available = [];

%%
try
    %% Fetch SQL Data
    
    ObjDB = connect_to_database;
    
    Settle_Date =  {'2017-03-26'} ;%%fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    SqlQuery_1 = 'select * from grainsmart_price_conversion_table' ;
    [PriceConversion_ColNames,PriceConversion_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    PriceConversion_Tbl = cell2dataset([PriceConversion_ColNames;PriceConversion_Data]);
    
    SqlQuery_2 = 'select * from subportfolio_product_mapping_table_grn where subportfolio = ''GR-GRAIN-SMART'' ';
    [ProdCodeTbl_ColNames,ProdCodeTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_2);
    ProdCode_Mapping_Tbl = cell2dataset([ProdCodeTbl_ColNames;ProdCodeTbl_Data]);
    
    SqlQuery_3 = ['select * from vol_id_table where expiry_date >= ''',char(Settle_Date),''''];
    [VolID_ColNames,VolID_Data] = Fetch_DB_Data(ObjDB,SqlQuery_3);
    VolID_Tbl = cell2dataset([VolID_ColNames;VolID_Data]);
    
    SqlQuery_4 = 'select * from grainsmart_price_conversion_table';
    [GrainSmart_Opera_ColNames,GrainSmart_Opera_Data] = Fetch_DB_Data(ObjDB,SqlQuery_4);
    GrainSmart_Opera_ProdCode_Mapping = cell2dataset([GrainSmart_Opera_ColNames;GrainSmart_Opera_Data]);
    
    %% Fetch MS SQL Data
    if(exist('GrainSmart_Data','var'))
        DBData = GrainSmart_Data;
    else
        ObjDB_MS_SQL = connect_to_database_MS_SQL;
        
        SqlQuery = 'select bot_commodity_symbol,commodity_id,board_of_trade,acct_nbr,buy_sell,EX_CODE_MONTH,strike_price,put_call,price,trading_broker_id,abs(LOTS) as LOTS,ContractNumber,Maturity,BarrierType,BarrierLevel,trade_date,Source_system,name,trade_blotter_nbr,add_Date,TRD_FLG from opera_derivativesnew_v ';
        ViewName = 'opera_derivativesnew_v' ;  %% opera_derivatives_v
        [ColNames,DBData] = Fetch_DB_Data(ObjDB_MS_SQL,SqlQuery); %% ViewName
        
        if(isempty(DBData) || strcmpi(DBData,'No Data'))
            warndlg('No Data in GrainSmart DB','Check') ;
            return;
        end
        
        DBData = cell2dataset([ColNames ;DBData]) ;
    end
    
    if(size(DBData,2) > 1)
        
        %% Mapping GrainSmart Prodcode vs Opera ProdCode
        GrainSmartUnique_PC = unique(DBData.bot_commodity_symbol);
        OperaUnique_PC = unique(GrainSmart_Opera_ProdCode_Mapping.grainsmart_product_code);
        
        Mismatched_PC = setdiff(GrainSmartUnique_PC ,OperaUnique_PC);
        if(~isempty(Mismatched_PC))
            OutErrorMsg = cellstr(['GrainSmart ProductCode not Mapped in Opera : ',{' '},' Refer grainsmart_price_conversion_table ',char(unique(Mismatched_PC))]);
            errordlg(['GrainSmart ProductCode not Mapped in Opera : ',{' '},' Refer grainsmart_price_conversion_table ',char(unique(Mismatched_PC))],'GrainSmart Opera Product Mappin Error');
        else
            for k = 1 : size(GrainSmart_Opera_ProdCode_Mapping,1)
                Current_GrainSmart_ProdCode  = GrainSmart_Opera_ProdCode_Mapping.grainsmart_product_code(k);
                Current_Opera_ProdCode = GrainSmart_Opera_ProdCode_Mapping.opera_product_code(k);
                
                GrainSmart_PC_Index = strcmpi(DBData.bot_commodity_symbol,Current_GrainSmart_ProdCode) ;
                if(~isempty( find(GrainSmart_PC_Index, 1)) )
                    DBData.bot_commodity_symbol(GrainSmart_PC_Index) = Current_Opera_ProdCode ;
                    
                end
            end
        end
        
        %% Make TR Format
        
        OutTRFormat = cell2dataset([TR_Header ; cell(size(DBData,1),size(TR_Header,2)) ]);
        for i = 1 : size(DBData,1)
            if(length(DBData.bot_commodity_symbol{i})==1)
                Current_ProdCode = strcat('CM',{' '},DBData.bot_commodity_symbol(i),{'  '} );
                Current_BBGOpt_Ticker = strcat(DBData.bot_commodity_symbol(i),{' '},DBData.EX_CODE_MONTH(i) ) ;
            else
                Current_ProdCode = strcat('CM',{' '},DBData.bot_commodity_symbol(i),{' '} );
                Current_BBGOpt_Ticker = strcat(DBData.bot_commodity_symbol(i),DBData.EX_CODE_MONTH(i) ) ;
            end
            
            Current_UniqueInstrument = unique(ProdCode_Mapping_Tbl.instrument(strcmpi(ProdCode_Mapping_Tbl.product_code,Current_ProdCode)));
            if(~isempty(Current_UniqueInstrument))
                if(strcmpi(DBData.put_call(i),'F'))
                    OutTRFormat.Instrument(i) = Current_UniqueInstrument;
                else
                    OutTRFormat.Instrument(i) = strcat(Current_UniqueInstrument,'-O');
                end
            else
                Instrument_Not_Available = [Instrument_Not_Available ; Current_ProdCode] ;
                
                %OutErrorMsg = cellstr(['Instrument is not availabe in Corresponding Product Code : ',char(Current_ProdCode)]);
                %errordlg(['Instrument is not availabe in Corresponding Product Code : ',{' '},char(Current_ProdCode)]);
                %return;
            end
            
            OutTRFormat.Portfolio(i) = {'GR-GRAIN-SMART'} ; %% Hard Code ;
            
            if(strcmpi(DBData.buy_sell(i),'B'))
                OutTRFormat.BuySell(i) = {'Bought'} ;
                OutTRFormat.InitialLots(i) = num2cell(DBData.LOTS(i) * 1) ;
            else
                OutTRFormat.BuySell(i) = {'Sold'} ;
                OutTRFormat.InitialLots(i) = num2cell(DBData.LOTS(i) * -1) ;
            end
            
            if(strcmpi(DBData.put_call(i),'C') || strcmpi(DBData.put_call(i),'P') )
                if(strcmpi(DBData.put_call(i),'C'))
                    OutTRFormat.OptType(i) = {'Vanilla_Call'};
                else
                    OutTRFormat.OptType(i) = {'Vanilla_Put'};
                end
                Matched_Index_VolID = strcmpi(VolID_Tbl.bbg_opt_ticker,Current_BBGOpt_Ticker);
                Current_VolID = strsplit( char(VolID_Tbl.vol_id(Matched_Index_VolID)),' ') ;
                OutTRFormat.Month(i) = Current_VolID(end);
                
            else
                OutTRFormat.OptType(i) = {'Future'};
                OutTRFormat.Month(i) = strcat(DBData.EX_CODE_MONTH(i),{'.'},DBData.EX_CODE_MONTH(i)) ;
            end
            
            Matched_Price_Coneversion = find(strcmpi(PriceConversion_Tbl.product_code,strtrim(Current_ProdCode)));
            if(~isempty(Matched_Price_Coneversion))
                Current_PriceConv = PriceConversion_Tbl.price_conversion(Matched_Price_Coneversion);
                OutTRFormat.Strike(i) = num2cell( DBData.strike_price(i).* Current_PriceConv ) ;
                OutTRFormat.Prem(i) = num2cell( DBData.price(i).* Current_PriceConv );
            else
                PriceConv_Not_Available = [PriceConv_Not_Available ; Current_ProdCode] ;
                
                %OutErrorMsg = cellstr(['Price Conversion is not availabe in Corresponding Product Code : ',char(Current_ProdCode)]);
                %                 errordlg(['Price Conversion is not availabe in Corresponding Product Code : ',{' '},char(Current_ProdCode)]);
                %                 return;
            end
            
            OutTRFormat.CounterParty(i) = DBData.trading_broker_id(i);
            OutTRFormat.TransactionDate(i) = DBData.trade_date(i);
            OutTRFormat.Source(i) = DBData.Source_system(i);
            OutTRFormat.Exe_Broker(i) = DBData.trading_broker_id(i);
            
            OutTRFormat.ExeType(i) = {'Elec'}; %% Hard Code ;
            OutTRFormat.Rates(i) = {'Normal'};%% Hard Code ;
            
            OutTRFormat.TraderName(i) = DBData.name(i);
            OutTRFormat.TradedDateTimestamp(i) = DBData.add_Date(i);
            OutTRFormat.OPSUnique(i) = DBData.trade_blotter_nbr(i);
            OutTRFormat.BROKERLOTS(i) = num2cell(DBData.LOTS(i));
            OutTRFormat.OPSAction(i) = DBData.TRD_FLG(i);
            
            OutTRFormat.CounterpartyEntity(i) = {'GRAIN SMART'} ; %% Hard Code ;
            
            
        end
        
        %% Check Empty Instrument and Remove
        
        Instrument_Empty_Index = cellfun(@(s) (isempty(s)), OutTRFormat.Instrument) ;
        Empty_Instrument_TRFormat = OutTRFormat(Instrument_Empty_Index,:);
        Empty_Instrument_DBData = DBData(Instrument_Empty_Index,:);
        
        OutTRFormat(Instrument_Empty_Index,:) = [];
        DBData(Instrument_Empty_Index,:) = [];
        
        %%
        if(~isempty(Instrument_Not_Available))
            OutErrorMsg = cellstr(['Instrument is not availabe in Corresponding Product Code : ',char(unique(Instrument_Not_Available))]);
            errordlg(['Instrument is not availabe in Corresponding Product Code : ',{' '},char(unique(Instrument_Not_Available))],'Instrument Error');
        end
        
        if(~isempty(PriceConv_Not_Available))
            OutErrorMsg = cellstr(['Price Conversion is not availabe in Corresponding Product Code : ',char(unique(PriceConv_Not_Available))]);
            errordlg(['Price Conversion is not availabe in Corresponding Product Code : ',{' '},char(unique(Instrument_Not_Available))],'PriceConv Error');
        end
        
        %% Excel Sheet Write
        
        %         OutTRFormat = dataset2cell(OutTRFormat) ;
        %
        %         xlswrite(OutXLSName,[TR_Header ; OutTRFormat(2:end,:)],'TR_Format') ;
        %         xlswrite(OutXLSName,dataset2cell(DBData),'Original Data');
        %
        %         if(sum(Instrument_Empty_Index) > 0)
        %             xlswrite(OutXLSName,dataset2cell(Empty_Instrument_TRFormat),'Empty_Instrument_TR_Format');
        %             xlswrite(OutXLSName,dataset2cell(Empty_Instrument_DBData),'Empty_Instrument_Original');
        %         end
        %
        %
        %         xls_delete_sheets(fullfile(pwd,OutXLSName));
        %         xls_change_activesheet(fullfile(pwd,OutXLSName),'TR_Format') ;
        %
        %         PC_Name = get_PC_Name();
        %         if(~isempty(PC_Name))
        %             xls_move_to_folder(OutXLSName) ;
        %         end
        
        
    else
        OutErrorMsg = {['No Data in ',ViewName]};
    end
    
catch ME
    OutErrorMsg = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end