function GrainSmart_2_Opera_Recon()

try
    %% Fetch MS SQL Data
    ObjDB_MS_SQL = connect_to_database_MS_SQL;
    
    ViewName = 'opera_derivativesnew_v' ;  %% opera_derivatives_v
    SqlQuery = 'select bot_commodity_symbol,commodity_id,board_of_trade,acct_nbr,buy_sell,EX_CODE_MONTH,strike_price,put_call,price,trading_broker_id,abs(LOTS) as LOTS,ContractNumber,Maturity,BarrierType,BarrierLevel,trade_date,Source_system,name,trade_blotter_nbr,add_Date,TRD_FLG from opera_derivativesnew_v ';
    
    [ColNames,DBData] = Fetch_DB_Data(ObjDB_MS_SQL,SqlQuery); %% ViewName
    
    GrainSmart_DBData = cell2dataset([ColNames ;DBData]) ;
    
    GrainSmart_NotExisting_Opera = [];
    Opera_NotExisting_GrainSmart = [];
    Overall_Data = [];
    
    if(size(DBData,2)>1)
        %% Fetch Opera Data
        ObjDB = connect_to_database ;
        
        SqlQuery = 'select ops_unique,abs(original_lots) as original_lots from deal_ticket_table_with_latest_versions_view_grn where counterparty_entity = ''GRAIN SMART'' ';
        [Opera_ColNames,Opera_DBData ] = Fetch_DB_Data(ObjDB,SqlQuery) ;
        Opera_DealTicTbl = cell2dataset([Opera_ColNames;Opera_DBData]);
        if(size(Opera_DBData,2)>1)
            
            %% Find MisMatch Unique Numbers
            GrainSmart_NotExisting_Opera = setdiff(GrainSmart_DBData.trade_blotter_nbr,Opera_DealTicTbl.ops_unique);
            Opera_NotExisting_GrainSmart = setdiff(Opera_DealTicTbl.ops_unique,GrainSmart_DBData.trade_blotter_nbr);
            
            GrainSmart_NotExisting_Opera_Index = cellStrfind_exact(GrainSmart_DBData.trade_blotter_nbr,GrainSmart_NotExisting_Opera);
            Opera_NotExisting_GrainSmart_Index = cellStrfind_exact(Opera_DealTicTbl.ops_unique,Opera_NotExisting_GrainSmart);
            
            GS_NotExist_Opera_Data = GrainSmart_DBData(GrainSmart_NotExisting_Opera_Index,:) ;
            Opera_NotExist_GS_Data = Opera_DealTicTbl(Opera_NotExisting_GrainSmart_Index,:) ;
            
            [~,~,~,GS_NotExist_Opera_TR_Format,Empty_Instrument_TRFormat,Empty_Instrument_DBData] = GrainSmart_2_Opera_TR_Format(GS_NotExist_Opera_Data);
            
            GrainSmart_DBData(GrainSmart_NotExisting_Opera_Index,:) = [] ;
            Opera_DealTicTbl(Opera_NotExisting_GrainSmart_Index,:)  =[];
            
            %% Sort Rows
            GrainSmart_DBData = sortrows(GrainSmart_DBData,'trade_blotter_nbr','ascend');
            Opera_DealTicTbl = sortrows(Opera_DealTicTbl,'ops_unique','ascend');
            
            GS_UserWant_Col = cellStrfind_exact(ColNames,{'trade_blotter_nbr','LOTS'});
            GS_UserWant_Data = GrainSmart_DBData(:,GS_UserWant_Col);
            
            Opera_UserWant_Col = cellStrfind_exact(Opera_ColNames,{'ops_unique','original_lots'});
            Opera_UserWant_Data = Opera_DealTicTbl(:,Opera_UserWant_Col);
            
            Temp_CellArray = cell(size(GS_UserWant_Data,1),1);
            
            %% Subtraction
            
            Recon_OPS_Unique_Str = cell(size(GS_UserWant_Data,1),1);
            
            Recon_OPS_Unique = strcmpi(GS_UserWant_Data.trade_blotter_nbr,Opera_UserWant_Data.ops_unique);
            Recon_OPS_Unique_Str(Recon_OPS_Unique,:) = {'TRUE'} ;
            Recon_OPS_Unique_Str(~Recon_OPS_Unique,:) = {'FALSE'} ;
            
            Recon_Lots_Diff = num2cell( GS_UserWant_Data.LOTS - Opera_UserWant_Data.original_lots ) ;
            
            Recon_Data = [Recon_OPS_Unique_Str , Recon_Lots_Diff] ;
            
            %%
            Overall_Data = [GS_UserWant_Data.trade_blotter_nbr,num2cell(GS_UserWant_Data.LOTS),Temp_CellArray,...
                Opera_UserWant_Data.ops_unique,num2cell(Opera_UserWant_Data.original_lots),Temp_CellArray,Recon_Data];
            
            Overall_Header = {'Grain Smart','','','Opera Grain Smart','','','Recon','';...
                'trade_blotter_nbr','LOTS','','ops_unique','original_lots','','ops_unique','original_lots'};
            
            %% Excel_Write
            
            OutXLSName = getXLSFilename('GrainSmart_2_Opera_Recon') ;
            
            if(~isempty(Overall_Data))
                xlswrite(OutXLSName,[Overall_Header ; Overall_Data],'GrainSmart_2_Opera');
            end
            
            if(~isempty(GrainSmart_NotExisting_Opera))
                xlswrite(OutXLSName,GrainSmart_NotExisting_Opera,'GS_NotExisting_Opera');
                xlswrite(OutXLSName,dataset2cell(GS_NotExist_Opera_Data),'GS_NotExist_Opera_Data');
                xlswrite(OutXLSName,dataset2cell(GS_NotExist_Opera_TR_Format),'GS_NotExist_Opera_TR_Format');
                
            end
            
            if(~isempty(Opera_NotExisting_GrainSmart))
                xlswrite(OutXLSName,Opera_NotExisting_GrainSmart,'Opera_NotExisting_GS');
                xlswrite(OutXLSName,dataset2cell(Opera_NotExist_GS_Data),'Opera_NotExist_GS_Data');
                
            end
            
            xls_delete_sheets(fullfile(pwd,OutXLSName));
            xls_change_activesheet(fullfile(pwd,OutXLSName),'GrainSmart_2_Opera') ;
            
        else
            warndlg('No Data in deal_ticket_table_with_latest_versions_view_grn Opera DB','Check DB') ;
        end
    else
        
        warndlg('No Data in GrainSmart DB','Check DB') ;
    end
catch ME
    OutErrorMsg = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
    uiwait(warndlg(errorMessage));
end
