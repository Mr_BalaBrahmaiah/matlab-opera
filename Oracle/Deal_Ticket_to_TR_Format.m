function [TRHeader,RowData] = Deal_Ticket_to_TR_Format(DBFieldNames,DBData,Commodity)

%Example

% SqlQuery = 'SELECT * FROM deal_ticket_table_with_latest_versions_view_cot where trade_group_type = ''OFUT'' ';
% [DBFieldNames,DBData] = read_from_database(0,0,SqlQuery);
%
% Commodity = 'OFUT';

%%

if(size(DBData,1) > 1)
    Instrument_Col = find(cellfun(@(V) strcmpi('instrument',V), DBFieldNames)) ;
    Portfolio_Col = find(cellfun(@(V) strcmpi('subportfolio',V), DBFieldNames)) ;
    BuySell_Col = find(cellfun(@(V) strcmpi('market_action',V), DBFieldNames)) ;
    Month_Strike_OptType_Col = find(cellfun(@(V) strcmpi('security_id',V), DBFieldNames)) ;
    
    Premium_Col = find(cellfun(@(V) strcmpi('premium',V), DBFieldNames)) ;
    Counterparty_Col = find(cellfun(@(V) strcmpi('counterparty_parent',V), DBFieldNames)) ;
    InitialLots_Col = find(cellfun(@(V) strcmpi('original_lots',V), DBFieldNames)) ;
    TransactionDate_Col = find(cellfun(@(V) strcmpi('transaction_date',V), DBFieldNames)) ;
    
    TRN_Number_Col =  find(cellfun(@(V) strcmpi('trade_id',V), DBFieldNames)) ;
    ParentTRN_Number_Col =  find(cellfun(@(V) strcmpi('parent_trade_id',V), DBFieldNames)) ;
    Exe_Broker_Col = find(cellfun(@(V) strcmpi('execution_broker',V), DBFieldNames)) ;
    Exe_Type_Col = find(cellfun(@(V) strcmpi('exe_type',V), DBFieldNames)) ;
    Rate_Col = find(cellfun(@(V) strcmpi('brokerage_rate_type',V), DBFieldNames)) ;
    SpecialRate_Col = find(cellfun(@(V) strcmpi('spcl_rate',V), DBFieldNames)) ;
    
    
    OpsUnique_Col = find(cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames)) ;
    BrokerLots_Col = find(cellfun(@(V) strcmpi('broker_lots',V), DBFieldNames)) ;
    
    
    %%
    
    Instrument_Data = DBData(:,Instrument_Col);
    Portfolio_Data = DBData(:,Portfolio_Col);
    BuySell_Data = DBData(:,BuySell_Col);
    
    Month_Strike_OptType_Data = DBData(:,Month_Strike_OptType_Col);
    [~,Maturity_Code,Strike_Data,OptType_Data]= Get_ProductCode_Strike_MonthCode_CallPut_from_SecurityID(Month_Strike_OptType_Data,Commodity);
    
    Premium_Data = DBData(:,Premium_Col);
    CounterParty_Data = DBData(:,Counterparty_Col);
    InitialLots_Data = DBData(:,InitialLots_Col);
    TransactionDate_Data = DBData(:,TransactionDate_Col);
    
    TRN_NumberData = DBData(:,TRN_Number_Col);
    ParentTRN_NumberData = DBData(:,ParentTRN_Number_Col);
    ExeBroker_Data = DBData(:,Exe_Broker_Col);
    ExeType_Data = DBData(:,Exe_Type_Col);
    Rate_Data = DBData(:,Rate_Col);
    SpecialRate_Data = DBData(:,SpecialRate_Col);
    
    OpsUnique_Data = DBData(:,OpsUnique_Col);
    BrokerLots_Data = DBData(:,BrokerLots_Col);
    
    %%
    Source = cell(size(DBData ,1) ,1) ;
    Source(:,:) = {'OPS'};
    
    Temp_Cell_Array = cell(size(DBData ,1) ,1);
    
    %% Make Data
    
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
        'OptType','Prem','CounterParty','Initial Lots','CONTRACTNB','Maturity','Barrier Type',...
        'Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS'};
    
    RowData = [Instrument_Data,Portfolio_Data,BuySell_Data,Maturity_Code,Strike_Data,...
        OptType_Data,Premium_Data,CounterParty_Data,InitialLots_Data,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,...
        Temp_Cell_Array,TransactionDate_Data,TRN_NumberData,Source,Temp_Cell_Array,Temp_Cell_Array,ParentTRN_NumberData,...
        ExeBroker_Data,ExeType_Data,Rate_Data,SpecialRate_Data,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,...
        OpsUnique_Data,BrokerLots_Data];
    
    
else
    
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
        'OptType','Prem','CounterParty','Initial Lots','CONTRACTNB','Maturity','Barrier Type',...
        'Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS'};
    
    RowData = cell(1,size(TRHeader,2));
end

