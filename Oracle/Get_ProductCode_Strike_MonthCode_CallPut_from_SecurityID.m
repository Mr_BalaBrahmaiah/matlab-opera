function [ProductCode,Maturity_Code,Strike,CallPut_Data] = Get_ProductCode_Strike_MonthCode_CallPut_from_SecurityID(InputData_CellArray,Commodity)

Split_Str = cellfun(@(s) strsplit(s,' '), InputData_CellArray, 'UniformOutput', false);

count = 1;
for i = 1 : 1 : size(Split_Str,1)
    
    ProductCode{count,1} =  Split_Str{i,1}{1,2} ;
    
    MonthCode{count,1} = Split_Str{i,1}{1,3};
    
    if(strcmpi(Commodity,'FUT'))
        Strike{count,1} = '';
    else
        Strike{count,1} = str2double(Split_Str{i,1}{1,4}) ;
    end
    
    count = count + 1;
    
end

%%
if(strcmpi(Commodity,'FUT'))
    Maturity_Code = strcat(MonthCode,{'.'},MonthCode);
    
    CallPut_Data =   cell(size(Maturity_Code,1),1);
    CallPut_Data(:,:) = {'future'} ;
else
    
    CallPut_Data = cellfun(@(x) x(end), MonthCode, 'UniformOutput', false) ;
    Call_Index = cellStrfind_exact(CallPut_Data,{'C'});
    Put_Index = cellStrfind_exact(CallPut_Data,{'P'});
    CallPut_Data(Call_Index,1) = {'vanilla_call'};
    CallPut_Data(Put_Index,1) = {'vanilla_put'};
    
    Maturity_Code = cellfun(@(x) x(1:end-1), MonthCode, 'UniformOutput', false) ;
end


