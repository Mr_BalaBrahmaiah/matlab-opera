clc

OPS_BUName = 'PO'; %% MP - DRY
InBUName = 'plm';

OutXLSName = getXLSFilename([OPS_BUName,'_OPS_2_Opera_TR_Format']);

ObjDB_Oracle = connect_to_database_Oracle;

Expiry_Date = '01-01-2018' ; %% EXPIRY_DT %% deal_dt

%% OPS Queries

FUT_Tbl_Name = {'OP_FUT_DUMP'};
OPS_FUT_SqlQuery = ['select * from ', char(FUT_Tbl_Name), ' where pr_Code = ''',OPS_BUName,''' and trunc(EXPIRY_DT) >= ''',Expiry_Date,''' '];

OPT_Tbl_Name = {'OP_OPT_DUMP'};
OPS_OPT_SqlQuery = ['select * from ', char(OPT_Tbl_Name), ' where pr_Code = ''',OPS_BUName,''' and trunc(EXPIRY_DT) >= ''',Expiry_Date,'''  '];


%%
try
    %% FUTURE
    
    
    %     OPS_FUT_SqlQuery = ['select * from ', char(FUT_Tbl_Name), ' where pr_Code = ''',OPS_BUName,''' and trunc(EXPIRY_DT) >= ''',Expiry_Date,''' and ex_code not in (''MTS'')'];
    
    [FUT_ColNames,FUT_DBData] = Fetch_DB_Data(ObjDB_Oracle,OPS_FUT_SqlQuery);
    
    Ops_Action_Str = 'NEW';
    [TRHeader , FUT_RowData ,Instrument_Error_Data ,Maturity_Error_Data] = OPS_Future_2_TR_Format(FUT_ColNames , FUT_DBData , Ops_Action_Str,InBUName);
    
    % [TRHeader,FUT_RowData] = Convert_OPS_FUT_2_TR_Format(FUT_ColNames,FUT_DBData,InBUName);
    
    %% OPTIONS
    
    %     OPS_OPT_SqlQuery = ['select * from ', char(OPT_Tbl_Name), ' where pr_Code = ''',OPS_BUName,''' and trunc(EXPIRY_DT) >= ''',Expiry_Date,''' and ex_code not in (''MTS'') '];
    
    [OPT_ColNames,OPT_DBData] = Fetch_DB_Data(ObjDB_Oracle,OPS_OPT_SqlQuery);
    
    Ops_Action_Str = 'NEW';
    [TRHeader , OPT_RowData ,Instrument_Error_Data ,Maturity_Error_Data] = OPS_Option_2_TR_Format(OPT_ColNames , OPT_DBData , Ops_Action_Str,InBUName);
    
    % [TRHeader,OPT_RowData] = Convert_OPS_OPT_2_TR_Format(OPT_ColNames,OPT_DBData,InBUName);
    
    %%
    
    OverallData = [FUT_RowData ; OPT_RowData ] ;
    
    xlswrite(OutXLSName,[TRHeader ; OverallData] ,'OPS_2_OPERA_TR_Format') ;
    xlswrite(OutXLSName,[FUT_ColNames ; FUT_DBData],'OPS_FUT_Dump');
    xlswrite(OutXLSName,[OPT_ColNames ; OPT_DBData],'OPS_OPT_Dump');
    
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end


