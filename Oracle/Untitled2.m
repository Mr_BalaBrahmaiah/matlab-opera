figure;
iteratormin = 1;
iteratormax = 100;
myslider = uicontrol('style','slider','units','normalized','position',[0.75 0.05 0.19 0.04],'min',iteratormin,'max',iteratormax,'value',iteratormin);
for ii = iteratormin:iteratormax
    %Some code here
    pause(0.1);
    set(myslider,'value',ii)
end