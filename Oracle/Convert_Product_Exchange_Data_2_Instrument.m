function [Instrument_Data , New_Instrument , Price_Conversion ,New_Instrument_Index] = Convert_Product_Exchange_Data_2_Instrument(Product_Exch_Data)

%% Read Mapping Sheet
% [~,~,Mapping_Data ] = xlsread('CB_Instrument_Mapping.xlsx','Instrument_Mapping') ;  %% Working_Cotton
% Mapping_Header = Mapping_Data(1,:);
% Mapping_Data = Mapping_Data(2:end,:);

ObjDB = connect_to_database ;
[Mapping_Header,Mapping_Data] = Fetch_DB_Data(ObjDB,[],'cb_instrument_mapping_table');

Map_Product_Exch_Data = Mapping_Data(:,1);
Map_Instrument_Data =  Mapping_Data(:,2);
Map_Price_Conversion = Mapping_Data(:,3);

Map_Price_Conversion(cellfun(@(x) any(isnan(x)),Map_Price_Conversion)) = {1} ;  %% For Multiplication Purpose

%% Find New Instrument 

New_Instrument = setdiff(Product_Exch_Data,Map_Product_Exch_Data) ;

if(~isempty(New_Instrument))
   New_Instrument_Index = cellStrfind_exact(Product_Exch_Data,New_Instrument) ;
   
else
    New_Instrument_Index = '';
    
end

%%
Instrument_Data = cell(size(Product_Exch_Data ,1) ,1) ;
Price_Conversion = cell(size(Product_Exch_Data ,1) ,1) ;

for i = 1 : size(Map_Product_Exch_Data)
    Current_Product_Exch = Map_Product_Exch_Data(i) ;
    Current_Instrment = Map_Instrument_Data(i) ;
    Current_Price = Map_Price_Conversion(i) ;
    
    Matched_Product_Index = cellStrfind_exact(Product_Exch_Data , Current_Product_Exch) ;
    
    if(~isempty(Matched_Product_Index))
        Instrument_Data(Matched_Product_Index,1) = Current_Instrment ;
        
        Price_Conversion(Matched_Product_Index,1) = Current_Price ;
    end
    
end


