function [TRHeader , RowData , Instrument_Error_Data , Maturity_Error_Data] = OPS_Option_2_TR_Format(DBFieldNames , DBData ,Ops_Action_Str ,InBUName)

Commodity = 'OFUT';

TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
    'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
    'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};

%     RowData = cell(1,size(TRHeader,2));
RowData = [];

Instrument_Error_Data ='';
Maturity_Error_Data = '';

%%

if(size(DBData,2) > 1)
    
    %%
    Pr_Code_Col = find(cellfun(@(V) strcmpi('pr_code',V), DBFieldNames)) ;
    Ex_Code_Col = find(cellfun(@(V) strcmpi('EX_CODE',V), DBFieldNames)) ;
    Prcat_Sd_Col = find(cellfun(@(V) strcmpi('prcat_sd',V), DBFieldNames)) ;
    Unit_Sd_Col = find(cellfun(@(V) strcmpi('unit_sd',V), DBFieldNames)) ;
    LotType_Col = find(cellfun(@(V) strcmpi('LOT_TYPE',V), DBFieldNames)) ;
    Month_Col = find(cellfun(@(V) strcmpi('TER_MONTH',V), DBFieldNames)) ;
    CounterParty_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
    TransactionDate_Col = find(cellfun(@(V) strcmpi('DEAL_DT',V), DBFieldNames)) ;
    ExeBroker_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
    Prem_Col = find(cellfun(@(V) strcmpi('RATE',V), DBFieldNames)) ;
    InitialLot_Col = find(cellfun(@(V) strcmpi('LOT',V), DBFieldNames)) ;
    Strike_Col = find(cellfun(@(V) strcmpi('STRIKE',V), DBFieldNames)) ;
    OptType_Col = find(cellfun(@(V) strcmpi('STATUS',V), DBFieldNames)) ;
    
    UniqueNumber_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), DBFieldNames)) ;
    LYBROK_Col = find(cellfun(@(V) strcmpi('LYBROK_ADJ',V), DBFieldNames)) ;
    
    BBR_Code_Col = find(cellfun(@(V) strcmpi('BBR_CODE',V), DBFieldNames)) ;
    
    COMPANY_SD_Col = find(cellfun(@(V) strcmpi('COMPANY_SD',V), DBFieldNames));
    
    UPD_DATE_Col = find(cellfun(@(V) strcmpi('UPD_DATE',V), DBFieldNames));
    
    REMARK_Col = find(cellfun(@(V) strcmpi('REMARK',V), DBFieldNames));
    
    %%  Organize the Data  %% Read Mapping Sheet and Get Instrument
    
    Product_Exch_Data = strcat(DBData(:,Pr_Code_Col), {'-'} , DBData(:,Ex_Code_Col) , {'-'} , DBData(:,Prcat_Sd_Col)) ;
    
    [Instrument_Data , New_Instrument ,Price_Conversion ,New_Instrument_Index] = Convert_Product_Exchange_Data_2_Instrument(Product_Exch_Data) ; %% Read Mapping SHeet
    
    if(~isempty(New_Instrument))
        
        warndlg([strcat('OPS_',Commodity,'_',Ops_Action_Str),' View Contain the Instrument ',New_Instrument',' which is not available in Mapping Master file. Please update this Instrument in master file(Instrument Mapping Sheet) to get these Instrument deals'],'OPS_2_OPERA');
        
        Instrument_Error_Data = DBData(New_Instrument_Index,:);
        %         Instrument_Error_Data =  [DBFieldNames ; Instrument_Error_Data];
        
        DBData(New_Instrument_Index,:) = [];
        Instrument_Data(New_Instrument_Index,:) = [];
        Price_Conversion(New_Instrument_Index,:) = [];
        
    else
        Instrument_Error_Data = '';
    end
    
    if(~isempty(DBData))
        
        Instrument_Data = strcat(Instrument_Data , {'-O'} );
        
        Portfolio = strcat(DBData(:,Pr_Code_Col) , {'-'}, DBData(:,Unit_Sd_Col));
        
        Buy_Sell = DBData(:,LotType_Col) ;
        Bounght_Index = cellStrfind_exact(Buy_Sell,{'Bot'}) ;
        Sold_Index = cellStrfind_exact(Buy_Sell,{'Sold'}) ;
        Buy_Sell(Bounght_Index)  = {'Bought'} ;
        Buy_Sell(Sold_Index)  = {'Sold'} ;
        
        Month_DB =  DBData(:,Month_Col) ;
        BBR_Code_Data = DBData(:,BBR_Code_Col);
        [Month_Maturity , Maturity_Code , Maturity_Error_Data] = Convert_Month_2_Maturity_Format(Month_DB,BBR_Code_Data,Commodity) ;
        
        %         Negative_StrikeIndex = cellfun(@(s) (s<10), num2cell(cell2mat(DBData(:,Strike_Col))));
        Strike_Data = num2cell(cell2mat(DBData(:,Strike_Col)) .* cell2mat(Price_Conversion));
        %         Negative_StrikeIndex = cellfun(@(s) (s<100), Strike_Data); %% Before
        Negative_StrikeIndex = cellfun(@(s) (s<100), Strike_Data) & cellfun(@(s) ~isempty(strfind(s,'CSO')),DBData(:,REMARK_Col)) ; % Here We should check CSO string becoz code is not using  looping
        
        OptType_Data = DBData(:,OptType_Col);
        Call_Index = cellStrfind_exact(OptType_Data,{'C'}) ;
        Put_Index = cellStrfind_exact(OptType_Data,{'P'}) ;
        OptType_Data(Call_Index)  = {'vanilla_call'} ;
        OptType_Data(Put_Index)  = {'vanilla_put'} ;
        
        Prem_Data = DBData(:,Prem_Col) ;
        CounterParty_Data = DBData(:,CounterParty_Col);
        InitialLot_Data = DBData(:,InitialLot_Col);
        TransactionDate_Data = DBData(:,TransactionDate_Col);
        ExeBroker_Data = DBData(:,ExeBroker_Col);
        
        UniqueNumber_Data = DBData(:,UniqueNumber_Col);
        BrokerLot_Data = num2cell(cell2mat(DBData(:,InitialLot_Col)) - cell2mat(DBData(:,LYBROK_Col)));
        
        CounterpartyEntity_Data = DBData(:,COMPANY_SD_Col);
        
        Traded_DateTime_Data = DBData(:,UPD_DATE_Col);
        
        %% Hard Code Field Names
        Source = cell(size(DBData ,1) ,1) ;
        ExeType = cell(size(DBData ,1) ,1) ;
        Rates = cell(size(DBData ,1) ,1) ;
        OtherRemarks = cell(size(DBData ,1) ,1) ;
        Month1_Data = cell(size(DBData ,1) ,1) ;
        
        
        Source(:,:) = {'OPS'};
        ExeType(:,:) = {'Elec'} ;
        Rates(:,:) = {'Normal'} ;
        
        %% CSO Month Mapping
        
        if(~isempty(cellStrfind(DBData(Negative_StrikeIndex,REMARK_Col),{'CSO'})))
            
            if(sum(Negative_StrikeIndex) > 0)
                OtherRemarks(Negative_StrikeIndex,1) = DBData(Negative_StrikeIndex,REMARK_Col);
                
                Instrument_Data(Negative_StrikeIndex,1) = strrep(Instrument_Data(Negative_StrikeIndex),'-O','-CSO');
                OptType_Data(Negative_StrikeIndex,1) = strrep(OptType_Data(Negative_StrikeIndex),'vanilla','spread');
                
                if(strcmpi(InBUName,'grn'))
                    Portfolio(Negative_StrikeIndex,1) = strcat(Portfolio(Negative_StrikeIndex),'-CSO');
                end
                
                ObjDB = connect_to_database;
                [CSO_ColNames,CSO_MapData] = Fetch_DB_Data(ObjDB,'select * from cso_mapping_month where spread_month = 1');
                CSO_MapData_SpreadMonth_1 = cell2dataset([CSO_ColNames;CSO_MapData]);
                [CSO_ColNames,CSO_MapData] = Fetch_DB_Data(ObjDB,'select * from cso_mapping_month where spread_month = 2');
                CSO_MapData_SpreadMonth_2 = cell2dataset([CSO_ColNames;CSO_MapData]);
                
                Negative_StrikeIndex_Maturity_Code = Maturity_Code(Negative_StrikeIndex);
                Negative_StrikeIndex_Remark = OtherRemarks(Negative_StrikeIndex);
                Negative_StrikeIndex_BBGCode_Data = strtrim(DBData(Negative_StrikeIndex,BBR_Code_Col));
                
                New_Maturity_Code = cell(size(Negative_StrikeIndex_Maturity_Code,1),1);
                for i = 1 : size(Negative_StrikeIndex_Remark,1)
                    Current_Remark = Negative_StrikeIndex_Remark(i);
                    Current_Maturity = Negative_StrikeIndex_Maturity_Code(i);
                    Current_BBG_Code = Negative_StrikeIndex_BBGCode_Data(i);
                    
                    Spread_Month = cellStrfind(Current_Remark,{'1'});
                    if(~isempty(Spread_Month))
                        Current_Maturity_char = char(Current_Maturity);
                        Replace_Str1_Index = strcmpi(CSO_MapData_SpreadMonth_1.month_1,Current_Maturity_char(1)) & strcmpi(CSO_MapData_SpreadMonth_1.bbg_product_code,char(Current_BBG_Code));
                        %Replace_Year1 = str2double(Current_Maturity_char(2));  %27-09-2019
                        %%% Newly added if-else conditiuon on 14-06-2016
                        %%% (for hadnling Maturity month)
                        if length(Current_Maturity_char) == 5
                            Replace_Year1 = str2double(Current_Maturity_char(2));  %% 27-09-2019
                            Replace_Str2_Index = strcmpi(CSO_MapData_SpreadMonth_1.month_1,Current_Maturity_char(4)) & strcmpi(CSO_MapData_SpreadMonth_1.bbg_product_code,char(Current_BBG_Code));
                            Replace_Year2 = str2double(Current_Maturity_char(5));
                        elseif length(Current_Maturity_char) == 7
                            Replace_Year1 = str2double(Current_Maturity_char(2:3));  %% 27-09-2019
                            Replace_Str2_Index = strcmpi(CSO_MapData_SpreadMonth_1.month_1,Current_Maturity_char(5)) & strcmpi(CSO_MapData_SpreadMonth_1.bbg_product_code,char(Current_BBG_Code));
                            Replace_Year2 = str2double(Current_Maturity_char(6:7));
                        end
                        
                        Replace_Str1 = CSO_MapData_SpreadMonth_1.month_2(Replace_Str1_Index);
                        if(strcmpi(CSO_MapData_SpreadMonth_1.year_change(Replace_Str1_Index),'Yes'))
                            Replace_Year1 = Replace_Year1 + 1;
                            if(Replace_Year1>9)
                                Replace_Year1 = Replace_Year1 + 10 ;
                            end
                        end
                        Replace_Str2 = CSO_MapData_SpreadMonth_1.month_2(Replace_Str2_Index);
                        if(strcmpi(CSO_MapData_SpreadMonth_1.year_change(Replace_Str2_Index),'Yes'))
                            Replace_Year2 = Replace_Year2 + 1;
                            if(Replace_Year2>9)
                                Replace_Year2 = Replace_Year2 + 10 ;
                            end
                        end
                        
                        try
                            New_Maturity_Code(i) = strcat(Replace_Str1,num2str(Replace_Year1),{'.'},Replace_Str2,num2str(Replace_Year2));
                        catch
                            errordlg([char(Current_BBG_Code),' ',char(Current_Maturity),' is not available in cso_mapping_month table'],'CSO Error');
                        end
                    else
                        Current_Maturity_char = char(Current_Maturity);
                        Replace_Str1_Index = strcmpi(CSO_MapData_SpreadMonth_2.month_1,Current_Maturity_char(1)) & strcmpi(CSO_MapData_SpreadMonth_2.bbg_product_code,char(Current_BBG_Code));
                        %Replace_Year1 = str2double(Current_Maturity_char(2));  %% 27-09-2019
                        %%% Newly added if-else conditiuon on 14-06-2016
                        %%% (for hadnling Maturity month)
                        if length(Current_Maturity_char) == 5
                            Replace_Year1 = str2double(Current_Maturity_char(2)); %% 27-09-2019
                            Replace_Str2_Index = strcmpi(CSO_MapData_SpreadMonth_1.month_1,Current_Maturity_char(4)) & strcmpi(CSO_MapData_SpreadMonth_1.bbg_product_code,char(Current_BBG_Code));
                            Replace_Year2 = str2double(Current_Maturity_char(5));
                        elseif length(Current_Maturity_char) == 7
                            Replace_Year1 = str2double(Current_Maturity_char(2:3)); %% 27-09-2019
                            Replace_Str2_Index = strcmpi(CSO_MapData_SpreadMonth_1.month_1,Current_Maturity_char(5)) & strcmpi(CSO_MapData_SpreadMonth_1.bbg_product_code,char(Current_BBG_Code));
                            Replace_Year2 = str2double(Current_Maturity_char(6:7));
                        end
                        
                        Replace_Str1 = CSO_MapData_SpreadMonth_2.month_2(Replace_Str1_Index);
                        if(strcmpi(CSO_MapData_SpreadMonth_2.year_change(Replace_Str1_Index),'Yes'))
                            Replace_Year1 = Replace_Year1 + 1;
                            if(Replace_Year1>9)
                                Replace_Year1 = Replace_Year1 + 10 ;
                            end
                        end
                        Replace_Str2 = CSO_MapData_SpreadMonth_2.month_2(Replace_Str2_Index);
                        if(strcmpi(CSO_MapData_SpreadMonth_2.year_change(Replace_Str2_Index),'Yes'))
                            Replace_Year2 = Replace_Year2 + 1;
                            if(Replace_Year2>9)
                                Replace_Year2 = Replace_Year2 + 10 ;
                            end
                        end
                        
                        try
                            New_Maturity_Code(i) = strcat(Replace_Str1,num2str(Replace_Year1),{'.'},Replace_Str2,num2str(Replace_Year2));
                        catch
                            errordlg([char(Current_BBG_Code),' ',char(Current_Maturity),' is not available in cso_mapping_month table'],'CSO Error');
                        end
                    end
                    
                end
                
                Month1_Data(Negative_StrikeIndex) = New_Maturity_Code;
            end
            
        end
        
        %% Empty  Cell Array
        % CONTRACTNB ,Maturity ,Barrier Type , Barrier , Averaging Start Date ,Averaging End Date , Parent Transaction Number ,Spcl. Rate /lot
        % Other Remarks , OTC DAILY EOD LEG , Trader Name , Traded DateTimestamp
        
        Temp_Cell_Array = cell(size(DBData ,1) ,1);
        
        OPS_Action_Data = cell(size(DBData ,1) ,1);
        OPS_Action_Data(:,:) = {Ops_Action_Str};
        
        %%  Getting Trade ID
        
        [TradeId , Last_TradeId] = getTradeID_4_Cell_Array(DBData,InBUName) ; %% Last Trade ID
        
        %%  Make Data
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
            'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
            'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
        
        RowData = [Instrument_Data,Portfolio,Buy_Sell,Maturity_Code,Month1_Data,Strike_Data,OptType_Data,Prem_Data,CounterParty_Data,InitialLot_Data,Temp_Cell_Array,Temp_Cell_Array,...
            Temp_Cell_Array,Temp_Cell_Array,TransactionDate_Data,Temp_Cell_Array,Source,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,...
            ExeBroker_Data,ExeType,Rates,Temp_Cell_Array,OtherRemarks,Temp_Cell_Array,Temp_Cell_Array,Traded_DateTime_Data,...
            UniqueNumber_Data,BrokerLot_Data,OPS_Action_Data,CounterpartyEntity_Data] ;
        
        
        %% Excel Sheet Write
        
        % Date_Str = strrep(char(datetime),':','_');
        %
        % OutXLSName = ['OPS_to_TR_Format_Options_',Date_Str,'.xlsx'];
        %
        % xlswrite(OutXLSName , [TRHeader ;  RowData] , 'OPSI TO TR Format') ;
        % xlswrite(OutXLSName , [DBFieldNames ;  DBData] , 'OPSI DB') ;
        %
        % OutXLSName = fullfile(pwd,OutXLSName) ;
        % xls_delete_sheets(OutXLSName) ;
        
    end
    
else
    
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
        'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
    
    %     RowData = cell(1,size(TRHeader,2));
    RowData = [];
    
end


