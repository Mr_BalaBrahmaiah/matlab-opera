function xls_change_activesheet(XLFilenameWithPath,SheetName)

% Open Excel file.
objExcel = actxserver('Excel.Application');
objExcel.Workbooks.Open(XLFilenameWithPath); % Full path is necessary!
% Make the sheet as active

try
    % Throws an error if the sheets do not exist.
    Sheets = objExcel.ActiveWorkBook.Sheets;
    CurrentSheet = get(Sheets, 'Item', SheetName);
    CurrentSheet.Activate;
catch ME
    %     errordlg(ME.message,'Missing Deals Parser');
end
% Save, close and clean up.
objExcel.ActiveWorkbook.Save;
objExcel.ActiveWorkbook.Close;
objExcel.Quit;

end