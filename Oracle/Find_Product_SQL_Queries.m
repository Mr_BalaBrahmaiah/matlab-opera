function [SqlQuery_1,SqlQuery_2,SqlQuery_3,SqlQuery_4,SqlQuery_5,InBUName] = Find_Product_SQL_Queries(Business_Unit,Date_Str,Start_Date,End_Date,Commodity)

%%

SqlQuery_1 = [];
SqlQuery_2 = [];
SqlQuery_3 = [];
SqlQuery_4 = [];
SqlQuery_5 = [];

%%
if(strcmpi(Commodity,'FUT'))
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' ']; %% ' where transaction_date >=''', Date_Str ,''' '
        % SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'')' ];
        % SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'')' ];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'];
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CT'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ; %%
        
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ex_code in (''LIFFE'',''CSCE'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))' ];
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ex_code in (''LIFFE'',''CSCE'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CC'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];  %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''',Business_Unit,''' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];  %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];  %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_FUT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];  %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        SqlQuery_3 = ['select * from OP_FUT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_FUT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
    else
        
        
    end
    
else  %% OFUT
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        % SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'')' ];
        %  SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'')' ];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ex_code not in (''MTS'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CT'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL'' '] ;
        
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ex_code in (''LIFFE'',''CSCE'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ex_code in (''LIFFE'',''CSCE'') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CC'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''',Business_Unit,''' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
        
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Date_Str ,''' '];
        SqlQuery_2 = ['select * from OP_OPT_NEW where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_3 = ['select * from OP_OPT_MOD where pr_Code = ''',Business_Unit,''' and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')) ' ];
        SqlQuery_4 = ['select * from OP_OPT_DEL where pr_code = ''CF'' and (trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,''')' ];
        SqlQuery_5 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;
    else
        
        
    end
end
end