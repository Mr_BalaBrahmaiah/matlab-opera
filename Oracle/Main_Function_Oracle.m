clc;
close all;
clear;

% Table_Name = {'ri_futdt','ri_futhd','ri_optdt','ri_opthd','ri_futdt_audit','ri_optdt_audit'};

% Table_Name = {'ORS_UNIT_REGION_V','ORS_TRD_CONV_V','OP_BI_PARTY_MAS','OP_BI_UNIT_MAS','OP_BI_PR_MAS','OP_BI_CATEGORY_MAS',...
%     'ri_futdt_v','ri_futhd','ri_unit_region','op_companymst','op_unitmst','op_partymst','op_prcatmst'};
%
% Table_Name = {'ri_termonmst','ri_lotdet'} ;
% Table_Name = {'ORS_TERMON_MAS_V'} ;
%
% Table_Name = {'ri_optdt','ri_opthd'};
% Table_Name = {'ri_optdt_audit'};

%%
Table_Name = {'OP_OPT_MOD'};

OPS_BUName = 'CT';
InBUName = 'cot';
Ops_Action_Str = 'NEW';

Group_Type = 0; %% 1 Means  FUT , 0 means OPT

OutXLSName = getXLSFilename(['OPSI_Oracle_DB_',char(Table_Name)]);


for i = 1 : length(Table_Name)
    
    Current_Tbl_Name = Table_Name{i};
    
    %% Read From DB
    
    %         [DBFieldNames,DBData] = read_from_database_Oracle(Current_Tbl_Name);
    %         xlswrite('Oracle_DB_Sample.xlsx',[DBFieldNames;DBData],Current_Tbl_Name);
    %         OracleDB.(Current_Tbl_Name) = [DBFieldNames;DBData];
    % fprintf('%d : %d\n',i,length(Table_Name));
    
    %% Connect to DB
    
    ObjDB = connect_to_database_Oracle;
    
    %     SqlQuery = ['select * from ', char(Current_Tbl_Name)] ;
    %     SqlQuery = ['select * from ', char(Current_Tbl_Name), ' where DEAL_DT >= to_date(''2017-03-22'',''yyyy-mm-dd'')']; %% DEAL_DT  %% SYS_DATE
    
    % SqlQuery = ['select * from ', char(Current_Tbl_Name), ' where DEAL_DT >= to_date(''2017-03-22'',''yyyy-mm-dd'') and pr_Code = ''CT'' '];
    %     SqlQuery = ['select * from ', char(Current_Tbl_Name), ' where pr_Code = ''CT'' and (UPD_DATE >= to_date(''2018-01-16'',''yyyy-mm-dd'') and UPD_DATE <= to_date(''2018-01-16'',''yyyy-mm-dd''))  or (deal_dt >= to_date(''2018-01-16'',''yyyy-mm-dd'') and deal_dt <= to_date(''2018-01-16'',''yyyy-mm-dd''))']; %% DEAL_DT  %% SYS_DATE
    %     SqlQuery = ['select * from ', char(Current_Tbl_Name), ' where pr_Code = ''CT'''];
    
    SqlQuery = 'select * from OP_OPT_MOD where pr_Code = ''CT'' and ex_code not in (''MTS'') and ((trunc(deal_dt) >= ''16-Jan-2018'' and trunc(deal_dt) <= ''19-Jan-2018'') or ( trunc(upd_date) >= ''16-Jan-2018'' and trunc(upd_date) <= ''19-Jan-2018'')) ';
    
    curs = exec(ObjDB,SqlQuery);
    curs = fetch(curs);
    
    AttrObj = attr(curs);
    DBFieldNames = {AttrObj.fieldName};
    DBData = curs.Data;
    
    OracleDB.(Current_Tbl_Name) = [DBFieldNames;DBData];
    
    if(Group_Type)
        [TRHeader , TR_RowData ,Instrument_Error_Data ,Maturity_Error_Data] = OPS_Future_2_TR_Format(DBFieldNames , DBData , Ops_Action_Str,InBUName);
    else
        [TRHeader , TR_RowData ,Instrument_Error_Data ,Maturity_Error_Data] = OPS_Option_2_TR_Format(DBFieldNames , DBData , Ops_Action_Str,InBUName);
    end
    
    xlswrite(OutXLSName,[DBFieldNames;DBData],Current_Tbl_Name);
    xlswrite(OutXLSName,[TRHeader;TR_RowData],'TR Foramt');
    
    fprintf('%d : %d\n',i,length(Table_Name));
    
end

% save Oracle_DB_Data.mat OracleDB;

%%

XLS_File_Path = [pwd filesep OutXLSName];
xls_delete_sheets(XLS_File_Path);


