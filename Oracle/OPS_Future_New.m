function [OutTRData,OutXLSName] = OPS_Future_New(SqlQuery)


%%
% Sql_Query_1 = 'select a.deal_dt,a.pr_code,a.pr_sd,a.prcat_code,cat.prcat_sd,a.unit_code,u.unit_sd,a.party_code,p.party_sd,a.ex_code,a.ter_month,nvl(a.bot_lot,0) -nvl(a.sold_lot,0) lot,nvl(lyadj_lot,0) lyadj_lot,nvl(a.lybrok_adj,0)  lybrok_adj,nvl(a.sold_rate,0) +nvl(a.bot_rate,0) rate,a.trd_type,a.sw_id,a.deal_no ,a.uniq_no,a.ord_no,a.remark,a.type1,nvl(a.company_code,''001'') company_code,c.company_sd,a.upd_Date ,decode(b.region,''CC'',''Central'',''Unit'') reg ,decode(sign(nvl(a.bot_lot,0)-nvl(a.sold_lot,0)),-1,''Sold'',''Bot'') Lot_Type,hd.exch_Rate from ri_futdt a,ri_futhd hd,ri_unit_region b,op_companymst c,op_unitmst u,op_partymst p,op_prcatmst cat where a.deal_no = hd.deal_no and a.pr_code = b.pr_code(+) and a.unit_code= b.unit_code(+) and nvl(a.company_code,''001'') = c.company_code and a.unit_code = u.unit_code and a.party_code = p.party_code and a.pr_code = cat.pr_Code and a.prcat_code = cat.prcat_code and (nvl(a.bot_lot,0)-nvl(a.sold_lot,0)-nvl(lyadj_lot,0) <> 0 or nvl(a.bot_lot,0)-nvl(a.sold_lot,0)-nvl(lybrok_adj,0) <> 0) and EX_CODE not in (''MTS'',''ICUS'')and exists  (  select uniq_no from ( select a.*,count(1) over (partition by uniq_no) ucnt from ri_futdt_audit a where  pr_Code = ''CT''';

% Sql_Query_2 = 'and ((trunc(deal_dt)  >= ''15-Feb-2017'' and trunc(deal_dt)  <= ''17-Feb-2017'') or ( trunc(sys_date) >= ''15-Feb-2017'' and trunc(sys_date) <= ''17-Feb-2017''))) c where ucnt =1 and cnt = 0 and action <> ''D'' and c.uniq_no = a.uniq_no )';

% SqlQuery = [Sql_Query_1 , Sql_Query_2] ;

SqlQuery = 'select * from OP_FUT_DUMP where pr_Code = ''CT'' and ex_code not in (''MTS'') and expiry_dt > ''03-Apr-2017''';


%%
ObjDB = connect_to_database_Oracle;

curs = exec(ObjDB,SqlQuery);
curs = fetch(curs);

AttrObj = attr(curs);
DBFieldNames = {AttrObj.fieldName};
DBData = curs.Data;

Overall_Data = [DBFieldNames ; DBData];

%%
Pr_Code_Col = find(cellfun(@(V) strcmpi('pr_code',V), DBFieldNames)) ;
Ex_Code_Col = find(cellfun(@(V) strcmpi('EX_CODE',V), DBFieldNames)) ;
Prcat_Sd_Col = find(cellfun(@(V) strcmpi('prcat_sd',V), DBFieldNames)) ;
Unit_Sd_Col = find(cellfun(@(V) strcmpi('unit_sd',V), DBFieldNames)) ;
LotType_Col = find(cellfun(@(V) strcmpi('LOT_TYPE',V), DBFieldNames)) ;
Month_Col = find(cellfun(@(V) strcmpi('TER_MONTH',V), DBFieldNames)) ;
CounterParty_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
TransactionDate_Col = find(cellfun(@(V) strcmpi('DEAL_DT',V), DBFieldNames)) ;
ExeBroker_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
Prem_Col = find(cellfun(@(V) strcmpi('RATE',V), DBFieldNames)) ;
InitialLot_Col = find(cellfun(@(V) strcmpi('LOT',V), DBFieldNames)) ;
LYADJ_Col = find(cellfun(@(V) strcmpi('LYADJ_OT',V), DBFieldNames)) ;

UniqueNumber_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), DBFieldNames)) ;
LYBROK_Col = find(cellfun(@(V) strcmpi('LYBROK_ADJ',V), DBFieldNames)) ;

BBR_Code_Col = find(cellfun(@(V) strcmpi('BBR_CODE',V), DBFieldNames)) ;

%%  Organize the Data  %% Read Mapping Sheet and Get Instrument

Product_Exch_Data = strcat(DBData(:,Pr_Code_Col), {'-'} , DBData(:,Ex_Code_Col) , {'-'} , DBData(:,Prcat_Sd_Col)) ;

[Instrument_Data , New_Instrument ,Price_Conversion] = Convert_Product_Exchange_Data_2_Instrument(Product_Exch_Data) ;

if(~isempty(New_Instrument))
    
    warndlg('New Instrument Found') ;
    
else
    
    Portfolio = strcat(DBData(:,Pr_Code_Col) , {'-'}, DBData(:,Unit_Sd_Col));
    
    Buy_Sell = DBData(:,LotType_Col) ;
    Bounght_Index = cellStrfind_exact(Buy_Sell,{'Bot'}) ;
    Sold_Index = cellStrfind_exact(Buy_Sell,{'Sold'}) ;
    Buy_Sell(Bounght_Index)  = {'Bought'} ;
    Buy_Sell(Sold_Index)  = {'Sold'} ;
    
    Month_DB =  DBData(:,Month_Col) ;
    Commodity = 'FUT';
    BBR_Code_Data = DBData(:,BBR_Code_Col);
    [Month_Maturity , Maturity_Code] = Convert_Month_2_Maturity_Format(Month_DB ,BBR_Code_Data,Commodity) ;
    
    Prem_Data = num2cell( cell2mat(DBData(:,Prem_Col)) .* cell2mat(Price_Conversion) );
    CounterParty_Data = DBData(:,CounterParty_Col);
    InitialLot_Data = num2cell(cell2mat(DBData(:,InitialLot_Col)) - cell2mat(DBData(:,LYADJ_Col)));
    TransactionDate_Data = DBData(:,TransactionDate_Col);
    ExeBroker_Data = DBData(:,ExeBroker_Col);
    
    UniqueNumber_Data = DBData(:,UniqueNumber_Col);
    BrokerLot_Data = num2cell(cell2mat(DBData(:,InitialLot_Col)) - cell2mat(DBData(:,LYBROK_Col)));
    
    %% Hard Code Field Names
    OptType = cell(size(DBData ,1) ,1) ;
    Source = cell(size(DBData ,1) ,1) ;
    ExeType = cell(size(DBData ,1) ,1) ;
    Rates = cell(size(DBData ,1) ,1) ;
    
    OptType(:,:) = {'Future'};
    Source(:,:) = {'OPS'};
    ExeType(:,:) = {'Elec'} ;
    Rates(:,:) = {'Normal'} ;
    
    
    %% Empty  Cell Array
    % Strike CONTRACTNB ,Maturity ,Barrier Type , Barrier , Averaging Start Date ,Averaging End Date , Parent Transaction Number ,Spcl. Rate /lot
    % Other Remarks , OTC DAILY EOD LEG , Trader Name , Traded DateTimestamp
    
    Temp_Cell_Array = cell(size(DBData ,1) ,1);
    
    Ops_Action_Str = 'New';
    OPS_Action_Data = cell(size(DBData ,1) ,1);
    OPS_Action_Data(:,:) = {Ops_Action_Str};
    
    %%  Getting Trade ID
    
    [TradeId , Last_TradeId] = getTradeID_4_Cell_Array(DBData,'cot') ; %% Last Trade ID
    
    %%  Make Data
    
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
        'OptType','Prem','CounterParty','Initial Lots','CONTRACTNB','Maturity','Barrier Type',...
        'Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS','OPS Action'};
    
    RowData = [Instrument_Data,Portfolio,Buy_Sell,Maturity_Code,Temp_Cell_Array,OptType,Prem_Data,CounterParty_Data,InitialLot_Data,Temp_Cell_Array,...
        Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,TransactionDate_Data,TradeId,Source,Temp_Cell_Array,Temp_Cell_Array,TradeId,...
        ExeBroker_Data,ExeType,Rates,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,...
        UniqueNumber_Data,BrokerLot_Data,OPS_Action_Data] ;
    
    
    %% Excel Sheet Write
    
    Date_Str = strrep(char(datetime),':','_');
    
    OutXLSName = ['OPS_to_TR_Format_Future_',Date_Str,'.xlsx'];
    
    OutTRData = [TRHeader ;  RowData];
    
    xlswrite(OutXLSName , [TRHeader ;  RowData] , 'OPSI TO TR Format') ;
    xlswrite(OutXLSName , [DBFieldNames ;  DBData] , 'OPSI DB') ;
    
    OutXLSName = fullfile(pwd,OutXLSName) ;
    xls_delete_sheets(OutXLSName) ;
    xls_change_activesheet(OutXLSName,'OPSI TO TR Format');
    
    
end




