function [OutXLSName] = Get_OverAll_Dump(BU_Name,Pr_Code,Live_Only,Live_Dead)

%% Connect to DB
ObjDB = connect_to_database;
ObjDB_Oracle = connect_to_database_Oracle;

Default_OperaView = 'deal_ticket_maturity_view_' ;
ViewName = strcat(Default_OperaView,BU_Name);


Today_Date = today ; %% 736770 ; %% 736788 ; %% today %%736785;

%% Get Excel Name

OutXLSName = getXLSFilename(strcat('Overall_Dump_OPS_OPERA_',BU_Name));

if(strcmpi(BU_Name,'cot'))
    Recon_ReplaceStr = 'CT-';
elseif(strcmpi(BU_Name,'coc'))
    Recon_ReplaceStr = 'CC-';
elseif(strcmpi(BU_Name,'cof'))
    Recon_ReplaceStr = 'CF-';
elseif(strcmpi(BU_Name,'grn'))
    Recon_ReplaceStr = 'GR-';
elseif(strcmpi(BU_Name,'dry'))
    Recon_ReplaceStr = 'MP-';
elseif(strcmpi(BU_Name,'rbr'))
    Recon_ReplaceStr = 'RB-';
elseif(strcmpi(BU_Name,'sgr'))
    Recon_ReplaceStr = 'SG-';
else
    
end

%%
if(Live_Only)
    
    if(strcmpi(Pr_Code,'CT'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    elseif(strcmpi(Pr_Code,'CC'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''CJ%''  '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''CJ%'' '];
    elseif(strcmpi(Pr_Code,'CF'))
        %         OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        %         OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    elseif(strcmpi(Pr_Code,'GR'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''GS%'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' and ops_unique not like ''GS%'' '];
    elseif(strcmpi(Pr_Code,'MP'))
        %         OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        %         OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    elseif(strcmpi(Pr_Code,'RB'))
        %         OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        %         OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    elseif(strcmpi(Pr_Code,'SG'))
        %         OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        %         OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and COMPANY_SD = ''OLAM INTERNATIONAL'' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and expiry_dt >= ''',datestr(Today_Date,'dd-mmm-yyyy'),''' '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and maturity_date>= ''',datestr(Today_Date,'yyyy-mm-dd'),''' '];
    else
        
    end
    
else %% Live & Dead
    
    if(strcmpi(Pr_Code,'CT'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code not in (''MTS'') '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
    elseif(strcmpi(Pr_Code,'CC'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,''' and ex_code in (''LIFFE'',''CSCE'') '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and ops_unique not like ''CJ%'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and ops_unique not like ''CJ%'' '];
    elseif(strcmpi(Pr_Code,'CF'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
    elseif(strcmpi(Pr_Code,'GR'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,'''  '];
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,'''  '];
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' and ops_unique not like ''GS%'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' and ops_unique not like ''GS%'' '];
    elseif(strcmpi(Pr_Code,'MP'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
    elseif(strcmpi(Pr_Code,'RB'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
    elseif(strcmpi(Pr_Code,'SG'))
        OPS_FUT_SqlQuery = ['select * from OP_FUT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        OPS_OPT_SqlQuery = ['select * from OP_OPT_DUMP where pr_Code = ''',Pr_Code,'''  ']; %% and COMPANY_SD = ''OLAM INTERNATIONAL''
        Opera_FUT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''FUT'' '];
        Opera_OPT_SqlQuery = ['select * from ',ViewName,' where ops_action<>''DEL'' and trade_group_type=''OFUT'' '];
        
    else
        
    end
    
end

%% Fetch Data

[OPS_FUT_FieldNames,OPS_FUT_DBData] = Fetch_DB_Data(ObjDB_Oracle,OPS_FUT_SqlQuery);
[OPS_OPT_FieldNames,OPS_OPT_DBData] = Fetch_DB_Data(ObjDB_Oracle,OPS_OPT_SqlQuery);

OPT_LOT_Col = cellStrfind_exact(OPS_FUT_FieldNames,{'LOT'});
LYADJ_OT_Col = cellStrfind_exact(OPS_FUT_FieldNames,{'LYADJ_OT'});
LYBROK_ADJ_Col = cellStrfind_exact(OPS_FUT_FieldNames,{'LYBROK_ADJ'});
FUT_UnitWise_Lots = num2cell(cell2mat(OPS_FUT_DBData(:,OPT_LOT_Col)) - cell2mat(OPS_FUT_DBData(:,LYADJ_OT_Col)));
FUT_BrokerWise_Lots = num2cell(cell2mat(OPS_FUT_DBData(:,OPT_LOT_Col)) - cell2mat(OPS_FUT_DBData(:,LYBROK_ADJ_Col)));
OPS_FUT_FieldNames = [OPS_FUT_FieldNames ,{'Calc_UnitWise_Lots','Calc_BrokerWise_Lots'}];
OPS_FUT_DBData = [OPS_FUT_DBData ,FUT_UnitWise_Lots , FUT_BrokerWise_Lots];

OPT_LOT_Col = cellStrfind_exact(OPS_OPT_FieldNames,{'LOT'});
LYBROK_ADJ_Col = cellStrfind_exact(OPS_OPT_FieldNames,{'LYBROK_ADJ'});
OPT_UnitWise_Lots = OPS_OPT_DBData(:,OPT_LOT_Col);
OPT_BrokerWise_Lots = num2cell(cell2mat(OPS_OPT_DBData(:,OPT_LOT_Col)) - cell2mat(OPS_OPT_DBData(:,LYBROK_ADJ_Col)));
OPS_OPT_FieldNames = [OPS_OPT_FieldNames , {'Calc_UnitWise_Lots','Calc_BrokerWise_Lots'}];
OPS_OPT_DBData = [OPS_OPT_DBData , OPT_UnitWise_Lots ,OPT_BrokerWise_Lots];

[Opera_FUT_FieldNames,Opera_FUT_DBData] = Fetch_DB_Data(ObjDB,Opera_FUT_SqlQuery);
[Opera_OPT_FieldNames,Opera_OPT_DBData] = Fetch_DB_Data(ObjDB,Opera_OPT_SqlQuery);

OPS_FUT_UniqueCol = cellStrfind_exact(OPS_FUT_FieldNames,{'UNIQ_NO'});
OPS_FUT_UniqueData = OPS_FUT_DBData(:,OPS_FUT_UniqueCol);

OPS_OPT_UniqueCol = cellStrfind_exact(OPS_OPT_FieldNames,{'UNIQ_NO'});
OPS_OPT_UniqueData = OPS_OPT_DBData(:,OPS_OPT_UniqueCol);

Opera_FUT_UniqueCol = cellStrfind_exact(Opera_FUT_FieldNames,{'ops_unique'});
Opera_FUT_UniqueData = Opera_FUT_DBData(:,Opera_FUT_UniqueCol);

Opera_OPT_UniqueCol = cellStrfind_exact(Opera_OPT_FieldNames,{'ops_unique'});
Opera_OPT_UniqueData = Opera_OPT_DBData(:,Opera_OPT_UniqueCol);

Overall_OPS_Data = [OPS_FUT_UniqueData ; OPS_OPT_UniqueData];
Overall_OPERA_Data = [Opera_FUT_UniqueData ; Opera_OPT_UniqueData];

%% Find Missing Trades
if(~isempty(Overall_OPS_Data) && ~isempty(Overall_OPERA_Data))
    OPS_NotExisting_OPERA = setdiff(Overall_OPS_Data,Overall_OPERA_Data);
    OPERA_NotExisting_OPS  = setdiff(Overall_OPERA_Data,Overall_OPS_Data);
else
    if(isempty(Overall_OPS_Data))
        Overall_OPS_Data = {''};
    end
    
    if(isempty(Overall_OPERA_Data))
        Overall_OPERA_Data = {''};
    end
    OPS_NotExisting_OPERA = setdiff(Overall_OPS_Data,Overall_OPERA_Data);
    OPERA_NotExisting_OPS  = setdiff(Overall_OPERA_Data,Overall_OPS_Data);
    
end


%% FUT Recon

OPS_FUT_Needed_Fileds = {'UNIQ_NO','UNIT_SD','Calc_UnitWise_Lots','Calc_BrokerWise_Lots'};
OPS_FUT_ReconData = OPS_FUT_DBData(:,cellStrfind_exact(OPS_FUT_FieldNames,OPS_FUT_Needed_Fileds));
UNIQ_NO_Col = cellStrfind_exact(OPS_FUT_Needed_Fileds,{'UNIQ_NO'});
OPS_NotExisting_OPERA_Index = cellStrfind_exact(OPS_FUT_ReconData(:,UNIQ_NO_Col),OPS_NotExisting_OPERA);


Opera_FUT_Needed_Fields = {'trade_id','ops_unique','original_lots','subportfolio','broker_lots'};
Opera_FUT_ReconData = Opera_FUT_DBData(:,cellStrfind_exact(Opera_FUT_FieldNames,Opera_FUT_Needed_Fields));
OPS_Unique_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'ops_unique'});
OPERA_NotExisting_OPS_Index = cellStrfind_exact(Opera_FUT_ReconData(:,OPS_Unique_Col),OPERA_NotExisting_OPS);

OPS_FUT_ReconData(OPS_NotExisting_OPERA_Index,:) = [];
Opera_FUT_ReconData(OPERA_NotExisting_OPS_Index,:) = [];

if(size(OPS_FUT_ReconData,1) == size(Opera_FUT_ReconData,1))
    
    UniqueNum_OrderIndex = cellStrfind_exact(OPS_FUT_ReconData(:,1),Opera_FUT_ReconData(:,2));
    OPS_FUT_ReconData = OPS_FUT_ReconData(UniqueNum_OrderIndex,:); %% Making Order for Subtraction
    
    OPS_UnitLots_Col = cellStrfind_exact(OPS_FUT_Needed_Fileds,{'Calc_UnitWise_Lots'});
    Opera_OriginalLots_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'original_lots'});
    FUT_Lots_Diff = num2cell(cell2mat(OPS_FUT_ReconData(:,OPS_UnitLots_Col)) - cell2mat(Opera_FUT_ReconData(:,Opera_OriginalLots_Col)));
    
    OPS_UnitSD_Col = cellStrfind_exact(OPS_FUT_Needed_Fileds,{'UNIT_SD'});
    Opera_Subportfolio_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'subportfolio'});
    Opera_FUT_ReconData(:,Opera_Subportfolio_Col) = strrep(Opera_FUT_ReconData(:,Opera_Subportfolio_Col),Recon_ReplaceStr,''); %% Remove Opera Extension if Cottton Like 'CT-' will be removed for Matching
    Subportfolio_Diff = num2cell(strcmpi(OPS_FUT_ReconData(:,OPS_UnitSD_Col) ,Opera_FUT_ReconData(:,Opera_Subportfolio_Col)));
    
    OPS_BrokerLots_Col = cellStrfind_exact(OPS_FUT_Needed_Fileds,{'Calc_BrokerWise_Lots'});
    Opera_BrokerLots_Col = cellStrfind_exact(Opera_FUT_Needed_Fields,{'broker_lots'});
    FUT_BrokerLots_Diff = num2cell(cell2mat(OPS_FUT_ReconData(:,OPS_BrokerLots_Col)) - cell2mat(Opera_FUT_ReconData(:,Opera_BrokerLots_Col)));
    
    OPS_FUT_Needed_Fileds = [{'OPS','','',''} ; OPS_FUT_Needed_Fileds];
    Opera_FUT_Needed_Fields = [{'OPERA','','','',''} ; Opera_FUT_Needed_Fields];
    Recon_Header = [{'Recon','',''} ; {'OriginalLots_Difference','Subportfolio matching','BrokerLots_Difference'} ] ;
    
    Temp_Cell = cell(size(Opera_FUT_ReconData,1),2);
    Temp_Header = cell(2,size(Temp_Cell,2));
    
    FUT_Recon_Header = [OPS_FUT_Needed_Fileds , Temp_Header , Opera_FUT_Needed_Fields , Temp_Header , Recon_Header];
    FUT_Recon_OverallData = [OPS_FUT_ReconData ,Temp_Cell ,Opera_FUT_ReconData , Temp_Cell , FUT_Lots_Diff , Subportfolio_Diff , FUT_BrokerLots_Diff];
    
    xlswrite(OutXLSName,[FUT_Recon_Header ; FUT_Recon_OverallData],'FUT RECON');
else
    xlswrite(OutXLSName,{'OPS_FUT Count and Opera_FUT Count is Mismatched So Unable to Create FUT Recon Dump'},'FUT RECON');
end

%% OPTION Recon

OPS_OPT_Needed_Fileds = {'UNIQ_NO','UNIT_SD','Calc_UnitWise_Lots','Calc_BrokerWise_Lots'};
OPS_OPT_ReconData = OPS_OPT_DBData(:,cellStrfind_exact(OPS_OPT_FieldNames,OPS_OPT_Needed_Fileds));
UNIQ_NO_Col = cellStrfind_exact(OPS_OPT_Needed_Fileds,{'UNIQ_NO'});
OPS_NotExisting_OPERA_Index = cellStrfind_exact(OPS_OPT_ReconData(:,UNIQ_NO_Col),OPS_NotExisting_OPERA);

Opera_OPT_Needed_Fields = {'trade_id','ops_unique','original_lots','subportfolio','broker_lots'};
Opera_OPT_ReconData = Opera_OPT_DBData(:,cellStrfind_exact(Opera_OPT_FieldNames,Opera_OPT_Needed_Fields));
OPS_Unique_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'ops_unique'});
OPERA_NotExisting_OPS_Index = cellStrfind_exact(Opera_OPT_ReconData(:,OPS_Unique_Col),OPERA_NotExisting_OPS);

OPS_OPT_ReconData(OPS_NotExisting_OPERA_Index,:) = [];
Opera_OPT_ReconData(OPERA_NotExisting_OPS_Index,:) = [];

if(size(OPS_OPT_ReconData,1) == size(Opera_OPT_ReconData,1))
    
    UniqueNum_OrderIndex = cellStrfind_exact(OPS_OPT_ReconData(:,1),Opera_OPT_ReconData(:,2));
    OPS_OPT_ReconData = OPS_OPT_ReconData(UniqueNum_OrderIndex,:); %% Making Order for Subtraction
    
    OPS_UnitLots_Col = cellStrfind_exact(OPS_OPT_Needed_Fileds,{'Calc_UnitWise_Lots'});
    Opera_OriginalLots_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'original_lots'});
    OPT_Lots_Diff = num2cell(cell2mat(OPS_OPT_ReconData(:,OPS_UnitLots_Col)) - cell2mat(Opera_OPT_ReconData(:,Opera_OriginalLots_Col)));
    
    OPS_UnitSD_Col = cellStrfind_exact(OPS_OPT_Needed_Fileds,{'UNIT_SD'});
    Opera_Subportfolio_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'subportfolio'});
    Opera_OPT_ReconData(:,Opera_Subportfolio_Col) = strrep(Opera_OPT_ReconData(:,Opera_Subportfolio_Col),Recon_ReplaceStr,''); %% Remove Opera Extension if Cottton Like 'CT-' will be removed for Matching
    Subportfolio_Diff = num2cell(strcmpi(OPS_OPT_ReconData(:,OPS_UnitSD_Col) ,Opera_OPT_ReconData(:,Opera_Subportfolio_Col)));
    
    OPS_BrokerLots_Col = cellStrfind_exact(OPS_OPT_Needed_Fileds,{'Calc_BrokerWise_Lots'});
    Opera_BrokerLots_Col = cellStrfind_exact(Opera_OPT_Needed_Fields,{'broker_lots'});
    OPT_BrokerLots_Diff = num2cell(cell2mat(OPS_OPT_ReconData(:,OPS_BrokerLots_Col)) - cell2mat(Opera_OPT_ReconData(:,Opera_BrokerLots_Col)));
    
    OPS_OPT_Needed_Fileds = [{'OPS','','',''} ; OPS_OPT_Needed_Fileds];
    Opera_OPT_Needed_Fields = [{'OPERA','','','',''} ; Opera_OPT_Needed_Fields];
    Recon_Header = [{'Recon','',''} ; {'OriginalLots_Difference','Subportfolio matching','BrokerLots_Difference'} ] ;
    
    Temp_Cell = cell(size(Opera_OPT_ReconData,1),2);
    Temp_Header = cell(2,size(Temp_Cell,2));
    
    OPT_Recon_Header = [OPS_OPT_Needed_Fileds , Temp_Header , Opera_OPT_Needed_Fields , Temp_Header , Recon_Header];
    OPT_Recon_OverallData = [OPS_OPT_ReconData ,Temp_Cell ,Opera_OPT_ReconData , Temp_Cell , OPT_Lots_Diff , Subportfolio_Diff, OPT_BrokerLots_Diff];
    
    xlswrite(OutXLSName,[OPT_Recon_Header ; OPT_Recon_OverallData],'OPT RECON');
else
    xlswrite(OutXLSName,{'OPS_OPT Count and Opera_OPT Count is Mismatched So Unable to Create OPT Recon Dump'},'OPT RECON');
end


%% Excel Write


xlswrite(OutXLSName,[OPS_FUT_FieldNames;OPS_FUT_DBData],'OPS_FUT_DUMP');
xlswrite(OutXLSName,[OPS_OPT_FieldNames;OPS_OPT_DBData],'OPS_OPT_DUMP');
xlswrite(OutXLSName,[Opera_FUT_FieldNames;Opera_FUT_DBData],'OPERA_FUT_DUMP');
xlswrite(OutXLSName,[Opera_OPT_FieldNames;Opera_OPT_DBData],'OPERA_OPT_DUMP');

if(~isempty(OPERA_NotExisting_OPS))
    xlswrite(OutXLSName,OPERA_NotExisting_OPS,'OPERA_NotExisting_OPS');
else
    xlswrite(OutXLSName,{'No Data OPERA and OPS Matched'},'OPERA_NotExisting_OPS');
end

if(~isempty(OPS_NotExisting_OPERA))
    
    xlswrite(OutXLSName,OPS_NotExisting_OPERA,'OPS_NotExisting_OPERA');
    
    OPS_FUT_Data = OPS_FUT_DBData(cellStrfind_exact(OPS_FUT_UniqueData,OPS_NotExisting_OPERA),:);
    if(~isempty(OPS_FUT_Data))
        %         [TRHeader_FUT,RowData_FUT] = Convert_OPS_FUT_2_TR_Format(OPS_FUT_FieldNames,OPS_FUT_Data,BU_Name);
        Ops_Action_Str = 'NEW';
        [TRHeader_FUT , RowData_FUT ] = OPS_Future_2_TR_Format(OPS_FUT_FieldNames , OPS_FUT_Data , Ops_Action_Str,BU_Name);
        xlswrite(OutXLSName,[TRHeader_FUT;RowData_FUT],'OPS_FUT_TR_Format');
    end
    
    
    OPS_OPT_Data = OPS_OPT_DBData(cellStrfind_exact(OPS_OPT_UniqueData,OPS_NotExisting_OPERA),:);
    if(~isempty(OPS_OPT_Data))
        %         [TRHeader_OPT,RowData_OPT] = Convert_OPS_OPT_2_TR_Format(OPS_OPT_FieldNames,OPS_OPT_Data,BU_Name);
        Ops_Action_Str = 'NEW';
        [TRHeader_OPT , RowData_OPT ] = OPS_Option_2_TR_Format(OPS_OPT_FieldNames , OPS_OPT_Data ,Ops_Action_Str ,BU_Name);
        xlswrite(OutXLSName,[TRHeader_OPT;RowData_OPT],'OPS_OPT_TR_Format');
    end
    
else
    xlswrite(OutXLSName,{'No Data OPS and OPERA Matched'},'OPS_NotExisting_OPERA');
    
end


OutXLSName = fullfile(pwd,OutXLSName) ;
xls_delete_sheets(OutXLSName) ;

end




%%
