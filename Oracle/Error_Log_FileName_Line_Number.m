function Error_Log_FileName_Line_Number(ME)

if(exist('Error_Log_FileName_Line_Number.log','file'))
    delete([pwd filesep 'Error_Log_FileName_Line_Number.log']);
end

fid = fopen('Error_Log_FileName_Line_Number.log','wt');

for i = 1 : length(ME.stack)
    
    fprintf(fid,'%s : %d\n' , char(ME.stack(i).file), ME.stack(i).line);
    
end

fclose(fid);