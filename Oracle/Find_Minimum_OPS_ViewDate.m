function [Min_Opera_Date] = Find_Minimum_OPS_ViewDate(Prod_Code,Start_Date,End_Date)

% Prod_Code = 'CT';

ObjDB_Oracle = connect_to_database_Oracle ;

%%
SqlQuery1 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_fut_new where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;
SqlQuery2 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_fut_mod where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;
SqlQuery3 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_opt_new where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;
SqlQuery4 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_opt_mod where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;
SqlQuery5 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_fut_del where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;
SqlQuery6 = ['select to_char(min(deal_dt),''DD-MON-YYYY'') from op_opt_del where pr_code in (''',Prod_Code,''') and ((trunc(deal_dt) >= ''',Start_Date,''' and trunc(deal_dt) <= ''',End_Date,''') or ( trunc(upd_date) >= ''',Start_Date,''' and trunc(upd_date) <= ''',End_Date,'''))'] ;


%%
FutNew_Date = fetch(ObjDB_Oracle,SqlQuery1) ;
FutMod_Date = fetch(ObjDB_Oracle,SqlQuery2) ;
OptNew_Date = fetch(ObjDB_Oracle,SqlQuery3) ;
OptMod_Date = fetch(ObjDB_Oracle,SqlQuery4) ;
FutDel_Date = fetch(ObjDB_Oracle,SqlQuery5) ;
OptDel_Date = fetch(ObjDB_Oracle,SqlQuery6) ;

All_Dates = [FutNew_Date ; FutMod_Date ; OptNew_Date ; OptMod_Date ; FutDel_Date ; OptDel_Date ] ;
All_Dates = All_Dates(cellfun(@(s) (length(s)>1), All_Dates)) ;  %% Remove Empty Date Fields

Min_All_Dates = min(datenum(All_Dates)) ;
Min_Ops_Date = datestr(Min_All_Dates,'dd-mmm-yyyy') ;

%%

[System_Date_Format,~] = get_System_Date_Format();

HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));

Opera_Date_Str = datestr(busdate(today-90,-1,HolidayVec),1);
Opera_Date_Num = datenum(Opera_Date_Str);

if(Opera_Date_Num < Min_All_Dates)
    Min_Opera_Date = Opera_Date_Str ;
else
    Min_Opera_Date = Min_Ops_Date ;
end

%%

close(ObjDB_Oracle) ;
delete(ObjDB_Oracle) ;

end
