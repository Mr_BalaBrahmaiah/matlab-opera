
ObjDB_Oracle = connect_to_database_Oracle;

%Input File
[filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
InXLSFilename = fullfile(pathname,filename);
[~,~,RawData] = xlsread(InXLSFilename);

Header = RawData(1,:);
RawData = RawData(2:end,:);

UniqueID = RawData;

%%% cell to Char
UniqueID_Str = convertCell2Char(UniqueID);
UniqueID_Str = strrep(UniqueID_Str,',',''',''');

%%% Fetch Data 

SqlQuery_1 = ['select * from op_fut_dump where UNIQ_NO in (''',UniqueID_Str,''')'];
SqlQuery_2 = ['select * from op_opt_dump where UNIQ_NO in (''',UniqueID_Str,''')'];

[FUT_FiledNames,FUT_Data] = Fetch_DB_Data(ObjDB_Oracle,SqlQuery_1);
[OPT_FiledNames,OPT_Data] = Fetch_DB_Data(ObjDB_Oracle,SqlQuery_2);

FUT_FiledNames = [FUT_FiledNames,{'Group_Type'}];
FUT_GroupType = cell(size(FUT_Data,1),1);
FUT_GroupType(:,1) = {'FUT'};
FUT_Data = [FUT_Data , FUT_GroupType];

OPT_FiledNames = [OPT_FiledNames,{'Group_Type'}];
OPT_GroupType = cell(size(OPT_Data,1),1);
OPT_GroupType(:,1) = {'OFUT'};
OPT_Data = [OPT_Data , OPT_GroupType];

FUT_UniqueNo_Col = cellStrfind_exact(FUT_FiledNames,{'UNIQ_NO'});
OPT_UniqueNo_Col = cellStrfind_exact(OPT_FiledNames,{'UNIQ_NO'});

Dump_UniqueID = [FUT_Data(:,FUT_UniqueNo_Col) ; OPT_Data(:,OPT_UniqueNo_Col)];

Missing_UniqueID = setdiff(RawData,Dump_UniqueID);

if(isempty(Missing_UniqueID)) 
Missing_UniqueID = {'No Missing ID'};
end

%%% Excel Write 
XLSName = getXLSFilename('UniqueID_Dump');
xlswrite(XLSName,[FUT_FiledNames ; FUT_Data],'FUT Dump');
xlswrite(XLSName,[OPT_FiledNames ; OPT_Data],'OPT Dump');
xlswrite(XLSName,Missing_UniqueID,'Missing ID in Dump');

xls_delete_sheets(fullfile(pwd,XLSName));










