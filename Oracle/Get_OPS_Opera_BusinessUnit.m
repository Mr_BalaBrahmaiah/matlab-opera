function [Business_Unit,InBUName] = Get_OPS_Opera_BusinessUnit(handles)


Business_Unit = '';
InBUName = '';

%%

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(CT || CC || CF || GR || MP || RB || SG)
    
    ProductNames = {'CT','CC','CF','GR','MP','RB','SG'}; %% Hard Code
    BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
    User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
    BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
    
    if(strcmpi(Business_Unit,'CT'))
        InBUName = 'cot';
    elseif(strcmpi(Business_Unit,'CC'))
        InBUName = 'coc';
    elseif(strcmpi(Business_Unit,'CF'))
        InBUName = 'cof';
    elseif(strcmpi(Business_Unit,'GR'))
        InBUName = 'grn';
    elseif(strcmpi(Business_Unit,'MP'))
        InBUName = 'dry';
    elseif(strcmpi(Business_Unit,'RB'))
        InBUName = 'rbr';
    elseif(strcmpi(Business_Unit,'SG'))
        InBUName = 'sgr';
    else
        
    end
    
end