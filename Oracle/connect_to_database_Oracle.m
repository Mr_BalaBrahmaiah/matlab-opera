function objDB = connect_to_database_Oracle
% connect to the database configuration as provided in the
% DatabaseConfiguration file
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/01/23 07:18:53 $
%  $Revision: 1.1 $
%

try
    javaaddpath('ojdbc6-11.2.0.4.0.jar');
    % Read the database configuration file to read the DB configuration
    % Default DB config values
    DBName = 'opsi';
    DBUserName = 'iolam';
    DBPassword = 'iolam1234';
    IsFromServer = 1;
    DBServerIP = '192.168.1.1';
    Schema_Name = 'iolam';
    DBServerPort = 1521;
    try
        DBConfigData = textread('DatabaseConfiguration_Oracle.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'schema_name');
        if ~isempty(IdxFound)
            [~,Schema_Name] = strtok(DBConfigData(IdxFound),'=');
            Schema_Name = char(strrep(Schema_Name,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'server_port');
        if ~isempty(IdxFound)
            DBServerPort = strsplit(char(DBConfigData(IdxFound)),'=');
            DBServerPort = char(DBServerPort(end));
            DBServerPort = str2num(DBServerPort);
        end
        disp(['Using the database ',DBName]);
    catch
        disp('Error occured in reading the DatabaseConfiguration_PostGresSQL.txt, hence using the default configuration!');
    end
    
    if IsFromServer
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','Oracle','DriverType','thin','Server',DBServerIP,'PortNumber',DBServerPort);
    else
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','Oracle','DriverType','thin');
    end
catch ME
    if isconnection(objDB)
        close(objDB);
    end
    errordlg(ME.message);
end
end