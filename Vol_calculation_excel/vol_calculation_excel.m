function varargout = vol_calculation_excel(varargin)
% VOL_CALCULATION_EXCEL MATLAB code for vol_calculation_excel.fig
%      VOL_CALCULATION_EXCEL, by itself, creates a new VOL_CALCULATION_EXCEL or raises the existing
%      singleton*.
%
%      H = VOL_CALCULATION_EXCEL returns the handle to a new VOL_CALCULATION_EXCEL or the handle to
%      the existing singleton*.
%
%      VOL_CALCULATION_EXCEL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VOL_CALCULATION_EXCEL.M with the given input arguments.
%
%      VOL_CALCULATION_EXCEL('Property','Value',...) creates a new VOL_CALCULATION_EXCEL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before vol_calculation_excel_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to vol_calculation_excel_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help vol_calculation_excel

% Last Modified by GUIDE v2.5 19-Aug-2019 16:18:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @vol_calculation_excel_OpeningFcn, ...
                   'gui_OutputFcn',  @vol_calculation_excel_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before vol_calculation_excel is made visible.
function vol_calculation_excel_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to vol_calculation_excel (see VARARGIN)

% Choose default command line output for vol_calculation_excel
handles.output = hObject;
set(handles.edit3,'String',-1);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes vol_calculation_excel wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = vol_calculation_excel_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.xlsx', 'Pick an excel-file');
if isequal(filename,0)
   msgbox('User selected Cancel')
else
    fullfilepath=fullfile(pathname, filename);
    set(handles.edit1,'string',fullfilepath);
end
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    time_shiftvalue=get(handles.edit3,'String');
    file_path=get(handles.edit1,'String');
    
    f=waitbar(0.1,'Reading excel file','Name','progress');
    [~,~,raw]=xlsread(file_path);
    headers=raw(1,:);
    asset_pos=find(strcmpi(headers,'Asset_price'));
    strike_pos=find(strcmpi(headers,'strike'));
    option_pos=find(strcmpi(headers,'optiontype'));
    matruity_pos=find(strcmpi(headers,'maturity_date'));
    settle_pos=find(strcmpi(headers,'settle_price')); 
    
    if ~isempty(asset_pos) && ~isempty(strike_pos) && ~isempty(option_pos) && ~isempty(matruity_pos) && ~isempty(settle_pos) && ~isempty(time_shiftvalue)
    waitbar(0.33,f,'Caluculating vols');
    SettleVols = vanillaimpvolbybs(raw(2:end,asset_pos), raw(2:end,strike_pos), raw(2:end,option_pos), raw(2:end,matruity_pos), str2double(time_shiftvalue), raw(2:end,settle_pos));
   
    [filepath,name,ext] = fileparts(file_path);
    
    output_file=[filepath filesep name '_output' ext];
    
    output_header=[headers(asset_pos) headers(strike_pos)  headers(option_pos)  headers(matruity_pos)  headers(settle_pos) 'Settlevols'];
    outputdata=[raw(2:end,asset_pos) raw(2:end,strike_pos) raw(2:end,option_pos) raw(2:end,matruity_pos)  raw(2:end,settle_pos) SettleVols];
  
    waitbar(0.67,f,'Writing data to excel');
    xlswrite(output_file,[output_header;outputdata]);
    
    waitbar(1,f,'Finished');
    %close(f);
    else
        errordlg('Required column not found in excel : Asset_price/strike/optiontype/maturity_datesettle_price or Timeshiftvalue is not entered');
        close(f);
    end
    
catch ME
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
     ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
   close(f);
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
