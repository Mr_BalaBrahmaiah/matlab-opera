function Output = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, OptionPrice)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/05/23 10:10:04 $
%  $Revision: 1.0 

    NumInputs = size(AssetPrice);
    Output = cell(NumInputs);
    Output(:,:) = {NaN};
    
    AssetPrice      = utilgetvalidnumericarray(AssetPrice);
    Strike          = utilgetvalidnumericarray(Strike);
    MaturityDate    = utilgetvaliddatearray(MaturityDate);
    OptionPrice     = utilgetvalidnumericarray(OptionPrice);
    
    Idx = ~isnan(AssetPrice) & ~isnan(Strike) & ~isnan(MaturityDate) & ~isnan(TimeShift) & ~isnan(OptionPrice);
    
    ValuationDate = datenum(today())+ TimeShift - 1;% -1 , Since Matlab follow TTM=MD-VD convention

      
    VDN = ValuationDate;
    MDN = MaturityDate;
    
    NumInputs = size(AssetPrice(Idx));
     
    validateattributes(AssetPrice(Idx),   {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'vanillaimpvolbybs', 'AssetPrice');
    validateattributes(Strike(Idx),       {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaimpvolbybs', 'Strike');
    validateattributes(MaturityDate(Idx), {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaimpvolbybs', 'MaturityDate'); 
    validateattributes(TimeShift,         {'numeric'}, {'nonempty', 'real',        'scalar'                   }, 'vanillaimpvolbybs', 'TimeShift');
    
    MaturityDate(MaturityDate <= ValuationDate) = ValuationDate+1; % To get rid of error message
  
    if ~iscell(OptionType)
        OptionType = {OptionType};
    end

    Rates = zeros(size(AssetPrice(Idx)));
    Volatility = nan(size(AssetPrice(Idx)));
    StartDates = ValuationDate*ones(size(AssetPrice(Idx)));

    RateSpec = intenvset('StartDates', StartDates, 'EndDates', MaturityDate(Idx), 'Rates', Rates, 'Compounding', -1, 'Basis', 10);

    StockSpec = stockspec(Volatility, AssetPrice(Idx));    
    
    if isscalar(OptionType)
        OptionType = repmat(OptionType,size(Idx));
    end
    
    Output(Idx,1) = num2cell(impvbybls(RateSpec, StockSpec, StartDates, MaturityDate(Idx), OptionType(Idx), Strike(Idx), OptionPrice(Idx)));
    
    Output(MDN(Idx) <= VDN) = num2cell(0.0);
    
end