function varargout = User_Input_CheckBox_GUI(varargin)
% USER_INPUT_CHECKBOX_GUI MATLAB code for User_Input_CheckBox_GUI.fig
%      USER_INPUT_CHECKBOX_GUI, by itself, creates a new USER_INPUT_CHECKBOX_GUI or raises the existing
%      singleton*.
%
%      H = USER_INPUT_CHECKBOX_GUI returns the handle to a new USER_INPUT_CHECKBOX_GUI or the handle to
%      the existing singleton*.
%
%      USER_INPUT_CHECKBOX_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in USER_INPUT_CHECKBOX_GUI.M with the given input arguments.
%
%      USER_INPUT_CHECKBOX_GUI('Property','Value',...) creates a new USER_INPUT_CHECKBOX_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before User_Input_CheckBox_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to User_Input_CheckBox_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help User_Input_CheckBox_GUI

% Last Modified by GUIDE v2.5 29-Nov-2016 11:44:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @User_Input_CheckBox_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @User_Input_CheckBox_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before User_Input_CheckBox_GUI is made visible.
function User_Input_CheckBox_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to User_Input_CheckBox_GUI (see VARARGIN)

% Choose default command line output for User_Input_CheckBox_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes User_Input_CheckBox_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(handles.checkbox5,'Enable','off');

% --- Outputs from this function are returned to the command line.
function varargout = User_Input_CheckBox_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global QF1;global QF2; global QF3;global QF4;global QF5;

QF1 = get(handles.checkbox1,'Value');
QF2 = get(handles.checkbox2,'Value');
QF3 = get(handles.checkbox3,'Value');
QF4 = get(handles.checkbox4,'Value');
QF5 = get(handles.checkbox5,'Value');

if(QF1 || QF2 || QF3 || QF4 || QF5)
    
    PortfolioNames = {'QF1','QF2','QF3','QF4','QF5'};            %% Hard Code
    User_Select_Portfolio = {QF1,QF2,QF3,QF4,QF5};
    Selected_PortfolioNames = PortfolioNames(logical(cell2mat(User_Select_Portfolio)));
    
    Prompt        = {'Enter the Transaction Date(yyyy-mm-dd format):'};
    Name          = 'Date Input for PostgreSQL Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    close(gcf);
    hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');       %% Processing Waitbar
    
    ObjDB_PostGreSQL = connect_to_database_PostgreSQL;

    for i = 1 : length(Selected_PortfolioNames)
        
        Current_Portfolio = char(Selected_PortfolioNames(i));
        
        SqlQuery = ['select (trade_date at time zone ''SGT'') as trade_date,trade_price,trade_qty,identifier,portfolio_name,trader_name,broker_name,trade_side_name,product_name from middle_office.trades_history_all_portfolio where (trade_date at time zone ''SGT'')::date=''',char(InputDates),''' and portfolio_name =''',Current_Portfolio,''''];
        
        [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB_PostGreSQL,SqlQuery);
        
        if strcmpi(DBData,'No Data')
            errordlg(['No ',Current_Portfolio,' Data in DB'],Current_Portfolio);
            continue;
        end
        
        XLSFileName = getXLSFilename(strcat(upper(char(Current_Portfolio)),'_Query')) ;
        
        xlswrite(XLSFileName,[DBFieldNames;DBData],'TR-Data');
        xls_delete_sheets([pwd filesep XLSFileName]);
        
        waitbar(i/length(Selected_PortfolioNames),hWaitbar,[Current_Portfolio,' Finished']);
        
    end
    
    close(hWaitbar);
    
    msgbox('Process Complete','Finish');
    
else
    close(gcf);
    msgbox('Please Select the any one of the portfolio','Warning');
    
end


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6

global ALL;

ALL = get(handles.checkbox6,'Value');

if(ALL)
    
    set(handles.checkbox1,'Value',1);
    set(handles.checkbox2,'Value',1);
    set(handles.checkbox3,'Value',1);
    set(handles.checkbox4,'Value',1);
%     set(handles.checkbox5,'Value',1);
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
%     set(handles.checkbox5,'Enable','off');
else
    
    set(handles.checkbox1,'Value',0);
    set(handles.checkbox2,'Value',0);
    set(handles.checkbox3,'Value',0);
    set(handles.checkbox4,'Value',0);
%     set(handles.checkbox5,'Value',0);
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
%     set(handles.checkbox5,'Enable','on');
    
end

