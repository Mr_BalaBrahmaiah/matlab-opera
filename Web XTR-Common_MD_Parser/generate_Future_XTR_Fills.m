function [Future_TRHeader , Future_NettedTRData] = generate_Future_XTR_Fills(InBUName , Current_XTR_File_Path , OutXLSFileName)

try  
    Future_TRHeader = {''};
    Future_NettedTRData = [];
    
    Master_Path = [pwd filesep 'Master Files' filesep];
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%%% XTR Traders Data 
    [~,~,RawData] = xlsread(char(Current_XTR_File_Path));  %% input data

    RawData(:,1) = [];  %%% delete first col
    Raw_Data = RawData(2:end,:);
    if isempty(Raw_Data)        %%% Checking condition? data is found or not
        warndlg('No data found in selected Traders file!') ;
        return;
    end

    %%%% Remove Blanks colomns @ Strike
    IdxNaNRows = [];
    IdxNaN = cellfun(@isnan,RawData(:,9),'UniformOutput',false);
    for iRNaN = 1:length(IdxNaN)
        if any(IdxNaN{iRNaN})
            IdxNaNRows = [IdxNaNRows; iRNaN];
        end
    end
    RawData(IdxNaNRows,:) = [];
    
    RawData_Header = RawData(1,:);
    RawData = cell2dataset(RawData);

    RawData.ExchTime = datestr(RawData.ExchTime,'HH:MM:SS');
    
    %%% Filter the data using Alias_Master Table
    [~,~,Alias_Master_Data] = xlsread([Master_Path filesep 'XTRFills_Broker_CurrentUser_Master.xlsx']);

    Alias_Master_Data = cell2dataset(Alias_Master_Data);
    if isempty(Alias_Master_Data)           %%% Checking condition? data is found or not
        warndlg('No data found in XTRFills_Broker_Alias_Master file!') ;
        return;
    end

    %%% Check Alias
    Index = strcmpi(InBUName,Alias_Master_Data.BusinessGroup);
    Alias_Data=unique(Alias_Master_Data.CurrentUser(Index),'rows','stable');

    Raw_Alias_Data=[];
    [r,~]=size(Alias_Data);
    for i=1:1:r
        field=Alias_Data(i);
        Matched_Data = RawData(strncmpi(field,RawData.CurrentUser,length(char(field))),:);
        if isempty(Matched_Data)
            continue;
        end
        Raw_Alias_Data = vertcat(Raw_Alias_Data,Matched_Data);
    end

    if isempty(Raw_Alias_Data)
        warndlg(['No ',upper(char(InBUName)),' Trades for Corresponding Date']) ;
        return;
    end 

    %%% Header and Data
    RawData_Alias_Data = dataset2cell(Raw_Alias_Data);
    Header = RawData_Alias_Data(1,:);
    
    %%% Filter the only Future trades
    PosFillType = strcmpi('FillType',Header);
    FillType = RawData_Alias_Data(:,PosFillType);
    PosproType = strcmpi('ProdType',Header);
    ProType = RawData_Alias_Data(:,PosproType);
    IdxProdType = strcmpi(ProType, 'Future');% & strcmpi(FillType, 'Leg');
    Data = RawData_Alias_Data(IdxProdType,:);
    Data(:,PosFillType) = {'future'};
    
    %%Read Missing Deals Master File
    if(strcmpi(InBUName,'agf'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'orx'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'ORYX_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'cfs'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'usg'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'cop'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'COP_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'ogp'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'OGP_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    elseif(strcmpi(InBUName,'og2'))
        [~,~,AccProdMapping] = xlsread([Master_Path 'OG2_MissingDeals_Master.xlsx'],'XTR Fills Conversion');
    end
    
    %%Remove NaN Fields
    Header1 = AccProdMapping(1,:);
    NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header1);  %% Nan_Col = find(Nan_Col);
    AccProdMapping(:,NaN_Col(:)) = [];
    
    Row_find_Nan = AccProdMapping(:,4);
    NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
    AccProdMapping(NaN_Row(:),:) = [];
    
    Master_Header = AccProdMapping(1,:); 
    AccProdMapping = AccProdMapping(2:end,:);
    
    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN         = cellfun(@isnan,AccProdMapping(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    AccProdMapping(IdxNaNRows,:) = [];
    
    AccProdMapping = cell2dataset([Master_Header ; AccProdMapping]);
    
    %%% Mapping the two tables (XTR Fills Conversion Futures and Input RawData
    %%% Table and get the matched data based on Account number and product code)
    Data_dataset = cell2dataset([Header ; Data]);
    [r1,~] = size(Data);
    Raw_AccPro_Data_Group=[]; Missing_Account_Number = [];
    for i1=1:1:r1
        field_product=Data_dataset.Product(i1);
        field_Acc_numb=Data_dataset.Account(i1);         
        Indx=find(strcmpi(field_Acc_numb,AccProdMapping.XTRAccountNo) & strcmpi(field_product, AccProdMapping.ProductCode));
        if isempty(Indx)
            Missing_Account_Number = unique([Missing_Account_Number,field_Acc_numb]);
            continue;
        end
        
        Raw_Matched_Data = AccProdMapping(Indx,:);
        if ~isempty(Raw_Matched_Data)
            str2=Raw_Matched_Data.XTRAccountNo;
            index=find(strcmp(str2,Data_dataset.Account));
            if (index)
                Unique_Group_Data=dataset2cell(Data_dataset(i1,:));
                Matched_RawData1=[Raw_Matched_Data.Description Raw_Matched_Data.PremiumDivBy8 Raw_Matched_Data.PriceConversion Unique_Group_Data(2,:)];
                Raw_AccPro_Data_Group=vertcat(Raw_AccPro_Data_Group,Matched_RawData1);
            end
        end          
    end

    if isempty(Raw_AccPro_Data_Group)
        warndlg(['No ',upper(char(InBUName)),' Trades are matching with MissingDeals_Master-Future.xlsx-XTR Fills Conversion file']) ;
        return;
    end

    ErrorAccnum = (unique(Missing_Account_Number));
    if(strcmpi(InBUName,'agf'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' Future-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(in XTR-Fills Conversion and in XTR-TR Format Master sheets) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' Future-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
     elseif(strcmpi(InBUName,'cfs'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' Future-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' Future-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
    elseif(strcmpi(InBUName,'usg'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' Future-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' Future-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
   elseif(strcmpi(InBUName,'ogp'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' Future-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' Future-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
   elseif(strcmpi(InBUName,'og2'))
        if(~isempty(ErrorAccnum))
            warndlg([upper(InBUName),' Future-XTR Fills file contain the Account Number ',ErrorAccnum,' which is not available in Master file. Please update this account number in master file(MMVT Missing Deals Master) to get these account deals'],upper(InBUName));
        else
            warndlg([upper(InBUName),' Future-XTR Fills file Account Number :',' NO NEW SHORT CODE FOUND ',],upper(InBUName));
        end
    end

    %%%% Header and Data for after matching MissingDeals_Master-Future Table
    Raw_Master_Header = [Master_Header(3) Master_Header(4) Master_Header(5) RawData_Header];
    Raw_AccPro_Master_Data = [Raw_Master_Header ; Raw_AccPro_Data_Group];
    
    Raw_Master_Data_All = cell2dataset(Raw_AccPro_Master_Data);

    %%% User required data fields
    Raw_Master_user_Data = [Raw_Master_Data_All.Description ,num2cell(Raw_Master_Data_All.PremiumDivBy8), num2cell(Raw_Master_Data_All.PriceConversion), ...
                        Raw_Master_Data_All.Broker, Raw_Master_Data_All.CurrentUser, Raw_Master_Data_All.Product, Raw_Master_Data_All.Contract, ...
                        num2cell(Raw_Master_Data_All.Strike), num2cell(Raw_Master_Data_All.P_C), Raw_Master_Data_All.ProdType, ...
                        Raw_Master_Data_All.B_S, num2cell(Raw_Master_Data_All.FillQty), num2cell(Raw_Master_Data_All.Price), Raw_Master_Data_All.Account, ...
                        Raw_Master_Data_All.ExchDate, num2cell(Raw_Master_Data_All.ExchTime), Raw_Master_Data_All.CurrentUser, ...
                        Raw_Master_Data_All.CurrentUser, Raw_Master_Data_All.ExchDate, num2cell(Raw_Master_Data_All.ExchTime)];
                    
    %%% User required header fields
    Raw_Master_user_Header = {'Description', 'PremiumDivBy8', 'PriceConversion', 'Broker', 'CurrentUser', 'Product', ...
                                'Contract', 'Strike', 'C_P', 'ProductType', 'B_S', 'Qty', 'Price', 'Account', ...
                                'ExchDate', 'ExchTime', 'TrdID', 'Username', 'OrderDate', 'OrderTime'};
    
     Raw_Master_user_Data = cell2dataset([Raw_Master_user_Header;Raw_Master_user_Data]); %%% convert cell to dataset
     
     %%% Product Master @ XTR
    if(strcmpi(InBUName,'cfs'))
        [~,~,ProductMaster] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'agf'))
        [~,~,ProductMaster] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'usg'))
        [~,~,ProductMaster] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'ogp'))
        [~,~,ProductMaster] = xlsread([Master_Path 'OGP_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    elseif(strcmpi(InBUName,'og2'))
        [~,~,ProductMaster] = xlsread([Master_Path 'OG2_MissingDeals_Master.xlsx'],'XTR-TR Format Master');
    else
        warndlg([upper(char(InBUName)),'_MissingDeals_Master-Future.xlsx is not found']) ;
        return;
    end

    ProductMaster_Data = ProductMaster(2:end,:);
    if isempty(ProductMaster_Data)           %%% Checking condition? data is found or not
        warndlg(['No data found in ',upper(char(InBUName)),'_MissingDeals_Master-Future.xlsx-XTR-TR Format Master File!']) ;
        return;
    end

    %%%  Remove NaN Fields
    Header = ProductMaster(1,:);
    NaN_Col =  cellfun(@(V) any(isnan(V(:))), Header);  %% Nan_Col = find(Nan_Col);
    ProductMaster(:,NaN_Col(:)) = [];

    Row_find_Nan = ProductMaster(:,4);
    NaN_Row =  cellfun(@(V) any(isnan(V(:))), Row_find_Nan); %% Nan_Row = find(Nan_Row);
    ProductMaster(NaN_Row(:),:) = [];

    Pro_Master_Header1 = ProductMaster(1,:);
    ProductMaster = ProductMaster(2:end,:);

    % To remove the lines of xl whcih contains NaN in all columns; the data
    % which gets removed from xl, but the row remains still
    IdxNaNRows = [];
    IdxNaN  = cellfun(@isnan,ProductMaster(:,1),'UniformOutput',false);
    for iR = 1:length(IdxNaN)
        if any(IdxNaN{iR})
            IdxNaNRows = [IdxNaNRows; iR];
        end
    end
    ProductMaster(IdxNaNRows,:) = [];

    ProductMaster = cell2dataset([Pro_Master_Header1;ProductMaster]);
     
    %%% Mapping the two tables (XTR-TR Format Master and Input RawData
    %%% Table and get the matched data)
    Raw_Product_Data=[];
    [rr,~]=size(Raw_Master_user_Data);
    for i1=1:1:rr
        field_Acc_numb1=Raw_Master_user_Data.Account(i1);  
        field_descr=Raw_Master_user_Data.Description(i1);
        Indx1=strcmpi(field_Acc_numb1,ProductMaster.AccountNumber) & strcmpi(field_descr,ProductMaster.ProductDescription);
        Raw_pro_Matched_Data = ProductMaster(Indx1,:);
        if ~isempty(Raw_pro_Matched_Data)
            Master_user_want_Data = [Raw_pro_Matched_Data.Portfolio Raw_pro_Matched_Data.Instrument Raw_pro_Matched_Data.ProductCode Raw_pro_Matched_Data.Strike_div Raw_pro_Matched_Data.Prem_div];
            Unique_Group_Data1=dataset2cell(Raw_Master_user_Data(i1,:));
            Raw_pro_Matched_Data1=[Master_user_want_Data Unique_Group_Data1(2:end,:)];
            Raw_Product_Data=vertcat(Raw_Product_Data,Raw_pro_Matched_Data1);
        end  
    end

    if isempty(Raw_Product_Data)
        warndlg(['No ',upper(char(InBUName)),' Trades are matching with MissingDeals_Master-Future.xlsx-XTR-TR Format Master File!']) ;
        return;
    end 

    %%%% Header and Data for after matching Product_Master Table
    Raw_pro_Master_Header=[Pro_Master_Header1(1) Pro_Master_Header1(2) Pro_Master_Header1(5) Pro_Master_Header1(6) Pro_Master_Header1(7) Raw_Master_user_Header];
    Raw_Pro_Master_TR_Data = [Raw_pro_Master_Header ; Raw_Product_Data];

    %consolidatedata -- Lots (Qty)
    ResetFutureTRData =Raw_Pro_Master_TR_Data;
    ResetFutureTRData_Header =  ResetFutureTRData(1,:);
    ResetFutureTRData = ResetFutureTRData(2:end,:);

    xlswrite(OutXLSFileName,[ResetFutureTRData_Header ; ResetFutureTRData],'Overall Data');
    
    UniqueFields = {'Portfolio','Instrument', 'Account','ProductCode','Description','PremiumDivBy8','PriceConversion','Broker','Contract','Strike','C_P','B_S','Price','TrdID'}; %added Contract as UniqueField on 23-01-2020
    SumFields = {'Qty'};
    OutputFields = ResetFutureTRData_Header ;
    WeightedAverageFields = [];
    [ResetFutureTRData_Header,ResetFutureTRData_Consolidate] = consolidatedata(ResetFutureTRData_Header, ResetFutureTRData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    %%% final user required data 
    Final_FutureTRData = [ResetFutureTRData_Header;ResetFutureTRData_Consolidate];
    
    %%% Counterparty Mapping
    if strcmpi(char(InBUName),'agf')
        [~,~,ConsoMapping] = xlsread([Master_Path 'AGF_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'usg')
        [~,~,ConsoMapping] = xlsread([Master_Path 'USG_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'cfs')
        [~,~,ConsoMapping] = xlsread([Master_Path 'MMVT_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'ogp')
        [~,~,ConsoMapping] = xlsread([Master_Path 'OGP_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    elseif strcmpi(char(InBUName),'og2')
        [~,~,ConsoMapping] = xlsread([Master_Path 'OG2_MissingDeals_Master.xlsx'],'Counterparty Mapping');
    end   
    
   Nan_Row =  cellfun(@(V) any(isnan(V(:))), ConsoMapping(:,1)); %% Nan_Row = find(Nan_Row);
   ConsoMapping(Nan_Row(:),:) = [];
   ConsoMapping = ConsoMapping(2:end,:);
    
    Unique_NettedTR = unique(Final_FutureTRData(2:end,1))';
    Unique_ConsoNetted = unique(ConsoMapping(:,1))';
    Portfolio_MatchedIndex = cellStrfind_Exact(Unique_NettedTR,Unique_ConsoNetted);
    
    Unique_NettedTR(Portfolio_MatchedIndex) = [];
    
    if(~isempty(Unique_NettedTR))
        warndlg(['Conso Mapping Portfolio',Unique_NettedTR,' Which is not available in Master File(in Conso Mapping). Please update this account number in master file(in Conso Mapping Sheet) to get these account deals'],'MD Parser');
    end
    
    ConsoHeader = Final_FutureTRData(1,:);
    Conso_NettedTRData = Final_FutureTRData(2:end,:);
    
    NettedTRData_Portfolio = strtrim(Conso_NettedTRData(:,1));
    NettedTRData_CounterParty = strtrim(Conso_NettedTRData(:,9));
    NettedTRData_Account = strtrim(Conso_NettedTRData(:,19));
    
    %%% To get the counterparty, the Mapping between Counterparty Mapping data and RawData
    ConsoNew_NettedTRData = [];
    Matched_Portfolio = [];
    Not_MatchedPortfolio = [];
    for k = 1 : length(ConsoMapping(:,1))
        ConsoMapping_Portfolio = strtrim(ConsoMapping{k,1});
        ConsoMapping_Counterparty = strtrim(ConsoMapping{k,2});
        ConsoMapping_Account = strtrim(ConsoMapping{k,4});
        
        MatchedIndex = find(strcmpi(ConsoMapping_Portfolio, NettedTRData_Portfolio) & strcmpi(ConsoMapping_Account, NettedTRData_Account) & ...
            strncmpi(ConsoMapping_Counterparty, NettedTRData_CounterParty,length(ConsoMapping_Counterparty)));
        if(~isempty(MatchedIndex))
            Temp_Conso = Conso_NettedTRData(MatchedIndex,:);
            Temp_Conso(:,9) =  {strtrim(ConsoMapping{k,3})};
            ConsoNew_NettedTRData = [ConsoNew_NettedTRData ; Temp_Conso];
            
            Temp_Conso = [];
            Matched_Portfolio =  [Matched_Portfolio ; {ConsoMapping_Portfolio}];
        else
            Not_MatchedPortfolio = [Not_MatchedPortfolio ; {ConsoMapping_Portfolio}];
        end 
    end
    
    Conso_FutureTRData = [ConsoHeader ; ConsoNew_NettedTRData] ;
    FutureTRData = cell2dataset(Conso_FutureTRData);
    
     %%% Final User Requirement Process
    [row,~]=size(FutureTRData);
    for i = 1:1:row
        Future_Portfolio(i,1) = FutureTRData.Portfolio(i);
        Future_Counterparty(i,1) = FutureTRData.Broker(i); 

        instrument = FutureTRData.Instrument(i);
        Future_Instrument(i,1) = cellstr([char(instrument)]);

        ExeBroker(i,1) = FutureTRData.Broker(i);
        Source(i,1) = {'XTR Fills'};
        ExeType(i,1) ={'Elec'};
        Rates(i,1) = {'Normal'};

        Buy_sell = FutureTRData.B_S(i);
        if strcmpi(Buy_sell,'S')
            BuySell(i,1) = {'Sold'};
        elseif strcmpi(Buy_sell,'B')
            BuySell(i,1) = {'Bought'};
        else
            BuySell(i,1) = {''};
        end

        Future_type = FutureTRData.C_P(i);
        if strcmpi(Future_type,'C')
            OptType(i,1) = {'Vanilla_Call'};
        elseif strcmpi(Future_type,'P')
            OptType(i,1) = {'Vanilla_Put'};
        else
            OptType(i,1) = {'future'};  
        end

        Future_Contract=FutureTRData.Contract(i);
        Future_TradePrice=FutureTRData.Price(i);
        Future_PriceConversion=FutureTRData.PriceConversion(i);

        Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
        MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
        YearMonth = char(Future_Contract);
        CYrMon = datestr(datenum(cellstr(YearMonth(:,end-4:end)),'mmmyy'),'mmm-yy');
        if CYrMon(end) == '0' || CYrMon(end) == '1' || CYrMon(end) == '2' || CYrMon(end) == '3' || CYrMon(end) == '4' || CYrMon(end) == '5' || CYrMon(end) == '6'
            Contr_YearMonthCode = cellstr([MonthCode{strncmpi(CYrMon,Month,3)},CYrMon(end-1:end)]);
        else
           Contr_YearMonthCode = cellstr([MonthCode{strncmpi(CYrMon,Month,3)},CYrMon(end)]); 
        end
        Future_Month(i,1)=strcat(Contr_YearMonthCode,{'.'},Contr_YearMonthCode); 

        %%%% Primium Calculations  %%%% Start
        PremiumDivBy8 = FutureTRData.PremiumDivBy8(i);
        if PremiumDivBy8 == 0
            Temp   = Future_TradePrice;
            NewTradePrice = Temp .* Future_PriceConversion;
        else 
            %%%% Primium Calculations  %%%% Start
            Temp   = num2str(Future_TradePrice);
            TempNo = str2num(Temp(1:end-1)); %#ok<*ST2NM>
            if isempty(TempNo)
                TempNo=0;
            end        
            NewTradePrice = TempNo + (str2num(Temp(end))/8);
            NewTradePrice = NewTradePrice .* Future_PriceConversion;
        end

        Premium_div = FutureTRData.Prem_div(i);
        TempPremium = NewTradePrice;
        if ~isnan(TempPremium) && isnumeric(TempPremium)
            TempPremium = TempPremium ./ Premium_div;
        end

        Strike = FutureTRData.Strike(i);
        StrikeConv = FutureTRData.Strike_div(i);
       if PremiumDivBy8 == 0
            Temp   = Strike;
            Strike = Temp * Future_PriceConversion;
       else
           Strike = Strike;
       end
          
        TempStrike = Strike;
        if ~isnan(TempStrike) && isnumeric(TempStrike)
            TempStrike = TempStrike ./ StrikeConv;
        end

        if isempty(TempStrike)
            Future_Strike(i,1) = num2cell(NaN);
        else
            Future_Strike(i,1) = num2cell(TempStrike);
        end

        if isempty(TempPremium) || isnan(TempPremium)
            Future_Premium(i,1) = num2cell(0);
        else
            Future_Premium(i,1) = num2cell(TempPremium);
        end
    end

    %%Transaction Number
    TempTradeId = '';
    try
    [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
    if strcmpi(OutErrorMsg,'No Errors')
        ObjDB = connect_to_database;
        SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
        TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
        TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));

        if(isempty(TempTradeId))
            TempTradeId = 0;      %% Default
        end
    end
    catch
        TempTradeId = '';
    end

    NumRows1 = size(FutureTRData,1); % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
    NTradeId = (TempTradeId+1:1:TempTradeId+NumRows1)';
    TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
    TradeId = strrep(TradeId,' ','');  %% Remove Spaces

    TRN_Number	= TradeId;
    Parent_Transaction_Number	= TradeId;

    Idxsold = strcmpi(BuySell,'Sold');
    IdxBrought = strcmpi(BuySell,'Bought');
    Counterparty = FutureTRData.Broker;
    Initial_Lots	= FutureTRData.Qty;

    Initial_Lots(Idxsold) = Initial_Lots(Idxsold) .* -1;
    Initial_Lots(IdxBrought) = Initial_Lots(IdxBrought) .* 1;

    ExchDate = FutureTRData.ExchDate;
    if isdeployed
        Transaction_Date = cellstr(datetime(ExchDate));
    else
        Transaction_Date = ExchDate;
    end
    Trader_Name	= FutureTRData.Username;            %Trd ID

    OrderDate = FutureTRData.OrderDate;
    OrderTime = FutureTRData.OrderTime;

    Traded_DateTimestamp = strcat(OrderDate,{' '},OrderTime);  %Order Date & Order Time

    CONTRACTNB	= cell(size(FutureTRData,1),1);
    Maturity	= cell(size(FutureTRData,1),1);
    Barrier_Type	= cell(size(FutureTRData,1),1);
    Barrier	= cell(size(FutureTRData,1),1);
    Averaging_Start_Date	= cell(size(FutureTRData,1),1);
    Averaging_End_Date	= cell(size(FutureTRData,1),1);
    Spcl_Rate_lot	= cell(size(FutureTRData,1),1);
    Other_Remarks	= cell(size(FutureTRData,1),1);
    OTC_DAILY_EOD_LEG	= cell(size(FutureTRData,1),1);

    %%%  Final user TR_Dump Header and Data
    Future_TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Initial Lots',...
                    'CONTRACTNB','Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source',...
                    'Averaging Start Date','Averaging End Date','Parent Transaction Number','Exe-Broker','Exe Type','Rates',...
                    'Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'}; 


    Future_NettedTRData  = [Future_Instrument, Future_Portfolio, BuySell, Future_Month, Future_Strike, OptType, Future_Premium, Counterparty, ...
                            num2cell(Initial_Lots), CONTRACTNB, Maturity, Barrier_Type, Barrier, Transaction_Date, TRN_Number, Source, Averaging_Start_Date, ...
                            Averaging_End_Date, Parent_Transaction_Number, ExeBroker, ExeType, Rates, Spcl_Rate_lot, Other_Remarks, OTC_DAILY_EOD_LEG, ...
                            Trader_Name, Traded_DateTimestamp];

catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end
