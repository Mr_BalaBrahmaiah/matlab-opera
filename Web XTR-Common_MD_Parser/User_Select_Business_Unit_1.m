function varargout = User_Select_Business_Unit_1(varargin)
% USER_SELECT_BUSINESS_UNIT_1 MATLAB code for User_Select_Business_Unit_1.fig
%      USER_SELECT_BUSINESS_UNIT_1, by itself, creates a new USER_SELECT_BUSINESS_UNIT_1 or raises the existing
%      singleton*.
%
%      H = USER_SELECT_BUSINESS_UNIT_1 returns the handle to a new USER_SELECT_BUSINESS_UNIT_1 or the handle to
%      the existing singleton*.
%
%      USER_SELECT_BUSINESS_UNIT_1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in USER_SELECT_BUSINESS_UNIT_1.M with the given input arguments.
%
%      USER_SELECT_BUSINESS_UNIT_1('Property','Value',...) creates a new USER_SELECT_BUSINESS_UNIT_1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before User_Select_Business_Unit_1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to User_Select_Business_Unit_1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help User_Select_Business_Unit_1

% Last Modified by GUIDE v2.5 22-Jan-2020 15:42:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @User_Select_Business_Unit_1_OpeningFcn, ...
    'gui_OutputFcn',  @User_Select_Business_Unit_1_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before User_Select_Business_Unit_1 is made visible.
function User_Select_Business_Unit_1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to User_Select_Business_Unit_1 (see VARARGIN)

% Choose default command line output for User_Select_Business_Unit_1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes User_Select_Business_Unit_1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = User_Select_Business_Unit_1_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

AGF = get(handles.checkbox1,'Value');
if(AGF)
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

ORX = get(handles.checkbox2,'Value');
if(ORX)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

CFS = get(handles.checkbox3,'Value');
if(CFS)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4

COP = get(handles.checkbox4,'Value');
if(COP)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5

USG = get(handles.checkbox5,'Value');
if(USG)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6

OGP = get(handles.checkbox6,'Value');
if(OGP)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox7,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox7,'Enable','on');
end


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7

OG2 = get(handles.checkbox7,'Value');
if(OG2)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox4,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox6,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox4,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox6,'Enable','on');
end

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

global AGF;global ORX; global CFS;global COP;global USG;global OGP;global OG2;

AGF = get(handles.checkbox1,'Value');
ORX = get(handles.checkbox2,'Value');
CFS = get(handles.checkbox3,'Value');
COP = get(handles.checkbox4,'Value');
USG = get(handles.checkbox5,'Value');
OGP = get(handles.checkbox6,'Value');
OG2 = get(handles.checkbox7,'Value');

if(AGF || ORX || CFS || COP || USG || OGP || OG2)
    Business_Units = {'AGF','ORX','CFS','COP','USG','OGP','OG2'};            %% Hard Code
    User_Select_BU = {AGF,ORX,CFS,COP,USG,OGP,OG2};
    Selected_Business_Unit = Business_Units(logical(cell2mat(User_Select_BU)));
    
%     LoopCount = get(handles.edit1,'String');
%     LoopCount = str2double(LoopCount);
    DefaultPath = pwd;
    for i = 1 : length(Selected_Business_Unit)  %% LoopCount        
        [XTR_FileName,XTR_PathName]  = uigetfile('*.csv',['Select the ', char(Selected_Business_Unit),' XTR Fills file ',num2str(i)],DefaultPath);
        DefaultPath = XTR_PathName;
        if(isnumeric(DefaultPath))
            DefaultPath = pwd;
        end
        if isnumeric(XTR_FileName) && XTR_PathName==0
            errordlg('XTR Fills file statement not selected!','Missing Deals Parser');
        else
            Excel_Path{i} = [XTR_PathName,XTR_FileName];
        end
    end
    
    OutXLSFileName = getXLSFilename(upper(char(Selected_Business_Unit)));
    xlswrite(OutXLSFileName,{''},'Netted_TR_Data');
    
    for i = 1 : length(Selected_Business_Unit)
        Current_Business_Unit = char(Selected_Business_Unit);
        %%%% Future XTR Fills
        [~ , Future_NettedTRData] = generate_Future_XTR_Fills(lower(Current_Business_Unit) , Excel_Path , OutXLSFileName);

        %%%% Option XTR Fills
        [~ , Option_NettedTRData] = generate_Option_XTR_Fills(lower(Current_Business_Unit) , Excel_Path , OutXLSFileName);

        NettedTRData = [Future_NettedTRData ; Option_NettedTRData];
        
        %%Transaction Number
        TempTradeId = '';
        try
        [OutErrorMsg,DBTradeId]= getLastTradeId(lower(Current_Business_Unit));
        if strcmpi(OutErrorMsg,'No Errors')
            ObjDB = connect_to_database;
            SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
            TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
            TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));

            if(isempty(TempTradeId))
                TempTradeId = 0;      %% Default
            end
        end
        catch
            TempTradeId = '';
        end

        NumRows1 = size(NettedTRData,1); % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
        NTradeId = (TempTradeId+1:1:TempTradeId+NumRows1)';
        TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
        TradeId = strrep(TradeId,' ','');  %% Remove Spaces
    
        NettedTRData(:,15) = TradeId;
        NettedTRData(:,19) = TradeId;
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Initial Lots',...
                    'CONTRACTNB','Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source',...
                    'Averaging Start Date','Averaging End Date','Parent Transaction Number','Exe-Broker','Exe Type','Rates',...
                    'Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'}; 
                
        if ~isempty(NettedTRData)
            xlswrite(OutXLSFileName,[TRHeader ; NettedTRData],'Netted_TR_Data');
        end
        %%%% Delete Empty sheets and active user sheet
        xls_delete_sheets(fullfile(pwd,OutXLSFileName));
            
    end
    Job_Done(jObj);
    msgbox('Process Complete','Finish');
else
    close(gcf);
    msgbox('Please Select the any one of the Business Unit','Warning');
end

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
