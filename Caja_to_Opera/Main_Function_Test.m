clc
clear

%% ODBC Local System
% tic;
% ObjDB = database('dbsystemUAT','user','invenio@123');
% 
% SqlQuery = 'select * from underlying_settle_value_table';
% [DBFieldNames,DBData1] = Fetch_DB_Data(ObjDB,SqlQuery);
% toc;

%% JDBC
% tic;
% ObjDB = connect_to_database;
% 
% SqlQuery = 'select * from underlying_settle_value_table';
% [DBFieldNames,DBData2] = Fetch_DB_Data(ObjDB,SqlQuery);
% toc;

%% ODBC SQL ANYWHERE

datasource = 'OlamInca';
conn = database(datasource,'testcon','sql');%% Check Message

% FUT_SqlQuery  = '{call dba.sp_derivatives_futures_status(''2017-08-17'')}';
% OPT_SqlQuery  = '{call dba.sp_derivatives_options_status(''2017-08-17'')}';

FUT_SqlQuery  = '{call dba.sp_derivatives_futures_opera(''2017-08-29'')}';
OPT_SqlQuery  = '{call dba.sp_derivatives_options_opera(''2017-08-29'')}';


[DBFieldNames,DBData] = Fetch_DB_Data(conn,FUT_SqlQuery);
if(strcmpi(DBData,'No Data'))
    DBData = '';
end

XLS_Name = 'Test.xlsx';
xlswrite(XLS_Name,[DBFieldNames;DBData],'FUT');

[DBFieldNames,DBData] = Fetch_DB_Data(conn,OPT_SqlQuery);
if(strcmpi(DBData,'No Data'))
    DBData = '';
end
xlswrite(XLS_Name,[DBFieldNames;DBData],'OPT');


msgbox('Finish','Finish');
