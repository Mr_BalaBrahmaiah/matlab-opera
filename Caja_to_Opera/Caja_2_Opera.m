clc;
clear;


try
    ObjDB = connect_to_database;
    
    %     Value_Date = fetch(ObjDB,'select value_date from valuation_date_table');
    %     Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    Prompt        = {'Enter the Settle Date(yyyy-mm-dd format):'};
    Name          = 'Date Input for Caja Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    Settle_Date    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    %% OPERA
    InBUName = 'coc';
    ViewName = strcat('deal_ticket_table_with_latest_versions_view_',InBUName);
    SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where ops_unique like ''CJ%'' '];
    
    [OperaFields , Opera_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    
    Opera_OpsUnique_Col = cellStrfind_exact(OperaFields,{'ops_unique'});
    Opera_TradeID_Col = cellStrfind_exact(OperaFields,{'trade_id'});
    
    %% SQL AnyWhere11
        datasource = 'OlamInca'; %% Main DB
        ObjDB_SqlAny11 = database(datasource,'testcon','sql');%% Check Message
    
%     datasource = 'Amazon Olam Inca'; %% Test DB
%     ObjDB_SqlAny11 = database(datasource,'testcon','sql');%% Check Message
    
    %         FUT_SqlQuery  = strcat('{call dba.sp_derivatives_futures_status(''',char(Settle_Date),''')}'); %% Murex Stored Procedure
    %         OPT_SqlQuery  = strcat('{call dba.sp_derivatives_options_status(''',char(Settle_Date),''')}'); %% Murex Stored Procedure
    
    FUT_SqlQuery  = strcat('{call dba.sp_derivatives_futures_opera(''',char(Settle_Date),''')}');
    OPT_SqlQuery  = strcat('{call dba.sp_derivatives_options_opera(''',char(Settle_Date),''')}');
    
    
    [DBFieldNamesFUT,DBDataFUT] = Fetch_DB_Data(ObjDB_SqlAny11,FUT_SqlQuery);
    if(strcmpi(DBDataFUT,'No Data'))
        DBDataFUT = '';
    end
    
    [DBFieldNamesOPT,DBDataOPT] = Fetch_DB_Data(ObjDB_SqlAny11,OPT_SqlQuery);
    if(strcmpi(DBDataOPT,'No Data'))
        DBDataOPT = '';
    end
    
    OverallData_SqlAny11 = [DBDataFUT ; DBDataOPT];
    
    OPSUnique_Col = cellStrfind_exact(DBFieldNamesFUT,{'OPS Unique'});
    OPSAction_Col = cellStrfind_exact(DBFieldNamesFUT,{'OPS Action'});
    TRN_Number_Col = cellStrfind_exact(DBFieldNamesFUT,{'TRN.Number'});
    Parent_TRN_Col = cellStrfind_exact(DBFieldNamesFUT,{'Parent Transaction Number'});
    
    
    %%
    
    if(~isempty(OverallData_SqlAny11))
        
        New_Data = [];
        Existing_Data = [];
        Overall_Del_Data = [];
        
        for i = 1 : size(OverallData_SqlAny11,1)
            Current_OPSUnique = OverallData_SqlAny11(i,OPSUnique_Col) ;
            
            Matched_Index = cellStrfind_exact(Opera_Data(:,Opera_OpsUnique_Col),Current_OPSUnique);
            
            if(isempty(Matched_Index))
                Current_NewData = OverallData_SqlAny11(i,:) ;
                
                Del_Index = cellStrfind_exact(Current_NewData(:,OPSAction_Col),{'Deleted'}); %% Get Delete Index and Ignore
                if(~isempty(Del_Index))
                    continue;
                else
                    Current_NewData(:,OPSAction_Col) = {'NEW'}; %% Modified and New
                    
                    New_Data = [New_Data ; Current_NewData];
                end
            else
                Current_ExistingData = OverallData_SqlAny11(i,:)  ;
                
                Del_Index = cellStrfind_exact(Current_ExistingData(:,OPSAction_Col),{'Deleted'}); %% Get Delete Index
                if(~isempty(Del_Index))
                    [DBFieldNames_Del , DBData_Del] = Update_2_Database(Current_ExistingData,InBUName);
                    Overall_Del_Data = [Overall_Del_Data ;  DBData_Del];
                else
                    Current_ExistingData(:,TRN_Number_Col) = Opera_Data(Matched_Index,Opera_TradeID_Col);
                    Current_ExistingData(:,Parent_TRN_Col) = Opera_Data(Matched_Index,Opera_TradeID_Col);
                    Current_ExistingData(:,OPSAction_Col) = {'MOD'} ;
                    
                    Existing_Data = [Existing_Data ; Current_ExistingData];
                end
            end
            
        end
        
        
        %% Get NewTrade ID
        
        if(~isempty(New_Data))
            [TradeId , Last_TradeId] = getTradeID_4_Cell_Array(New_Data,InBUName) ;
            
            New_Data(:,TRN_Number_Col) = TradeId;
            New_Data(:,Parent_TRN_Col) = TradeId;
            
        end
        %% Overall Data
        
        OverallData = [New_Data ; Existing_Data];
        
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
            'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
            'OPS Unique','BROKER LOTS','OPS Action'};
        
        NaN_Col = {'CONTRACTNB','Maturity','Barrier Type','Barrier','Averaging Start Date','Averaging End Date',...
            'Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
        NaN_Col_Index = cellStrfind_exact(TRHeader,NaN_Col);
        
        OverallData(:,NaN_Col_Index) = {''};
        
        %% Excel Write
        
        OutXLSName = getXLSFilename([datestr(Settle_Date,1),'_Caja_TR_Format']);
        
        xlswrite(OutXLSName,[TRHeader;OverallData],'Caja_TR_Format');
        xlswrite(OutXLSName,[DBFieldNamesFUT;DBDataFUT],'Caja FUT');
        xlswrite(OutXLSName,[DBFieldNamesOPT;DBDataOPT],'Caja OPT');
        
        if(~isempty(Overall_Del_Data))
            xlswrite(OutXLSName,[DBFieldNames_Del;Overall_Del_Data],'Caja Delete in Opera');
        end
        OutXLSName = fullfile(pwd,OutXLSName);
        xls_delete_sheets(OutXLSName);
        xls_change_activesheet(OutXLSName,'Caja_TR_Format');
        
    else
        
        msgbox('No Data for Particular Settle Date in SQL Anywhere 11')  ;
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

msgbox('Finish');