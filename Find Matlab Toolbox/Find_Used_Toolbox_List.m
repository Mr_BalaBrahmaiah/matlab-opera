tic
fileList = getAllFiles('D:\Works\DB2MurexMigration');

id =endsWith(fileList,'.m');
required_list =fileList(id);
tfList =[];
tpList =[];
for k=1:length(required_list)      
     [fList,pList] = matlab.codetools.requiredFilesAndProducts(required_list{k});
     tfList=[tfList,fList];
     
     da =struct2cell(pList);
     for d = 1:size(da,3)
       db{d,1} =da{1,d};
     end
     tpList=[tpList;db];
     clear db;
     
end
xlswrite('toolboxused2.xlsx',tpList) %% Matlab Toolbox
xlswrite('toolboxused3.xlsx',tfList') %% .m file names with location path
toc
