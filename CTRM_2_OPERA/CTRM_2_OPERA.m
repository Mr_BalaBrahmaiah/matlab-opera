function varargout = CTRM_2_OPERA(varargin)
% CTRM_2_OPERA MATLAB code for CTRM_2_OPERA.fig
%      CTRM_2_OPERA, by itself, creates a new CTRM_2_OPERA or raises the existing
%      singleton*.
%
%      H = CTRM_2_OPERA returns the handle to a new CTRM_2_OPERA or the handle to
%      the existing singleton*.
%
%      CTRM_2_OPERA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CTRM_2_OPERA.M with the given input arguments.
%
%      CTRM_2_OPERA('Property','Value',...) creates a new CTRM_2_OPERA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CTRM_2_OPERA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CTRM_2_OPERA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only
%      onek
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CTRM_2_OPERA

% Last Modified by GUIDE v2.5 26-Apr-2018 14:56:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CTRM_2_OPERA_OpeningFcn, ...
    'gui_OutputFcn',  @CTRM_2_OPERA_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before CTRM_2_OPERA is made visible.
function CTRM_2_OPERA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CTRM_2_OPERA (see VARARGIN)

% Choose default command line output for CTRM_2_OPERA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

                %%%% CTRM Project Program %%%%
%%% connect to CTRM database
ObjDB_MS_SQL = connect_to_database_MS_SQL ;
MS_SQL_DB_Message = ObjDB_MS_SQL.Message;
if(isempty(MS_SQL_DB_Message))
    MS_SQL_DB_Message = 'MS_SQL Database is Connected';
end
set(handles.text11,'String',MS_SQL_DB_Message);

%%% Get system date
[System_Date_Format,~] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

%%% get the Opera date (Today Date - last 3 months date)
HolDates = {'2016-01-01','2017-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));
Opera_Date_Str = datestr(busdate(today-90,-1,HolidayVec),1);

%%% CTRM date (StartDate and EndDate)
[CTRM_StartDate_Str,CTRM_EndDate_Str,~] = Get_User_Want_Business_Days([],2) ;
CTRM_StartDate_Str = datestr(datenum(CTRM_StartDate_Str,DB_Date_Format),1);
CTRM_EndDate_Str = datestr(datenum(CTRM_EndDate_Str,DB_Date_Format),1);

%%% Set handles
set(handles.edit1,'String',Opera_Date_Str);
set(handles.edit2,'String',CTRM_StartDate_Str);
set(handles.edit3,'String',CTRM_EndDate_Str);

set(handles.edit1,'Enable','off');
set(handles.uibuttongroup1,'Visible','on'); %% If User Want Full Dump Button Group

handles.Opera_Date_Str = Opera_Date_Str;
handles.CTRM_StartDate_Str = CTRM_StartDate_Str;
handles.CTRM_EndDate_Str = CTRM_EndDate_Str;

Live_Only = get(handles.radiobutton1,'Value');
handles.Live_Only = Live_Only;

Live_Dead = get(handles.radiobutton2,'Value');
handles.Live_Dead = Live_Dead;

From_Database = get(handles.radiobutton5,'Value');
handles.From_Database = From_Database;

From_Excel = get(handles.radiobutton6,'Value');
handles.From_Excel = From_Excel;

guidata(hObject,handles);

% --- Outputs from this function are returned to the command line.
function varargout = CTRM_2_OPERA_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

CT = get(handles.checkbox1,'Value');
if(CT)
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox10,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox10,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.CT = CT;
guidata(hObject,handles);

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

CC = get(handles.checkbox2,'Value');
if(CC)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox10,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox10,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.CC = CC;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

CF = get(handles.checkbox3,'Value');
if(CF)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox10,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox10,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.CF = CF;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

SG = get(handles.checkbox8,'Value');
if(SG)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox10,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox10,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.SG = SG;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox9_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

GR = get(handles.checkbox9,'Value');
if(GR)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox10,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox10,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.GR = GR;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox10_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

MP = get(handles.checkbox10,'Value');
if(MP)
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox11,'Enable','off')
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox11,'Enable','on');
end

handles.MP = MP;
guidata(hObject,handles);

% --- Executes on button press in checkbox3.
function checkbox11_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3

RB = get(handles.checkbox11,'Value');
if(RB)
    set(handles.checkbox1,'Enable','off')
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox9,'Enable','off');
    set(handles.checkbox10,'Enable','off');
else
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox9,'Enable','on');
    set(handles.checkbox10,'Enable','on');
end

handles.RB = RB;
guidata(hObject,handles);

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

CTRM_StartDate_Str = get(handles.edit2,'String');
handles.CTRM_StartDate_Str = CTRM_StartDate_Str;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

CTRM_EndDate_Str = get(handles.edit3,'String');
handles.CTRM_EndDate_Str = CTRM_EndDate_Str;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global CT;global CC; global CF; global SG; global GR; global MP; global RB;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[jObj] = Job_Start();
drawnow;

From_Database = get(handles.radiobutton5,'Value');
From_Excel = get(handles.radiobutton6,'Value');

%%% get the selected Date's by user
Opera_Date_Str = handles.Opera_Date_Str;
Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');
CTRM_StartDate_Str = handles.CTRM_StartDate_Str;
CTRM_StartDate_Str = datestr(CTRM_StartDate_Str,'yyyy-mm-dd');
CTRM_EndDate_Str = handles.CTRM_EndDate_Str;
CTRM_EndDate_Str = datestr(CTRM_EndDate_Str,'yyyy-mm-dd');

%%% get the selected BU by user
CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
SG = get(handles.checkbox8,'Value');
GR = get(handles.checkbox9,'Value');
MP = get(handles.checkbox10,'Value');
RB = get(handles.checkbox11,'Value');

if(datenum(CTRM_StartDate_Str))
    %%Only Run Fast Seven Days
    if(CT || CC || CF || SG || GR || MP || RB)
        
        ProductNames = {'CT','CC','CF','SG','GR','MP','RB'}; %% Hard Code
        BU_Names = {'cot','coc','cof','sgr','grn','dry','rbr'};
        User_Select_Portfolio = {CT,CC,CF,SG,GR,MP,RB};
        
        Selected_BU_Names = BU_Names(logical(cell2mat(User_Select_Portfolio)));
        Selected_ProductNames = ProductNames(logical(cell2mat(User_Select_Portfolio)));
        
        %%% Find the minimum date
        [Opera_Date_Str] = Find_Minimum_CTRM_ViewDate(char(Selected_ProductNames),CTRM_StartDate_Str,CTRM_EndDate_Str) ;
        set(handles.edit1,'String',Opera_Date_Str);
        Opera_Date_Str = datestr(Opera_Date_Str,'yyyy-mm-dd');

        %%% get Output File name
        XLS_Name = strcat('CTRM_2_OPERA_TR_Format_',upper(char(Selected_BU_Names)));
        OutXLSName = getXLSFilename(XLS_Name);
        xlswrite(OutXLSName,{''},'CTRM_2_OPERA_TR_Format');
        
        %%% SQL Query's for deal_ticket_table view and CTRM view's
        ViewName = strcat('deal_ticket_table_with_latest_versions_view_',char(Selected_BU_Names));
        SqlQuery_1 = ['SELECT transaction_date, ops_unique ,trade_id FROM ',ViewName,' where transaction_date >=''', Opera_Date_Str ,''' '];
        SqlQuery_2 = ['select * from report.vw_for_opera_derivative where pr_Code in (''',char(Selected_ProductNames),''') and ((cast(Deal_date as date) >= ''',CTRM_StartDate_Str,''' and cast(Deal_date as date) <= ''',CTRM_EndDate_Str,''') or (cast(Modified_Date as date) >= ''',CTRM_StartDate_Str,''' and cast(Modified_Date as date) <= ''',CTRM_EndDate_Str,'''))'] ;
        SqlQuery_3 = ['SELECT * FROM ',ViewName,' where ops_action <>''DEL''  '] ;

        %%% Fetch data from View's using above sql query's
        [DBFieldNames,DBData] = read_from_database(0,0,SqlQuery_1);
        if(From_Database)
            [CTRM_FieldNames,CTRM_Data] = read_from_database_MS_SQL([],SqlQuery_2);
        else %% if (From_Excel)
            %%% Read the CTRM DUMP Input file from System (manually)
            [FileName,PathName] = uigetfile('*.*','Select the CTRM Dump File');
            Excel_Path = [PathName,FileName];
            [~,~,CTRMRawData] = xlsread(char(Excel_Path));  %% CTRM Dump Input File
            CTRM_FieldNames = CTRMRawData(1,:);
            CTRM_Data = CTRMRawData(2:end,:);
        end
        
        [DBFieldNames_Del,DBData_Del] = read_from_database(0,0,SqlQuery_3); 
        
        if( size(DBData,2) > 1 )
            if(~strcmpi('No Data',CTRM_Data ))
                %%% CTRM Main Program Logic
                CTRM_FieldNames=strrep(CTRM_FieldNames,'/','_');
                CTRM_View_Data = cell2table(CTRM_Data,'VariableNames',CTRM_FieldNames);  %% Convert to table
                %%% serial date(double value) convert to datetime format
                if(From_Database)
                    CTRM_View_Data.Modified_Date = datestr(datenum(CTRM_View_Data.Modified_Date)+datenum('30-Dec-1899'));
                else %% if (From_Excel)
                    if ~iscell(CTRM_View_Data.Modified_Date(1))
                        CTRM_View_Data.Modified_Date = datestr(CTRM_View_Data.Modified_Date+datenum('30-Dec-1899'));
                    end
                end
                
                %%% Filter the Confirmed data
                STATUS_NAME = 'Confirmed';
                Index_Confirmed = strcmpi(STATUS_NAME,CTRM_View_Data.status_name);
                CTRM_Confirmed_Data = CTRM_View_Data(Index_Confirmed,:);  
                
                if ~isempty(CTRM_Confirmed_Data)
                    %%Compare CTRM DB and OUR DB %% Get New Trades
                    OUR_UniqueNumber_Col = cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames) ;
                    CTRM_UniqueNumber_Col = cellfun(@(V) strcmpi('uniq_no',V), CTRM_FieldNames) ;
                    TradeID_Col = cellfun(@(V) strcmpi('trade_id',V), DBFieldNames) ;
                    
                    OUR_Unique_Data  = DBData(:,OUR_UniqueNumber_Col);
                    CTRM_Unique_Data = CTRM_Confirmed_Data(:,CTRM_UniqueNumber_Col);

                    [Matched_UniqueNumber , Location] = ismember(CTRM_Unique_Data.uniq_no,OUR_Unique_Data);
                    CTRM_Old_Data = CTRM_Confirmed_Data(Matched_UniqueNumber,:);
                    CTRM_New_Data = CTRM_Confirmed_Data(~Matched_UniqueNumber,:);
                    if any(Matched_UniqueNumber)
                        Existing_TradeId = DBData(nonzeros(Location),TradeID_Col);
                    end
                else
                    warndlg('No Data for Confirmed Trades');
                    CTRM_New_Data = [];
                    CTRM_Old_Data = [];
                end

                %%CTRM View New Data 2 TR FORMAT
                if(exist('CTRM_New_Data','var'))
                    [TRHeader , New_OutTRData , Market_Price_Index_Error_New , Maturity_Error_Data_New] = CTRM_Future_Option_2_TR_Format(CTRM_New_Data,'NEW',From_Database) ;
                end

                %%CTRM View Mod Data 2 TR FORMAT
                if(exist('CTRM_Old_Data','var'))
                    [TRHeader , Mod_OutTRData ,Market_Price_Index_Error_Old , Maturity_Error_Data_Old] = CTRM_Future_Option_2_TR_Format(CTRM_Old_Data,'MOD',From_Database) ;

                    if(size(Mod_OutTRData,2) > 1)
                        TRN_Number_Col = cellfun(@(V) strcmpi('TRN.Number',V), TRHeader) ;
                        Parent_TRN_Number_Col = cellfun(@(V) strcmpi('Parent Transaction Number',V), TRHeader) ;

                        Mod_OutTRData(:,TRN_Number_Col) = Existing_TradeId ;
                        Mod_OutTRData(:,Parent_TRN_Number_Col) = Existing_TradeId ;
                    end
                end

                %%% Filter the Voided data
                STATUS_NAME1 = {'Cancelled'};
                Index_Voided = ismember(CTRM_View_Data.status_name,STATUS_NAME1);
                CTRM_Voided_Data = CTRM_View_Data(Index_Voided,:);

                if ~isempty(CTRM_Voided_Data)
                    %%Compare CTRM DB Data and OUR DB Data %% Get Del Trades
                    UNIQ_NO_Del_Col = cellfun(@(V) strcmpi('uniq_no',V), CTRM_FieldNames) ;
                    Del_ID = CTRM_Voided_Data(:,UNIQ_NO_Del_Col);
                    OUR_UniqueNumber_Col = cellfun(@(V) strcmpi('ops_unique',V), DBFieldNames_Del) ;

                    if(size(DBData_Del,2)>1)
                        Del_ID_Index  = cellStrfind_exact(DBData_Del(:,OUR_UniqueNumber_Col) , table2cell(Del_ID)) ;
                        Del_Data = DBData_Del(Del_ID_Index,:) ;  %% CTRM Delete Trades
                    else
                        Del_ID_Index = [];
                    end

                    if(~isempty(Del_ID_Index))  
                        [DBFieldNames_Del ,  Del_Data] = Update_2_Database(Del_Data,char(Selected_BU_Name)) ; %% Update Table and Get Data From Table 
                    else
                        warndlg(SqlQuery_3,'Not Matching Delete Unique ID with deal_ticket_table_with_latest_versions_view');
                    end
                else
                    warndlg('No Data for Cancelled Trades');
                end
    
                %%% Combine the CTRM New Data and CTRM Modified Data
                Overall_New_TRData = New_OutTRData;
                Overall_Mod_TRData =  Mod_OutTRData;
                Overall_New_Mod_TRData = [Overall_New_TRData ; Overall_Mod_TRData];

                if isempty(Overall_New_Mod_TRData)
                    warndlg('No Data found in Future and Options in report.vw_for_opera_derivative View!','CTRM_2_OPERA');
                    msgbox('Process Complete','Finish');
                    Job_Done(jObj);
                end
            
                %%% get the trade_id for only CTRM New data
                [TradeId , ~] = getTradeID_4_Cell_Array(Overall_New_TRData,Selected_BU_Names);

                if ~strcmpi(TradeId , '')    %% if CTRM New data is not empty           
                    TRN_TradeID_Col = cellStrfind_exact(TRHeader , {'TRN.Number'}) ;
                    Parent_TradeID_Col = cellStrfind_exact(TRHeader , {'Parent Transaction Number'}) ;
                    Overall_New_TRData(:,TRN_TradeID_Col) = TradeId ;
                    Overall_New_TRData(:,Parent_TradeID_Col) = TradeId ;

                    Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];               
                    xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'CTRM_2_OPERA_TR_Format') ;               
                else  %% if CTRM New data is empty 
                    Overall_TRData = [Overall_New_TRData ; Overall_Mod_TRData];
                    xlswrite(OutXLSName , [TRHeader ; Overall_TRData] , 'CTRM_2_OPERA_TR_Format') ;
                end
     
                %%% Excel Write for CTRM data
                if isempty(CTRM_New_Data) 
                    xlswrite(OutXLSName , {'No Data'} , 'CTRM_New_Data') ;
                else
                    xlswrite(OutXLSName , [CTRM_FieldNames ;  table2cell(CTRM_New_Data)] , 'CTRM_New_Data') ;
                end

                if isempty(CTRM_Old_Data) 
                    xlswrite(OutXLSName , {'No Data'} , 'CTRM_Old_Data') ;
                else
                    xlswrite(OutXLSName , [CTRM_FieldNames ;  table2cell(CTRM_Old_Data)] , 'CTRM_Old_Data') ;
                end

                if(exist('Del_Data','var') && (~isempty(Del_ID_Index)) )
                    xlswrite(OutXLSName , [DBFieldNames_Del ;  Del_Data] , 'CTRM_Voided_Data') ; 
                end

                if(~isempty(Market_Price_Index_Error_New) || ~isempty(Market_Price_Index_Error_Old))
                    Market_Price_Index_Error = [Market_Price_Index_Error_New ; Market_Price_Index_Error_Old];
                    xlswrite(OutXLSName , Market_Price_Index_Error , 'Instrument Error Data') ;
                end

                if(~isempty(Maturity_Error_Data_New) || ~isempty(Maturity_Error_Data_Old))
                    Maturity_Error_Data = [Maturity_Error_Data_New ; Maturity_Error_Data_Old];
                    xlswrite(OutXLSName , Maturity_Error_Data , 'Maturity Error Data') ;
                end
    
                %%%% Delete Empty sheets and active the user required sheet
                OutXLSName = fullfile(pwd,OutXLSName) ;
                xls_delete_sheets(OutXLSName) ;
                xls_change_activesheet(OutXLSName , 'CTRM_2_OPERA_TR_Format'); 

                msgbox('Process Complete','Finish');
            else
                warndlg('No Data found in report.vw_for_opera_derivative view for given Date ranges') ;  
            end 
        else
            warndlg(SqlQuery_1,'No Data Last 90 Days deal_ticket_table_with_latest_versions_view');
        end
   else   
        close(gcf);
        msgbox('Please Select the any one of the BU Name','Warning');
    end
else
    msgbox(['The Data can be retrieved only for Last 7 days.',{''},'Please choose the appropriate Date for Opera Start & End Date.'],'Warning');
end

Job_Done(jObj);
set(handles.pushbutton1,'Enable','on');


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[jObj] = Job_Start();
drawnow;

Live_Only = get(handles.radiobutton1,'Value');
Live_Dead = get(handles.radiobutton2,'Value');

try
    [BU_Name,Pr_Code] = Get_BU_Prcode(handles); %% User Selected BU
    [OutXLSName] = Get_OverAll_Dump(char(BU_Name),char(Pr_Code),Live_Only,Live_Dead);
catch ME
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end

msgbox(['Overall Dump File is ',OutXLSName],'Finish');

Job_Done(jObj);

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% Live_Only = get(handles.radiobutton1,'Value');
% handles.Live_Only = Live_Only;
% guidata(hObject,handles);

% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

% Live_Dead = get(handles.radiobutton2,'Value');
% handles.Live_Dead = Live_Dead;
% guidata(hObject,handles);

% --- Executes on button press in radiobutton1.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% --- Executes on button press in radiobutton1.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

