function ceaString = convertChar2Cell(chrString,chrDelimiter)
%
% Splits a string 'chrString' delimited by characters in 'chrDelimiter' and
% returns a cell array containing those strings
% This function has to be used after INIREAD to parse the Character array in 
% INI file and to convert it into cell array of strings
%
%% Syntax:
%  =======
%    ceaString = convertChar2Cell(chrString,chrDelimiter)
% 
%% Input Arguments:
%  ================
%   chrString - <charArray> of String read from INIFile using INIREAD
%   function in INIFileManager library
%   chrDelimiter - <char> Delimiter characters
% 
%% Return Values:
%  ==============
%  ceaString - <cellArray> of strings parsed after reading from INIREAD
%  function

ceaString = {};
cmp=cell(1,1);
while (~isempty(chrString) && ~isequal(cmp,chrString))
    [chrToken,chrString] = strtok(chrString,chrDelimiter); 
    ceaString = {ceaString{:}, chrToken};
end