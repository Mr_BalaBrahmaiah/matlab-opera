function [BU_Name,Business_Unit] = Get_BU_Prcode(handles)

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
SG = get(handles.checkbox8,'Value');
GR = get(handles.checkbox9,'Value');
MP = get(handles.checkbox10,'Value');
RB = get(handles.checkbox11,'Value');

if(CT || CC || CF || SG || GR || MP || RB)
    
    ProductNames = {'CT','CC','CF','SG','GR','MP','RB'}; %% Hard Code
    BU_Names = {'cot','coc','cof','sgr','grn','dry','rbr'}; %% Hard Code
    User_Select_Portfolio = {PLM,RBR,EDL};
    BU_Name = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
        
end