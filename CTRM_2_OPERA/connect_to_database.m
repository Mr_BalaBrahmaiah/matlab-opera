function objDB = connect_to_database
% connect to the database configuration as provided in the
% DatabaseConfiguration file
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/01/23 07:18:53 $
%  $Revision: 1.1 $
%

   javaaddpath('mysql-connector-java-5.1.25-bin.jar');
  
    % Read the database configuration file to read the DB configuration
    % Default DB config values   
    DBName = 'dbsystemuat';
    DBUserName = 'user';
    DBPassword = 'invenio@123';
    IsFromServer = 1;
    DBServerIP = '192.168.37.60';
    try
        DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        %         disp(['Using the database ',DBName]);
        fprintf('Using the database : %s : IP : %s\n',DBName,DBServerIP);
    catch
        disp('Error occured in reading the DatabaseConfiguration.txt, hence using the default configuration!');
    end
    
    %   objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.71');
    if IsFromServer
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
    else
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL');
    end

end