function [Min_Opera_Date] = Find_Minimum_CTRM_ViewDate(Prod_Code,Start_Date,End_Date)

% Prod_Code = 'CT';

ObjDB_MS_SQL = connect_to_database_MS_SQL ;

%%
SqlQuery1 = ['select cast(min(Deal_date) as date) from report.vw_for_opera_derivative where pr_Code in (''',Prod_Code,''') and status_name = ''','Confirmed',''' and ((cast(Deal_date as date) >= ''',Start_Date,''' and cast(Deal_date as date) <= ''',End_Date,''') or (cast(Modified_Date as date) >= ''',Start_Date,''' and cast(Modified_Date as date) <= ''',End_Date,'''))'] ;
SqlQuery2 = ['select cast(min(Deal_date) as date) from report.vw_for_opera_derivative where pr_Code in (''',Prod_Code,''') and status_name = ''','Cancelled',''' and ((cast(Deal_date as date) >= ''',Start_Date,''' and cast(Deal_date as date) <= ''',End_Date,''') or (cast(Modified_Date as date) >= ''',Start_Date,''' and cast(Modified_Date as date) <= ''',End_Date,'''))'] ;

%%
Confirmed_Date = fetch(ObjDB_MS_SQL,SqlQuery1) ;
Fulfilled_Date = fetch(ObjDB_MS_SQL,SqlQuery2) ;

All_Dates = [Confirmed_Date ; Fulfilled_Date] ;
All_Dates = All_Dates(cellfun(@(s) (length(s)>1), All_Dates)) ;  %% Remove Empty Date Fields

Min_All_Dates = min(datenum(All_Dates)) ;
Min_CTRM_Date = datestr(Min_All_Dates,'dd-mmm-yyyy') ;

%%
[System_Date_Format,~] = get_System_Date_Format();

HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));

Opera_Date_Str = datestr(busdate(today-90,-1,HolidayVec),1);
Opera_Date_Num = datenum(Opera_Date_Str);

if(Opera_Date_Num < Min_All_Dates)
    Min_Opera_Date = Opera_Date_Str ;
else
    Min_Opera_Date = Min_CTRM_Date ;
end

%%
close(ObjDB_MS_SQL) ;
delete(ObjDB_MS_SQL) ;

end
