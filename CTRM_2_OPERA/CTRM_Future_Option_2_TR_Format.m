function [TRHeader , RowData ,Missing_Instrument_Index_Error ,Maturity_Error_Data] = CTRM_Future_Option_2_TR_Format(CTRM_Data , CTRM_Action_Str, From_Database)

    Missing_Instrument_Index_Error = '';
    Maturity_Error_Data = '';

    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from vol_id_table
    SqlQuery = 'select vol_id,bbg_opt_ticker,expiry_date from vol_id_table';
    [~,DBData] = read_from_database('vol_id_table',0,SqlQuery);
    if strcmpi(DBData,'No Data') 
        warndlg('Data not Available in vol_id_table');
        return;
    end
    DBVolID = DBData(:,1);
    DBBBGOptTicker = DBData(:,2);
    DBExpiryDate = cellstr(datestr(datenum(DBData(:,3),'yyyy-mm-dd'),'yyyy'));
    
    %%% Fetch Data from ctrm_instrument_mapping_table
    Instrument_Mapping_Table_Name = 'ctrm_instrument_mapping_table';
    sql_ctrm_instrument_mapping = ['select * from ',Instrument_Mapping_Table_Name,];
    [ctrm_instrument_mapping_Cols,ctrm_instrument_mapping_Data] = Fetch_DB_Data(ObjDB,sql_ctrm_instrument_mapping);

    if strcmpi(ctrm_instrument_mapping_Data,'No Data') 
        warndlg({['Data not Available in ',Instrument_Mapping_Table_Name,]}) ;
        return;
    end

    ctrm_instrument_mapping_Tab_Data = cell2table(ctrm_instrument_mapping_Data,'VariableNames',ctrm_instrument_mapping_Cols);  %% Convert to table

    %%% Check if the Data is avaliable or not
    if(size(CTRM_Data,1) >= 1)
        %%% Mapping the Instrument
        CTRM_instrument = strcat(CTRM_Data.pr_Code,{'-'},CTRM_Data.exchange,{'-'},CTRM_Data.product);
        Index_instrument = ismember(CTRM_instrument,ctrm_instrument_mapping_Tab_Data.pr_exh_name);
        CTRM_View_Data = CTRM_Data(Index_instrument,:);  %%% Final excat data

        %%% Missing Instrument_Index List
        Missing_Instrument_Index_Error = unique(CTRM_instrument(~Index_instrument)); 

        if ~isempty(Missing_Instrument_Index_Error)
            warndlg(['CTRM Missing Instrument',' View Contain the Instrument ',Missing_Instrument_Index_Error',' which is not available in ctrm_instrument_mapping_table. Please update this Instrument in database to get these Instrument deals'],'CTRM_2_OPERA');
        end
        
        %%% get the user required columns
        % Portfolio
        Portfolio = strcat(CTRM_View_Data.pr_Code,{'-'},CTRM_View_Data.unit);
        
        % Buy_Sell
        Buy_Sell_Data = CTRM_View_Data.Buy_Sell;
        Bounght_Index = cellStrfind_exact(Buy_Sell_Data,{'Buy'}) ;
        Sold_Index = cellStrfind_exact(Buy_Sell_Data,{'Sell'}) ;
        Buy_Sell(Bounght_Index,1) = {'Bought'};
        Buy_Sell(Sold_Index,1) = {'Sold'};
        
        % InitialLots
        InitialLot_Data = CTRM_View_Data.lots - CTRM_View_Data.lyadj_lot;
        
        % BrokerLots
        BrokerLot_Data = CTRM_View_Data.lots - CTRM_View_Data.lybrok_adj;
        
        %OptType
        OptType = CTRM_View_Data.sub_instrument;
        putcall = CTRM_View_Data.put_call;
        
        Futures_Index = cellStrfind_exact(OptType,{'Futures'}) ;
        Options_Index = cellStrfind_exact(OptType,{'Options'}) ;
        Spread_Index = cellStrfind_exact(OptType,{'Spread Options'});
        
        if (From_Database)
            Future_Index = strcmpi(OptType,{'Futures'}) & strcmpi(putcall,{''});
        else
            Future_Index = strcmpi(OptType,{'Futures'});
        end
        Options_Put_Index = strcmpi(OptType,{'Options'}) & strcmpi(putcall,{'put'});
        Options_Call_Index = strcmpi(OptType,{'Options'}) & strcmpi(putcall,{'call'});
        Spread_Put_Index = strcmpi(OptType,{'Spread Options'}) & strcmpi(putcall,{'put'});
        Spread_Call_Index = strcmpi(OptType,{'Spread Options'}) & strcmpi(putcall,{'call'});

        OptType_putcall(Future_Index,1) = {'Future'};
        OptType_putcall(Options_Put_Index,1) = {'vanilla_put'};
        OptType_putcall(Options_Call_Index,1) = {'vanilla_call'};
        OptType_putcall(Spread_Put_Index,1) = {'spread_put'};
        OptType_putcall(Spread_Call_Index,1) = {'spread_call'};
        
        %%% To get the Instrument and Maturity Month-2
        Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
        MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
                
        [row,~] = size(CTRM_View_Data);
        Maturity_Error_Data = [];
        CTRM_View_instrument = strcat(CTRM_View_Data.pr_Code,{'-'},CTRM_View_Data.exchange,{'-'},CTRM_View_Data.product);
        for i=1:1:row
            Matched_MP_Index = strcmpi(CTRM_View_instrument(i) ,ctrm_instrument_mapping_Tab_Data.pr_exh_name);
            instrument_mapping_data = ctrm_instrument_mapping_Tab_Data(Matched_MP_Index,:);

            Instrument(i,1) = instrument_mapping_data.instrument;
            price_conversion(i,1) = instrument_mapping_data.price_conversion;
            
            %%% Maturity Month-1 and Month-2
            if strcmpi(CTRM_View_Data.sub_instrument(i) ,'Futures')
                Maturity_Code(i,1) = strcat(CTRM_View_Data.code_month(i),'.',CTRM_View_Data.code_month(i)); 
                Month2_code(i,1) = {''};
            elseif strcmpi(CTRM_View_Data.sub_instrument(i) ,'Options')
                EXPIRY_DATE = CTRM_View_Data.last_trade_date(i);
                Current_Expiry_Date = cellstr(datestr(datenum(EXPIRY_DATE,'yyyy-mm-dd'),'yyyy'));
                Month_Code = CTRM_View_Data.code_month(i);
                product_code = CTRM_View_Data.market_price_code(i);
                cxc_bbg_ticker = strcat(product_code,Month_Code);

                BBG_Opt_Ticker_Index = find(strcmpi(cxc_bbg_ticker,DBBBGOptTicker) & strcmpi(Current_Expiry_Date,DBExpiryDate));

                if(~isempty(BBG_Opt_Ticker_Index))
                    Vol_ID = DBVolID(BBG_Opt_Ticker_Index , 1);
                    Split_Str =strsplit(char(Vol_ID),' ');  
                else
                    Split_Str = cxc_bbg_ticker;
                    Maturity_Error_Data = [Maturity_Error_Data ;cellstr(strcat(cxc_bbg_ticker,' CXC_BBG_Ticker not available in vol_id_table '))];
                end
                Maturity_Code(i,1) =  Split_Str(end) ;
                
                Month2_code(i,1) = {''};
                
            elseif strcmpi(CTRM_View_Data.sub_instrument(i) ,'Spread Options')
                    Maturity_Code(i,1) = strcat(CTRM_View_Data.code_month(i),'.',CTRM_View_Data.code_month(i)); 
                    Month2_code(i,1) = strcat(CTRM_View_Data.Code_Month_2(i),'.',CTRM_View_Data.Code_Month_2(i)); 
            else
                Maturity_Code(i,1) = {''};
                Month2_code(i,1) = {''};
            end
        end

    % Instrument
    instrument = Instrument;
    instrument(Options_Index) = strcat(instrument(Options_Index),'-O');
    instrument(Spread_Index) = strcat(instrument(Spread_Index),'-CSO');
    
    % Strike
    Strike = CTRM_View_Data.strike_price;
    Strike(Futures_Index) = Strike(Futures_Index);
    Strike(Options_Index) = Strike(Options_Index) ./ price_conversion(Options_Index);
    Strike(Spread_Index) = Strike(Spread_Index) ./ price_conversion(Spread_Index);
    
    %Premimum_Data
    Prem_Data = CTRM_View_Data.rate ./ price_conversion;

    %%% get the data for CounterParty&ExeBroker, TransactionDate,
    %%% Traded_DateTimestamp, Trader_Name, UniqueNumber and CounterpartyEntity
    CounterParty_Data = CTRM_View_Data.broker;
    ExeBroker_Data = CounterParty_Data;
    
    if (From_Database)
        TransactionDate = CTRM_View_Data.Deal_date;
    else %% if (From_Excel)
        if ~iscell(CTRM_View_Data.Deal_date(1))
            TransactionDate = cellstr(datestr(CTRM_View_Data.Deal_date+datenum('30-Dec-1899')));
        else
            TransactionDate = CTRM_View_Data.Deal_date;
        end
    end
    
    Traded_DateTimestamp = cellstr(CTRM_View_Data.Modified_Date);
    Trader_Name = CTRM_View_Data.modified_by;
    UniqueNumber_Data = CTRM_View_Data.uniq_no; 
    CounterpartyEntity_Data = CTRM_View_Data.entity; 

    %%% Hard Code fields
    Source = repmat({'CTRM'},size(CTRM_View_Data,1),1); 
    ExeType = repmat({'Elec'},size(CTRM_View_Data,1),1); 
    Rates = repmat({'Normal'},size(CTRM_View_Data,1),1); 
    CTRM_Action_Data = repmat({CTRM_Action_Str},size(CTRM_View_Data,1),1); %% CTRM_Action is either NEW or MOD
 
    %%% Just fill with empty data
    TRN_Number = cell(size(CTRM_View_Data,1),1);
    Parent_Trans_Number = cell(size(CTRM_View_Data,1),1);
    CONTRACTNB = cell(size(CTRM_View_Data,1),1);
    Maturity = cell(size(CTRM_View_Data,1),1);
    Barrier_Type = cell(size(CTRM_View_Data,1),1);
    Barrier = cell(size(CTRM_View_Data,1),1);
    Averaging_Start_Date = cell(size(CTRM_View_Data,1),1);
    Averaging_End_Date = cell(size(CTRM_View_Data,1),1);
    Spcl_Rate_lot = cell(size(CTRM_View_Data,1),1);
    Other_Remarks = cell(size(CTRM_View_Data,1),1);
    OTC_DAILY_EOD_LEG = cell(size(CTRM_View_Data,1),1);

    %%% Required Header
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
        'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
    %%% Required Data
    RowData = [instrument, Portfolio, Buy_Sell, Maturity_Code, Month2_code, num2cell(Strike), OptType_putcall, num2cell(Prem_Data), CounterParty_Data,num2cell(InitialLot_Data), ...
                CONTRACTNB, Maturity, Barrier_Type, Barrier, TransactionDate,TRN_Number, Source, Averaging_Start_Date, Averaging_End_Date, ...
                Parent_Trans_Number, ExeBroker_Data, ExeType, Rates, Spcl_Rate_lot, Other_Remarks, OTC_DAILY_EOD_LEG, Trader_Name, Traded_DateTimestamp, ...
                UniqueNumber_Data, num2cell(BrokerLot_Data), CTRM_Action_Data, CounterpartyEntity_Data] ;
    
    else %% if Data is empty
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
            'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
            'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};

        RowData = [];
    end
end
