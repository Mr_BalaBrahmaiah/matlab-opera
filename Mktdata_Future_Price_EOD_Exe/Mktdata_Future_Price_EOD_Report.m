function [OutErrorMsg] =  Mktdata_Future_Price_EOD_Report()

%%% Project Name: MKTData Future Price EOD Exe
%%% Module Name: Mktdata Future Price EOD Report
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 29 Apr 2019 / 15:03:03

try
    OutErrorMsg = {'No Errors'};
    ObjDB = connect_to_database; %%% Connect to database
        
    %%% Exe Inputs
    Prompt        = {'Enter the MKT Date:'};
    Name          = 'Inputs for Future Price Report';
    Numlines      = 1;
    
    Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    Defaultanswer = Settele_date;
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Date input was not given by user!');
        return;
    end
    
    mkt_date = InputDates(1);  %%% default date
    
    ObjDB1 = connect_to_dbchannel;  %%% Conncet to OPERA Channel
    
    %%Fetch data from view
    Table_Name = 'mktdata_future_price_table ';
    SqlQuery_mktdata_future_price = ['select * from mktdata_future_price_table where mkt_date = ''',char(mkt_date),''' '] ;
    [mktdata_future_price_Cols,mktdata_future_price_Data] = Fetch_DB_Data(ObjDB1,SqlQuery_mktdata_future_price);
    
    if strcmpi(mktdata_future_price_Data,'No Data') 
       errordlg ({['Data not Available in ',char(Table_Name)]}) ;
       return;
    end
    
    %%% get Output File Name
    TimeStamp = strsplit(datestr(now),' ');
    TimeStamp = strrep(TimeStamp(end),':','');
    Date_TimeStamp = [char(datestr(mkt_date,'yyyymmdd')),'_',char(TimeStamp)];
    XLSFileName = strcat('mktdata_future_price','_',Date_TimeStamp,'.xlsx'); 

    %%% write the data into excel sheet
    xlswrite(XLSFileName,[mktdata_future_price_Cols;mktdata_future_price_Data],'Future Price Data');
    xls_delete_sheets([pwd filesep XLSFileName]);
        
    msgbox('Process Complete','Finish');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end