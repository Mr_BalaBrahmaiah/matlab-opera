function parse_ftp_dump_updated
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/16 08:29:11 $
%  $Revision: 1.4 $
%
try
    parpool;
    
    hWaitbar = waitbar(0,'Please wait...');
    
    Prompt        = {'Enter the Valuation Date(yyyy-mm-dd format):','Enter the Settlement Date for US products(yyyy-mm-dd format):','Enter the Settlement Date for UK products(yyyy-mm-dd format):'};
    Name          = 'Date Input for FTP Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(today,'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    DBValueDate    = InputDates(1);
    DBSettleDateUS = InputDates(2);
    DBSettleDateUK = InputDates(3);
    
    if isempty(DBValueDate)
        DBValueDate  = {datestr(today,'yyyy-mm-dd')};
    end
    DBSettleDate = [DBSettleDateUS,DBSettleDateUK];
    if isempty(DBSettleDate)
        DBSettleDate = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    
    [OptionFileNames,OptionPathName] = uigetfile('*.*','Select the Option Dump File (namr/eur)','Multiselect','on');
    
    if isnumeric(OptionPathName)
        DefPathName = pwd;
    else
        DefPathName = OptionPathName;
    end
    
    [FutureFileName,FuturePathName]  = uigetfile('*.*','Select the Future Dump File',DefPathName);
    
    [EquityFileName,EquityPathName]  = uigetfile('*.*','Select the Equity Dump File',DefPathName);
    
    [FX_FutureFileName,FX_FuturePathName]  = uigetfile('*.*','Select the FX Future Dump File',DefPathName);
    
    %     Mapping_FilePath = [pwd filesep 'Equity_Mapping_Files' filesep 'Mapping_Eqd&Bond.xlsx'];
    [MappingFieldNames,MappingData] = read_from_database('asset_class_bbgcode_mapping',0);
    
    [DBFieldNames,DBData] = read_from_database('underlying_list_table',0);
    FutDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_UndId = FutDataDB.bbg_underlying_id;
    UndId = FutDataDB.underlying_id;
    Asset_Class = FutDataDB.asset_class;
    %     DBCurrency = FutDataDB.currency;
    IdxEmpty = cellfun(@isempty,BBG_UndId);
    BBG_UndId(IdxEmpty) = [];
    UndId(IdxEmpty) = [];
    Asset_Class(IdxEmpty) = [];
    %     DBCurrency(IdxEmpty) = [];
    
    %     BBG_UndId = unique(BBG_UndId);                   %% check why give same ID multiple time %% New Change
    
    [DBFieldNames,DBData] = read_from_database('vol_id_table',0);
    VolDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_OptCode = VolDataDB.bbg_opt_ticker;
    DBVolId = VolDataDB.vol_id;
    
    %     InvenioProductCode = unique(FutDataDB.invenio_product_code);
    %     IdxProdRemove = cellStrfind(InvenioProductCode,'USD');
    %     InvenioProductCode(IdxProdRemove) = [];
    
    %     if ~isnumeric(OptionFileNames) || ~isnumeric(FutureFileName)
    %         SelProdIdx = listdlg('PromptString','Select the Product Code','SelectionMode','multiple',...
    %             'Name','Product Code Selection','ListString',InvenioProductCode,...
    %             'InitialValue',[],'ListSize',[300,500]);
    %         if ~isempty(SelProdIdx)
    %             SelProducts = InvenioProductCode(SelProdIdx);
    %             % change the below id values so that only these are read
    %             IdxFut = cellStrfind(UndId,SelProducts);
    %             BBG_UndId = BBG_UndId(IdxFut);
    %             UndId = UndId(IdxFut);
    %             IdxOpt = cellStrfind(DBVolId,SelProducts);
    %             DBVolId = DBVolId(IdxOpt);
    %             BBG_OptCode = BBG_OptCode(IdxOpt);
    %         end
    %     end
    
    
    %% Option dump parser
    if isnumeric(OptionFileNames) && OptionFileNames==0
        errordlg('Option Dump file not selected!','FTP Parser');
    else
        RequiredOptionFields = {'TICKER','OPT_PUT_CALL','OPT_UNDL_PX','OPT_STRIKE_PX',...
            'PX_LAST','PX_OPEN','PX_HIGH','PX_LOW','PX_VOLUME','OPT_OPEN_INT',...
            'OPT_UNDL_TICKER','OPT_IMPLIED_VOLATILITY_MID',...
            'PX_SETTLE_LAST_DT','PX_SETTLE','OPT_EXPIRE_DT',...
            'PX_VOLUME_1D','SECURITY_TYP','CRNCY'};
        
        if ~iscellstr(OptionFileNames)
            OptionFileNames = cellstr(OptionFileNames);
        end
        if ~iscellstr(OptionPathName)
            OptionPathName = cellstr(OptionPathName);
        end
        
        FileName = fullfile(OptionPathName,OptionFileNames);
        NumOptFiles = length(FileName);
        OptionData = [];
        waitbar(0.1,hWaitbar,'Please wait, parsing option dump file');
        for iFile = 1:NumOptFiles
            OutData = parse_option_file_updated(FileName{iFile},BBG_OptCode);
            if isempty(OutData)
                continue;
            end
            %             xlswrite(['raw_option_dump_',OptionFileNames{iFile},'.xlsx'],dataset2cell(OutData));
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            DumpSettleDate = cellstr(datestr(datenum(OutData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
            IdxDate = ismember(DumpSettleDate,DBSettleDate);
            IdxComdty = strcmpi('Comdty',OutData.MARKET_SECTOR_DES); % to ignore the others like curncy and index market types, since they return irrelevant data
            IdxFound = IdxDate & IdxComdty;
            for iField = 1:length(RequiredOptionFields)
                TempField = RequiredOptionFields{iField};
                if isfield(OptionData,TempField)
                    OptionData.(TempField) = [OptionData.(TempField);OutData.(TempField)(IdxFound)];
                else
                    OptionData.(TempField) = OutData.(TempField)(IdxFound);
                end
            end
            clear OutData;
        end
        
        SettlePrice  = str2double(OptionData.PX_SETTLE);
        Strike       = str2double(OptionData.OPT_STRIKE_PX);
        AssetPrice   = str2double(OptionData.OPT_UNDL_PX);
        % TODO - verify the below code
        AssetPrice(AssetPrice<0) = 0.1;
        Strike(Strike<0) = 0;
        SettlePrice(SettlePrice<0) = 0;
        % End of TODO
        
        % Settlement Price
        SecurityID = cell(size(OptionData.TICKER));
        VolID = cell(size(OptionData.TICKER));
        for iCode = 1:length(BBG_OptCode)
            Idx = strmatch(BBG_OptCode{iCode},OptionData.TICKER);
            for iIdx = 1:length(Idx)
                SecurityID{Idx(iIdx)} = [DBVolId{iCode},OptionData.OPT_PUT_CALL{Idx(iIdx)},' ',num2str(Strike(Idx(iIdx)))];
            end
            VolID(Idx) = DBVolId(iCode);
        end
        Value_Date = repmat(DBValueDate,size(SecurityID));
        DataSource = repmat(cellstr('bbg_dumpfile'),size(SecurityID));
        Settle_Date = cellstr(datestr(datenum(OptionData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
        SettlePriceHeader = {'Security ID','Value Date','Settlement Price',...
            'Data Source','Settlement Date',...
            'Last Price','Open Price','High Price','Low Price','Open Int',...
            'Volume','Volume 1D','Imp Vol Mid'};
        SettlePriceData = [SecurityID,Value_Date,num2cell(SettlePrice),...
            DataSource,Settle_Date,...
            num2cell(str2double(OptionData.PX_LAST)),num2cell(str2double(OptionData.PX_OPEN)),...
            num2cell(str2double(OptionData.PX_HIGH)),num2cell(str2double(OptionData.PX_LOW)),...
            num2cell(str2double(OptionData.OPT_OPEN_INT)),num2cell(str2double(OptionData.PX_VOLUME)),...
            num2cell(str2double(OptionData.PX_VOLUME_1D)),num2cell(str2double(OptionData.OPT_IMPLIED_VOLATILITY_MID)/100)];
        
        xlswrite(['Settlement_Price_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettlePriceHeader;SettlePriceData]);
        
        waitbar(0.4,hWaitbar,'Please wait, calculating settle vols');
        % Settlement Vol
        MaturityDate = datenum(OptionData.OPT_EXPIRE_DT,'yyyymmdd');
        OptionType = OptionData.OPT_PUT_CALL;
        OptionType = strrep(OptionType,'C','call');
        OptionType = strrep(OptionType,'P','put');
        
        %         TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today +1;
        TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today ; % TODO - added on 01-July-2014 for new TTM convention
        
        SettleVols = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, SettlePrice);
        
        % Check the NaN vols and ignore them before updating in excel,
        % since they cannot be uploaded in database
        NonNanIdx  = ~(cellfun(@isnan,SettleVols));
        Value_Date = repmat(DBValueDate,size(SettleVols(NonNanIdx)));
        
        SettleVolHeader = {'Value Date','VolId','Strike','Call_Put_Id','Settle Vol'};
        SettleVolData = [Value_Date,VolID(NonNanIdx),num2cell(Strike(NonNanIdx)),OptionData.OPT_PUT_CALL(NonNanIdx),SettleVols(NonNanIdx)];
        xlswrite(['Settlement_Vol_Surf_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettleVolHeader;SettleVolData]);
        
        
    end % end of option dump file selection
    
    
    
    %% Future dump parser
    if isnumeric(FutureFileName) && FutureFileName==0
        errordlg('Future Dump file not selected!','FTP Parser');
    else
        FileName = fullfile(FuturePathName,FutureFileName);
        
        waitbar(0.8,hWaitbar,'Please wait, parsing future dump file');
        
        FutureData = parse_future_file_updated(FileName,[MappingFieldNames;MappingData],BBG_UndId);
        % ignore the records for whcih the settlement date does not
        % match with the user entered settlement date
        % To find the invalid date entries that cannot be handled by
        % datenum function
        InvalidIdx = strcmpi(FutureData.SettlementDate,'N.A.') | strcmpi(FutureData.SettlementDate,'N.A') | ...
            strcmpi(FutureData.SettlementDate,'') | strcmpi(FutureData.SettlementDate,' ');
        FutureData.SettlementDate(InvalidIdx) = {'01/01/1900'};
        if isdeployed
            DumpSettleDate = cellstr(datestr(datenum(FutureData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
        else
            DumpSettleDate = cellstr(datestr(datenum(FutureData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
        end
        %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
        IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
        
        % underlying settle value computation
        FutureData.FutureName(IdxDate) = [];
        FutureData.FuturePrice(IdxDate)= [];
        FutureData.AssetClass(IdxDate)= [];
        
        FutureID = cell(size(FutureData.FutureName));
        for iCode = 1 : length(BBG_UndId)
            %  Idx = strmatch(BBG_UndId{iCode},FutureData.FutureName); %#ok<*MATCH2>
            Idx = strcmpi(BBG_UndId{iCode},FutureData.FutureName) & strcmpi(Asset_Class{iCode},FutureData.AssetClass);
            FutureID(Idx) = UndId(iCode);
        end
        
        %% Check FutureID or Asset Class  %% FutureData.AssetClass
        IdxNonEmpty = cellfun(@(s) (length(s)>1), FutureData.AssetClass) & cellfun(@(s) (length(s)>1), FutureID);  %% Product Code Match in Table & InputFiles not Asset Class
        
        FutureID  =  FutureID(IdxNonEmpty);
        FutureData.AssetClass = FutureData.AssetClass(IdxNonEmpty);        %% Remove Empty Asset Class Fileds
        FutureData.SettlementDate = FutureData.SettlementDate(IdxNonEmpty);
        FutureData.FuturePrice = FutureData.FuturePrice(IdxNonEmpty);
        
        % check for the redundant entries if at all present for the same
        % settlement date
        [~,UniqIdx,TempIdx] = unique(FutureID,'stable');
        TempVar = zeros(size(UniqIdx));
        Value_Date = repmat(DBValueDate,size(UniqIdx));
        % TODO - Below lines are to divide OR price by 100. Remove them
        % once it is handled in database
        IdxOR = strmatch('OR',FutureID(UniqIdx));
        %         FutPrice = str2double(FutureData.FuturePrice(UniqIdx));
        FutPrice = FutureData.FuturePrice(UniqIdx);
        FutPrice(IdxOR) = FutPrice(IdxOR)/100;
        % End of TODO
        UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
        %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(FutureData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
        UndSettleData = [FutureData.AssetClass,FutureID(UniqIdx),Value_Date,num2cell(FutPrice),num2cell(TempVar),...
            num2cell(TempVar),FutureData.SettlementDate];
        
        % write in xls file
        xlswrite(['underlying_settle_value_table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndSettleHeader;UndSettleData]);
        
        % Generate the traders surface data for every business unit in
        % the below segment of code; activate for every BU whenever
        % requested
        %         BUNames = {'cfs','cop','agf','qf1','qf2','qf3','qf4','qf5'};
        BUNames = {'ogp','usg','og2'};  % Remove agf and add ogp (05-07-2019) & added og2 on 2020-01-24
        for iBU = 1:length(BUNames)
            InBUName = BUNames{iBU};
            try
                %             [~,~,RawMapData] = xlsread('Suportfolio_Product_Mapping.xlsx');
                %             subportfolio_mapping = RawMapData(2:end,:);                
                
                SqlQuery = 'select distinct(subportfolio),product_code,directional_portfolio from subportfolio_product_mapping_table where product_code not like ''%USD%''';
                [~,subportfolio_mapping] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery,InBUName);
            catch
                errordlg('Product mapping data for subportfolio is not found! Hence underlying_traders_value file will not be created!');
                return;
            end
            
            num_pf = length(subportfolio_mapping);
            
            UndSettleHeader_Traders = UndSettleHeader;  %% Changing Future Data For Traders table Purpose
            UndSettleData_Traders = UndSettleData;
            UndSettleHeader_Traders(:,1) = [];
            UndSettleHeader_Traders(:,end) = [];
            UndSettleData_Traders(:,1) = [];
            UndSettleData_Traders(:,end) = [];
            
            UndTradersData =[];
            for iP = 1:num_pf
                pf_code = subportfolio_mapping{iP,2};
                subportfolio = subportfolio_mapping(iP,1);
                Index = strncmp(pf_code,UndSettleData_Traders(:,1),length(pf_code));
                temp_portfolio = repmat(subportfolio,size(find(Index)));
                temp_data = [temp_portfolio UndSettleData_Traders(Index,:)];
                UndTradersData = [UndTradersData;temp_data]; %#ok<AGROW>
                % to add data for directional portfolios
                dir_portfolio = subportfolio_mapping{iP,3};
                if ~isempty(dir_portfolio)
                    temp_dirportfolio = repmat(cellstr(dir_portfolio),size(find(Index)));
                    temp_dirdata = [temp_dirportfolio UndSettleData_Traders(Index,:)];
                    UndTradersData = [UndTradersData;temp_dirdata];
                end
            end
            UndTradersHeader = ['Subportfolio',UndSettleHeader_Traders];
            
            % write in xls file
            xlswrite(['underlying_traders_value_table_',InBUName,'_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndTradersHeader;UndTradersData]);
        end
        %% End of TODO
    end
    
    %% Equity File Parser
    
    if isnumeric(EquityFileName) && EquityFileName==0
        errordlg('Equity Dump file not selected!','FTP Parser');
    else
        Equity_FilePath = fullfile(EquityPathName,EquityFileName);
        [EquityData] = parse_equity_file(Equity_FilePath,[MappingFieldNames;MappingData],BBG_UndId);
        
        if(isempty(EquityData.FutureName))
            errordlg('Equity Dump file Data not Found in DB!','FTP Parser');
        else
            InvalidIdx = strcmpi(EquityData.SettlementDate,'N.A.') | strcmpi(EquityData.SettlementDate,'N.A') | ...
                strcmpi(EquityData.SettlementDate,'') | strcmpi(EquityData.SettlementDate,' ');
            EquityData.SettlementDate(InvalidIdx) = {'01/01/1900'};
            if isdeployed
                DumpSettleDate = cellstr(datestr(datenum(EquityData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
            else
                DumpSettleDate = cellstr(datestr(datenum(EquityData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
            end
            %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
            IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
            
            % underlying settle value computation
            EquityData.FutureName(IdxDate) = [];
            EquityData.FuturePrice(IdxDate)= [];
            EquityData.AssetClass(IdxDate)= [];
            
            EquityID = cell(size(EquityData.FutureName));
            for iCode = 1:length(BBG_UndId)
                %             Idx = strmatch(BBG_UndId{iCode},EquityData.FutureName); %#ok<*MATCH2>
                Idx = strcmp(BBG_UndId{iCode},EquityData.FutureName) & strcmpi(Asset_Class{iCode},EquityData.AssetClass);
                EquityID(Idx) = UndId(iCode);
            end
            
            IdxNonEmpty = cellfun(@(s) (length(s)>1), EquityID);  %% Product Code Match in Table & InputFiles not Asset Class
            EquityID  =  EquityID(IdxNonEmpty);
            EquityData.AssetClass = EquityData.AssetClass(IdxNonEmpty);        %% Remove Empty Asset Class Fileds
            EquityData.SettlementDate = EquityData.SettlementDate(IdxNonEmpty);
            EquityData.FuturePrice = EquityData.FuturePrice(IdxNonEmpty);
            
            
            % check for the redundant entries if at all present for the same
            % settlement date
            [~,UniqIdx,~] = unique(EquityID,'stable');
            TempVar = zeros(size(UniqIdx));
            Value_Date = repmat(DBValueDate,size(UniqIdx));
            % TODO - Below lines are to divide OR price by 100. Remove them
            % once it is handled in database
            IdxOR = strmatch('OR',EquityID(UniqIdx));
            %         FutPrice = str2double(EquityData.FuturePrice(UniqIdx));
            EquityPrice = EquityData.FuturePrice(UniqIdx);
            EquityPrice(IdxOR) =  EquityPrice(IdxOR)/100;
            % End of TODO
            UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
            %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(EquityData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
            UndSettleData = [EquityData.AssetClass,EquityID(UniqIdx),Value_Date,num2cell(EquityPrice),num2cell(TempVar),num2cell(TempVar),EquityData.SettlementDate];
            
            % write in xls file
            xlswrite(['underlying_equity_table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndSettleHeader;UndSettleData]);
        end
    end
    
    
    %% FX Future File Parser
    
    if isnumeric(FX_FutureFileName) && FX_FutureFileName==0
        errordlg('FX Future Dump file not selected!','FTP Parser');
    else
        FX_Future_FileName = fullfile(FX_FuturePathName,FX_FutureFileName);
        
        waitbar(0.8,hWaitbar,'Please wait, parsing future dump file');
        
        FX_FutureData = parse_fx_future_file(FX_Future_FileName,[MappingFieldNames;MappingData],BBG_UndId);
        
        if ( isempty(FX_FutureData.FutureName) && isempty(FX_FutureData.FuturePrice) )
            errordlg('FX Future Dump file is Does not have any Data  !','FTP Parser');
        else
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            % To find the invalid date entries that cannot be handled by
            % datenum function
            InvalidIdx = strcmpi(FX_FutureData.SettlementDate,'N.A.') | strcmpi(FX_FutureData.SettlementDate,'N.A') | ...
                strcmpi(FX_FutureData.SettlementDate,'') | strcmpi(FX_FutureData.SettlementDate,' ');
            FX_FutureData.SettlementDate(InvalidIdx) = {'01/01/1900'};
            if isdeployed
                DumpSettleDate = cellstr(datestr(datenum(FX_FutureData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
            else
                DumpSettleDate = cellstr(datestr(datenum(FX_FutureData.SettlementDate,'dd-mm-yyyy'),'yyyy-mm-dd'));
            end
            %       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
            IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
            
            % underlying settle value computation
            FX_FutureData.FutureName(IdxDate) = [];
            FX_FutureData.FuturePrice(IdxDate)= [];
            FX_FutureData.AssetClass(IdxDate)= [];
            
            FX_FutureID = cell(size(FX_FutureData.FutureName));
            for iCode = 1:length(BBG_UndId)
                %  Idx = strmatch(BBG_UndId{iCode},FutureData.FutureName); %#ok<*MATCH2>
                Idx = strcmp(BBG_UndId{iCode},FX_FutureData.FutureName) & strcmp(Asset_Class{iCode},FX_FutureData.AssetClass);
                FX_FutureID(Idx) = UndId(iCode);
            end
            
            IdxNonEmpty = cellfun(@(s) (length(s)>1), FX_FutureData.AssetClass);  %% Product Code Match in Table & InputFiles not Asset Class
            FX_FutureID  =  FX_FutureID(IdxNonEmpty);
            FX_FutureData.AssetClass = FX_FutureData.AssetClass(IdxNonEmpty);        %% Remove Empty Asset Class Fileds
            FX_FutureData.SettlementDate = FX_FutureData.SettlementDate(IdxNonEmpty);
            FX_FutureData.FuturePrice = FX_FutureData.FuturePrice(IdxNonEmpty);
            
            % check for the redundant entries if at all present for the same
            % settlement date
            [~,UniqIdx,~] = unique(FX_FutureID,'stable');
            FX_TempVar = zeros(size(UniqIdx));
            FX_Value_Date = repmat(DBValueDate,size(UniqIdx));
            % TODO - Below lines are to divide OR price by 100. Remove them
            % once it is handled in database
            FX_IdxOR = find(strncmp(FX_FutureID, 'OR', 2)); %%FX_IdxOR = strmatch('OR',FX_FutureID(UniqIdx));
            %         FutPrice = str2double(FutureData.FuturePrice(UniqIdx));
            FX_FutPrice = FX_FutureData.FuturePrice(UniqIdx);
            FX_FutPrice(FX_IdxOR) = FX_FutPrice(FX_IdxOR)/100;
            % End of TODO
            FX_UndSettleHeader = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
            %         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(FutureData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
            FX_UndSettleData = [FX_FutureData.AssetClass,FX_FutureID(UniqIdx),FX_Value_Date,num2cell(FX_FutPrice),num2cell(FX_TempVar),...
                num2cell(FX_TempVar),FX_FutureData.SettlementDate];
            
            % write in xls file
            xlswrite(['underlying_fx_future_table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[FX_UndSettleHeader;FX_UndSettleData]);
        end
        
    end
    %%
    
    waitbar(1,hWaitbar,'Parser completed');
    delete(hWaitbar);
    delete(gcp('nocreate'));
    msgbox(['Files are saved successfully in the directory ',pwd],'FTP Parser');
    
catch ME
    if ishandle(hWaitbar)
        delete(hWaitbar);
    end
    delete(gcp('nocreate'));
    errordlg(ME.message);
end
end