function DBTimeStamp = getCurrentDateTimestamp

 CurrentDateTime = strsplit(datestr(now),' ');
  Date  = CurrentDateTime{1};
Time = CurrentDateTime{2};
 DBTimeStamp = [datestr(Date,'yyyy-mm-dd'),' ',Time];
 
end