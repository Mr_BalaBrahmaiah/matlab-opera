function [UndTradersHeader,UndTradersData] = Get_Underlying_Traders_Value_Table(BUNames,UndSettleHeader,UndSettleData,MappingFieldNames,Mapping_Data,DBValueDate)

% Generate the traders surface data for every business unit in
% the below segment of code; activate for every BU whenever
% requested
% BUNames = {'cfs','cop','agf','qf1','qf2','qf3','qf4','qf5'};

%  BUNames = [];
% BUNames = {'cfs'};

for iBU = 1:length(BUNames)
    InBUName = BUNames{iBU};
    try
        %             [~,~,RawMapData] = xlsread('Suportfolio_Product_Mapping.xlsx');
        %             subportfolio_mapping = RawMapData(2:end,:);
        
        SqlQuery = 'select distinct(subportfolio),product_code,directional_portfolio from subportfolio_product_mapping_table where product_code not like ''%USD%''';
        [~,subportfolio_mapping] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery,InBUName);
    catch
        errordlg('Product mapping data for subportfolio is not found! Hence underlying_traders_value file will not be created!');
        return;
    end
    
    num_pf = length(subportfolio_mapping);
    
    UndSettleHeader_Traders = UndSettleHeader;  %% Changing Future Data For Traders table Purpose
    UndSettleData_Traders = UndSettleData;
    UndSettleHeader_Traders(:,1) = [];
    UndSettleHeader_Traders(:,end) = [];
    UndSettleData_Traders(:,1) = [];
    UndSettleData_Traders(:,end) = [];
    
    UndTradersData =[];
    for iP = 1:num_pf
        pf_code = subportfolio_mapping{iP,2};
        subportfolio = subportfolio_mapping(iP,1);
        Index = strncmp(pf_code,UndSettleData_Traders(:,1),length(pf_code));
        temp_portfolio = repmat(subportfolio,size(find(Index)));
        temp_data = [temp_portfolio UndSettleData_Traders(Index,:)];
        UndTradersData = [UndTradersData;temp_data]; %#ok<AGROW>
        % to add data for directional portfolios
        dir_portfolio = subportfolio_mapping{iP,3};
        if ~isempty(dir_portfolio)
            temp_dirportfolio = repmat(cellstr(dir_portfolio),size(find(Index)));
            temp_dirdata = [temp_dirportfolio UndSettleData_Traders(Index,:)];
            UndTradersData = [UndTradersData;temp_dirdata];
        end
    end
    UndTradersHeader = [{'Asset Class','Subportfolio'},UndSettleHeader_Traders];
    
    [ShortCode_Array,Remain] = strtok(UndTradersData(:,2),' ');
    [ProductCode_Array,dd] = strtok(Remain,' ');
    [AssetClass_Array] = Get_Asset_Class_4_CellArray(ProductCode_Array,ShortCode_Array,[MappingFieldNames;Mapping_Data]);
    
    UndTradersData = [AssetClass_Array,UndTradersData] ;
    
    % write in xls file
    xlswrite(['underlying_traders_fx_future_table_',InBUName,'_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndTradersHeader;UndTradersData]);
    
end