function Status = customcsvwrite(Filename,InData,Header)
% csvwrite to write cell array of inputs
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:16:58 $
%  $Revision: 1.1 $
%

% open csv file
fid = fopen(Filename,'wt');

% if header is present
if ~isempty(Header)
    HeaderFormat = repmat(cellstr('%s'),1,size(Header,2));
    HeaderFormat = strjoin(HeaderFormat,',');
    HeaderFormat = [HeaderFormat,'\n'];
    fprintf(fid,HeaderFormat,Header{1,:});
end

% get the contents of first row of actual data to get the data format to be
% written in csv file
RowData = InData(1,:);
Format = cell(1,size(InData,2));

for iR = 1:length(RowData)
    if isnumeric(RowData{iR})
        Format{iR} = '%f';
    elseif islogical(RowData{iR})
        Format{iR} = '%f';
    else        
        Format{iR} = '%s';
    end
end

Format = strjoin(Format,',');
Format = [Format,'\n'];
% write the data in csv file using the format
for i = 1:size(InData,1)
    fprintf(fid,Format,InData{i,:});
end

Status = fclose(fid);

end