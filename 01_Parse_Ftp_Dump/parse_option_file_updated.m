function [OutData] = parse_option_file_updated(FileName,BBG_OptCode)

% Read the rawdata from option dump file
RawData = textread(FileName,'%s','delimiter','\n');

% find the start and end of fields and data
Start_Of_Fields = find(strcmpi('START-OF-FIELDS',RawData));
End_Of_Fields = find(strcmpi('END-OF-FIELDS',RawData));
IdxFields = (Start_Of_Fields + 1) : (End_Of_Fields - 1);
FieldNames = RawData(IdxFields);

Start_Of_Data = find(strcmpi('START-OF-DATA',RawData));
End_Of_Data = find(strcmpi('END-OF-DATA',RawData));
IdxData = (Start_Of_Data + 1) : (End_Of_Data - 1);
InData = RawData(IdxData);

% clear the variables that will not be used anymore to avoid out of memory
% issues due to huge data handling
clear RawData; clear IdxFields; clear IdxData;

% find the indices in rawdata which matches with the bbg_optcode. This will
% ignore the products that are not traded by Olam
[FoundIndex] = Find_Index(InData,BBG_OptCode);

% split the records to match with each field data
if ~isempty(FoundIndex)
    for i = 1:length(FoundIndex)
        Data(i,:) = strsplit(InData{FoundIndex(i)},'|');
    end
    % data to be taken from 4th position to match with the order of  fields in the dump
    Data = Data(:,4:end-1);
    
    % consider the records in which the bloomberg ticker matches with the
    % bbg_opt_ticker from master table in database
    
    [FoundIndex] = Find_Index(Data(:,1),BBG_OptCode);
    
    
    OutData = cell2dataset([FieldNames';Data(FoundIndex,:)]);
else
    OutData = [];
end
clear Data;

end

%%
function  [Index_my] = Find_Index(InData,str_match)
Index_my = [];

if ~iscellstr(str_match)
    str_match = cellstr(str_match);
end

parfor i = 1:size(InData,1)
    
    Current_Line = InData{i};
    [Index] = Find_Index_Sub(i,Current_Line,str_match);
    Index_my = [Index_my;Index];
    
end
end

function [Index] = Find_Index_Sub(i,Current_Line,str_match)

Index = [];

parfor k = 1:length(str_match)
    
    %         idx = strncmp(InData{i},str_match{k},length(str_match{k}));
    
    if(strncmp(Current_Line,str_match{k},length(str_match{k})))
        
        Index = [Index;i];
        
    end
end
end