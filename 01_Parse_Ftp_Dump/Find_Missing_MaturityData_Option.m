function [OutputData] = Find_Missing_MaturityData_Option(InputData,SecurityIDFormat)  %% Input VolID Format Only

if(SecurityIDFormat)
    VolID_Format = cellfun(@(x) x(1:11), InputData, 'UniformOutput', false) ; %% Get Vol ID Format
else
    VolID_Format = InputData ;
end

%%
Common_Month_Code = cellfun(@(x) x(end-4:end), VolID_Format, 'UniformOutput', false);
Month_Code_1 = cellfun(@(x) x(end-1:end), VolID_Format, 'UniformOutput', false) ;
Month_Code_2 =  cellfun(@(x) x(7:8), VolID_Format, 'UniformOutput', false) ;


%%
MonthNames = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
MonthNumber = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11';'12'};

Current_Year = datestr(today,'yyyy');
Current_Year_LastStr = str2double(Current_Year(end));
if Current_Year_LastStr == 0 %%% Newly added logic on 02-01-2020 (for handling last character is ZERO)
    Current_Year_AddStr = str2double(Current_Year(end-1));
else
    Current_Year_AddStr = str2double(Current_Year(end-1)) + 1;
end

Current_Month = datestr(today,'mmm') ;

%%

for k = 1 : 2
    
    if(k==1)
        RawData = Month_Code_1;
        New_Underlying_ID_Array = cell(size(RawData,1),1);
        
    else
        RawData = Month_Code_2;
        New_Underlying_ID_Array_1 = New_Underlying_ID_Array;
        New_Underlying_ID_Array = cell(size(RawData,1),1);
    end
    
    for i = 1 : size(RawData,1)
        Current_UndId = RawData(i,1) ;
        Last_Character_Underlying_ID = str2double(cell2mat( cellfun(@(x) x(end), Current_UndId, 'UniformOutput', false) )); %% Year
        Month_Character_Underlying_ID = cellfun(@(x) x(end-1), Current_UndId, 'UniformOutput', false); %% Month
        
        if(Last_Character_Underlying_ID == 0)
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
            
        elseif((Last_Character_Underlying_ID < Current_Year_LastStr))
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
            
        elseif((Last_Character_Underlying_ID == Current_Year_LastStr))
            
            Underying_ID_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthCode , Month_Character_Underlying_ID))));
            Current_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthNames , {Current_Month} ) )));
            
            if(Underying_ID_MonthNumber < Current_MonthNumber)
                
                ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
                char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
                Last_2_char = str2double(char_last_2(:,end-1:end));
                if isnan(Last_2_char)
                    New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
                else
                    New_Underlying_ID_Array(i,1) = Current_UndId;  
                end 
                
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;
            end
       elseif((Last_Character_Underlying_ID > Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end     
        
        else
            
            New_Underlying_ID_Array(i,1) = Current_UndId;
        end
        
    end
    
end


%%

New_MaturityCode = strcat(New_Underlying_ID_Array,{'.'},New_Underlying_ID_Array_1);

OutputData = strrep(InputData ,Common_Month_Code ,New_MaturityCode);


