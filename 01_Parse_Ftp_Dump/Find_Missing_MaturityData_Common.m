function [Missing_UnderlyingID_Data,Missing_SettleValue_Data] = Find_Missing_MaturityData_Common(InData,Missing_Maturities,DBValueDate)

Header = {'asset_class','underlying_id','invenio_product_code','bbg_underlying_id','expiry_date','updated_by','updated_timestamp'};
Temp_Row_Data_1 = cell(1 , size(Header,2)) ;

Header_SettleValue = {'Asset_Class','Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
Temp_Row_Data_2 = cell(1 , size(Header_SettleValue,2)) ;

%%
try
    
    Matchced_MissingData_Index = cellStrfind_Perfect(InData(:,7),Missing_Maturities);
    Missing_Data = InData(Matchced_MissingData_Index,:);
    
    Asset_Class = Missing_Data(:,6);
    Underlying_ID = Missing_Data(:,7);
    Invenio_ProdCode = strtrim(cellfun(@(x) x(1:end-3), Missing_Data(:,7), 'UniformOutput', false));
    [bbg_UnderlyingID,bb] = strtok(Missing_Data(:,1),' ');
    Remove_Space_Str = strtrim(bb(:,1));
    Find_Max_Length = find(cellfun(@(s) (length(s)>6), Remove_Space_Str));
    
    if(~isempty(Find_Max_Length))
        Extra_Length_Str = char(Remove_Space_Str(Find_Max_Length));
        Extra_Length_Str = cellstr(Extra_Length_Str(:,1:2));
        
        bbg_UnderlyingID(Find_Max_Length) = strcat(bbg_UnderlyingID(Find_Max_Length),{' '},Extra_Length_Str);  %% Add MonthCode to Single Character Prodcode
        
    end
    
    %%
    
    MonthNames = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
    MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
    MonthNumber = {'1';'2';'3';'4';'5';'6';'7';'8';'9';'10';'11';'12'};
    
    Current_Year = datestr(today,'yyyy');
    Current_Year_LastStr = str2double(Current_Year(end));
    if Current_Year_LastStr == 0 %%% Newly added logic on 02-01-2020 (for handling last character is ZERO)
    Current_Year_AddStr = str2double(Current_Year(end-1));
    else
        Current_Year_AddStr = str2double(Current_Year(end-1)) + 1;
    end
    
    Current_Month = datestr(today,'mmm') ;
    
    New_Underlying_ID_Array = cell(size(Underlying_ID,1),1);
    
    for i = 1 : size(Underlying_ID,1)
        Current_UndId = Underlying_ID(i,1) ;
        Last_Character_Underlying_ID = str2double(cell2mat( cellfun(@(x) x(end), Current_UndId, 'UniformOutput', false) ));
        Month_Character_Underlying_ID = cellfun(@(x) x(end-1), Current_UndId, 'UniformOutput', false);
        
        if(Last_Character_Underlying_ID == 0)
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
            
        elseif((Last_Character_Underlying_ID < Current_Year_LastStr))
            
            ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end 
            
        elseif((Last_Character_Underlying_ID == Current_Year_LastStr))
            
            Underying_ID_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthCode , Month_Character_Underlying_ID))));
            Current_MonthNumber = str2double(cell2mat(MonthNumber(cellStrfind_exact(MonthNames , {Current_Month} ) )));
            
            if(Underying_ID_MonthNumber < Current_MonthNumber)
                
                ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
                char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
                Last_2_char = str2double(char_last_2(:,end-1:end));
                if isnan(Last_2_char)
                    New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
                else
                    New_Underlying_ID_Array(i,1) = Current_UndId;  
                end 
                
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;
            end
       elseif((Last_Character_Underlying_ID > Current_Year_LastStr))
        
        ReplaceStr = strcat(num2str(Current_Year_AddStr),num2str(Last_Character_Underlying_ID));
            char_last_2 = cell2mat(Current_UndId);%%% Newly added logic on 06-01-2020
            Last_2_char = str2double(char_last_2(:,end-1:end));
            if isnan(Last_2_char)
                New_Underlying_ID_Array(i,1) =  strrep(Current_UndId ,num2str(Last_Character_Underlying_ID) ,ReplaceStr);
            else
                New_Underlying_ID_Array(i,1) = Current_UndId;  
            end     
        
        else
            
            New_Underlying_ID_Array(i,1) = Current_UndId;
        end
        
    end
    
    %%
    Expiry_Date = cell(size(Missing_Data,1),1); %%cellstr(datestr(datenum(Missing_Data(:,5),'dd-mm-yyyy'),'yyyy-mm-dd'));
    Expiry_Date(:,:) = {''};
    
    Temp = cell(size(Missing_Data,1),2);  %% Create Default Cell arrays
    Updated_Str = 'system';
    Date_Str = datestr(now);
    Temp(:,1) = {Updated_Str};
    Temp(:,2) = {Date_Str};
    
    RowData = [Asset_Class,New_Underlying_ID_Array,Invenio_ProdCode,bbg_UnderlyingID,Expiry_Date,Temp];  %% Underlying_ID
    
    Missing_UnderlyingID_Data = [Header;RowData];
    
    %%
    
    Value_Date = repmat(DBValueDate,size(Asset_Class));
    try
        Settlement_Date = cellstr(datestr(datenum(Missing_Data(:,5),'dd-mm-yyyy'),'yyyy-mm-dd'));
    catch
        Settlement_Date = cellstr(datestr(datenum(Missing_Data(:,5),'mm/dd/yyyy'),'yyyy-mm-dd'));
    end
    Settle_Value = Missing_Data(:,4);
    
    Temp_1 = cell(size(Missing_Data,1),2);
    Temp_1(:,:) = cellstr(num2str(0));
    
    RowData_Missing_SettleValue = [Asset_Class,New_Underlying_ID_Array,Value_Date,Settle_Value,Temp_1,Settlement_Date];  %% Underlying_ID
    
    Missing_SettleValue_Data = [Header_SettleValue;RowData_Missing_SettleValue];
    
catch ME
    errordlg(ME.message);
    Error_Log_FileName_Line_Number(ME)
    
    Missing_UnderlyingID_Data = [Header ; Temp_Row_Data_1] ;
    Missing_SettleValue_Data = [Header_SettleValue ; Temp_Row_Data_2] ;
    
    %     fprintf('Find Missing Maturiy Function');
    
    
end

end
