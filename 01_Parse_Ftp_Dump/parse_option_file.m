function OutData = parse_option_file(FileName,BBG_OptCode)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:24:36 $
%  $Revision: 1.1 $
%

% Read the rawdata from option dump file 
RawData = textread(FileName,'%s','delimiter','\n');

% find the start and end of fields and data
Start_Of_Fields = find(strcmpi('START-OF-FIELDS',RawData));
End_Of_Fields = find(strcmpi('END-OF-FIELDS',RawData));
IdxFields = (Start_Of_Fields + 1) : (End_Of_Fields - 1);
FieldNames = RawData(IdxFields);

Start_Of_Data = find(strcmpi('START-OF-DATA',RawData));
End_Of_Data = find(strcmpi('END-OF-DATA',RawData));
IdxData = (Start_Of_Data + 1) : (End_Of_Data - 1);
InData = RawData(IdxData);

% clear the variables that will not be used anymore to avoid out of memory
% issues due to huge data handling
clear RawData; clear IdxFields; clear IdxData;

% form the search pattern using the bbg_optcode from master table in
% database
SearchPattern = strjoin(strcat('^',BBG_OptCode,'.')','|');

% find the indices in rawdata which matches with the bbg_optcode. This will
% ignore the products that are not traded by Olam
Index = regexp(InData,SearchPattern);
TmpIndex = ~(cellfun(@isempty,Index));
FoundIndex = find(TmpIndex);

% split the records to match with each field data
if ~isempty(FoundIndex)
    for i = 1:length(FoundIndex)
        Data(i,:) = strsplit(InData{FoundIndex(i)},'|');
    end
    % data to be taken from 4th position to match with the order of  fields in the dump
    Data = Data(:,4:end-1);
    
    % consider the records in which the bloomberg ticker matches with the
    % bbg_opt_ticker from master table in database
    Index = regexp(Data(:,1),SearchPattern);
    FoundIndex = ~(cellfun(@isempty,Index));
    
    OutData = cell2dataset([FieldNames';Data(FoundIndex,:)]);
else
    OutData = [];
end
clear Data;


end




