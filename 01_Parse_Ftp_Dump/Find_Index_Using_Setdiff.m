%% Future
% load('Find_Index_Future.mat')
% 
% [Index_my] = Find_Index(FutureData,BBG_UndId);
% Correct_Matched_Future = strtrim(FutureData(:,2));
% Correct_Matched_Future = Correct_Matched_Future(Index_my);
% %%$
% BBG_UndId = strtrim(BBG_UndId);
% Compare_Cell_Future = strtrim(FutureData(:,2));
% [C_Future,Index_Future] = setdiff(Compare_Cell_Future,BBG_UndId,'sorted');
% Compare_Cell_Future(Index_Future) = [];

%%

load('Find_Index_Equity.mat')

[Index_my] = unique(Find_Index(EQD_Data,BBG_UndId));
Correct_Matched_Equity = strtrim(EQD_Data(:,2));
Correct_Matched_Equity = Correct_Matched_Equity(Index_my);

%%$
BBG_UndId = strtrim(BBG_UndId);
Compare_Cell_Equity = strtrim(EQD_Data(:,2));


[C_Equity,Index_Equity] = setdiff(BBG_UndId,Compare_Cell_Equity,'stable');
BBG_UndId(Index_Equity) = [];
Compare_Cell_Equity = sortrows(BBG_UndId(:,1));

%% Equity using Mapping
  
% [aa,bb] = strtok(EQD_Data(:,1));
% Remove_Space_Str = strtrim(bb(:,1));
% Find_Max_Length = find(cellfun(@(s) (length(s)>6), Remove_Space_Str));
% Extra_Length_Str = char(Remove_Space_Str(Find_Max_Length));
% Extra_Length_Str = cellstr(Extra_Length_Str(:,4:end));
% Remove_Space_Str(Find_Max_Length,:) = Extra_Length_Str;
% 
% [C_Equity,Index_Equity] = setdiff(Compare_Cell_Equity,BBG_UndId,'sorted') ;
% [Ind_cmdty,Index_cmdty] = setdiff(Compare_Cell_Equity,Mapping_Data(:,2),'sorted');
% 
% [CorrectEqty,CorrectInx] = setdiff(Ind_cmdty,C_Equity,'sorted');
% 
% Compare_Cell_Equity = CorrectEqty;


%%
% load('Find_Index_FX_Future.mat')
% [Index_my] = Find_Index(FutureData,BBG_UndId);
% Correct_Matched_FX_Future = FutureData(:,2);
% Correct_Matched_FX_Future = Correct_Matched_FX_Future(Index_my);
% 
% %%$
% BBG_UndId = strtrim(BBG_UndId);
% Compare_Cell_FX = strtrim(FutureData(:,2));
% 
% [C_FX,Index_FX] = setdiff(Compare_Cell_FX,BBG_UndId,'sorted');
% Compare_Cell_FX(Index_FX) = [];



