function [NettedTRData,ErrorData,Total_Data] = Get_TR_Format_QF5(InBUName,DBFieldNames,DBData,Instrument_Code,DBPortfolio,DBProduct_Code)

global ObjDB ;

NettedTRData = [];
ErrorData = [];
Total_Data = [];

%%  Transaction Number
%         ObjDB = connect_to_database;

TempTradeId = '';                               %% Transaction Number
try
    [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
    if strcmpi(OutErrorMsg,'No Errors')
        % TradeIdPrefix = 'TR-FY16-17-';
        SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
        TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
        TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
        
        if(isempty(TempTradeId))
            TempTradeId = 0;      %% Default
        end
    end
catch
    TempTradeId = '';
end


%% Excel Sheet Write

%     TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
%         'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
%         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
%         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
%         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
%         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};

TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
    'OptType','Prem','CounterParty','Initial Lots',...
    'CONTRACTNB','Maturity','Barrier Type',...
    'Barrier','Transaction Date','TRN.Number','Source',...
    'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};

Temp = cell(length(DBProduct_Code),1);
Temp(:,:) = num2cell(NaN); %% Temp(:,:) = {''};  %% Temp(:,:) = num2cell(NaN);

BuySell_Col = cellfun(@(V) strcmpi('trade_side_name',V), DBFieldNames);
BuySell = strtrim(DBData(:,BuySell_Col));
Buy_Index = (~cellfun(@isempty,regexp(BuySell,'Buy')));   %% Changing in BuySell Variable Buy into bought & Sell into sold
Sell_Index = (~cellfun(@isempty,regexp(BuySell,'Sell')));
BuySell(Buy_Index) = {'Bought'};
BuySell(Sell_Index) = {'Sold'};

Lots_Col = cellfun(@(V) strcmpi('trade_qty',V), DBFieldNames);  %% Actice Lots ans Initial Lots
Lots_Data = DBData(:,Lots_Col);
IdxSell = strcmpi(BuySell,'Sold');
Lots_Data(IdxSell) = num2cell(cell2mat(Lots_Data(IdxSell)) .* -1);

Maturity_Trades = Temp ; %% strcat(Month_Code,'.',Month_Code);

OptType = cell(length(DBProduct_Code),1);
OptType(:,:) = {'future'};

Premium_Col = cellfun(@(V) strcmpi('trade_price',V), DBFieldNames);  %% Avg Premium and Prem
Premium = DBData(:,Premium_Col);

CounterParty_Col = cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);   %% CounterParty and Exe-Broker as same
CounterParty = DBData(:,CounterParty_Col);

TraderName_Col = cellfun(@(V) strcmpi('trader_name',V), DBFieldNames);
TraderName = DBData(:,TraderName_Col);

TransactionDate_Col = cellfun(@(V) strcmpi('trade_date',V), DBFieldNames);
ActualTransactionDate = DBData(:,TransactionDate_Col);
[TransactionDate,Timing] = strtok(ActualTransactionDate);

% TransactionDate = cellfun(@(V) datestr(V), TransactionDate,'UniformOutput',false);
% TransactionDate = cellfun(@(V) x2mdate(V), TransactionDate,'UniformOutput',false);   %% m2xdate %% x2mdate
% formatOut = 'yyyy-mm-dd';
% datestr(TransactionDate,formatOut)

Source = cell(length(DBProduct_Code),1);   %% Get Database Name
Source(:,:) = {'TraderDB'};

Exe_Type = cell(length(DBProduct_Code),1);   %% Get Database Name
Exe_Type(:,:) = {'Elec'};

Rates = cell(length(DBProduct_Code),1);   %% Get Database Name
Rates(:,:) = {'Normal'};

%% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType

[~,~,RawData] = xlsread('Mapping_derivativeType.xls');
Header = RawData(1,:);
Data = RawData(2:end,:);

for k = 1 : length(Data)
    Current_Instrument = strtrim(Data{k,1});
    Current_OptType = strtrim(Data{k,2});
    MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
    
    if(~isempty(MatchedIndex))
        OptType(MatchedIndex,1) = cellstr(Current_OptType);
    end
    
end


%% WRITE EXCEL SHEET

%     RowData = [Instrument_Code,DBPortfolio,BuySell,Lots_Data,Maturity_Trades,Temp,OptType,Premium,CounterParty,Temp,Lots_Data,Temp,Premium,...
%         Temp,Temp,Temp,Temp,TransactionDate,Temp,Temp,Source,Temp,Temp,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];

RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
    Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp,TraderName,ActualTransactionDate];

Total_Data = [TRHeader;RowData];

UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date'};
SumFields = {'Initial Lots'};
NettedTRData = calc_netted_data(TRHeader, RowData,TRHeader,UniqueFields,SumFields,[]);

IdxEmpty = find(cellfun(@(s) (length(s)<=1), NettedTRData(:,1)));  %%  Find Empty Instrument
ErrorData = NettedTRData(IdxEmpty,:);   %% Move Empty Rows to Errors Sheet
ErrorData = [TRHeader;ErrorData];       %% Add Header

NettedTRData(IdxEmpty,:) = [];      %% Find Empty Instrument and Remove Empty rows in NettedTRData

if ~isempty(TempTradeId)
    PosTrnId = strcmpi('TRN.Number',TRHeader);
    PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
    NumRows = size(NettedTRData,1) - 1; % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
    NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
    TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
    TradeId = strrep(TradeId,' ','');  %% Remove Spaces
    NettedTRData(2:end,PosTrnId) = TradeId;
    NettedTRData(2:end,PosParentTrnId) = TradeId;
end

%% Changing Instrument KMA to KMA1

if(size(NettedTRData,1) > 1)
    
    Instrument_Col = strcmpi('Instrument',TRHeader);
    Changed_Instrument = strrep( NettedTRData(2:end,Instrument_Col) ,'KMA INDEX FUTURE' ,'KMA1 INDEX FUTURE');
    NettedTRData(2:end,1) = Changed_Instrument ;
    
end


