function Main_PostgresSql_Global(SqlQuery,InBUName,InputDates)

%%
% [DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_all_portfolio',0); %% 'trades_today_all_portfolio' %% 'trades_history_all_portfolio'

% Out = strtok(DBData(:,1),' ');
% UniqueDate = unique(Out);

global ObjDB ;
ObjDB = connect_to_database;

global ObjDB_PostGreSQL;
ObjDB_PostGreSQL = connect_to_database_PostgreSQL;

%% SQL

ViewName = 'helper_quandfund_subpf_mapping_view';  %% helper_settle_pricing_subportfoliovalues_view
[MappingColNames,MappingData] = read_from_database(ViewName,0);

%% PostgreSQL

% SqlQuery = 'select * from invenio_read_only.trades_history_all_portfolio where trade_date::text like ''2016-11-09%'' and portfolio_name = ''QF1''';

% [DBFieldNames,DBData] = read_from_database_postgreSQL('trades_history_all_portfolio',0,SqlQuery);
[DBFieldNames,DBData] = Fetch_DB_Data(ObjDB_PostGreSQL,SqlQuery);

%%% Newly added code (22-05-2019) --.  include subaccount tag
[~,~,counterparty_RawData] = xlsread('counterparty_mapping.xlsx');
counterparty_Data = counterparty_RawData(2:end,:);
 
Index_match = strcmpi(InBUName,counterparty_Data(:,3));

if any(Index_match)
    [row,~] = size(DBData);
    for i = 1:1:row
        Single_account = DBData(i,9);
        Index_account= strcmpi(Single_account,counterparty_Data(:,1));
        if any(Index_account)
            New_counterparty = counterparty_Data(Index_account,2);
            DBData(i,7) = New_counterparty;
        end
    end
end
%%% End Code

%%Preprocessing for PostgreSQL Data

if(length(DBData)>1)
    
    if(strcmpi(InBUName,'QF5'))
        Identifier_Col =  cellfun(@(V) strcmpi('identifier',V), DBFieldNames);
        Include_Dot_Prod_Index = cellStrfind(DBData(:,Identifier_Col),{'.'});
        DBData_Dot = DBData(Include_Dot_Prod_Index,:);
        DBData(Include_Dot_Prod_Index,:) = [];
        
        Missing_Identifier = [];
        Unique_Identifier = unique(DBData_Dot(:,Identifier_Col)) ;
        Instrument_Data = cell(size(DBData_Dot,1),1);
        Portfolio_Data = cell(size(DBData_Dot,1),1);
        ProductCode_Data = cell(size(DBData_Dot,1),1);
        for iPF = 1:size(Unique_Identifier,1)
            Current_Identifier = Unique_Identifier(iPF);
            SqlQuery_Dot = ['select instrument,portfolio,product_code from subportfolio_product_mapping_table_qf5 where instrument=''',char(Current_Identifier),''' '];
            
            try
                IdxPF = strcmpi(Unique_Identifier{iPF},DBData_Dot(:,Identifier_Col));
                [ColNames,Instrument_Portfolio] = Fetch_DB_Data(ObjDB,SqlQuery_Dot);
                Instrument_Data(IdxPF,:) = Instrument_Portfolio(1);
                Portfolio_Data(IdxPF,:) = Instrument_Portfolio(2);
                ProductCode_Data(IdxPF,:) = Instrument_Portfolio(3);
            catch ME
                Missing_Identifier = [Missing_Identifier ; Current_Identifier];
                continue;
            end
        end
        
        [NettedTRData_Dot,ErrorData_Dot,Total_Data_Dot] = Get_TR_Format_QF5(InBUName,DBFieldNames,DBData_Dot,Instrument_Data,Portfolio_Data,ProductCode_Data) ;
    end
    
    %% Existing Code
    Identifier_Col =  cellfun(@(V) strcmpi('identifier',V), DBFieldNames);
    Identifier = DBData(:,Identifier_Col);
    
    [ProductCode_Month, BBG_Code] = strtok(Identifier,' ');
    
    IdxSingle = find(cellfun(@(s) (length(s)<=1), ProductCode_Month));
    Change_BBG_Code = BBG_Code(IdxSingle);
    [Change_Month_code,Remain_BBG_Code] = strtok(Change_BBG_Code,' ');
    
    BBG_Code(IdxSingle) = Remain_BBG_Code;
    BBG_Code = strtrim(BBG_Code);
    
    Change_ProductCoode = ProductCode_Month(IdxSingle);
    
    ProductCode_Month(IdxSingle) = cellstr([char(Change_ProductCoode),char(Change_Month_code)]);
    
    Month_Code = cellfun(@(x) x(end-1:end), ProductCode_Month, 'UniformOutput', false);   %% Get Month Code Only
    
    %%%% Newly added code
    %%% Logic:- Find the emphty month data
    Find_NumberIndex = arrayfun(@(x) isempty(str2num(char(x))), Month_Code, 'UniformOutput', false);
  
    IndexMonth = find(~(cell2mat(Find_NumberIndex)) & ~false);
  
    Month_Code(IndexMonth) = cellfun(@(x) strcat(x(end-2),x(end)), (ProductCode_Month(IndexMonth)), 'UniformOutput', false);
    
    ProductCode_Month(IndexMonth) = cellfun(@(x) strcat(x(1:end-2),x(end)), (ProductCode_Month(IndexMonth)), 'UniformOutput', false);
    
   %%% End code
    
    ProductCode = ProductCode_Month;
    for i = 1 : length(ProductCode)
        ProductCode{i}((end-1:end)) = [];             %% Get Product Code Only
    end
    
    %% Mapping SQL Data vs PostgersSQL Data
    
    Map_ProdCol =  cellfun(@(V) strcmpi('product_code',V), MappingColNames);
    Map_ProductCode = MappingData(:,Map_ProdCol);
    
    Map_BBGCol =  cellfun(@(V) strcmpi('bbg_code',V), MappingColNames);
    Map_BBGCode = MappingData(:,Map_BBGCol);
    
    Map_DBProductCol =  cellfun(@(V) strcmpi('db_product_code',V), MappingColNames);
    Map_DBProductCode = MappingData(:,Map_DBProductCol);
    
    DBProduct_Code = cell(length(ProductCode),1);          %% Create Empty cell Array
    DBProduct_Code(:,:) = {' '};
    
    for k = 1 : length(ProductCode)
        Current_ProductCode = strtrim(ProductCode{k});
        Current_BBGCode = strtrim(BBG_Code{k});
        Map_DBProductCode = strtrim(Map_DBProductCode);
        
        MatchedIndex = find(strcmpi(Current_ProductCode, Map_ProductCode) & strcmpi(Current_BBGCode, Map_BBGCode));
        
        if(~isempty(MatchedIndex))
            DBProduct_Code(k,1) = cellstr(strtrim(Map_DBProductCode(MatchedIndex)));
        end
        
    end
    
    DBPortfolio_Col = cellfun(@(V) strcmpi('portfolio_name',V), DBFieldNames);
    DBPortfolio = strtrim(DBData(:,DBPortfolio_Col));   %% Get Portfolio Name from PostgreSQL DB
    
    %% Mapping QF1 Support Portfolio vs DB Product Code and DB Portfolio For Getting Instrument
    
    %     InBUName = 'qf1';
    SqlQuery2 = 'select instrument,subportfolio,product_code from subportfolio_product_mapping_table where product_code not like ''%USD%''';
    [Tbl_DBFieldNames,Tbl_DBData] = read_from_database('subportfolio_product_mapping_table',0,SqlQuery2,lower(InBUName));
    
    if(length(Tbl_DBData)>1)
        
        Tbl_InstrumentCol =  cellfun(@(V) strcmpi('instrument',V), Tbl_DBFieldNames);
        Tbl_Instrument = strtrim(Tbl_DBData(:,Tbl_InstrumentCol));
        
        Tbl_ProductCol =  cellfun(@(V) strcmpi('product_code',V), Tbl_DBFieldNames);
        Tbl_ProductCode = strtrim(Tbl_DBData(:,Tbl_ProductCol));
        
        Tbl_PortfolioCol =  cellfun(@(V) strcmpi('subportfolio',V), Tbl_DBFieldNames);
        Tbl_Portfolio = strtrim(Tbl_DBData(:,Tbl_PortfolioCol));
        
        Instrument_Code = cell(length(DBProduct_Code),1);
        Instrument_Code(:,:) = {' '};
        
        %%  Transaction Number
        %         ObjDB = connect_to_database;
        
        TempTradeId = '';                               %% Transaction Number
        try
            [OutErrorMsg,DBTradeId]= getLastTradeId(InBUName);
            if strcmpi(OutErrorMsg,'No Errors')
                % TradeIdPrefix = 'TR-FY16-17-';
                SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
                TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
                TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
                
                if(isempty(TempTradeId))
                    TempTradeId = 0;      %% Default
                end
            end
        catch
            TempTradeId = '';
        end
        %%
        %     TradeId = cell(length(DBProduct_Code),1); %% Add Transaction Number in OVerallData
        for i = 1 : length(DBProduct_Code)
            %% Add Transaction Number in OVerallData
            %             if ~isempty(TempTradeId)
            %                 TempTradeId = TempTradeId + 1;
            %                 TradeId{i} = [TradeIdPrefix,num2str(TempTradeId)];
            %
            %             end
            
            %%
            Current_ProductCode = strtrim(DBProduct_Code{i});
            Current_Portfolio = strtrim(DBPortfolio{i});
            
            MatchedIndex = find(strcmpi(Current_ProductCode, Tbl_ProductCode) & strcmpi(Current_Portfolio, Tbl_Portfolio));
            
            if(~isempty(MatchedIndex))
                Instrument_Code(i,1) = cellstr(strtrim(Tbl_Instrument(MatchedIndex)));
            else
                
            end
        end
        
        
        %% Excel Sheet Write
        
        %     TRHeader = {'Instrument','Portfolio','Buy Sell','Active Lots','Month','Strike',...
        %         'OptType','Prem','CounterParty','Sum of Init Pre','Initial Lots',...
        %         'Sum of Live quantity','Avg Premium','CONTRACTNB','Maturity','Barrier Type',...
        %         'Barrier','Transaction Date','TRN.Number','TRN.TYPOLOGY','Source','Expiry_Processed',...
        %         'Barrier 1 Hit?','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG'};
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
            'OptType','Prem','CounterParty','Initial Lots',...
            'CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','Source',...
            'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};
        
        Temp = cell(length(DBProduct_Code),1);
        Temp(:,:) = num2cell(NaN); %% Temp(:,:) = {''};  %% Temp(:,:) = num2cell(NaN);
        
        BuySell_Col = cellfun(@(V) strcmpi('trade_side_name',V), DBFieldNames);
        BuySell = strtrim(DBData(:,BuySell_Col));
        Buy_Index = (~cellfun(@isempty,regexp(BuySell,'Buy')));   %% Changing in BuySell Variable Buy into bought & Sell into sold
        Sell_Index = (~cellfun(@isempty,regexp(BuySell,'Sell')));
        BuySell(Buy_Index) = {'Bought'};
        BuySell(Sell_Index) = {'Sold'};
        
        Lots_Col = cellfun(@(V) strcmpi('trade_qty',V), DBFieldNames);  %% Actice Lots ans Initial Lots
        Lots_Data = DBData(:,Lots_Col);
        IdxSell = strcmpi(BuySell,'Sold');
        Lots_Data(IdxSell) = num2cell(cell2mat(Lots_Data(IdxSell)) .* -1);
        
        Maturity_Trades = strcat(Month_Code,'.',Month_Code);
        
        OptType = cell(length(DBProduct_Code),1);
        OptType(:,:) = {'future'};
        
        Premium_Col = cellfun(@(V) strcmpi('trade_price',V), DBFieldNames);  %% Avg Premium and Prem
        Premium = DBData(:,Premium_Col);
        
        CounterParty_Col = cellfun(@(V) strcmpi('broker_name',V), DBFieldNames);   %% CounterParty and Exe-Broker as same
        CounterParty = DBData(:,CounterParty_Col);
        
        TraderName_Col = cellfun(@(V) strcmpi('trader_name',V), DBFieldNames);
        TraderName = DBData(:,TraderName_Col);
        
        TransactionDate_Col = cellfun(@(V) strcmpi('trade_date',V), DBFieldNames);
        ActualTransactionDate = DBData(:,TransactionDate_Col);
        [TransactionDate,Timing] = strtok(ActualTransactionDate);
        
        % TransactionDate = cellfun(@(V) datestr(V), TransactionDate,'UniformOutput',false);
        % TransactionDate = cellfun(@(V) x2mdate(V), TransactionDate,'UniformOutput',false);   %% m2xdate %% x2mdate
        % formatOut = 'yyyy-mm-dd';
        % datestr(TransactionDate,formatOut)
        
        Source = cell(length(DBProduct_Code),1);   %% Get Database Name
        Source(:,:) = {'TraderDB'};
        
        Exe_Type = cell(length(DBProduct_Code),1);   %% Get Database Name
        Exe_Type(:,:) = {'Elec'};
        
        Rates = cell(length(DBProduct_Code),1);   %% Get Database Name
        Rates(:,:) = {'Normal'};
        
        %% INSTRUMENT VERIFY FOR FX-FUTURE and Changing OptType
        
        [~,~,RawData] = xlsread('Mapping_derivativeType.xls'); % 'Mapping_derivativeType.xls' % 'Mapping_fxfuture.xls'
        Header = RawData(1,:);
        Data = RawData(2:end,:);
        
        for k = 1 : length(Data)
            Current_Instrument = strtrim(Data{k,1});
            Current_OptType = strtrim(Data{k,2});
            MatchedIndex = find(strcmpi(Current_Instrument, Instrument_Code));
            
            if(~isempty(MatchedIndex))
                OptType(MatchedIndex,1) = cellstr(Current_OptType);
            end
            
        end
        
        
        %% WRITE EXCEL SHEET
        
        %     RowData = [Instrument_Code,DBPortfolio,BuySell,Lots_Data,Maturity_Trades,Temp,OptType,Premium,CounterParty,Temp,Lots_Data,Temp,Premium,...
        %         Temp,Temp,Temp,Temp,TransactionDate,Temp,Temp,Source,Temp,Temp,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp];
        
        RowData = [Instrument_Code,DBPortfolio,BuySell,Maturity_Trades,Temp,OptType,Premium,CounterParty,Lots_Data,...
            Temp,Temp,Temp,Temp,TransactionDate,Temp,Source,Temp,Temp,Temp,CounterParty,Exe_Type,Rates,Temp,Temp,Temp,TraderName,ActualTransactionDate];
        
        %Total_Data = [TRHeader;RowData];
        
        %%% Newly added code
        %%% Logic:- Delete the entire row, if Initial Lots = 0 (Date->27-02-2019 --> based on user requirement)
            Total_Data_dataset = cell2dataset([TRHeader;RowData]);
            Index_lots_zeros = eq(Total_Data_dataset.InitialLots,0);
            Total_Data_dataset(Index_lots_zeros,:) = [];
            Total_Data = dataset2cell(Total_Data_dataset);
            RowData = Total_Data(2:end,:);
        %%% End code
        
        UniqueFields = {'Instrument','Portfolio','Buy Sell','Month','Strike','OptType','Prem','CounterParty','Transaction Date'};
        SumFields = {'Initial Lots'};
        NettedTRData = calc_netted_data(TRHeader, RowData,TRHeader,UniqueFields,SumFields,[]);
        
        IdxEmpty = find(cellfun(@(s) (length(s)<=1), NettedTRData(:,1)));  %%  Find Empty Instrument
        ErrorData = NettedTRData(IdxEmpty,:);   %% Move Empty Rows to Errors Sheet
        ErrorData = [TRHeader;ErrorData];       %% Add Header
        
        NettedTRData(IdxEmpty,:) = [];      %% Find Empty Instrument and Remove Empty rows in NettedTRData
        
        if ~isempty(TempTradeId)
            PosTrnId = strcmpi('TRN.Number',TRHeader);
            PosParentTrnId = strcmpi('Parent Transaction Number',TRHeader);
            NumRows = size(NettedTRData,1) - 1; % -1 to exclude the header, as NetterTRData contains the header from calc_netted_data
            NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
            TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
            TradeId = strrep(TradeId,' ','');  %% Remove Spaces
            NettedTRData(2:end,PosTrnId) = TradeId;
            NettedTRData(2:end,PosParentTrnId) = TradeId;
        end
        
        %% Changing Instrument KMA to KMA1
        
        if(size(NettedTRData,1) > 1)
            
            Instrument_Col = strcmpi('Instrument',TRHeader);
            Changed_Instrument = strrep( NettedTRData(2:end,Instrument_Col) ,'KMA INDEX FUTURE' ,'KMA1 INDEX FUTURE');
            NettedTRData(2:end,1) = Changed_Instrument ;
            
        end
        
        %%  If QF5 Concatenate the With and Without Identifiers Data
        
        if(strcmpi(InBUName,'QF5'))
            NettedTRData = [NettedTRData ; NettedTRData_Dot(2:end,:)] ;
            
            [TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(NettedTRData(2:end,1),InBUName);
            
            TRN_Num_Col = cellStrfind_exact(TRHeader,{'TRN.Number'});
            ParTRN_Num_Col = cellStrfind_exact(TRHeader,{'Parent Transaction Number'});
            
            NettedTRData(2:end,TRN_Num_Col) = TradeId;
            NettedTRData(2:end,ParTRN_Num_Col) = TradeId;
            
            ErrorData = [ErrorData ; ErrorData_Dot(2:end,:)];
            Total_Data = [Total_Data ; Total_Data_Dot(2:end,:)] ;
        end
        
        %% Covert HH:MM:SS to HH:MM:SS:SSS
        
%         TimeStamp_Col = cellStrfind_exact(NettedTRData(1,:) , {'Traded DateTimestamp'});
%         [OutErrorMsg , DateTime_HHMMSSSSS] = convert_HHMMSS_2_HHMMSSSSS(NettedTRData(2:end,TimeStamp_Col));
%         NettedTRData(2:end,TimeStamp_Col) = DateTime_HHMMSSSSS;
        
        %% Write & Delete Excel Sheet
        %         TimeStamp = char(strrep(datestr(now,'HH:MM:SS'),':','_'));
        %         XLSFileName = [InBUName,'_Trade_Dump_',InputDates,'_',TimeStamp,'.xlsx'];
        XLSFileName = getXLSFilename(strcat(upper(char(InBUName)),'_Trade_Dump')) ;
        
        if(exist(XLSFileName))
            delete(XLSFileName);
            xlswrite(XLSFileName,NettedTRData,'Netted-TRData');
            xlswrite(XLSFileName,ErrorData,'Errors');
            xlswrite(XLSFileName,Total_Data,'OverallData');
            xls_delete_sheets([pwd filesep XLSFileName]);
        else
            xlswrite(XLSFileName,NettedTRData,'Netted-TRData');
            xlswrite(XLSFileName,ErrorData,'Errors');
            xlswrite(XLSFileName,Total_Data,'OverallData');
            xls_delete_sheets([pwd filesep XLSFileName]);
        end
        
        % Make Active Sheet
        SheetName = 'Netted-TRData';
        try
            Full_Path = [pwd filesep XLSFileName];
            xls_change_activesheet(Full_Path,SheetName);  %% Full Path Necessary..!!!!
        catch
        end
        
    else
        errordlg('No Data in Support Portfolio DB',InBUName);
        
    end
    
else
    
    errordlg(['No ',InBUName,' Data in DB'],InBUName);
    
end


end





