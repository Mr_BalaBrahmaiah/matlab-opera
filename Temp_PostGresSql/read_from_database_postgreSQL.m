function [DBFieldNames,DBData] = read_from_database_postgreSQL(InTableName,IsDateNeeded,varargin)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 06:15:07 $
%  $Revision: 1.6 $
%

DBFieldNames = [];
DBData       = [];

objDB = connect_to_database_PostgreSQL;

try
    javaaddpath('postgresql-9.4.1212.jre6.jar');
    % Read the database configuration file to read the DB configuration
    % Default DB config values
    DBName = 'invenio_ref';
    DBUserName = 'opera_reporting';
    DBPassword = 'Op3raRead0nly';
    IsFromServer = 1;
    DBServerIP = 'invproddb.olamdomain.com';
    Schema_Name = 'middle_office';
    DBPortNumber = 5433;
    try
        DBConfigData = textread('DatabaseConfiguration_PostGresSQL.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'schema_name');
        if ~isempty(IdxFound)
            [~,Schema_Name] = strtok(DBConfigData(IdxFound),'=');
            Schema_Name = char(strrep(Schema_Name,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'port_number');
        if ~isempty(IdxFound)
            [~,DBPortNumber] = strtok(DBConfigData(IdxFound),'=');
            DBPortNumber = str2double(char(strrep(DBPortNumber,'=','')));
        end
        disp(['Using the database ',DBName]);
    catch
        disp('Error occured in reading the DatabaseConfiguration_PostGresSQL.txt, hence using the default configuration!');
    end
    
    %   objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.194');
    if IsFromServer
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','PostgreSQL','Server',DBServerIP,'PortNumber',DBPortNumber);
    else
        objDB = database(DBName,DBUserName,DBPassword,'Vendor','PostgreSQL','PortNumber',DBPortNumber);
    end
    
    if ~isempty(varargin) % if sql query is passed as an input
        IsQueryInput = 1;
    else
        IsQueryInput = 0;
    end
    if isconnection(objDB)
        if IsQueryInput
            SqlQuery = varargin{1};
        else
            if IsDateNeeded
                ValuationDate = fetch(objDB,'select value_date from valuation_date_table');
                SqlQuery = ['select * from `',char(Schema_Name),'.',char(InTableName),'` where value_date = ''',char(ValuationDate),''''];
            else
                SqlQuery = ['select * from ',char(Schema_Name),'.',char(InTableName)];
                %                 SqlQuery = ['select * from `',char(InTableName),'` where vol1_id = ''CT Z5.Z5'' and strike = 77 and opt_type = ''vanilla_call'' and maturity >= ''2014-12-24'''];% for CSO
                % SqlQuery = ['select * from `',char(InTableName),'` where security_id like ''sb%'' and opt_type not like ''future'' and maturity >= value_date'];
                %  SqlQuery = ['select * from `',char(InTableName),'` where p1_name = ''OR K5'''];
            end
        end
        
        curs = exec(objDB,SqlQuery);
        curs = fetch(curs);
        AttrObj = attr(curs);
        DBFieldNames = {AttrObj.fieldName};
        DBData = curs.Data;
        
        close(objDB);
    end
catch ME
    if isconnection(objDB)
        close(objDB);
    end
    errordlg(ME.message);
end

end