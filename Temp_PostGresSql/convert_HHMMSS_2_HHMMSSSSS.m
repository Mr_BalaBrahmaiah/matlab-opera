function [OutErrorMsg , DateTime_HHMMSSSSS] = convert_HHMMSS_2_HHMMSSSSS(DateTime_CellArray)

OutErrorMsg = {'No Error'};
DateTime_HHMMSSSSS = '';
try
    DateTime_HHMMSSSSS = cell(size(DateTime_CellArray,1),1);
    for i = 1 : size(DateTime_CellArray,1)
        
        Current_DateTime = DateTime_CellArray(i) ;
        
        DateTime_HHMMSSSSS(i) = cellstr( datestr(Current_DateTime,'yyyy-mm-dd HH:MM:SS.FFF') );
                
    end
    
catch ME
    
    OutErrorMsg = cellstr(ME.message);
end