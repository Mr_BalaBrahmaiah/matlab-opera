function [DBFieldNames,DBData,ObjDB_ErrorMsg] = Fetch_DB_Data(ObjDB,SqlQuery,varargin)

DBFieldNames = {'No Data'};
DBData = {''};
ObjDB_ErrorMsg = '';

%%

if(isempty(SqlQuery))
   NewQuery = ['select * from ',char(varargin)];
   SqlQuery =  NewQuery;
end

%%
try
curs = exec(ObjDB,SqlQuery);
curs = fetch(curs);
AttrObj = attr(curs);
DBFieldNames = {AttrObj.fieldName};
DBData = curs.Data;

if ~isempty(curs.Message)
    ObjDB_ErrorMsg = {curs.Message};
end

catch ME
    DBFieldNames = {'Error Data'};
    DBData = {''};
end
end