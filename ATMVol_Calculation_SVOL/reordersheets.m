function reordersheets(excelfile)
%excelfile: full path of excel file whose sheet are to be reordered
%sheet names must ALL follow the EXACT pattern number_number,number
excel = actxserver('Excel.Application');  %start excel
clearobj = onCleanup(@() excel.Quit);   %Quit excel whenever the function exits
workbook = excel.Workbooks.Open(excelfile);  %open excel file
sheetnames = arrayfun(@(idx) workbook.Worksheets.Item(idx).Name, 1:workbook.Worksheets.Count, 'UniformOutput', false);  %get names of all worksheets
%     tokens = regexp(sheetnames, '^(\d+)_(\d+),(\d+)$', 'tokens', 'once');  %extract numbers from names
%     assert(all(~cellfun(@isempty, tokens)), 'At least one sheet name doesn''t conform to pattern');
%     tokens = str2double(vertcat(tokens{:}));   %convert number strings to actual numbers
%     [~, neworder] = sortrows(tokens, 'descend');   %get new order
id_sh1=find(strcmpi( sheetnames ,'Sheet1'));
id_va1=find(strcmpi( sheetnames ,'Var_price'));
id_sh2=find(strcmpi( sheetnames ,'Sheet2'));

if ~isempty(id_sh2)
    ordered=[  sheetnames( id_sh1) sheetnames( id_va1) sheetnames( id_sh2)] ;
    lastsheet = workbook.Worksheets.Item(ordered{3});
    for sheetname = ordered(2:end-1)
        currentsheet = workbook.Sheets.Item(sheetname{1});
        currentsheet.Move(lastsheet);  %move sheet before previous sheet
        lastsheet = currentsheet;
    end
else
    ordered=[sheetnames( id_sh1) sheetnames( id_va1)  ] ;
    lastsheet = workbook.Worksheets.Item(ordered{2});
    for sheetname = ordered(1:end-1)
        currentsheet = workbook.Sheets.Item(sheetname{1});
        currentsheet.Move(lastsheet);  %move sheet before previous sheet
        lastsheet = currentsheet;
    end
end
%     ordered = sheetnames(neworder);     %and reorder the names accordingly
%     lastsheet = workbook.Worksheets.Item(ordered{1});   %sheet before which to move current sheet

%     for sheetname = ordered(2:end-1)
%         currentsheet = workbook.Sheets.Item(sheetname{1});
%         currentsheet.Move(lastsheet);  %move sheet before previous sheet
%         lastsheet = currentsheet;
%     end
%     sheet2 = get(sheetname, 'Item', 1);
%     invoke(sheetnames{1}, 'Activate');

workbook.Save;  %save workbook
end
