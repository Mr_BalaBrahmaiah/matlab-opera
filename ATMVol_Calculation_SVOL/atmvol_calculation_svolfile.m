function varargout = atmvol_calculation_svolfile(varargin)
% ATMVOL_CALCULATION_SVOLFILE MATLAB code for atmvol_calculation_svolfile.fig
%      ATMVOL_CALCULATION_SVOLFILE, by itself, creates a new ATMVOL_CALCULATION_SVOLFILE or raises the existing
%      singleton*.
%
%      H = ATMVOL_CALCULATION_SVOLFILE returns the handle to a new ATMVOL_CALCULATION_SVOLFILE or the handle to
%      the existing singleton*.
%
%      ATMVOL_CALCULATION_SVOLFILE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ATMVOL_CALCULATION_SVOLFILE.M with the given input arguments.
%
%      ATMVOL_CALCULATION_SVOLFILE('Property','Value',...) creates a new ATMVOL_CALCULATION_SVOLFILE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before atmvol_calculation_svolfile_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to atmvol_calculation_svolfile_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help atmvol_calculation_svolfile

% Last Modified by GUIDE v2.5 09-Jan-2020 16:21:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @atmvol_calculation_svolfile_OpeningFcn, ...
    'gui_OutputFcn',  @atmvol_calculation_svolfile_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before atmvol_calculation_svolfile is made visible.
function atmvol_calculation_svolfile_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to atmvol_calculation_svolfile (see VARARGIN)

% Choose default command line output for atmvol_calculation_svolfile
handles.output = hObject;
myimage= imread('olam_logo.jpg');
axes(handles.axes1);
imshow(myimage);
set(handles.axes1,'Units','normalized');
% Update handles structure

[~,DBData] = read_from_database('valuation_date_table',0,'SELECT settlement_date FROM valuation_date_table');
handles.Dbdate=DBData;
set(handles.edit2,'string',DBData);
guidata(hObject, handles);

% UIWAIT makes atmvol_calculation_svolfile wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = atmvol_calculation_svolfile_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Browse_file.
function Browse_file_Callback(hObject, eventdata, handles)
% hObject    handle to Browse_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile('*.xlsx', 'Pick an excel-file');
if isequal(filename,0)
    msgbox('User selected Cancel')
else
    fullfilepath=fullfile(pathname, filename);
    set(handles.edit1,'string',fullfilepath);
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ex_button.
function ex_button_Callback(hObject, eventdata, handles)
% hObject    handle to ex_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% fetching values and file path from edit boxes
[jObj] = Job_Start();
drawnow;




try
    % reading Settelement Date 
    Settlement_Date=get(handles.edit2,'String');
    ObjDB = connect_to_database;
    sqlquery =['select value_date from vdate_sdate_table where settlement_date = ''',char(Settlement_Date),''' '];
    [~,DBData] = Fetch_DB_Data(ObjDB,sqlquery ,[]);
    if ~strcmp(Settlement_Date ,'')
        % finding selected radio button
        selected_button=get(handles.uibuttongroup2,'SelectedObject');
        if strcmpi(selected_button.String,'CALL')
            selected_option='C';
        else
            selected_option='P';
        end
        
        % finding input file and path for reading data and if file not
        % selected it will through error
        file_path=get(handles.edit1,'String');
        dest_file=get(handles.ed_destfile,'String');
        if ~strcmpi(file_path,'Browsed File') &&  ~strcmpi( dest_file,'Browsed File')
            
            [~,~,raw]=xlsread(file_path);
            
            % read destination file data
            
              [~,~,raw_dest]=xlsread(dest_file);
              % end of readinf
            invenio_code=raw(2:end);
            try
                h = waitbar(0,'fetching data from database...');
                ObjDB = connect_to_database;
                
                %% reading data from underlying list table for finding bbg_code
                
                SqlQuery = ['select bbg_underlying_id,underlying_id from underlying_list_table where expiry_date >= ''',char(Settlement_Date),''' '];
                
                [columns_unlist_table,Data_unlist_table] = Fetch_DB_Data(ObjDB,SqlQuery ,[]);
                underlyinglist_Data = cell2dataset([columns_unlist_table;Data_unlist_table]);
                [ first_unde,second_under] = strtok( underlyinglist_Data .underlying_id,' ' );
                [first_unde1,~]=strtok(second_under,' ');
                underlyinglist_Data.invcode=strcat(first_unde,{' '} ,first_unde1);
                clear columns_unlist_table Data_unlist_table;
                
                %% reading data from underlying settele value table
                
                SqlQuery = ['select underlying_id,settle_value,settlement_date from underlying_settle_value_table where settlement_date = ''',char(Settlement_Date),''''  ' and asset_class=''commodity'''];
                [columns_setttable,Data_setttable] = Fetch_DB_Data(ObjDB,SqlQuery ,[]);
                settelement_Data = cell2dataset([columns_setttable;Data_setttable]);
                [ first_sett,second_sett] = strtok( settelement_Data.underlying_id,' ' );
                [first1_sett,~]=strtok(second_sett,' ');
                settelement_Data.invcode=strcat(first_sett,{' '} ,first1_sett );
                clear columns_setttable Data_setttable;
                
                %%  reading data from settlement_price_table and taking only call data
                
                SqlQuery=[' select security_id, settlement_price from settlement_price_table where settlement_date = ''' ,char(Settlement_Date),''' '];
                [columns_sepricetable,Data_sepricetable] = Fetch_DB_Data(ObjDB,SqlQuery ,[]);
                settelprice_Data = cell2dataset([columns_sepricetable;Data_sepricetable]);
                
                %%% for taking only call data from security id in settelprice data
                [firstpart_seid,secondpart_seid]=strtok(settelprice_Data.security_id,'.');
                [cdata_seid,cstrike]=strtok(secondpart_seid,' ');
                settelprice_Data.strike=str2double(strtrim(cstrike));
                settelprice_Data.firstpart_seid=firstpart_seid;
                
                %%% here choosing call data or put data if C means call data P means Put
                %%% data
                index_c=endsWith(cdata_seid,selected_option);
                settelprice_Data_required=settelprice_Data(index_c,:);
                
                [ first,second] = strtok( settelprice_Data_required.firstpart_seid,' ' );
                [first1,~]=strtok(second,' ');
                settelprice_Data_required.invcode=strcat(first,{' '} ,first1 );
                clear columns_sepricetable Data_sepricetable;
                
                %% reading data from settlement_vol_surf_table and filtering data based on call/put
                SqlQuery=['select vol_id,strike,call_put_id ,settle_vol from settlement_vol_surf_table where value_date = ''',char(DBData),''' '];
                [surfacetable_column,Data_surfacetable] = Fetch_DB_Data(ObjDB,SqlQuery ,[]);
                surfacetable_Data=cell2dataset([surfacetable_column;Data_surfacetable]);
                [ first_vol,~] = strtok(surfacetable_Data.vol_id,'.' );
                surfacetable_Data.invcode=first_vol;                
                surfacereq_id=strcmpi(surfacetable_Data.call_put_id,selected_option);                
                requiredsurface_data=dataset2cell(surfacetable_Data(surfacereq_id,:));
                clear volid_column Data_volid;
                close(ObjDB);
                waitbar(.45,h,'Processing your data');
                pause(1)
                %%    reading data from vol_id_table for expiry date
                
%                 SqlQuery=['select underlying_id,expiry_date ,vol_id from vol_id_table where expiry_date >= ''',char(Settlement_Date),''' '];
%                 [volid_column,Data_volid] = Fetch_DB_Data(ObjDB,SqlQuery ,[]);
%                 volid_Data=cell2dataset([volid_column;Data_volid]);
%                 [ first_vol,~] = strtok(volid_Data.vol_id,'.' );
%                 % [first_vol1,~]=strtok(second_vol,' ');
%                 volid_Data.invcode=first_vol;
%                 clear volid_column Data_volid;
%                 
%                 close(ObjDB);
%                 waitbar(.45,h,'Processing your data');
%                 pause(1)
                %% calculating the price based on invenio code
                total_data=[];
                missing_id=[];
                for c=1:length(invenio_code)
                    current_code=invenio_code{c};
                    tf_setteledata=ismember(settelement_Data.invcode, current_code);
                    required_setteledata=settelement_Data(tf_setteledata,:);
%                     tf_settelprice_Data=ismember(settelprice_Data_required.invcode, current_code);
%                     cal_setteleprice_data=[ num2cell(settelprice_Data_required.strike(tf_settelprice_Data)) num2cell(settelprice_Data_required.settlement_price(tf_settelprice_Data))...
%                         settelprice_Data_required.firstpart_seid(tf_settelprice_Data)];
                    final_data=[];
                    % getting data based on underlying id and predit the
                    % data
                    for k=1:size(required_setteledata,1)
                        current_undid=required_setteledata.underlying_id(k);
                        tf_cur_unid=ismember(requiredsurface_data(:,5),current_undid);
                        training_data=cell2mat(requiredsurface_data(tf_cur_unid,[2 4]));
                        if ~isempty( training_data) && size(training_data,1)>1
                            % finding value for settle_value data
                            predit_price= interp1(training_data(:,1), training_data(:,2), required_setteledata.settle_value(k) ,'linear', 'extrap');
                            final_data=[final_data;  current_undid required_setteledata.settle_value(k) predit_price];
                        else
                            missing_id=[missing_id;strcat('The data is not available for selected_' ,current_undid)];
                            %             disp(['The data is not available for selected' current_undid]);
                            continue
                        end
                    end
                    total_data=[total_data ;final_data];
                end
                
%                 headers={'under_id','assetprice', 'strike', 'optiontype' ,'maturity_date','timeshift','modeoption_price'};
%                 
                for s=1:size( total_data,1)
                    current_un=total_data(s,1);
                    tf_bbg=ismember(underlyinglist_Data.underlying_id,current_un);
                    bbg_code(s,1)=underlyinglist_Data.bbg_underlying_id(tf_bbg);
                end
                
                total_data(:,1)=bbg_code;
%                 final_total=[headers;total_data];
                
                % calculating SettleVols
%                 SettleVols = vanillaimpvolbybs(final_total(2:end,2), final_total(2:end,3), final_total(2:end,4), final_total(2:end,5), str2double('-1'), final_total(2:end,7));
                final_headers={'bbg_undid', 'Settele_date','strike','settle_vol'};
                %%% settleVols multiply by 100
                total_data(:,3)=cellfun(@(x) x*100,total_data(:,3),'un',0);
                settel_date(1:size(total_data,1))=Settlement_Date;
                data_write=[bbg_code settel_date' total_data(:,2:3) ];
                final_data_write=[final_headers;data_write]; 
                
                % filling data based on underlying_id
                raw_dest(2:end,4)={''};
                for g=1:size(final_data_write,1)
                    cu_bbg =final_data_write(g,1);
                    idx_se=ismember(raw_dest(:,1),cu_bbg );
                    raw_dest( idx_se,4)=final_data_write(g,4);
                end
                % TempName = ['ATM_Vol','_',char(strrep(Settlement_Date,'-','_'))];
%                 TempName = ['ATM_Vol','_',char(Settlement_Date)];
%                 filename=[TempName,'.xlsx'];
                xlswrite(dest_file,raw_dest,'sheet1');
                if ~isempty(missing_id)
                    xlswrite(dest_file,missing_id,'sheet2');
                end
                reordersheets(dest_file);
                xls_change_activesheet(dest_file,'sheet1')
                waitbar(1,h,'completed');
             
              
                Job_Done(jObj);
            catch ME
                disp( cellstr(ME.message));
                close(h)
            end
        else
            errordlg('Select Excel file having invenio code','Error');
        end
    else
        errordlg('Provide Settlement data in yyyy-mm-dd format','Error');
    end
catch ME
    disp( cellstr(ME.message));
    disp('Excel File is not selected for calculating vols');
    
end



function ed_destfile_Callback(hObject, eventdata, handles)
% hObject    handle to ed_destfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ed_destfile as text
%        str2double(get(hObject,'String')) returns contents of ed_destfile as a double


% --- Executes during object creation, after setting all properties.
function ed_destfile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ed_destfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pu_dest.
function pu_dest_Callback(hObject, eventdata, handles)
% hObject    handle to pu_dest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.xlsx', 'Pick an excel-file');
if isequal(filename,0)
    msgbox('User selected Cancel')
else
    destfile=fullfile(pathname, filename);
    set(handles.ed_destfile,'string',destfile);
end