function Output = vanillaoptskewadjdelta(AssetPrice, Strike, OptionType, Volatility_1, Volatility_2, MaturityDate, TimeShift, FractionChange)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:06:29 $
%  $Revision: 1.1 $

    NumInputs = size(AssetPrice);
    
    
    if isscalar(FractionChange)
        FractionChange = ones(NumInputs)*FractionChange;
    end     
    
    validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'AssetPrice');
    validateattributes(Strike,                {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'Strike');
    validateattributes(Volatility_1,          {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'Volatility_1'); 
    validateattributes(Volatility_2,          {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'Volatility_2');
    validateattributes(MaturityDate,          {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'MaturityDate'); 
    validateattributes(FractionChange,        {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'vanillaoptskewadjdelta', 'FractionChange'); 

    Opt1 = vanillaoptsensbybs(AssetPrice.*(1-FractionChange), Strike, OptionType, Volatility_1, MaturityDate, TimeShift, 0, {'price'});
    Opt2 = vanillaoptsensbybs(AssetPrice.*(1+FractionChange), Strike, OptionType, Volatility_2, MaturityDate, TimeShift, 0, {'price'});
    Output = (Opt2-Opt1)./(2*FractionChange.*AssetPrice);

end
