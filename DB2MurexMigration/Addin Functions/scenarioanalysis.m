function Mat = scenarioanalysis(Spot_Col, TimeBump_Row, OptionMat, Param, Move, DayCount)
    
     [nRow nCol] = size(OptionMat);
     if nCol ~= 12
         throw('Unknown No of Column');
     end
    
     [iRow, t1] = size(Spot_Col);
     [t2, iCol] = size(TimeBump_Row);
     
     Mat = NaN(iRow, iCol);
     ValuationDate  = datenum(cell2mat(OptionMat(:,1)));
     Spot_Ref       = cell2mat(OptionMat(:,2));
     Lots           = cell2mat(OptionMat(:,4));
     Strike         = cell2mat(OptionMat(:,6));
     OptionType     = OptionMat(:,7);
     Expiry         = datenum(cell2mat(OptionMat(:,8)));
     Volatility     = cell2mat(OptionMat(:,10));
     Rate           = cell2mat(OptionMat(:,11));
     
     HIGHVALUE      = 100000000000;
     LOWVALUE       = 0.000000000001;
     
     if strcmp(Param, 'Gamma')
             for i = 1:iRow
                for j = 1:iCol
                    AssetPrice = Spot_Col(i,1)*ones(nRow,1);
                    Tau = (Expiry - ValuationDate +1 - TimeBump_Row(1,j) )/365;
                    Tau(Tau<=0) = HIGHVALUE;
                    Port_Gamma = blsgamma(AssetPrice, Strike, Rate, Tau, Volatility);
                    Mat(i,j) = sum(Lots.*Port_Gamma);
                end
             end   
     elseif strcmp(Param,'Theta') 
             for i = 1:iRow
                for j = 1:iCol
                    AssetPrice = Spot_Col(i,1)*ones(nRow,1);
                    Tau = (Expiry - ValuationDate +1 - TimeBump_Row(1,j))/365;
                    Tau(Tau<=0) = HIGHVALUE;
                    Port_Theta = blstheta(AssetPrice, Strike, Rate, Tau, Volatility, OptionType);
                    Mat(i,j) = sum(Lots.*Port_Theta);
                end
             end              
     elseif strcmp(Param,'BE')
             for i = 1:iRow
                for j = 1:iCol
                    AssetPrice = Spot_Col(i,1)*ones(nRow,1);
                    Tau = (Expiry - ValuationDate +1 - TimeBump_Row(1,j))/365;
                    Tau(Tau<=0) = HIGHVALUE;
                    Port_Gamma = blsgamma(AssetPrice, Strike, Rate, Tau, Volatility);
                    Port_Theta = blstheta(AssetPrice, Strike, Rate, Tau, Volatility, OptionType);
                    Sq_BE = (-2* sum(Lots.*Port_Theta))/(sum(Lots.*Port_Gamma));
                    
                    if Sq_BE < 0
                        Sq_BE = NaN;
                    end
                    
                    Mat(i,j) = sqrt(Sq_BE);
                end
             end                   
     elseif strcmp(Param,'Vega')
             for i = 1:iRow
                for j = 1:iCol
                    AssetPrice = Spot_Col(i,1)*ones(nRow,1);
                    Tau = (Expiry - ValuationDate +1 - TimeBump_Row(1,j))/365;
                    Tau(Tau<=0) = LOWVALUE;
                    Port_Vega = blsvega(AssetPrice, Strike, Rate, Tau, Volatility);
                    Mat(i,j) = sum(Lots.*Port_Vega);
                end
             end   
     elseif strcmp(Param, 'Daily P&L Potential')
             for i = 1:iRow
                for j = 1:iCol
                    AssetPrice = Spot_Col(i,1)*ones(nRow,1);
                    Tau = (Expiry - ValuationDate +1 - TimeBump_Row(1,j))/365;
                    Tau(Tau<=0) = HIGHVALUE;
                    Port_Gamma = blsgamma(AssetPrice, Strike, Rate, Tau, Volatility);
                    Port_Theta = bstheta(AssetPrice, Strike, Rate, Tau, Volatility, OptionType);
                    Mat(i,j) = 0.5*sum(Lots.*Port_Gamma)*Move*Move + sum(Lots.*Port_Theta)/DayCount;
                end
             end            
     else
             disp('Unknown Parameter type')
     end
end
