function OutPercentile = calculate_xl_percentile(InDataVector,PercentileValue)

Nan_Row =  cellfun(@(V) any(isnan(V(:))), num2cell(InDataVector));
InDataVector =  InDataVector(~Nan_Row); %% Without NaN Value

SortedData = sort(InDataVector);

N = length(InDataVector);

Index = ((PercentileValue / 100)  * (N-1))  +1;

k = floor(Index);
d = Index - k;

if(size(InDataVector,1) == 1)
    OutPercentile = SortedData(k) + (d * (SortedData(k) -  SortedData(k)));
else
    OutPercentile = SortedData(k) + (d * (SortedData(k+1) -  SortedData(k)));
end
