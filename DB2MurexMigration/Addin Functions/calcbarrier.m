function Output = calcbarrier(AssetPrice, Strike, BarrierLevel, Rebate, IsBarrierBreached, RFR, CostOfCarry, Volatility,...
    TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult,IsExpiryProcessed,TickSize,OutputSpecification)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/06/20 08:09:38 $
%  $Revision: 1.1 $

Output = [];

NumInputs = size(AssetPrice);
if isscalar(YearToDay_Mult)
    YearToDay_Mult = repmat(YearToDay_Mult,NumInputs);
end
Currency = repmat(cellstr('USD'),NumInputs);
ExchangeATMConvention = repmat(cellstr('trading'),NumInputs);
IdxCal = find(YearToDay_Mult==365);
ExchangeATMConvention(IdxCal) = cellstr('calendar'); %#ok<*FNDSB>

obj = EuropeanBarrierMertonReinerAndRubinsteinModel(AssetPrice, Strike, BarrierLevel, Rebate, IsBarrierBreached, RFR, CostOfCarry, Volatility, ...
    TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);

if ischar(OutputSpecification)
    OutputSpecification = cellstr(OutputSpecification);    
end

for iSpec = 1:length(OutputSpecification)
    Parameter = OutputSpecification{iSpec};
    switch lower(Parameter)
        case 'price'
            Output = [Output, obj.getprice];
        case {'delta','delta_1','delta1'}
            Output = [Output, obj.getdelta1]; %#ok<*AGROW>
        case {'gamma','gamma_11','gamma11'}
            Output = [Output, obj.getgamma11];
        case {'vega','vega_1','vega1'}
            Output = [Output, obj.getvega1];
        case 'theta'
            Output = [Output, obj.gettheta];            
    end
    
end



end