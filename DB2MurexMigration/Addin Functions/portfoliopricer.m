function Output = portfoliopricer(Inputmatrix, Inputheading, Outputheading)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:06:28 $
%  $Revision: 1.1 $

[nRow, ~] = size(Inputmatrix);

Output = cell(nRow, length(Outputheading));
Output(:,:) = {' '};

% if ~any(strcmpi(Inputheading,'Expiry_Processed')) 
%     errordlg('Error! Inputheading must contain Expiry_Processed', 'portfoliopricer', 'Modal')
%     return
% end
% if ~any(strcmpi(Inputheading,'ExchangeATMConvention')) 
%     errordlg('Error! Inputheading must contain ExchangeATMConvention', 'portfoliopricer', 'Modal')
%     return
% end
if ~any(strcmpi(Inputheading,'Lots'))
    errordlg('Error! Inputheading must contain Lots', 'portfoliopricer', 'Modal')
    return
end
if ~any(strcmpi(Inputheading,'Opt_Type')) 
    errordlg('Error! Inputheading must contain Opt_Type', 'portfoliopricer', 'Modal')
    return
end
% if ~any(strcmpi(Inputheading,'Opt_Periodicity'))  
%     errordlg('Error! Inputheading must contain Opt_Periodicity', 'portfoliopricer', 'Modal')
%     return
% end
if ~any(strcmpi(Inputheading,'Lot_Mult'))  
    errordlg('Error! Inputheading must contain Lot_Mult', 'portfoliopricer', 'Modal')
    return
end
if ~any(strcmpi(Inputheading,'Curr_Mult'))  
    errordlg('Error! Inputheading must contain Curr_Mult', 'portfoliopricer', 'Modal')
    return
end
if ~any(strcmpi(Inputheading,'P1_SettlePrice'))  
    errordlg('Error! Inputheading must contain P1_SettlePrice', 'portfoliopricer', 'Modal')
    return
end


% % Pos_Expiry_Processed = find(strcmp('Expiry_Processed',Inputheading));
Pos_Lots             = find(strcmp('Lots',            Inputheading));
Pos_Lot_Mult         = find(strcmp('Lot_Mult',        Inputheading));
Pos_Curr_Mult        = find(strcmp('Curr_Mult',       Inputheading));

Pos_Opt_Type         = find(strcmp('Opt_Type',        Inputheading));
% Pos_Opt_Periodicity  = find(strcmp('Opt_Periodicity', Inputheading));
Pos_P1_StartPrice    = find(strcmp('P1_SettlePrice',  Inputheading));
Pos_P2_StartPrice    = find(strcmp('P2_SettlePrice',  Inputheading));
Pos_Strike           = find(strcmp('Strike',          Inputheading));
Pos_RF_R1            = find(strcmp('RF_Rate_1',       Inputheading));
Pos_RF_R2            = find(strcmp('RF_Rate_2',       Inputheading));
Pos_P1_DivYld        = find(strcmp('P1_Div_Yield',    Inputheading));
Pos_P2_DivYld        = find(strcmp('P2_Div_Yield',    Inputheading));
Pos_P1_Vol           = find(strcmp('P1_Vol',          Inputheading));
Pos_P2_Vol           = find(strcmp('P2_Vol',          Inputheading));
Pos_Correl_12        = find(strcmp('Correl_12',       Inputheading));

% Pos_Opt_DayCount     = find(strcmp('Opt_DayCount',    Inputheading));
%Barrier Specific
Pos_Barrier_Type     = find(strcmp('Barrier_Type',         Inputheading));
Pos_Barrier_Obs_Type = find(strcmp('Barrier_Obs_Type',     Inputheading));
Pos_Barrier_1        = find(strcmp('Barrier1_Level',       Inputheading));
Pos_Rebate           = find(strcmp('Barrier1_Rebate',      Inputheading));
Pos_Barrier_1_Hit    = find(strcmp('IsBarrier1Breached',   Inputheading));
%
Pos_Compo_Type       = find(strcmp('Compo_Type',           Inputheading));


%Output Positions
Pos_Price    = find(strcmp('Price',                 Outputheading));
Pos_Delta_1  = find(strcmp('Delta_1',               Outputheading));
Pos_Delta_2  = find(strcmp('Delta_2',               Outputheading));
Pos_Gamma_11 = find(strcmp('Gamma_11',              Outputheading));
Pos_Gamma_22 = find(strcmp('Gamma_22',              Outputheading));
Pos_Gamma_12 = find(strcmp('Gamma_12',              Outputheading));
Pos_Gamma_21 = find(strcmp('Gamma_21',              Outputheading));
Pos_Vega_1   = find(strcmp('Vega_1',                Outputheading));
Pos_Vega_2   = find(strcmp('Vega_2',                Outputheading));
Pos_Theta    = find(strcmp('Theta',                 Outputheading));
Pos_Chi      = find(strcmp('Chi',                   Outputheading));
Pos_BE_1     = find(strcmp('BE_1',                  Outputheading));
Pos_BE_2     = find(strcmp('BE_2',                  Outputheading));

if ~isempty(Pos_Price)
    Outputheading(1,Pos_Price) = {'price'};
end
if ~isempty(Pos_Delta_1)
    Outputheading(1,Pos_Delta_1) = {'delta1'};
end
if ~isempty(Pos_Delta_2)
    Outputheading(1,Pos_Delta_2) = {'delta2'};
end
if ~isempty(Pos_Gamma_11)
    Outputheading(1,Pos_Gamma_11) = {'gamma11'};
end
if ~isempty(Pos_Gamma_12)
    Outputheading(1,Pos_Gamma_12) = {'gamma12'};
end
if ~isempty(Pos_Gamma_21)
    Outputheading(1,Pos_Gamma_21) = {'gamma12'};
end
if ~isempty(Pos_Gamma_22)
    Outputheading(1,Pos_Gamma_22) = {'gamma22'};
end
if ~isempty(Pos_Vega_1)
    Outputheading(1,Pos_Vega_1) = {'vega1'};
end
if ~isempty(Pos_Vega_2)
    Outputheading(1,Pos_Vega_2) = {'vega2'};
end
if ~isempty(Pos_Theta)
    Outputheading(1,Pos_Theta) = {'theta'};
end
if ~isempty(Pos_Chi)
    Outputheading(1,Pos_Chi) = {'chi'};
end
if ~isempty(Pos_BE_1)
    Outputheading(1,Pos_BE_1) = {'be1'};
end
if ~isempty(Pos_BE_2)
    Outputheading(1,Pos_BE_2) = {'be2'};
end
% General validations

ValidOptionTypes = {'future','future_spread','vanilla_put','vanilla_call','euro_up_in_call','euro_up_in_put','euro_up_out_call','euro_up_out_put','euro_down_in_call',...
    'euro_down_in_put','euro_down_out_call','euro_down_out_put','amer_up_in_call','amer_up_in_put','amer_up_out_call','amer_up_out_put','amer_down_in_call',...
    'amer_down_in_put','amer_down_out_call','amer_down_out_put'};

OptTypes = Inputmatrix(:, Pos_Opt_Type);
Idx_OptTypes = cellfun(@isnumeric, OptTypes);
OptTypes(Idx_OptTypes) = cellstr('');
OptionTypes = lower(OptTypes);

Idx_OptTypes = ismember(OptionTypes, ValidOptionTypes);
Output(~Idx_OptTypes, Pos_Price) = cellstr('Opt_Type not implemented!');
IdxFut     = strcmpi(OptionTypes, 'future');
IdxFutSpr  = strcmpi(OptionTypes, 'future_spread');
IdxVanilla = strcmpi(OptionTypes, 'vanilla_call') | strcmpi(OptionTypes, 'vanilla_put');
IdxSprOpt  = strcmpi(OptionTypes, 'spread_call')  | strcmpi(OptionTypes, 'spread_put');
IdxBarOpt  = strcmpi(OptionTypes, 'euro_up_in_call')  | strcmpi(OptionTypes, 'euro_up_in_put') | strcmpi(OptionTypes, 'euro_up_out_call')  | strcmpi(OptionTypes, 'euro_up_out_put') | ...
    strcmpi(OptionTypes, 'euro_down_in_call')  | strcmpi(OptionTypes, 'euro_down_in_put') | strcmpi(OptionTypes, 'euro_down_out_call')  | strcmpi(OptionTypes, 'euro_down_out_put') | ...
    strcmpi(OptionTypes, 'amer_up_in_call')  | strcmpi(OptionTypes, 'amer_up_in_put') | strcmpi(OptionTypes, 'amer_up_out_call')  | strcmpi(OptionTypes, 'amer_up_out_put') | ...
    strcmpi(OptionTypes, 'amer_down_in_call')  | strcmpi(OptionTypes, 'amer_down_in_put') | strcmpi(OptionTypes, 'amer_down_out_call')  | strcmpi(OptionTypes, 'amer_down_out_put');

% ExchangeATMConvention = utilgetvalidnumericarray(Inputmatrix(:, Pos_Expiry_Processed));
% Expiry_Processed      = utilgetvalidnumericarray(Inputmatrix(:, Pos_Expiry_Processed));
Lots                  = utilgetvalidnumericarray(Inputmatrix(:, Pos_Lots));
Lot_Mult              = utilgetvalidnumericarray(Inputmatrix(:, Pos_Lot_Mult));
Curr_Mult             = utilgetvalidnumericarray(Inputmatrix(:, Pos_Curr_Mult));
AssetPrice_1          = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_StartPrice));

% validateattributes(Expiry_Processed,      {'numeric'}, {'nonempty', 'binary', 'column'}, 'portfoliopricer', 'Expiry_Processed');
% validateattributes(ExchangeATMConvention, {'numeric'}, {'nonempty', 'binary', 'column'}, 'portfoliopricer', 'ExchangeATMConvention');

Idx_Lots         = Lots ~= 0 & ~isnan(Lots);
Idx_Lot_Mult      = Lot_Mult ~= 0 & ~isnan(Lot_Mult);
Idx_Curr_Mult     = Curr_Mult ~= 0 & ~isnan(Curr_Mult);
Idx_AssetPrice_1 = AssetPrice_1 ~= 0 & ~isnan(AssetPrice_1);
% Idx_Expiry_Processed = Expiry_Processed == 1;

if ~isempty(Pos_RF_R1)
    RF_Rate = utilgetvalidnumericarray(Inputmatrix(:, Pos_RF_R1));
    Idx_RF_Rate = ~isnan(RF_Rate);
end
if ~isempty(Pos_P2_StartPrice)
    AssetPrice_2 = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_StartPrice)); 
    Idx_AssetPrice_2 = AssetPrice_2 ~= 0 & ~isnan(AssetPrice_2);
end
if ~isempty(Pos_P1_DivYld)
    P1_DivYld = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_DivYld)); 
    Idx_P1_DivYld =~isnan(P1_DivYld);
end
if ~isempty(Pos_Strike)
    Strike_1 = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike)); 
    Idx_Strike_1 =~isnan(Strike_1);    
end
if ~isempty(Pos_P1_Vol)
    P1_Vol = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_Vol)); 
    Idx_P1_Vol =~isnan(P1_Vol);
end
if ~isempty(Pos_P2_Vol)
    P2_Vol = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_Vol)); 
    Idx_P2_Vol =~isnan(P2_Vol);
end
if ~isempty(Pos_Correl_12)
    Correl_12 = utilgetvalidnumericarray(Inputmatrix(:, Pos_Correl_12)); 
    Idx_Correl_12 =~isnan(Correl_12);
end
if ~isempty(Pos_TTM)
    TTM = utilgetvalidnumericarray(Inputmatrix(:, Pos_TTM)); 
    Idx_TTM =~isnan(TTM); 
end
if ~isempty(Pos_Barrier_1)
    Barrier_1 = utilgetvalidnumericarray(Inputmatrix(:, Pos_Barrier_1)); 
    Idx_Barrier_1 =~isnan(Barrier_1); 
end
if ~isempty(Pos_Rebate)
    Rebate = utilgetvalidnumericarray(Inputmatrix(:, Pos_Rebate)); 
    Idx_Rebate =~isnan(Rebate); 
end
if ~isempty(Pos_Barrier_1_Hit)
    Barrier_1_Hit = utilgetvalidnumericarray(Inputmatrix(:, Pos_Barrier_1_Hit)); 
    Idx_Barrier_1_Hit =~isnan(Barrier_1_Hit); 
end

if any(IdxFutSpr)
    if isempty(Pos_P2_StartPrice)
        Output(IdxFutSpr, Pos_Price)  = cellstr('Error! Inputheading must contain P2_StartPrice'); 
        IdxFutSpr = [];
    end
end

if any(IdxVanilla)
    if (isempty(Pos_RF_R1) || isempty(Pos_P1_DivYld) || isempty(Pos_Strike) || isempty(Pos_P1_Vol))
        Output(IdxVanilla, Pos_Price)  = cellstr('Error! One or more Input/s is/are missing: RF_Rate_1, P1_Div_Yield, Strike, P1_Vol'); 
        IdxVanilla = [];
    end
end

% if any(IdxSprOpt)
%     if (isempty(Pos_RF_R1) || isempty(Pos_Strike) || isempty(Pos_P1_Vol) || isempty(Pos_P2_Vol) || isempty(Pos_Correl_12))
%         Output(IdxSprOpt, Pos_Price)  = cellstr('Error! One or more Input/s is/are missing: RF_R, Strike_1, P1_Vol, P2_Vol, Correl_12'); 
%         IdxSprOpt = [];
%     end
% end
% 
% if any(IdxBarOpt)
%     if(isempty(Pos_RF_R1) || isempty(Pos_P1_DivYld) || isempty(Pos_Strike) || isempty(Pos_P1_Vol) || isempty(Pos_Barrier_1) || isempty(Pos_Rebate) || isempty(Pos_Barrier_1_Hit))
%         Output(IdxBarOpt, Pos_Price)  = cellstr('Error! One or more Input/s is/are missing: RF_R, P1_DivYld, Strike_1, P1_Vol, Barrier_1, Rebate, Barrier 1 Hit'); 
%         IdxBarOpt = [];
%     end
% end

Valid_Opt_Periodicity = {'on date', 'daily'};
Opt_Periodicity = Inputmatrix(:, Pos_Opt_Periodicity);
Idx_Opt_Periodicity = ismember(lower(Opt_Periodicity), Valid_Opt_Periodicity);
Output(~Idx_Opt_Periodicity, Pos_Price) = cellstr('Error! Opt_Periodicity should be "On Date" or "Daily"');
Idx_Opt_Periodicity = ismember(lower(Opt_Periodicity), {'on date'});

Valid_Opt_DayCount = {'trading', 'calendar'};
Opt_DayCount = Inputmatrix(:, Pos_Opt_DayCount);
Idx_Opt_DayCount = ismember(lower(Opt_DayCount), Valid_Opt_DayCount);
Output(~Idx_Opt_DayCount, Pos_Price) = cellstr('Error! Opt_DayCount should be "Trading" or "Calendar"');
Idx_Opt_DayCount = ismember(lower(Opt_DayCount), {'trading'});
YearToDay_Mult = zeros(size(Opt_DayCount));
YearToDay_Mult(Idx_Opt_DayCount) = 252;
YearToDay_Mult(~Idx_Opt_DayCount) = 365;

%% future
if any(IdxFut)
    IdxTmp = IdxFut;

    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp, Pos_Price)    = cellstr('Opt_Periodicity of type "Daily" is not implemented for Future');
        end
    end

    IdxTmp = IdxFut & Idx_Lots & Idx_AssetPrice_1 ;
    if any(IdxTmp)    
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(AssetPrice_1(IdxTmp));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(Lots(IdxTmp));
        end
    end
    if any(IdxTmp & Idx_Expiry_Processed)    
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(0);
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(0);
        end
    end
end
%% future_spread
if any(IdxFutSpr)
    IdxTmp = IdxFutSpr & ~Idx_Opt_Periodicity;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp, Pos_Price)    = cellstr('Opt_Periodicity of type "Daily" is not implemented for Future Spread');
        end
    end

    IdxTmp = IdxFutSpr & Idx_Lots & Idx_Opt_Periodicity & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_AssetPrice_2;
    if any(IdxTmp & ~Idx_Expiry_Processed)
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(AssetPrice_1(IdxTmp));
        end

        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(Lots(IdxTmp));
        end

        if ~isempty(Pos_Delta_2)
            Output(IdxTmp,Pos_Delta_2)  = num2cell(Lots(IdxTmp));
        end
    end

    if any(IdxTmp & Idx_Expiry_Processed)
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(0);
        end

        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(0);
        end

        if ~isempty(Pos_Delta_2)
            Output(IdxTmp,Pos_Delta_2)  = num2cell(0);
        end
    end
end

%% european options
if any(IdxVanilla)
    IdxTmp = IdxVanilla & ~Idx_Opt_Periodicity;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp, Pos_Price)    = cellstr('Opt_Periodicity of type "Daily" is not implemented for European Options/Vanilla');
        end
    end
    
    IdxTmp = strcmpi(OptionTypes, 'vanilla_call');
    OptionTypes(IdxTmp) = {'call'};
    IdxTmp = strcmpi(OptionTypes, 'vanilla_put');
    OptionTypes(IdxTmp) = {'put'};
    
    IdxTmp = (IdxVanilla  & Idx_Opt_Periodicity) & (Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_RF_Rate & Idx_P1_DivYld & Idx_Strike_1 & Idx_P1_Vol & Idx_TTM);
    if any(IdxTmp)    
        CoC_1 = zeros(size(RF_Rate));
        CoC_1(IdxTmp)        = RF_Rate(IdxTmp) - P1_DivYld(IdxTmp);    
        bsobj = VanillaOptionBlackScholesModel(AssetPrice_1(IdxTmp), Strike_1(IdxTmp), RF_Rate(IdxTmp), ...
            CoC_1(IdxTmp), P1_Vol(IdxTmp), TTM(IdxTmp), OptionTypes(IdxTmp), Lots(IdxTmp), Lot_Mult(IdxTmp), Curr_Mult(IdxTmp), YearToDay_Mult(IdxTmp), ExchangeATMConvention(IdxTmp), Expiry_Processed(IdxTmp));
        Output(IdxTmp,:) = helperoutputhandlingforpricerclasses(bsobj, Outputheading, sum(IdxTmp));    
    end
    
    IdxTmp = (IdxVanilla  & Idx_Opt_Periodicity) & ~(Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_RF_Rate & Idx_P1_DivYld & Idx_Strike_1 & Idx_P1_Vol & Idx_TTM);
    if any(IdxTmp)  
        Output(IdxTmp,Pos_Price) = cellstr('One or more inputs are invalid/empty');
    end
    
end

%% spread options
% if any(IdxSprOpt)
%     IdxTmp = IdxSprOpt & ~Idx_Opt_Periodicity;
%     if any(IdxTmp)
%         if ~isempty(Pos_Price)
%             Output(IdxTmp, Pos_Price)    = cellstr('Opt_Periodicity of type "Daily" is not implemented for Spread Options');
%         end
%     end   
%     IdxTmp = IdxSprOpt & Idx_Lots & Idx_Opt_Periodicity & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_AssetPrice_2 & Idx_RF_Rate  & Idx_Strike_1 & Idx_P1_Vol & Idx_P2_Vol & Idx_Correl_12 & Idx_TTM;
%     if any(IdxTmp)
%         sobj = SpreadOptionBjerksundModel(AssetPrice_1(IdxTmp), AssetPrice_2(IdxTmp), ...
%             Strike_1(IdxTmp), P1_Vol(IdxTmp), P2_Vol(IdxTmp), ...
%             Correl_12(IdxTmp), RF_Rate(IdxTmp), TTM(IdxTmp), OptionTypes(IdxTmp), 0, Expiry_Processed(IdxTmp));        
%         outputhandling(sobj, IdxTmp);
%     end  
% end
%% barrier options
if any(IdxBarOpt)
    IdxTmp = IdxBarOpt & ~Idx_Opt_Periodicity;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp, Pos_Price)    = cellstr('Opt_Periodicity of type "Daily" is not implemented for European Options/Vanilla');
        end
    end
    
    IdxTmp = (IdxBarOpt &  Idx_Opt_Periodicity) & (Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_RF_Rate & Idx_P1_DivYld & Idx_Strike_1 & Idx_P1_Vol & Idx_TTM & Idx_Barrier_1 & Idx_Rebate & Idx_Barrier_1_Hit);
    if any(IdxTmp)    
        CoC_1 = zeros(size(RF_Rate));
        CoC_1(IdxTmp)        = RF_Rate(IdxTmp) - P1_DivYld(IdxTmp); 
        obj = EuropeanBarrierMertonReinerAndRubinsteinModel(AssetPrice_1(IdxTmp), Strike_1(IdxTmp), Barrier_1(IdxTmp), Rebate(IdxTmp), Barrier_1_Hit(IdxTmp), RF_Rate(IdxTmp), ...
            CoC_1(IdxTmp), P1_Vol(IdxTmp), TTM(IdxTmp), OptionTypes(IdxTmp), Lots(IdxTmp), Lot_Mult(IdxTmp), Curr_Mult(IdxTmp), YearToDay_Mult(IdxTmp), ExchangeATMConvention(IdxTmp), Expiry_Processed(IdxTmp));
        Output(IdxTmp,:) = helperoutputhandlingforpricerclasses(obj, Outputheading, sum(IdxTmp)); 
    end
    
    IdxTmp = (IdxBarOpt &  Idx_Opt_Periodicity) & ~(Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_RF_Rate & Idx_P1_DivYld & Idx_Strike_1 & Idx_P1_Vol & Idx_TTM & Idx_Barrier_1 & Idx_Rebate & Idx_Barrier_1_Hit);
    
    if any(IdxTmp)  
        Output(IdxTmp,Pos_Price) = cellstr('One or more inputs are invalid/empty');
    end
    
end
end