function Output = interpolation(X, Y, OutX, Method, IsExtrapolationNeeded)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/05/23 10:09:15 $
%  $Revision: 1.2 $
%
   validateattributes(X,    {'numeric'}, {'nonempty'}, 'interpolation', 'X'); 
   validateattributes(Y,    {'numeric'}, {'nonempty'}, 'interpolation', 'Y');
   validateattributes(OutX, {'numeric'}, {'nonempty'}, 'interpolation', 'OutX');
   
   if ~(strcmpi(Method, 'linear') || strcmpi(Method, 'spline'))
       error('Method should be linear or spline');
   end
   
   if IsExtrapolationNeeded
       Output = interp1(X, Y, OutX, Method, 'extrap');
   else
       Output = interp1(X, Y, OutX, Method, NaN);
   end
   
end