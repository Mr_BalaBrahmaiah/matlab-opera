function Output = floatvolcurvespline(Spot, StdDevNum, RelativeVol, LeftSlope, RightSlope, Step, Strike)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:06:28 $
%  $Revision: 1.1 $
%
   validateattributes(Spot,        {'numeric'}, {'nonempty', 'positive',    'scalar'                          }, 'cubicsplinefloatvolcurve', 'Spot');
   validateattributes(StdDevNum,   {'numeric'}, {'nonempty', 'real',        'column'                          }, 'cubicsplinefloatvolcurve', 'StdDevNum');
   validateattributes(RelativeVol, {'numeric'}, {'nonempty', 'real',        'column', 'size', size(StdDevNum) }, 'cubicsplinefloatvolcurve', 'RelativeVol');
   validateattributes(LeftSlope,   {'numeric'}, {'nonempty', 'real',        'scalar'                          }, 'cubicsplinefloatvolcurve', 'LeftSlope');
   validateattributes(RightSlope,  {'numeric'}, {'nonempty', 'real',        'scalar'                          }, 'cubicsplinefloatvolcurve', 'RightSlope');
   validateattributes(Step,        {'numeric'}, {'nonempty', 'positive',    'scalar'                          }, 'cubicsplinefloatvolcurve', 'Step');
   validateattributes(Strike,      {'numeric'}, {'nonempty', 'nonnegative', 'column'                          }, 'cubicsplinefloatvolcurve', 'Strike');
   
   DAYCONVENTION = 252;
   
   Idx = StdDevNum == 0;
   rv  = RelativeVol;
   if sum(Idx) == 0
       error('ATM Volatility is not given');
   elseif sum(Idx) > 1
       error('More than one ATM Volatility is given');
   else
       rv(~Idx)         = 0.0;
       ATMVol           = sum(rv);
       RelativeVol(Idx) = 0.0;
       Vol              = ATMVol + RelativeVol;
   end
   
   InputStrike = (1 + (ATMVol/sqrt(DAYCONVENTION))*StdDevNum)*Spot;
   
   [~, MaxStrikeID] = max(InputStrike); 
   [~, MinStrikeID] = min(InputStrike);
   
   OutputVol = zeros(size(Strike));
   
   Idx = Strike > max(InputStrike);
   if any(Idx)
      OutputVol(Idx) = Vol(MaxStrikeID, 1) + (Strike(Idx)-InputStrike(MaxStrikeID,1))*RightSlope/Step; 
   end
   
   Idx = Strike < min(InputStrike);
   if any(Idx)
      OutputVol(Idx) = Vol(MinStrikeID, 1) + (Strike(Idx)-InputStrike(MinStrikeID,1))*LeftSlope/Step; 
   end
   
   Idx =  Strike <= max(InputStrike) & Strike >= min(InputStrike);
   if any(Idx)
       OutputVol(Idx) = interp1(InputStrike, Vol, Strike(Idx), 'spline', NaN);
   end
   
   Output = OutputVol;
end