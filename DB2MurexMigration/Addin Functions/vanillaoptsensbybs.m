function Output = vanillaoptsensbybs(AssetPrice, Strike, OptionType, Volatility, MaturityDate, TimeShift, ExchangeATMConvention, OutputSpecification)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:06:29 $
%  $Revision: 1.1 $

    ValuationDate = datenum(today()) + TimeShift - 1;
    
    % Assuming that we have taken care of date format in VBA
       
    VDN = ValuationDate;
    MDN = MaturityDate;
    V = Volatility;
    
    NumInputs = size(AssetPrice);
    
    if isscalar(ExchangeATMConvention)
        ExchangeATMConvention = ones(NumInputs)*ExchangeATMConvention;
    end    
    
    validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'vanillaoptsensbybs', 'AssetPrice');
    validateattributes(Strike,                {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptsensbybs', 'Strike');
    validateattributes(Volatility,            {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptsensbybs', 'Volatility'); 
    validateattributes(MaturityDate,          {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'vanillaoptsensbybs', 'MaturityDate'); 
    validateattributes(ExchangeATMConvention, {'numeric'}, {'nonempty', 'binary',      'column', 'size', NumInputs}, 'vanillaoptsensbybs', 'ExchangeATMConvention');
    
    
    if ischar(OutputSpecification)
        OutputSpecification = cellstr(OutputSpecification);
    end
    
    MaturityDate(MaturityDate <= ValuationDate) = ValuationDate+1; % To get rid of error message
    
    Volatility(Volatility==0.0) = .20;

    Rates = zeros(length(AssetPrice),1);
    StartDates = ValuationDate*ones(length(AssetPrice),1);

    RateSpec = intenvset('StartDates', StartDates, 'EndDates', MaturityDate, 'Rates', Rates, 'Compounding', -1, 'Basis', 10);

    StockSpec = stockspec(Volatility, AssetPrice);

    NumOutputs = length(OutputSpecification);
    
    switch NumOutputs
        case 1
             [Output1] = optstocksensbybls(RateSpec, StockSpec, ValuationDate, MaturityDate, OptionType, Strike, 'OutSpec', OutputSpecification);
             Output = maturityadjustment(OutputSpecification, Output1);
        case 2 
             [Output1, Output2] = optstocksensbybls(RateSpec, StockSpec, ValuationDate, MaturityDate, OptionType, Strike, 'OutSpec', OutputSpecification);
             Output1 = maturityadjustment(OutputSpecification{1}, Output1);
             Output2 = maturityadjustment(OutputSpecification{2}, Output2);
             Output = [Output1, Output2];
        case 3
            [Output1, Output2, Output3] = optstocksensbybls(RateSpec, StockSpec, ValuationDate, MaturityDate, OptionType, Strike, 'OutSpec', OutputSpecification);
             Output1 = maturityadjustment(OutputSpecification{1}, Output1);
             Output2 = maturityadjustment(OutputSpecification{2}, Output2);            
             Output3 = maturityadjustment(OutputSpecification{3}, Output3);
             Output = [Output1, Output2, Output3];
        case 4 
            [Output1, Output2, Output3, Output4] = optstocksensbybls(RateSpec, StockSpec, ValuationDate, MaturityDate, OptionType, Strike, 'OutSpec', OutputSpecification);
             Output1 = maturityadjustment(OutputSpecification{1}, Output1);
             Output2 = maturityadjustment(OutputSpecification{2}, Output2);            
             Output3 = maturityadjustment(OutputSpecification{3}, Output3);      
             Output4 = maturityadjustment(OutputSpecification{4}, Output4);        
             Output = [Output1, Output2, Output3, Output4];
        case 5
            [Output1, Output2, Output3, Output4, Output5] = optstocksensbybls(RateSpec, StockSpec, ValuationDate, MaturityDate, OptionType, Strike, 'OutSpec', OutputSpecification);
             Output1 = maturityadjustment(OutputSpecification{1}, Output1);
             Output2 = maturityadjustment(OutputSpecification{2}, Output2);            
             Output3 = maturityadjustment(OutputSpecification{3}, Output3);      
             Output4 = maturityadjustment(OutputSpecification{4}, Output4);               
             Output5 = maturityadjustment(OutputSpecification{5}, Output5);   
            Output = [Output1, Output2, Output3, Output4, Output5];
        otherwise
            error('OutputSpecification should cotains five Outputs headers only');
    end
    
    function Out = maturityadjustment(Ospec, Ovec)
        switch lower(char(Ospec))
            case 'price'
                 Idx = (strcmpi(OptionType,'call')) & ((MDN <= VDN) | (V==0));
                 if any(Idx)
                    Ovec(Idx) = max(AssetPrice(Idx) - Strike(Idx),0);
                 end
                 Idx = (strcmpi(OptionType,'put')) & ((MDN <= VDN) | (V==0));
                 if any(Idx)
                    Ovec(Idx) =  max(Strike(Idx) - AssetPrice(Idx),0);
                 end                
            case 'delta'
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'call')) & ((AssetPrice-Strike)<0); 
                if any(Idx)
                    Ovec(Idx) = 0;
                end
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'call')) & ((AssetPrice-Strike)>0); 
                if any(Idx)
                    Ovec(Idx) = 1;
                end
                
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'call')) & ((AssetPrice-Strike)==0 & ExchangeATMConvention); % ATM = ITM
                if any(Idx)
                    Ovec(Idx) = 1;
                end
                
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'call')) & ((AssetPrice-Strike)==0 & ~ExchangeATMConvention); % ATM = OTM
                if any(Idx)
                    Ovec(Idx) = 0;
                end
                
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'put')) & ((Strike-AssetPrice)<0); 
                if any(Idx)
                    Ovec(Idx) = 0;
                end
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'put')) & ((Strike-AssetPrice)>0); 
                if any(Idx)
                    Ovec(Idx) = -1;
                end   
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'put')) & ((AssetPrice-Strike)==0 & ExchangeATMConvention); % ATM = ITM
                if any(Idx)
                    Ovec(Idx) = -1;
                end
                Idx = ((MDN <= VDN) | (V==0)) & (strcmpi(OptionType,'put')) & ((AssetPrice-Strike)==0 & ~ExchangeATMConvention); % ATM = OTM
                if any(Idx)
                    Ovec(Idx) = 0;
                end                
            case 'gamma'
                Ovec((MDN <= VDN) | (V==0)) = 0.0;
            case 'vega'
                Ovec((MDN <= VDN) | (V==0)) = 0.0;
            case 'theta'
                Ovec((MDN <= VDN) | (V==0)) = 0.0;
            otherwise
                error('Unknown Output Sprecification type');
        end
        Out = Ovec;        
    end
    
end

