function Output = derivativepricer_previous_settle_vols(InBUName,Inputmatrix, Inputheading, Outputheading,varargin)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/29 10:15:22 $
%  $Revision: 1.21 $

[nRow, ~] = size(Inputmatrix);

Output = cell(nRow, length(Outputheading));
Output(:,:) = {' '};
% Output(:,:) = {0}; % TO make the default output as zero

if nargin == 5
    if varargin{1} == 1 % if the query is from sensitivity report
        UpdateFlag = 0;
    else % if the query is from any part of pricing function and to update all prices
        UpdateFlag = 1;
    end
else % if the query is from addin or if to price partial trades
    UpdateFlag = 2;
end
disp(['UpdateFlag in derivativepricer:',num2str(UpdateFlag)]);
% CONST_SKEW_STEP_MULT =  0.1;
CONST_SKEW_STEP_MULT =  1;

if ~any(strcmpi(Inputheading,'Lots'))
    waitfor(errordlg('Error! Inputheading must contain Lots', 'derivativepricer'));
    return
end
if ~any(strcmpi(Inputheading,'Opt_Type'))
    waitfor(errordlg('Error! Inputheading must contain Opt_Type', 'derivativepricer'));
    return
end
if ~any(strcmpi(Inputheading,'Lot_Mult'))
    waitfor(errordlg('Error! Inputheading must contain Lot_Mult', 'derivativepricer'));
    return
end
if ~any(strcmpi(Inputheading,'Curr_Mult'))
    waitfor(errordlg('Error! Inputheading must contain Curr_Mult', 'derivativepricer'));
    return
end
if ~any(strcmpi(Inputheading,'P1_SettlePrice'))
    waitfor(errordlg('Error! Inputheading must contain P1_SettlePrice', 'derivativepricer'));
    return
end

%general
Pos_Lots             = find(strcmpi('Lots',             Inputheading));
Pos_Time_Bump        = find(strcmpi('Time_Bump',        Inputheading));
Pos_Opt_DayCount     = find(strcmpi('Opt_DayCount',     Inputheading));
Pos_Value_Date       = find(strcmpi('Value_Date',       Inputheading));
Pos_Settlement_Date       = find(strcmpi('Settlement_Date',       Inputheading));
Pos_Maturity         = find(strcmpi('Maturity',         Inputheading));
Pos_Opt_Type         = find(strcmpi('Opt_Type',         Inputheading));
Pos_P1_SettlePrice   = find(strcmpi('P1_SettlePrice',   Inputheading));
Pos_P2_SettlePrice   = find(strcmpi('P2_SettlePrice',   Inputheading));
Pos_RF_Rate_1        = find(strcmpi('RF_Rate1',         Inputheading));
Pos_P1_Div_Yield     = find(strcmpi('P1_Div_Yield',     Inputheading));
Pos_Strike           = find(strcmpi('Strike',           Inputheading));
Pos_Vol_Surf_Type    = find(strcmpi('Vol_Surf_Type',    Inputheading));
Pos_P1_Vol           = find(strcmpi('P1_Vol',           Inputheading));
Pos_P2_Vol           = find(strcmpi('P2_Vol',           Inputheading));
Pos_Correl_12        = find(strcmpi('Correl_12',        Inputheading));
Pos_ATM1_Vol         = find(strcmpi('ATM1_Vol',         Inputheading));
Pos_Call1_Skew       = find(strcmpi('Call1_Skew',       Inputheading));
Pos_Put1_Skew        = find(strcmpi('Put1_Skew',        Inputheading));
PosTickSize          = find(strcmpi('Tick_Size',        Inputheading));
Pos_Skew1_Step       = find(strcmpi('Skew1_Step',       Inputheading));
Pos_ATM2_Vol         = find(strcmpi('ATM2_Vol',         Inputheading));
Pos_Call2_Skew       = find(strcmpi('Call2_Skew',       Inputheading));
Pos_Put2_Skew        = find(strcmpi('Put2_Skew',        Inputheading));
Pos_Skew2_Step       = find(strcmpi('Skew2_Step',       Inputheading));

Pos_Lot_Mult         = find(strcmpi('Lot_Mult',         Inputheading));
Pos_Curr_Mult        = find(strcmpi('Curr_Mult',        Inputheading));
Pos_Quanto_Type      = find(strcmpi('Quanto_Type',      Inputheading));
Pos_Settlement_Price = find(strcmpi('Settlement_Price', Inputheading));
Pos_Currency         = find(strcmpi('Currency',         Inputheading));

Pos_Avg_Freq        = find(strcmpi('Avg_Freq',          Inputheading));
Pos_Avg_SD          = find(strcmpi('Avg_SD',            Inputheading));
Pos_Avg_ED          = find(strcmpi('Avg_ED',            Inputheading));
Pos_Avg_Till_Now    = find(strcmpi('Avg_Till_Now',      Inputheading));
% 
% unused values
% %accumulator specific
% Pos_Accum_Freq      = find(strcmpi('Accum_Freq',        Inputheading));
% Pos_Accum_SD        = find(strcmpi('Accum_SD',          Inputheading));
% Pos_Accum_ED        = find(strcmpi('Accum_ED',          Inputheading));
% Pos_Accum_Till_Now  = find(strcmpi('Accum_Till_Now',    Inputheading));

Pos_SecurityId      = find(strcmpi('Security_ID',      Inputheading));
Pos_Subportfolio    = find(strcmpi('Subportfolio',     Inputheading));
% Pos_P1_Name          = find(strcmpi('P1_Name',          Inputheading));
% Pos_P2_Name          = find(strcmpi('P2_Name',          Inputheading));
% Pos_RF_Rate_2        = find(strcmpi('RF_Rate2',         Inputheading));
% Pos_P2_Div_Yield     = find(strcmpi('P2_Div_Yield',     Inputheading));

% 
% Pos_Barrier_Type    = find(strcmpi('Barrier_Type',      Inputheading));
% Pos_Barrier_Obs_Type= find(strcmpi('Barrier_Obs_Type',  Inputheading));
% Pos_Barrier_2_Level = find(strcmpi('Barrier2_Level',    Inputheading));
% Pos_Barrier2_Obs_SD = find(strcmpi('Barrier2_Obs_SD',   Inputheading));
% Pos_Barrier2_Obs_ED = find(strcmpi('Barrier2_Obs_ED',   Inputheading));
% Pos_Barrier2_Rebate = find(strcmpi('Barrier2_Rebate',   Inputheading));
% Pos_Barrier_2_Hit   = find(strcmpi('IsBarrier2Breached',Inputheading));

%barrier specific
Pos_Barrier_1_Level = find(strcmpi('Barrier1_Level',    Inputheading));
Pos_Barrier1_Obs_SD = find(strcmpi('Barrier1_Obs_SD',   Inputheading));
Pos_Barrier1_Obs_ED = find(strcmpi('Barrier1_Obs_ED',   Inputheading));
Pos_Barrier1_Rebate = find(strcmpi('Barrier1_Rebate',   Inputheading));
Pos_Barrier_1_Hit   = find(strcmpi('IsBarrier1Breached',Inputheading));


% outputs
% Pos_Price           = find(strcmpi('Price',   Outputheading));
% Pos_Delta_1         = find(strcmpi('Delta_1', Outputheading));
% Pos_Delta_2         = find(strcmpi('Delta_2', Outputheading));
% Pos_Gamma_11        = find(strcmpi('Gamma_11',Outputheading));
% Pos_Gamma_12        = find(strcmpi('Gamma_12',Outputheading));
% Pos_Gamma_21        = find(strcmpi('Gamma_21',Outputheading));
% Pos_Gamma_22        = find(strcmpi('Gamma_22',Outputheading));
% Pos_Vega_1          = find(strcmpi('Vega_1',  Outputheading));
% Pos_Vega_2          = find(strcmpi('Vega_2',  Outputheading));
% Pos_Theta           = find(strcmpi('Theta',   Outputheading));
% Pos_Chi             = find(strcmpi('Chi',     Outputheading));
% Pos_Curr_Exp        = find(strcmpi('Curr_Exp',Outputheading));

Outputheading = lower(Outputheading);
Pos_Price           = cellStrfind(Outputheading, 'price');
Pos_Delta_1         = cellStrfind(Outputheading, 'delta_1');
Pos_Delta_2         = cellStrfind(Outputheading, 'delta_2');
Pos_Gamma_11        = cellStrfind(Outputheading, 'gamma_11');
Pos_Gamma_12        = cellStrfind(Outputheading, 'gamma_12');
Pos_Gamma_21        = cellStrfind(Outputheading, 'gamma_21');
Pos_Gamma_22        = cellStrfind(Outputheading, 'gamma_22');
Pos_Vega_1          = cellStrfind(Outputheading, 'vega_1');
Pos_Vega_2          = cellStrfind(Outputheading, 'vega_2');
Pos_Theta           = cellStrfind(Outputheading, 'theta');
Pos_Chi             = cellStrfind(Outputheading, 'chi');
Pos_Curr_Exp        = cellStrfind(Outputheading, 'curr_Exp');

ValidOptionTypes = {'future','fx_future','fx_forward_df','fx_forward_ndf','fx_vanilla_put',...
    'fx_vanilla_call','vanilla_put','vanilla_call','fx_spot',...
    'euro_up_in_call','euro_up_in_put','euro_up_out_call','euro_up_out_put',...
    'euro_down_in_call','euro_down_in_put','euro_down_out_call','euro_down_out_put',...
    'amer_up_in_call','amer_up_in_put','amer_up_out_call','amer_up_out_put',...
    'amer_down_in_call','amer_down_in_put','amer_down_out_call','amer_down_out_put',...
    'fixed_strike_am_call','fixed_strike_am_put','avg_swap_nc','avg_swap_cc',...
    'spread_call','spread_put','future_spread','equity','equity_swap','cs_future'};
    

OptTypes = Inputmatrix(:, Pos_Opt_Type);
Idx_OptTypes = cellfun(@isnumeric, OptTypes);
OptTypes(Idx_OptTypes) = cellstr('');
OptionTypes = lower(OptTypes);

Idx_OptTypes = ismember(lower(OptionTypes), ValidOptionTypes);
Output(~Idx_OptTypes, Pos_Price) = cellstr('Opt_Type not implemented!');


IdxFut     = strcmpi(OptionTypes, 'future') |  strcmpi(OptionTypes, 'fx_future');
IdxFxFwd   = strcmpi(OptionTypes, 'fx_forward_df') | strcmpi(OptionTypes, 'fx_forward_ndf');
IdxVanilla = strcmpi(OptionTypes, 'vanilla_call') | strcmpi(OptionTypes, 'vanilla_put') | ...
    strcmpi(OptionTypes, 'fx_vanilla_call') | strcmpi(OptionTypes, 'fx_vanilla_put');
IdxCash    = strcmpi(OptionTypes, 'fx_spot');
IdxEquity   = strcmpi(OptionTypes, 'equity');
IdxEquitySwap   = strcmpi(OptionTypes, 'equity_swap');
IdxCSFut   = strcmpi(OptionTypes, 'cs_future');
IdxFutSpr  = strcmpi(OptionTypes, 'future_spread');
IdxSprOpt  = strcmpi(OptionTypes, 'spread_call')  | strcmpi(OptionTypes, 'spread_put');
IdxAmerBarOpt  = strcmpi(OptionTypes, 'amer_up_in_call')  | strcmpi(OptionTypes, 'amer_up_in_put') | strcmpi(OptionTypes, 'amer_up_out_call')  | strcmpi(OptionTypes, 'amer_up_out_put') | ...
    strcmpi(OptionTypes, 'amer_down_in_call')  | strcmpi(OptionTypes, 'amer_down_in_put') | strcmpi(OptionTypes, 'amer_down_out_call')  | strcmpi(OptionTypes, 'amer_down_out_put');
IdxEuroBarOpt = strcmpi(OptionTypes, 'euro_up_in_call')  | strcmpi(OptionTypes, 'euro_up_in_put') | strcmpi(OptionTypes, 'euro_up_out_call')  | strcmpi(OptionTypes, 'euro_up_out_put') | ...
    strcmpi(OptionTypes, 'euro_down_in_call')  | strcmpi(OptionTypes, 'euro_down_in_put') | strcmpi(OptionTypes, 'euro_down_out_call')  | strcmpi(OptionTypes, 'euro_down_out_put');
IdxExoticOpt = strcmpi(OptionTypes, 'fixed_strike_am_call')  | strcmpi(OptionTypes, 'fixed_strike_am_put');
IdxSwapNC   = strcmpi(OptionTypes, 'avg_swap_nc') ;
IdxSwapCC   = strcmpi(OptionTypes, 'avg_swap_cc');

Lots                  = utilgetvalidnumericarray(Inputmatrix(:, Pos_Lots));
Lot_Mult              = utilgetvalidnumericarray(Inputmatrix(:, Pos_Lot_Mult));
Curr_Mult             = utilgetvalidnumericarray(Inputmatrix(:, Pos_Curr_Mult));
AssetPrice_1          = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_SettlePrice));
if ~isempty(Pos_P2_SettlePrice)
AssetPrice_2          = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_SettlePrice));
else
    AssetPrice_2 = [];
end
Strike                = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike));
if ~isempty(Pos_RF_Rate_1)
RiskFreeRate          = utilgetvalidnumericarray(Inputmatrix(:, Pos_RF_Rate_1));
Idx_NaNRFR = isnan(RiskFreeRate) ; RiskFreeRate(Idx_NaNRFR) = 0; % Added on 24 June to avoid getting NaN values in pricer output, due to this NaN
else
    RiskFreeRate = [];
end
if ~isempty(Pos_P1_Div_Yield)
P1_Div_Yield          = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_Div_Yield));
else
    P1_Div_Yield =[];
end
if ~isempty(Pos_RF_Rate_1) && ~isempty(Pos_P1_Div_Yield)
CostOfCarry           = RiskFreeRate - P1_Div_Yield;
else
    CostOfCarry = [];
end
Opt_DayCount          = Inputmatrix(:, Pos_Opt_DayCount);
QuantoType            = Inputmatrix(:, Pos_Quanto_Type);

if ~isempty(Pos_ATM1_Vol)
    ATM1_Vol              = utilgetvalidnumericarray(Inputmatrix(:, Pos_ATM1_Vol));
    Idx_ATM1_Vol          = ~isnan(ATM1_Vol);
end
if ~isempty(Pos_Call1_Skew)
    Call1_Skew            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Call1_Skew));
    Idx_Call1_Skew        = ~isnan(Call1_Skew);
end
if ~isempty(Pos_Put1_Skew)
    Put1_Skew             = utilgetvalidnumericarray(Inputmatrix(:, Pos_Put1_Skew));
    Idx_Put1_Skew         = ~isnan(Put1_Skew);
end
if ~isempty(Pos_Skew1_Step)
    Skew1_Step            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Skew1_Step));  %#ok<*FNDSB>
    Idx_Skew1_Step        = Skew1_Step ~= 0 & ~isnan(Skew1_Step);
end
if ~isempty(Pos_ATM2_Vol)
    ATM2_Vol              = utilgetvalidnumericarray(Inputmatrix(:, Pos_ATM2_Vol));
    Idx_ATM2_Vol          = ~isnan(ATM2_Vol);
end
if ~isempty(Pos_Call2_Skew)
    Call2_Skew            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Call2_Skew));
    Idx_Call2_Skew        = ~isnan(Call2_Skew);
end
if ~isempty(Pos_Put2_Skew)
    Put2_Skew             = utilgetvalidnumericarray(Inputmatrix(:, Pos_Put2_Skew));
    Idx_Put2_Skew         = ~isnan(Put2_Skew);
end
if ~isempty(Pos_Skew2_Step)
    Skew2_Step            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Skew2_Step));  %#ok<*FNDSB>
    Idx_Skew2_Step        = Skew2_Step ~= 0 & ~isnan(Skew2_Step);
end
if ~isempty(PosTickSize)
    TickSize        = utilgetvalidnumericarray(Inputmatrix(:, PosTickSize));
    Idx_TickSize     = TickSize ~= 0 & ~isnan(TickSize);
end    
if ~isempty(Pos_P1_Vol)
    P1_Vol                = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_Vol));
end
if ~isempty(Pos_P2_Vol)
    P2_Vol                = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_Vol));
end
if ~isempty(Pos_Correl_12)
    Correl_12            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Correl_12));
    Idx_Correl_12        = ~isnan(Correl_12);
end
if ~isempty(Pos_Barrier_1_Level)
    Barrier_1_Level = utilgetvalidnumericarray(Inputmatrix(:, Pos_Barrier_1_Level)); 
    Idx_Barrier_1_Level =~isnan(Barrier_1_Level); 
end
if ~isempty(Pos_Barrier1_Rebate)
    Barrier1_Rebate = utilgetvalidnumericarray(Inputmatrix(:, Pos_Barrier1_Rebate)); 
    Idx_Barrier1_Rebate =~isnan(Barrier1_Rebate); 
end
if ~isempty(Pos_Barrier_1_Hit)
    Barrier_1_Hit = utilgetvalidnumericarray(Inputmatrix(:, Pos_Barrier_1_Hit)); 
    Idx_Barrier_1_Hit =~isnan(Barrier_1_Hit); 
end

if ~isempty(Pos_Avg_SD)
    AvgStartDate        = utilgetvaliddatearray(Inputmatrix(:, Pos_Avg_SD));    
    Idx_AvgStartDate    = ~isnan(AvgStartDate);
end
if ~isempty(Pos_Avg_ED)
    AvgEndDate          = utilgetvaliddatearray(Inputmatrix(:, Pos_Avg_ED));    
    Idx_AvgEndDate      = ~isnan(AvgEndDate);
end
if ~isempty(Pos_Avg_Till_Now)
    AvgTillNow = utilgetvalidnumericarray(Inputmatrix(:, Pos_Avg_Till_Now));    
    Idx_AvgTillNow         = ~isnan(AvgTillNow);
end

Value_Date            = Inputmatrix(:, Pos_Value_Date);
Settlement_Date       = Inputmatrix(:, Pos_Settlement_Date);

Maturity              = Inputmatrix(:, Pos_Maturity);
Maturity(cellfun(@isempty,Maturity)) = cellstr(datestr(today+1,'yyyy-mm-dd'));
Vol_Surf_Type         = Inputmatrix(:, Pos_Vol_Surf_Type);
Time_Bump             = utilgetvalidnumericarray(Inputmatrix(:, Pos_Time_Bump));

try
    TTM               = (datenum(Maturity,'yyyy-mm-dd') - datenum(Value_Date,'yyyy-mm-dd') +1 - Time_Bump)/365;
catch
    TTM               = (datenum(cell2mat(Maturity)) - datenum(cell2mat(Value_Date)) +1 - Time_Bump)/365;
end
if ~isempty(Pos_Settlement_Price)
    Settlement_Price = utilgetvalidnumericarray(Inputmatrix(:, Pos_Settlement_Price));
    Idx_Settlement_price  = ~isnan(Settlement_Price);
end
if ~isempty(Pos_Currency)
    Currency = Inputmatrix(:, Pos_Currency);
end
Idx_Lots              = Lots ~= 0 & ~isnan(Lots);
Idx_Lot_Mult          = Lot_Mult ~= 0 & ~isnan(Lot_Mult);
Idx_Curr_Mult         = Curr_Mult ~= 0 & ~isnan(Curr_Mult);
Idx_AssetPrice_1      = AssetPrice_1 ~= 0 & ~isnan(AssetPrice_1);
Idx_AssetPrice_2      = AssetPrice_2 ~= 0 & ~isnan(AssetPrice_2);
Idx_Strike            = ~isnan(Strike);
Idx_RiskFreeRate      = ~isnan(RiskFreeRate);
Idx_CostOfCarry       = ~isnan(CostOfCarry);
Idx_TTM               = ~isnan(TTM);
Idx_Expired           = TTM <=0;
Idx_MaturityVol       = strcmp(Value_Date,Maturity);  % Idx_MaturityVol = strcmp(Settlement_Date,Maturity);

% % TODO - Below code to be revisited after year end 2013-14
if ~isempty(Pos_P1_Vol) && ~isempty(Pos_Settlement_Price) % should be done only for acc surface and not for book surface
    IdxExpiredNaNVol = isnan(P1_Vol) & Idx_MaturityVol;
    %     P1_Vol(isnan(P1_Vol(Idx_MaturityVol)))   = 0.0001;
    P1_Vol(IdxExpiredNaNVol)   = 0.0001;
end
if ~isempty(Pos_P2_Vol) && ~isempty(Pos_Settlement_Price) % should be done only for acc surface and not for book surface
    IdxExpiredNaNVol = isnan(P2_Vol) & Idx_MaturityVol;
    %     P2_Vol(isnan(P2_Vol(Idx_MaturityVol)))   = 0.0001;
    P2_Vol(IdxExpiredNaNVol)   = 0.0001;
end
% % End of TODO - code to be revisited after year end 2013-14

if ~isempty(Pos_P1_Vol)
    Idx_P1_Vol            = ~isnan(P1_Vol);
end
if ~isempty(Pos_P2_Vol)
    Idx_P2_Vol            = ~isnan(P2_Vol);
end

Calc_P1_Vol = P1_Vol;
if exist('P2_Vol','var')
Calc_P2_Vol = P2_Vol;
end
IsExpiryProcessed     = zeros(size(Lots));
IsExpiryProcessed(Idx_Expired) = 1;

YearToDay_Mult = repmat(252,nRow,1);
IdxDayCount = ismember(Opt_DayCount,'calendar');
YearToDay_Mult(IdxDayCount) = 365;

%% future
if any(IdxFut)
    IdxTmp = IdxFut & Idx_Lots & Idx_AssetPrice_1;
    IdxFut1 = IdxTmp & ~Idx_Expired;
    IdxFut2 = IdxTmp & Idx_Expired;
    if any(IdxFut1)        
        if ~isempty(Pos_Price)
            Output(IdxFut1,Pos_Price)    = num2cell(AssetPrice_1(IdxFut1));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxFut1,Pos_Delta_1)  = num2cell(Lots(IdxFut1));
        end
        if ~isempty(Pos_Curr_Exp)
            Output(IdxFut1,Pos_Curr_Exp)  = num2cell(0);
            IdxCurr = IdxFut1 & ~(strcmpi(Currency,'USD') | strcmpi(Currency,'USd'));  
            if any(IdxCurr)
                Output(IdxCurr,Pos_Curr_Exp)  = num2cell(Lots(IdxCurr).*AssetPrice_1(IdxCurr));
            end
        end
    end
    if any(IdxFut2)        
        if ~isempty(Pos_Price)
            Output(IdxFut2,Pos_Price)    = num2cell(0);
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxFut2,Pos_Delta_1)  = num2cell(0);
        end
         if ~isempty(Pos_Curr_Exp)
            Output(IdxFut2,Pos_Curr_Exp)  = num2cell(0);
        end
    end
end
%% fx_forward
if any(IdxFxFwd)
    IdxTmp = IdxFxFwd & Idx_Lots & Idx_AssetPrice_1;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(AssetPrice_1(IdxTmp));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(0);
        end
        if ~isempty(Pos_Curr_Exp)
            Output(IdxTmp,Pos_Curr_Exp)  = num2cell(0);
            IdxCurr = IdxTmp & ~(strcmpi(Currency,'USD') | strcmpi(Currency,'USd'));
            if any(IdxCurr)
                Output(IdxCurr,Pos_Curr_Exp)  = num2cell(Lots(IdxCurr));
            end
        end
    end

end
%% cash equity
if any(IdxEquity)
    IdxTmp = IdxEquity & Idx_Lots & Idx_AssetPrice_1;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(AssetPrice_1(IdxTmp));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(0);
        end       
    end
end
%% equity swaps
if any(IdxEquitySwap)
    IdxTmp = IdxEquitySwap & Idx_Lots & Idx_AssetPrice_1;
    if any(IdxTmp)
        if ~isempty(Pos_Price)
            Output(IdxTmp,Pos_Price)    = num2cell(AssetPrice_1(IdxTmp));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmp,Pos_Delta_1)  = num2cell(0);
        end       
    end
end
%% compo_swap_future
if any(IdxCSFut)
    IdxTmp = IdxCSFut & Idx_Lots & Idx_AssetPrice_1;
    IdxFut1 = IdxTmp & ~Idx_Expired;
    IdxFut2 = IdxTmp & Idx_Expired;
    IdxMultPrice = ismember(Currency(IdxFut1),{'BRL'});
    if any(IdxFut1)        
        if ~isempty(Pos_Price)
            Output(IdxFut1,Pos_Price)    = num2cell(AssetPrice_1(IdxFut1));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxFut1,Pos_Delta_1)  = num2cell(Lots(IdxFut1));
        end
    end
    if any(IdxFut2)        
        if ~isempty(Pos_Price)
            Output(IdxFut2,Pos_Price)    = num2cell(0);
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxFut2,Pos_Delta_1)  = num2cell(0);
        end       
    end
end
%% fx_spot options
if any(IdxCash)
    IdxTmp = IdxCash & Idx_Lots & Idx_AssetPrice_1 & Idx_Lot_Mult;
    IdxCash1 = IdxTmp & ~Idx_Expired;
    IdxCash2 = IdxTmp & Idx_Expired;
    if any(IdxCash1)        
        if ~isempty(Pos_Price)
            Output(IdxCash1,Pos_Price)    = num2cell(AssetPrice_1(IdxCash1).*Lots(IdxCash1).*Lot_Mult(IdxCash1));
        end        
        if ~isempty(Pos_Curr_Exp)
            Output(IdxCash1,Pos_Curr_Exp)  = num2cell(0);
            IdxCurr = IdxCash1 & ~(strcmpi(Currency,'USD') | strcmpi(Currency,'USd'));  
            if any(IdxCurr)
                Output(IdxCurr,Pos_Curr_Exp)  = num2cell(Lots(IdxCurr).*Lot_Mult(IdxCurr).*AssetPrice_1(IdxCurr));
            end
        end
    end
    if any(IdxCash2)        
        if ~isempty(Pos_Price)
            Output(IdxCash2,Pos_Price)    = num2cell(0);
        end        
        if ~isempty(Pos_Curr_Exp)
            Output(IdxCash2,Pos_Curr_Exp)  = num2cell(0);
        end
    end
end
%% vanilla options
if any(IdxVanilla)     
    IdxFloatVanilla = IdxVanilla & strcmpi(Vol_Surf_Type, 'float_3param');
    if any(IdxFloatVanilla)
        IdxTempVanilla = (IdxFloatVanilla & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_ATM1_Vol & Idx_Call1_Skew & Idx_Put1_Skew & ...
            Idx_Skew1_Step & Idx_TTM & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_TickSize) | (IdxFloatVanilla & Idx_Expired);
        
        if any(IdxTempVanilla)
            P11_Settle_Price = zeros(size(IdxTempVanilla));
            P12_Settle_Price = zeros(size(IdxTempVanilla));
            Vol = zeros(size(IdxTempVanilla));
            Vol1 = zeros(size(IdxTempVanilla));
            Vol2 = zeros(size(IdxTempVanilla));
            
            AssetPrice_1(Idx_Expired) = 0.1;            

            Vol(IdxTempVanilla) =  floatvolcurvelinear(AssetPrice_1(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol(Idx_Expired) = 0;
            obj = VanillaOptionBlackScholesModel(AssetPrice_1(IdxTempVanilla), Strike(IdxTempVanilla), RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla),...
                Vol(IdxTempVanilla), TTM(IdxTempVanilla), OptionTypes(IdxTempVanilla), Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));
            outputhandling(obj, IdxTempVanilla);
            Calc_P1_Vol(IdxTempVanilla) = Vol(IdxTempVanilla);
            
            P11_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) - (TickSize(IdxTempVanilla) .* CONST_SKEW_STEP_MULT);            
%             P11_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) - (AssetPrice_1(IdxTempVanilla) .* 0.01);            
            P11_Settle_Price(Idx_Expired) = 0.1;
            Vol1(IdxTempVanilla) =  floatvolcurvelinear(P11_Settle_Price(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol1(Idx_Expired) = 0;
            obj1 = VanillaOptionBlackScholesModel(P11_Settle_Price(IdxTempVanilla), Strike(IdxTempVanilla), RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla),...
                Vol1(IdxTempVanilla), TTM(IdxTempVanilla), OptionTypes(IdxTempVanilla), Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla),...
                IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));
            Price_1 =  obj1.getprice();

            P12_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) + (TickSize(IdxTempVanilla) .* CONST_SKEW_STEP_MULT);   
%             P12_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) + (AssetPrice_1(IdxTempVanilla) .* 0.01);     
            P12_Settle_Price(Idx_Expired) = 0.1;
            Vol2(IdxTempVanilla) =  floatvolcurvelinear(P12_Settle_Price(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol2(Idx_Expired) = 0;
            obj2 = VanillaOptionBlackScholesModel(P12_Settle_Price(IdxTempVanilla), Strike(IdxTempVanilla), RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla),...
                Vol2(IdxTempVanilla), TTM(IdxTempVanilla), OptionTypes(IdxTempVanilla), Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));
            Price_2 =  obj2.getprice();

            Output(IdxTempVanilla,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*TickSize(IdxTempVanilla).* CONST_SKEW_STEP_MULT));
%           Output(IdxTempVanilla,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*AssetPrice_1(IdxTempVanilla).* 0.01));
        end
    end
    
    IdxFixedVanilla = IdxVanilla & strcmpi(Vol_Surf_Type, 'fixed');
    if any(IdxFixedVanilla)
        IdxTempVanilla = (IdxFixedVanilla & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_P1_Vol & Idx_TTM & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult) | (IdxFixedVanilla & Idx_Expired);    
        if any(IdxTempVanilla)
            AssetPrice_1(Idx_Expired) = 0.1;
            P1_Vol(Idx_Expired) = 0; 
            obj = VanillaOptionBlackScholesModel(AssetPrice_1(IdxTempVanilla), Strike(IdxTempVanilla), RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla),...
                P1_Vol(IdxTempVanilla), TTM(IdxTempVanilla), OptionTypes(IdxTempVanilla), Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla),...
                Curr_Mult(IdxTempVanilla), IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));
            outputhandling(obj, IdxTempVanilla);
            Calc_P1_Vol(IdxTempVanilla) = P1_Vol(IdxTempVanilla);
        end
    end
    
end

%% american barrier options
if any(IdxAmerBarOpt) 
    IdxFloatBarrier = IdxAmerBarOpt & strcmpi(Vol_Surf_Type, 'float_3param');
    if any(IdxFloatBarrier)
        IdxTempBarrier = (IdxFloatBarrier & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_ATM1_Vol & Idx_Call1_Skew & Idx_Put1_Skew & ...
            Idx_Skew1_Step & Idx_TTM & Idx_Lots & Idx_TickSize &...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_Barrier_1_Level & Idx_Barrier1_Rebate & Idx_Barrier_1_Hit) | (IdxFloatBarrier & Idx_Expired);
        
        if any(IdxTempBarrier)
            Vol = zeros(size(IdxTempBarrier));
            Vol1 = zeros(size(IdxTempBarrier));
            Vol2 = zeros(size(IdxTempBarrier));
            P11_Settle_Price = zeros(size(IdxTempBarrier));
            P12_Settle_Price = zeros(size(IdxTempBarrier));
            
            AssetPrice_1(Idx_Expired) = 0.1;
            
            Vol(IdxTempBarrier) =  floatvolcurvelinear(AssetPrice_1(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));
            Vol(Idx_Expired) = 0;
            obj = EuropeanOptionAmericanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier), Barrier_1_Hit(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),Vol(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            outputhandling(obj, IdxTempBarrier);
            Calc_P1_Vol(IdxTempBarrier) = Vol(IdxTempBarrier);
            
            P11_Settle_Price(IdxTempBarrier) = AssetPrice_1(IdxTempBarrier) - (TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT);            
            P11_Settle_Price(Idx_Expired) = 0.1;
            Vol1(IdxTempBarrier) =  floatvolcurvelinear(P11_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));            
            Vol1(Idx_Expired) = 0;            
            obj1 = EuropeanOptionAmericanBarrier(P11_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier), Barrier_1_Hit(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),Vol1(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            Price_1 =  obj1.getprice();
           
            P12_Settle_Price(IdxTempBarrier) = AssetPrice_1(IdxTempBarrier) + (TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT);            
            P12_Settle_Price(Idx_Expired) = 0.1;
            Vol2(IdxTempBarrier) =  floatvolcurvelinear(P12_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));
            Vol2(Idx_Expired) = 0;
            obj2 = EuropeanOptionAmericanBarrier(P12_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier), Barrier_1_Hit(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),Vol2(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            Price_2 =  obj2.getprice();
            
            Output(IdxTempBarrier,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT));
      
        end
    end
    
    IdxFixedBarrier = IdxAmerBarOpt & strcmpi(Vol_Surf_Type, 'fixed');
    if any(IdxFixedBarrier)
        IdxTempBarrier = (IdxFixedBarrier & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_P1_Vol & Idx_TTM & Idx_TickSize & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_Barrier_1_Level & Idx_Barrier1_Rebate & Idx_Barrier_1_Hit) | (IdxFixedBarrier & Idx_Expired);
        if any(IdxTempBarrier)
            AssetPrice_1(Idx_Expired) = 0.1;    
            P1_Vol(Idx_Expired) = 0;
            obj = EuropeanOptionAmericanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier), Barrier_1_Hit(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),P1_Vol(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            outputhandling(obj, IdxTempBarrier);
            Calc_P1_Vol(IdxTempBarrier) = P1_Vol(IdxTempBarrier);
        end
    end
    
end
%% european barrier options
if any(IdxEuroBarOpt) 
    IdxFloatBarrier = IdxEuroBarOpt & strcmpi(Vol_Surf_Type, 'float_3param');
    if any(IdxFloatBarrier)
        IdxTempBarrier = (IdxFloatBarrier & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_ATM1_Vol & Idx_Call1_Skew & Idx_Put1_Skew & ...
            Idx_Skew1_Step & Idx_TTM & Idx_Lots & Idx_TickSize &...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_Barrier_1_Level & Idx_Barrier1_Rebate) | (IdxFloatBarrier & Idx_Expired);
        
        if any(IdxTempBarrier)
            StrikeVol = zeros(size(IdxTempBarrier));
            BarrierVol = zeros(size(IdxTempBarrier));
            StrikeVol1 = zeros(size(IdxTempBarrier));
            StrikeVol2 = zeros(size(IdxTempBarrier));
            BarrierVol1 = zeros(size(IdxTempBarrier));
            BarrierVol2 = zeros(size(IdxTempBarrier));
            P11_Settle_Price = zeros(size(IdxTempBarrier));
            P12_Settle_Price = zeros(size(IdxTempBarrier));
            
            AssetPrice_1(Idx_Expired) = 0.1;
            
            StrikeVol(IdxTempBarrier) =  floatvolcurvelinear(AssetPrice_1(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));
            StrikeVol(Idx_Expired) = 0;
            BarrierVol(IdxTempBarrier) =  floatvolcurvelinear(AssetPrice_1(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier));
            BarrierVol(Idx_Expired) = 0;
            obj = EuropeanOptionEuropeanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol(IdxTempBarrier),BarrierVol(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));   
%            obj = EuropeanOptionEuropeanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
%                 RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol(IdxTempBarrier),StrikeVol(IdxTempBarrier), TTM(IdxTempBarrier),...
%                 OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
%                 YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));      
            outputhandling(obj, IdxTempBarrier);
             Calc_P1_Vol(IdxTempBarrier) = StrikeVol(IdxTempBarrier);
             Calc_P2_Vol(IdxTempBarrier) = BarrierVol(IdxTempBarrier);
            
            P11_Settle_Price(IdxTempBarrier) = AssetPrice_1(IdxTempBarrier) - (TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT);            
            P11_Settle_Price(Idx_Expired) = 0.1;
            StrikeVol1(IdxTempBarrier) =  floatvolcurvelinear(P11_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));            
            StrikeVol1(Idx_Expired) = 0;   
            BarrierVol1(IdxTempBarrier) =  floatvolcurvelinear(P11_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier));            
            BarrierVol1(Idx_Expired) = 0; 
            obj1 = EuropeanOptionEuropeanBarrier(P11_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol1(IdxTempBarrier),BarrierVol1(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
%             obj1 = EuropeanOptionEuropeanBarrier(P11_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
%                 RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol1(IdxTempBarrier),StrikeVol1(IdxTempBarrier), TTM(IdxTempBarrier),...
%                 OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
%                 YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            Price_1 =  obj1.getprice();
           
            P12_Settle_Price(IdxTempBarrier) = AssetPrice_1(IdxTempBarrier) + (TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT);            
            P12_Settle_Price(Idx_Expired) = 0.1;
            StrikeVol2(IdxTempBarrier) =  floatvolcurvelinear(P12_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Strike(IdxTempBarrier));
            StrikeVol2(Idx_Expired) = 0;
            BarrierVol2(IdxTempBarrier) =  floatvolcurvelinear(P12_Settle_Price(IdxTempBarrier), ATM1_Vol(IdxTempBarrier), Call1_Skew(IdxTempBarrier),...
                            Put1_Skew(IdxTempBarrier), Skew1_Step(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier));
            BarrierVol2(Idx_Expired) = 0;
            obj2 = EuropeanOptionEuropeanBarrier(P12_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol2(IdxTempBarrier),BarrierVol2(IdxTempBarrier), TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
%             obj2 = EuropeanOptionEuropeanBarrier(P12_Settle_Price(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
%                 RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),StrikeVol2(IdxTempBarrier),StrikeVol2(IdxTempBarrier), TTM(IdxTempBarrier),...
%                 OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
%                 YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            Price_2 =  obj2.getprice();
            
            Output(IdxTempBarrier,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*TickSize(IdxTempBarrier).* CONST_SKEW_STEP_MULT));
      
        end
    end
    
    IdxFixedBarrier = IdxEuroBarOpt & strcmpi(Vol_Surf_Type, 'fixed');
    if any(IdxFixedBarrier)
        IdxTempBarrier = (IdxFixedBarrier & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_P1_Vol & Idx_TTM & Idx_TickSize & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_Barrier_1_Level & Idx_Barrier1_Rebate) | (IdxFixedBarrier & Idx_Expired);
        
        if any(IdxTempBarrier)
            AssetPrice_1(Idx_Expired) = 0.1;
            P1_Vol(Idx_Expired) = 0;
            P2_Vol(Idx_Expired) = 0;
            obj = EuropeanOptionEuropeanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
                RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),P1_Vol(IdxTempBarrier),P2_Vol(IdxTempBarrier),TTM(IdxTempBarrier),...
                OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
                YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            %               obj = EuropeanOptionEuropeanBarrier(AssetPrice_1(IdxTempBarrier), Strike(IdxTempBarrier), Barrier_1_Level(IdxTempBarrier),Barrier1_Rebate(IdxTempBarrier),...
            %                 RiskFreeRate(IdxTempBarrier), CostOfCarry(IdxTempBarrier),P1_Vol(IdxTempBarrier),P1_Vol(IdxTempBarrier),TTM(IdxTempBarrier),...
            %                 OptionTypes(IdxTempBarrier), Lots(IdxTempBarrier), Lot_Mult(IdxTempBarrier), Curr_Mult(IdxTempBarrier), ...
            %                 YearToDay_Mult(IdxTempBarrier),Opt_DayCount(IdxTempBarrier),  IsExpiryProcessed(IdxTempBarrier),Currency(IdxTempBarrier),TickSize(IdxTempBarrier));
            outputhandling(obj, IdxTempBarrier);
             Calc_P1_Vol(IdxTempBarrier) = P1_Vol(IdxTempBarrier);
             Calc_P2_Vol(IdxTempBarrier) = P2_Vol(IdxTempBarrier);
        end
    end
    
end
%% exotic pricing
if any(IdxExoticOpt) 
    IdxFloatVanilla = IdxExoticOpt & strcmpi(Vol_Surf_Type, 'float_3param');
    if any(IdxFloatVanilla)
        IdxTempVanilla = (IdxFloatVanilla & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_ATM1_Vol & Idx_Call1_Skew & Idx_Put1_Skew & ...
            Idx_Skew1_Step & Idx_TTM & Idx_Lots & Idx_TickSize & ...
            Idx_Lot_Mult & Idx_Curr_Mult & Idx_AvgTillNow & Idx_AvgStartDate & Idx_AvgEndDate) | (IdxFloatVanilla & Idx_Expired);
        
        if any(IdxTempVanilla)
            P11_Settle_Price = zeros(size(IdxTempVanilla));
            P12_Settle_Price = zeros(size(IdxTempVanilla));
            Vol = zeros(size(IdxTempVanilla));
            Vol1 = zeros(size(IdxTempVanilla));
            Vol2 = zeros(size(IdxTempVanilla));
            OriginalTimeInAveragePeriod = zeros(size(IdxTempVanilla));
            OriginalTimeInAveragePeriod(IdxTempVanilla) = datenum(cell2mat(AvgEndDate(IdxTempVanilla))) - datenum(cell2mat(AvgStartDate(IdxTempVanilla)));
            
            AssetPrice_1(Idx_Expired) = 0.1;           
            
            Vol(IdxTempVanilla) =  floatvolcurvelinear(AssetPrice_1(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol(Idx_Expired) = 0;
            
            obj = ArithmeticAsianTurnbullAndWakemanModel(AssetPrice_1(IdxTempVanilla), AvgTillNow(IdxTempVanilla), Strike(IdxTempVanilla),...
                  RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla), ...
                  Vol(IdxTempVanilla), TTM(IdxTempVanilla), OriginalTimeInAveragePeriod(IdxTempVanilla), OptionTypes(IdxTempVanilla),...
                  Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                  IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));            
            outputhandling(obj, IdxTempVanilla);
             Calc_P1_Vol(IdxTempVanilla) = Vol(IdxTempVanilla);
            
%             P11_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) - (Skew1_Step(IdxTempVanilla).* CONST_SKEW_STEP_MULT);
            P11_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) - (TickSize(IdxTempVanilla).* CONST_SKEW_STEP_MULT);
            P11_Settle_Price(Idx_Expired) = 0.1;
            Vol1(IdxTempVanilla) =  floatvolcurvelinear(P11_Settle_Price(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol1(Idx_Expired) = 0;            
            obj1 = ArithmeticAsianTurnbullAndWakemanModel(P11_Settle_Price(IdxTempVanilla), AvgTillNow(IdxTempVanilla), Strike(IdxTempVanilla),...
                  RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla), ...
                  Vol1(IdxTempVanilla), TTM(IdxTempVanilla), OriginalTimeInAveragePeriod(IdxTempVanilla), OptionTypes(IdxTempVanilla),...
                  Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                  IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));            
            Price_1 =  obj1.getprice();

%           P12_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) + (Skew1_Step(IdxTempVanilla).* CONST_SKEW_STEP_MULT);
            P12_Settle_Price(IdxTempVanilla) = AssetPrice_1(IdxTempVanilla) + (TickSize(IdxTempVanilla).* CONST_SKEW_STEP_MULT);
            P12_Settle_Price(Idx_Expired) = 0.1;
            Vol2(IdxTempVanilla) =  floatvolcurvelinear(P12_Settle_Price(IdxTempVanilla), ATM1_Vol(IdxTempVanilla), Call1_Skew(IdxTempVanilla),...
                            Put1_Skew(IdxTempVanilla), Skew1_Step(IdxTempVanilla), Strike(IdxTempVanilla));
            Vol2(Idx_Expired) = 0;            
            obj2 = ArithmeticAsianTurnbullAndWakemanModel(P12_Settle_Price(IdxTempVanilla), AvgTillNow(IdxTempVanilla), Strike(IdxTempVanilla),...
                  RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla), ...
                  Vol2(IdxTempVanilla), TTM(IdxTempVanilla), OriginalTimeInAveragePeriod(IdxTempVanilla), OptionTypes(IdxTempVanilla),...
                  Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                  IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));            
            Price_2 =  obj2.getprice();

%             Output(IdxTempVanilla,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*Skew1_Step(IdxTempVanilla).* CONST_SKEW_STEP_MULT));
            Output(IdxTempVanilla,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*TickSize(IdxTempVanilla).* CONST_SKEW_STEP_MULT));
        end
    end
    
    IdxFixedVanilla = IdxExoticOpt & strcmpi(Vol_Surf_Type, 'fixed');
    if any(IdxFixedVanilla)
        IdxTempVanilla = (IdxFixedVanilla & Idx_AssetPrice_1 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_CostOfCarry & Idx_P1_Vol & Idx_TTM & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult  & Idx_AvgTillNow & Idx_AvgStartDate & Idx_AvgEndDate) | (IdxFixedVanilla & Idx_Expired);
        if any(IdxTempVanilla)
            AssetPrice_1(Idx_Expired) = 0.1;
            P1_Vol(Idx_Expired) = 0;   
            OriginalTimeInAveragePeriod = zeros(size(IdxTempVanilla));
            OriginalTimeInAveragePeriod(IdxTempVanilla) = datenum(cell2mat(AvgEndDate(IdxTempVanilla))) - datenum(cell2mat(AvgStartDate(IdxTempVanilla)));
            obj = ArithmeticAsianTurnbullAndWakemanModel(AssetPrice_1(IdxTempVanilla), AvgTillNow(IdxTempVanilla), Strike(IdxTempVanilla),...
                  RiskFreeRate(IdxTempVanilla), CostOfCarry(IdxTempVanilla), ...
                  P1_Vol(IdxTempVanilla), TTM(IdxTempVanilla), OriginalTimeInAveragePeriod(IdxTempVanilla), OptionTypes(IdxTempVanilla),...
                  Lots(IdxTempVanilla), Lot_Mult(IdxTempVanilla), Curr_Mult(IdxTempVanilla), ...
                  IsExpiryProcessed(IdxTempVanilla),YearToDay_Mult(IdxTempVanilla),Currency(IdxTempVanilla));            
            outputhandling(obj, IdxTempVanilla);
            Calc_P1_Vol(IdxTempVanilla) = P1_Vol(IdxTempVanilla);
        end
    end
    
end

%% swap pricer for native currency

if any(IdxSwapNC) || any(IdxSwapCC)
try
    % get the list of holidays from db table
    % connect to database object
    objDB = connect_to_database;
    DBHolidaysData = fetch(objDB,'select exchange_code,invenio_product_code,holidays from holidays_list_table');
    DBExchCode = DBHolidaysData(:,1);
    DBProdCode = DBHolidaysData(:,2);
    DBHolidays = DBHolidaysData(:,3);
    IdxSGXHolidays = strcmpi('SGX',strtrim(DBExchCode)) & strcmpi('CM OR',strtrim(DBProdCode));
    HolidaysList = DBHolidays(IdxSGXHolidays);
    close(objDB);
catch
    disp('Error in accessing the holidays list from DB for swaps calculation!');
    HolidaysList = {'2012-08-09';'2012-08-20';'2012-11-13';'2012-12-25';...
        '2013-01-01';'2013-02-10';'2013-02-11';'2013-02-12';'2013-03-29';'2013-05-01';'2013-05-24';'2013-08-08';'2013-08-09';'2013-10-15';'2013-11-02';'2013-12-25';...
        '2014-01-01';'2014-01-31';'2014-02-01';'2014-04-18';'2014-05-01';'2014-05-13';'2014-07-28';'2014-08-09';'2014-10-06';'2014-10-23';'2014-12-25';'2015-01-01';...
        '2015-02-19';'2015-02-20';'2015-04-03';'2015-05-01';'2015-06-01';'2015-07-17';'2015-08-09';'2015-09-24';'2015-11-10';'2015-12-25'};
end
end

if any(IdxSwapNC)
  IdxTmpSwap = (IdxSwapNC & Idx_Lots & Idx_AvgTillNow & Idx_AvgStartDate & Idx_AvgEndDate);
    
    if any(IdxTmpSwap)
        TND         = zeros(size(IdxTmpSwap));
        NDE         = zeros(size(IdxTmpSwap));
        ElapsedDays = zeros(size(IdxTmpSwap));
        
        % if holiday list is provided then use the below syntax for TND and
        % ElapsedDays calculation
%         HolidaysList - Vector of dates from database
        VDates = datenum(Value_Date,'yyyy-mm-dd');
        
        % if v_Date > AvgEndDate, vDate = AvgEndDate
        Idx = zeros(size(IdxTmpSwap));
        SwapValueDate = zeros(size(IdxTmpSwap));
        SwapValueDate(IdxTmpSwap) = VDates(IdxTmpSwap) - 1;
        Idx(IdxTmpSwap) = SwapValueDate(IdxTmpSwap) > AvgEndDate(IdxTmpSwap);
        IdxTmp = find(Idx);
        SwapValueDate(IdxTmp) = AvgEndDate(IdxTmp);
        
        SwapIndices= find(IdxTmpSwap);
        for iVec = 1:length(SwapIndices)
            TND(SwapIndices(iVec))         = length(busdays(AvgStartDate(SwapIndices(iVec)),AvgEndDate(SwapIndices(iVec)),'daily',char(HolidaysList)));
            ElapsedDays(SwapIndices(iVec)) = length(busdays(AvgStartDate(SwapIndices(iVec)),SwapValueDate(SwapIndices(iVec)),'daily',char(HolidaysList)));
            NDE(SwapIndices(iVec))         = max(ElapsedDays(SwapIndices(iVec)),0);    % NDE = max(value_date - Avg_SD +1 ,0);
        end  
        
        SwapAssetPrice_1 = AssetPrice_1(IdxTmpSwap);        
        SwapAssetPrice_1(isnan(SwapAssetPrice_1)) = 0;
        
        obj = AverageSwapATDNativeCurrency(SwapAssetPrice_1 , OptionTypes(IdxTmpSwap) , AvgTillNow(IdxTmpSwap) , TND(IdxTmpSwap) , NDE(IdxTmpSwap) , Lots(IdxTmpSwap) );
        outputhandling(obj, IdxTmpSwap);
    end
end
%% swap pricer for cross currency
if any(IdxSwapCC)
      IdxTmpSwap = (IdxSwapCC &  Idx_Lots & Idx_AvgTillNow &  Idx_AvgStartDate & Idx_AvgEndDate);
    
    if any(IdxTmpSwap)
        TND = zeros(size(IdxTmpSwap));
        NDE = zeros(size(IdxTmpSwap));
        ElapsedDays = zeros(size(IdxTmpSwap));
        
        % if holiday list is provided then use the below syntax for TND and
        % ElapsedDays calculation
        VDates = datenum(Value_Date,'yyyy-mm-dd');
        
        % if v_Date > AvgEndDate, vDate = AvgEndDate
        Idx = zeros(size(IdxTmpSwap));
        SwapValueDate = zeros(size(IdxTmpSwap));
        SwapValueDate(IdxTmpSwap) = VDates(IdxTmpSwap) - 1;
        Idx(IdxTmpSwap) = SwapValueDate(IdxTmpSwap) > AvgEndDate(IdxTmpSwap);
        IdxTmp = find(Idx);
        SwapValueDate(IdxTmp) = AvgEndDate(IdxTmp);
        
        SwapIndices= find(IdxTmpSwap);
        for iVec = 1:length(SwapIndices)
            TND(SwapIndices(iVec))         = length(busdays(AvgStartDate(SwapIndices(iVec)),AvgEndDate(SwapIndices(iVec)),'daily',char(HolidaysList)));
            ElapsedDays(SwapIndices(iVec)) = length(busdays(AvgStartDate(SwapIndices(iVec)),SwapValueDate(SwapIndices(iVec)),'daily',char(HolidaysList)));
            NDE(SwapIndices(iVec))         = max(ElapsedDays(SwapIndices(iVec)),0);    % NDE = max(value_date - Avg_SD +1 ,0);
        end                    
        
        SwapAssetPrice_1 = AssetPrice_1(IdxTmpSwap) ./ 100;        
        SwapAssetPrice_1(isnan(SwapAssetPrice_1)) = 0;
        SwapAssetPrice_2 = AssetPrice_2(IdxTmpSwap);        
        SwapAssetPrice_2(isnan(SwapAssetPrice_2)) = 0;
        SwapAvgTillNow = AvgTillNow(IdxTmpSwap) ./ 100;
       
        obj = AverageSwapATDCrossCurrency(SwapAssetPrice_1 ,SwapAssetPrice_2, OptionTypes(IdxTmpSwap) , SwapAvgTillNow , TND(IdxTmpSwap) , NDE(IdxTmpSwap) , Lots(IdxTmpSwap), QuantoType(IdxTmpSwap) );
        outputhandling(obj, IdxTmpSwap);
    end
end

%% future_spread
if any(IdxFutSpr)    
    IdxTmpFutSpr = IdxFutSpr & Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_AssetPrice_2;
    if any(IdxTmpFutSpr & ~Idx_Expired)
        if ~isempty(Pos_Price)
            Output(IdxTmpFutSpr,Pos_Price)    = num2cell(AssetPrice_1(IdxTmpFutSpr));
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmpFutSpr,Pos_Delta_1)  = num2cell(Lots(IdxTmpFutSpr));
        end
        if ~isempty(Pos_Delta_2)
            Output(IdxTmpFutSpr,Pos_Delta_2)  = num2cell(Lots(IdxTmpFutSpr));
        end
    end

    if any(IdxTmpFutSpr & Idx_Expired)
        if ~isempty(Pos_Price)
            Output(IdxTmpFutSpr,Pos_Price)    = num2cell(0);
        end
        if ~isempty(Pos_Delta_1)
            Output(IdxTmpFutSpr,Pos_Delta_1)  = num2cell(0);
        end
        if ~isempty(Pos_Delta_2)
            Output(IdxTmpFutSpr,Pos_Delta_2)  = num2cell(0);
        end
    end
end

%% spread options
if any(IdxSprOpt)
    IdxFloatSprOpt = IdxSprOpt & strcmpi(Vol_Surf_Type, 'float_3param');
    if any(IdxFloatSprOpt)
        IdxTempSprOpt = (IdxFloatSprOpt & Idx_AssetPrice_1 & Idx_AssetPrice_2 & Idx_Strike & Idx_RiskFreeRate & ...
            Idx_ATM1_Vol & Idx_Call1_Skew & Idx_Put1_Skew & ...
            Idx_Skew1_Step & Idx_ATM2_Vol & Idx_Call2_Skew & Idx_Put2_Skew & ...
            Idx_Skew2_Step & Idx_Correl_12 & Idx_TTM & Idx_TickSize & Idx_Lots & ...
            Idx_Lot_Mult & Idx_Curr_Mult) | (IdxFloatSprOpt & Idx_Expired);
        
        if any(IdxTempSprOpt)
            P11_Settle_Price = zeros(size(IdxTempSprOpt)); P21_Settle_Price = zeros(size(IdxTempSprOpt));
            Vol1 = zeros(size(IdxTempSprOpt));             Vol2 = zeros(size(IdxTempSprOpt));
            Vol11 = zeros(size(IdxTempSprOpt));            Vol21 = zeros(size(IdxTempSprOpt)); 
            
            AssetPrice_1(Idx_Expired) = 0.1;            
            AssetPrice_2(Idx_Expired) = 0.1;
            
            Vol1(IdxTempSprOpt) =  floatvolcurvelinear(AssetPrice_1(IdxTempSprOpt), ATM1_Vol(IdxTempSprOpt), Call1_Skew(IdxTempSprOpt),...
                            Put1_Skew(IdxTempSprOpt), Skew1_Step(IdxTempSprOpt), Strike(IdxTempSprOpt));
            Vol1(Idx_Expired) = 0;
            Vol2(IdxTempSprOpt) =  floatvolcurvelinear(AssetPrice_2(IdxTempSprOpt), ATM2_Vol(IdxTempSprOpt), Call2_Skew(IdxTempSprOpt),...
                            Put2_Skew(IdxTempSprOpt), Skew2_Step(IdxTempSprOpt), Strike(IdxTempSprOpt));
            Vol2(Idx_Expired) = 0;
            Correl_12(Idx_Expired) = 0; 
            sobj = SpreadOptionBjerksundModel(AssetPrice_1(IdxTempSprOpt), AssetPrice_2(IdxTempSprOpt), ...
                Strike(IdxTempSprOpt), Vol1(IdxTempSprOpt), Vol2(IdxTempSprOpt), ...
                Correl_12(IdxTempSprOpt), RiskFreeRate(IdxTempSprOpt), TTM(IdxTempSprOpt), OptionTypes(IdxTempSprOpt), ...
                Lots(IdxTempSprOpt), Lot_Mult(IdxTempSprOpt), Curr_Mult(IdxTempSprOpt), ...
                YearToDay_Mult(IdxTempSprOpt),Opt_DayCount(IdxTempSprOpt),  IsExpiryProcessed(IdxTempSprOpt),TickSize(IdxTempSprOpt));
            outputhandling(sobj, IdxTempSprOpt);
            Calc_P1_Vol(IdxTempSprOpt) = Vol1(IdxTempSprOpt);
             Calc_P2_Vol(IdxTempSprOpt) = Vol2(IdxTempSprOpt);
            
            P11_Settle_Price(IdxTempSprOpt) = AssetPrice_1(IdxTempSprOpt) - (TickSize(IdxTempSprOpt) .* CONST_SKEW_STEP_MULT);            
            P11_Settle_Price(Idx_Expired) = 0.1;            
            Vol11(IdxTempSprOpt) =  floatvolcurvelinear(P11_Settle_Price(IdxTempSprOpt), ATM1_Vol(IdxTempSprOpt), Call1_Skew(IdxTempSprOpt),...
                            Put1_Skew(IdxTempSprOpt), Skew1_Step(IdxTempSprOpt), Strike(IdxTempSprOpt));
            Vol11(Idx_Expired) = 0;                      
            sobj1 = SpreadOptionBjerksundModel(P11_Settle_Price(IdxTempSprOpt), AssetPrice_2(IdxTempSprOpt), ...
                Strike(IdxTempSprOpt), Vol11(IdxTempSprOpt), Vol2(IdxTempSprOpt), ...
                Correl_12(IdxTempSprOpt), RiskFreeRate(IdxTempSprOpt), TTM(IdxTempSprOpt), OptionTypes(IdxTempSprOpt), ...
                Lots(IdxTempSprOpt), Lot_Mult(IdxTempSprOpt), Curr_Mult(IdxTempSprOpt), ...
                YearToDay_Mult(IdxTempSprOpt),Opt_DayCount(IdxTempSprOpt),  IsExpiryProcessed(IdxTempSprOpt),TickSize(IdxTempSprOpt));
            Price_1 =  sobj1.getprice();
            

            P21_Settle_Price(IdxTempSprOpt) = AssetPrice_1(IdxTempSprOpt) + (TickSize(IdxTempSprOpt) .* CONST_SKEW_STEP_MULT);            
            P21_Settle_Price(Idx_Expired) = 0.1;            
            Vol21(IdxTempSprOpt) =  floatvolcurvelinear(P21_Settle_Price(IdxTempSprOpt), ATM1_Vol(IdxTempSprOpt), Call1_Skew(IdxTempSprOpt),...
                            Put1_Skew(IdxTempSprOpt), Skew1_Step(IdxTempSprOpt), Strike(IdxTempSprOpt));
            Vol21(Idx_Expired) = 0;           
            sobj2 = SpreadOptionBjerksundModel(P21_Settle_Price(IdxTempSprOpt), AssetPrice_2(IdxTempSprOpt), ...
                Strike(IdxTempSprOpt), Vol21(IdxTempSprOpt), Vol2(IdxTempSprOpt), ...
                Correl_12(IdxTempSprOpt), RiskFreeRate(IdxTempSprOpt), TTM(IdxTempSprOpt), OptionTypes(IdxTempSprOpt), ...
                Lots(IdxTempSprOpt), Lot_Mult(IdxTempSprOpt), Curr_Mult(IdxTempSprOpt), ...
                YearToDay_Mult(IdxTempSprOpt),Opt_DayCount(IdxTempSprOpt),  IsExpiryProcessed(IdxTempSprOpt),TickSize(IdxTempSprOpt));
           Price_2 =  sobj2.getprice();
           
            Output(IdxTempSprOpt,Pos_Delta_1) = num2cell((Price_2-Price_1)./(2.*TickSize(IdxTempSprOpt).* CONST_SKEW_STEP_MULT));
            
        end
    end
    
    IdxFixedSprOpt = IdxSprOpt & strcmpi(Vol_Surf_Type, 'fixed');
    if any(IdxFixedSprOpt)
        IdxTmpSprOpt = (IdxFixedSprOpt & Idx_Lots & Idx_Lot_Mult & Idx_Curr_Mult & Idx_AssetPrice_1 & Idx_AssetPrice_2 & Idx_RiskFreeRate  ...
            & Idx_Strike & Idx_P1_Vol & Idx_P2_Vol & Idx_Correl_12 & Idx_TTM ) | (IdxFixedSprOpt & Idx_Expired);
        if any(IdxTmpSprOpt)
            AssetPrice_1(Idx_Expired) = 0.1; AssetPrice_2(Idx_Expired) = 0.1;
            P1_Vol(Idx_Expired) = 0;    P2_Vol(Idx_Expired) = 0; Correl_12(Idx_Expired) = 0;          
            sobj = SpreadOptionBjerksundModel(AssetPrice_1(IdxTmpSprOpt), AssetPrice_2(IdxTmpSprOpt), ...
                Strike(IdxTmpSprOpt), P1_Vol(IdxTmpSprOpt), P2_Vol(IdxTmpSprOpt), ...
                Correl_12(IdxTmpSprOpt), RiskFreeRate(IdxTmpSprOpt), TTM(IdxTmpSprOpt), OptionTypes(IdxTmpSprOpt), ...
                Lots(IdxTmpSprOpt), Lot_Mult(IdxTmpSprOpt), Curr_Mult(IdxTmpSprOpt), ...
                YearToDay_Mult(IdxTmpSprOpt),Opt_DayCount(IdxTmpSprOpt),  IsExpiryProcessed(IdxTmpSprOpt),TickSize(IdxTmpSprOpt));
            outputhandling(sobj, IdxTmpSprOpt);
            Calc_P1_Vol(IdxTmpSprOpt) = P1_Vol(IdxTmpSprOpt);
            Calc_P2_Vol(IdxTmpSprOpt) = P2_Vol(IdxTmpSprOpt);
        end
    end
end

if UpdateFlag
    try
        if UpdateFlag == 1 % if the query is to price all trades, then update the vol table
            if ~isempty(Pos_Settlement_Price) % if its settle_view
                VolData = [Inputmatrix(:,Pos_SecurityId),num2cell(Calc_P1_Vol),num2cell(Calc_P2_Vol)];
                TableName = ['calculated_settle_previous_vol_table_',InBUName];
            else % if its traders view
                VolData = [Inputmatrix(:,Pos_Subportfolio),Inputmatrix(:,Pos_SecurityId),num2cell(Calc_P1_Vol),num2cell(Calc_P2_Vol)];
                TableName = ['calculated_traders_vol_table_',InBUName];
            end
            try
                objDB = connect_to_database;
                 set(objDB,'AutoCommit','off');
%                 SqlQuery = ['delete from ',TableName,' where value_date =''',Value_Date{1},''''];
                SqlQuery = ['delete from ',TableName];
                curs = exec(objDB,SqlQuery);
                commit(objDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(objDB,'AutoCommit','on');
                if isconnection(objDB)
                    close(objDB);
                end
            catch ME
                disp(ME.message);
                if isconnection(objDB)
                    close(objDB);
                end
            end
            upload_in_database(TableName, VolData);
        end
    catch
        disp('error while updating the calculated vols table!');
    end
    
%     if ~isempty(Pos_Settlement_Price) && any(Idx_Settlement_price)
%         Output(Idx_Settlement_price, Pos_Price) = num2cell(Settlement_Price(Idx_Settlement_price));
%     end
end
    function outputhandling(obj, Row)
        
        if ~isempty(Pos_Price)            
            Output(Row, Pos_Price) = num2cell(obj.getprice());           
        end
        if ~isempty(Pos_Delta_1)
            Output(Row,Pos_Delta_1)  = num2cell(obj.getdelta1());
        end
        if ~(isempty(Pos_Delta_2) || isempty(obj.getdelta2()))
            Output(Row,Pos_Delta_2)  =num2cell(obj.getdelta2());
        end
        if ~(isempty(Pos_Gamma_11) || isempty(obj.getgamma11()))
            Output(Row,Pos_Gamma_11) = num2cell(obj.getgamma11());
        end
        if ~(isempty(Pos_Gamma_12) || isempty(obj.getgamma12()))
            Output(Row,Pos_Gamma_12) = num2cell(obj.getgamma12());
        end
        if ~(isempty(Pos_Gamma_21) || isempty(obj.getgamma12()))
            Output(Row,Pos_Gamma_21) = num2cell(obj.getgamma12());
        end
        if ~(isempty(Pos_Gamma_22) || isempty(obj.getgamma22()))
            Output(Row,Pos_Gamma_22) = num2cell(obj.getgamma22());
        end
        if ~(isempty(Pos_Vega_1) || isempty(obj.getvega1()))
            Output(Row,Pos_Vega_1)   = num2cell(obj.getvega1());
        end
        if ~(isempty(Pos_Vega_2) || isempty(obj.getvega2()))
            Output(Row,Pos_Vega_2)   = num2cell(obj.getvega2());
        end
        if ~(isempty(Pos_Theta) || isempty(obj.gettheta()))
            Output(Row,Pos_Theta)    = num2cell(obj.gettheta());
        end
        if ~(isempty(Pos_Chi) || isempty(obj.getchi()))
            Output(Row,Pos_Chi) = num2cell(obj.getchi());
        end
        if ~(isempty(Pos_Curr_Exp) || isempty(obj.getcurrencyexposure()))
            Output(Row,Pos_Curr_Exp) = num2cell(obj.getcurrencyexposure());
        end
        
    end
    
end

