function Output = csosensbybjerksund(Future_1, Future_2, Strike, Vol_1, Vol_2, Corr, RFR, TimeToMaturity, OptionType, OutputSpecification)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:06:28 $
%  $Revision: 1.1 $
%
validateattributes(OutputSpecification, {'char','cell'},{'nonempty'},'csobjerksundmodel','OutputSpecification');
ValidOutputTypes = {'price','delta1','delta2','gamma11','gamma22','gamma12','gamma21','vega1','vega2','chi','theta'};

if ~all(ismember(lower(OutputSpecification),ValidOutputTypes))
    error('Invalid Output Specfication type! Allowed output values are price,delta1,delta2,gamma11,gamma22,gamma12,gamma21,vega1,vega2,chi,theta');
else
    if ischar(OutputSpecification)
        OutputSpecification = cellstr(lower(OutputSpecification));
    else
        OutputSpecification = lower(OutputSpecification);
    end
end

obj = SpreadOptionBjerksundModel(Future_1, Future_2, Strike, Vol_1, Vol_2, Corr, RFR, TimeToMaturity, OptionType);

[isF,idx] = ismember('gamma21',OutputSpecification);
if isF
    OutputSpecification{idx} = 'gamma12';
end
Output = helperoutputhandlingforpricerclasses(obj, OutputSpecification);
end


