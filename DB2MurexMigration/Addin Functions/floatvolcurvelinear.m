function Output =  floatvolcurvelinear(Spot, ATMVol, CallSkew, PutSkew, Step, Strike)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/05/23 10:08:49 $
%  $Revision: 1.3 $
%

    NumInputs = size(Strike);

    if isscalar(Spot) 
        Spot = repmat(Spot,NumInputs);
    end
    if isscalar(ATMVol)
         ATMVol = repmat(ATMVol,NumInputs);
    end
    if isscalar(CallSkew)
         CallSkew = repmat(CallSkew,NumInputs);
    end
    if isscalar(PutSkew)
         PutSkew = repmat(PutSkew,NumInputs);
    end
    if isscalar(Step)
         Step = repmat(Step,NumInputs);
    end
    
    validateattributes(Spot,     {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'floatvolcurvelinear', 'Spot');
    validateattributes(ATMVol,   {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'floatvolcurvelinear', 'ATMVol');
    validateattributes(CallSkew, {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'floatvolcurvelinear', 'CallSkew');
    validateattributes(PutSkew,  {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'floatvolcurvelinear', 'PutSkew');
    validateattributes(Step,     {'numeric'}, {'nonempty', 'positive',    'column', 'size', NumInputs}, 'floatvolcurvelinear', 'Step');
    validateattributes(Strike,   {'numeric'}, {'nonempty', 'real',    'column'}, 'floatvolcurvelinear', 'Strike');
        
    Idx = (Strike - Spot) >= 0 ; 
    NonIdx = ~Idx;
    
    Output = zeros(size(Strike));
    if any(Idx)
        Output(Idx)  = ATMVol(Idx) + (Strike(Idx)  - Spot(Idx)) .* (CallSkew(Idx)./Step(Idx));
    end
    if any(NonIdx)
        Output(NonIdx) = ATMVol(NonIdx) - (Strike((NonIdx)) - Spot(NonIdx)) .* (PutSkew(NonIdx)./Step(NonIdx));
    end
end