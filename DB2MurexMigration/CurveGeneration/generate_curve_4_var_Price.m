function generate_curve_4_var_Price()

% clc;
% clear;
warning('off','all');

%%% Use R2016b Version

%% Connect to Database
ObjDB = connect_to_database ;
SqlQuery = 'select * from var_cont_price_change_table';
% VolIds = fetch(ObjDB,'select * from helper_fetch_futureids_for_curve') ;
VolIds = fetch(ObjDB,SqlQuery) ;

VolIds  = unique(VolIds(:,2))  ;

ValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
% ValueDate = '2017-01-24';

%% Get Business Date

% Bus_Date  = busdate(datenum(today-3),-1) ;
NoDaysShift = 7;
Bus_Date = busdate(datenum(ValueDate,'yyyy-mm-dd')-NoDaysShift,-1);

Date_Format = 'yyyy-mm-dd' ;
Date_Str = datestr(Bus_Date,Date_Format) ;

%% Make Output Folder

Date_Str_2 = strrep(datestr(now),':','_'); %% datestr(today,Date_Format)
Output_FolderPath = [pwd filesep 'Graph_Output'];
Destination_Path = [Output_FolderPath filesep strcat('Var_Price_Graph',Date_Str_2)];

if(~exist(Destination_Path))
    mkdir(Destination_Path);
else
    [status, message, messageid] = rmdir(Destination_Path,'s') ;
    while(status)
        [status, message, messageid] = rmdir(Destination_Path,'s') ;
    end
    mkdir(Destination_Path);
end

%% Color Specification

Color_Spec = {'k','m','c','r','g','b','w','y'};

%%
% VolIds(5:end) = []; %% For Testing

XLS_FileName  = getXLSFilename('Var_Price_Graph');

FilePath =fullfile(Output_FolderPath,XLS_FileName);
Create_Empty_XLS_Sheets_User_Defined_Variables(FilePath,VolIds);

%%

for i = 1 : length(VolIds)
    
    Current_VolID = VolIds{i};
    
    %     SqlQuery = ['select * from var_cont_price_change_table where cont_month = ',['''' Current_VolID ''''], ' and settlement_date >= ',['''' Date_Str '''']];
    SqlQuery = ['select * from var_cont_price_change_table where cont_month = ',['''' Current_VolID ''''], ' and settlement_date >= ',['''' Date_Str ''' and settlement_date <= ''',ValueDate,'''']];
    
    [DBFieldNames,DBData] = read_from_database('',0,SqlQuery);
    
    if(~strcmpi(DBData,'No Data'))
        
        
        ValueDate_Col = find(cellfun(@(V) strcmpi('settlement_date',V), DBFieldNames));
        Unique_Date = unique(DBData(:,ValueDate_Col));
        
        SettleValue_Col = find(cellfun(@(V) strcmpi('price_change',V), DBFieldNames));
        %         DBData(:,SettleVol_Col) =  num2cell( cell2mat(DBData(:,SettleVol_Col)) .* 100 );
        
        
        %% Open the Figure
        %   Fig = figure;
        Fig = figure('units','normalized','outerposition',[0 0 1 1]);
        
        %% Unique Date have Single Value so do not need this for loop
        
        %         for ii = 1 : length(Unique_Date)
        %             Date_Matched =  cellStrfind_exact(DBData(:,ValueDate_Col),Unique_Date(ii));
        %             Date_Matched_Data = DBData(Date_Matched,:);
        %
        %             x_Axis = datenum(Date_Matched_Data(:,ValueDate_Col));
        %             y_Axis = cell2mat(Date_Matched_Data(:,SettleValue_Col));
        
        %%
        x_Axis = datenum(DBData(:,ValueDate_Col));
        y_Axis = cell2mat(DBData(:,SettleValue_Col));
        
        subplot(2,1,1);
        %         grid on;grid minor;
        plot(x_Axis, y_Axis, [Color_Spec{6},'o'], 'Linestyle', '-' ); %% Color_Spec{ii}
        %         hold on;
        title([Current_VolID]);
        xlabel('Value Date');
        ylabel('Settle Value');
        
        datetick('x','yyyy-mm-dd');
        %         set(gca,'XTickLabel',char(DBData(:,ValueDate_Col)));
        grid on;grid minor;
        %             set(gca,'XTickLabel',char(datetime(DBData(:,ValueDate_Col))));
        
        %         legend(char(Unique_Date),'location','northeastoutside');
        %             legend(char(strcat((DBData(:,ValueDate_Col)),{' means '},num2str(x_Axis))),'location','northeastoutside');
        
        
        %% Set Points Value
        
        a = num2str(y_Axis(:));
        b = cellstr(a);
        c = strtrim(b);
        h = text(x_Axis,y_Axis,c);
        
        %% Unique for Loop end
        %   end
        
        %% Save Fig into XLS
        PlotInExcel(FilePath,Current_VolID,200, 200,'C1');
        
        %% Save Plots to PDF , fig or Image
        
        %         set(Fig,'Units','Inches');
        %         pos = get(Fig,'Position');
        %         set(Fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
        %         print(Fig,[Destination_Path filesep strrep(Current_VolID,'.','_')], '-dpdf', '-r0');
        
        %% Save Fig into Folder
        %  saveas(Fig,[Destination_Path filesep strcat(Current_VolID,'.bmp')] );
        savefig(Fig, [Destination_Path filesep strcat(Current_VolID,'.fig')] );
        
        close(gcf); %% Fig  %% gcf
        
        
        
        
    end
end

zip(strcat(Destination_Path,'.zip'),Destination_Path);
% msgbox('Finish','Finish') ;


