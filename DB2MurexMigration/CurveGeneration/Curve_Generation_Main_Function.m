
clc;
clear;

%%
theCell = {'VolID','FuturePrice','OptionPrice'};
[Selected_PortfolioNames, theChosenIDX] = uicellect(theCell,'Multi', 1);

if(ischar(Selected_PortfolioNames))
    Selected_PortfolioNames = cellstr(Selected_PortfolioNames);
end
hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');

for i = 1 : length(Selected_PortfolioNames)
    
    Current_Portfolio = char(Selected_PortfolioNames(i));
    
    if(strcmpi('VolID',Current_Portfolio))
        generate_curve_4_Vol_Months();
        waitbar(i/length(Selected_PortfolioNames),hWaitbar,'VolID  Finished');
    end
    
    if(strcmpi('FuturePrice',Current_Portfolio))
        generate_curve_4_Future_Price();
        waitbar(i/length(Selected_PortfolioNames),hWaitbar,'FuturePrice  Finished');
    end
    
    if(strcmpi('OptionPrice',Current_Portfolio))
        generate_curve_4_Option_Prices();
        waitbar(i/length(Selected_PortfolioNames),hWaitbar,'OptionPrice  Finished');
    end
    
end




