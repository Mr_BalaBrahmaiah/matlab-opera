function generate_curve_4_Price()

% Replaced by generate_curve_4_Option_Prices()

%%
% clc;
% clear;
warning('off','all');

%%% Use R2016b Version

%% Connect to Database
ObjDB = connect_to_database ;
% fetch(ObjDB,'select * from helper_fetch_volmonths_for_curve') ;
VolIds = fetch(ObjDB,'select * from helper_fetch_volmonths_for_curve') ;

%% Get Business Date

BusDate  = busdate(datenum(today)-7,-1) ;

Date_Format = 'yyyy-mm-dd' ;
Date_Str = datestr(BusDate,Date_Format) ;

%% Make Output Folder

Output_FolderPath = [pwd filesep 'Graph_Output'];
Destination_Path = [Output_FolderPath filesep strcat('zz_Price_Graph_',datestr(today,Date_Format))];

if(~exist(Destination_Path))
    mkdir(Destination_Path);
else
    [status, message, messageid] = rmdir(Destination_Path,'s') ;
    while(status)
        [status, message, messageid] = rmdir(Destination_Path,'s') ;
    end
    mkdir(Destination_Path);
end

%% Color Specification
Color_Spec = {'k','m','c','r','g','b','w','y'};

%%
% VolIds(5:end) = []; %% For Testing

for i = 1 : length(VolIds)
    
    Current_VolID = VolIds{i};
    
    %% Fetch Call Data
    SqlQuery_Call = ['select * from settlement_price_table where security_id like ',['''' Current_VolID,'C%', ''''], ' and value_date >= ',['''' Date_Str '''']];
    [DBFieldNames_Call,DBData_Call] = read_from_database('',0,SqlQuery_Call);
    
    [Strings_Call,Strike_Call] = Get_Seperate_String_Numbers_CellArray(DBData_Call(:,1));
    DBFieldNames_Call = [DBFieldNames_Call,{'strike'}];
    DBData_Call = [DBData_Call,Strike_Call];
    
    %% Fetch Put Data
    SqlQuery_Put = ['select * from settlement_price_table where security_id like ',['''' Current_VolID,'P%', ''''], ' and value_date >= ',['''' Date_Str '''']];
    [DBFieldNames_Put,DBData_Put] = read_from_database('',0,SqlQuery_Put);
    
    [Strings_Put,Strike_Put] = Get_Seperate_String_Numbers_CellArray(DBData_Put(:,1));
    DBFieldNames_Put = [DBFieldNames_Put,{'strike'}];
    DBData_Put = [DBData_Put,Strike_Put];
    
    if(~strcmpi(DBData_Call,'No Data')) %%|| (~strcmpi(DBData_Put,'No Data'))
        
        Date_Col = find(cellfun(@(V) strcmpi('value_date',V), DBFieldNames_Call));
        Unique_Date = unique(DBData_Call(:,Date_Col));
        
        SettlePrice_Col = find(cellfun(@(V) strcmpi('settlement_price',V), DBFieldNames_Call));
        DBData_Call(:,SettlePrice_Col) =  num2cell( cell2mat(DBData_Call(:,SettlePrice_Col)) .* 100 );
        
        Strike_Col = find(cellfun(@(V) strcmpi('strike',V), DBFieldNames_Call));
        
        %% Open the Figure
        %   Fig = figure;
        Fig = figure('units','normalized','outerposition',[0 0 1 1]);
        
        %%
        for ii = 1 : length(Unique_Date)
            if(~strcmpi(DBData_Call,'No Data'))
                Date_Matched_Call =  cellStrfind_exact(DBData_Call(:,Date_Col),Unique_Date(ii));
                Call_Data = DBData_Call(Date_Matched_Call,:);
                x_Call = cell2mat(Call_Data(:,Strike_Col));
                y_Call = cell2mat(Call_Data(:,SettlePrice_Col));
                
                subplot(2,1,1);
                plot(x_Call, y_Call, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                title([Current_VolID,' Call']);
                xlabel('Strike');
                ylabel('Settle Price');
                legend(char(Unique_Date),'location','northeastoutside');
                
            end
            
            if(~strcmpi(DBData_Put,'No Data'))
                Date_Matched_Put =  cellStrfind_exact(DBData_Put(:,Date_Col),Unique_Date(ii));
                Put_Data = DBData_Put(Date_Matched_Put,:);
                x_Put = cell2mat(Put_Data(:,Strike_Col));
                y_Put = cell2mat(Put_Data(:,SettlePrice_Col));
                
                subplot(2,1,2);
                plot(x_Put, y_Put, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                title([Current_VolID,' Put']);
                xlabel('Strike');
                ylabel('Settle Price');
                legend(char(Unique_Date),'location','northeastoutside');
                
            end
            
        end
        %% Move Legend Box
        %         Legend_Handle=findall(gcf,'tag','legend');
        %         set(Legend_Handle,'location','northeastoutside');
        
        %% Set Points Value
        
        %     a = num2str(x_Call(:));
        %     b = cellstr(a);
        %     c = strtrim(b);
        %     h = text(x_Call,y_Call,c);
        
        %% Save Plots to PDF , fig or Image
        
        set(Fig,'Units','Inches');
        pos = get(Fig,'Position');
        set(Fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
        print(Fig,[Destination_Path filesep strrep(Current_VolID,'.','_')], '-dpdf', '-r0');
        
        %  saveas(Fig,[Destination_Path filesep strcat(Current_VolID,'.bmp')] );
        savefig(Fig, [Destination_Path filesep strcat(Current_VolID,'.fig')] );
        
        close(gcf); %% Fig  %% gcf
        
        
        
        
    end
end


zip(strcat(Destination_Path,'.zip'),Destination_Path);
msgbox('Finish','Finish') ;






