function PlotInExcel(filename,sheetname,width, height,varargin)

%%
% Example :

% PlotInExcel('File.xls',SheetName,200, 200,'C1');

%%

options = varargin;

range = varargin{1};

[fpath,file,ext] = fileparts(char(filename));
if isempty(fpath)
    fpath = pwd;
end
Excel = actxserver('Excel.Application');
set(Excel,'Visible',0);
Workbook = invoke(Excel.Workbooks, 'open', [fpath filesep file ext]);


sheet = get(Excel.Worksheets, 'Item',sheetname);
invoke(sheet,'Activate');

ExAct = Excel.Activesheet;

ExActRange = get(ExAct,'Range',range);
ExActRange.Select;
pos=get(gcf,'Position');
% set(gcf,'Position',[ pos(1:2) width height])
print -dmeta

invoke(Excel.Selection,'PasteSpecial');

invoke(Workbook, 'Save');
invoke(Excel, 'Quit');
delete(Excel);




