function Create_Empty_XLS_Sheets_User_Defined_Variables(FilePath,Cell_Array)


Excel = actxserver ('Excel.Application');
% Excel.Visible = 1;

if ~exist(FilePath,'file')
    ExcelWorkbook=Excel.Workbooks.Add;
    ExcelWorkSheet = ExcelWorkbook.Sheets;
    
    for i = ExcelWorkSheet.Count+1 : length(Cell_Array)
        ExcelWorkbook.Sheets.Add;
    end
    
    ExcelWorkSheet = ExcelWorkbook.Sheets;
    
    for i = 1: length(Cell_Array)
        ExcelWorkSheet.Item(i).Name = [Cell_Array{i}];
        
    end
end

ExcelWorkbook.SaveAs(FilePath);
ExcelWorkbook.Close;
Excel.Quit;

end