function generate_curve_4_Vol_Months()
try
    % clc;
    % clear;
    warning('off','all');
    
    %%% Use R2016b Version
    
    %% Connect to Database
    ObjDB = connect_to_database ;
    % fetch(ObjDB,'select * from helper_fetch_volmonths_for_curve') ;
    VolIds = fetch(ObjDB,'select * from helper_fetch_volmonths_for_curve') ;
    
    ValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
    
    %% Get Business Date
    
    % Bus_Date  = busdate(datenum(today-3),-1) ;
    NoDaysShift = 7;
    Bus_Date = busdate(datenum(ValueDate,'yyyy-mm-dd')-NoDaysShift,-1);
    
    Date_Format = 'yyyy-mm-dd' ;
    Date_Str = datestr(Bus_Date,Date_Format) ;
    
    % SettlementDate = datestr(datenum(ValueDate,'yyyy-mm-dd')-1,Date_Format);
    SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
    
    %% Make Output Folder
    
    Date_Str_2 = strrep(datestr(now),':','_');  %% datestr(today,Date_Format)
    Output_FolderPath = [pwd filesep 'Graph_Output'];
    Destination_Path = [Output_FolderPath filesep strcat('VolID_Graph_',Date_Str_2)];
    
    if(~exist(Destination_Path))
        mkdir(Destination_Path);
    else
        [status, message, messageid] = rmdir(Destination_Path,'s') ;
        while(status)
            [status, message, messageid] = rmdir(Destination_Path,'s') ;
        end
        mkdir(Destination_Path);
    end
    
    %% Color Specification
    Color_Spec = {'y','k','m','c','r','g','b'};
    
    %%
    % VolIds(5:end) = []; %% For Testing
    
    XLS_FileName  = getXLSFilename('Vol_ID_Graph');
    
    FilePath =fullfile(Output_FolderPath,XLS_FileName);
    Create_Empty_XLS_Sheets_User_Defined_Variables(FilePath,VolIds);
    
    %%
    
    for i = 1 : length(VolIds)
        
        Current_VolID = VolIds{i};
        
        SqlQuery = ['select * from settlement_vol_surf_table where vol_id = ',['''' Current_VolID ''''], ' and value_date >= ',['''' Date_Str '''']];
        
        [DBFieldNames,DBData] = read_from_database('',0,SqlQuery);
        
        if(~strcmpi(DBData,'No Data'))
            
            Strike_Col = find(cellfun(@(V) strcmpi('strike',V), DBFieldNames));
            Date_Col = find(cellfun(@(V) strcmpi('value_date',V), DBFieldNames));
            Unique_Date = unique(DBData(:,Date_Col));
            
            CallPut_Col = find(cellfun(@(V) strcmpi('call_put_id',V), DBFieldNames)) ;
            
            SettleVol_Col = find(cellfun(@(V) strcmpi('settle_vol',V), DBFieldNames));
            DBData(:,SettleVol_Col) =  num2cell( cell2mat(DBData(:,SettleVol_Col)) .* 100 );
            
            %% Open the Figure
            %   Fig = figure;
            Fig = figure('units','normalized','outerposition',[0 0 1 1]);
            
            %%
            for ii = 1 : length(Unique_Date)
                Date_Matched =  cellStrfind_exact(DBData(:,Date_Col),Unique_Date(ii));
                Date_Matched_Data = DBData(Date_Matched,:);
                
                
                Call_Matched = cellStrfind_exact(Date_Matched_Data(:,CallPut_Col),{'C'});
                Put_Matched = cellStrfind_exact(Date_Matched_Data(:,CallPut_Col),{'P'});
                
                Call_Data = Date_Matched_Data(Call_Matched,:);
                x_Call = cell2mat(Call_Data(:,Strike_Col));
                y_Call = cell2mat(Call_Data(:,SettleVol_Col));
                %             y_Call = datenum(Call_Data(:,Date_Col));
                
                Put_Data = Date_Matched_Data(Put_Matched,:);
                x_Put = cell2mat(Put_Data(:,Strike_Col));
                y_Put = cell2mat(Put_Data(:,SettleVol_Col));
                %             y_Put = datenum(Put_Data(:,Date_Col));
                
                %%  Call Put Difference
                %             if( size(x_Call,1) == size(x_Put,1) )
                %                 Call_Put_Diff = y_Call - y_Put;
                %             else
                
                % call put vol difference curve to be plotte only for the day
                if strcmpi(Unique_Date(ii),ValueDate)
                    MissingX = setdiff(x_Call,x_Put);
                    MissingY = setdiff(x_Put,x_Call);
                    
                    if ~isempty(MissingY)
                        x_Call_Diff = [x_Call;MissingY];
                        Temp = zeros(size(MissingY,1),1);
                        y_Call_Diff = [y_Call;Temp];
                        CallData = sortrows([x_Call_Diff,y_Call_Diff]);
                        x_Call_Diff = CallData(:,1);
                        y_Call_Diff = CallData(:,2);
                    end
                    
                    if ~isempty(MissingX)
                        x_Put_Diff = [x_Put;MissingX];
                        Temp = zeros(size(MissingX,1),1);
                        y_Put_Diff = [y_Put;Temp];
                        PutData = sortrows([x_Put_Diff,y_Put_Diff]);
                        x_Put_Diff = PutData(:,1);
                        y_Put_Diff = PutData(:,2);
                    end
                    
                    
                    if ( ~isempty(MissingY) && ~isempty(MissingX) )
                        Call_Put_Diff_with_Change = round(y_Call_Diff - y_Put_Diff,6);
                        
                    else
                        Call_Put_Diff_without_Change = round(y_Call - y_Put,6);
                    end
                end
                %             end
                %%
                subplot(2,2,1);
                grid on;grid minor;
                plot(x_Call, y_Call, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                title([Current_VolID,' Call']);
                xlabel('Strike');
                ylabel('Settle Vol');
                legend(char(Unique_Date),'location','northeastoutside');
                
                
                subplot(2,2,2);
                grid on;grid minor;
                plot(x_Put, y_Put, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                title([Current_VolID,' Put']);
                xlabel('Strike');
                ylabel('Settle Vol');
                legend(char(Unique_Date),'location','northeastoutside');
                
                if exist('Call_Put_Diff_with_Change','var')
                    subplot(2,2,3);
                    plot(x_Put_Diff, Call_Put_Diff_with_Change, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                    grid on;grid minor;
                    title([Current_VolID,' Call Put Difference']);
                    xlabel('Strike');
                    ylabel('Settle Vol Difference');
                    legend(char(Unique_Date(ii)),'location','northeastoutside');
                    
                    
                    clear Call_Put_Diff_with_Change;
                end
                
                if exist('Call_Put_Diff_without_Change','var')
                    
                    subplot(2,2,3);
                    plot(x_Put, Call_Put_Diff_without_Change, [Color_Spec{ii},'o'], 'Linestyle', '-' ); hold on;
                    grid on;grid minor;
                    title([Current_VolID,' Call Put Difference']);
                    xlabel('Strike');
                    ylabel('Settle Vol Difference');
                    legend(char(Unique_Date(ii)),'location','northeastoutside');
                    
                    
                    clear Call_Put_Diff_without_Change;
                end
                
            end
            %% Move Legend Box
            %         Legend_Handle=findall(gcf,'tag','legend');
            %         set(Legend_Handle,'location','northeastoutside');
            
            %% Set Points Value
            
            %     a = num2str(x_Call(:));
            %     b = cellstr(a);
            %     c = strtrim(b);
            %     h = text(x_Call,y_Call,c);
            
            %% Save Fig into XLS
            
            PlotInExcel(FilePath,Current_VolID,200, 200,'C1');
            
            %% Save Plots to PDF , fig or Image
            
            %         set(Fig,'Units','Inches');
            %         pos = get(Fig,'Position');
            %         set(Fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
            %         print(Fig,[Destination_Path filesep strrep(Current_VolID,'.','_')], '-dpdf', '-r0');
            
            %% Save Fig into Folder
            %  saveas(Fig,[Destination_Path filesep strcat(Current_VolID,'.bmp')] );
            savefig(Fig, [Destination_Path filesep strcat(Current_VolID,'.fig')] );
            
            close(gcf); %% Fig  %% gcf
            
            
            
            
        end
    end
    
    
    zip(strcat(Destination_Path,'.zip'),Destination_Path);
    % msgbox('Finish','Finish') ;
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end





