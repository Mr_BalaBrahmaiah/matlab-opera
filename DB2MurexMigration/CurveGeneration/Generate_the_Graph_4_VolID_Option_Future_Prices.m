function varargout = Generate_the_Graph_4_VolID_Option_Future_Prices(varargin)
% GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES MATLAB code for Generate_the_Graph_4_VolID_Option_Future_Prices.fig
%      GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES, by itself, creates a new GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES or raises the existing
%      singleton*.
%
%      H = GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES returns the handle to a new GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES or the handle to
%      the existing singleton*.
%
%      GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES.M with the given input arguments.
%
%      GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES('Property','Value',...) creates a new GENERATE_THE_GRAPH_4_VOLID_OPTION_FUTURE_PRICES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Generate_the_Graph_4_VolID_Option_Future_Prices_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Generate_the_Graph_4_VolID_Option_Future_Prices_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Generate_the_Graph_4_VolID_Option_Future_Prices

% Last Modified by GUIDE v2.5 09-Feb-2017 12:01:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Generate_the_Graph_4_VolID_Option_Future_Prices_OpeningFcn, ...
    'gui_OutputFcn',  @Generate_the_Graph_4_VolID_Option_Future_Prices_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Generate_the_Graph_4_VolID_Option_Future_Prices is made visible.
function Generate_the_Graph_4_VolID_Option_Future_Prices_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Generate_the_Graph_4_VolID_Option_Future_Prices (see VARARGIN)

% Choose default command line output for Generate_the_Graph_4_VolID_Option_Future_Prices
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Generate_the_Graph_4_VolID_Option_Future_Prices wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Generate_the_Graph_4_VolID_Option_Future_Prices_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4

global ALL;

ALL = get(handles.checkbox4,'Value');

if(ALL)
    
    set(handles.checkbox1,'Value',1);
    set(handles.checkbox2,'Value',1);
    set(handles.checkbox3,'Value',1);
    
    
    set(handles.checkbox1,'Enable','off');
    set(handles.checkbox2,'Enable','off');
    set(handles.checkbox3,'Enable','off');
    
else
    
    set(handles.checkbox1,'Value',0);
    set(handles.checkbox2,'Value',0);
    set(handles.checkbox3,'Value',0);
    
    
    set(handles.checkbox1,'Enable','on');
    set(handles.checkbox2,'Enable','on');
    set(handles.checkbox3,'Enable','on');
    
    
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global VolID;global FuturePrice; global OptionPrice;global ALL;

VolID = get(handles.checkbox1,'Value');
FuturePrice = get(handles.checkbox2,'Value');
OptionPrice = get(handles.checkbox3,'Value');
ALL = get(handles.checkbox4,'Value');

if(VolID || FuturePrice || OptionPrice || ALL)
    
    PortfolioNames = {'VolID','FuturePrice','OptionPrice','ALL'};            %% Hard Code
    User_Select_Portfolio = {VolID,FuturePrice,OptionPrice,ALL};
    Selected_PortfolioNames = PortfolioNames(logical(cell2mat(User_Select_Portfolio)));
    
    hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');       %% Processing Waitbar
    
    
    if(length(Selected_PortfolioNames)>=3)
        WaitBar_Length = length(Selected_PortfolioNames)-1;
        
        generate_curve_4_Vol_Months();
        waitbar(1/WaitBar_Length,hWaitbar,[char(Selected_PortfolioNames(1)),' Finished']);
        
        generate_curve_4_Future_Price();
        waitbar(2/WaitBar_Length,hWaitbar,[char(Selected_PortfolioNames(2)),' Finished']);
        
        generate_curve_4_Option_Prices();
        waitbar(3/WaitBar_Length,hWaitbar,[char(Selected_PortfolioNames(3)),' Finished']);
        
    else
        WaitBar_Length = length(Selected_PortfolioNames);
        for i = 1 : length(Selected_PortfolioNames)
            
            Current_Portfolio = char(Selected_PortfolioNames(i));
            
            if(strcmpi('VolID',Current_Portfolio))
                generate_curve_4_Vol_Months();
            end
            
            if(strcmpi('FuturePrice',Current_Portfolio))
                generate_curve_4_Future_Price();
            end
            
            if(strcmpi('OptionPrice',Current_Portfolio))
                generate_curve_4_Option_Prices();
            end
            
            waitbar(i/WaitBar_Length,hWaitbar,[Current_Portfolio,' Finished']);
            
        end
        
    end
    
    close(hWaitbar);
    
    %     pause(4);
    %     delete(findobj(allchild(0), '-regexp', 'Tag', '^Msgbox_'));  %% Delete all child Objects (error dialog box)
    
    %     close(gcf);
    msgbox('Process Complete','Finish');
    
else
    close(gcf);
    msgbox('Please Select the any one of the portfolio','Warning');
end
