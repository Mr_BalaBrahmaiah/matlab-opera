function [DBFieldNames,DBData,ObjDB_ErrorMsg] = Call_StoredProcedure(ObjDB,StoreProcedure_Name,StoreProcedure_Input)
try
    
DBFieldNames = {'No Data'};
DBData = {''};
ObjDB_ErrorMsg = '';

%% Example
% ObjDB = connect_to_database;
% StoreProcedure_Name = 'generate_position_monitoring_data_agf';
% StoreProcedure_Input = '2018-02-15';
% sqlquery = '{call generate_position_monitoring_data_agf(''2018-02-15'')}';

%%
if(iscell(StoreProcedure_Name))
    StoreProcedure_Name = char(StoreProcedure_Name);
end

if(iscell(StoreProcedure_Input))
    StoreProcedure_Input = char(StoreProcedure_Input);
end

SqlQuery = ['{call ',char(StoreProcedure_Name),'(''',char(StoreProcedure_Input),''')}'] ; 
% SqlQuery = strrep(SqlQuery,'''','''''');

curs = exec(ObjDB,SqlQuery);

curs = fetch(curs);

AttrObj = attr(curs);
DBFieldNames = {AttrObj.fieldName};
DBData = curs.Data;

if ~isempty(curs.Message)
    ObjDB_ErrorMsg = {curs.Message};
end

close(curs);
% close(ObjDB);

catch ME
    DBFieldNames = {'Error Data'};
    DBData = {''};
end
