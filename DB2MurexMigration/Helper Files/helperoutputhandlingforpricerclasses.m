function Output = helperoutputhandlingforpricerclasses(obj, OutputSpecification, NoOutRow)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:07:53 $
%  $Revision: 1.1 $
%

if ~isrow(OutputSpecification)
    error('OutputSpecification should be row vector of cells');
end
if ~isscalar(NoOutRow)
    error('NoOutRow should be scalar');
end
NumOutputs = length(OutputSpecification);
switch(NumOutputs)
    case 1
        Output = cell(NoOutRow,NumOutputs);
        Output(:,:) = {' '};
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
    case 2
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        
    case 3
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        
    case 4
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        
    case 5
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        
    case 6
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        
    case 7
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        
    case 8
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        
    case 9
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{9}]))
            Output(:,9) = num2cell(obj.(['get',OutputSpecification{9}]));
        end
    case 10
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{9}]))
            Output(:,9) = num2cell(obj.(['get',OutputSpecification{9}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,10) = num2cell(obj.(['get',OutputSpecification{10}]));
        end        
    case 11
         Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{9}]))
            Output(:,9) = num2cell(obj.(['get',OutputSpecification{9}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,10) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,11) = num2cell(obj.(['get',OutputSpecification{10}]));
        end         
        
    case 12
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{9}]))
            Output(:,9) = num2cell(obj.(['get',OutputSpecification{9}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,10) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,11) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,12) = num2cell(obj.(['get',OutputSpecification{10}]));
        end         
        
    case 13
        Output = cell(NoOutRow,NumOutputs);
        
        if ~ isempty(obj.(['get',OutputSpecification{1}]))
            Output(:,1) = num2cell(obj.(['get',OutputSpecification{1}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{2}]))
            Output(:,2) = num2cell(obj.(['get',OutputSpecification{2}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{3}]))
            Output(:,3) = num2cell(obj.(['get',OutputSpecification{3}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{4}]))
            Output(:,4) = num2cell(obj.(['get',OutputSpecification{4}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{5}]))
            Output(:,5) = num2cell(obj.(['get',OutputSpecification{5}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{6}]))
            Output(:,6) = num2cell(obj.(['get',OutputSpecification{6}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{7}]))
            Output(:,7) = num2cell(obj.(['get',OutputSpecification{7}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{8}]))
            Output(:,8) = num2cell(obj.(['get',OutputSpecification{8}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{9}]))
            Output(:,9) = num2cell(obj.(['get',OutputSpecification{9}]));
        end
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,10) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,11) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,12) = num2cell(obj.(['get',OutputSpecification{10}]));
        end 
        if ~ isempty(obj.(['get',OutputSpecification{10}]))
            Output(:,13) = num2cell(obj.(['get',OutputSpecification{10}]));
        end         
    otherwise 
        Output = {'Implemented for 1 to 13 outputs only'};
end
end