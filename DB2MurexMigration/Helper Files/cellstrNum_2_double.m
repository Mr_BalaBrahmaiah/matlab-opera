function [OutputArray]=cellstrNum_2_double(inputcell)
% Function to convert an all numeric cell array to a double precision array
% ********************************************
% Usage: outputmatrix=cell2num(inputcellarray)
% ********************************************
% Output matrix will have the same dimensions as the input cell array
% Non-numeric cell contest will become NaN outputs in outputmat
% This function only works for 1-2 dimensional cell arrays

if ~iscell(inputcell), error('Input cell array is not.'); end

OutputArray =  cell(size(inputcell,1),1);
OutputArray_Num_Index =  zeros(size(inputcell));

for c=1:size(inputcell,2)
    for r=1:size(inputcell,1)
        if isnumeric(str2double(inputcell{r,c}))
            OutputArray(r,c) = num2cell(str2double(inputcell{r,c}));
            if(numel(inputcell{r,c}))
                OutputArray_Num_Index(r,c) = 1;
            else
                OutputArray_Num_Index(r,c) = 0;
            end
        else
            OutputArray_Num_Index(r,c)=NaN;
        end
    end
end

end