function OutFileName = getXLSFilename(Reportname)

objDB = connect_to_database;
DBValueDate = fetch(objDB,'select value_date from valuation_date_table');
DBValueDate = datestr(datenum(DBValueDate,'yyyy-mm-dd'),'dd-mmm-yyyy');

CurrentDateTime = strsplit(datestr(now),' ');
Time = CurrentDateTime{2};

TempName = [char(Reportname),'_',DBValueDate,'_',Time];
TempName = strrep(TempName,':','-');
TempName = strrep(TempName,' ','_');
OutFileName =  [TempName,'.xlsx'];

%% Check My PC
% [ret, PC_Name] = system('hostname');   
% 
% if ret ~= 0,
%    if ispc
%       PC_Name = getenv('COMPUTERNAME');
%    else      
%       PC_Name = getenv('HOSTNAME');      
%    end
% end
% 
% if(strcmpi(strtrim(PC_Name),'INVBLR-DEV06'))
%     Report_Path = 'D:\00_MUTHUKUMAR\WORKS\00_Office\zzz_Reports\DB2MurexMigration';
%     OutFileName = [Report_Path filesep OutFileName] ;
% end


