function Output = utilgetvaliddatearray(InputColumn)
% To handle the below errors
% EmptyValue
% ErrNull       #NULL!       -2146826288
% ErrDiv0       #DIV/0!      -2146826281
% ErrValue      #VALUE!      -2146826273
% ErrRef        #REF!        -2146826265
% ErrName       #NAME?       -2146826259
% ErrNum        #NUM!        -2146826252
% ErrNA         #N/A         -2146826246
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:07:53 $
%  $Revision: 1.1 $

    if ~iscolumn(InputColumn)
        error('InputColumn should be column vector');
    end

    if iscell(InputColumn) 
        IdxEmpty = cellfun('isempty',InputColumn);
        Y1 = nan(size(InputColumn));
        InputColumn(IdxEmpty) = {NaN};
%         Y1 = double([InputColumn{:}]'); [InputColumn{:}] converting to int32 loss of decimal place
        for i = 1:length(InputColumn)           
            Y1(i,1) = double(datenum(cell2mat(InputColumn(i,1))));           
        end

    else
        Y1 = InputColumn;
    end

    Idx = Y1 == -2146826288 | Y1 == -2146826281 | Y1 == -2146826273 | Y1 == -2146826265 | Y1 == -2146826259 | Y1 == -2146826252 | Y1 == -2146826246;
    if any(Idx)
        Y1(Idx) = NaN; 
    end
    Output = Y1;
end
