function [Consolidate_Header,Consolidate_Data] = Consolidate_CellArray(InputHeader,InputData,UniqueFields,SumFields,OutputFields)

% UniqueFields = {'product_code','contract_month','call_put_id','strike'};
% SumFields = {'active_lots','settle_delta_1','settle_gamma_11','settle_vega_1','settle_theta','mkt_value_usd'};
% OutputFields = {'settle_price'};

%%

UniqueFields_Col = cellStrfind_Perfect(InputHeader,UniqueFields);
SumFields_Col = cellStrfind_Perfect(InputHeader,SumFields);
OutputFields_Col = cellStrfind_Perfect(InputHeader,OutputFields);

[uCA,ndx,pos] = uniqueRowsCA(InputData(:,UniqueFields_Col));

Unique_Index = unique(pos) ;

% Consolidate_Data = cell(size(Unique_Index,1) ,length([UniqueFields,OutputFields,SumFields]));
Consolidate_Data = [];

for i = 1 : length(Unique_Index)

    Matched_Index = find(ismember( pos ,  Unique_Index(i)));
    
    if(size(Matched_Index,1)>1)
    Sum_Col_Fields = num2cell( sum( cell2mat(InputData(Matched_Index,SumFields_Col )) ));
    
    else
        Sum_Col_Fields = num2cell( cell2mat(InputData(Matched_Index,SumFields_Col )) );
    end
    
    [Unique_Col_Fields,~,~] = uniqueRowsCA(InputData(Matched_Index,[UniqueFields_Col;OutputFields_Col])) ;

    Temp_Row = [Unique_Col_Fields,Sum_Col_Fields] ; 
    
    Consolidate_Data = [Consolidate_Data;Temp_Row];
    
end

Consolidate_Header = [UniqueFields,OutputFields,SumFields];




