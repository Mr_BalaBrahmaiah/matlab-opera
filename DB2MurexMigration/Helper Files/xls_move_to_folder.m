function xls_move_to_folder(OutXLSFile_Name)

%% Get Destination Directory

try
DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
IdxFound = cellStrfind(DBConfigData,'reportsfolder');
if ~isempty(IdxFound)
    [~,SharedFolder] = strtok(DBConfigData(IdxFound),'=');
    SharedFolder = char(strrep(SharedFolder,'=',''));
else
    SharedFolder = '\\SGTCX-DBAPP01\Opera\download';
end
catch
    SharedFolder = '\\SGTCX-DBAPP01\Opera\download';
end

%% Check My PC

[ret, PC_Name] = system('hostname');   

if ret ~= 0,
   if ispc
      PC_Name = getenv('COMPUTERNAME');
   else      
      PC_Name = getenv('HOSTNAME');      
   end
end

if(strcmpi(strtrim(PC_Name),'INVBLR-DEV06'))
    SharedFolder = 'D:\00_MUTHUKUMAR\WORKS\00_Office\zzz_Reports\DB2MurexMigration';    
end


%% Move to Folder

DestinationFile = fullfile(SharedFolder,OutXLSFile_Name);
movefile(OutXLSFile_Name,DestinationFile);

