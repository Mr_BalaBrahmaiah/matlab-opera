function Output = utilgetcleanedvectorpair(X, Y, OutputSpecification)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/05/27 11:04:57 $
%  $Revision: 1.2 $
% 

    if iscell(Y) 
        IdxEmpty = cellfun('isempty',Y);
        Y(IdxEmpty) = num2cell(0);
        Y1 = double([Y{:}]);
    else
        Y1 = Y;
        IdxEmpty = Y1 == 0;
    end

    
    validateattributes(X,  {'numeric'}, {'nonempty', 'real'},                  'cleaningvectorpair', 'X');
    validateattributes(Y1, {'numeric'}, {'nonempty', 'real', 'size', size(X)}, 'cleaningvectorpair', 'Y');
    
    Idx = IdxEmpty | Y1 == -2146826288 | Y1 == -2146826281 | Y1 == -2146826273 | Y1 == -2146826265 | Y1 == -2146826259 | Y1 == -2146826252 | Y1 == -2146826246;
    
    if any(Idx)
        X = X(~Idx);
        Y = Y1(~Idx);
    end
    
    if strcmpi(OutputSpecification,'x')
        Output = X;
    elseif strcmpi(OutputSpecification,'y')
        Output = Y;
    elseif strcmpi(OutputSpecification,'both')
        Output = [X Y];
    else
        error('Unknown Output Specification');
    end     
end
