function [RowSum,ColSum] = cell2sum_Row_Col(Input_Cell)

%% NaN to Zero
% Input_Cell(cellfun(@(x) any(isnan(x)),Input_Cell)) = {0};

%%

if(iscell(Input_Cell))
    
    [Row , Col] = size(Input_Cell);
    
    RowSum = [];
    ColSum = [];
    
    for i = 1 : Row
        
        Current_Row = Input_Cell(i,:);
        
        %         RowSum{i,1} = sum(cell2mat(Current_Row));
        RowSum{i,1} = nansum(cell2mat(Current_Row));
        
    end
    
    for ii = 1 :  Col
        
        Current_Col = Input_Cell(:,ii);
        
        %         ColSum{1,ii} = sum(cell2mat(Current_Col));
        ColSum{1,ii} = nansum(cell2mat(Current_Col));
        
        
    end
    
end

end
