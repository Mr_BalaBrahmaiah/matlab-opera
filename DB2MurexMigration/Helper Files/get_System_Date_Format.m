function [System_Date_Format,System_Date] = get_System_Date_Format()


%% Get System Date Format

System_Date_Format = get_System_Date_Format_Sub_Function();

System_Date = datestr(today, get_System_Date_Format_Sub_Function);

System_Date_Format = strrep(System_Date_Format,',','');
Split_Str = strsplit(System_Date_Format,' ');

if(strcmpi( char(Split_Str(1)),'mm'))
    System_Delimiter = '/';
end

if(strcmpi( char(Split_Str(1)),'dd'))
    System_Delimiter = '-';
end

System_Date_Format = strtrim(strrep(System_Date_Format,' ',System_Delimiter));
System_Date = datestr(today,System_Date_Format); %% System_Date

% fprintf('System Date Format : %s : System Date :  %s\n',System_Date_Format,System_Date);

%     disp(datestr(today, get_System_Date_Format_Sub_Function))
%     system('date');

end

%%  
% dateInstance = java.text.DateFormat.getDateInstance();
% aa = dateInstance.getDateFormatSymbols ;
% aa.getZoneStrings ;

%%
% import java.text.DateFormat;
% import java.text.SimpleDateFormat;
% 
% df = DateFormat.getDateInstance(DateFormat.SHORT); %% FULL, SHORT,MEDIUM ,LONG 
% dateFormat = char(df.toPattern())

  
 


