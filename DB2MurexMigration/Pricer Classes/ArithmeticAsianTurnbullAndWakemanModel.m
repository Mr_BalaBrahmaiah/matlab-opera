classdef ArithmeticAsianTurnbullAndWakemanModel < FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:08:19 $
%  $Revision: 1.1 $

    properties(GetAccess = private, SetAccess = private)
        CostOfCarry
        TTSAP  % Time to start of average periods in years
        OTIAP  % original time in average period in years, constant over life of an option
        TTM   
        Vol
        Strike
        AssetPrice
        RealizedAverage
        RFR
        OptionType
        Lots
        Curr_Mult
        Lot_Mult
        YearToDay_Mult
        IsExpiryProc
        Currency
    end
    
    methods (Access = public)
        function obj = ArithmeticAsianTurnbullAndWakemanModel(AssetPrice, RealizedAverageOfAssetPrice, Strike, RiskFreeRate, CostOfCarry, ...
                Volatility, TimeToMaturity, OriginalTimeInAveragePeriod, OptionType, Lots, Lot_Mult, Curr_Mult, IsExpiryProcessed,YearToDay_Mult,Currency)
            
            validateattributes(OptionType, {'char','cell'},{'nonempty'},'ArithmeticAsianTurnbullAndWakemanModel','OptionType');
            
            if ~all(ismember(lower(OptionType),{'fixed_strike_am_call','fixed_strike_am_put'}))
                error('Invalid Option Type. Value should be either fixed_strike_am_call or fixed_strike_am_put');
            else
                if ischar(OptionType)
                    obj.OptionType = cellstr(lower(OptionType));
                else
                    obj.OptionType = lower(OptionType);
                end
            end            
            NumInputs = size(obj.OptionType);
            
            validateattributes(AssetPrice,                  {'numeric'}, {'nonempty', 'positive',    'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'AssetPrice');
            validateattributes(RealizedAverageOfAssetPrice, {'numeric'}, {'nonempty', 'real',        'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'RealizedAverageOfAssetPrice');
            validateattributes(Strike,                      {'numeric'}, {'nonempty', 'nonnegative', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Strike');
            validateattributes(RiskFreeRate,                {'numeric'}, {'nonempty', 'real',        'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'RiskFreeRate');
            validateattributes(CostOfCarry,                 {'numeric'}, {'nonempty', 'real',        'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'CostOfCarry');
            validateattributes(Volatility,                  {'numeric'}, {'nonempty', 'nonnegative', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Volatility');
            validateattributes(TimeToMaturity,              {'numeric'}, {'nonempty', 'real',        'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'TimeToMaturity');
%           validateattributes(TimeToStartOfAveragePeriod,  {'numeric'}, {'nonempty', 'nonnegative', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'TimeToStartOfAveragePeriod');
            validateattributes(OriginalTimeInAveragePeriod, {'numeric'}, {'nonempty', 'nonnegative', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'OriginalTimeInAveragePeriod');
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Lots');
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Lot_Mult');            
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Curr_Mult');
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'IsExpiryProcessed');
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'YearToDay_Mult');
            validateattributes(Currency,              {'cell','char'},    {'nonempty',         'column', 'size', NumInputs}, 'ArithmeticAsianTurnbullAndWakemanModel', 'Currency');
            
            TimeToMaturity(TimeToMaturity<0) = 0.0;
            
            obj.AssetPrice = AssetPrice;
            obj.RealizedAverage = RealizedAverageOfAssetPrice;
            obj.Strike = Strike;
            obj.RFR = RiskFreeRate;
            obj.CostOfCarry = CostOfCarry;
            obj.Vol = Volatility;
            obj.TTM = TimeToMaturity;            
            obj.OTIAP = OriginalTimeInAveragePeriod;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.IsExpiryProc = IsExpiryProcessed;
            obj.YearToDay_Mult = YearToDay_Mult;
            obj.Currency = Currency;
            
            obj.TTSAP = zeros(NumInputs);
        end
        
        function Price = getprice(obj)
            S  = obj.AssetPrice;
            r  = obj.RFR;
            T  = obj.TTM;
            Curr = obj.Currency;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            YTDM = obj.YearToDay_Mult;
            bExpiryProcessed = obj.IsExpiryProc;
             
            OptType = obj.OptionType;
            Sa = obj.RealizedAverage;
            [X_e, b_e, v_e] = inputmanipulation(obj);
            
            Price = zeros(size(OptType));
            Idx = (strcmpi(OptType,'fixed_strike_am_call') & T == 0.0);
            if any(Idx)
                Price(Idx) = max(0, Sa(Idx)-obj.Strike(Idx));
            end
            Idx = (strcmpi(OptType,'fixed_strike_am_put') & T == 0.0);
            if any(Idx)
                Price(Idx) = max(0, obj.Strike(Idx)-Sa(Idx));
            end
            
            Idx = (T ~= 0.0);
            if any(Idx)
                tempOptType =  strrep(OptType,'fixed_strike_am','vanilla');
                Price(Idx) = VanillaOptionBlackScholesModel(S(Idx), X_e(Idx), r(Idx), b_e(Idx), v_e(Idx), T(Idx), tempOptType(Idx), L(Idx), LM(Idx), CM(Idx), bExpiryProcessed(Idx),YTDM(Idx),Curr(Idx)).getprice();
            end
            
        end
        
        function Delta = getdelta1(obj)
           S  = obj.AssetPrice;
           Sa = obj.RealizedAverage;
           r  = obj.RFR;
           T  = obj.TTM;
           OptType = obj.OptionType;
           Curr = obj.Currency;
           L = obj.Lots;
           LM = obj.Lot_Mult;
           CM = obj.Curr_Mult;
           YTDM = obj.YearToDay_Mult;
           bExpiryProcessed = obj.IsExpiryProc;
           
           T2 = obj.OTIAP;
           [X_e, b_e, v_e] = inputmanipulation(obj);
           
           Delta = zeros(size(OptType));
           
           Idx = (strcmpi(OptType,'fixed_strike_am_call') & T == 0.0);
           if any(Idx)
               if Sa(Idx) - obj.Strike(Idx) > 0
                   Delta(Idx) = 1./(T2(Idx).*256); % Assuming
               else
                   Delta(Idx) = 0.0;
               end
           end
           Idx = (strcmpi(OptType,'fixed_strike_am_put') & T == 0.0);
           if any(Idx)
               if obj.Strike(Idx) - Sa(Idx) > 0
                   Delta(Idx) = -1./(T2(Idx).*256); % Assuming
               else
                   Delta(Idx) = 0.0;
               end
           end
           
           Idx = (T ~= 0.0);
           if any(Idx)
               tempOptType =  strrep(OptType,'fixed_strike_am','vanilla');
               Delta(Idx) = VanillaOptionBlackScholesModel(S(Idx), X_e(Idx), r(Idx), b_e(Idx), v_e(Idx), T(Idx), tempOptType(Idx), L(Idx), LM(Idx), CM(Idx), bExpiryProcessed(Idx),YTDM(Idx),Curr(Idx)).getdelta1();               
           end           
           
        end
        
        function Gamma = getgamma11(obj)
           S  = obj.AssetPrice;
           r  = obj.RFR;
           T  = obj.TTM;
           OptType = obj.OptionType;
           Curr = obj.Currency;
           L = obj.Lots;
           LM = obj.Lot_Mult;
           CM = obj.Curr_Mult;
           YTDM = obj.YearToDay_Mult;
           bExpiryProcessed = obj.IsExpiryProc;
           
           [X_e, b_e, v_e] = inputmanipulation(obj);
           Gamma = zeros(size(OptType));
           
           Idx = (T == 0.0);
           if any(Idx)
               Gamma(Idx) = 0.0;
           end
           
           Idx = (T ~= 0.0);
           if any(Idx)
               tempOptType =  strrep(OptType,'fixed_strike_am','vanilla');
               Gamma(Idx) = VanillaOptionBlackScholesModel(S(Idx), X_e(Idx), r(Idx), b_e(Idx), v_e(Idx), T(Idx), tempOptType(Idx), L(Idx), LM(Idx), CM(Idx), bExpiryProcessed(Idx),YTDM(Idx),Curr(Idx)).getgamma11();
           end   
        end        
        
        function Vega = getvega1(obj)
           S  = obj.AssetPrice;
           %r  = obj.RFR;
           T  = obj.TTM;
           OptType = obj.OptionType;
           Curr = obj.Currency;
           L = obj.Lots;
           LM = obj.Lot_Mult;
           CM = obj.Curr_Mult;
           YTDM = obj.YearToDay_Mult;
           bExpiryProcessed = obj.IsExpiryProc;
           
           %[X_e, b_e, v_e] = inputmanipulation(obj);
           Vega = zeros(size(OptType));
           
           PricePlus  = ArithmeticAsianTurnbullAndWakemanModel(S, obj.RealizedAverage, obj.Strike,  obj.RFR, obj.CostOfCarry, (1+0.01)*obj.Vol, T, obj.OTIAP, OptType,L,LM,CM,bExpiryProcessed,YTDM,Curr).getprice();
           %VanillaOptionBlackScholesModel(S, X_e, r, b_e, (1+0.01).*v_e, T, OptType, 1, 1, 1, 365, 0, 0).getprice();
           PriceMinus = ArithmeticAsianTurnbullAndWakemanModel(S, obj.RealizedAverage, obj.Strike,  obj.RFR, obj.CostOfCarry, (1-0.01)*obj.Vol, T, obj.OTIAP, OptType,L,LM,CM,bExpiryProcessed,YTDM,Curr).getprice();
           %VanillaOptionBlackScholesModel(S, X_e, r, b_e, (1-0.01).*v_e, T, OptType, 1, 1, 1, 365, 0, 0).getprice();
           
           Idx = (T == 0.0);
           if any(Idx)
               Vega(Idx) = 0.0;
           end
           
           Idx = (T ~= 0.0);
           if any(Idx)
               Vega(Idx) = (PricePlus(Idx)-PriceMinus(Idx))./(2.*0.01.*obj.Vol(Idx));                       
           end       
        end
        
        function Theta = gettheta(obj)
           S  = obj.AssetPrice;
           r  = obj.RFR;
           T  = obj.TTM;
           OptType = obj.OptionType;
           Curr = obj.Currency;
           L = obj.Lots;
           LM = obj.Lot_Mult;
           CM = obj.Curr_Mult;
           YTDM = obj.YearToDay_Mult;
           bExpiryProcessed = obj.IsExpiryProc;
           
           [X_e, b_e, v_e] = inputmanipulation(obj);
           Theta = zeros(size(OptType));           
           
           Idx = (T == 0.0);
           if any(Idx)
               Theta(Idx) = 0.0;
           end
           
           Idx = (T ~= 0.0);
           if any(Idx)
               tempOptType =  strrep(OptType,'fixed_strike_am','vanilla');
               Theta(Idx) = VanillaOptionBlackScholesModel(S(Idx), X_e(Idx), r(Idx), b_e(Idx), v_e(Idx), T(Idx), tempOptType(Idx), L(Idx), LM(Idx), CM(Idx), bExpiryProcessed(Idx),YTDM(Idx),Curr(Idx)).gettheta();
           end        
        end           
        
    end
   
    methods (Access = private)
        function [X_e, b_e, v_e] = inputmanipulation(obj)
           b  = obj.CostOfCarry;
           t1 = obj.TTSAP; 
           T  = obj.TTM;
           T2 = obj.OTIAP; 
           v  = obj.Vol;
           X  = obj.Strike;
           Sa = obj.RealizedAverage;
           OptType = obj.OptionType;

           M1 = zeros(size(OptType));
           M2 = zeros(size(OptType));
           
           tau = max(0, T2-T);
           X_e = T2.*X./T - tau.*Sa./T;
           
           Idx = (b == 0.0);
           if any(Idx)               
               M1(Idx) = 1;
               M2(Idx) = (2.*exp(v(Idx).*v(Idx).*T(Idx)) - 2.*exp(v(Idx).*v(Idx).*t1(Idx)).*(1+v(Idx).*v(Idx).*(T(Idx)-t1(Idx)))) ./ (v(Idx).^4 .* (T(Idx)-t1(Idx)).^2);
           end
           
           Idx = (b ~= 0.0);
           if any(Idx)
               M1(Idx) = (exp(b(Idx).*T(Idx)) - exp(b(Idx).*t1(Idx)))./(b(Idx).*(T(Idx)-t1(Idx)));
               M2(Idx) = (2.*exp((2.*b(Idx)+v(Idx).*v(Idx)).*T(Idx))./((b(Idx)+v(Idx).*v(Idx)).*(2.*b(Idx)+v(Idx).*v(Idx)).*(T(Idx)-t1(Idx)).^2)) + ...
               ((2.*exp((2.*b(Idx)+v(Idx).*v(Idx)).*t1(Idx)))./(b(Idx).*(T(Idx)-t1(Idx)).^2)).*(1./(2.*b(Idx)+v(Idx).*v(Idx))-exp(b(Idx).*(T(Idx)-t1(Idx)))./(b(Idx)+v(Idx).*v(Idx)));
           end
           
           b_e = log(M1)./T;
           v_e = sqrt(log(M2)./T - 2.*b_e);            
        end
    end
    
end