classdef SpreadOptionBjerksundModel < FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/11/19 05:48:50 $
%  $Revision: 1.6 $

    %Asset1 - Asset2
    properties(GetAccess = private, SetAccess = private)
        Spot_1
        Spot_2
        Strike
        RFR
        OptType
        Vol_1
        Vol_2
        Corr
        TTM
        IsExpiryProcessed
        ExchangeATMConvention
        Lots
        Curr_Mult
        Lot_Mult
        YearToDay_Mult    
        TickSize
    end
    
    methods (Access = public)
        function obj = SpreadOptionBjerksundModel(Future_1, Future_2, Strike, Vol_1, Vol_2, Corr, RFR, TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,TickSize)    

            if ~all(ismember(lower(OptionType),{'spread_call','spread_put'}))
                error('Invalid Option Type .Value should be either spread_call/spread_put');
            else
                if ischar(OptionType)
                    obj.OptType = cellstr(lower(OptionType));
                else
                    obj.OptType = lower(OptionType);
                end
            end 
            
            NumInputs = size(obj.OptType);
            
            if isscalar(IsExpiryProcessed)
                IsExpiryProcessed = ones(NumInputs)*IsExpiryProcessed;
            end
            
            validateattributes(Future_1,              {'numeric'}, {'nonempty', 'positive',     'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Future_1');
            validateattributes(Future_2,              {'numeric'}, {'nonempty', 'positive',     'column', 'size', NumInputs}, 'SpreadOptionBjerksundModel', 'Future_2');
            validateattributes(Strike,                {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Strike');
            validateattributes(Vol_1,                 {'numeric'}, {'nonempty', 'nonnegative',  'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Vol_1');            
            validateattributes(Vol_2,                 {'numeric'}, {'nonempty', 'nonnegative',  'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Vol_2');            
            validateattributes(Corr,                  {'numeric'}, {'nonempty', '>=',-1,'<=',1, 'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Corr');            
            validateattributes(RFR,                   {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'RFR');            
            validateattributes(TimeToMaturity,        {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'TimeToMaturity'); 
            validateattributes(ExchangeATMConvention, {'cell','char'},    {'nonempty',          'column','size', NumInputs}, 'SpreadOptionBjerksundModel', 'ExchangeATMConvention'); 
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'binary',       'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'IsExpiryProcessed');    
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Lots');
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Lot_Mult');            
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative',  'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'Curr_Mult');
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative',  'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'YearToDay_Mult');            
            validateattributes(TickSize,              {'numeric'}, {'nonempty', 'real',         'column', 'size', NumInputs},  'SpreadOptionBjerksundModel', 'TickSize');
            
            TimeToMaturity(TimeToMaturity<0) = 0.0;
            
            obj.Spot_1 = Future_1;
            obj.Spot_2 = Future_2;
            obj.Strike = Strike;
            obj.Vol_1 = Vol_1;
            obj.Vol_2 = Vol_2;
            obj.Corr = Corr;
            obj.RFR = RFR;
            obj.TTM = TimeToMaturity;
            obj.IsExpiryProcessed = IsExpiryProcessed;
            obj.ExchangeATMConvention = ExchangeATMConvention;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.YearToDay_Mult = YearToDay_Mult;   
            obj.TickSize = TickSize;
        end
        
        function Price = getprice(obj)
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            T = obj.TTM;
            OptionType = obj.OptType;
            bExpProc = obj.IsExpiryProcessed;
            
            Price = zeros(size(OptionType));
            
            IdxExpProc = bExpProc == 1;
            
            Idx = (strcmpi(OptionType,'spread_call') & T == 0.0 & ~IdxExpProc);
            if any(Idx)
                Price(Idx) = max(0,F1(Idx)-F2(Idx)-X(Idx));                                
            end
            Idx = (strcmpi(OptionType,'spread_put') & T == 0.0 & ~IdxExpProc);
            if any(Idx)
                Price(Idx) = max(0, X(Idx)-F1(Idx)+F2(Idx));                
            end            
            
            Idx = (T ~= 0.0 & ~IdxExpProc);
            IdxPut = (strcmpi(OptionType,'spread_put') & T ~= 0.0 & ~IdxExpProc);
            if any(Idx)
                a     = F2(Idx) + X(Idx);
                b     = F2(Idx)./(F2(Idx)+X(Idx));
                v = sqrt(v1(Idx).^2 - 2.*b.*c(Idx).*v2(Idx).*v1(Idx) + (b.*v2(Idx)).^2);
                d1 = (log(F1(Idx)./a) + ( 0.5.*v1(Idx).^2 - b.*c(Idx).*v2(Idx).*v1(Idx) + 0.5.*b.^2.*v2(Idx).^2).*T(Idx))./(v.*sqrt(T(Idx)));
                d2 = (log(F1(Idx)./a) + (-0.5.*v1(Idx).^2 + c(Idx).*v2(Idx).*v1(Idx) + 0.5.*(b.*v2(Idx)).^2 - b.*v2(Idx).^2).*T(Idx))./(v.*sqrt(T(Idx)));
                d3 = (log(F1(Idx)./a) + (-0.5.*v1(Idx).^2 + 0.5.*(b.*v2(Idx)).^2).*T(Idx))./(v.*sqrt(T(Idx)));
                
                Price(Idx) = (F1(Idx).*normcdf(d1) - F2(Idx).*normcdf(d2) - X(Idx).*normcdf(d3)).*exp(-r(Idx).*T(Idx));
                Price(IdxPut) = Price(IdxPut) - (F1(IdxPut)-F2(IdxPut)-X(IdxPut)).*exp(-r(IdxPut).*T(IdxPut));
            end
        end
        
        function Delta_1 = getdelta1(obj, varargin)
            narginchk(1,2);
            OptionType = obj.OptType;
            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');            
                Change = varargin{1};
            end
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            X  = obj.Strike;
            T = obj.TTM;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            c = obj.Corr;
            r = obj.RFR;                

            obj1 = SpreadOptionBjerksundModel((F1+Change), F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel((F1-Change), F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            Delta_1 = (obj1.getprice()-obj2.getprice())./(2.*Change);

            Delta_1 = Delta_1.*obj.Lots;
        end
        
        function Delta_2 = getdelta2(obj, varargin)
            narginchk(1,2);
            OptionType = obj.OptType;              
            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');
                Change = varargin{1};
            end
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            X  = obj.Strike;  
            T = obj.TTM;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            c = obj.Corr;
            r = obj.RFR;

            obj1 = SpreadOptionBjerksundModel(F1, (F2+Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel(F1, (F2-Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
%             Delta_2 = (obj1.getprice()-obj2.getprice())./(2.*Change.*F2);
            Delta_2 = (obj1.getprice()-obj2.getprice())./(2.*Change);

            Delta_2 = Delta_2.*obj.Lots; 
        end 
        
        function Gamma_11 = getgamma11(obj, varargin)
            narginchk(1,2);

            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');            
                Change = varargin{1};
            end          
            T = obj.TTM;

            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;
            
            obj1 = SpreadOptionBjerksundModel((F1+Change), F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel((F1-Change), F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            objIn = SpreadOptionBjerksundModel(F1,F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);

%           Gamma_11 = (obj1.getprice()-2.*objIn.getprice()+obj2.getprice())./(Change.*F1).^2;
            Gamma_11 = (obj1.getprice()-2.*objIn.getprice()+obj2.getprice())./(Change.^2);
%           Gamma_11 = (obj1.getprice()- (2.*objIn.getprice()+obj2.getprice()))./(Change.^2);
            Gamma_11 = Gamma_11.*obj.Lots.*obj.Lot_Mult;
        end      
        
        function Gamma_22 = getgamma22(obj, varargin)
            narginchk(1,2);
            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');
                Change = varargin{1};
            end
            T = obj.TTM;
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;

            OptionType = obj.OptType;
            obj1 = SpreadOptionBjerksundModel(F1, (F2+Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel(F1, (F2-Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            objIn = SpreadOptionBjerksundModel(F1,F2, X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
%           Gamma_22 = (obj1.getprice()-2.*objIn.getprice()+obj2.getprice())./(Change.*F2).^2;
            Gamma_22 = (obj1.getprice()-2.*objIn.getprice()+obj2.getprice())./(Change.^2);
%           Gamma_22 = (obj1.getprice()- (2.*objIn.getprice()+obj2.getprice()))./(Change.^2);
            Gamma_22 = Gamma_22.*obj.Lots.*obj.Lot_Mult;
        end
        
        function Gamma_12 = getgamma12(obj, varargin)
            narginchk(1,2);
            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');            
                Change = varargin{1};
            end        
            T = obj.TTM;

            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;

            obj1 = SpreadOptionBjerksundModel(F1, (F2+Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel(F1, (F2-Change), X, v1, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
%           Gamma_12 = (obj1.getdelta1(Change)-obj2.getdelta1(Change))./(2.*Change.*F2); 
            Gamma_12 = (obj1.getdelta1(Change)-obj2.getdelta1(Change))./(2.*Change); 
            Gamma_12 = Gamma_12.*obj.Lots.*obj.Lot_Mult;
        end        
        
        function Vega_1 = getvega1(obj, varargin)
            narginchk(1,2);
            if nargin == 1
%                 Change = 0.1 .* obj.TickSize;
                Change = 0.0001;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');
                Change = varargin{1};
            end
            
            T = obj.TTM;
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;

            obj1 = SpreadOptionBjerksundModel(F1, F2, X, (v1+Change), v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            TempVol = max(0,v1-Change);
            obj2 = SpreadOptionBjerksundModel(F1, F2, X, TempVol, v2, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
%           Vega_1 = (obj1.getprice()-obj2.getprice())./(2.*Change.*v1);
            Vega_1 = (obj1.getprice()-obj2.getprice())./(2.*Change);
            Vega_1 = (Vega_1.*obj.Lots.*obj.Lot_Mult.*obj.Curr_Mult)/100;
        end
        
        function Vega_2 = getvega2(obj, varargin)
            narginchk(1,2);       
            if nargin == 1
%                 Change = 0.1 .* obj.TickSize;
                  Change = 0.0001;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');
                Change = varargin{1}; 
            end
            T = obj.TTM;
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;

            obj1 = SpreadOptionBjerksundModel(F1, F2, X, v1, (v2+Change), c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            TempVol = max(0,v2-Change);
            obj2 = SpreadOptionBjerksundModel(F1, F2, X, v1, TempVol, c, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
%           Vega_2 = (obj1.getprice()-obj2.getprice())./(2.*Change.*v2);
            Vega_2 = (obj1.getprice()-obj2.getprice())./(2.*Change);
            Vega_2 = (Vega_2.*obj.Lots.*obj.Lot_Mult.*obj.Curr_Mult)/100;
        end  
        
        function Chi = getchi(obj, varargin)
            narginchk(1,2);           
            if nargin == 1
%                 Change = 0.1 .* obj.TickSize;
                  Change = 0.0001;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','Change');            
                Change = varargin{1}; 
            end      
            T = obj.TTM;
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;
            
            UpCorrel = (c+Change);
%             UpCorrel(c==0) = 0.001;
            DownCorrel = (c-Change);
%             DownCorrel(c==0) = -0.001;
            

            obj1 = SpreadOptionBjerksundModel(F1, F2, X, v1, v2, UpCorrel,   r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel(F1, F2, X, v1, v2, DownCorrel, r, T, OptionType, ...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            Chi = (obj1.getprice()-obj2.getprice())./(UpCorrel-DownCorrel);
            Chi = Chi / 100;    
        end       
        
        function Theta = gettheta(obj, varargin)
            narginchk(1,2);
            OptionType = obj.OptType;
            NumInputs = size(OptionType);
            if nargin == 1
                Change = 1/(24*365);
                NumDay = repmat(Change,NumInputs);
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'SpreadOptionBjerksundModel','NumDay');
                NumDay = repmat(varargin{1}/(24*365), NumInputs); 
            end
            T = obj.TTM;
            F1 = obj.Spot_1;
            F2 = obj.Spot_2;
            v1 = obj.Vol_1;
            v2 = obj.Vol_2;
            X  = obj.Strike;
            c = obj.Corr;
            r = obj.RFR;
            OptionType = obj.OptType;

            obj1 = SpreadOptionBjerksundModel(F1, F2, X, v1, v2, c, r, T, OptionType,...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            obj2 = SpreadOptionBjerksundModel(F1, F2, X, v1, v2, c, r, T-NumDay, OptionType,...
                obj.Lots, obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.TickSize);
            Theta = (obj2.getprice()-obj1.getprice())./NumDay;
        
            Theta = (Theta.*obj.Lots.*obj.Lot_Mult.*obj.Curr_Mult)./obj.YearToDay_Mult;   
        end
        
        function BE1 = getbe1(obj)
            IsExpiryP = obj.IsExpiryProcessed;
            CM = obj.Curr_Mult;
            T = obj.TTM;
            
            BE1 = zeros(size(T));
            
            IdxTemp = ~(IsExpiryP==1 | T==0);
            Theta = obj.gettheta();
            Gamma = obj.getgamma12();
            BE1(IdxTemp) = sqrt(((-2.*Theta(IdxTemp)./CM(IdxTemp))./Gamma(IdxTemp)));
            IdxTemp = ~isreal(BE1);
            BE1(IdxTemp) = NaN;
        end
        
    end
end