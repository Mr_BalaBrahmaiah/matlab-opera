function VolSmile_val = VolSmile_fun_code (ATM_Vol, D_Risk_Reversal_25, D_Butterfly_25, Put_Delta)

    VolSmile_val = (ATM_Vol) + (2 .* (D_Risk_Reversal_25) .* (Put_Delta - 0.5)) + (16 .* (D_Butterfly_25) .* (Put_Delta - 0.5).^2);

end
