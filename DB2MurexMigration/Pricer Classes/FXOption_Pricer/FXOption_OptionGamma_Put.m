function [OptionGamma] = FXOption_OptionGamma_Put(Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    SpotC= (Spot) .* (1.01);
    SpotP= Spot;

    OptionUp = FXOption_OptionDelta_Put (SpotC ,strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    OptionDown = FXOption_OptionDelta_Put (SpotP, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    
    OptionGamma = (OptionUp) - (OptionDown);

% %     OptionGamma = (OptionGamma) .* (Notional) ;
    
end

