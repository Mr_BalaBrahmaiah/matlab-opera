
function Time_to_Maturity_yr = Time_maturity_in_date(StartDate,EndDate,Day_Count_Convention)

        StartDate=datetime(StartDate);
        EndDate=datetime(EndDate);
        StartDate1 = datenum(StartDate);
        EndDate1 = datenum(EndDate);
        Total_number_of_days = daysact(StartDate1,EndDate1) ;
        
        Time_to_Maturity_yr = Total_number_of_days ./ Day_Count_Convention ;

end
