function [OptionGamma] = FXOption_OptionTheta_Put (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    T_Bump=1;
    T_Bump_up= Time_matuarity_years-(T_Bump ./ 365);
 
    OptionUp = FXOptionPx_Put (Spot, strike, T_Bump_up, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);

    OptionDown = FXOptionPx_Put (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    
    OptionUp = round(OptionUp,8);
    OptionDown = round(OptionDown,8);
    
    OptionGamma = (OptionUp - OptionDown);

% %     OptionGamma = OptionGamma .* Notional ;

end

