function PutDelta_Strike_value = PutDelta_To_Strike ( Spot, Put_Delta, USD_Rate_Start, BLR_Rate_Start, Expiry_in_years, Vol_from_VolSmile)


value_1 = (exp( USD_Rate_Start .* Expiry_in_years) .* (-Put_Delta) + 1);
value_2 = ((norminv(value_1)) .* (Vol_from_VolSmile) .* (sqrt(Expiry_in_years)));
value_3 = (((BLR_Rate_Start - USD_Rate_Start) + (0.5 .* ((Vol_from_VolSmile).^2)) ) .* Expiry_in_years);
value_4 = (value_2 - value_3);
value_5 = exp(value_4);
PutDelta_Strike_value = Spot ./ value_5;

end
