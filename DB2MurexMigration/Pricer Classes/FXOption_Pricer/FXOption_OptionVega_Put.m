function [OptionVega] = FXOption_OptionVega_Put (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    V_Bump	= 0.00000001;
    V_Bump_up= Implied_Vol_sigma + V_Bump;
    V_Bump_Down= Implied_Vol_sigma - V_Bump;
 
    OptionUp = FXOptionPx_Put (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, V_Bump_up);

    OptionDown = FXOptionPx_Put (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, V_Bump_Down);

    OptionUp = round(OptionUp,8);
    OptionDown = round(OptionDown,8);
    
    OptionVega = 0.01 .* (OptionUp - OptionDown) ./ (V_Bump .* 2) ./ Spot;

% %     OptionVega = OptionVega .* Notional ;

end
