function y = linear_interpolate(x1, y1, x2, y2, x)

    y= y1 + ( ( (y2-y1) ./ (x2-x1) ).* (x-x1) );

end