function [OutErrorMsg ,OutData ] = FXOption_Pricer(Strike,Option_Type,End_Date)

try
    
    OutErrorMsg = {'No Errors'};
    OutData = [];
    OutXLSName = getXLSFilename('FXOption_Pricer_Output');
    
    %%Fetching Data
    
    ObjDB = connect_to_database;
     
%     Spot	= 3.7048; % take the spot value from underlying_Settle_value_table
    
%     Option_Type = {'put','put','call','put','put','call'};
%     End_Date = {'2018-12-28','2018-12-28','2018-12-28','2018-12-28','2018-12-28','2018-12-28'}; 
%     Strike	= [3.7,3.5,3.9,3.7,3.5,3.9];     % trade input
%     Notional = [-3000000,3000000,3000000,3000000,-3000000,-3000000]; % Lots of the trade


%%%% Newly added code
%     DBFxOptionData1 = importdata('TR Dump_FX Option Strip_USDBRL Deal Done.xlsx'); 
% 
%     Strike	= DBFxOptionData1.data(:,1);     % trade input
%     Option_Type = DBFxOptionData1.textdata(2:end,6);
%     End_Date = DBFxOptionData1.textdata(2:end,29);   % maturity_date of the trade
%     Notional = DBFxOptionData1.data(:,5);
%     ConterParty = DBFxOptionData1.textdata(2:end,8);
%%%%% end code
    
    
    DBDateData = fetch(ObjDB,'select value_date,settlement_date from valuation_date_table');     
    ValueDate  = DBDateData(1);
    SettlementDate = DBDateData(2); 
    
    Spot_Table = ['select settle_value from underlying_settle_value_table where settlement_date = ''',char(SettlementDate),''' and underlying_id = ''','CR BRLUSD Spot BGN',''' '] ;
    Spot_Val = fetch(ObjDB,Spot_Table); 
    Spot = cell2mat(Spot_Val);
    
    %To run for UAT Date from UAT database
    Start_Date = repmat(SettlementDate,size(Strike));

    %To run for PRODUCTION Date from UAT database
%     Start_Date = repmat(ValueDate,size(Strike));
     
    %%FXOption Pricer Table

    FXOption_Pricer_Tbl_Name = ('fxoption_data_table');    
       
    SqlQuery_FXOption_Pricer_Table = ['select * from ',char(FXOption_Pricer_Tbl_Name),' where settlement_date = ''',char(SettlementDate),''' '] ;
    [FXOption_Pricer_ColNames,FXOption_Pricer_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_FXOption_Pricer_Table);
    FxOptionData = (cell2dataset([FXOption_Pricer_ColNames;FXOption_Pricer_Table_Data]));
   
    Time_year=FxOptionData.forward_code(1:end-1,:); %<> 'SPOT'
    
    [r, c] = size(Time_year);
    for i=1:1:r
        Chat_Time_year=char(Time_year(i));
        Last_ltr=Chat_Time_year(end);
        First_ltr=str2double(Chat_Time_year(1:end-1));
        if Last_ltr == 'W'
            Expiry_years(i,1)=First_ltr/52;
        elseif Last_ltr == 'M'
            Expiry_years(i,1)=First_ltr/12;
        elseif Last_ltr == 'Y'
            Expiry_years(i,1)=First_ltr/1;
        end

    end

    FxOptionData.forward_code=Expiry_years;
    DBFxOptionData = sortrows(FxOptionData);

    %%% Input data file
    format long;

    ATM_Price= DBFxOptionData.atm_price(2:end,:)/100;
    D_Risk_Reversal_25= DBFxOptionData.riskreversal_price_25d(2:end,:)/100;
    D_Butterfly_25= DBFxOptionData.butterfly_price_25d(2:end,:)/100;

    USD_yield	= DBFxOptionData.usd_yield(2:end,:);
    Spot1 = DBFxOptionData.forward_rate(1,1);
    Forward_Rate = DBFxOptionData.forward_rate(2:end,:);
    Expiry_years = DBFxOptionData.forward_code(2:end,:);

%     Strike	= DBFxOptionData1.data(6,1:end)';     % trade input
%     Start_Date = DBFxOptionData1.textdata(3,2:end)'; % value_date.settlement_date
%     End_Date = DBFxOptionData1.textdata(4,2:end)';   % maturity_date of the trade

%     Option_Type = DBFxOptionData1.textdata(8,2:end)';

%     Strike  = [3.7,3.5,3.9];     % trade input
%     Start_Date = {'2018-10-18','2018-10-18','2018-10-18'}; 
%     End_Date = {'2018-12-28','2018-12-28','2018-12-28'}; 

    Call_Delta	= [0.06 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.94]; % fxoption_delta_ladder_table.call_delta

    Day_Count_Convention = repmat(360,size(Strike)); % fxoption_delta_ladder_table.days_per_year(1)
    Delta_call_Val=1;  % 100%
    Time = Time_maturity_in_date( Start_Date , End_Date, Day_Count_Convention);

    [row, col] = size(Call_Delta);
    Put_Delta = 1 - Call_Delta;
    VolSmile_Vol_Strike = VolSmile_fun_code(ATM_Price, D_Risk_Reversal_25, D_Butterfly_25, Put_Delta); % vol curve

    for i=1:1:r

        % intermediate outputs of forward table
        rUSD_cont(i,1) = log((1+(USD_yield(i)/100)).^ Expiry_years(i) ) / Expiry_years(i) ;

        BRL_yieldc(i,1) = 100 .*( rUSD_cont(i) + log(Forward_Rate(i) ./ Spot1)/ Expiry_years(i));

        rBRL_cont(i,1) = BRL_yieldc(i)/100 ;

    end


    for k=1:1:r
        for j=1:1:col

            Put_Delta = 1 - Call_Delta(j);

            PutDelta_Vol_Strike_value1 = PutDelta_To_Strike (Spot1,Put_Delta, rUSD_cont(k), rBRL_cont(k), Expiry_years(k) ,VolSmile_Vol_Strike(k,j)); % yield curve
            
            if isnan(PutDelta_Vol_Strike_value1)
               ValueUp= PutDelta_Vol_Strike_value(k-1,j);
               ValueDown= PutDelta_Vol_Strike_value(k-2,j);
               PutDelta_Vol_Strike_value1 = 2.*(ValueUp) - ValueDown ;  
            end

           PutDelta_Vol_Strike_value(k,j) = PutDelta_Vol_Strike_value1;
        end

    end

    [Up_Time,Down_Time,Up_USD_Vol,Down_USD_Vol,Up_BRL_Vol,Down_BRL_Vol,...
        Vol_Time_uP1,Vol_Time_up2,Vol_Time_down1,Vol_Time_down2,...
        Vol_strike_up1,Vol_strike_up2,Vol_strike_down1,Vol_strike_down2] = arrayfun(@Find_Min_Max_Vol_Yield_Curve,Time,Strike);

    % Implied Vol calculation
    LP_Vol_Strike_Up = linear_interpolate (Vol_Time_uP1, Vol_strike_up1, Vol_Time_up2, Vol_strike_up2, Strike);
    LP_Vol_Strike_Down = linear_interpolate (Vol_Time_down1, Vol_strike_down1, Vol_Time_down2, Vol_strike_down2, Strike);

    Implied_Vol_sigma = GetVolFromTenorStrike (Down_Time, Up_Time, LP_Vol_Strike_Down, LP_Vol_Strike_Up, Time);

    % calculate the interest rate parameters
    CCY1_Interest_Rate = (linear_interpolate (Down_Time, Down_USD_Vol, Up_Time, Up_USD_Vol, Time));
    CCY2_Interest_Rate = (linear_interpolate (Down_Time, Down_BRL_Vol, Up_Time, Up_BRL_Vol, Time)) ;

    %%% Calculations
    IdxCall = strcmpi('fx_vanilla_call',Option_Type);
    IdxPut = strcmpi('fx_vanilla_put',Option_Type);

    FXOptionPx_C_or_P(IdxCall) = FXOptionPx_Call (Spot, Strike(IdxCall),Time(IdxCall), CCY1_Interest_Rate(IdxCall),CCY2_Interest_Rate(IdxCall), Implied_Vol_sigma(IdxCall));
    FXOptionPx_C_or_P(IdxPut) = FXOptionPx_Put (Spot, Strike(IdxPut),Time(IdxPut), CCY1_Interest_Rate(IdxPut),CCY2_Interest_Rate(IdxPut), Implied_Vol_sigma(IdxPut));

% %     Option_Price_CCY2_C_or_P = FXOptionPx_C_or_P .* (Notional');

    Option_Price_CCY1_C_or_P = FXOptionPx_C_or_P ./ Spot;

% %     Option_Price_CCY1_Cash_C_or_P = Option_Price_CCY1_C_or_P .* (Notional');

    OptionDelta_P = FXOption_OptionDelta_Put(Spot, Strike, Time, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    
% %     sign_N=sign(Notional);
    
    OptionDelta_C_or_P(IdxCall) = Delta_call_Val +  OptionDelta_P(IdxCall);
    OptionDelta_C_or_P(IdxPut) = OptionDelta_P(IdxPut) ;
    
% %     OptionDelta_Premium_adj_C_or_P = OptionDelta_C_or_P - Option_Price_CCY1_C_or_P;
    
% %     OptionDelta_C_or_P = OptionDelta_C_or_P .* (sign_N');
% %     OptionDelta_Premium_adj_C_or_P = OptionDelta_Premium_adj_C_or_P .* (sign_N');

    OptionGamma_C_or_P(IdxCall) = FXOption_OptionGamma_Call ( Spot, Strike(IdxCall),Time(IdxCall), CCY1_Interest_Rate(IdxCall),CCY2_Interest_Rate(IdxCall), Implied_Vol_sigma(IdxCall));
    OptionGamma_C_or_P(IdxPut) = FXOption_OptionGamma_Put ( Spot, Strike(IdxPut),Time(IdxPut), CCY1_Interest_Rate(IdxPut),CCY2_Interest_Rate(IdxPut), Implied_Vol_sigma(IdxPut));

    OptionTheta_C_or_P(IdxCall) = FXOption_OptionTheta_Call ( Spot, Strike(IdxCall),Time(IdxCall), CCY1_Interest_Rate(IdxCall),CCY2_Interest_Rate(IdxCall), Implied_Vol_sigma(IdxCall));
    OptionTheta_C_or_P(IdxPut) = FXOption_OptionTheta_Put ( Spot, Strike(IdxPut),Time(IdxPut), CCY1_Interest_Rate(IdxPut),CCY2_Interest_Rate(IdxPut), Implied_Vol_sigma(IdxPut));


    OptionVega_C_or_P(IdxCall) = FXOption_OptionVega_Call ( Spot, Strike(IdxCall),Time(IdxCall), CCY1_Interest_Rate(IdxCall),CCY2_Interest_Rate(IdxCall), Implied_Vol_sigma(IdxCall));
    OptionVega_C_or_P(IdxPut) = FXOption_OptionVega_Put ( Spot, Strike(IdxPut),Time(IdxPut), CCY1_Interest_Rate(IdxPut),CCY2_Interest_Rate(IdxPut), Implied_Vol_sigma(IdxPut));

    %%% User want data
    User_want_Data = [Option_Price_CCY1_C_or_P', OptionDelta_C_or_P', OptionGamma_C_or_P',...
        OptionTheta_C_or_P',OptionVega_C_or_P'];

    try
    %%% xlswrite process
    Spot = repmat(Spot,size(Strike));
    
    %%% User required data to print and required header
    Required_Data = [Option_Type,num2cell(Strike),...
        Start_Date,End_Date,num2cell(Spot),...
        num2cell(CCY1_Interest_Rate),num2cell(CCY2_Interest_Rate),num2cell(Implied_Vol_sigma), ...
             num2cell(User_want_Data)];        
    
%          Required_Data = [Option_Type',num2cell(Strike'),num2cell(Notional'),...
%                 Start_Date',End_Date',num2cell(Spot'),...
%                 num2cell(CCY1_Interest_Rate'),num2cell(CCY2_Interest_Rate'),num2cell(Implied_Vol_sigma'), ...
%                 num2cell(User_want_Data)];
         
         Required_Header = {'OptionType','Strike','Start_Date','End_Date','Spot','CCY1_Interest_Rate','CCY2_Interest_Rate','Implied_Vol_sigma', ...
        'Option Price (CCY1%)','Delta','Gamma','Theta','Vega'};
         
    OutData = User_want_Data;
    %%Temp Check
    xlswrite(OutXLSName,[Required_Header;Required_Data],'FxOption_Pricer_Data');
   

    Required_Header1 = {'Expiry (years)','ATM','25D RR','25DBF'};
    Required_Header2 = {'Expiry (years)','FWD','USD yield','BRL yieldc','rUSD cont','rBRL cont'};
    Required_Header3={'Call Delta','Put Delta','Implied Volatilities'};
    
    Required_Data1 = [num2cell(Expiry_years),num2cell(ATM_Price),num2cell(D_Risk_Reversal_25),num2cell(D_Butterfly_25)];
    Required_Data2 = [num2cell(Expiry_years),num2cell(Forward_Rate),num2cell(USD_yield),num2cell(BRL_yieldc),num2cell(rUSD_cont),num2cell(rBRL_cont)];
    Required_Data3=[num2cell(Call_Delta);num2cell(1-Call_Delta);num2cell(VolSmile_Vol_Strike)];

    Required_New_Data=[Required_Header1;Required_Data1];
    Required_Data_Header=[Required_Header2;Required_Data2];
    
    xlswrite(OutXLSName,Required_New_Data,'Volatilities');
    xlswrite(OutXLSName,Required_Data_Header,'Strikes'); 

    sheet = 'Vol_curve&yield_curve';
    xlRange = 'B2';
    xlswrite(OutXLSName,Required_Data3,sheet,xlRange);  
 
    xlRange = 'A2';
    xlswrite(OutXLSName,Required_Header3',sheet,xlRange); 
    
    xlRange = 'A1';
    xlswrite(OutXLSName,{'Spot(FWD curve)'}',sheet,xlRange); 

    xlRange = 'B1';
    xlswrite(OutXLSName,Spot1,sheet,xlRange); 
    
    xlRange = 'A21';
    xlswrite(OutXLSName,{'Yield Strikes'}',sheet,xlRange); 

    xlRange = 'B21';
    xlswrite(OutXLSName,PutDelta_Vol_Strike_value,sheet,xlRange); 
    catch
    end

%%%% Subfunction --> Find_Min_Max_Vol_Yield_Curve(Time,Strike)
catch ME
    OutErrorMsg = {ME.message};    
end

 function [Up_Time,Down_Time,Up_USD_Vol,Down_USD_Vol,Up_BRL_Vol,Down_BRL_Vol,...
    Vol_Time_uP1,Vol_Time_up2,Vol_Time_down1,Vol_Time_down2,...
    Vol_strike_up1,Vol_strike_up2,Vol_strike_down1,Vol_strike_down2] = Find_Min_Max_Vol_Yield_Curve(Time,Strike)

    real_int_val=(Strike);%cellstr(Option_Type)
    [row1,col1]=size(Expiry_years);
    for jj=1:1:size(Expiry_years)

        if  Time < Expiry_years(1,1)     %% Time is less than Expiry_years (Time Out of Range)

            Up_Time = Expiry_years(jj+1);
            Down_Time = Expiry_years(jj);

            Up_USD_Vol = rUSD_cont(jj+1);  % added line
            Down_USD_Vol = rUSD_cont(jj); % added line

            Up_BRL_Vol = rBRL_cont(jj+1);  % added line
            Down_BRL_Vol = rBRL_cont(jj);  % added line

        for ii=1:1:col
                %%% added code- Time up -- Vol Time Up @ Vol Strike Up values
                if real_int_val > PutDelta_Vol_Strike_value(jj+1,ii)

                ii1 = ii-1;
                if ii1 == 0                 %%% ii <= 0, Strike out of Range
                Vol_Time_uP1 = PutDelta_Vol_Strike_value(jj+1,ii1+1);
                Vol_Time_up2 = PutDelta_Vol_Strike_value(jj+1,ii+1);
                Vol_strike_up1 = VolSmile_Vol_Strike(jj+1,ii1+1);
                Vol_strike_up2 = VolSmile_Vol_Strike(jj+1,ii+1);
                break;

                elseif ii <= col             %%% in between the Strike range
                Vol_Time_uP1=PutDelta_Vol_Strike_value(jj+1,ii1);
                Vol_Time_up2=PutDelta_Vol_Strike_value(jj+1,ii);
                Vol_strike_up1=VolSmile_Vol_Strike(jj+1,ii1);
                Vol_strike_up2=VolSmile_Vol_Strike(jj+1,ii);

                break;
                end
                end
                if ii == col                    %%% ii >= Col, Strike out of Range
                Vol_Time_uP1 = PutDelta_Vol_Strike_value(jj+1,col-1);
                Vol_Time_up2 = PutDelta_Vol_Strike_value(jj+1,col);
                Vol_strike_up1 =  VolSmile_Vol_Strike(jj+1,col-1);
                Vol_strike_up2 = VolSmile_Vol_Strike(jj+1,col);
                break;
                end


        end
        for ii=1:1:col
                %%% added code - Time Down -- Vol Time down @ Vol Strike down values
                if real_int_val > PutDelta_Vol_Strike_value(jj,ii)

                ii2 = ii-1;
                if ii2 == 0                  %%% ii <= 0, Strike out of Range
                Vol_Time_down1 = PutDelta_Vol_Strike_value(jj,ii2+1);
                Vol_Time_down2 = PutDelta_Vol_Strike_value(jj,ii+1);
                Vol_strike_down1 = VolSmile_Vol_Strike(jj,ii2+1);
                Vol_strike_down2 = VolSmile_Vol_Strike(jj,ii+1);
                break;

                elseif ii <= col              %%% in between the Strike range
                Vol_Time_down1=PutDelta_Vol_Strike_value(jj,ii2);
                Vol_Time_down2=PutDelta_Vol_Strike_value(jj,ii);
                Vol_strike_down1=VolSmile_Vol_Strike(jj,ii2);
                Vol_strike_down2=VolSmile_Vol_Strike(jj,ii);
                break;
                end
                end
                
                if ii == col                   %%% ii >= Col, Strike out of Range
                Vol_Time_down1 = PutDelta_Vol_Strike_value(jj,col-1);
                Vol_Time_down2 = PutDelta_Vol_Strike_value(jj,col);
                Vol_strike_down1 = VolSmile_Vol_Strike(jj,col-1);
                Vol_strike_down2 = VolSmile_Vol_Strike(jj,col);
                break;
                end
         end
        break;      

        elseif Expiry_years(jj) >= Time     %% in between Expiry_years

            Up_Time = Expiry_years(jj);
            Down_Time = Expiry_years(jj-1);

            Up_USD_Vol = rUSD_cont(jj);  % added line
            Down_USD_Vol = rUSD_cont(jj-1); % added line

            Up_BRL_Vol = rBRL_cont(jj);  % added line
            Down_BRL_Vol = rBRL_cont(jj-1);  % added line

          for ii=1:1:col
                %%% added code- Time up -- Vol Time Up @ Vol Strike Up values
                if real_int_val > PutDelta_Vol_Strike_value(jj,ii)

                ii1 = ii-1;
                if ii1 == 0          %%% ii <= 0, Strike out of Range
                Vol_Time_uP1 = PutDelta_Vol_Strike_value(jj,ii1+1);
                Vol_Time_up2 = PutDelta_Vol_Strike_value(jj,ii+1);
                Vol_strike_up1 = VolSmile_Vol_Strike(jj,ii1+1);
                Vol_strike_up2 = VolSmile_Vol_Strike(jj,ii+1);
                break;

                elseif ii <= col            %%% in between the Strike range
                Vol_Time_uP1=PutDelta_Vol_Strike_value(jj,ii1);
                Vol_Time_up2=PutDelta_Vol_Strike_value(jj,ii);
                Vol_strike_up1=VolSmile_Vol_Strike(jj,ii1);
                Vol_strike_up2=VolSmile_Vol_Strike(jj,ii);
                break;
                end

                end 
                if ii == col            %%% ii >= Col, Strike out of Range
                Vol_Time_uP1 = PutDelta_Vol_Strike_value(jj,col-1);
                Vol_Time_up2 = PutDelta_Vol_Strike_value(jj,col);
                Vol_strike_up1 = VolSmile_Vol_Strike(jj,col-1);
                Vol_strike_up2 = VolSmile_Vol_Strike(jj,col);
                break;
                end
          end
          for ii=1:1:col
                %%% added code - Time Down -- Vol Time down @ Vol Strike down values
                if real_int_val > PutDelta_Vol_Strike_value(jj-1,ii)

                ii2 = ii-1;
                if ii2 == 0            %%% ii <= 0, Strike out of Range
                Vol_Time_down1 = PutDelta_Vol_Strike_value(jj-1,ii2+1);
                Vol_Time_down2 = PutDelta_Vol_Strike_value(jj-1,ii+1);
                Vol_strike_down1 = VolSmile_Vol_Strike(jj-1,ii2+1);
                Vol_strike_down2 = VolSmile_Vol_Strike(jj-1,ii+1);
                break;

                elseif ii <= col          %%% in between the Strike range
                Vol_Time_down1=PutDelta_Vol_Strike_value(jj-1,ii2);
                Vol_Time_down2=PutDelta_Vol_Strike_value(jj-1,ii);
                Vol_strike_down1=VolSmile_Vol_Strike(jj-1,ii2);
                Vol_strike_down2=VolSmile_Vol_Strike(jj-1,ii);
                break;
                end

                end
                if ii == col                     %%% ii >= Col, Strike out of Range
                Vol_Time_down1 = PutDelta_Vol_Strike_value(jj-1,col-1);
                Vol_Time_down2 = PutDelta_Vol_Strike_value(jj-1,col);
                Vol_strike_down1 = VolSmile_Vol_Strike(jj-1,col-1);
                Vol_strike_down2 = VolSmile_Vol_Strike(jj-1,col);
                break;
                end 
          end
          break;

        end

        if jj == row1                      %% Time is greater than Expiry_years (Time Out of Range)

            Up_Time = Expiry_years(jj);
            Down_Time = Expiry_years(jj-1);

            Up_USD_Vol = rUSD_cont(jj);  % added line
            Down_USD_Vol = rUSD_cont(jj-1); % added line

            Up_BRL_Vol = rBRL_cont(jj);  % added line
            Down_BRL_Vol = rBRL_cont(jj-1);  % added line


                Vol_Time_uP1=0;
                Vol_Time_up2=0;
                Vol_strike_up1=0;
                Vol_strike_up2=0;
                
                Vol_Time_down1=0;
                Vol_Time_down2=0;
                Vol_strike_down1=0;
                Vol_strike_down2=0;
                
                disp('Time out of Range');
        break;
            
        end   
   
    end
 end
end



