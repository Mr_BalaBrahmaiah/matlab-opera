function [Final_Value] = FXOptionPx_Call (Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    %%%% Old Code
%     if (Time_matuarity_years <= 0)      
%         Time_matuarity_years = 0.000000001 ;   
%     else
%         %Time_matuarity_years = Time_matuarity_years;
%     end
%     
%     if (Implied_Vol_sigma <= 0) 
%         Implied_Vol_sigma = 0.000000001;    
%     else
%         %Implied_Vol_sigma = Implied_Vol_sigma;
%     end
    
    %%%% Newly added code
    Ind_less_time = find(Time_matuarity_years <= 0);   %%% check values are less than zero or not
    Ind_less_vol = find(Implied_Vol_sigma <= 0);       %%% check values are less than zero or not

    Time_matuarity_years(Ind_less_time,:) = 0.000000001 ; 
    Implied_Vol_sigma(Ind_less_vol,:) =   0.000000001;
    %%% End code
    
    d1 = ((log(Spot./strike)) + ((CCY2_Interest_Rate - CCY1_Interest_Rate) + 0.5 .* (Implied_Vol_sigma .* Implied_Vol_sigma)) .* Time_matuarity_years) ./ (Implied_Vol_sigma .* sqrt(Time_matuarity_years));
    d2 = d1 - (Implied_Vol_sigma .* sqrt(Time_matuarity_years));
    Value_1 = Spot .* (exp(-CCY1_Interest_Rate .* Time_matuarity_years));
    Value_2 = normcdf(d1);
    %Value_2 = normcdf(real(d1)); %%% Newly added line
    Value_3 = strike .* (exp(-CCY2_Interest_Rate .* Time_matuarity_years));
    Value_4 = normcdf(d2);
    %Value_4 = normcdf(real(d2));   %%% Newly added line
    
    Final_Value = (Value_1 .* Value_2) - (Value_3 .* Value_4);

end

