function Final_OutPut_value = GetVolFromTenorStrike (down_Time, up_Time, down_vol, up_vol, Input_Time)

        Alpha	= ( up_Time - Input_Time ) ./ ( up_Time - down_Time);
        Beta	= ( Input_Time - down_Time ) ./ ( up_Time - down_Time);
        
        Final_OutPut_value	=  sqrt(((Alpha .* down_Time .* down_vol .* down_vol) + (Beta .* up_Time .* up_vol .* up_vol)) ./ Input_Time);
        
        %%% Newly added code
        Final_OutPut_value = real(Final_OutPut_value);
        %%% find time is less than expiry or not
        Ind_less_time = find((down_Time < Input_Time) == 0);
        Final_OutPut_value(Ind_less_time,:) = down_vol(Ind_less_time,:) + ( Input_Time(Ind_less_time,:) - down_Time(Ind_less_time,:) ) .* ((up_vol(Ind_less_time,:) - down_vol(Ind_less_time,:)) ./ (up_Time(Ind_less_time,:) - down_Time(Ind_less_time,:)));
        %%% end code
end