function [Final_Value_OptionDelta] = FXOption_OptionDelta_Call(Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    S_Bump	= 0.00000001;
    
    SpotU= Spot + S_Bump;
    SpotD= Spot - S_Bump;
    
    OptionUp = FXOptionPx_Call (SpotU, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);

    OptionDown = FXOptionPx_Call (SpotD, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    
    OptionUp = round(OptionUp,8);
    OptionDown = round(OptionDown,8);
    
    Final_Value_OptionDelta = (OptionUp-OptionDown) ./ (S_Bump .* 2);
    
end