function [OptionGamma] = FXOption_OptionGamma_Call(Spot, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma)

    SpotU= (Spot) .* (1.01);
    SpotD= Spot;

    OptionUp = FXOption_OptionDelta_Call (SpotU ,strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
    OptionDown = FXOption_OptionDelta_Call (SpotD, strike, Time_matuarity_years, CCY1_Interest_Rate, CCY2_Interest_Rate, Implied_Vol_sigma);
 
    OptionGamma = (OptionUp) - (OptionDown);

% %     OptionGamma = (OptionGamma) .* (Notional) ;
    
end

