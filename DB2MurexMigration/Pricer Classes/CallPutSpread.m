%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/06/24 12:04:52 $
%  $Revision: 1.2 $

classdef CallPutSpread < FinancialInstruments
    properties(GetAccess = private, SetAccess = private)
        AssetPrice
        Strike1
        Strike2
        RiskFreeRate
        CostOfCarry
        Vol1
        Vol2
        TimeToMaturity
        OptionType
        Lots
        Lot_Mult
        Curr_Mult
        IsExpiryProcessed
        YearToDay_Mult
        Currency
        ObjCall1
        ObjCall2
        ObjPut1
        ObjPut2
    end
    
    methods (Access = public)
        function obj = CallPutSpread(AssetPrice, Strike1,Strike2, RiskFreeRate, CostOfCarry, Vol1,Vol2, TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, IsExpiryProcessed,YearToDay_Mult,Currency)
            
            if ischar(OptionType)
                OptionType = cellstr(OptionType);
            end
            if ~all(ismember(lower(OptionType),{'callspread','putspread'}))
                error('Invalid Option Type. Value should be either callspread/putspread');
            end
            
            NumInputs = size(OptionType);
            
            validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'AssetPrice');
            validateattributes(Strike1,               {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'Strike1');
            validateattributes(Strike2,               {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'Strike2');
            validateattributes(RiskFreeRate,          {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'RiskFreeRate');
            validateattributes(CostOfCarry,           {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'CostOfCarry');
            validateattributes(Vol1,                  {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'Vol1');
            validateattributes(Vol2,                  {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'Vol2');
            validateattributes(TimeToMaturity,        {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'TimeToMaturity');
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'Lots');
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'Lot_Mult');
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'Curr_Mult');
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'CallPutSpread', 'IsExpiryProcessed');
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'CallPutSpread', 'YearToDay_Mult');
            validateattributes(Currency,              {'cell','char'},    {'nonempty',         'column', 'size', NumInputs}, 'CallPutSpread', 'Currency');
            
            TimeToMaturity(TimeToMaturity<0) = 0.0;
            
            % assumption - strike1 > strike2
            obj.AssetPrice = AssetPrice;
            obj.Strike1 = Strike1;
            obj.Strike2 = Strike2;
            obj.RiskFreeRate = RiskFreeRate;
            obj.CostOfCarry = CostOfCarry;
            obj.Vol1 = Vol1;
            obj.Vol2 = Vol2;
            obj.TimeToMaturity = TimeToMaturity;
            obj.OptionType = OptionType;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.IsExpiryProcessed = IsExpiryProcessed;
            obj.YearToDay_Mult = YearToDay_Mult;
            obj.Currency = Currency;
            
            % compute the call and spread objects using strike1,vol1 and
            % strike2,vol2 which will be used in all computations in the
            % class
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                OptType = repmat(cellstr('vanilla_call'),size(find(IdxCall)));
                Strike = obj.Strike1(IdxCall); Volatility = obj.Vol1(IdxCall);
                obj.ObjCall1 = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxCall), Strike, obj.RiskFreeRate(IdxCall), obj.CostOfCarry(IdxCall), ...
                    Volatility, obj.TimeToMaturity(IdxCall), OptType, obj.Lots(IdxCall), obj.Lot_Mult(IdxCall), ...
                    obj.Curr_Mult(IdxCall), obj.IsExpiryProcessed(IdxCall),obj.YearToDay_Mult(IdxCall),obj.Currency(IdxCall));
                Strike = obj.Strike2(IdxCall); Volatility = obj.Vol2(IdxCall);
                obj.ObjCall2 = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxCall), Strike, obj.RiskFreeRate(IdxCall), obj.CostOfCarry(IdxCall), ...
                    Volatility, obj.TimeToMaturity(IdxCall), OptType, obj.Lots(IdxCall), obj.Lot_Mult(IdxCall), ...
                    obj.Curr_Mult(IdxCall), obj.IsExpiryProcessed(IdxCall),obj.YearToDay_Mult(IdxCall),obj.Currency(IdxCall));
            end
            
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                OptType = repmat(cellstr('vanilla_put'),size(find(IdxPut)));
                Strike = obj.Strike1(IdxPut); Volatility = obj.Vol1(IdxPut);
                obj.ObjPut1 = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxPut), Strike, obj.RiskFreeRate(IdxPut), obj.CostOfCarry(IdxPut), ...
                    Volatility, obj.TimeToMaturity(IdxPut), OptType, obj.Lots(IdxPut), obj.Lot_Mult(IdxPut), ...
                    obj.Curr_Mult(IdxPut), obj.IsExpiryProcessed(IdxPut),obj.YearToDay_Mult(IdxPut),obj.Currency(IdxPut));
                Strike = obj.Strike2(IdxPut); Volatility = obj.Vol2(IdxPut);
                obj.ObjPut2 = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxPut), Strike, obj.RiskFreeRate(IdxPut), obj.CostOfCarry(IdxPut), ...
                    Volatility, obj.TimeToMaturity(IdxPut), OptType, obj.Lots(IdxPut), obj.Lot_Mult(IdxPut), ...
                    obj.Curr_Mult(IdxPut), obj.IsExpiryProcessed(IdxPut),obj.YearToDay_Mult(IdxPut),obj.Currency(IdxPut));
            end
            
        end
        
        function Price = getprice(obj)
            Price = zeros(size(obj.OptionType));
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                obj1 = obj.ObjCall1; obj2 = obj.ObjCall2;
                Price(IdxCall) = obj2.getprice() - obj1.getprice();
            end
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                obj1 = obj.ObjPut1; obj2 = obj.ObjPut2;
                Price(IdxPut) = obj1.getprice() - obj2.getprice();
            end
        end
        
        function Delta = getdelta1(obj)
            Delta = zeros(size(obj.OptionType));
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                obj1 = obj.ObjCall1; obj2 = obj.ObjCall2;
                Delta(IdxCall) = obj2.getdelta1() - obj1.getdelta1();
            end
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                obj1 = obj.ObjPut1; obj2 = obj.ObjPut2;
                Delta(IdxPut) = obj1.getdelta1() - obj2.getdelta1();
            end
        end
        
        function Gamma11 = getgamma11(obj)
            Gamma11 = zeros(size(obj.OptionType));
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                obj1 = obj.ObjCall1; obj2 = obj.ObjCall2;
                Gamma11(IdxCall) = obj2.getgamma11() - obj1.getgamma11();
            end
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                obj1 = obj.ObjPut1; obj2 = obj.ObjPut2;
                Gamma11(IdxPut) = obj1.getgamma11() - obj2.getgamma11();
            end
        end
        
        function Vega1 = getvega1(obj)
            Vega1 = zeros(size(obj.OptionType));
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                obj1 = obj.ObjCall1; obj2 = obj.ObjCall2;
                Vega1(IdxCall) = obj2.getvega1() - obj1.getvega1();
            end
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                obj1 = obj.ObjPut1; obj2 = obj.ObjPut2;
                Vega1(IdxPut) = obj1.getvega1() - obj2.getvega1();
            end
        end
        
        function Theta = gettheta(obj)
            Theta = zeros(size(obj.OptionType));
            IdxCall = strcmpi('callspread',obj.OptionType);
            if any(IdxCall)
                obj1 = obj.ObjCall1; obj2 = obj.ObjCall2;
                Theta(IdxCall) = obj2.gettheta() - obj1.gettheta();
            end
            IdxPut = strcmpi('putspread',obj.OptionType);
            if any(IdxPut)
                obj1 = obj.ObjPut1; obj2 = obj.ObjPut2;
                Theta(IdxPut) = obj1.gettheta() - obj2.gettheta();
            end
        end
        
    end
end