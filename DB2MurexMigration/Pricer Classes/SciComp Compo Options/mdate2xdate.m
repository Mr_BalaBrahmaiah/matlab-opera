function excel_serial = mdate2xdate(matlab_serial)
%MDATE2XDATE Converts MATLAB serial date to Excel serial date
%
%   excel_serial = mdate2xdate(matlab_serial)
%
%   Summary: Converts MATLAB serial date (or string) to Excel serial date
%     See the following link for the serial date information
%     https://www.mathworks.com/help/exlink/convert-dates-between-microsoft-excel-and-matlab.html
%
%   Inputs: matlab_serial - Array of dates in MATLAB serial date number
%              or datetime form.
%
%   Outputs: Array of serial date numbers in Excel serial date number form.
%
%   Example: matlab_serial = '6/22/2018'
%
%            excel_serial = mdate2xdate(matlab_serial);
%
%            returns:
%
%            excel_serial = 43273
%
%   See also DATENUM

%   Copyright 2018 SciComp, Inc.
%https://www.mathworks.com/help/exlink/convert-dates-between-microsoft-excel-and-matlab.html

% check if cell array input
if iscell(matlab_serial)
    % convert to MATLAB matrix
    out = zeros(size(matlab_serial));
    for k = 1:numel(matlab_serial)
        % convert to MATLAB serial dates
        out(k) = datenum(matlab_serial{k});
    end
    matlab_serial = out;
end

% Convert date strings to serial date numbers if necessary
if any(ischar(matlab_serial(:))) || isdatetime(matlab_serial)
     matlab_serial = datenum(matlab_serial);
end

% Initialize all as NaN.  NaN dates should fall through as NaNs.
origSize = size(matlab_serial);
excel_serial = nan(origSize);

% Set conversion factor for both 1900 date systems
X2MATLAB1900 = 693961;

% Convert to the Excel serial date number
actual1900Idx = matlab_serial < X2MATLAB1900 + 60;
if any(actual1900Idx(:))
     excel_serial(actual1900Idx) = matlab_serial(actual1900Idx) - X2MATLAB1900;
end

% Excel erroneously believes 1900 was a leap year, so after February 28,
% 1900, we adjust to account for this.
corrected1900Idx = matlab_serial >= X2MATLAB1900 + 60;
if any(corrected1900Idx(:))
    excel_serial(corrected1900Idx) = matlab_serial(corrected1900Idx) - X2MATLAB1900 + 1;
end


