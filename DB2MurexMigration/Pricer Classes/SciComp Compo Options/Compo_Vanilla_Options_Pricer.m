function [OutErrorMsg,OutData] = Compo_Vanilla_Options_Pricer(InBUName,UnderlyingID,VolID,Strike,Option_Type,Maturity,P1_SettlePrice,SettlementDate,VolCurve,Currency)

OutErrorMsg = {'No Errors'};
OutData = [];

try
    ObjDB = connect_to_database;
    
    % Get the Domestic and Foreign curve dates and rates from the same Fx
    % Option Pricer results
    [OutErrorMsg,DBFxOptionData] = generateFxVolCurve(ObjDB,SettlementDate);
    if ~strcmpi(OutErrorMsg,'No Errors')
        return;
    end
    
    DZeroCurve.Dates = DBFxOptionData.Expiry_years(2:end);
    DZeroCurve.Rates = DBFxOptionData.rBRL_cont(2:end);
    FZeroCurve.Dates = DBFxOptionData.Expiry_years(2:end);
    FZeroCurve.Rates = DBFxOptionData.rUSD_cont(2:end);

    %%% Domestic holiday calendar
    DHolidayCal = {};
    FHolidayCal = {};
    
    % To SciComp model, 1 should be passed for Call and
    % -1 should be passed for Put
    IdxCall = contains(Option_Type,'vanilla_call');
    IdxPut = contains(Option_Type,'vanilla_put');
    
    CallOrPut = ones(size(Option_Type));
    CallOrPut(IdxPut) = -1;
    
    CallPutID = repmat({'C'},size(Option_Type));
    CallPutID(IdxPut) = {'P'};
    
    for iVol = 1:length(Option_Type)
        
        %%% Fetch Spot value and assign to FXStruct.X0 (Foreign exchange
        %%% rate)
        SqlQuery = ['select spot from currency_spot_mapping_table where currency = ''',Currency{iVol},''''];
        SpotFactor = fetch(ObjDB,SqlQuery);
        if ~isempty(SpotFactor)
            SqlQuery = ['select settle_value from underlying_settle_value_table where settlement_date = ''',...
                char(SettlementDate),''' and underlying_id = ''',char(SpotFactor),''' '] ;
            SpotValue = fetch(ObjDB,SqlQuery);
            if ~isempty(SpotValue)
                X0 = cell2mat(SpotValue);   %% old line -->FXStruct.X0 = cell2mat(SpotValue);
            else
                OutErrorMsg = {['Error@SciComp Compo Options: Spot Value for ''',SpotFactor,''' is missing in underlying_settle_value_table! Please contact OPERA IT!']};
                return;
            end
        else
            OutErrorMsg = {['Error@SciComp Compo Options: Currency ''',Currency{iVol},''' used in Compo option booking is not mapped in currency_spot_mapping_table! Please contact OPERA IT!']};
            return;
        end        

        %%% Fetch data for  FXStruct and ModelingStruct parameters
            %%% Fetch  the currencyPair from currency_forward_mapping_table
            SqlQuery_CurrencyPair = ['select product_code from currency_forward_mapping_table where currency = ''',char(Currency{iVol}),''' '];
            CurrencyPair = fetch(ObjDB,SqlQuery_CurrencyPair);

        if isempty(CurrencyPair)
            OutErrorMsg = {['Error@SciComp Compo Options: CurrencyPair for ''',char(CurrencyPair),''' is missing in currency_forward_mapping_table! Please contact OPERA IT!']};
            return;
        end 
            % TODO - Fetch the FxStruct values from table compo_options_fx_parameters_table_<InBUName> 
            Fx_parameters_Table_Name = ['compo_options_fx_parameters_table_',char(InBUName)];
            SqlQuery_2 = ['select sigma_x,rho_fx,sigma_c from ',char(Fx_parameters_Table_Name),' where settlement_date = ''',char(SettlementDate),''' and underlying_id = ''',char(UnderlyingID{iVol}),''' and currency_pair = ''',char(CurrencyPair),''' '];
            FXStruct_Cell = fetch(ObjDB,SqlQuery_2);
        if isempty(FXStruct_Cell)
          OutErrorMsg = {['Error@SciComp Compo Options: Data not Available in ',char(Fx_parameters_Table_Name)]};
          return;
        end 
            FXStruct_Data = [X0 FXStruct_Cell]; 

            FXStruct_Cols = {'X0','SigmaX','RhoFX','SigmaC'}; %% Exact col names based on SCiComp module description
            FXStruct_table = cell2table(FXStruct_Data,'VariableNames',FXStruct_Cols);  %%% data is in Table format
            FXStruct = table2struct(FXStruct_table);  %%% structure array data

            % TODO - Fetch the below values from table compo_options_modelling_parameters_table_<InBUName>
            Modelling_parameters_Table_Name = ['compo_options_modelling_parameters_table_',char(InBUName)];
            SqlQuery_3 = ['select accuracy,sigma_fbump,sigma_xbump,x0_bump,rho_fxbump,ff_bump from ',char(Modelling_parameters_Table_Name), ...
                    ' where settlement_date = ''',char(SettlementDate),''' and underlying_id = ''',char(UnderlyingID{iVol}),''' and currency_pair = ''',char(CurrencyPair),''' '];
            ModelingStruct_Data = fetch(ObjDB,SqlQuery_3);

        if isempty(ModelingStruct_Data)
          OutErrorMsg = {['Error@SciComp Compo Options: Data not Available in ',char(Modelling_parameters_Table_Name)]};
          return;
        end 
            %%% Exact col names based on SCiComp module description
            ModelingStruct_Cols = {'Accuracy','SigmaFBump','SigmaXBump','X0Bump','RhoFXBump','FFBump'};
            ModelingStruct_table = cell2table(ModelingStruct_Data,'VariableNames',ModelingStruct_Cols); %%% data is in Table format
            ModelingStruct = table2struct(ModelingStruct_table);  %%% structure array data
        %%% end Code

        %%% FX-FORWARD Calculation
        Day_Count_Convention = 360;
        Time = Time_maturity_in_date( SettlementDate ,  Maturity{iVol}, Day_Count_Convention);
        [Up_Time,Down_Time,Up_USD_Vol,Down_USD_Vol,Up_BRL_Vol,Down_BRL_Vol] = Find_Min_Max_Vol_Curve(Time,DBFxOptionData);

        % calculate the interest rate parameters
        CCY1_Interest_Rate = (linear_interpolate (Down_Time, Down_USD_Vol, Up_Time, Up_USD_Vol, Time));
        CCY2_Interest_Rate = (linear_interpolate (Down_Time, Down_BRL_Vol, Up_Time, Up_BRL_Vol, Time)) ;

        P2_Settle_Price = cell2mat(SpotValue) * (exp((CCY2_Interest_Rate - CCY1_Interest_Rate) .* Time));
        StrikeForVolCalc = Strike(iVol) ./ P2_Settle_Price;
        
        % construct the vol curve
        IdxVolCurveID = strcmpi(VolID{iVol},VolCurve.vol_id) & strcmpi(CallPutID{iVol},VolCurve.call_put_id);
        if ~any(IdxVolCurveID)
            OutData = [OutData;[0 0 0 0 0]];
            continue;
        end
        XStrike       = VolCurve.strike(IdxVolCurveID);
        YVol          = VolCurve.settle_vol(IdxVolCurveID);
        OutVol  = interp1(XStrike, YVol,StrikeForVolCalc,'linear',NaN);  %%% future lognormal implied vol on ValueDate
        
        %%% ForeignFuture  - Foreign Future struct
        ForeignFuture.FF0T = P1_SettlePrice(iVol);
        ForeignFuture.SigmaF = OutVol;
        
        %%% CompoOptionStruct - Compo Vanilla option parameters structure
        if datenum(Maturity{iVol},'yyyy-mm-dd') <= datenum(SettlementDate,'yyyy-mm-dd')
            RowData = [0, 0, 0, 0, 0];
        else
            CompoOptionStruct.ValueDate         = SettlementDate;
            CompoOptionStruct.FirstExerDate     = Maturity{iVol};
            CompoOptionStruct.LastExerDate      = Maturity{iVol};
            CompoOptionStruct.Strike            = Strike(iVol);
            CompoOptionStruct.PutCall           = CallOrPut(iVol);
            CompoOptionStruct.IsDaily           = 0;
            CompoOptionStruct.BarrierType       = 0;
            CompoOptionStruct.Barrier1          = 0;
            CompoOptionStruct.Barrier2          = 0;
            CompoOptionStruct.AmericanBarrier   = 0;
            CompoOptionStruct.SettleDays        = 0;

            %%% CSO required Outputs:
            [ PV, Theta, CompoVol, nDays, DeltaF, GammaF, VegaF, DeltaD, GammaD, ...
                DeltaX, VegaX, DeltaRho, FXForward ] = compobardaily(ForeignFuture, ...
                CompoOptionStruct, FXStruct, ModelingStruct, DZeroCurve, FZeroCurve, DHolidayCal, FHolidayCal);
            RowData = [ PV, DeltaD, GammaD, Theta, VegaF];
        end
        OutData = [OutData;RowData];
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end

%%%% Subfunction --> Find_Min_Max_Vol_Yield_Curve(Time,Strike)
function [Up_Time,Down_Time,Up_USD_Vol,Down_USD_Vol,Up_BRL_Vol,Down_BRL_Vol] = Find_Min_Max_Vol_Curve(Time,DBFxOptionData)
    
 Expiry_years = DBFxOptionData.Expiry_years(2:end);
 rUSD_cont = DBFxOptionData.rUSD_cont(2:end);
 rBRL_cont = DBFxOptionData.rBRL_cont(2:end);
 
    [row1,~]=size(Expiry_years);
    for jj=1:1:size(Expiry_years)

        if  Time < Expiry_years(1,1)     %% Time is less than Expiry_years(Time Out of Range)
            Up_Time = Expiry_years(jj+1);
            Down_Time = Expiry_years(jj);

            Up_USD_Vol = rUSD_cont(jj+1);  
            Down_USD_Vol = rUSD_cont(jj); 

            Up_BRL_Vol = rBRL_cont(jj+1);  
            Down_BRL_Vol = rBRL_cont(jj);  
            break;
            
        elseif Expiry_years(jj) >= Time     %% in between Expiry_years
            Up_Time = Expiry_years(jj);
            Down_Time = Expiry_years(jj-1);

            Up_USD_Vol = rUSD_cont(jj);  
            Down_USD_Vol = rUSD_cont(jj-1);

            Up_BRL_Vol = rBRL_cont(jj);  
            Down_BRL_Vol = rBRL_cont(jj-1);  
            break;
            
        end

        if jj == row1                      %% Time is greater than Expiry_years (Time Out of Range)
            Up_Time = Expiry_years(jj);
            Down_Time = Expiry_years(jj-1);

            Up_USD_Vol = rUSD_cont(jj); 
            Down_USD_Vol = rUSD_cont(jj-1); 

            Up_BRL_Vol = rBRL_cont(jj);  
            Down_BRL_Vol = rBRL_cont(jj-1);  
            break;
        end   
    end
 end