% compovanillamx.m Help file for compovanillamx MEX file 
%
%COMPOVANILLA Option on the Domestic Currency price of a Foreign Futures Contract
%
%   [PV, Theta, CompoVol, nDays, DeltaF, GammaF, VegaF, DeltaD, GammaD, ... 
%           DeltaX, VegaX, DeltaRho, FXForward] = compobardailymx(ForeignFuture, ...
%   CompoOptionStruct, FXStruct, ModelingStruct, DZeroCurve, FZeroCurve, DHolidayCal, FHolidayCal);
%
%   Created with: 
%   MATLAB R2018b
%   Platform: win64
%   Microsoft Visual C++ 2015 compiler

%   MEX File function.
