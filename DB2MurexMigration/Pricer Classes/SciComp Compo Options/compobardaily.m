function [ PV, Theta, CompoVol, nDays, DeltaF, GammaF, VegaF, DeltaD, GammaD, ... 
           DeltaX, VegaX, DeltaRho, FXForward ] = compobardaily(ForeignFuture, ...
    CompoOptionStruct, FXStruct, ModelingStruct, DZeroCurve, FZeroCurve, DHolidayCal, FHolidayCal)
%COMPOBARDAILY Option on the Domestic Currency price of a Foreign Futures Contract
% FF is the Foreign Futures price expressed in Foreign Currency
% X is the Exchange Rate expresed as the price in Domestic Currency of one unit of Foreign Currency
% FD = X*FF is the Foreign Futures price expressed in Domestic Currency
% Strikes and barriers are expressed in Domestic Futures price FD
%
% [ PV, Theta, CompoVol, nDays, DeltaF, GammaF, VegaF, DeltaD, GammaD, ... 
%           DeltaX, VegaX, DeltaRho, FXForward ] = compobardaily(ForeignFuture, ...
%    CompoOptionStruct, FXStruct, ModelingStruct, DZeroCurve, FZeroCurve, DHolidayCal, FHolidayCal);
%
% Inputs:
%   ForeignFuture  - Foreign Future struct
%               FF0T : future spot (on ValueDate) price denominated in foreign currency
%             SigmaF : future lognormal implied vol on ValueDate
%   CompoOptionStruct - Compo Vanilla option parameters structure
%          ValueDate : Value date
%      FirstExerDate : First exercise opportunity
%                      For EOD options (IsDaily=0), FirstExerDate and LastExerDate are equal and represent the option expiration.
%                      For Daily options FirstExerDate is typically ValueDate+1, but could be ValueDate + N for a forward starting Daily option.
%       LastExerDate : Last exercise opportunity
%		       (see comments on FirstExecDate		
%            IsDaily : IsDaily=0:  EOD option, 
%                      IsDaily=1: Daily option,
%		       (See comments on FirstExerDate of LastExerDate.)
%             Strike : Option Strike, denominated in Domestic Currency.  E.g. Strike is fixed at X*FF at inception.  
%            PutCall : Put = -1, Call = +1
%        BarrierType : BarrierType=1,    DownOut
%		       BarrierType=2,    UpOut
%                      BarrierType=3,    DownIn
%                      BarrierType=4,    UpIn Barrier
%           Barrier1 : See comment on BarrierType
%     AmericanBarrier: AmericanBarrier=0:  European barrier.  Barriers are monitored discretely (on ExerDates, 
%                      independently of whether the option can exercise on those dates. (Exercise is determined by the IsDaily switch).
%		       AmericanBarrier=1:  American barrier.  Barriers are monitored continuously from ValueDate through LastExerDate.
%         SettleDays : Settlement is in SettleDays business days
%
%      FXStruct       - FX parameters structure
%                 X0 : Foreign exchange rate X at ValueDate. 
%                      X is the price in Domestic Currency of one unit of Foreign Currency
%             SigmaX : Volatility of the Foreign Exchange rate X
%              RhoFX : Correlation of Exchange Rate X and Foreign Futures price FF
%             SigmaC : User specified composite volatility.
%                      If user-specified composite vol SigmaC is nonzero, then it is used and is output as CompoVol.
%                      If user-specified composite vol SigmaC is zero, then composite volatility is computed from SigmaF,
%                      SigmaX and RhoFX, and output as CompoVol. 
%   ModelingStruct - Modeling parameters structure
%       SigmaFBump : See comment on VegaF
%         SigmaXBump : See comment on VegaX
%             X0Bump : See comment on DeltaX
%          RhoFXBump : See comment on DeltaRho
%             FFBump : See comment on DeltaF
%   DZeroCurve      - Domestic Zero curve struct
%              Dates : An array of zero curve support dates
%              Rates : An array of zero curve rates (continuously compounded)
%   FZeroCurve      - Fomestic Zero curve struct 
%   DHolidayCal     - Domestic holiday calendar
%   FHolidayCal     - Domestic holiday calendar
%
% Outputs:
%  PV                - Present value of the CSO
%  Theta             - Change in PV when ValueDate is advanced one day.
%  CompoVol          - Composite volatility. If user-specified composite vol sigmaC is nonzero, then it is used and is output as CompoVol.
%                      If user-specified composite vol sigmaC is zero, then composite volatility is computed from SigmaF, 
%                      SigmaX and RhoFX, and output as CompoVol.
%  nDays             - Number of actual exercise days
%  DeltaF            - Sensitivity of PV to Foreign Futures price FF0T.
%                      d(PV)/d(FF0T)
%  GammaF            - Sensitivity of DeltaF to Foreign Futures price FF0T.
%                      d(DeltaF)/(d(FF0T)
%  VegaF             - Change in PV when the Foreign Futures volatility sigmaF is incremented by sigmaFBump.
%                      If user-specified composite volatility sigmaC is non-zero, VegaF is the change in PV for an increment of sigmaVolBump in sigmaC
%  DeltaD            - Sensitivity of PV to Foreign Futures price expressed in Domestic Currency.
%                      D(PV)/d(FF0T*X0)
%  GammaD            - Sensitivity of DeltaD to Foreign Futures price expressed in Domestic currency.
%                      d(DeltaD)/d(FF0T*X0)
%  DeltaX            - Change in PV when the Foreign Exchange rate X0 is incremented by X0Bump.
%  VegaX             - change in PV when the Foreign Exchange volatility sigmaX is incremented by sigmaXBump.
%                      If sigmaC is non-zero, then sigmaX and rhoX are not used. In this case, VegaX and DeltaRho are 0.
%  DeltaRho          - Change in PV when the Futures/FX correlation rhoFX is incremented by rhoFXBump
%                      If sigmaC is non-zero, then sigmaX and rhoX are not used.  In this case, VegaX and DeltaRho are 0.
%  FXForward         - Change in PV when the Futures/FX correlation rhoFX is incremented by rhoFXBump 

%   Copyright 2019 SciComp, Inc.

%---------------------------------------------------
% Input argument checking & default assignment
%---------------------------------------------------
if nargin < 8
    error('compobardaily:inputs','InsufficientInputs');
end

if ~isstruct(ForeignFuture)
    error('compobardaily:inputs','ForeignFuture must be a struct');
end

if ~isstruct(CompoOptionStruct)
    error('compobardaily:inputs','CompoOptionStruct must be a struct');
end

if ~isstruct(FXStruct)
    error('compobardaily:inputs','FxStruct must be a struct');
end

if ~isstruct(ModelingStruct)
    error('compobardaily:inputs','ModelingStruct must be a struct');
end

if ~isstruct(DZeroCurve)
    error('compobardaily:inputs','ZeroCurve must be a struct');
end

if ~isstruct(FZeroCurve)
    error('compobardaily:inputs','ZeroCurve must be a struct');
end

% convert dates to Excel serial date
CompoOptionStruct.ValueDate = mdate2xdate(CompoOptionStruct.ValueDate);
CompoOptionStruct.FirstExerDate = mdate2xdate(CompoOptionStruct.FirstExerDate);
CompoOptionStruct.LastExerDate = mdate2xdate(CompoOptionStruct.LastExerDate);
DHolidayCal = mdate2xdate(DHolidayCal);
FHolidayCal = mdate2xdate(FHolidayCal);

% convert cell arrays to arrays if needed
if iscell(DZeroCurve.Rates)
    DZeroCurve.Rates = cell2mat(DZeroCurve.Rates);
end

if iscell(DZeroCurve.Dates)
    DZeroCurve.Dates = cell2mat(DZeroCurve.Dates);
end

if ~isvector(DZeroCurve.Dates)
    error('compobardaily:inputs','DZeroCurve.Dates must be a vector');
end

if ~isvector(DZeroCurve.Rates)
    error('compobardaily:inputs','DZeroCurve.Rates must be a vector');
end

if numel(DZeroCurve.Dates) ~= numel(DZeroCurve.Rates)
    error('compobardaily:inputs','DZeroCurve data is inconsistent');
end

% convert cell arrays to arrays if needed
if iscell(FZeroCurve.Dates)
    FZeroCurve.Dates = cell2mat(FZeroCurve.Dates);
end

if iscell(FZeroCurve.Rates)
    FZeroCurve.Rates = cell2mat(FZeroCurve.Rates);
end

if ~isvector(FZeroCurve.Dates)
    error('compobardaily:inputs','FZeroCurve.Dates must be a vector');
end

if ~isvector(FZeroCurve.Rates)
    error('compobardaily:inputs','FZeroCurve.Rates must be a vector');
end

if numel(FZeroCurve.Dates) ~= numel(FZeroCurve.Rates)
    error('compobardaily:inputs','FZeroCurve data is inconsistent');
end

[PV, Theta, CompoVol, nDays, DeltaF, GammaF, VegaF, DeltaD, GammaD, ... 
           DeltaX, VegaX, DeltaRho, FXForward] = compobardailymx(ForeignFuture, ...
    CompoOptionStruct, FXStruct, ModelingStruct, DZeroCurve, FZeroCurve, DHolidayCal, FHolidayCal);
