function [OutErrorMsg,DBFxOptionData] = generateFxVolCurve(ObjDB,SettlementDate)

try
    OutErrorMsg = {'No Errors'};
    %OutXLSName = getXLSFilename('FXOption_SciComp'); 
    
    %%FXForward points Table
    FXOption_Pricer_Tbl_Name = ('fxoption_data_table');
    
    SqlQuery_FXOption_Pricer_Table = ['select * from ',char(FXOption_Pricer_Tbl_Name),' where settlement_date = ''',char(SettlementDate),''' '] ;
    [FXOption_Pricer_ColNames,FXOption_Pricer_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_FXOption_Pricer_Table);
    FxOptionData = (cell2dataset([FXOption_Pricer_ColNames;FXOption_Pricer_Table_Data]));
   
    Time_year=FxOptionData.forward_code(1:end-1,:); %<> 'SPOT'
%      Time_year=FxOptionData.forward_code(1:end,:); %<> 'SPOT'
    [r, ~] = size(Time_year);
    for i=1:1:r
        Chat_Time_year=char(Time_year(i));
        Last_ltr=Chat_Time_year(end);
        First_ltr=str2double(Chat_Time_year(1:end-1));
        if Last_ltr == 'W'
            Expiry_years(i,1)=First_ltr/52;
            Final_Time_year(i,1) = cellstr(Chat_Time_year);
        elseif Last_ltr == 'M'
            Expiry_years(i,1)=First_ltr/12;
            Final_Time_year(i,1) = cellstr(Chat_Time_year);
        elseif Last_ltr == 'Y'
            Expiry_years(i,1)=First_ltr/1;
            Final_Time_year(i,1) = cellstr(Chat_Time_year);
        end

    end

    FxOptionData.forward_code=Final_Time_year;
    FxOptionData.Expiry_years = Expiry_years;
    DBFxOptionData = sortrows(FxOptionData,'Expiry_years');
    
    %%% remove updated_by and updated_timestamp columns
    DBFxOptionData.updated_by = [];
    DBFxOptionData.updated_timestamp = [];
    
    %%% Input data file
    format long;

   DBFxOptionData.atm_price= DBFxOptionData.atm_price/100;
   DBFxOptionData.riskreversal_price_25d= DBFxOptionData.riskreversal_price_25d/100;
   DBFxOptionData.butterfly_price_25d= DBFxOptionData.butterfly_price_25d/100;
   
    USD_yield	= DBFxOptionData.usd_yield;
    Spot1 = DBFxOptionData.forward_rate(1,1);
    Forward_Rate = DBFxOptionData.forward_rate;
    Expiry_years = DBFxOptionData.Expiry_years;
    
    for i=1:1:r
        % intermediate outputs of forward table
        rUSD_cont(i,1) = log((1+(USD_yield(i)/100)).^ Expiry_years(i) ) / Expiry_years(i) ;

        BRL_yieldc(i,1) = 100 .*( rUSD_cont(i) + log(Forward_Rate(i) ./ Spot1)/ Expiry_years(i));

        rBRL_cont(i,1) = BRL_yieldc(i)/100 ;
    end

  DBFxOptionData.BRL_yieldc = BRL_yieldc; 
  DBFxOptionData.rUSD_cont = rUSD_cont;
  DBFxOptionData.rBRL_cont = rBRL_cont;

catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end
