% COMPOBARDAILY
% Version 1.3.1 (R2019a) 10-Sep-2019
%
% Files
%   compobardaily   - Option on the Domestic Currency price of a Foreign Futures Contract
%   compobardailymx - compovanillamx.m Help file for compovanillamx MEX file 
%   mdate2xdate     - Converts MATLAB serial date to Excel serial date
