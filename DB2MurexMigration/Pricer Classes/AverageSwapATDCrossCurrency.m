classdef AverageSwapATDCrossCurrency < FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/04/15 09:17:53 $
%  $Revision: 1.2 $
    
    %long -> float receiver
    properties(GetAccess = private, SetAccess = private)
        AssetPrice_1
        AssetPrice_2
        AvgTillDate        
        Lots
        OptionType       
        TND % Number of Average days
        NDE % Number of elapsed days
    end
    
    methods (Access = public)
        function obj = AverageSwapATDCrossCurrency(AssetPrice_1,AssetPrice_2, OptionType,AvgTillDate,TND,NDE,Lots,QuantoType)
            
            validateattributes(OptionType, {'char','cell'},{'nonempty'},'AverageSwapCrossCurrency','OptionType');
            
            if ~all(ismember(lower(OptionType),{'avg_swap_cc'}))
                error('Invalid Option Type. Value should be avg_swap_cc');
            else
                if ischar(OptionType)
                    obj.OptionType = cellstr(lower(OptionType));
                else
                    obj.OptionType = lower(OptionType);
                end
            end            
            NumInputs = size(obj.OptionType);
            
            validateattributes(AssetPrice_1,      {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'AssetPrice_1');                        
            validateattributes(AssetPrice_2,      {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'AssetPrice_2');                        
            validateattributes(AvgTillDate,       {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'AvgTillDate');
            validateattributes(TND,               {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'TND');
            validateattributes(NDE,               {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'NDE');
            validateattributes(Lots,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'Lots');
            validateattributes(QuantoType,        {'char','cell'}, {'nonempty' ,           'column','size', NumInputs}, 'AverageSwapCrossCurrency', 'QuantoType');
                                               
            Idx = strcmpi('division',QuantoType);
            if any(Idx)                 
                AssetPrice_2(Idx) = 1./AssetPrice_2(Idx);
                AssetPrice_2(isinf(AssetPrice_2(Idx))) = 0;
            end
                        
            obj.AssetPrice_1   = AssetPrice_1;
            obj.AssetPrice_2   = AssetPrice_2;
            obj.AvgTillDate    = AvgTillDate;            
            obj.Lots           = Lots;            
            obj.TND            = TND;
            obj.NDE            = NDE;
        end
        
        function Price = getprice(obj)           
         
%             Price = (((obj.NDE .* obj.AvgTillDate) + (obj.TND - obj.NDE) .* obj.AssetPrice_1 .* obj.AssetPrice_2) ./  obj.TND);
             Price = ((obj.NDE .* obj.AvgTillDate) + ((obj.TND - obj.NDE) .* obj.AssetPrice_1 .* obj.AssetPrice_2)) ./  obj.TND;
            
        end
        
        function Delta = getdelta1(obj)
                       
            Delta = (((obj.TND - obj.NDE) ./ obj.TND) .* obj.Lots);
           
        end
        
        function Gamma = getgamma11(obj)
            Gamma = ' ';        
        end        
        
        function Vega = getvega1(obj)
            Vega = ' ';           
           
        end
        
        function Theta = gettheta(obj)          
           Theta = ' ';
        end
           
        
    end
   
end