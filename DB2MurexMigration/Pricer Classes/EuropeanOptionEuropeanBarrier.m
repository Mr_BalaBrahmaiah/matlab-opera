classdef EuropeanOptionEuropeanBarrier < FinancialInstruments
    %
    %  Matlab Version(s):   R2013a
    %  Matlab Toolboxes required:
    %
    %  Invenio Commodity Services Private Limited
    %
    %  CVS
    %  $Author: invenio $
    %  $Date: 2014/11/19 05:51:04 $
    %  $Revision: 1.3 $
    
    properties (GetAccess = private, SetAccess = private)
        % option parameters
        AssetPrice
        Strike
        BarrierLevel
        Rebate
        IsBarBreached
        RFR
        CostOfCarry
        StrikeVol
        BarrierVol
        OptType
        TTM
        Lots
        Lot_Mult
        Curr_Mult
        YearToDay_Mult
        ExchangeATMConvention
        IsExpiryProcessed
        Currency
        TickSize
        % objects needed to compute price and greeks
        ObjDownCall1
        ObjDownCall2
        ObjDownVanCall
        ObjDownPut1
        ObjDownPut2
        ObjDownVanPut
        ObjUpCall1
        ObjUpCall2
        ObjUpVanCall
        ObjUpPut1
        ObjUpPut2
        ObjUpVanPut
        %indices needed to compute price and greeks
        IdxDICall
        IdxDOCall
        IdxDCall
        IdxDIPut
        IdxDOPut
        IdxDPut
        IdxUICall
        IdxUOCall
        IdxUCall
        IdxUIPut
        IdxUOPut
        IdxUPut
        CalcLots
    end
    
    methods (Access = public)
        function obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate, RFR, CostOfCarry, StrikeVol,BarrierVol, ...
                TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize,InVol1ID,VolCurve)
            
            
            ValidOptionTypes = {'Euro_up_in_call','Euro_up_in_put','Euro_up_out_call','Euro_up_out_put','Euro_down_in_call','Euro_down_in_put','Euro_down_out_call','Euro_down_out_put'};
            
            if ischar(OptionType)
                OptionType = cellstr(OptionType);
            end
            if any(~ismember(lower(OptionType), lower(ValidOptionTypes)))
                error('Invalid barrier option types');
            end
            
            NumInputs = size(OptionType);
            
            if isscalar(IsExpiryProcessed)
                IsExpiryProcessed = ones(NumInputs)*IsExpiryProcessed;
            end
            
            validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'AssetPrice');
            validateattributes(Strike,                {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Strike');
            validateattributes(BarrierLevel,          {'numeric'}, {'nonempty', 'positive',    'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'BarierLevel');
            validateattributes(Rebate,                {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Rebate');
            validateattributes(RFR,                   {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'RiskFreeRate');
            validateattributes(CostOfCarry,           {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'CostOfCarry');
            validateattributes(StrikeVol,             {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'StrikeVol');
            validateattributes(BarrierVol,            {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'BarrierVol');
            validateattributes(TimeToMaturity,        {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'TimeToMaturity');
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Lots');
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Lot_Mult');
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Curr_Mult');
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'YearToDay_Mult');
            validateattributes(ExchangeATMConvention, {'cell','char'},    {'nonempty',         'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'ExchangeATMConvention');
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'IsExpiryProcessed');
            validateattributes(Currency,              {'cell','char'},    {'nonempty',         'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'Currency');
            validateattributes(TickSize,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionEuropeanBarrier', 'TickSize');
            % TODO - add the check for rebate to be always equal to zero
            % add message 'rebate other than 0 is not implemented'
            
            TimeToMaturity(TimeToMaturity<0) = 0.0;
            RFR(isnan(RFR)) = 0.0;
            
            obj.AssetPrice = AssetPrice;
            obj.Strike = Strike;
            obj.BarrierLevel = BarrierLevel;
            obj.Rebate = Rebate;
            obj.RFR = RFR;
            obj.CostOfCarry = CostOfCarry;
            obj.StrikeVol  = StrikeVol;
            obj.BarrierVol  = BarrierVol;
            obj.OptType = OptionType;
            obj.TTM = TimeToMaturity;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.YearToDay_Mult = YearToDay_Mult;
            obj.ExchangeATMConvention = ExchangeATMConvention;
            obj.IsExpiryProcessed = IsExpiryProcessed;
            obj.Currency = Currency;
            obj.TickSize = TickSize;
            
            CalculateValues();
            function CalculateValues
                OptionType = obj.OptType;
                K = obj.Strike;
                B = obj.BarrierLevel;
                KVol = obj.StrikeVol;
                BVol = obj.BarrierVol;
                % compute Lots, B-K should always be positive
                CalcLots = (B - K) ./ obj.TickSize;
                IdxLots = CalcLots < 0;
                CalcLots(IdxLots) = (K(IdxLots) - B(IdxLots)) ./ obj.TickSize(IdxLots);

                % Euro_Down_In_Call
                IdxDICall = strcmpi('euro_down_in_call',OptionType);
                IdxDOCall = strcmpi('euro_down_out_call',OptionType);
                IdxDCall = (IdxDICall | IdxDOCall) & (B > K);
                if any(IdxDCall)                  
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018                    
                    OutVol    = zeros(size(IdxDCall));                   
                    TmpInVol  = InVol1ID(IdxDCall);
                    UniqVolID = unique(TmpInVol);
                    for iVol = 1:length(UniqVolID)
                        IdxInVolID    = IdxDCall & (strcmpi(UniqVolID{iVol},InVol1ID));
                        IdxVolCurveID = strcmpi(UniqVolID{iVol},VolCurve.vol_id) & strcmpi('C',VolCurve.call_put_id);
                        XStrike       = VolCurve.strike(IdxVolCurveID);
                        YVol          = VolCurve.settle_vol(IdxVolCurveID);                  
                        OutStrike     = (B(IdxInVolID)+ obj.TickSize(IdxInVolID));
                        try
                            OutVol(IdxInVolID)        = interp1(XStrike, YVol,OutStrike,'linear',NaN);
                        catch
                            OutVol(IdxInVolID) = 0; % if the Vol ID is expired, then XStrike and YVol will be empty; assigning OutVol = 0 in that scenario
                        end
                    end                 
                    % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                    
                    OptionType(IdxDCall) = {'callspread'};
                    obj.ObjDownCall1 = CallPutSpread(obj.AssetPrice(IdxDCall), B(IdxDCall), K(IdxDCall), obj.RFR(IdxDCall), ...
                        obj.CostOfCarry(IdxDCall),BVol(IdxDCall),KVol(IdxDCall), obj.TTM(IdxDCall), OptionType(IdxDCall),...
                        obj.Lots(IdxDCall), obj.Lot_Mult(IdxDCall), obj.Curr_Mult(IdxDCall), ...
                        obj.IsExpiryProcessed(IdxDCall),obj.YearToDay_Mult(IdxDCall),obj.Currency(IdxDCall));
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018
%                     obj.ObjDownCall2 =  CallPutSpread(obj.AssetPrice(IdxDCall),(B(IdxDCall)+ obj.TickSize(IdxDCall)),...
%                         B(IdxDCall), obj.RFR(IdxDCall), ...
%                         obj.CostOfCarry(IdxDCall),BVol(IdxDCall),BVol(IdxDCall), obj.TTM(IdxDCall), OptionType(IdxDCall),...
%                         obj.Lots(IdxDCall), obj.Lot_Mult(IdxDCall), obj.Curr_Mult(IdxDCall), ...
%                         obj.IsExpiryProcessed(IdxDCall),obj.YearToDay_Mult(IdxDCall),obj.Currency(IdxDCall));
                      obj.ObjDownCall2 =  CallPutSpread(obj.AssetPrice(IdxDCall),(B(IdxDCall)+ obj.TickSize(IdxDCall)),...
                        B(IdxDCall), obj.RFR(IdxDCall), ...
                        obj.CostOfCarry(IdxDCall),OutVol(IdxDCall),BVol(IdxDCall), obj.TTM(IdxDCall), OptionType(IdxDCall),...
                        obj.Lots(IdxDCall), obj.Lot_Mult(IdxDCall), obj.Curr_Mult(IdxDCall), ...
                        obj.IsExpiryProcessed(IdxDCall),obj.YearToDay_Mult(IdxDCall),obj.Currency(IdxDCall));
                    
                end
                
                % Euro_Down_Out_call
                OptionType(IdxDOCall) = {'vanilla_call'};
                if any(IdxDOCall)
                    obj.ObjDownVanCall = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxDOCall),...
                        K(IdxDOCall), obj.RFR(IdxDOCall), obj.CostOfCarry(IdxDOCall), ...
                        KVol(IdxDOCall), obj.TTM(IdxDOCall), OptionType(IdxDOCall), ...
                        obj.Lots(IdxDOCall), obj.Lot_Mult(IdxDOCall), obj.Curr_Mult(IdxDOCall), ...
                        obj.IsExpiryProcessed(IdxDOCall),obj.YearToDay_Mult(IdxDOCall),obj.Currency(IdxDOCall));
                end
                
                % Euro_Down_Out_put
                IdxDIPut = strcmpi('euro_down_in_put',OptionType);
                IdxDOPut = strcmpi('euro_down_out_put',OptionType);
                IdxDPut = (IdxDIPut | IdxDOPut) & (B < K);
                if any(IdxDPut)
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018                    
                    OutVol    = zeros(size(IdxDPut));                   
                    TmpInVol  = InVol1ID(IdxDPut);
                    UniqVolID = unique(TmpInVol);
                    for iVol = 1:length(UniqVolID)
                        IdxInVolID         = IdxDPut & (strcmpi(UniqVolID{iVol},InVol1ID));
                        IdxVolCurveID      = strcmpi(UniqVolID{iVol},VolCurve.vol_id) & strcmpi('P',VolCurve.call_put_id);
                        XStrike            = VolCurve.strike(IdxVolCurveID);
                        YVol               = VolCurve.settle_vol(IdxVolCurveID);                  
                        OutStrike          = (B(IdxInVolID)+ obj.TickSize(IdxInVolID));
                        try
                            OutVol(IdxInVolID) = interp1(XStrike, YVol,OutStrike,'linear',NaN);
                        catch
                            OutVol(IdxInVolID) = 0; % if the Vol ID is expired, then XStrike and YVol will be empty; assigning OutVol = 0 in that scenario
                        end
                    end                 
                    % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                     
                    OptionType(IdxDPut) = {'putspread'};
                    obj.ObjDownPut1 = CallPutSpread(obj.AssetPrice(IdxDPut), K(IdxDPut), B(IdxDPut), obj.RFR(IdxDPut), ...
                        obj.CostOfCarry(IdxDPut),KVol(IdxDPut),BVol(IdxDPut), obj.TTM(IdxDPut), OptionType(IdxDPut),...
                        obj.Lots(IdxDPut), obj.Lot_Mult(IdxDPut), obj.Curr_Mult(IdxDPut), ...
                        obj.IsExpiryProcessed(IdxDPut),obj.YearToDay_Mult(IdxDPut),obj.Currency(IdxDPut));
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018
%                   obj.ObjDownPut2 =  CallPutSpread(obj.AssetPrice(IdxDPut),(B(IdxDPut)+ obj.TickSize(IdxDPut)),...
%                         B(IdxDPut), obj.RFR(IdxDPut), ...
%                         obj.CostOfCarry(IdxDPut),BVol(IdxDPut),BVol(IdxDPut), obj.TTM(IdxDPut), OptionType(IdxDPut),...
%                         obj.Lots(IdxDPut), obj.Lot_Mult(IdxDPut), obj.Curr_Mult(IdxDPut), ...
%                         obj.IsExpiryProcessed(IdxDPut),obj.YearToDay_Mult(IdxDPut),obj.Currency(IdxDPut));
                    obj.ObjDownPut2 =  CallPutSpread(obj.AssetPrice(IdxDPut),(B(IdxDPut)+ obj.TickSize(IdxDPut)),...
                        B(IdxDPut), obj.RFR(IdxDPut), ...
                        obj.CostOfCarry(IdxDPut),OutVol(IdxDPut),BVol(IdxDPut), obj.TTM(IdxDPut), OptionType(IdxDPut),...
                        obj.Lots(IdxDPut), obj.Lot_Mult(IdxDPut), obj.Curr_Mult(IdxDPut), ...
                        obj.IsExpiryProcessed(IdxDPut),obj.YearToDay_Mult(IdxDPut),obj.Currency(IdxDPut));
                     % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018

                end
                
                % Euro_Down_In_Put
                if any(IdxDIPut)
                    OptionType(IdxDIPut) = {'vanilla_put'};
                    obj.ObjDownVanPut = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxDIPut),...
                        K(IdxDIPut), obj.RFR(IdxDIPut), obj.CostOfCarry(IdxDIPut), ...
                        KVol(IdxDIPut), obj.TTM(IdxDIPut), OptionType(IdxDIPut), ...
                        obj.Lots(IdxDIPut), obj.Lot_Mult(IdxDIPut), obj.Curr_Mult(IdxDIPut), ...
                        obj.IsExpiryProcessed(IdxDIPut),obj.YearToDay_Mult(IdxDIPut),obj.Currency(IdxDIPut));
                end
                
                % Euro_Up_Out_Call
                IdxUICall = strcmpi('euro_up_in_call',OptionType);
                IdxUOCall = strcmpi('euro_up_out_call',OptionType);
                IdxUCall = (IdxUICall | IdxUOCall) & (B > K);
                if any(IdxUCall)
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018                    
                    OutVol    = zeros(size(IdxUCall));                   
                    TmpInVol  = InVol1ID(IdxUCall);
                    UniqVolID = unique(TmpInVol);
                    for iVol = 1:length(UniqVolID)
                        IdxInVolID    = IdxUCall & (strcmpi(UniqVolID{iVol},InVol1ID));
                        IdxVolCurveID = strcmpi(UniqVolID{iVol},VolCurve.vol_id) & strcmpi('C',VolCurve.call_put_id);
                        XStrike       = VolCurve.strike(IdxVolCurveID);
                        YVol          = VolCurve.settle_vol(IdxVolCurveID);                  
                        OutStrike     = (B(IdxInVolID)- obj.TickSize(IdxInVolID));
                        try
                            OutVol(IdxInVolID) = interp1(XStrike, YVol,OutStrike,'linear',NaN);
                        catch
                            OutVol(IdxInVolID) = 0; % if the Vol ID is expired, then XStrike and YVol will be empty; assigning OutVol = 0 in that scenario
                        end
                    end
                    % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                    
                    OptionType(IdxUCall) = {'callspread'};
                    obj.ObjUpCall1 = CallPutSpread(obj.AssetPrice(IdxUCall), B(IdxUCall), K(IdxUCall), obj.RFR(IdxUCall), ...
                        obj.CostOfCarry(IdxUCall),BVol(IdxUCall),KVol(IdxUCall), obj.TTM(IdxUCall), OptionType(IdxUCall),...
                        obj.Lots(IdxUCall), obj.Lot_Mult(IdxUCall), obj.Curr_Mult(IdxUCall), ...
                        obj.IsExpiryProcessed(IdxUCall),obj.YearToDay_Mult(IdxUCall),obj.Currency(IdxUCall));
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018
%                   obj.ObjUpCall2 =  CallPutSpread(obj.AssetPrice(IdxUCall),B(IdxUCall),(B(IdxUCall)- obj.TickSize(IdxUCall)),...
%                         obj.RFR(IdxUCall),obj.CostOfCarry(IdxUCall),BVol(IdxUCall),BVol(IdxUCall), obj.TTM(IdxUCall), ...
%                         OptionType(IdxUCall),obj.Lots(IdxUCall), obj.Lot_Mult(IdxUCall), obj.Curr_Mult(IdxUCall), ...
%                         obj.IsExpiryProcessed(IdxUCall),obj.YearToDay_Mult(IdxUCall),obj.Currency(IdxUCall));
                    obj.ObjUpCall2 =  CallPutSpread(obj.AssetPrice(IdxUCall),B(IdxUCall),(B(IdxUCall)- obj.TickSize(IdxUCall)),...
                        obj.RFR(IdxUCall),obj.CostOfCarry(IdxUCall),BVol(IdxUCall),OutVol(IdxUCall), obj.TTM(IdxUCall), ...
                        OptionType(IdxUCall),obj.Lots(IdxUCall), obj.Lot_Mult(IdxUCall), obj.Curr_Mult(IdxUCall), ...
                        obj.IsExpiryProcessed(IdxUCall),obj.YearToDay_Mult(IdxUCall),obj.Currency(IdxUCall));
                     % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                end
                
                % Euro_Up_In_call
                OptionType(IdxUICall) = {'vanilla_call'};
                if any(IdxUICall)
                    obj.ObjUpVanCall = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxUICall),...
                        K(IdxUICall), obj.RFR(IdxUICall), obj.CostOfCarry(IdxUICall), ...
                        KVol(IdxUICall), obj.TTM(IdxUICall), OptionType(IdxUICall), ...
                        obj.Lots(IdxUICall), obj.Lot_Mult(IdxUICall), obj.Curr_Mult(IdxUICall), ...
                        obj.IsExpiryProcessed(IdxUICall),obj.YearToDay_Mult(IdxUICall),obj.Currency(IdxUICall));
                end
                
                % Euro_Up_In_put
                IdxUIPut = strcmpi('euro_up_in_put',OptionType);
                IdxUOPut = strcmpi('euro_up_out_put',OptionType);
                IdxUPut = (IdxUIPut | IdxUOPut) & (B < K);
                if any(IdxUPut)
                  % FIX FOR DIG OPT PRICING ERROR: 11-06-2018                    
                    OutVol    = zeros(size(IdxUPut));                   
                    TmpInVol  = InVol1ID(IdxUPut);
                    UniqVolID = unique(TmpInVol);
                    for iVol = 1:length(UniqVolID)
                        IdxInVolID    = IdxUPut & (strcmpi(UniqVolID{iVol},InVol1ID));
                        IdxVolCurveID = strcmpi(UniqVolID{iVol},VolCurve.vol_id) & strcmpi('P',VolCurve.call_put_id);
                        XStrike       = VolCurve.strike(IdxVolCurveID);
                        YVol          = VolCurve.settle_vol(IdxVolCurveID);                  
                        OutStrike     = (B(IdxInVolID)- obj.TickSize(IdxInVolID));
                        try
                            OutVol(IdxInVolID)        = interp1(XStrike, YVol,OutStrike,'linear',NaN);
                        catch
                            OutVol(IdxInVolID) = 0; % if the Vol ID is expired, then XStrike and YVol will be empty; assigning OutVol = 0 in that scenario
                        end
                    end                 
                    % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                    
                    OptionType(IdxUPut) = {'putspread'};
                    obj.ObjUpPut1 = CallPutSpread(obj.AssetPrice(IdxUPut), K(IdxUPut), B(IdxUPut), obj.RFR(IdxUPut), ...
                        obj.CostOfCarry(IdxUPut),KVol(IdxUPut),BVol(IdxUPut), obj.TTM(IdxUPut), OptionType(IdxUPut),...
                        obj.Lots(IdxUPut), obj.Lot_Mult(IdxUPut), obj.Curr_Mult(IdxUPut), ...
                        obj.IsExpiryProcessed(IdxUPut),obj.YearToDay_Mult(IdxUPut),obj.Currency(IdxUPut));
                    % FIX FOR DIG OPT PRICING ERROR: 11-06-2018
%                   obj.ObjUpPut2 =  CallPutSpread(obj.AssetPrice(IdxUPut),B(IdxUPut),(B(IdxUPut)- obj.TickSize(IdxUPut)),...
%                         obj.RFR(IdxUPut), ...
%                         obj.CostOfCarry(IdxUPut),BVol(IdxUPut),BVol(IdxUPut), obj.TTM(IdxUPut), OptionType(IdxUPut),...
%                         obj.Lots(IdxUPut), obj.Lot_Mult(IdxUPut), obj.Curr_Mult(IdxUPut), ...
%                         obj.IsExpiryProcessed(IdxUPut),obj.YearToDay_Mult(IdxUPut),obj.Currency(IdxUPut));
                    obj.ObjUpPut2 =  CallPutSpread(obj.AssetPrice(IdxUPut),B(IdxUPut),(B(IdxUPut)- obj.TickSize(IdxUPut)),...
                        obj.RFR(IdxUPut), ...
                        obj.CostOfCarry(IdxUPut),BVol(IdxUPut),OutVol(IdxUPut), obj.TTM(IdxUPut), OptionType(IdxUPut),...
                        obj.Lots(IdxUPut), obj.Lot_Mult(IdxUPut), obj.Curr_Mult(IdxUPut), ...
                        obj.IsExpiryProcessed(IdxUPut),obj.YearToDay_Mult(IdxUPut),obj.Currency(IdxUPut));
                    % END OF FIX FOR DIG OPT PRICING ERROR: 11-06-2018
                end
                
                % Euro_Up_Out_Put
                if any(IdxUOPut)
                    OptionType(IdxUOPut) = {'vanilla_put'};
                    obj.ObjUpVanPut = VanillaOptionBlackScholesModel(obj.AssetPrice(IdxUOPut),...
                        K(IdxUOPut), obj.RFR(IdxUOPut), obj.CostOfCarry(IdxUOPut), ...
                        KVol(IdxUOPut), obj.TTM(IdxUOPut), OptionType(IdxUOPut), ...
                        obj.Lots(IdxUOPut), obj.Lot_Mult(IdxUOPut), obj.Curr_Mult(IdxUOPut), ...
                        obj.IsExpiryProcessed(IdxUOPut),obj.YearToDay_Mult(IdxUOPut),obj.Currency(IdxUOPut));
                end
                obj.IdxDICall = IdxDICall;
                obj.IdxDOCall = IdxDOCall;
                obj.IdxDCall  = IdxDCall;
                obj.IdxDIPut  = IdxDIPut;
                obj.IdxDOPut  = IdxDOPut;
                obj.IdxDPut   = IdxDPut; %#ok<*PROP>
                obj.IdxUICall = IdxUICall;
                obj.IdxUOCall = IdxUOCall;
                obj.IdxUCall  = IdxUCall;
                obj.IdxUIPut  = IdxUIPut;
                obj.IdxUOPut  = IdxUOPut;
                obj.IdxUPut   = IdxUPut;
                obj.CalcLots  = CalcLots;
            end
        end
        
        function Price = getprice(obj)
            Price = zeros(size(obj.OptType));
            
            IdxDOCall = obj.IdxDOCall;
            IdxDCall = obj.IdxDCall;
            IdxDIPut = obj.IdxDIPut;
            IdxDPut  = obj.IdxDPut;
            IdxUICall = obj.IdxUICall;
            IdxUCall = obj.IdxUCall;
            IdxUOPut = obj.IdxUOPut;
            IdxUPut  = obj.IdxUPut;
            CalcLots = obj.CalcLots;
            
            if any(IdxDCall)
                Price(IdxDCall) = obj.ObjDownCall1.getprice() - (CalcLots(IdxDCall) .* obj.ObjDownCall2.getprice());
            end
            
            if any(IdxDOCall)
                Price(IdxDOCall) = obj.ObjDownVanCall.getprice() - Price(IdxDOCall);
            end
            
            
            if any(IdxDPut)
                Price(IdxDPut) = obj.ObjDownPut1.getprice() - (CalcLots(IdxDPut) .* obj.ObjDownPut2.getprice());
            end
            
            if any(IdxDIPut)
                Price(IdxDIPut) = obj.ObjDownVanPut.getprice() - Price(IdxDIPut);
            end
            
            if any(IdxUCall)
                Price(IdxUCall) = obj.ObjUpCall1.getprice() - (CalcLots(IdxUCall) .* obj.ObjUpCall2.getprice());
            end
            
            if any(IdxUICall)
                Price(IdxUICall) = obj.ObjUpVanCall.getprice() - Price(IdxUICall);
            end
            
            if any(IdxUPut)
                Price(IdxUPut) = obj.ObjUpPut1.getprice() - (CalcLots(IdxUPut) .* obj.ObjUpPut2.getprice());
            end
            
            if any(IdxUOPut)
                Price(IdxUOPut) = obj.ObjUpVanPut.getprice() - Price(IdxUOPut);
            end
            Price(Price<0) = 0;
        end
        function Delta = getdelta1(obj)
            Delta = zeros(size(obj.OptType));
            
            IdxDOCall = obj.IdxDOCall;
            IdxDCall = obj.IdxDCall;
            IdxDIPut = obj.IdxDIPut;
            IdxDPut  = obj.IdxDPut;
            IdxUICall = obj.IdxUICall;
            IdxUCall = obj.IdxUCall;
            IdxUOPut = obj.IdxUOPut;
            IdxUPut  = obj.IdxUPut;
            CalcLots = obj.CalcLots;
            
            if any(IdxDCall)
                Delta(IdxDCall) = obj.ObjDownCall1.getdelta1() - (CalcLots(IdxDCall) .* obj.ObjDownCall2.getdelta1());
            end
            
            if any(IdxDOCall)
                Delta(IdxDOCall) = obj.ObjDownVanCall.getdelta1() - Delta(IdxDOCall);
            end
            
            
            if any(IdxDPut)
                Delta(IdxDPut) = obj.ObjDownPut1.getdelta1() - (CalcLots(IdxDPut) .* obj.ObjDownPut2.getdelta1());
            end
            
            if any(IdxDIPut)
                Delta(IdxDIPut) = obj.ObjDownVanPut.getdelta1() - Delta(IdxDIPut);
            end
            
            if any(IdxUCall)
                Delta(IdxUCall) = obj.ObjUpCall1.getdelta1() - (CalcLots(IdxUCall) .* obj.ObjUpCall2.getdelta1());
            end
            
            if any(IdxUICall)
                Delta(IdxUICall) = obj.ObjUpVanCall.getdelta1() - Delta(IdxUICall);
            end
            
            if any(IdxUPut)
                Delta(IdxUPut) = obj.ObjUpPut1.getdelta1() - (CalcLots(IdxUPut) .* obj.ObjUpPut2.getdelta1());
            end
            
            if any(IdxUOPut)
                Delta(IdxUOPut) = obj.ObjUpVanPut.getdelta1() - Delta(IdxUOPut);
            end
        end
        function Gamma11 = getgamma11(obj)
            Gamma11 = zeros(size(obj.OptType));
            
            IdxDOCall = obj.IdxDOCall;
            IdxDCall = obj.IdxDCall;
            IdxDIPut = obj.IdxDIPut;
            IdxDPut  = obj.IdxDPut;
            IdxUICall = obj.IdxUICall;
            IdxUCall = obj.IdxUCall;
            IdxUOPut = obj.IdxUOPut;
            IdxUPut  = obj.IdxUPut;
            CalcLots = obj.CalcLots;
            
            if any(IdxDCall)
                Gamma11(IdxDCall) = obj.ObjDownCall1.getgamma11() - (CalcLots(IdxDCall) .* obj.ObjDownCall2.getgamma11());
            end
            if any(IdxDOCall)
                Gamma11(IdxDOCall) = obj.ObjDownVanCall.getgamma11() - Gamma11(IdxDOCall);
            end
            if any(IdxDPut)
                Gamma11(IdxDPut) = obj.ObjDownPut1.getgamma11() - (CalcLots(IdxDPut) .* obj.ObjDownPut2.getgamma11());
            end
            if any(IdxDIPut)
                Gamma11(IdxDIPut) = obj.ObjDownVanPut.getgamma11() - Gamma11(IdxDIPut);
            end
            if any(IdxUCall)
                Gamma11(IdxUCall) = obj.ObjUpCall1.getgamma11() - (CalcLots(IdxUCall) .* obj.ObjUpCall2.getgamma11());
            end
            if any(IdxUICall)
                Gamma11(IdxUICall) = obj.ObjUpVanCall.getgamma11() - Gamma11(IdxUICall);
            end
            if any(IdxUPut)
                Gamma11(IdxUPut) = obj.ObjUpPut1.getgamma11() - (CalcLots(IdxUPut) .* obj.ObjUpPut2.getgamma11());
            end
            if any(IdxUOPut)
                Gamma11(IdxUOPut) = obj.ObjUpVanPut.getgamma11() - Gamma11(IdxUOPut);
            end
        end
        
        function Vega1 = getvega1(obj)
            Vega1 = zeros(size(obj.OptType));
            
            IdxDOCall = obj.IdxDOCall;
            IdxDCall = obj.IdxDCall;
            IdxDIPut = obj.IdxDIPut;
            IdxDPut  = obj.IdxDPut;
            IdxUICall = obj.IdxUICall;
            IdxUCall = obj.IdxUCall;
            IdxUOPut = obj.IdxUOPut;
            IdxUPut  = obj.IdxUPut;
            CalcLots = obj.CalcLots;
            
            if any(IdxDCall)
                Vega1(IdxDCall) = obj.ObjDownCall1.getvega1() - (CalcLots(IdxDCall) .* obj.ObjDownCall2.getvega1());
            end
            if any(IdxDOCall)
                Vega1(IdxDOCall) = obj.ObjDownVanCall.getvega1() - Vega1(IdxDOCall);
            end
            if any(IdxDPut)
                Vega1(IdxDPut) = obj.ObjDownPut1.getvega1() - (CalcLots(IdxDPut) .* obj.ObjDownPut2.getvega1());
            end
            if any(IdxDIPut)
                Vega1(IdxDIPut) = obj.ObjDownVanPut.getvega1() - Vega1(IdxDIPut);
            end
            if any(IdxUCall)
                Vega1(IdxUCall) = obj.ObjUpCall1.getvega1() - (CalcLots(IdxUCall) .* obj.ObjUpCall2.getvega1());
            end
            if any(IdxUICall)
                Vega1(IdxUICall) = obj.ObjUpVanCall.getvega1() - Vega1(IdxUICall);
            end
            if any(IdxUPut)
                Vega1(IdxUPut) = obj.ObjUpPut1.getvega1() - (CalcLots(IdxUPut) .* obj.ObjUpPut2.getvega1());
            end
            if any(IdxUOPut)
                Vega1(IdxUOPut) = obj.ObjUpVanPut.getvega1() - Vega1(IdxUOPut);
            end
        end
        function Theta = gettheta(obj)
            Theta = zeros(size(obj.OptType));
            
            IdxDOCall = obj.IdxDOCall;
            IdxDCall = obj.IdxDCall;
            IdxDIPut = obj.IdxDIPut;
            IdxDPut  = obj.IdxDPut;
            IdxUICall = obj.IdxUICall;
            IdxUCall = obj.IdxUCall;
            IdxUOPut = obj.IdxUOPut;
            IdxUPut  = obj.IdxUPut;
            CalcLots = obj.CalcLots;
            
            if any(IdxDCall)
                Theta(IdxDCall) = obj.ObjDownCall1.gettheta() - (CalcLots(IdxDCall) .* obj.ObjDownCall2.gettheta());
            end
            if any(IdxDOCall)
                Theta(IdxDOCall) = obj.ObjDownVanCall.gettheta() - Theta(IdxDOCall);
            end
            if any(IdxDPut)
                Theta(IdxDPut) = obj.ObjDownPut1.gettheta() - (CalcLots(IdxDPut) .* obj.ObjDownPut2.gettheta());
            end
            if any(IdxDIPut)
                Theta(IdxDIPut) = obj.ObjDownVanPut.gettheta() - Theta(IdxDIPut);
            end
            if any(IdxUCall)
                Theta(IdxUCall) = obj.ObjUpCall1.gettheta() - (CalcLots(IdxUCall) .* obj.ObjUpCall2.gettheta());
            end
            if any(IdxUICall)
                Theta(IdxUICall) = obj.ObjUpVanCall.gettheta() - Theta(IdxUICall);
            end
            if any(IdxUPut)
                Theta(IdxUPut) = obj.ObjUpPut1.gettheta() - (CalcLots(IdxUPut) .* obj.ObjUpPut2.gettheta());
            end
            if any(IdxUOPut)
                Theta(IdxUOPut) = obj.ObjUpVanPut.gettheta() - Theta(IdxUOPut);
            end
        end
    end
end