%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/06/02 11:03:04 $
%  $Revision: 1.2 $

classdef VanillaOptionBlackScholesModel < FinancialInstruments
    properties(GetAccess = private, SetAccess = private)
        Spot
        Strike
        RFR
        OptType
        CostOfCarry
        Volatility
        TTM
        IsExpiryProc
        ExchATMConv
        Lots
        Curr_Mult
        Lot_Mult
        YearToDay_Mult
        Currency
    end
    
    methods (Access = public)
        function obj = VanillaOptionBlackScholesModel(AssetPrice, Strike, RiskFreeRate, CostOfCarry, Volatility, TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, IsExpiryProcessed,YearToDay_Mult,Currency)    

            if ischar(OptionType)
                OptionType = cellstr(OptionType);
            end
%             if ~all(ismember(lower(OptionType),{'vanilla_call','vanilla_put'}))
%                 error('Invalid Option Type. Value should be either vanilla_call/vanilla_put');
%             end
            
            NumInputs = size(OptionType);
           
            
            validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'nonnegative',    'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'AssetPrice');
            validateattributes(Strike,                {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Strike');
            validateattributes(RiskFreeRate,          {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'RiskFreeRate');
            validateattributes(CostOfCarry,           {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'CostOfCarry');            
            validateattributes(Volatility,            {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Volatility');            
            validateattributes(TimeToMaturity,        {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'TimeToMaturity');
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Lots');
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Lot_Mult');            
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Curr_Mult');
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'real',        'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'IsExpiryProcessed');
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative', 'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'YearToDay_Mult');
            validateattributes(Currency,              {'cell','char'},    {'nonempty',                'column', 'size', NumInputs}, 'VanillaOptionBlackScholesModel', 'Currency');

            TimeToMaturity(TimeToMaturity<0) = 0.0;
            
            obj.Spot = AssetPrice;
            obj.Strike = Strike;
            obj.RFR = RiskFreeRate;
            obj.CostOfCarry = CostOfCarry;
            obj.TTM = TimeToMaturity;
            obj.OptType = OptionType;
            obj.Volatility = Volatility ;
            obj.IsExpiryProc = IsExpiryProcessed;
            obj.ExchATMConv  = 1;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.YearToDay_Mult = YearToDay_Mult;
            obj.Currency = Currency;
            
        end
        
        function Price = getprice(obj)
            OptionType = obj.OptType;
            S = obj.Spot;
            X = obj.Strike;
            r = obj.RFR;
            b = obj.CostOfCarry;
            v = obj.Volatility;
            T = obj.TTM;
            d1 = (log(S./X) + (b+0.5*v.*v).*T)./(v.*sqrt(T));
            bExpiryProcessed = obj.IsExpiryProc;
            
            d1(T==0 & log(S./X) >= 0.0) = Inf;
            d1(T==0 & log(S./X) < 0.0) = -Inf;
            
            d1(v==0 & (log(S./X) + (b.*T)) >= 0.0) = Inf;
            d1(v==0 & (log(S./X) + (b.*T)) <  0.0) = -Inf;
           
            d2 = d1 - v.*sqrt(T);
            
            Price = zeros(size(OptionType));
            Idx = strcmpi(OptionType,'vanilla_call') & ~bExpiryProcessed;
            if any(Idx)
                Price(Idx) = S(Idx).*exp((b(Idx) - r(Idx)).*T(Idx)).*normcdf(d1(Idx)) - X(Idx).*exp(-r(Idx).*T(Idx)).*normcdf(d2(Idx));
            end
            Idx = strcmpi(OptionType,'vanilla_put') & ~bExpiryProcessed;
            if any(Idx)                
                Price(Idx) = X(Idx).*exp(-r(Idx).*T(Idx)).*normcdf(-d2(Idx)) -  S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*normcdf(-d1(Idx));
            end
        end
        
        function Delta1 = getdelta1(obj)
            OptionType = obj.OptType;
            S = obj.Spot;
            X = obj.Strike;
            r = obj.RFR;
            b = obj.CostOfCarry;
            v = obj.Volatility;
            T = obj.TTM;
            L = obj.Lots;
            bExpiryProcessed = obj.IsExpiryProc;
            ATMConv = obj.ExchATMConv;
            
            d1 = (log(S./X) + (b+0.5*v.*v).*T)./(v.*sqrt(T));
            
            d1(v==0 & (log(S./X) + (b.*T)) >= 0.0) = Inf;
            d1(v==0 & (log(S./X) + (b.*T)) <  0.0) = -Inf;
            
            Delta1 = zeros(size(OptionType));
            
            Delta1(bExpiryProcessed==1.0) = 0.0;
            
            Idx = (strcmpi(OptionType,'vanilla_call') & ~bExpiryProcessed & T==0 & S-X>0) | (strcmpi(OptionType,'vanilla_call') & ~bExpiryProcessed & T==0 & S-X==0 & ATMConv);
            Delta1(Idx) = 1;
            Idx = (strcmpi(OptionType,'vanilla_call') & ~bExpiryProcessed & T>0);
            if any(Idx)                
                Delta1(Idx) = exp((b(Idx)-r(Idx)).*T(Idx)).*normcdf(d1(Idx));
            end
            
            Idx = (strcmpi(OptionType,'vanilla_put') & ~bExpiryProcessed & T==0 & X-S>0) | (strcmpi(OptionType,'vanilla_put') & ~bExpiryProcessed & T==0 & X-S==0 & ATMConv);
            Delta1(Idx) = -1;
            Idx = (strcmpi(OptionType,'vanilla_put') & ~bExpiryProcessed & T>0);
            if any(Idx)                
                Delta1(Idx) = exp((b(Idx)-r(Idx)).*T(Idx)).*(normcdf(d1(Idx))-1);
            end            
            Delta1 = L.*Delta1;
        end
        
        function curr_exp = getcurrencyexposure(obj)
            OptionType = obj.OptType;
            Curr = obj.Currency;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            
            curr_exp = zeros(size(OptionType));
            Idx = ~(strcmp(Curr,'USD') | strcmp(Curr,'USd'));
            Price = obj.getprice();
            curr_exp(Idx) = Price(Idx).*L(Idx).*LM(Idx);
        end
        
        function Gamma11 = getgamma11(obj)
            OptionType = obj.OptType;
            S = obj.Spot;
            X = obj.Strike;
            r = obj.RFR;
            b = obj.CostOfCarry;
            v = obj.Volatility;
            T = obj.TTM;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            
            bExpiryProcessed = obj.IsExpiryProc;
            
%           d1 = (log(S./X) + (b+0.5*v.*v).*T)./(v.*sqrt(T));
            
            Gamma11 = zeros(size(OptionType));            
            
            Idx = ~bExpiryProcessed & T>0 & v>0.0;
            d1 = (log(S(Idx)./X(Idx)) + (b(Idx)+0.5*v(Idx).*v(Idx)).*T(Idx))./(v(Idx).*sqrt(T(Idx)));

            Gamma11(Idx) = (normpdf(d1).*exp((b(Idx)-r(Idx)).*T(Idx)))./(S(Idx).*v(Idx).*sqrt(T(Idx)));
            
            Gamma11 = Gamma11.*L.*LM./CM;
        end
        
        function Vega1 = getvega1(obj)
            OptionType = obj.OptType;
            S = obj.Spot;
            X = obj.Strike;
            r = obj.RFR;
            b = obj.CostOfCarry;
            v = obj.Volatility;
            T = obj.TTM;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            bExpiryProcessed = obj.IsExpiryProc;
            
            d1 = (log(S./X) + (b+0.5*v.*v).*T)./(v.*sqrt(T));
            
            d1(v==0 & (log(S./X) + (b.*T)) >= 0.0) = Inf;
            d1(v==0 & (log(S./X) + (b.*T)) <  0.0) = -Inf;            
            
            Vega1 = zeros(size(OptionType));

            Idx = ~bExpiryProcessed & T>0 & v>0.0;
            d1(Idx) = (log(S(Idx)./X(Idx)) + (b(Idx)+0.5*v(Idx).*v(Idx)).*T(Idx))./(v(Idx).*sqrt(T(Idx)));
            Vega1(Idx) = S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*normpdf(d1(Idx)).*sqrt(T(Idx));
            
            Vega1 = Vega1.*L.*LM.*CM/100;
        end
        
        function Theta = gettheta(obj) % Pure bleed from Haug
            OptionType = obj.OptType;
            S = obj.Spot;
            X = obj.Strike;
            b = obj.CostOfCarry;
            v = obj.Volatility;
            T = obj.TTM;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            YTDM = obj.YearToDay_Mult;
            bExpiryProcessed = obj.IsExpiryProc;
            
            d1 = (log(S./X) + (b+0.5*v.*v).*T)./(v.*sqrt(T));
            
            d1(v==0 & (log(S./X) + (b.*T)) >= 0.0) = Inf;
            d1(v==0 & (log(S./X) + (b.*T)) <  0.0) = -Inf;            
            
            Theta = zeros(size(OptionType));
            Idx = ~bExpiryProcessed & T>0 & v>0.0;
            d1(Idx) = (log(S(Idx)./X(Idx)) + (b(Idx)+0.5*v(Idx).*v(Idx)).*T(Idx))./(v(Idx).*sqrt(T(Idx)));
            Theta(Idx) = -S(Idx).*normpdf(d1(Idx)).*v(Idx)./(2*sqrt(T(Idx))); 
            
            Theta = Theta.*L.*LM.*CM./YTDM;
        end
        
        function BE1 = getbe1(obj)
            IsExpiryP = obj.IsExpiryProc;
            CM = obj.Curr_Mult;
            T = obj.TTM;
            
            BE1 = zeros(size(T));
            
            IdxTemp = ~(IsExpiryP==1 | T==0);
            Theta = obj.gettheta();
            Gamma = obj.getgamma11();
            BE1(IdxTemp) = sqrt(((-2.*Theta(IdxTemp)./CM(IdxTemp))./Gamma(IdxTemp)));
            IdxTemp = ~isreal(BE1);
            BE1(IdxTemp) = NaN;
        end      

    end
end