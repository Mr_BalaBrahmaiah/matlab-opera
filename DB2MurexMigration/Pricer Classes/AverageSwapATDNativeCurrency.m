classdef AverageSwapATDNativeCurrency < FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/04/15 09:18:25 $
%  $Revision: 1.2 $


    %long -> float receiver
    properties(GetAccess = private, SetAccess = private)
        AssetPrice
        AvgTillDate        
        Lots
        OptionType       
        TND % Number of Average days
        NDE % Number of elapsed days
    end
    
    methods (Access = public)
        function obj = AverageSwapATDNativeCurrency(AssetPrice, OptionType,AvgTillDate,TND,NDE,Lots)
            
            validateattributes(OptionType, {'char','cell'},{'nonempty'},'AverageSwapNativeCurrency','OptionType');
            
            if ~all(ismember(lower(OptionType),{'avg_swap_nc'}))
                error('Invalid Option Type. Value should be avg_swap_nc');
            else
                if ischar(OptionType)
                    obj.OptionType = cellstr(lower(OptionType));
                else
                    obj.OptionType = lower(OptionType);
                end
            end            
            NumInputs = size(obj.OptionType);
            
            validateattributes(AssetPrice,        {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapNativeCurrency', 'AssetPrice');                        
            validateattributes(AvgTillDate,       {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapNativeCurrency', 'AvgTillDate');
            validateattributes(TND,               {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapNativeCurrency', 'TND');
            validateattributes(NDE,               {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'AverageSwapNativeCurrency', 'NDE');
            validateattributes(Lots,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'AverageSwapNativeCurrency', 'Lots');
                                               
            obj.AssetPrice     = AssetPrice;
            obj.AvgTillDate    = AvgTillDate;            
            obj.Lots           = Lots;            
            obj.TND            = TND;
            obj.NDE            = NDE;
        end
        
        function Price = getprice(obj)           
         
%             Price = (((obj.NDE .* obj.AvgTillDate) + (obj.TND - obj.NDE) .*  obj.AssetPrice) ./  obj.TND);
              Price = ((obj.NDE .* obj.AvgTillDate) + ((obj.TND - obj.NDE) .*  obj.AssetPrice)) ./  obj.TND;

        end
        
        function Delta = getdelta1(obj)
                       
            Delta = (((obj.TND - obj.NDE) ./ obj.TND) .* obj.Lots);
           
        end
        
        function Gamma = getgamma11(obj)
            Gamma = ' ';        
        end        
        
        function Vega = getvega1(obj)
            Vega = ' ';           
           
        end
        
        function Theta = gettheta(obj)          
           Theta = ' ';
        end
           
        
    end
   
end