classdef EuropeanOptionAmericanBarrier < FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/06/20 04:38:49 $
%  $Revision: 1.1 $

    properties (GetAccess = private, SetAccess = private)
        Spot
        Strike
        BarLevel  
        Rebate 
        IsBarBreached
        RFR 
        CostOfCarry 
        Volatility 
        OptType
        TTM
        Lots
        Lot_Mult
        Curr_Mult
        YearToDay_Mult
        ExchangeATMConvention
        IsExpiryProcessed     
        Currency
        TickSize
    end       
    
    methods (Access = public)
        function obj = EuropeanOptionAmericanBarrier(AssetPrice, Strike, BarrierLevel, Rebate, IsBarrierBreached, RFR, CostOfCarry, Volatility, ...
                TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize)
            
            
            ValidOptionTypes = {'Amer_up_in_call','Amer_up_in_put','Amer_up_out_call','Amer_up_out_put','Amer_down_in_call','Amer_down_in_put','Amer_down_out_call','Amer_down_out_put'};

            if ischar(OptionType)
                OptionType = cellstr(OptionType);
            end
            if any(~ismember(lower(OptionType), lower(ValidOptionTypes)))
                error('Invalid barrier option types');
            end
            
            NumInputs = size(OptionType);
            
            if isscalar(IsExpiryProcessed)
                IsExpiryProcessed = ones(NumInputs)*IsExpiryProcessed;
            end
%             if isscalar(ExchangeATMConvention)
%                 ExchangeATMConvention = ones(NumInputs)*ExchangeATMConvention;
%             end            
            
            validateattributes(AssetPrice,            {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'AssetPrice');
            validateattributes(Strike,                {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Strike');
            validateattributes(BarrierLevel,          {'numeric'}, {'nonempty', 'positive',    'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'BarierLevel');
            validateattributes(Rebate,                {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Rebate');
            validateattributes(IsBarrierBreached,     {'numeric'}, {'nonempty', 'binary',      'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'IsBarrierBreached');
            validateattributes(RFR,                   {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'RiskFreeRate');
            validateattributes(CostOfCarry,           {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'CostOfCarry');
            validateattributes(Volatility,            {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Volatility');
            validateattributes(TimeToMaturity,        {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'TimeToMaturity');  
            validateattributes(Lots,                  {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Lots'); 
            validateattributes(Lot_Mult,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Lot_Mult'); 
            validateattributes(Curr_Mult,             {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Curr_Mult'); 
            validateattributes(YearToDay_Mult,        {'numeric'}, {'nonempty', 'nonnegative', 'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'YearToDay_Mult'); 
            validateattributes(ExchangeATMConvention, {'cell','char'},    {'nonempty',         'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'ExchangeATMConvention'); 
            validateattributes(IsExpiryProcessed,     {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'IsExpiryProcessed'); 
            validateattributes(Currency,              {'cell','char'},    {'nonempty',         'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'Currency');
            validateattributes(TickSize,              {'numeric'}, {'nonempty', 'real',        'column','size', NumInputs}, 'EuropeanOptionAmericanBarrier', 'TickSize');
            
            TimeToMaturity(TimeToMaturity<0) = 0.0;
            
            obj.Spot = AssetPrice;
            obj.Strike = Strike;
            obj.BarLevel = BarrierLevel;  
            obj.Rebate = Rebate;
            obj.IsBarBreached = IsBarrierBreached;
            obj.RFR = RFR;
            obj.CostOfCarry = CostOfCarry;
            obj.Volatility  = Volatility;
            obj.OptType = OptionType;
            obj.TTM = TimeToMaturity;
            obj.Lots = Lots;
            obj.Lot_Mult = Lot_Mult;
            obj.Curr_Mult = Curr_Mult;
            obj.YearToDay_Mult = YearToDay_Mult;
            obj.ExchangeATMConvention = ExchangeATMConvention;
            obj.IsExpiryProcessed = IsExpiryProcessed;
            obj.Currency = Currency;
            obj.TickSize = TickSize;
        end
        
        function Price = getprice(obj)
            OptionType = obj.OptType;
            IsBarrierBreached = obj.IsBarBreached;
            S = obj.Spot;   
            b = obj.CostOfCarry;  
            r = obj.RFR;  
            X = obj.Strike;  
            H = obj.BarLevel;  
            Vol = obj.Volatility;
            K  = obj.Rebate;
            T = obj.TTM;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            YTDC = obj.YearToDay_Mult;
            EATMC = obj.ExchangeATMConvention;
            IsExpiryP = obj.IsExpiryProcessed;
            Curr = obj.Currency;
            
            phi    = NaN;
            eta    = NaN;
            mu     = (b-0.5.*Vol.^2)./Vol.^2;
            lambda = sqrt(mu.*mu + 2.*r./Vol.^2);

            x1 = log(S./X)./(Vol.*sqrt(T)) + (1+mu).*Vol.*sqrt(T);
            x2 = log(S./H)./(Vol.*sqrt(T)) + (1+mu).*Vol.*sqrt(T);
            y1 = log(H.*H./(S.*X))./(Vol.*sqrt(T)) + (1+mu).*Vol.*sqrt(T);
            y2 = log(H./S)./(Vol.*sqrt(T)) + (1+mu).*Vol.*sqrt(T);
            z  = log(H./S)./(Vol.*sqrt(T)) + lambda.*Vol.*sqrt(T);
            
            Price = zeros(size(OptionType));
            
            IdxExpProc = IsExpiryP==1;
            
            if any(IdxExpProc)
                Price(IdxExpProc) = 0.0;
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_up_in_call');
            if any(Idx)
                eta = -1;
                phi = 1;
                     
                IdxTemp = Idx & (IsBarrierBreached == 1 | S>=H);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)+E(IdxTemp);
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end    
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = B(IdxTemp)-C(IdxTemp)+D(IdxTemp)+E(IdxTemp);
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end                     
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_up_in_put');
            if any(Idx)
                eta = -1;
                phi = -1;
                
                IdxTemp = Idx & (IsBarrierBreached == 1 | S>=H);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-B(IdxTemp)+D(IdxTemp)+E(IdxTemp);                    
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end                   
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = C(IdxTemp)+E(IdxTemp);                    
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;                    
                end                
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_up_out_call');
            if any(Idx)
                eta = -1;
                phi = 1;
                
                IdxTemp = Idx & (IsBarrierBreached == 1 | S>=H);
                if any(IdxTemp)
                    Price(IdxTemp) = K(IdxTemp).*exp(-r(IdxTemp).*T(IdxTemp));
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T>0.0);
                if  any(IdxTemp)
                    Price(IdxTemp) = F(IdxTemp);
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T==0.0);
                if  any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-B(IdxTemp)+C(IdxTemp)-D(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & (~(IsBarrierBreached == 1 | S>=H) & X<H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_up_out_put');
            if any(Idx)
                eta = -1;
                phi = -1;
                
                IdxTemp = Idx & ( Idx & (IsBarrierBreached == 1 | S>=H));
                if any(IdxTemp)
                    Price(IdxTemp) = K(IdxTemp).*exp(-r(IdxTemp).*T(IdxTemp));   
                end
                IdxTemp = Idx & ( Idx & (~(IsBarrierBreached == 1 | S>=H) & X>=H & T>0.0));
                if  any(IdxTemp)
                    Price(IdxTemp) = B(IdxTemp)-D(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S>=H) & X>=H & T==0.0);
                if  any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S>=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-C(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S>=H) & X<H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                         
            end
           
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_down_in_call');
            if any(Idx)
                eta = 1;
                phi = 1;
             
                IdxTemp = Idx & ( IsBarrierBreached == 1 | S<=H);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = C(IdxTemp)+E(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-B(IdxTemp)+D(IdxTemp)+E(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end                   
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_down_in_put');
            if any(Idx)
                eta = 1;
                phi = -1;
                
                IdxTemp = Idx & ( IsBarrierBreached == 1 | S<=H);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = B(IdxTemp)-C(IdxTemp)+D(IdxTemp)+E(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp) + E(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T==0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = 0.0;
                end                            
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_down_out_call');
            if any(Idx)
                eta = 1;
                phi = 1;
                
                IdxTemp = Idx & ( IsBarrierBreached == 1 | S<=H);
                if any(IdxTemp)
                    Price(IdxTemp) = K(IdxTemp).*exp(-r(IdxTemp).*T(IdxTemp));
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-C(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                
                
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) =  B(IdxTemp)-D(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_call'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                
            end
            
            Idx = ~IdxExpProc & strcmpi(OptionType,'amer_down_out_put');
            if any(Idx)
                eta = 1;
                phi = -1;
                
                IdxTemp = Idx & ( IsBarrierBreached == 1 | S<=H);
                if any(IdxTemp)
                    Price(IdxTemp) = K(IdxTemp).*exp(-r(IdxTemp).*T(IdxTemp));
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = A(IdxTemp)-B(IdxTemp)+C(IdxTemp)-D(IdxTemp)+F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X>=H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                
                
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T>0.0);
                if any(IdxTemp)
                    Price(IdxTemp) = F(IdxTemp);
                end
                IdxTemp = Idx & ( ~(IsBarrierBreached == 1 | S<=H) & X<H & T==0.0);
                if any(IdxTemp)
                    TmpOptionType = OptionType;
                    TmpOptionType(IdxTemp) = {'vanilla_put'};
                    Price(IdxTemp) = VanillaOptionBlackScholesModel(S(IdxTemp),X(IdxTemp),r(IdxTemp),b(IdxTemp),Vol(IdxTemp),T(IdxTemp),TmpOptionType(IdxTemp),...
                        L(IdxTemp), LM(IdxTemp), CM(IdxTemp), IsExpiryP(IdxTemp), YTDC(IdxTemp),Curr(IdxTemp)).getprice();
                end                          
            end
                        
                function ret = A(Idx)
                    ret = phi.*S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*normcdf(phi.*x1(Idx)) - phi.*X(Idx).*exp(-r(Idx).*T(Idx)).*normcdf(phi.*x1(Idx) - phi.*Vol(Idx).*sqrt(T(Idx)));
                end

                function ret = B(Idx)
                    ret = phi.*S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*normcdf(phi.*x2(Idx)) - ...
                        phi.*X(Idx).*exp(-r(Idx).*T(Idx)).*normcdf(phi.*x2(Idx) - phi.*Vol(Idx).*sqrt(T(Idx)));
                end

                function ret = C(Idx)
                    ret = phi.*S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*((H(Idx)./S(Idx)).^(2.*(1+mu(Idx)))).*normcdf(eta.*y1(Idx)) - ...
                        phi.*X(Idx).*exp(-r(Idx).*T(Idx)).*((H(Idx)./S(Idx)).^(2.*mu(Idx))).*normcdf(eta.*y1(Idx) - eta.*Vol(Idx).*sqrt(T(Idx)));
                end

                function ret = D(Idx)
                    ret = phi.*S(Idx).*exp((b(Idx)-r(Idx)).*T(Idx)).*((H(Idx)./S(Idx)).^(2.*(1+mu(Idx)))).*normcdf(eta.*y2(Idx)) - ...
                        phi.*X(Idx).*exp(-r(Idx).*T(Idx)).*((H(Idx)./S(Idx)).^(2.*mu(Idx))).*normcdf(eta.*y2(Idx) - eta.*Vol(Idx).*sqrt(T(Idx)));
                end

                function ret = E(Idx)
                    ret = K(Idx).*exp(-r(Idx).*T(Idx)).*(normcdf(eta.*x2(Idx) - eta.*Vol(Idx).*sqrt(T(Idx))) - ...
                        ((H(Idx)./S(Idx)).^(2.*mu(Idx))).*normcdf(eta.*y2(Idx)-eta.*Vol(Idx).*sqrt(T(Idx))));
                end

                function ret = F(Idx)
                    ret = K(Idx).*(((H(Idx)./S(Idx)).^(mu(Idx)+lambda(Idx))).*normcdf(eta.*z(Idx)) + ...
                        ((H(Idx)./S(Idx)).^(mu(Idx)-lambda(Idx))).*normcdf(eta.*z(Idx)-2.*eta.*lambda(Idx).*Vol(Idx).*sqrt(T(Idx))));
                end
        end
        
        function Delta = getdelta1(obj,varargin)
            narginchk(1,2);           
            if nargin == 1
                Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'EuropeanOptionAmericanBarrier','Change');
                Change = varargin{1};
            end
            L = obj.Lots;

            obj1 = EuropeanOptionAmericanBarrier((obj.Spot+Change), obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            obj2 = EuropeanOptionAmericanBarrier(max(0,(obj.Spot-Change)), obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
                
            Delta = (obj1.getprice()-obj2.getprice())./(2.*Change);
            Delta = L.*Delta;
        end
        
        function Gamma = getgamma11(obj,varargin)
            narginchk(1,2);
            if nargin == 1
                  Change = 0.1 .* obj.TickSize;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'EuropeanOptionAmericanBarrier','Change');
                Change = varargin{1};
            end
            
            L = obj.Lots;
            LM = obj.Lot_Mult; 
            CM = obj.Curr_Mult;

            obj1 = EuropeanOptionAmericanBarrier((obj.Spot+Change), obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            obj2 = EuropeanOptionAmericanBarrier(max(0,(obj.Spot-Change)), obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            
            Gamma = (obj1.getprice()-2.*obj.getprice()+obj2.getprice())./(Change.^2);
			Gamma(obj.TTM==0.0) = 0.0;
            
            Gamma = Gamma.*L.*LM./CM;
        end
        
        function Vega = getvega1(obj, varargin)
            narginchk(1,2);
            if nargin == 1
                Change = 0.0001;
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'EuropeanOptionAmericanBarrier','Change');
                 Change = varargin{1};
            end
            
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            

            obj1 = EuropeanOptionAmericanBarrier(obj.Spot, obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, (obj.Volatility+Change), obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            TempVol = max(0,obj.Volatility-Change);
            obj2 = EuropeanOptionAmericanBarrier(obj.Spot, obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, TempVol, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            
            Vega = (obj1.getprice()-obj2.getprice())./(2.*Change);
			Vega(obj.TTM==0.0) = 0.0;
            Vega = (Vega.*L.*LM.*CM)./100;            
        end
        
        function Theta = gettheta(obj, varargin)
            narginchk(1,2);
            OptionType = obj.OptType;
            NumInputs = size(OptionType);
            if nargin == 1
                Change = 1/(24*365);
                NumDay = repmat(Change,NumInputs);
            else
                validateattributes(varargin{1}, {'numeric'},{'nonempty'},'EuropeanOptionAmericanBarrier','Change');
                NumDay = repmat(varargin{1}/(24*365),NumInputs);
            end
            
            L = obj.Lots;
            LM = obj.Lot_Mult;
            CM = obj.Curr_Mult;
            YTDM = obj.YearToDay_Mult;

            obj1 = EuropeanOptionAmericanBarrier(obj.Spot, obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            obj2 = EuropeanOptionAmericanBarrier(obj.Spot, obj.Strike, obj.BarLevel, obj.Rebate,...
                obj.IsBarBreached, obj.RFR, obj.CostOfCarry, obj.Volatility, obj.TTM-NumDay, obj.OptType, obj.Lots, ...
                obj.Lot_Mult, obj.Curr_Mult, obj.YearToDay_Mult, obj.ExchangeATMConvention, obj.IsExpiryProcessed,obj.Currency,obj.TickSize);
            
            Theta = (obj2.getprice()-obj1.getprice())./(NumDay);        
			Theta(obj.TTM==0.0) = 0.0;
            Theta = (Theta.*L.*LM.*CM)./YTDM;   
        end
    
        function BE1 = getbe1(obj)
            IsExpiryP = obj.IsExpiryProcessed;
            CM = obj.Curr_Mult;
            T = obj.TTM;
            
            BE1 = zeros(size(T));
            
            IdxTemp = ~(IsExpiryP==1 | T==0);
            Theta = obj.gettheta();
            Gamma = obj.getgamma11();
            BE1(IdxTemp) = sqrt(((-2.*Theta(IdxTemp)./CM(IdxTemp))./Gamma(IdxTemp)));
            IdxTemp = ~isreal(BE1);
            BE1(IdxTemp) = NaN;
        end
        
        function curr_exp = getcurrencyexposure(obj)
            OptionType = obj.OptType;
            Curr = obj.Currency;
            L = obj.Lots;
            LM = obj.Lot_Mult;
            
            curr_exp = zeros(size(OptionType));
            Idx = ~(strcmp(Curr,'USD') | strcmp(Curr,'USd'));
            Price = obj.getprice();
            curr_exp(Idx) = Price(Idx).*L(Idx).*LM(Idx);
        end
    
    end
end
