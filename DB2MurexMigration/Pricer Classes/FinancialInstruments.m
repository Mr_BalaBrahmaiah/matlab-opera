classdef FinancialInstruments
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/02/20 11:08:19 $
%  $Revision: 1.1 $

    properties(GetAccess = private, SetAccess = private)
        DefaultOutput
    end
    methods (Abstract)
        getprice(obj)
        getdelta1(obj)
        getgamma11(obj)
        getvega1(obj)
        gettheta(obj)        
    end
    methods (Access = public)
        function obj = FinancialInstruments()
            obj.DefaultOutput =  ' '; % make it as 0, to print output as zero in all cells
        end
        function Delta2 = getdelta2(obj)
            Delta2 = obj.DefaultOutput;
        end
        function Gamma12 = getgamma12(obj)
            Gamma12 = obj.DefaultOutput;
        end
        function Gamma22 = getgamma22(obj)
            Gamma22 = obj.DefaultOutput;
        end
        function Vega2 = getvega2(obj)
            Vega2 = obj.DefaultOutput;
        end 
        function Chi = getchi(obj)
            Chi = obj.DefaultOutput;
        end
        function Curr_exp = getcurrencyexposure(obj)
            Curr_exp = obj.DefaultOutput;
        end
    end
end