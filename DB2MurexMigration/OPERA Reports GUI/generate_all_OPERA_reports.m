function  [OutErrorMsg] = generate_all_OPERA_reports()

try
    OutErrorMsg = {'No errors'};
    
    ObjDB = connect_to_database ;
    SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    TextFile = strcat('LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
    diary(TextFile) ;
    
    EMail_Report_1 = [];
    EMail_Report_2 = [];
    EMail_Report_3 = [];
    
    %% BUSINESS UNIT SELECTION
    
    theCell = {'AGF','COT','COC','COF','COP','ORX','QF1','QF2','QF3','QF4','QF5'};
    [InBUName, theChosenIDX] = uicellect_title(theCell,'BUSINESS UNIT SELECTION','Multi', 0);
    
    if(isempty(InBUName))
        OutErrorMsg = {'Please Select anyone of Business Unit'} ;
        warndlg('Please Select anyone of Business Unit');
        return;
    end
    
    
    %% REPORTS TAB SELECTION
    theCell = {'MO','RMO','MCO','ACCOUNTING'};
    [Selected_Report_Tab, theChosenIDX] = uicellect_title(theCell,'REPORTS TAB SELECTION','Multi', 0);
    
    if(isempty(Selected_Report_Tab))
        OutErrorMsg = {'Please Select anyone of reports Tab'} ;
        warndlg('Please Select anyone of reports Tab');
        return;
    end
    
    %     hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');
    
    %% MO REPORTS
    if(strcmpi(Selected_Report_Tab,'MO'))
        if(~isempty(cellStrfind_exact({'QF1','QF2','QF3','QF4','COT','COF','ORX'},InBUName)))
            MO_Reports_Button = {'Summary Report','Monthwise Summary Report','FXFutures Report','FXForwards Report'};
            [Selected_MO_Reports_Button, theChosenIDX] = uicellect_title(MO_Reports_Button,'MO_Reports_Button','Multi', 0);
        elseif(~isempty(cellStrfind_exact({'COC'},InBUName)))
            MO_Reports_Button = {'Traders Report Summary','PnP Report'};
            [Selected_MO_Reports_Button, theChosenIDX] = uicellect_title(MO_Reports_Button,'MO_Reports_Button','Multi', 0);
        else
            MO_Reports_Button = {'Portfolio Delta Report','Options Realized and Unrealized Report','Net Futures Portfoliowise Report','Net Futures Productwise Report',...
                'FF Portfolio Report','FF Positions Report','FF Greeks Report','PnL Monitoring Report','Greek PnL Report','VAR Report','Monthend_EOD',...
                'Traders Report Summary','PnP Report','Previous Settle Accounting Traders Report','Prervious Settle Accounting PnL Report'};
            [Selected_MO_Reports_Button, theChosenIDX] = uicellect_title(MO_Reports_Button,'MO_Reports_Button','Multi', 0);
        end
        
        if(~isempty(Selected_MO_Reports_Button))
            Overall_ErrorMsg = [];
            
            for i = 1 : length(Selected_MO_Reports_Button)
                Current_Report_Button =  Selected_MO_Reports_Button(i);
                
                hWaitbar = waitbar(0,[char(InBUName),' ',char(Current_Report_Button),' Processing.....'],'Name',' Processing.....');
                
                %% Portfolio Delta Report
                if(strcmpi(Current_Report_Button,'Portfolio Delta Report'))
                    [OutErrorMsg1,PortfolioDeltaFilename] = generate_portfolio_delta_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,PortfolioDeltaFilename]];
                    else
                        EMail_Report_1 = PortfolioDeltaFilename;
                    end
                    continue;
                end
                
                %% Options Realized and Unrealized Report
                if(strcmpi(Current_Report_Button,'Options Realized and Unrealized Report'))
                    [OutErrorMsg1,Opt_Real_Unr_Filename] = generate_opt_real_unr_report(InBUName);
                    if ~strcmpi(OutErrorMsg2,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Opt_Real_Unr_Filename]];
                    else
                        EMail_Report_1 = Opt_Real_Unr_Filename;
                    end
                    continue;
                end
                
                %% Net Futures Portfoliowise Report
                if(strcmpi(Current_Report_Button,'Net Futures Portfoliowise Report'))
                    [OutErrorMsg1,Net_Fut_PortfolioWise_Filename] = generate_net_fut_portfoliowise_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Net_Fut_PortfolioWise_Filename]];
                    else
                        EMail_Report_1 = Net_Fut_PortfolioWise_Filename;
                    end
                    continue;
                end
                
                %% Net Futures Productwise Report
                if(strcmpi(Current_Report_Button,'Net Futures Productwise Report'))
                    [OutErrorMsg1,Net_Fut_ProductWise_Filename] = generate_net_fut_prodwise_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Net_Fut_ProductWise_Filename]];
                    else
                        EMail_Report_1 = Net_Fut_ProductWise_Filename;
                    end
                    continue;
                end
                
                %% FF Portfolio Report
                if(strcmpi(Current_Report_Button,'FF Portfolio Report'))
                    [OutErrorMsg1,FF_Portfolio_Filename] = generate_ff_portfolio_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FF_Portfolio_Filename]];
                    else
                        EMail_Report_1 = FF_Portfolio_Filename;
                    end
                    continue;
                end
                
                %% FF Positions Report
                if(strcmpi(Current_Report_Button,'FF Positions Report'))
                    [OutErrorMsg1,FF_Position_Filename] = generate_ff_position_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FF_Position_Filename]];
                    else
                        EMail_Report_1 = FF_Position_Filename;
                    end
                    continue;
                end
                %% FF Greeks Report
                if(strcmpi(Current_Report_Button,'FF Greeks Report'))
                    [OutErrorMsg1,FF_Greeks_FileName] = generate_ff_greeks_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FF_Greeks_FileName]];
                    else
                        EMail_Report_1 = FF_Greeks_FileName;
                    end
                    continue;
                end
                %% PnL Monitoring Report
                if(strcmpi(Current_Report_Button,'PnL Monitoring Report'))
                    [OutErrorMsg1,PandL_Monitoring_Filename] = generate_pandl_monitoring_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,PandL_Monitoring_Filename]];
                    else
                        EMail_Report_1 = PandL_Monitoring_Filename;
                    end
                    continue;
                end
                %% Greek PnL Report
                if(strcmpi(Current_Report_Button,'Greek PnL Report'))
                    [OutErrorMsg1,Greel_PnL_Filename] = generate_greek_pandl_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Greel_PnL_Filename]];
                    else
                        EMail_Report_1 = Greel_PnL_Filename;
                    end
                    continue;
                end
                %% VAR Report
                if(strcmpi(Current_Report_Button,'VAR Report'))
                    [OutErrorMsg1,VARFilename] = generate_accounting_VAR_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,VARFilename]];
                    else
                        EMail_Report_1 = VARFilename;
                    end
                    continue;
                end
                %% Monthend_EOD
                if(strcmpi(Current_Report_Button,'Monthend_EOD'))
                    [OutErrorMsg1,MonthEnd_EOD_Filename] = generate_monthend_eod_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,MonthEnd_EOD_Filename]];
                    else
                        EMail_Report_1 = MonthEnd_EOD_Filename;
                    end
                    continue;
                end
                %% Traders Report Summary
                if(strcmpi(Current_Report_Button,'Traders Report Summary'))
                    [OutErrorMsg1,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,TradersReportSummaryFilename]];
                    else
                        EMail_Report_1 = TradersReportSummaryFilename;
                    end
                    continue;
                end
                %% PnP Report
                if(strcmpi(Current_Report_Button,'PnP Report'))
                    [OutErrorMsg1,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,PnpOptionDumpFileName]];
                    else
                        EMail_Report_1 = PnpOptionDumpFileName;
                        EMail_Report_2 = PnpFutureDumpFileName;
                        EMail_Report_3 = PnpMUsDumpFileName;
                        
                    end
                    continue;
                end
                %% Previous Settle Accounting Traders Report
                if(strcmpi(Current_Report_Button,'Previous Settle Accounting Traders Report'))
                    [OutErrorMsg1,AccountingTRReport] = generate_previous_settle_accounting_traders_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,AccountingTRReport]];
                    else
                        EMail_Report_1 = AccountingTRReport;
                    end
                    continue;
                end
                %% Prervious Settle Accounting PnL Report
                if(strcmpi(Current_Report_Button,'Prervious Settle Accounting PnL Report'))
                    [OutErrorMsg1,PnlReport] = generate_previous_accounting_tr_pnl_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,PnlReport]];
                    else
                        EMail_Report_1 = PnlReport;
                    end
                    continue;
                end
                %% Summary Report (QF1,QF2,QF3,QF4)
                if(strcmpi(Current_Report_Button,'Summary Report'))
                    [OutErrorMsg1,Summary_Report] = generate_mo_summary_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Summary_Report]];
                    else
                        EMail_Report_1 = Summary_Report;
                    end
                    continue;
                end
                %% Monthwise Summary Report (QF1,QF2,QF3,QF4)
                if(strcmpi(Current_Report_Button,'Monthwise Summary Report'))
                    [OutErrorMsg1,MonthWise_Summary_Report] = generate_mo_summary_monthwise_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,MonthWise_Summary_Report]];
                    else
                        EMail_Report_1 = MonthWise_Summary_Report;
                    end
                    continue;
                end
                %% FXFutures Report (QF1,QF2,QF3,QF4)
                if(strcmpi(Current_Report_Button,'FXFutures Report'))
                    [OutErrorMsg1,FxFutures_Report] = generate_mo_fxfutures_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FxFutures_Report]];
                    else
                        EMail_Report_1 = FxFutures_Report;
                    end
                    continue;
                end
                %% FXForwards Report (QF1,QF2,QF3,QF4)
                if(strcmpi(Current_Report_Button,'FXForwards Report'))
                    [OutErrorMsg1,FxForward_Report] = generate_mo_fxforwards_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FxForward_Report]];
                    else
                        EMail_Report_1 = FxForward_Report;
                    end
                    continue;
                end
            end
        else
            OutErrorMsg = {'Please Select anyone of the MO Report Buttton'};
        end
        
        %% RMO REPORTS
    elseif(strcmpi(Selected_Report_Tab,'RMO'))
        RMO_Reports_Button = {'Options Report','Futures Report','FX Report','Trade Query Report','OTC Report','Netting PnL Reports'};
        [Selected_RMO_Reports_Button, theChosenIDX] = uicellect_title(RMO_Reports_Button,'RMO_Reports_Button','Multi', 0);
        
        if(~isempty(Selected_RMO_Reports_Button))
            Overall_ErrorMsg = [];
            for i = 1 : length(Selected_RMO_Reports_Button)
                Current_Report_Button =  Selected_RMO_Reports_Button(i);
                
                hWaitbar = waitbar(0,[char(InBUName),' ',char(Current_Report_Button),' Processing.....'],'Name',' Processing.....');
                
                %% Options Report
                if(strcmpi(Current_Report_Button,'Options Report'))
                    [OutErrorMsg1,OptionReport] = generate_options_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,OptionReport]];
                    else
                        EMail_Report_1 = OptionReport;
                    end
                    continue;
                end
                %% Futures Report
                if(strcmpi(Current_Report_Button,'Futures Report'))
                    [OutErrorMsg1,FutureReport] = generate_futures_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FutureReport]];
                    else
                        EMail_Report_1 = FutureReport;
                    end
                    continue;
                end
                %% FX Report
                if(strcmpi(Current_Report_Button,'FX Report'))
                    [OutErrorMsg1,FX_Report] = generate_currency_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,FX_Report]];
                    else
                        EMail_Report_1 = FX_Report;
                    end
                    continue;
                end
                %% Trade Query Report
                if(strcmpi(Current_Report_Button,'Trade Query Report'))
                    [OutErrorMsg1,TradeQueryReport] = generate_trade_query_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,TradeQueryReport]];
                    else
                        EMail_Report_1 = TradeQueryReport;
                    end
                    continue;
                end
                %% OTC Report
                if(strcmpi(Current_Report_Button,'OTC Report'))
                    [OutErrorMsg1,OTC_Report] = generate_riskoffice_rms_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,OTC_Report]];
                    else
                        EMail_Report_1 = OTC_Report;
                    end
                    continue;
                end
                %% Netting PnL Reports
                if(strcmpi(Current_Report_Button,'Netting PnL Reports'))
                    [OutErrorMsg1,NettingPNL_Report] = generate_netting_pnl_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,NettingPNL_Report]];
                    else
                        EMail_Report_1 = NettingPNL_Report;
                    end
                    continue;
                end
            end
        else
            OutErrorMsg = {'Please Select anyone of the RMO Report Buttton'};
        end
        %% MCO REPORTS
    elseif(strcmpi(Selected_Report_Tab,'MCO'))
        RMO_Reports_Button = {'Summary Report','Trade Details','Settlement Price & Delta Details','Counterparty Delta Position Report','Delta Reports'};
        [Selected_MCO_Reports_Button, theChosenIDX] = uicellect_title(RMO_Reports_Button,'RMO_Reports_Button','Multi', 0);
        
        if(~isempty(Selected_MCO_Reports_Button))
            Overall_ErrorMsg = [];
            for i = 1 : length(Selected_MCO_Reports_Button)
                Current_Report_Button =  Selected_MCO_Reports_Button(i);
                
                hWaitbar = waitbar(0,[char(InBUName),' ',char(Current_Report_Button),' Processing.....'],'Name',' Processing.....');
                
                %% Summary Report
                if(strcmpi(Current_Report_Button,'Summary Report'))
                    [OutErrorMsg1,Summary_Report] = generate_compliance_report_summary(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Summary_Report]];
                    else
                        EMail_Report_1 = Summary_Report;
                    end
                    continue;
                end
                %% Trade Details
                if(strcmpi(Current_Report_Button,'Trade Details'))
                    [OutErrorMsg1,TradeDetails_Report] = generate_compliance_trade_view(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,TradeDetails_Report]];
                    else
                        EMail_Report_1 = TradeDetails_Report;
                    end
                    continue;
                end
                %% Settlement Price & Delta Details
                if(strcmpi(Current_Report_Button,'Settlement Price & Delta Details'))
                    [OutErrorMsg1,SettlementPrice_Delta_Report] = generate_compliance_summary_view(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,SettlementPrice_Delta_Report]];
                    else
                        EMail_Report_1 = SettlementPrice_Delta_Report;
                    end
                    continue;
                end
                %% Counterparty Delta Position Report
                if(strcmpi(Current_Report_Button,'Counterparty Delta Position Report'))
                    [OutErrorMsg1,CounterpartyDelta_Report] = generate_counterparty_delta_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,CounterpartyDelta_Report]];
                    else
                        EMail_Report_1 = CounterpartyDelta_Report;
                    end
                    continue;
                end
                %% Delta Reports
                if(strcmpi(Current_Report_Button,'Delta Reports'))
                    [OutErrorMsg1,OutFilename1,OutFilename2,OutFilename3] = generate_compliance_delta_reports(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,OutFilename1]];
                    else
                        EMail_Report_1 = OutFilename1;
                        EMail_Report_2 = OutFilename2;
                        EMail_Report_3 = OutFilename3;
                        
                    end
                    continue;
                end
            end
        else
            OutErrorMsg = {'Please Select anyone of the MCO Report Buttton'};
        end
        
        %% ACCOUNTING REPORTS
    elseif(strcmpi(Selected_Report_Tab,'ACCOUNTING'))
        Accounting_Reports_Button = {'Accounting Report'};
        %[Selected_Accounting_Reports_Button, theChosenIDX] = uicellect_title(Accounting_Reports_Button,'Accounting_Reports_Button','Multi', 0);
        Selected_Accounting_Reports_Button = Accounting_Reports_Button;
        
        if(~isempty(Selected_Accounting_Reports_Button))
            Overall_ErrorMsg = [];
            for i = 1 : length(Selected_Accounting_Reports_Button)
                Current_Report_Button =  Selected_Accounting_Reports_Button(i);
                
                hWaitbar = waitbar(0,[char(InBUName),' ',char(Current_Report_Button),' Processing.....'],'Name',' Processing.....');
                
                %% Accounting Report
                if(strcmpi(Current_Report_Button,'Accounting Report'))
                    [OutErrorMsg1,Accounting_Report] = generate_accounting_report(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[Current_Report_Button ,OutErrorMsg1,Accounting_Report]];
                    else
                        EMail_Report_1 = Accounting_Report;
                    end
                    continue;
                end
            end
        else
            OutErrorMsg = {'Please Select anyone of the Accounting Report Buttton'};
            warndlg('Please Select anyone of the Accounting Report Buttton');
        end
    else
        OutErrorMsg = {'Please Selected anyone of Reports Tab'};
        warndlg('Please Selected anyone of Reports Tab');
    end
    
    %% Excel Write
    
    Opera_Report_Log = getXLSFilename('Opera_Report_Log');
    if(isempty(Overall_ErrorMsg))
        Overall_ErrorMsg = {'No Errors'};
    end
    %     xlswrite(Opera_Report_Log,Overall_ErrorMsg);
    
    waitbar(1,hWaitbar,[char(Current_Report_Button),' Finished']);
    
    diary off ;
    
    %% Send Email
    
    System_Name = upper(getComputerName()) ;
    
    Email_Subject = [char(InBUName),' ',char(Selected_Report_Tab),' ',char(Current_Report_Button),' Report for COB ''',char(SettlementDate),''' '] ;
    try
        if(~isempty(EMail_Report_2) && ~isempty(EMail_Report_3))
            sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,...
                ['Attached ',Email_Subject,' from ',System_Name],{EMail_Report_1,EMail_Report_2,EMail_Report_3});
        else
            sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,...
                ['Attached ',Email_Subject,' from ',System_Name],{EMail_Report_1});
        end
    catch
        sendmail({'raghavendra.sn@olamnet.com'},['Error in OPERA Report Parser ',Email_Subject],[' No Attachement ',Email_Subject,' from ',System_Name]);
    end
    
catch ME
    delete(hWaitbar) ;
    
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
end

diary off ;

