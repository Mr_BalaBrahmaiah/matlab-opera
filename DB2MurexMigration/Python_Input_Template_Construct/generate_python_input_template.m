function [OutErrorMsg,OutXLSName] = generate_python_input_template(InBUName)
try
    %     InBUName = 'agf';
    
    OutErrorMsg = {'No Errors'};
    %     OutXLSName = getXLSFilename([upper(char(InBUName)),'_Python_Input_Template']);
    OutXLSName = getXLSFilename('path');
    
    OutXLSName = strrep(OutXLSName,'.xlsx','.csv');
    ObjDB = connect_to_database;
    
    %% Path Variables
    VAR_Write_Path= fullfile(pwd,'DB2MurexMigration\Python_Input_Template_Construct\Python\VarCalc\pkgs\src\scripts\var_write');
    PortfolioVARModule_Path=fullfile(pwd,'DB2MurexMigration\Python_Input_Template_Construct\Python\VarCalc\pkgs\src\scripts\PortfolioVARModule');
    Date_Str='2018-02-23';   
       
    %% Input File
    
    %     [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
    %     InXLSFilename = fullfile(pathname,filename);
    %     [~,~,RawData] = xlsread(InXLSFilename,'Sample Trades from OPERA');
    %     Input_Header = RawData(1,:);
    %     RawData(1,:) = [];
    
    %% Get Aggregation Data
    AggrrgationTable = strcat('var_aggregation_level_table_',char(InBUName));
    [AggrrgationTable_Header,AggrrgationTable_Data] = Fetch_DB_Data(ObjDB,[],AggrrgationTable);
    
    if(strcmpi(AggrrgationTable_Data,'No Data'))
        OutErrorMsg = {'Not Getting Aggregation Data from DB'};
        return;
    end
    if(isempty(AggrrgationTable_Data))
        OutErrorMsg = {'Not Getting Aggregation Data from DB'};
        return;
    end
    
    AggrrgationTable_Data = cell2dataset([AggrrgationTable_Header ; AggrrgationTable_Data]) ;
    
    Unique_AggrgationLevel = unique(AggrrgationTable_Data.aggregation_level);
    
    %% Input from DB
    
    Default_TableName = 'deal_ticket_security_info_table_view_';
    TableName = [Default_TableName,char(InBUName)];
    SqlQuery = ['select * from ',TableName,' where group_type in (''OFUT'',''FUT'') '];
    [Input_Header,RawData] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(RawData))
        OutErrorMsg = {['Not getting Data from ',TableName]};
        return;
    end
    if(strcmpi(RawData,'No Data'))
        OutErrorMsg = {['Not getting Data from ',TableName]};
        return;
    end
    if(strcmpi(Input_Header,'Error Data'))
        OutErrorMsg = {['Error in getting Data from ',TableName]};
        return;
    end
    
    % Dataset
    Overall_InputData = cell2dataset([Input_Header;RawData]);
    
    %%
    Output_Header = {'Type','strike','vol_id','call_put_id','optiontype','barriertype','direction','knockin','knockout','bullet','shorted','hedgeorOTC','lots'};
    
    %%
    Overall_VAR_Number = cell(size(Unique_AggrgationLevel,1),2);
    for k = 1 : length(Unique_AggrgationLevel)
        Current_AggrgationLevel = Unique_AggrgationLevel(k);
        
        Matched_ProductCode = AggrrgationTable_Data.product_code( strcmpi(AggrrgationTable_Data.aggregation_level,Current_AggrgationLevel));
        Matched_ProductCode = strrep(Matched_ProductCode,'CM ','');
        Matched_Subportfolio = AggrrgationTable_Data.subportfolio( strcmpi(AggrrgationTable_Data.aggregation_level,Current_AggrgationLevel));
        
        InputData = Overall_InputData( ismember(Overall_InputData.product_code,Matched_ProductCode) & ismember(Overall_InputData.subportfolio,Matched_Subportfolio), :);
        
        if(isempty(InputData))
            continue;
        end
        
        %%
        OutputData = cell(size(InputData,1),size(Output_Header,2));
        OutputData = cell2dataset([Output_Header;OutputData]);
        
        for i = 1 : size(InputData,1)
            
            %% group_type &  optiontype  & hedgeorOTC
            Current_Grouptype = InputData.group_type(i);
            if(strcmpi(Current_Grouptype,'OFUT'))
                OutputData.Type(i) = {'Option'};
                OutputData.optiontype(i) = {'amer'};
                OutputData.hedgeorOTC(i) = {'OTC'};
            else
                OutputData.Type(i) = {'Future'};
                OutputData.hedgeorOTC(i) = {'OTC'};
            end
            
            %% strike
            Current_Strike = InputData.strike(i);
            OutputData.strike(i) = {Current_Strike};
            
            %% Vol ID
            Current_ProductCode = InputData.product_code(i);
            Current_ContractMonth = InputData.contract_month(i);
            
            Current_VolID = strcat(Current_ProductCode,{' '},Current_ContractMonth);
            OutputData.vol_id(i) = Current_VolID;
            
            %% call Put ID
            
            if(cellStrfind(InputData.derivative_type(i),'put'))
                OutputData.call_put_id(i) = {'put'};
            elseif(cellStrfind(InputData.derivative_type(i),'call'))
                OutputData.call_put_id(i) = {'call'};
            else
                OutputData.vol_id(i) = cellfun(@(s) (s(1:end-3)), Current_VolID,'UniformOutput',0); %% Future Only one Maturity
            end
            
            %% shorted
            
            Current_MarketAction = InputData.market_action(i);
            if(strcmpi(Current_MarketAction,'bought'))
                OutputData.shorted(i) = {'FALSE'};
            else
                OutputData.shorted(i) = {'TRUE'};
            end
            
            %% Lots
            
            Current_Lots = InputData.active_lots(i);
            
            OutputData.lots(i)  = {abs(Current_Lots)};
            
        end
        
        %% Excel Write
        
        %         xlswrite(OutXLSName,dataset2cell(OutputData),'Output');
        %         xlswrite(OutXLSName,dataset2cell(InputData),'Input');
        %         xls_delete_sheets(fullfile(pwd,OutXLSName));
        
        OutputData = dataset2cell(OutputData);
        ColNames = OutputData(1,:);
        Data = OutputData(2:end,:);
        customcsvwrite(OutXLSName,Data,ColNames);
        
        %% Python Module
        try
            py.importlib.import_module('var_api');
            Var_Number = py.var_api.run_var(Matched_ProductCode,VAR_Write_Path,OutXLSName,PortfolioVARModule_Path,Date_Str);
            
            Overall_VAR_Number(k,1) = Current_AggrgationLevel;
            Overall_VAR_Number(k,2) = num2cell(Var_Number);
            
            delete(fullfile(pwd,OutXLSName)); % No Need
        catch
            Overall_VAR_Number(k,1) = Current_AggrgationLevel;
            Overall_VAR_Number(k,2) = {'Error in python Module'};
        end
        
    end
    
    %% VAR Output
    OutXLSName = getXLSFilename('Python_VAR_Output');
    Out_Header = {'Aggregation_Level','5% VAR Number'};
    
    xlswrite(OutXLSName,[Out_Header;Overall_VAR_Number]) ;
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end
