function upload_var_table_data(Type)

if strcmpi(Type,'price')
    [~,~,RawData] = xlsread('PriceChange_RawData.xlsx');
else
    [~,~,RawData] = xlsread('VolChange_RawData.xlsx');
end

SettleDates = RawData(2:end,1);
ContChangeMonths = RawData(1,2:end);
Data = RawData(2:end,2:end);

if ~iscolumn(ContChangeMonths)
ContChangeMonths = ContChangeMonths';
end

% BBGContChangeMonths = ContChangeMonths;
% ContChangeMonths = strrep(ContChangeMonths,' ','');

OutData = [];

for iDate = 1:length(SettleDates)
    
    SettlementDate = repmat(cellstr(datestr(datenum(SettleDates{iDate},'dd-mm-yyyy'),'yyyy-mm-dd')),size(ContChangeMonths));
    RowData = Data(iDate,:);
    if ~iscolumn(RowData)
        RowData = RowData';
    end
        
    SettleDateData = [SettlementDate,ContChangeMonths,RowData];
    
    OutData = [OutData;SettleDateData];
end

if strcmpi(Type,'price')
    Header = {'settlement_date','cont_month','price_change'};
    xlswrite('Price_change_data.xlsx',[Header;OutData]);
    upload_in_database('var_cont_price_change_table',OutData);
else
    Header = {'settlement_date','cont_month','vol_change'};    
    xlswrite('Vol_change_data.xlsx',[Header;OutData]);
    upload_in_database('var_cont_vol_change_table',OutData);
end