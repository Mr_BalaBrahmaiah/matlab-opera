function pagesetup(OutPDFFile)
s=OutPDFFile.CurrentPageLayout;
        s.PageSize.Height='13.12in';
        s.PageSize.Width='21.5in'; %19.5
        s.PageMargins.Left='0.12in';
        s.PageMargins.Right='0.12in';
        s.PageMargins.Top='0.55in';
        s.PageMargins.Bottom='0.55in';
        s.PageMargins.Header='0.3in';
        s.PageMargins.Footer='0.3in';
end
