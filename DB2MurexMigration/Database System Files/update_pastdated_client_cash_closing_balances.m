function OutErrorMsg = update_pastdated_client_cash_closing_balances(InBUName,client_code,Currency,S_Date,E_Date)

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    LogFilename = getTimeStampedFilename('ML_CashAccBalaceRefresh_Log','.txt');
    
    OutErrorMsg = {'No Errors'};
    ObjDB = connect_to_database;
    
    TableName=['client_cash_account_balance_table_',InBUName];
    
    %% If all the inputs are given by the user.
    
    SqlQuery=['select * from ',char(TableName),' where client_code = ''',char(client_code),''' and currency like ''USD%'' group by cob_date'];
    [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);    
        
    DB_Client_Data = cell2table(Data);
    DB_Client_Data.Properties.VariableNames = SettleColNames;
    
    UniqueCOBDates = unique(DB_Client_Data.cob_date,'stable');
    
    for iDate = 1:length(UniqueCOBDates)
        
        SettlementDate = UniqueCOBDates{iDate};
        
        SqlQuery = ['select settlement_date from vdate_sdate_table where value_date = ''',char(SettlementDate),''''];
        PrevSettlementDate = char(fetch(ObjDB,SqlQuery));
        
        IdxPrevDate = strcmpi(PrevSettlementDate,DB_Client_Data.cob_date);
        IdxTodaysDate = strcmpi(SettlementDate,DB_Client_Data.cob_date);
        
        PrevDayCashBalanceData = DB_Client_Data(IdxPrevDate,:);
        TodaysCashBalanceData  = DB_Client_Data(IdxTodaysDate,:);
        
        if ~isempty(PrevDayCashBalanceData)
            % assign previous days closing balance to todays opening balance
            TodaysCashBalanceData.opening_balance = PrevDayCashBalanceData.closing_balance;
        end
        TodaysCashBalanceData.closing_balance = TodaysCashBalanceData.opening_balance + ...
            TodaysCashBalanceData.expired_or_liquidated_cash + TodaysCashBalanceData.premium_paid_or_recd + ...
            TodaysCashBalanceData.commission + TodaysCashBalanceData.interest_charges + ...
            TodaysCashBalanceData.transfers_in + TodaysCashBalanceData.transfers_out;
        DB_Client_Data(IdxTodaysDate,:) = TodaysCashBalanceData;
    end    
        
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where client_code = ''',char(client_code),''' and currency like ''USD%'''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, table2cell(DB_Client_Data));
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end