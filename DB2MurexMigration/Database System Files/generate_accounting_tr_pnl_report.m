function PnlReport = generate_accounting_tr_pnl_report(InBUName)

ViewName = ['helper_accounting_tr_pnl_view_',InBUName];
[ColNames,Data] = read_from_database(ViewName,0);

TRData = cell2dataset([ColNames;Data]);

Filename = ['Accounting_Pnl_Report_',upper(InBUName)];
PnlReport = getXLSFilename(Filename);
    
try
    if (exist(PnlReport,'file') == 2)
        delete(PnlReport);
    end
catch
end

Subportfolios = unique(TRData.subportfolio);
NumPortfolios = length(Subportfolios);

Header = {'ValueDate','Subportfolio','delta','gamma','theta','vega',' mtm-usd','PnL'};

HolDates = {'2016-01-01';'2016-12-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

SqlQuery = 'select value_date from valuation_date_table';
[~,Data] = read_from_database('valuation_date_table',0,SqlQuery);
TodayDate = char(Data);

VBusDays = busdays(datenum('2016-01-04'), datenum(TodayDate),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

for iPF = 1:NumPortfolios
    TempPF = regexprep(Subportfolios{iPF},'[^a-zA-Z0-9]','_');
    
    IdxPortfolio = strcmpi(Subportfolios{iPF},TRData.subportfolio);
    
    ValueDate = TRData.value_date(IdxPortfolio);
    M2M = TRData.mtm_usd(IdxPortfolio);
    
    PortfolioData = [ValueDate,...
        TRData.subportfolio(IdxPortfolio),....
        num2cell(TRData.delta(IdxPortfolio)),...
        num2cell(TRData.gamma(IdxPortfolio)),...
        num2cell(TRData.theta(IdxPortfolio)),...
        num2cell(TRData.vega(IdxPortfolio)),...
        num2cell(M2M)];
    
    PortfolioData = sortrows(PortfolioData,1);
        
    DailyPnL = zeros(size(PortfolioData,1),1);
       
    LoopCntr = 1;
    try
    for i = 1:length(VBusDays)
       
            IdxToday = strcmpi(VDates{i},ValueDate);
            IdxYest   = strcmpi(SDates{i},ValueDate);
            
            TodaysM2M  = M2M(IdxToday);
            YestM2M    = M2M(IdxYest);
            
            if isempty(TodaysM2M) && isempty(YestM2M) 
                continue;
            end            
            
            if isempty(TodaysM2M) && ~isempty(YestM2M)
                continue;
            end            
            
            if isempty(YestM2M) && ~isempty(TodaysM2M)
                YestM2M = 0;
            end            
                
            if i~=1
                DailyPnL(LoopCntr) = TodaysM2M - YestM2M;               
            else
                DailyPnL(LoopCntr) = TodaysM2M;               
            end
        LoopCntr = LoopCntr + 1;
    end
    
    OutData = [PortfolioData,num2cell(DailyPnL)];
    xlswrite(PnlReport,[Header;OutData],TempPF);
    catch
        disp(TempPF);
        continue;
    end
end

try
    xls_delete_sheets(fullfile(pwd,PnlReport));
catch
end