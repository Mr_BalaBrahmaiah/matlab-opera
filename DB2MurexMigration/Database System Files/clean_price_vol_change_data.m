function OutDSData = clean_price_vol_change_data(DSData)

% %% temp code below - to be removed after integration with compute price vol change data
% [filename, pathname] = uigetfile( ...
%     {'*.xlsx';'*.xls';'*.*'}, ...
%     'Pick the files');
% 
% if pathname ~= 0
%     Fullfilename = fullfile(pathname,filename);
% else 
%     return;
% end
% 
% [~,~,RawData] = xlsread(Fullfilename);
% Header = RawData(1,:);
% Data = RawData(2:end,:);
% DSData = cell2dataset([Header;Data]);
% %% temp code above - to be removed after integration with compute price vol change data
% %  once integrated, DSData can be obtained directly from function input

% find unique rows and find the prodcut code and cont month number
DSData = unique(DSData);
Out = char(DSData.cont_month);
%% TODO - rewrite the code more generic by using regexp to extract alphabets 
% and numbers for product code and cont month number respectively

% DSData.product_code = cellstr(Out(:,1:2)); %% Old
% [AssetClass,Remain_ProductCode] = strtok(DSData.underlying_id,' ');
% [ProductCode,Remain_ProductCode] = strtok(Remain_ProductCode);
% ProductCode = strtrim(ProductCode);
% DSData.product_code = ProductCode; %% New

% DSData.cont_month_number = str2num(Out(:,3:end)); %% Old
[cont_month_number,NumStart,NumberEnd] = regexp(DSData.cont_month,'\d+(\.)?(\d+)?','match');
cont_month_number = str2double([cont_month_number{:}])';
DSData.cont_month_number = cont_month_number; %% New

OutDSData = DSData;

[ColNames,Data] = read_from_database('var_product_code_mapping_table',0);
ProductCodeMapping = cell2dataset([ColNames;Data]);

AddedData = [];
IdxRemoval = [];

ContMonthNumber = 15;

SDates = unique(DSData.settlement_date);
for iDate = 1:length(SDates)
    IdxDate = ismember(DSData.settlement_date,SDates(iDate));
    Products = unique(DSData.product_code(IdxDate));
    
    for iProd = 1:length(Products)
        IdxProduct = IdxDate & strcmpi(Products{iProd},DSData.product_code);
        ContMonthNumbers = DSData.cont_month_number(IdxProduct);
        
        MaxContMonth = max(ContMonthNumbers);
        
        if MaxContMonth > ContMonthNumber
            IdxRem = IdxProduct & (DSData.cont_month_number > ContMonthNumber);
            IdxRemoval = [IdxRemoval;find(IdxRem)];
        elseif MaxContMonth < ContMonthNumber
            IdxMapping = strcmpi(Products{iProd},ProductCodeMapping.cont_month_format_code); %% ProductCodeMapping.invenio_product_code
            if isempty(find(IdxMapping))
                continue;
            end
        ContMonthFormatProduct = ProductCodeMapping.cont_month_format_code{IdxMapping};
        
            IdxAdd = IdxProduct & (DSData.cont_month_number == MaxContMonth);
            NewContMonth = MaxContMonth+1:ContMonthNumber;
            NumRows = length(NewContMonth);
            RowData = repmat(DSData(IdxAdd,:),NumRows,1);
            for iR = 1:NumRows
                RowData.cont_month_number(iR) = NewContMonth(iR);
                RowData.cont_month(iR) = cellstr([ContMonthFormatProduct,num2str(NewContMonth(iR))]);
            end
            AddedData= [AddedData;RowData];
        end
    end
end

OutDSData(IdxRemoval,:) = [];
OutDSData = [OutDSData;AddedData];

OutDSData = sortrows(OutDSData);

% OutFilename = getXLSFilename('Cleaned_Data');
% xlswrite(OutFilename,dataset2cell(OutDSData));


