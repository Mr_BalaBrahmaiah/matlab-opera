function output = Sep1000Str(g)
[row,col]=size(g);
% output=[];
output=cell(row,col);
for k=1:row
    for l=1:col
        S=g{k,l};        
        if ~isempty(S)
            S=strrep(S,',','');
            tf=strfind(S,'.');
            % [a,b]=strtok(N,'.');
            if isempty(tf)
                
                if strfind(S,'-')
                    S(2, length(S) - 3:-3:2) = ',';
                    output{k,l} = transpose(S(S ~= char(0)));
                else
                    S(2, length(S) - 3:-3:1) = ',';
                    output{k,l} = transpose(S(S ~= char(0)));
                end
                % tf= strfin
                % S = num2str(N);
            else
                if length(tf)==1
                t=S(1:tf-1);
                t1=S(tf+1:end);
                if strfind(S,'-')
                    t(2, length(t) -3:-3:2) = ',';
                    t = transpose(t(t ~= char(0)));
                    output{k,l} =[t '.' t1];
                else
                    t(2, length(t) -3:-3:1) = ',';
                    t = transpose(t(t ~= char(0)));
                    output{k,l} =[t '.' t1];
                end
                  else
                    tf=strfind(S,'/');
                    if ~isempty(tf)
                    data1=S(1:tf-1);
                    data2=S(tf+1:end);
                    data1= Sep1000Str(cellstr(data1));
                    data2= Sep1000Str(cellstr(data2));
                    output{k,l}=[num2str(cell2mat(data1)), '/', num2str(cell2mat(data2))];
                    else
                      output{k,l} ={''};   
                    end
                end
            end
        else
             output{k,l} ={''};
        end
    end
end
output=cellfun(@(x) char(x),output, 'UniformOutput',false);
