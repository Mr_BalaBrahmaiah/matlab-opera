% find the number of local workers available in the pc dynamically
myCluster = parcluster('local');
NumWorkers = myCluster.NumWorkers;

% start the parallel pool of matlab workers
parpool('local',NumWorkers);

tic
% view to be read from database
ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';

objDB = connect_to_database;

% find the columns of the given view
% ColNames = columns(objDB,objDB.Instance,objDB.Instance,ViewName);

% find the number of records in the given view so that it can be split
% across the available pool of workers
SqlQuery = ['select count(trade_id) from ',ViewName];
NumRecords = cell2mat(fetch(objDB,SqlQuery));

RecordPerWorker = NumRecords/NumWorkers;

ArrayRecords = 0:RecordPerWorker:NumRecords;

spmd
    switch(labindex)
        case 1
            SqlQuery = ['select * from ',ViewName,' limit ',num2str(ArrayRecords(1)),',',num2str(RecordPerWorker)];
            [InReportHeader,InReportData] = read_from_database(ViewName,0,SqlQuery);
        case 2
            SqlQuery = ['select * from ',ViewName,' limit ',num2str(ArrayRecords(2)),',',num2str(RecordPerWorker)];
            [InReportHeader,InReportData] = read_from_database(ViewName,0,SqlQuery);
        case 3
            SqlQuery = ['select * from ',ViewName,' limit ',num2str(ArrayRecords(3)),',',num2str(RecordPerWorker)];
            [InReportHeader,InReportData] = read_from_database(ViewName,0,SqlQuery);
        case 4
            SqlQuery =['select * from ',ViewName,' limit ',num2str(ArrayRecords(4)),',',num2str(RecordPerWorker)];
            [InReportHeader,InReportData] = read_from_database(ViewName,0,SqlQuery);
    end
    
end

ColNames = InReportHeader{1};
Data = [InReportData{1};InReportData{2};InReportData{3};InReportData{4}];
toc

XLFilename = [ViewName,'.xlsx'];
xlswrite(XLFilename,[ColNames;Data]);
delete(gcp)