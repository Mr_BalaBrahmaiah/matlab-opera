function refresh_and_save_p_and_g_views

ObjDB = connect_to_database;

SqlQuery = 'truncate TABLE helper_settle_p_and_g_view_cache_table';
Curs = exec(ObjDB,SqlQuery);

SqlQuery = 'INSERT INTO helper_settle_p_and_g_view_cache_table SELECT * FROM helper_settle_p_and_g_view';
Curs = exec(ObjDB,SqlQuery);

SqlQuery = 'truncate TABLE helper_traders_view_cache_table';
Curs = exec(ObjDB,SqlQuery);

sqlQuery = 'INSERT INTO helper_traders_p_and_g_view_cache_table SELECT * FROM helper_traders_p_and_g_view';
Curs = exec(ObjDB,SqlQuery);

