function run_sensitivity_report_grn()

theCell = {'FUT','OFUT','OTC'};
[Check_Str, theChosenIDX] = uicellect(theCell,'Multi', 0);

%% FUTURE M2M with PRICEMOVE REPORT
if(strcmpi(Check_Str,'FUT'))
    InBUName = 'grn';
    InputFilterValues = {'subportfolio',{'GR-GRAINS TRD','GR-GENEVA','GR-USA'};...
        'derivative_type',{'future'};'deal_status',{'live'}};
    InputScenarios = [{'on',5,2,'Percentage Move'};{'off',3,5,''};{'off',3,10,''}];
    SummmaryParameters = {'subportfolio','product_code','instrument','contract_month'};
    PricingType = 'settle';
    OutputFields = {'active_lots','price','pricemove','m2m_usd'};
    
    Report_Name = strcat('FUT_Sensitivity_',char(InBUName));
    
elseif(strcmpi(Check_Str,'OFUT'))
    
    %% OPTIONS with PRICEMOVE & VOLMOVE REPORT 'subportfolio',{'GR-GRAINS TRD','GR-GENEVA','GR-USA','GR-USA-CSO'};
    InBUName = 'grn';
    InputFilterValues = {'derivative_type',{'vanilla_call','vanilla_put','spread_put','spread_call'};'deal_status',{'live'}}; %% deal_status
    InputScenarios = [{'on',5,2,'Percentage Move'};{'on',3,2,''};{'off',3,10,''}];
    SummmaryParameters = {'subportfolio','product_code','instrument','contract_month','strike','derivative_type'};
    PricingType = 'settle';
    OutputFields = {'active_lots','price','pricemove','vol','volmove','m2m_usd','delta','gamma','vega','theta'};
    
    Report_Name = strcat('OFUT_Sensitivity_',char(InBUName));
    
elseif(strcmpi(Check_Str,'OTC'))
    InBUName = 'grn';
    InputFilterValues = {'subportfolio',{'GR-COMPO','GR-GENEVA-COMPO','GR-OTC-GENEVA','GR-OTC-GSB','GR-OTC-GSM','GR-OTC-GT'};...
        'deal_status',{'live'}}; %% deal_status
    InputScenarios = [{'on',3,2,'Percentage Move'};{'off',3,2,''};{'off',3,10,''}];
    SummmaryParameters = {'subportfolio','product_code','instrument','contract_month','strike'};
    PricingType = 'settle';
    OutputFields = {'active_lots','price','pricemove','m2m_usd','delta','gamma','vega','theta'}; %% 'vol','volmove'
    
    Report_Name = strcat('OTC_Sensitivity_',char(InBUName));
end

%% generate_sensitivity_report

tic;
try
    [OutErrorMsg,OutFilename] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields,Report_Name);
    
    msgbox(['Without Parfor : ', num2str(toc) ]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

%% generate_sensitivity_report_parfor

% parpool;
% tic;
% try
%     [OutErrorMsg,OutFilename] = generate_sensitivity_report_parfor(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
%
%     msgbox(['With Parfor : ', num2str(toc) ]);
%
% catch ME
%     OutErrorMsg = cellstr(ME.message);
%
%     errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
%         ME.stack(1).name, ME.stack(1).line, ME.message);
%     fprintf(1, '%s\n', errorMessage);
%     %     uiwait(warndlg(errorMessage));
%
% end
%
% delete(gcp('nocreate'));





