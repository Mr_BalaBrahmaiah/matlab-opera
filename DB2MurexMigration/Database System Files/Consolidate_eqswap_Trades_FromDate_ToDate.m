function [OutErrorMsg,OutFilename] =  Consolidate_eqswap_Trades_FromDate_ToDate(InBUName,FromDate,ToDate)

% InBUName = 'qf5';
% FromDate = '2017-08-01';
% ToDate = '2017-08-31';

OutErrorMsg = {'No errors'};
OutFilename = '';

ObjDB = connect_to_database;

try
    
    %% Validate FromDate & ToDate
    
    FromDate = char(FromDate);
    ToDate = char(ToDate);
    
    HolDates = {'2016-01-01','2017-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    DateArray = cellstr( datestr(busdays(FromDate,ToDate,'daily',HolidayVec),'yyyy-mm-dd') );
    
    NoData_For_SettleDate = {'NoData_For_SettleDate'};
    Overall_ReportData = [];
    
    for i = 1 : size(DateArray,1)
        
        Settle_Date = DateArray(i) ;
        
        %% Fetch Data From DB
        
        if(strcmpi(InBUName,'qf5'))
            
            %  Settle_Date =  fetch(ObjDB,'select settlement_date from valuation_date_table'); %% {'2017-09-07'};
            TradeGroup_Type = 'EQD_SWAP';
            Supportfolio_Like = 'RES-%';
            SqlQuery_1 =  ['select * from deal_ticket_table_with_latest_versions_view_qf5 where transaction_date =''',char(Settle_Date),''' and trade_group_type = ''',TradeGroup_Type,''' and subportfolio like ''',Supportfolio_Like,''' '];
            [DealTiket_ColNames,DealTicket_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
            if(strcmpi(DealTicket_Data,'No Data'))
                % OutErrorMsg = {'No Data for Corresponding Settle_Date in deal_ticket_table_with_latest_versions_view_qf5 Check DB'};
                NoData_For_SettleDate = [NoData_For_SettleDate ;{['No Data for Corresponding Settle_Date = ',char(Settle_Date),' in deal_ticket_table_with_latest_versions_view_qf5 Check DB']} ];
                continue;
            end
            
            
            spot_id = 'CR USDCNH Spot BGN';
            % Value_Date =  fetch(ObjDB,'select value_date from valuation_date_table'); %% {'2017-09-08'};
            % SqlQuery_2 = ['select traded_spot_rate from equity_swaps_spot_table_qf5 where value_Date = ''',char(Value_Date),''' and spot_id = ''',spot_id,''' '];
            SqlQuery_2 = ['select counterparty_parent,spot_id,traded_spot_rate from equity_swaps_spot_table_qf5 where settlement_date = ''',char(Settle_Date),'''  ']; %% and spot_id = ''',spot_id,'''
            [Spot_ColNames,Spot_Data] = Fetch_DB_Data(ObjDB,SqlQuery_2);
            if(strcmpi(Spot_Data,'No Data'))
                %  msgbox(['No Spot_Data for Corresponding value_date and Spot_ID' ,char(Value_Date),{''},spot_id ],'Check DB') ;
                OutErrorMsg = {['No Spot_Data for Corresponding settlement_date and Spot_ID ' ,char(Settle_Date),' ',spot_id ]};
                return;
            end
            
            try
                SqlQuery_Currency_Map_Table = 'select * from equity_currency_mapping_table_qf5';
                [Currency_MapTbl_ColNames,Currency_MapTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Currency_Map_Table);
                Currency_Map_Tbl = cell2dataset([Currency_MapTbl_ColNames;Currency_MapTbl_Data]);
            catch
                OutErrorMsg = {'Error Occured when Fetching equity_currency_mapping_table_qf5'};
                return;
            end
            %% CoporateAction Recall Trades
            
            TraderName_Col = cellStrfind_exact(DealTiket_ColNames,{'trader_name'});
            CorporateAction_Recall_Index = cellStrfind_exact(DealTicket_Data(:,TraderName_Col),{'CorporateAction','Recall'});
            CorporateAction_Reacall_Data = DealTicket_Data(CorporateAction_Recall_Index,:);
            DealTicket_Data(CorporateAction_Recall_Index,:) = [];
            
            %% Consolidate Data
            UniqueFields = {'subportfolio', 'instrument', 'security_id','counterparty_parent','equity_swaps_trade_type'}; %% 'trader_name'
            SumFields = {'original_lots','lots_to_be_netted'};
            OutputFields = DealTiket_ColNames ;%% [UniqueFields,SumFields,{'premium'}];
            WeightedAverageFields =  {'premium','original_lots' ; 'premium_nc','original_lots'}; %% {'premium','original_lots'};
            [OutputFields,ReportData] = consolidatedata(DealTiket_ColNames, DealTicket_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            %% Concatenate The CoporateAction Trades
            
            Final_ReportData = [ReportData ; CorporateAction_Reacall_Data];
            
            Instrument_Col = cellStrfind_exact(OutputFields,{'instrument'});
            [~,StockType ]= strtok(Final_ReportData(:,Instrument_Col),'.');
            StockType = strrep(StockType,'.','');
            CounterParty_Col = cellStrfind_exact(OutputFields,{'counterparty_parent'});
            Premium_Col = cellStrfind_exact(OutputFields,{'premium'});
            
            CounterpartyParent_Col = cellStrfind_exact(Spot_ColNames,{'counterparty_parent'});
            SpotID_Col = cellStrfind_exact(Spot_ColNames,{'spot_id'});
            TradeSpot_SpotRate_Col = cellStrfind_exact(Spot_ColNames,{'traded_spot_rate'});
            
            %% Calculation
            
            for ii = 1 : size(Spot_Data,1)
                
                Current_Counterparty = Spot_Data(ii,CounterpartyParent_Col);
                Current_Spot_ID = Spot_Data(ii,SpotID_Col);
                Current_Spot_Data = Spot_Data(ii,TradeSpot_SpotRate_Col);
                
                Split_Str = strsplit(char(Current_Spot_ID),' ');
                USD_Str_Check = char(Split_Str(2));
                USD_Str_Check = USD_Str_Check(1:3);
                
                Current_Currency = unique(Currency_Map_Tbl.currency(strcmpi(Currency_Map_Tbl.spot_id,Current_Spot_ID)));
                Current_StockType = unique(Currency_Map_Tbl.stock_type(strcmpi(Currency_Map_Tbl.spot_id,Current_Spot_ID) & strcmpi(Currency_Map_Tbl.counterparty_parent,Current_Counterparty)));
                
                Current_StockType_Index = cellStrfind_exact(StockType,Current_StockType);
                
                Temp_Final_ReportData = Final_ReportData(Current_StockType_Index,:);
                if(isempty(Temp_Final_ReportData))
                    continue;
                end
                
                CounterParty_MatchedIndex = cellStrfind_exact(Temp_Final_ReportData(:,CounterParty_Col),Current_Counterparty);
                ReportData = Temp_Final_ReportData(CounterParty_MatchedIndex,:);
                CounterParty_MatchedIndex = cellStrfind_exact(Final_ReportData(:,CounterParty_Col),Current_Counterparty);
                ReportData = Final_ReportData(CounterParty_MatchedIndex,:);
                
                if(strcmpi(USD_Str_Check,'USD'))
                    ReportData(:,Premium_Col) = num2cell(round(cell2mat(ReportData(:,Premium_Col)),4) ./ cell2mat(Current_Spot_Data));
                else
                    ReportData(:,Premium_Col) = num2cell(round(cell2mat(ReportData(:,Premium_Col)),4) .* cell2mat(Current_Spot_Data));
                end
                
                %% Remove 'RES-'
                Supportfolio_Col = cellStrfind_exact(OutputFields,{'subportfolio'});
                ReportData(:,Supportfolio_Col) = regexprep(ReportData(:,Supportfolio_Col),'RES-','');
                
                Make_Empty_Cols = cellStrfind_exact(OutputFields,{'version_created_by','version_timestamp','version_comments'});
                ReportData(:,Make_Empty_Cols) = {''};
                
                Make_VersionNo_Zero = cellStrfind_exact(OutputFields,{'version_no'});
                ReportData(:,Make_VersionNo_Zero) = {0};
                
                Premium_Col = cellStrfind_exact(OutputFields,{'premium'});
                ReportData(:,Premium_Col) = num2cell(round(cell2mat(ReportData(:,Premium_Col)),4));
                
                Overall_ReportData = [Overall_ReportData ; ReportData];
                
            end
            
            
        else
            OutErrorMsg = {'This Code not Implemented for Other than QF5'};
        end
        
        
    end
    
    %%
    
    if(isempty(Overall_ReportData))
        OutErrorMsg = {'No Data for Selected Date Range Please Select Appropriate Date Range'} ;
        return;
    end
    
    %% Compute Trade ID
    
    %  [OutErrorMsg,LastTradeId,TradeID]= getLastTradeId_RES_QF5(InBUName,ReportData);
    [TradeID , Last_DBTradeId] = getTradeID_4_Cell_Array(Overall_ReportData,InBUName);
    
    TradeID_Col = cellStrfind_exact(OutputFields,{'trade_id'});
    ParentTradeID_Col = cellStrfind_exact(OutputFields,{'parent_trade_id'});
    Overall_ReportData(:,TradeID_Col) = TradeID;
    Overall_ReportData(:,ParentTradeID_Col) = TradeID;
    
    %% Excel Write
    
    OutFilename = getXLSFilename(['Past_Date_Consolidate_EqSwap_Trades_',char(InBUName),'_',char(datestr(FromDate,1)),'_',char(datestr(ToDate,1))]);
    
    xlswrite(OutFilename,[OutputFields;Overall_ReportData]);
    xlswrite(OutFilename,NoData_For_SettleDate,'NoData_For_SettleDate');
    
    %     OutFilename = fullfile(pwd,OutFilename);
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end