function [OTC_FX_Swaps_check_Data_Output_Total]=format_total_fx_swaps(OTC_FXSwaps_Header,OTC_FXSwaps_Data_Output)

OTC_FX_Swaps_check_Data_Output_Total ='';

try
    
    Fixed_Header_Col_Size = size(OTC_FXSwaps_Header,2);
    
    %% VALUE DATE and FIX DATE
    VALUE_DATE = cellStrfind_exact(OTC_FXSwaps_Header,{'VALUE_DATE'}); %%   Ref_Month_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'VALUE_DATE'})
    FIX_DATE = cellStrfind_exact(OTC_FXSwaps_Header,{'FIX_DATE'});
    
    %% SUM FIELD
    Total_Sum_Required_Col = {'BUY_NOMINAL','SELL_NOMINAL','MTM_NC','MTM_USD'};
    Total_Sum_Required_Col_Index = cellStrfind_exact(OTC_FXSwaps_Header,Total_Sum_Required_Col);
    
    %% AVG SUM FIELDS
    Avg_Sum_Required_Col = {'BUY_PRICE','SELL_PRICE','SETT_PRICE'};
    Avg_Sum_Required_Col_Index = cellStrfind_exact(OTC_FXSwaps_Header,Avg_Sum_Required_Col);
    
    %% BUY PRICE and CELL PRICE Index
    BUY_PRICE=  Avg_Sum_Required_Col_Index(1);
    SELL_PRICE=Avg_Sum_Required_Col_Index(2);

    %% SUM PRICE INDEXES
    BUY_NOMINAL_INDEX = cellStrfind_exact(OTC_FXSwaps_Header,{'BUY_NOMINAL'});
    SELL_NOMINAL_INDEX = cellStrfind_exact(OTC_FXSwaps_Header,{'SELL_NOMINAL'});
    MTM_NC_INDEX = cellStrfind_exact(OTC_FXSwaps_Header,{'MTM_NC'});
    MTM_USD_INDEX=cellStrfind_exact(OTC_FXSwaps_Header,{'MTM_USD'});
    SETT_INDEX=cellStrfind_exact(OTC_FXSwaps_Header,{'SETT_PRICE'});
    %% Value date reference manipulation
    Unique_RefMonth = unique(OTC_FXSwaps_Data_Output.VALUE_DATE,'stable');
    
    %% Data aggregation manipulation
    OTC_FXSwaps_Data_Output_cellarray = dataset2cell(OTC_FXSwaps_Data_Output);
    OTC_FXSwaps_Data_Output_cellarray(1,:)=[];
    
    Total_Overall_ColSum_Array = cell(2,Fixed_Header_Col_Size);
    [~,Overall_ColSum] = cell2sum_Row_Col(OTC_FXSwaps_Data_Output_cellarray(:,Total_Sum_Required_Col_Index));
    Total_Overall_ColSum_Array(2,Total_Sum_Required_Col_Index) = Overall_ColSum ;
    Total_Overall_ColSum_Array(2,1) = cellstr('TOTAL');
    
    
    OTC_FX_Swaps_check_Data_Output_Total = [];
    for i  = 1 : length(Unique_RefMonth)
        Current_RefMonth = Unique_RefMonth(i) ;
        Matched_Ref_Month = strcmpi(OTC_FXSwaps_Data_Output_cellarray(:,VALUE_DATE),Current_RefMonth) ;
        if(~isempty( find(Matched_Ref_Month,1)))
            Matched_Rows = OTC_FXSwaps_Data_Output_cellarray(Matched_Ref_Month,:);
            Num_of_Rows = size(Matched_Rows,1);
            Total_Empty_Array = cell(2,Fixed_Header_Col_Size);
            [~,ColSum] = cell2sum_Row_Col(Matched_Rows(:,Total_Sum_Required_Col_Index));
            Total_Empty_Array(2,Total_Sum_Required_Col_Index) = ColSum ;
            Total_Empty_Array(2,5) =  cellstr('TOTAL'); % strcat(Current_RefMonth,' TOTAL');
            
            %% Here if karthiks requirement will come you have to chnage the things.
            % Total_Empty_Array(2,BUY_NOMINAL_INDEX) = num2cell( nansum(cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX)) .*  cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX))) ./ cell2mat(Total_Empty_Array(2,BUY_NOMINAL_INDEX)) );
            Total_Empty_Array(2,BUY_NOMINAL_INDEX) = num2cell(nansum(cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX))));
            % rajashekhar added for data
            Total_Empty_Array(2,SETT_INDEX) = num2cell(max(cell2mat(Matched_Rows(:,SETT_INDEX))));
            %Total_Empty_Array(2,BUY_NOMINAL_INDEX) = num2cell( nansum(cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX)) .*  cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX))) ./ cell2mat(Total_Empty_Array(2,SELL_NOMINAL_INDEX)) );
            %Total_Empty_Array(2,SELL_NOMINAL_INDEX) = num2cell(cell2mat(Total_Empty_Array(2,SELL_NOMINAL_INDEX))./Num_of_Rows);
            Total_Empty_Array(2,SELL_NOMINAL_INDEX) = num2cell(nansum(cell2mat(Matched_Rows(:,SELL_NOMINAL_INDEX))));
            Total_Empty_Array(2,BUY_PRICE) = num2cell( nansum(cell2mat(Matched_Rows(:,BUY_PRICE)) .*  cell2mat(Matched_Rows(:,BUY_NOMINAL_INDEX))) ./ cell2mat(Total_Empty_Array(2,BUY_NOMINAL_INDEX)) );
            Total_Empty_Array(2,SELL_PRICE) = num2cell( nansum(cell2mat(Matched_Rows(:,SELL_PRICE)) .*  cell2mat(Matched_Rows(:,SELL_NOMINAL_INDEX))) ./ cell2mat(Total_Empty_Array(2,SELL_NOMINAL_INDEX)) );
            OTC_FX_Swaps_check_Data_Output_Total = [OTC_FX_Swaps_check_Data_Output_Total ; [Matched_Rows ; Total_Empty_Array] ] ;
            
        end
    end
    OTC_FX_Swaps_check_Data_Output_Total = [OTC_FX_Swaps_check_Data_Output_Total ; Total_Overall_ColSum_Array];
    %OTC_FX_Swaps_check_Data_Output_Total(cellfun(@isnan,OTC_FX_Swaps_check_Data_Output_Total),'UniformOutput',false)='-';
     OTC_FX_Swaps_check_Data_Output_Total(cell2mat(cellfun(@(x)any(isnan(x)),OTC_FX_Swaps_check_Data_Output_Total,'UniformOutput',false))) = {[]};
    OTC_FX_Swaps_check_Data_Output_Total = cell2dataset([OTC_FXSwaps_Header ; OTC_FX_Swaps_check_Data_Output_Total]) ;
    
    
catch ME
     OTC_FX_Swaps_check_Data_Output_Total='';
     
end
end


