function [OutErrorMsg,OutFilename] = generate_mo_fxforwards_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    Filename = ['FXForwards_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    if ~strcmpi(Data,'No Data')
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FXD'});
        Data = Data(ispresent_FUT_OFUT,:);
        
        if ~isempty(Data)
            ObjDB = connect_to_database;
            InData = cell2dataset([ColNames;Data]);
            
            IdxBlankContractMonth = strcmpi('',InData.contract_month);
            InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
            
            [OutHeader,OutData] = compute_monthend_mtm_values(InBUName,InData,1);
            
            UniqueFields = {'ASSET_CLASS','PRODUCT_CODE','PRODUCT','PORTFOLIO','CONTRACT_MONTH','CURRENCY'};
            SumFields = {'Total_USD','Total_NC','ActiveLots'};
            OutputFields = [UniqueFields,SumFields,'SettlePrice'];
            WeightedAverageFields = [];
            [OutputFields,ConsDeals] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            TempOutData = cell2dataset([OutputFields;ConsDeals]);
            
            TableName = 'currency_spot_mapping_table';
            [~,Data] = read_from_database(TableName,0);
            DBCurrency = Data(:,1);
            DBSpot = Data(:,2);
            
            ValueDate = fetch(ObjDB, 'select value_date from valuation_date_table');
            
            SpotRate = cell(size(TempOutData.CURRENCY,1),1);
            for iCurr = 1:length(DBCurrency)
                IdxCurr = strcmpi(DBCurrency{iCurr},TempOutData.CURRENCY);
                SqlQuery = ['select settle_value from underlying_settle_value_table where value_date  = ''',char(ValueDate),''' and underlying_id = ''',DBSpot{iCurr},''''];
                SpotRate(IdxCurr) = fetch(ObjDB,SqlQuery);
            end
            
            TempOutData.SpotRate = SpotRate;
            TempOutData.CURRENCY = []; TempOutData = dataset2cell(TempOutData);
            
            OutHeader = {'Type','Product Code','Product','Portfolio','Maturity','Book_Value_USD','Book_Value_NC','Notional','FX Forward Rate','Spot FX Rate'};
            
            OutData = TempOutData(2:end,:);
            
            xlswrite(OutFilename,[OutHeader;OutData]);
        else
            OutErrorMsg = {'No data found'};
        end
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
%     fprintf(1, '%s\n', errorMessage);
end