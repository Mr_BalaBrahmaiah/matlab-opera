function Main_OTC_Filler_Central_Books(InBUName)

%%
TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
    'OptType','Prem','CounterParty','Initial Lots',...
    'CONTRACTNB','Maturity','Barrier Type',...
    'Barrier','Transaction Date','TRN.Number','Source',...
    'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};

ObjDB = connect_to_database ;

[~,TRHeader] = Fetch_DB_Data(ObjDB,'select column_name from tr_dump_column_format_table order by column_number asc');
TRHeader = TRHeader';

XLS_FileName = strcat('OTC_Filler_Central_Books_',InBUName);
OutXLSFileName = getXLSFilename(XLS_FileName);

%% Get & Read Input Excel
[Input_Filename, Input_Pathname] = uigetfile( ...
    {'*.xlsx';'*.xls'}, ...
    'Pick a Input File');

if isnumeric(Input_Filename) && Input_Filename==0
    errordlg(['You did not selected the Input File Please Select Input File',{''},'Input File is mandatory']);
    return;
else
    
    InXLSFilename = fullfile(Input_Pathname,Input_Filename);
    
    % InXLSFilename = 'D:\00_MUTHUKUMAR\WORKS\00_Office\OTC Filler\RMS Deal Ticket template.xlsx';
    [~,~,RawData] = xlsread(InXLSFilename,'Trade_Template');
    
    RawData_Header = strtrim(RawData(1,:));
    RawData_Header = strtrim(RawData_Header);
    RawData = RawData(2:end,:);
    
    HolidayExcahnge_Prod_Col = cellStrfind_exact(RawData_Header,{'Product','Calendar_days'});
    HolidayExcahnge_Data = RawData(:,HolidayExcahnge_Prod_Col);
    
    if(sum( cellfun(@(V) any(isnan(V(:))), HolidayExcahnge_Data(:,2)) ))
        errordlg(['Holiday_Exchange Column is Should not Empty',{''},' Please Fill the appropriate Fields']);
        return;
    end
    
    %% Get & Read Holiday Exchange
    
    [~,~,Holiday_RawData] = xlsread(InXLSFilename,'Holidays');
    
    Nan_Col =  cellfun(@(V) any(isnan(V(:))), Holiday_RawData(1,:));
    Holiday_RawData(:,Nan_Col(:)) = [];
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Holiday_RawData(:,1));
    Holiday_RawData(Nan_Row(:),:) = [];
    
    Holiday_RawData_Header = Holiday_RawData(1,:);
    Holiday_RawData = Holiday_RawData(2:end,:);
    
    %% Check Input Sheet & Holiday Sheet Combination
    [InputUnique,~,~] = uniqueRowsCA(HolidayExcahnge_Data) ; %% Input File ProdCode & Exchange Unique
    [HolidayUnique,~,~] = uniqueRowsCA(Holiday_RawData(:,2:3)) ; %% Holiday Sheet ProdCode & Exchange Unique
    
    InputUnique = sortrows(InputUnique);
    HolidayUnique = sortrows(HolidayUnique);
    
    Unmatched_Product = setdiff(upper(InputUnique(:,1)),upper(HolidayUnique(:,1))) ;
    Unmatched_Exchange = setdiff(upper(InputUnique(:,2)),upper(HolidayUnique(:,2))) ;
    
    if(~strcmpi(Unmatched_Exchange,'all'))        %% ~strcmpi(Unmatched_Exchange,'all')
   
        if(~isempty(Unmatched_Product) || ~isempty(Unmatched_Exchange))
            Combination_Str = strcat(Unmatched_Product,{' and '},Unmatched_Exchange);
            errordlg([char(Combination_Str),{''},' Combination Does not Exist in Holiday List']);
        end
    else
        Unmatched_Product = [];
        Unmatched_Exchange = [];
    end
    
    %% Get Matched Input Data
    if(~isempty(Unmatched_Product) && ~isempty(Unmatched_Exchange))
        UnMatched_ProductExchange_Index =  strcmpi(HolidayExcahnge_Data(:,1),Unmatched_Product) & strcmpi(HolidayExcahnge_Data(:,2),Unmatched_Exchange) ;
        
        RawData(UnMatched_ProductExchange_Index,:) = [];
        HolidayExcahnge_Data = RawData(:,HolidayExcahnge_Prod_Col);
    end
    
end

%% Validate the Input File

Default_Header = {'Portfolio','Product','Lots','Month','Strike','derivative_type','Prem/Rate','Exe-Broker','Clearer/CP','Daily/EOD',...
    'Barrier','Start','Expiry','Contract Number','Other Remarks','Trader Name','Traded DateTimestamp','Calendar_days'};

if(length(Default_Header) == length(RawData_Header))
    
    Compare_Header = strcmpi(Default_Header,RawData_Header) ;
    if(find(Compare_Header == 0))
        Not_Match_Header =  char(RawData_Header(find(Compare_Header == 0))) ;
        Error_Str  = ['Input File Header does not Matched with Template Header : ',Not_Match_Header];
        errordlg(Error_Str);
        return;
    end
    
else
    Not_Match_Header = char(setdiff(RawData_Header,Default_Header));
    if(isempty(Not_Match_Header))
        Not_Match_Header = char(setdiff(Default_Header,RawData_Header));
    end
    Error_Str = ['Input File Header length does not Matched with Template Header length : ', Not_Match_Header];
    errordlg(Error_Str);
    return;
end

%% Empty and NaN Filed Checking
UserWant_Header = {'Portfolio','Product','Lots','Month','Strike','derivative_type','Prem/Rate','Clearer/CP','Daily/EOD','Start','Contract Number'};
Matched_UserWant_Col = cellStrfind_exact(RawData_Header,UserWant_Header);
UserWant_Data = RawData(:,Matched_UserWant_Col);

if(sum(sum(cellfun(@(V) any(isempty(V(:))), UserWant_Data))))
    Empty_Str_Col = find( sum(cellfun(@(V) any(isempty(V(:))), UserWant_Data)) );
    Error_Str = UserWant_Header(Empty_Str_Col) ;
    errordlg(Error_Str , 'Empty Field Columns') ;
    return;
end

if(sum(sum(cellfun(@(V) any(isnan(V(:))), UserWant_Data))))
    Empty_Str_Col = find( sum(cellfun(@(V) any(isnan(V(:))), UserWant_Data)) );
    Error_Str = UserWant_Header(Empty_Str_Col) ;
    errordlg(Error_Str , 'NaN Field Columns') ;
    return;
end

%% Get 'Instrument'    'Subportfolio'    'Strike Factor'    'Premium'  Informations

Default_TableName = 'subportfolio_product_mapping_table_';
% InBUName = 'cot';
Table_Name =  strcat(Default_TableName,InBUName); %% 'subportfolio_product_mapping_table' ; %% strcat(Default_TableName,InBUName) ;

Portfolio_Col = find(cellfun(@(V) strcmpi('Portfolio',V), RawData_Header));
Product_Col = find(cellfun(@(V) strcmpi('Product',V), RawData_Header));

Modify_Map_Data = cell(size(RawData,1),7);
Modify_Map_Data(:,1) = {InBUName};

for i = 1 : size(RawData,1)
    
    Current_Portfolio = char(RawData(i,Portfolio_Col));
    Current_Product = char(RawData(i,Product_Col)) ; %% char(strcat({'CM '},RawData(i,Product_Col)));
    
    SqlQuery = ['SELECT product_code,portfolio,instrument,subportfolio,strike_factor,premium_factor FROM ',Table_Name,' where subportfolio = ''' Current_Portfolio ''' and product_code = ''' Current_Product ''' ' ] ;
    
    [Map_ProdTbl_Fileds,Map_ProdTbl_Data] = read_from_database(0,0,SqlQuery);
    
    Modify_Map_Data(i,2:end) = Map_ProdTbl_Data ;
end

Map_ProductData_Header = {'PortfolioType','Product','Portfolio','Instrument','Subportfolio','Strike Factor','Premium'};
Modify_Map_Data(:,2) = strtrim(Modify_Map_Data(:,2)); %% Remove Spaces
Map_ProductData = Modify_Map_Data;


%% Get Portfolio and Instrument for Input File

Instrument_Portfolio_Data = cell(size(RawData,1),2);
Instrument_Portfolio_Data(:,:) = {' '};

Strike_Factor = cell(size(RawData,1),1);
Premium_Factor = cell(size(RawData,1),1);

for i = 1 : size(RawData,1)
    try
        Raw_PortfolioType = strtrim(RawData{i,1});
        Raw_ProdCode = char(RawData{i,2}) ; %% strtrim(strcat({'CM '},RawData{i,2}));  %%  Add CM before the product Code
    catch
        Raw_PortfolioType = RawData{i,1};
        Raw_ProdCode = char(RawData{i,2}) ; %% strcat({'CM '},RawData{i,2});
    end
    
    MatchedIndex = find(strcmpi(Raw_ProdCode,Map_ProductData(:,2)) & strcmpi(Raw_PortfolioType , Map_ProductData(:,3)));
    
    if(~isempty(MatchedIndex))
        Instrument_Portfolio_Data(MatchedIndex,:) = Map_ProductData(MatchedIndex,4:5);
        Strike_Factor(MatchedIndex,1) = Map_ProductData(MatchedIndex,6);
        Premium_Factor(MatchedIndex,1) = Map_ProductData(MatchedIndex,7);
    end
    
end

%% Get Buy Sell From Lots Field

Lots_Col = find(cellfun(@(V) strcmpi('Lots',V), RawData_Header));  %% Actice Lots ans Initial Lots
Lots_Data = RawData(:,Lots_Col);
Index_Sell = find(cellNumber_2_Number(cellfun(@(x) x<0, Lots_Data, 'UniformOutput', false)));
Index_Buy = find(cellNumber_2_Number(cellfun(@(x) x>0, Lots_Data, 'UniformOutput', false)));

BuySell_Data = cell(length(Lots_Data),1);
BuySell_Data(Index_Sell) = {'Sold'};
BuySell_Data(Index_Buy) = {'Bought'};

%% Get Month
Month_Col = find(cellfun(@(V) strcmpi('Month',V), RawData_Header));
Month_Data = RawData(:,Month_Col);

%% Get Strike and Premium

Strike_Col = find(cellfun(@(V) strcmpi('Strike',V), RawData_Header));
Strike_Data = RawData(:,Strike_Col);

Nan_Row =  cellfun(@(V) any(isnan(V(:))), Strike_Data); %% Nan_Row = find(Nan_Row);
Strike_Data(Nan_Row(:),:) = num2cell(0);
Strike_Data = num2cell (cellNumber_2_Number(Strike_Data) ./ cellNumber_2_Number(Strike_Factor));


Premium_Col = find(cellfun(@(V) strcmpi('Prem/Rate',V), RawData_Header));
Premium_Data = RawData(:,Premium_Col);

Nan_Row =  cellfun(@(V) any(isnan(V(:))), Premium_Data); %% Nan_Row = find(Nan_Row);
Premium_Data(Nan_Row(:),:) = num2cell(0);
Premium_Data = num2cell (cellNumber_2_Number(Premium_Data) ./ cellNumber_2_Number(Premium_Factor));

%% Get Counterparty ,Expiry and CONTRACTNB
CounterParty_Col = find(cellfun(@(V) strcmpi('Clearer/CP',V), RawData_Header)) ;
CounterParty_Data = RawData(:,CounterParty_Col);

CONTRACTNB_Col = find(cellfun(@(V) strcmpi('Contract Number',V), RawData_Header)) ;
CONTRACTNB_Data = RawData(:,CONTRACTNB_Col);

Maturity_Col = find(cellfun(@(V) strcmpi('Expiry',V), RawData_Header)) ;
Maturity_Data = RawData(:,Maturity_Col);

%% Barrier
Barrier_Col = find(cellfun(@(V) strcmpi('Barrier',V), RawData_Header)) ;
Barrier_Data = RawData(:,Barrier_Col);

Nan_Row =  cellfun(@(V) any(isnan(V(:))), Barrier_Data); %% Nan_Row = find(Nan_Row);
Barrier_Data(Nan_Row(:),:) = num2cell(0);
Char_Row = cellfun(@(V) any(ischar(V(:))), Barrier_Data);
Barrier_Data(Char_Row(:),:) = num2cell(0);

Barrier_Data = num2cell (cellNumber_2_Number(Barrier_Data) ./ cellNumber_2_Number(Strike_Factor));

%% Get Exe-Broker , Trader Name , Traded DateTimestamp , Daily/EOD and Other Remarks
ExeBroker_Col = find(cellfun(@(V) strcmpi('Exe-Broker',V), RawData_Header)) ;
ExeBroker_Data = RawData(:,ExeBroker_Col);

Other_Col = find(cellfun(@(V) strcmpi('Other Remarks',V), RawData_Header)) ;
OtherRemarks_Data = RawData(:,Other_Col);

OTC_EOD_Col = find(cellfun(@(V) strcmpi('Daily/EOD',V), RawData_Header)) ;
OTC_EOD_Data = RawData(:,OTC_EOD_Col);

TraderName_Col = find(cellfun(@(V) strcmpi('Trader Name',V), RawData_Header)) ;
TraderName_Data = RawData(:,TraderName_Col);

DateTime_Col = find(cellfun(@(V) strcmpi('Traded DateTimestamp',V), RawData_Header)) ;
DateTime_Data = RawData(:,DateTime_Col);

%% Transaction Date

TransactionDate_Col = find(cellfun(@(V) strcmpi('Start',V), RawData_Header)) ;
TransactionDate_Data = RawData(:,TransactionDate_Col);

% SqlQuery = 'select settlement_date from valuation_date_table';
% [~,Settlement_Date] = read_from_database('valuation_date_table',0,SqlQuery);
%
% TransactionDate_Data  = cell(length(Lots_Data),1);
% TransactionDate_Data(:,:) = Settlement_Date;

%% Source
Source_Data  = cell(length(Lots_Data),1);
Source_Data(:,:) = {'OTC-Booking Order'};

%% Opt Type
OptType_Col = find(cellfun(@(V) strcmpi('derivative_type',V), RawData_Header)) ; %% 'Put/Call/Fut'
OptType_Data = RawData(:,OptType_Col);

%% Make Temporary RowData
Temp_Data = cell(length(Lots_Data),1);
Temp_Data_NULL = cell(length(Lots_Data),1);
Temp_Data(:,:) = num2cell(NaN);
Temp_Data_NULL(:,:) = {'NULL'};

RowData = [Instrument_Portfolio_Data(:,1),Instrument_Portfolio_Data(:,2),BuySell_Data,Month_Data,Temp_Data,Strike_Data,OptType_Data,Premium_Data,...
    CounterParty_Data,Lots_Data,CONTRACTNB_Data,Maturity_Data,Temp_Data,Barrier_Data,TransactionDate_Data,Temp_Data,Source_Data,Temp_Data,Temp_Data,...
    Temp_Data,ExeBroker_Data,Temp_Data,Temp_Data,Temp_Data,OtherRemarks_Data,OTC_EOD_Data,TraderName_Data,DateTime_Data,Temp_Data_NULL,Temp_Data_NULL,Temp_Data_NULL,Temp_Data_NULL];

%% Checking OptType and OTC_EOD
Total_Data = [];
Error_Data = [];  %% StartDate <= ExpiryDate

OptType_Default = {'Future'};   %% {'Future','Put','Call'}; %% For make some calculation other  than three

[System_Date_Format,System_Date] = get_System_Date_Format();
% msgbox(System_Date_Format,'Date Format');

if(~isempty(OTC_EOD_Data))
    for i = 1 : size(RowData,1)
        
        Current_OptType = RowData(i,6);   %% OptType Col
        Matched_Index = cellStrfind_exact(OptType_Default,Current_OptType); %% For make some calculation other  than three
        if(Matched_Index)
            Total_Data = [Total_Data ; RowData(i,:)];
        else
            
            if(strcmpi(OTC_EOD_Data{i},'Daily'))
                StartDate = TransactionDate_Data{i};
                ExpiryDate = Maturity_Data{i};
                
                if( datenum(StartDate,System_Date_Format) > datenum(ExpiryDate,System_Date_Format) )  %% datenum(StartDate,'dd-mm-yyyy') > datenum(ExpiryDate,'dd-mm-yyyy')
                    Error_Data = [Error_Data ; RowData(i,:)];      %% Error Sheet (StartDate <= ExpiryDate)
                    continue;
                end
                
                if(strcmpi(HolidayExcahnge_Data(i,2),'All'))
                    HolDates = {'2016-01-01','2017-01-01'};
                    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
                else
                    Date_Col = cellStrfind_exact(Holiday_RawData_Header,{'date'});
                    HolDates = Holiday_RawData(strcmpi(Holiday_RawData(:,cellStrfind_exact(Holiday_RawData_Header,{'product'})),HolidayExcahnge_Data(i,1)) & strcmpi(Holiday_RawData(:,cellStrfind_exact(Holiday_RawData_Header,{'exchange'})),HolidayExcahnge_Data(i,2)) ,Date_Col);
                    HolidayVec = datenum(HolDates,System_Date_Format) ;
                end
                
                try
                    VBusDays = busdays(datenum(StartDate,System_Date_Format), datenum(ExpiryDate,System_Date_Format),'daily',HolidayVec); %% Where Start and Expiry are the input column names
                catch
                    VBusDays = busdays(datenum(StartDate,System_Date_Format), datenum(ExpiryDate,System_Date_Format),'daily',HolidayVec);
                end
                
                Maturity = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
                
                Temp = cell(length(Maturity),size(RowData,2));
                %             Temp(:,:) =  RowData(i,:);
                
                for ii = 1 : length(Maturity)
                    
                    Temp(ii,:) = RowData(i,:);
                end
                
                Temp(:,12) = Maturity;          %% Change Maturity Date to Business Date
                Total_Data = [Total_Data ; Temp];
                
            else
                
                Total_Data = [Total_Data ; RowData(i,:)];              %% EOD Line
                
            end
        end
    end
    
    %% Except Future, Instrument Name Should be Add '-O'
    PosOptType = strcmpi('OptType',TRHeader);
    PosInstrument = strcmpi('Instrument',TRHeader);
    OptType_Data = Total_Data(:,PosOptType);
    
    Except_FUT_OptType = find(~strcmpi('future',OptType_Data));
    Total_Data(Except_FUT_OptType,PosInstrument) = strcat(Total_Data(Except_FUT_OptType,PosInstrument),'-O');
    
    %%
    % xlswrite(OutXLSFileName , [RawData_Header;RawData] , 'INPUT');
    xlswrite(OutXLSFileName , [TRHeader ; Total_Data],'OTC TR Format');
    xlswrite(OutXLSFileName , [TRHeader ; Error_Data],'Error Sheet');
    
    %% Delete Empty Excel Sheet
    SheetName = 'OTC TR Format';
    try
        xls_delete_sheets([pwd filesep OutXLSFileName]);
        xls_change_activesheet([pwd filesep OutXLSFileName],SheetName);
    catch
        
    end
    
    
else
    
    msgbox('Daily EOD Column is Empty','Finish');
end

%%
msgbox(OutXLSFileName,'Finish');



