%%[?25-?09-?2019 10:11]  Rajashekhar B R:  
function [ fut_month, OutErrorMsg ] = fut_refmonth(  contract_month)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
try
  [~,seconf_part]=strtok(contract_month{1},'.');
  seconf_part=strrep(seconf_part,'.','');
  [month]= moth_mapping_opera(seconf_part(1));
  if length(seconf_part)==3
   fut_month=[month '-' seconf_part(2:end)];
  elseif length(seconf_part)==2
   fut_month=[month '-' '1' seconf_part(2:end)];
  end
  OutErrorMsg ={''};
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end 
 
