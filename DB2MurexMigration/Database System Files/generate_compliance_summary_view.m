function [OutErrorMsg,OutFilename] = generate_compliance_summary_view(InBUName)

OutErrorMsg = {'No errors'};

try    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
ViewName = 'helper_settle_pricing_subportfoliovalues_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

UniqueFields = {'subportfolio','product_code','counterparty','contract_month','derivative_type'};
SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11',...
             'settle_gamma_12','settle_gamma_21','settle_gamma_22',...
             'settle_vega_1','settle_vega_2','settle_theta','mtm_usd','mtm_nc'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

OutFilename = getXLSFilename('Compliance_summary_view');
xlswrite(OutFilename,[OutputFields;OutData]);

catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end