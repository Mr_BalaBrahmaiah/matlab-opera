function [OutErrorMsg,OutFileName] = generate_report_user_required_format(Header,RowData,OutFileName,OutFileFormat)

load('Traders_Report_Summary.mat')

% Header - Output Header
% RowData - OutData

OutErrorMsg = {'No Errors'} ;
OutFileName = '';
try
    
    
    %% Initialization
    if( ~exist('OutFileName','var'))
        OutFileName = 'Output';
        DateTimeStamp = strrep(strrep(datestr(now,0),' ','_'),':','_');
        OutFileName = [OutFileName,'_' ,DateTimeStamp];
    elseif(isempty(OutFileName))
        OutFileName = 'Output';
        DateTimeStamp = strrep(strrep(datestr(now,0),' ','_'),':','_');
        OutFileName = [OutFileName,'_' ,DateTimeStamp];
    else
    end
    
    if( ~exist('OutFileFormat','var'))
        OutFileFormat = 'pdf';
    elseif(isempty(OutFileFormat))
        OutFileFormat = 'pdf';
    else
        
    end
    
    %%
    if(isempty(Header))
        Header = RowData(1,:);
        RowData(1,:) =  [];
    end
    
    %% Seperate Data Type
    [Row,Col] =  size(RowData);
    
    Char_DataType_Col_Index = [];
    Double_DataType_Col_Index = [];
    for i = 1 : Col
        
        if(strcmpi(class(RowData{1,i}),'char'))
            Char_DataType_Col_Index = [Char_DataType_Col_Index ; i];
        elseif(strcmpi(class(RowData{1,i}),'double'))
            Double_DataType_Col_Index = [Double_DataType_Col_Index ; i];
        else
            
        end
    end
    
    %%  To attain proper data type for round off
    
    RowData(:,Double_DataType_Col_Index) = num2cell( round(cell2mat(RowData(:,Double_DataType_Col_Index))) );
    
    %% Concatenation for required output
    Required_Data = [Header;RowData];
    
    %% Generate PDF
    import mlreportgen.report.*;
    import mlreportgen.dom.*;
    
    d=Document(OutFileName,OutFileFormat); % Document Method in REport Dom Libraray
    
    table = Table(Required_Data);
    table.Style = {Border('single','black','1px'), ...
        ColSep('single','black','1px'), ...
        RowSep('single','black','1px'),FontSize('11pt'),FontFamily('Calibri')};
    
    table.Width = '100%';
    row_two = row(table,1);
    row_two.Style = {BackgroundColor('magenta')};
    table_col_specs_grp = TableColSpecGroup();
    specs(1) = TableColSpec;
    specs(1).Style = {BackgroundColor('yellow')};
    table_col_specs_grp.ColSpecs = specs;
    table.ColSpecGroups = table_col_specs_grp;
    append(d,table);
    
    %%
    
    %     for i = 1 : table.NCols
    %         table_col_specs_grp = TableColSpecGroup();
    %         if(mod(i,2)==1)
    %             specs(i) = TableColSpec;
    %             specs(i).Style = {BackgroundColor('yellow')};
    %
    %         else
    %             specs(i) = TableColSpec;
    %             specs(i).Style = {BackgroundColor('yellow')};
    %
    %         end
    %
    %     end
    %      table_col_specs_grp.ColSpecs = specs;
    %             table.ColSpecGroups = table_col_specs_grp;
    %     append(d,table);
    
    %%
    close(d);
    
    rptview(d);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    OutFileName = '';
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end



