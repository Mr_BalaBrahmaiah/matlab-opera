HolDates = {'2015-04-03';'2015-05-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2016-02-01'), datenum('2016-02-12'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

[ColNames,Data] = read_from_database('var_product_code_mapping_table',0);
ProductCodeMapping = cell2dataset([ColNames;Data]);

objDB = connect_to_database;

for i = 1:length(VBusDays)
%     SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
%     curs = exec(objDB,SqlQuery);
%     if ~isempty(curs.Message)
%         errordlg(curs.Message);
%     end
%     SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
%     curs = exec(objDB,SqlQuery);
%     if ~isempty(curs.Message)
%         errordlg(curs.Message);
%     end
    
    FutureColNames = {'underlying_id','expiry_date','invenio_product_code'};
    FutData = fetch(objDB,'select * from helper_get_underlying_id_for_var');
    FutureData = cell2dataset([FutureColNames;FutData]);
    
    OptionColNames = {'vol_id','expiry_date','underlying_id','invenio_product_code'};
    VolData = fetch(objDB,'select * from helper_get_underlying_id_for_options');
    OptionData = cell2dataset([OptionColNames;VolData]);
    OptionData.invenio_product_code = strtrim(OptionData.invenio_product_code);
    
    IdxProductsNotSelected = ~ismember(FutureData.invenio_product_code,ProductCodeMapping.invenio_product_code);
    FutureData(IdxProductsNotSelected,:) = [];
    IdxProductsNotSelected = ~ismember(OptionData.invenio_product_code,ProductCodeMapping.invenio_product_code);
    OptionData(IdxProductsNotSelected,:) = [];
    
    % identify the option months for the serial months
    RefMaturity = datenum(OptionData.expiry_date,'yyyy-mm-dd');
    UndFutId = unique(OptionData.underlying_id);
    UnderlyingVolId = OptionData.vol_id;
    UnderlyingVolExpiryDate = OptionData.expiry_date;
    
    for iFut = 1:length(UndFutId)
        IdxFutId = find(strcmpi(UndFutId{iFut},OptionData.underlying_id));
        [~,Indx] = max(RefMaturity(IdxFutId));
        UnderlyingVolMonth = OptionData.vol_id(IdxFutId(Indx));
        UnderlyingVolExpiry = OptionData.expiry_date(IdxFutId(Indx));
        UnderlyingVolId(IdxFutId) = UnderlyingVolMonth;
        UnderlyingVolExpiryDate(IdxFutId) = UnderlyingVolExpiry;
    end
    
    FutureContinuationSeries = cell(size(FutureData.invenio_product_code));
    SettleDateUnderlyingId   = cell(size(FutureData.invenio_product_code));
    
    OptionContinuationSeries = cell(size(OptionData.invenio_product_code));
    SettleDateVolId   = cell(size(OptionData.invenio_product_code));
    UnderlyingVolIdContSeries   = cell(size(OptionData.invenio_product_code));
    
    % identify the future and option continuation series for this settlement date
    Products = unique(FutureData.invenio_product_code);
    for iProd = 1:length(Products)
        IdxMapping = strcmpi(Products{iProd},ProductCodeMapping.invenio_product_code);
        if isempty(find(IdxMapping))
            continue;
        end
        ContMonthFormatProduct = ProductCodeMapping.cont_month_format_code{IdxMapping};
        
        IdxProduct = strcmpi(Products{iProd},FutureData.invenio_product_code);
        
        % identify the future continuation series; used with price_change
        % data
        TempContracts = FutureData.underlying_id(IdxProduct);
        TempMaturity = FutureData.expiry_date(IdxProduct);
        
        %         [A,IA,IC] = unique(TempMaturity,'sorted');
        [A,IA] = sort(TempMaturity);
        FutureContracts = TempContracts(IA);
        
        TempCSeries = (1:length(FutureContracts))';
        
        CSeries = cell(size(TempCSeries));
        for iFut=1:length(TempCSeries)
            CSeries{iFut} = [ContMonthFormatProduct,num2str(TempCSeries(iFut))];
        end
        for iContract = 1:length(FutureContracts)
            IdxContract = IdxProduct & strcmpi(FutureContracts{iContract},FutureData.underlying_id);
            FutureContinuationSeries(IdxContract) = CSeries(iContract);
            SettleDateUnderlyingId(IdxContract) = SDates(i);
        end
        
        IdxProduct = strcmpi(Products{iProd},OptionData.invenio_product_code);
        
        % to identify the option continuation series; which is needed to use the vol_change data
        TempContracts = OptionData.vol_id(IdxProduct);
        TempMaturity = OptionData.expiry_date(IdxProduct);
        
        %         [A,IA,IC] = unique(TempMaturity,'sorted');
        [A,IA] = sort(TempMaturity);
        FutureContracts = TempContracts(IA);
        
        TempCSeries = (1:length(FutureContracts))';
        
        CSeries = cell(size(TempCSeries));
        for iOpt=1:length(TempCSeries)
            CSeries{iOpt} = [ContMonthFormatProduct,num2str(TempCSeries(iOpt))];
        end
        for iContract = 1:length(FutureContracts)
            IdxContract = IdxProduct & strcmpi(FutureContracts{iContract},OptionData.vol_id);
            OptionContinuationSeries(IdxContract) = CSeries(iContract);
            SettleDateVolId(IdxContract) = SDates(i);
        end
        
        % to identify the option continuation series ignoring the serial
        % months
        TempContracts = UnderlyingVolId(IdxProduct);
        TempMaturity = UnderlyingVolExpiryDate(IdxProduct);
        
        [A,IA,IC] = unique(TempMaturity,'sorted');
        %         [A,IA] = sort(TempMaturity);
        FutureContracts = TempContracts(IA);
        
        TempCSeries = (1:length(FutureContracts))';
        
        CSeries = cell(size(TempCSeries));
        for iOpt=1:length(TempCSeries)
            CSeries{iOpt} = [ContMonthFormatProduct,num2str(TempCSeries(iOpt))];
        end
        for iContract = 1:length(FutureContracts)
            IdxContract = IdxProduct & strcmpi(FutureContracts{iContract},UnderlyingVolId);
            UnderlyingVolIdContSeries(IdxContract) = CSeries(iContract);
        end
        
    end
%     OutFilename = getXLSFilename('future_cont_series_table');
    FutHeader = {'settlement_date','underlying_id','cont_month'};
    OutFutureData = [SettleDateUnderlyingId,FutureData.underlying_id,FutureContinuationSeries];
%     xlswrite(OutFilename,[FutHeader;OutFutureData]);
    fastinsert(objDB, 'future_cont_series_table', FutHeader,OutFutureData);
    
%     OutFilename = getXLSFilename('option_cont_series_table');
    OptHeader = {'settlement_date','vol_id','underlying_vol_id','vol_id_cont_month','underlying_vol_id_cont_month'};
    OutOptionData = [SettleDateVolId,OptionData.vol_id,UnderlyingVolId,OptionContinuationSeries,UnderlyingVolIdContSeries];
%     xlswrite(OutFilename,[OptHeader;OutOptionData]);    
    fastinsert(objDB, 'option_cont_series_table', OptHeader,OutOptionData);
    

end

