
BU_Array = {'cfs','agf','orx','qf1','qf2','qf3','qf4','qf5','cot','coc','cof','grn'};
[Selected_BU, theChosenIDX] = uicellect(BU_Array);

hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');

%%
InBUName = Selected_BU;
try
    if(1)
        % Select File
        tic;
        [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
        InXLSFilename = fullfile(pathname,filename);
        [OutErrorMsg,OutSecurityInfoFilename,OutDealTicketFilename] = deals_security_id_generation(InBUName,InXLSFilename);
        toc;
    else
        % Select Folder
        parpool;
        
        tic;
        Target_Dir = uigetdir;
        Target_Files = dir(Target_Dir);
        
        Target_Files(strcmpi('.',{Target_Files.name})) = [];
        Target_Files(strcmpi('..',{Target_Files.name})) = [];
        Target_Files(strcmpi('Thumbs.db.',{Target_Files.name})) = [];
        
        parfor i = 1 : size(Target_Files,1)
            Current_File = Target_Files(i).name ;
            InXLSFilename = [Target_Dir filesep Current_File];
            
            [OutErrorMsg,OutSecurityInfoFilename,OutDealTicketFilename] = deals_security_id_generation(InBUName,InXLSFilename);
            
        end
        
        toc;
        delete(gcp('nocreate'));
    end
    
    waitbar(1,hWaitbar,'Deal Processing is Finished');msgbox('Process Finished','Finished');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end

