clc
close all
clear


%% Choosing Uploading Folder

TargetDir = uigetdir('\\10.190.7.149\Invenio Database System\DB System Demo\Executables\Madhu\Missing Price\Output files\2014\zz_All_Settle_Price','Choose Uploading Folder');
TargetFiles = dir(TargetDir);

TargetFiles(strcmpi('.',{TargetFiles.name})) = [];
TargetFiles(strcmpi('..',{TargetFiles.name})) = [];
TargetFiles(strcmpi('Thumbs.db',{TargetFiles.name})) = [];

for i = 1 : length(TargetFiles)
    
    Current_XLS_File = [TargetDir filesep TargetFiles(i).name];
    
    [~,~,RawData] = xlsread(Current_XLS_File);
    
    RawData = RawData(:,1:13);
    
    InData = RawData(2:end,:);
    
    %% Change NaN Fileds to NULL in 6 to 13 Columns 
    
    aa = InData(:,6:13);    %% Changing NaN to NULL
    bb = cellfun(@isnan, aa);
    aa(bb) = cellstr('NULL');
    InData(:,6:13) = aa;
    
    %% Find NaN Fileds from Date Column  and Remove NaN Fileds in All Columns   
    
    NaN_Index = cellfun(@(s) any(isnan(s)),InData(:,2));  %% Find NaN Fields From Date Column
    InData(NaN_Index,:) = [];   %% Remove NaN Fileds Row From All Columns because Overcome DATENUM Failed.    
    
    %%
    try
        if(isnumeric(InData{1,2}))
            InData(:,2) = cellstr(datestr(x2mdate(cell2mat(InData(:,2))),'yyyy-mm-dd'));
        else
            InData(:,2) = cellstr(datestr(datenum(InData(:,2),'dd-mm-yyyy'),'yyyy-mm-dd'));
        end
        
        if(isnumeric(InData{1,5}))
            InData(:,5) = cellstr(datestr(x2mdate(cell2mat(InData(:,5))),'yyyy-mm-dd'));
        else
            InData(:,5) = cellstr(datestr(datenum(InData(:,5),'dd-mm-yyyy'),'yyyy-mm-dd'));
        end
        
        Error = upload_in_database_new('settlement_price_table', InData);
        
        if(Error)
            if(~exist([pwd filesep 'zz_Pending']))
                mkdir([pwd filesep 'zz_Pending']);
            end
            Destination = [pwd filesep 'zz_Pending'];
            copyfile(Current_XLS_File,Destination);
            
        end
        
        fprintf('Finished XLSX File : %s : Current Count of File : %d : Total File Count :%d\n',TargetFiles(i).name,i,length(TargetFiles));
        
    catch ME
        Destination = [pwd filesep 'zz_Pending'];
        copyfile(Current_XLS_File,Destination);
        continue;
    end
    
        
end



%%


