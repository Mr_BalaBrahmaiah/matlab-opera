function InterpVol = interpolate_settlement_vol_strike(InputData)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/27 11:30:08 $
%  $Revision: 1.4 $
%

TableName = 'settlement_vol_surf_table';
[ColNames,Data] = read_from_database(TableName,1);
DbData = cell2dataset([ColNames;Data]);

% InterpVol = nan(size(InputData.strike));
InterpVol = InputData.vol;

CallPutID = 'C';    interpolate_data;
CallPutID = 'P';    interpolate_data;

    function interpolate_data
        
        VolId = unique(InputData.vol_id);
        for iVol = 1:length(VolId)
            Index = find(strcmpi(VolId{iVol},InputData.vol_id) & strcmpi(CallPutID,InputData.call_put_id));
            OutStrike = InputData.strike(Index);
            
            DbIndex = find(ismember(upper(DbData.vol_id),upper(VolId{iVol})) & ismember(upper(DbData.call_put_id),upper(CallPutID)));
            if ~isempty(DbIndex) && ~isempty(Index)
                XStrike = DbData.strike(DbIndex);
                YVol =  DbData.settle_vol(DbIndex);
                if (length(DbIndex)==1) % if there is one value available in db, then interp1 cannot be invoked for the single value
                    if abs(XStrike - OutStrike) < 0.000001 % to check if both strikes are equal
                        InterpVol(Index) = YVol;
                    end
                else
                    OutVol = interp1(XStrike,YVol,OutStrike,'linear',NaN);
                    % if the strikes are out of range, then assign the vol
                    % corresponding to nearest strike
                    MinStrike = min(XStrike);
                    MaxStrike = max(XStrike);
                    MinVol = YVol(isnumericequal(XStrike,MinStrike));
                    MaxVol = YVol(isnumericequal(XStrike,MaxStrike));
                    OutVol(OutStrike > MaxStrike) = MaxVol;
                    OutVol(OutStrike < MinStrike) = MinVol;
                    InterpVol(Index) = OutVol;
                end
            end
        end
        
    end
end