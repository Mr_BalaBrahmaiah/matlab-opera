function [OutErrorMsg,OutFilename] = generate_accounting_VAR_report(InBUName)


OutErrorMsg = {'No errors'};
OutFilename = '';
% OutVARFilename  = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    update_future_cont_series_table;
    update_option_cont_series_table;
    
    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    SqlQuery = 'select * from helper_settle_pricing_subportfoliovalues_view where derivative_type not like ''fx%'' and subportfolio not like ''conso%'' and p1_name not like ''CM OR%''';
    [SettleColNames,SData] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    ObjDB = connect_to_database;
    OutFilename = getXLSFilename('VAR_Report');
%     OutVARFilename = getXLSFilename(['VAR_Report_',InBUName]);
    %     store accounting data for var report
    
    UniqueFields = {'value_date','settlement_date','subportfolio','p1_name','contract_month','derivative_type','strike'};
    SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11',...
        'settle_gamma_12','settle_gamma_21','settle_gamma_22',...
        'settle_vega_1','settle_vega_2','settle_theta','mtm-usd','mtm-nc'};
    OutputFields = [UniqueFields,'p1_settleprice',SumFields];
    WeightedAverageFields = [];
    [~,OutData] = consolidatedata(SettleColNames, SData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Delta = cell2mat(OutData(:,9)) +  cell2mat(OutData(:,10));
    Gamma = cell2mat(OutData(:,11)) +  cell2mat(OutData(:,12)) + cell2mat(OutData(:,13)) +  cell2mat(OutData(:,14));
    Vega =  cell2mat(OutData(:,15)) +  cell2mat(OutData(:,16));
    
    AccVARReportData = [OutData(:,1:8),num2cell(Delta),num2cell(Gamma),num2cell(Vega),OutData(:,17:end)];
    
    TableName = ['var_accounting_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, AccVARReportData);
    
    update_curr_mult_table;
    
catch ME
    disp('error occured while uploading var_accounting_table data!');
end

try
    TableName = ['var_aggregation_level_table_',InBUName];
    SqlQuery = ['select distinct(aggregation_level) from ',TableName];
    AggregationLevel = fetch(ObjDB,SqlQuery);
    if (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg')|| strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2'))
        NumDays = 100;%100,252,500
        [~,~,Out100VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
%         xlswrite(OutFilename,Out100VARData,'100Days VAR');
        NumDays = 252;
        [~,~,Out252VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
%         xlswrite(OutFilename,Out252VARData,'252Days VAR');
        NumDays = 500;
        [~,~,Out500VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
%         xlswrite(OutFilename,Out500VARData,'500Days VAR');
        
        %     % calculate the average of 100,252 and 500 days VAR
        OutputHeader = {'value_date','Aggregation_level','VAR'};
        ConsolidatedVARData = [Out100VARData(2:end,:);Out252VARData(2:end,:);Out500VARData(2:end,:)];
        DSConsolidatedVARData = cell2dataset([OutputHeader;ConsolidatedVARData]);
        AggrLevel = unique(DSConsolidatedVARData.Aggregation_level);
        OutVARData = [];
        for i = 1:length(AggrLevel)
            IdxFound = strcmpi(AggrLevel{i},DSConsolidatedVARData.Aggregation_level);
            ValueDate = unique(DSConsolidatedVARData.value_date(IdxFound));
            AvgVAR = mean(DSConsolidatedVARData.VAR(IdxFound));
            RowData = [ValueDate,AggrLevel(i),num2cell(AvgVAR)];
            OutVARData = [OutVARData;RowData];
        end
        xlswrite(OutFilename,[OutputHeader;OutVARData]);
        %         xlswrite(OutFilename,[OutputHeader;OutVARData],'Average VAR');
        
        try
            % create the Breach VaR report
            TableName = ['var_aggregation_level_table_',char(InBUName)];
            SqlQuery = ['select distinct(aggregation_level), breach_var_group from ',char(TableName)];
            BreachGroupData = fetch(ObjDB,SqlQuery);
            BreachGroupData(strcmpi(BreachGroupData(:,1),'TOTAL:TOTAL'),2) = {'PORTFOLIO-GROUP VAR'};
            IdxEmpty = cellfun(@isempty,BreachGroupData(:,2));
            BreachGroupData(IdxEmpty,:) = [];
            BreachAggrLevel = BreachGroupData(:,1);
            BreachVaRGroup = BreachGroupData(:,2);
            UniqueGroup = unique(BreachVaRGroup);
            for iGrp = 1:length(UniqueGroup)
                CurrGroup = UniqueGroup(iGrp);
                IdxBreahGroup = strcmpi(BreachVaRGroup,CurrGroup);
                CurrAggrLevel = BreachAggrLevel(IdxBreahGroup);
                IdxOutData = ismember(OutVARData(:,2),CurrAggrLevel);
                xlswrite(OutFilename,[OutputHeader;OutVARData(IdxOutData,:)],char(CurrGroup));
            end
        catch
        end
        
        %         try
        %             [Overall_ProductWiseData , Overall_ProductGroupWiseData] = AGF_SecondCut_Reports_Exposure(OutVARData(:,2:3));
        %             xlswrite(OutVARFilename,Overall_ProductWiseData,'Product_VAR_Table');
        %             xlswrite(OutVARFilename,Overall_ProductGroupWiseData,'ProductGroup_VAR_Table');
        %         catch
        %         end
        
    else
        NumDays = 950;
        [~,~,OutVARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
        xlswrite(OutFilename,OutVARData,'950Days VAR');
    end
    
    %%% Newly added code
    [Overall_ProductWiseData , Overall_ProductGroupWiseData] = AGF_SecondCut_Reports_Exposure(InBUName);
    
    xlswrite(OutFilename,Overall_ProductWiseData,'Product_VAR_Table');
    xlswrite(OutFilename,Overall_ProductGroupWiseData,'ProductGroup_VAR_Table');
    %%% end code
    
    %%% Upload data into table
    TableName = ['var_report_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutVARData);
    
catch ME
    disp('error occured while caculating V@R report!');
end
