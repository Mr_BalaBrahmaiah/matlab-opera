function [ OutErrorMsg,OutData ] = calculate_settlement_vol( InBUName,InVolIds )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

OutErrorMsg = {'No errors'};
OutData = [];

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

[ColNames,Data] = read_from_database('helper_calculate_settle_vols_view',0,'',InBUName);
OptionData = cell2dataset([ColNames;Data]); 

IdxFound = ismember(upper(OptionData.vol1_id),upper(InVolIds));

DBValueDate = OptionData.value_date{1};

MaturityDate = datenum(OptionData.maturity(IdxFound),'yyyy-mm-dd');
OptionType = strrep(OptionData.opt_type(IdxFound),'vanilla_','');

TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today ;

SettleVols = vanillaimpvolbybs(OptionData.p1_settleprice(IdxFound), OptionData.strike(IdxFound),...
    OptionType, MaturityDate, TimeShift, OptionData.settlement_price(IdxFound));

OptionType = strrep(OptionType,'call','C');
OptionType = strrep(OptionType,'put','P');

OutData = [OptionData.value_date(IdxFound),OptionData.vol1_id(IdxFound),num2cell(OptionData.strike(IdxFound)),OptionType,SettleVols];
catch ME
    OutErrorMsg = cellstr(ME.message);
end
