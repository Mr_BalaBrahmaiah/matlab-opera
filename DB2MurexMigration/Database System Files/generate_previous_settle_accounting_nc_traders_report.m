function AccountingTRReport = generate_previous_settle_accounting_nc_traders_report

try
%     OutErrorMsg = {'No errors'};
%     
derivativepricerinterface_previous_settle_vols('cfs','settle',1,'system');

    ObjDB = connect_to_database;
    
    ViewName = 'helper_settle_pricing_previous_settle_vols_view';
    [ColNames,Data] = read_from_database(ViewName,0);
    
    AccountingTRReport = getXLSFilename('Previous_Settle_Accounting_Traders_Report');
    
    PosTradeDate   = strcmpi('transaction_date',ColNames);
    PosHedgeStatus = strcmpi('fut_hedge_status',ColNames);
    TradeClassification = cell(size(Data,1),1);
    FutHedgeStatus = cell(size(Data,1),1);
    
    SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    IdxFTDTrades = strcmpi(SettlementDate,Data(:,PosTradeDate));
    TradeClassification(IdxFTDTrades)  = {'FTD'};
    TradeClassification(~IdxFTDTrades) = {'NonFTD'};
    
    DBHedgeData = strtrim(Data(:,PosHedgeStatus));
    IdxHedge = strcmpi('H',DBHedgeData) | strcmpi('XH',DBHedgeData);
    FutHedgeStatus(IdxHedge) = {'H'};
    FutHedgeStatus(~IdxHedge) = {'NH'};
    
    Data     = [Data,TradeClassification,FutHedgeStatus];
    ColNames = [ColNames,'trade_classification','future_hedge_status'];
    
    UniqueFields = {'value_date','subportfolio','group_type','trade_classification','future_hedge_status'};
    SumFields = {'settle_delta_1','settle_gamma_11','settle_theta','settle_vega_1','mtm-nc'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'PreviousVol Summary Report');
    
%     TableName = 'previous_settle_vol_accounting_tr_summary_table';
    TableName = 'dealprofit_previous_settle_vol_accounting_tr_summary_table';
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutData);        

    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
%     ViewName = 'helper_eod_monthend_report_lean_view';
    [ColNames,Data] = read_from_database(ViewName,0);  
    
    PosTradeDate   = strcmpi('transaction_date',ColNames);
    PosHedgeStatus = strcmpi('fut_hedge_status',ColNames);
    TradeClassification = cell(size(Data,1),1);
    FutHedgeStatus = cell(size(Data,1),1);
    
    SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    IdxFTDTrades = strcmpi(SettlementDate,Data(:,PosTradeDate));
    TradeClassification(IdxFTDTrades)  = {'FTD'};
    TradeClassification(~IdxFTDTrades) = {'NonFTD'};
    
    DBHedgeData = strtrim(Data(:,PosHedgeStatus));
    IdxHedge = strcmpi('H',DBHedgeData) | strcmpi('XH',DBHedgeData);
    FutHedgeStatus(IdxHedge) = {'H'};
    FutHedgeStatus(~IdxHedge) = {'NH'};
    
    Data     = [Data,TradeClassification,FutHedgeStatus];
    ColNames = [ColNames,'trade_classification','future_hedge_status'];
    
%     InData = cell2dataset([ColNames;Data]);
%     [~,OutData] = compute_monthend_mtm_values(InData); 
%     VDate = repmat(DBValueDate,size(OutData,1),1);
%     OutData = [VDate,OutData];
    
    UniqueFields = {'value_date','subportfolio','group_type','trade_classification','future_hedge_status'};
    SumFields = {'settle_delta_1','settle_gamma_11','settle_theta','settle_vega_1','mtm-nc'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'TodaysVol Summary Report');
    
    TableName = 'todays_settle_vol_accounting_tr_summary_table';
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutData); 
           
    try
        xls_delete_sheets(fullfile(pwd,AccountingTRReport));
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    AccountingTRReport = '';
end