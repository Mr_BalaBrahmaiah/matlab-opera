InputFilterValues = {'subportfolio',{'MM-C-CBT-OIV'}}
SummmaryParameters = {'subportfolio'}
PricingType = 'settle'
OutputFields = {'price','pricemove','vol','volmove','time','timemove','delta','gamma','vega','theta','m2m_usd','m2m_nc'}
InputScenarios = {'on',3,10,'PriceTick';... % IsPerformSensitivity, NumberOfSteps,StepSize - Price move
'on',3,0.5,[];...% IsPerformSensitivity, NumberOfSteps,StepSize - vol move(in %)
'on',5,5,[]}% IsPerformSensitivity, NumberOfSteps,StepSize - time move(in days)

SummaryStatisticsInfo = calculate_sensitivity_summary(InputFilterValues,InputScenarios)

tic
OutFilename = generate_sensitivity_report(InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
toc


