function [OutErrorMsg,OutFilename,OutVARData] = generate_VAR_report(InBUName,InAggregationLevel,StartDate,EndDate,NumDays)

OutErrorMsg = {'No errors'};
OutFilename = '';
OutVARData =[];

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

ObjDB = connect_to_database;


% read the data dump between the start and end dates selected by user
SqlQuery = ['select * from helper_var_accounting_report where value_date between ''',StartDate,''' and ''',EndDate,''' and invenio_product_code not in (''','EG CVR',''')'];
%SqlQuery = ['select * from helper_var_accounting_report where value_date between ''',StartDate,''' and ''',EndDate,''''];
% SqlQuery = ['select * from helper_var_accounting_report where value_date between ''',StartDate,''' and ''',EndDate,''' and invenio_product_code = ''CM KW'' and subportfolio like ''AL%'''];

[ColNames,Data] = read_from_database('helper_var_accounting_report',0,SqlQuery,InBUName);

[AggregationColNames,AggregationData] = read_from_database('var_aggregation_level_table',0,'',InBUName);
if strcmpi(AggregationData,'No Data')
    OutErrorMsg = {'Unable to generate VAR report as aggregation level for VAR report is not defined in master table!'};
    return;
end

AggregationData = cell2dataset([AggregationColNames;AggregationData]);
IsFound  = ismember(AggregationData.aggregation_level,InAggregationLevel);
InSubportfolios = unique(AggregationData.subportfolio(IsFound));

% consider only the products selected by user and remove the others from
% dump
PosSubportfolio = strcmpi('subportfolio',ColNames);
IdxSubportfolioNotSelected = ~ismember(Data(:,PosSubportfolio),InSubportfolios);
Data(IdxSubportfolioNotSelected,:) = [];

InData = cell2dataset([ColNames;Data]);
clear Data;


% HolDates = {'2015-04-03';'2015-05-25'};
% HolidayVec = datenum(HolDates,'yyyy-mm-dd');
% StartSDate = cellstr(datestr(busdate(datenum(StartDate),-1,HolidayVec),'yyyy-mm-dd'));
% EndSDate = cellstr(datestr(busdate(datenum(EndDate),-1,HolidayVec),'yyyy-mm-dd'));

% list of unique value and settlement dates in the dump
% SDates = unique(InData.settlement_date);
VDates = unique(InData.value_date);
ProductCode = unique(InData.invenio_product_code);

% get the curr and lot mult values from the dump
SqlQuery = ['select * from helper_var_mult_factor_view where value_date between ''',StartDate,''' and ''',EndDate,''''];
[ColNames,Data] = read_from_database('helper_var_mult_factor_view',0,SqlQuery);
MultFactorData = cell2dataset([ColNames;Data]);

LotMult  =  zeros(size(InData.invenio_product_code));
CurrMult =  zeros(size(InData.invenio_product_code));
MultFactor =  zeros(size(InData.invenio_product_code));


for iCode = 1:length(ProductCode)
    for iDate = 1:length(VDates)
        IdxFactor = strcmpi(ProductCode(iCode),InData.invenio_product_code) & strcmpi(VDates(iDate),InData.value_date);
        IdxMultFactor = strcmpi(ProductCode(iCode),MultFactorData.invenio_product_code) & strcmpi(VDates(iDate),MultFactorData.value_date);
        
        TempLotMult  = unique(MultFactorData.lot_mult(IdxMultFactor));
        TempCurrMult = unique(MultFactorData.curr_mult(IdxMultFactor));
        TempMultFactor = unique(MultFactorData.mult_factor(IdxMultFactor));
        
        if isempty(TempLotMult) && isempty(TempCurrMult) && isempty(TempMultFactor)
            continue
        end
        if length(TempLotMult) > 1
            TempLotMult = TempLotMult(1);
        end
        
        if length(TempCurrMult)  > 1
            TempCurrMult = TempCurrMult(1);
        end
        
        if length(TempMultFactor)  > 1
            TempMultFactor = TempMultFactor(1);
        end
        
        LotMult(IdxFactor) = TempLotMult;
        CurrMult(IdxFactor) = TempCurrMult;
        MultFactor(IdxFactor) = TempMultFactor;
    end
end

InData.lot_mult = LotMult;
InData.curr_mult = CurrMult;
InData.mult_factor = MultFactor;

clear LotMult; clear CurrMult; clear MultFactor; clear MultFactorData;

% compute the screen values
InData.Delta_Screen = InData.delta .* InData.lot_mult .* InData.curr_mult;
InData.Gamma_Screen = InData.gamma .* InData.curr_mult.^2 ;
InData.Vega_Screen  = InData.vega;
InData.Theta_Screen = InData.theta .* (5/7);

InData = dataset2cell(InData);
ColNames = InData(1,:);
Data     = InData(2:end,:);

UniqueFields = {'value_date','settlement_date','subportfolio','future_continuation_series','option_continuation_series'};
SumFields = {'Delta_Screen','Gamma_Screen','Vega_Screen','Theta_Screen'};
OutputFields = [UniqueFields,'p1_name','contract_month','vol_id','p1_settleprice','invenio_product_code','future_expiry','option_expiry',SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

InData = cell2dataset([OutputFields;OutData]);

NumRows = size(InData,1);
DeltaPnL = cell(NumRows,1);
GammaPnL = cell(NumRows,1);
VegaPnL  = cell(NumRows,1);
ThetaPnL = cell(NumRows,1);
TotalPnL = cell(NumRows,1);
StrTotalPnl = cell(NumRows,1);

% PrctileLoss95   = cell(NumRows,1);
% PrctileLoss98   = cell(NumRows,1);
% PrctileLoss99   = cell(NumRows,1);
% PrctileProfit95 = cell(NumRows,1);
% PrctileProfit98 = cell(NumRows,1);
% PrctileProfit99 = cell(NumRows,1);

UniqueKeySubportfolioLevel = cell(NumRows,1);

% try
% if all(isnan(InData.option_continuation_series)) || all(isnan(InData.future_continuation_series))
%     OutErrorMsg = {'Unable to generate VAR report as the Price Change or Vol Change data is not computed in VAR Data page!'};
%     return;
% end
% catch
% end

for iRecord = 1:NumRows    
    
    disp(iRecord); % just to track the iterations
   
    SettleDate = InData.settlement_date{iRecord};
    SqlQuery = ['select price_change from var_cont_price_change_table where cont_month = ''',InData.future_continuation_series{iRecord},...
        ''' and settlement_date <= ''',SettleDate,''' order by settlement_date desc limit 0,',num2str(NumDays)];
    PriceChangeVector = fetch(ObjDB,SqlQuery);
    PriceChangeVector = cell2mat(PriceChangeVector);
    PriceChangeVector = flipud(PriceChangeVector);
    % to check if any value is missing (to prevent NaN values)
    if ~isempty(find(isnan(PriceChangeVector)))
        disp([InData.future_continuation_series{iRecord},':',SettleDate]);
    end
    
    DeltaPnL{iRecord} = (InData.Delta_Screen(iRecord) .* PriceChangeVector)';
    GammaPnL{iRecord} = (0.5 .* InData.Gamma_Screen(iRecord) .* (PriceChangeVector.^2))';
    
    IsFutures = 0;
    if ~iscell(InData.option_continuation_series) && all(isnan(InData.option_continuation_series)) % if no live options are found
        IsFutures = 1;
    elseif isempty(InData.option_continuation_series{iRecord}) || any(isnan(InData.option_continuation_series{iRecord}))
        IsFutures = 1;
    end
    
    if IsFutures  % for futures
        VegaPnL{iRecord} = zeros(1,NumDays);
    else
        SqlQuery = ['select vol_change from var_cont_vol_change_table where cont_month = ''',InData.option_continuation_series{iRecord},...
            ''' and settlement_date <= ''',SettleDate,''' order by settlement_date desc limit 0,',num2str(NumDays)];
        VolChangeVector = fetch(ObjDB,SqlQuery);
        if isempty(VolChangeVector)
            VolChangeVector = zeros(NumDays,1);
        else
            VolChangeVector = cell2mat(VolChangeVector);
            VolChangeVector = flipud(VolChangeVector);
        end
        
        VegaPnL{iRecord} = (InData.Vega_Screen(iRecord) .* VolChangeVector)';
        % to check if any value is missing (to prevent NaN values)
        if ~isempty(find(isnan(VolChangeVector)))
            disp([InData.option_continuation_series{iRecord},':',SettleDate]);
        end
    end
    
    ThetaPnL{iRecord} = repmat(InData.Theta_Screen(iRecord),1,NumDays);
    
    TotalPnL{iRecord} = DeltaPnL{iRecord} + GammaPnL{iRecord} + VegaPnL{iRecord} + ThetaPnL{iRecord};
    StrTotalPnl{iRecord} = num2str(TotalPnL{iRecord}); 
    UniqueKeySubportfolioLevel{iRecord} = [InData.value_date{iRecord},';',InData.subportfolio{iRecord}];
    
end

InData.DeltaPnL = num2str(cell2mat(DeltaPnL));
InData.GammaPnL = num2str(cell2mat(GammaPnL));
InData.VegaPnL  = num2str(cell2mat(VegaPnL));
InData.ThetaPnL = num2str(cell2mat(ThetaPnL));
InData.TotalPnL = num2str(cell2mat(TotalPnL));

% clear DeltaPnL; clear GammaPnL; clear VegaPnL; clear ThetaPnL;

OutFilename = getXLSFilename('VAR_Report_TradeBook');

TotalPnL = cell2mat(TotalPnL);

% compute the V@R at aggregation level
OutRowData = [];
InData.invenio_product_code = strtrim(InData.invenio_product_code);
AggregationData.product_code = strtrim(AggregationData.product_code);

% store the total pnls of the trades as its needed to aggregate for dynamic
% var computation
try
    if (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg') || strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2'))
        OutDBVaRData = [repmat(VDates,size(InData.invenio_product_code)),...
            InData.invenio_product_code,InData.subportfolio,...
            repmat(num2cell(NumDays),size(InData.invenio_product_code)),StrTotalPnl];
        
        TableName = ['var_dynamic_report_',InBUName];
        set(ObjDB,'AutoCommit','off');
        DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
        SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''' and num_days = ',num2str(NumDays)];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName,OutDBVaRData);
    end
catch
end

for iDate = 1:length(VDates)
    for iAggLev = 1:length(InAggregationLevel)
        IsFound  = ismember(AggregationData.aggregation_level,InAggregationLevel{iAggLev});
        
        InSubportfolios = AggregationData.subportfolio(IsFound);        
        IdxSubportfolio = logical(zeros(size(InData.subportfolio)));
        IdxFound = ismember(InData.subportfolio,InSubportfolios);
        IdxSubportfolio(IdxFound) =1;
        
        if ~strcmpi(InBUName,'cfs')
        InProductCode = AggregationData.product_code(IsFound);
        IdxProductCode = logical(zeros(size(InData.subportfolio)));        
        IdxFound = ismember(InData.invenio_product_code,InProductCode);
        IdxProductCode(IdxFound) =1;
        
        IdxAggrLevel =  IdxSubportfolio & ...
              IdxProductCode & ...
            strcmpi(VDates(iDate),InData.value_date);
        else
             IdxAggrLevel =  IdxSubportfolio & ...              
            strcmpi(VDates(iDate),InData.value_date);
        end       
        
        
        if length(find(IdxAggrLevel)) == 1
            TempTotalPnL = TotalPnL(IdxAggrLevel,:);
        else
            TempTotalPnL = sum(TotalPnL(IdxAggrLevel,:));
        end
        
        PrctileLoss95 = calculate_xl_percentile(TempTotalPnL,5);
        RowData = [VDates(iDate),InAggregationLevel(iAggLev),PrctileLoss95];
        OutRowData = [OutRowData;RowData];
    end
end
OutputHeader = {'value_date','Aggregation_level','PrctileLoss95'};
% xlswrite(OutFilename,[OutputHeader;OutRowData],'V@R-AggregationLevel'); 
% xlswrite(OutFilename,[OutputHeader;OutRowData]); 
OutVARData = [OutputHeader;OutRowData];

InData = dataset2cell(InData);
ColNames = InData(1,:);
Data     = InData(2:end,:);

% write the trade_book format
UniqueFields = {'value_date','settlement_date','subportfolio','future_continuation_series','option_continuation_series'};
SumFields = {'Delta_Screen','Gamma_Screen','Vega_Screen','Theta_Screen'};
OutputFields = [UniqueFields,'invenio_product_code','p1_name','vol_id',SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
xlswrite(OutFilename,[OutputFields;OutData],'Trade_Book');

OutXLSFileName = fullfile(pwd,OutFilename);
try
xls_delete_sheets(OutXLSFileName);
catch
end

catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
    OutVARData =[];
end