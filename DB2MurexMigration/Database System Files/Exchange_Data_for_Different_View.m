
clc;
clear all;

try
    %%
    
    ObjDB = connect_to_database;
    
    Data_View = 'deal_ticket_table_with_latest_versions_view_coc';
    
    Update_View = 'cloned_deal_ticket_table_coc';
    
    %% Fetch Data
    SqlQuery1 = ['select trade_id,deal_status,broker_lots from ',Data_View];
    [DataView_FieldNames,CorrectData] = Fetch_DB_Data(ObjDB,SqlQuery1);
    
    
    TradeID_Col = cellStrfind_exact(DataView_FieldNames,{'trade_id'});
    DealStatus_Col = cellStrfind_exact(DataView_FieldNames,{'deal_status'});
    BrokerLots_Col = cellStrfind_exact(DataView_FieldNames,{'broker_lots'});
    
    %%
    tic;
    MissingTrades = [];
    for i = 1 : size(CorrectData)
        
        Current_TradeID = char(CorrectData(i,TradeID_Col));
        Current_DealStatus = char(CorrectData(i,DealStatus_Col));
        Current_BrokerLots = num2str(cell2mat(CorrectData(i,BrokerLots_Col)));
        
        try
            Update_SqlQuery = ['update ',Update_View,' set deal_status = ''',Current_DealStatus,''', broker_lots = ',Current_BrokerLots,' where trade_id=''',Current_TradeID,''' '];
            
            curs = exec(ObjDB,Update_SqlQuery);
            
            
        catch
            MissingTrades = [MissingTrades ; CorrectData(i,:)] ;
            continue;            
        end
        
    end
    
    toc;
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end