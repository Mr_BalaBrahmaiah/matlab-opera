function [OutErrorMsg,FutureData] = openposition_futures_report(InBUName,SettleColNames,SettleData)
%  Generate the futures in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/14 07:53:07 $
%  $Revision: 1.7 $
%
%

OutErrorMsg = {'No errors'};
FutureData = [];

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end 

    %%Remove  AGF Subportfolio in Counterparty Column (Directional Deals)
    ObjDB = connect_to_database;
    
    SqlQuery_1 = ['select distinct(subportfolio) from subportfolio_product_mapping_table_',char(InBUName)] ;
    [~,Subportfolio_1] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    
    SqlQuery_2 = ['select distinct(directional_portfolio) from subportfolio_product_mapping_table_',char(InBUName)];
    [~,Subportfolio_2] = Fetch_DB_Data(ObjDB,SqlQuery_2);
    
    Overall_Subportfolio = [Subportfolio_1 ; Subportfolio_2];
    
    Counterparty_Col = cellStrfind_exact(SettleColNames,{'counterparty_parent'});
    Matched_Subportfolio_Index = cellStrfind_exact(SettleData(:,Counterparty_Col),Overall_Subportfolio);
    
    if(~isempty(Matched_Subportfolio_Index))
        SettleData(Matched_Subportfolio_Index,:) = [];
    end

    %%%
    if(~strcmpi(SettleData,'No Data'))
        Pos_OptType = strcmpi('derivative_type',SettleColNames);
        Pos_Maturity = strcmpi('maturity_date',SettleColNames);
        Pos_ValueDate = strcmpi('value_date',SettleColNames);
        Pos_Counterparty = strcmpi('counterparty_parent',SettleColNames);
        
        % remove the internal deals
        Counterparty = SettleData(:,Pos_Counterparty);
        IdxRemove = strncmpi(Counterparty,'MM',2) | strncmpi(Counterparty,'RMS',3);
        SettleData(IdxRemove,:) = [];
        
        % find the future records
        IdxFut = strcmpi('future',SettleData(:,Pos_OptType)) |  strcmpi('fx_future',SettleData(:,Pos_OptType));
        
        SettleColNames = [SettleColNames,'deal_status'];
        
        % generate the futures report
        DBFutData = SettleData(IdxFut,:);
        PosNettingStatus = strcmpi('netting_status',SettleColNames);
        IdxNetted = strcmpi('fully_netted',DBFutData(:,PosNettingStatus)); % dead
        MaturityDate = datenum(DBFutData(:,Pos_Maturity),'yyyy-mm-dd');
        ValueDate = datenum(DBFutData(:,Pos_ValueDate),'yyyy-mm-dd');
        IdxDeadFut = (ValueDate > MaturityDate) | IdxNetted;
        % DBFutData(IdxDeadFut,:) = [];
        DealStatus = repmat({'live'},size(MaturityDate));
        DealStatus(IdxDeadFut) = {'dead'};
        
        DBFutData = [DBFutData,DealStatus];
        
        PosProdCode = strcmpi('product_code',SettleColNames);
        IdxOR = strncmpi(DBFutData(:,PosProdCode),'OR',2);
        DBFutData(IdxOR,PosProdCode) = {'OR'};
        
        Pos_SettlePrice = strcmpi('settle_price',SettleColNames);
        Pos_ActiveLots = strcmpi('active_lots',SettleColNames);
        Pos_Premium = strcmpi('original_premium',SettleColNames);
%         Pos_OriginalLots  = strcmpi('original_lots',SettleColNames);
        
        SettlePrice = cell2mat(DBFutData(:,Pos_SettlePrice));
        ActiveLots = cell2mat(DBFutData(:,Pos_ActiveLots));
        Premium = cell2mat(DBFutData(:,Pos_Premium));
%         OriginalLots = cell2mat(DBFutData(:,Pos_OriginalLots));
        SettlePrice(isnan(SettlePrice)) = 0;
        
        mtm_nc = (SettlePrice .* ActiveLots) - (Premium .* ActiveLots);
        mtm_nc = num2cell(mtm_nc);
        
        SettleColNames = [SettleColNames,'mtm_nc'];
        DBFutData = [DBFutData,mtm_nc];
        OutputFields = {'product_code','product_name','settlement_date','counterparty_parent','contract_month','p1_name',...
            'p1_settleprice','market_action','active_lots','original_premium',...
            'mtm_nc','deal_status','mult_factor','conv_factor','currency'};
        WeightedAverageFields = {'original_premium','active_lots'};
        UniqueFields = {'product_code','counterparty_parent','contract_month','market_action','p1_name','original_premium','deal_status'};
        SumFields = {'active_lots','mtm_nc'};
        [FutureFields,FutureDeals] = consolidatedata(SettleColNames, DBFutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        ConsFutData = cell2dataset([FutureFields;FutureDeals]);
        
        IdxLiveFut = strcmpi('live', ConsFutData.deal_status);
        IdxDeadFut = strcmpi('dead', ConsFutData.deal_status);
        
        IdxFutBot = strcmpi('bought', ConsFutData.market_action) & IdxLiveFut;
        IdxFutSold = strcmpi('sold', ConsFutData.market_action) & IdxLiveFut;
        
        % initialisation
        OpenBought = zeros(size(IdxFutBot));
        OpenSold = zeros(size(IdxFutSold));
        RealisedFut = zeros(size(IdxFutSold));
        M2MFut = zeros(size(IdxFutSold));
        Fut_Open_Sold_Rate = zeros(size(IdxFutSold));
        Fut_Open_Bot_Rate = zeros(size(OpenBought));
        
        MultFac = ConsFutData.mult_factor;
        
        OpenBought(IdxFutBot) = abs(ConsFutData.active_lots(IdxFutBot));
        OpenSold(IdxFutSold) = abs(ConsFutData.active_lots(IdxFutSold));
        NetFutOpen = OpenBought - OpenSold;
        
        RealisedFut(IdxDeadFut) = ConsFutData.mtm_nc(IdxDeadFut) .* MultFac(IdxDeadFut);
        M2MFut(IdxLiveFut) = ConsFutData.mtm_nc(IdxLiveFut) .* MultFac(IdxLiveFut);
        
        Mkt_Rate = ConsFutData.p1_settleprice;
        
        Fut_Mkt_Val = ((OpenBought .* Mkt_Rate) - (OpenSold .* Mkt_Rate)) .* MultFac;
        Fut_Val = Fut_Mkt_Val - M2MFut;
        
        Fut_Open_Sold_Rate(IdxFutSold) = ConsFutData.original_premium(IdxFutSold) ;
        Fut_Open_Bot_Rate(IdxFutBot) =  ConsFutData.original_premium(IdxFutBot);
        
        
        [~,FutID] = strtok(ConsFutData.contract_month,'.');
        FutureMonth = strrep(FutID,'.','');
        
        %%% Actual Report
        OutData_Actual = [ConsFutData.product_code,ConsFutData.product_name,ConsFutData.counterparty_parent,FutureMonth,...
            num2cell(OpenSold),num2cell(OpenBought),num2cell(NetFutOpen),...
            num2cell(RealisedFut),num2cell(M2MFut),num2cell(Fut_Val),...
            num2cell(Fut_Mkt_Val),num2cell(Mkt_Rate),num2cell(Fut_Open_Sold_Rate),...
            num2cell(Fut_Open_Bot_Rate),ConsFutData.currency,ConsFutData.settlement_date];
        OutHeader_Actual = {'ProductCode','ProductName','Counterparty','ContractMonth','Open_Sold',...
            'Open_Bought','Net_Open','Realised_PnL','M2M','Val','MktVal','Mkt_Rate',...
            'Open_Sold_Rate','Open_Bot_Rate','Currency','Market_ValueDate'};
        
        WeightedAverageFields = [];
        UniqueFields = {'ProductCode','ProductName','Counterparty','ContractMonth'};
        SumFields = {'Open_Sold','Open_Bought','Net_Open','Realised_PnL','M2M',...
            'Val','MktVal','Open_Sold_Rate','Open_Bot_Rate'};
        [~,OutFutureData_Actual] = consolidatedata(OutHeader_Actual, OutData_Actual,UniqueFields,SumFields,OutHeader_Actual,WeightedAverageFields);
        
        %%Live Position Only
        OutData = [OutData_Actual,ConsFutData.deal_status];
        OutHeader_LiveOnly = [OutHeader_Actual,{'deal_status'}];
        
        WeightedAverageFields = [];
        UniqueFields = {'ProductCode','ProductName','Counterparty','ContractMonth','deal_status'};
        SumFields = {'Open_Sold','Open_Bought','Net_Open','Realised_PnL','M2M',...
            'Val','MktVal','Open_Sold_Rate','Open_Bot_Rate'};
        [~,OutFutureData_LiveOnly] = consolidatedata(OutHeader_LiveOnly, OutData,UniqueFields,SumFields,OutHeader_LiveOnly,WeightedAverageFields);
        
        %%Dead Deals Removal
        Deal_Status_Col = cellStrfind_exact(OutHeader_LiveOnly,{'deal_status'});
        Dead_Deals_Index = cellStrfind_exact(OutFutureData_LiveOnly(:,Deal_Status_Col),{'dead'});
        
        OutFutureData_LiveOnly(Dead_Deals_Index,:) = [];
        
        OutHeader_LiveOnly(:,Deal_Status_Col) = [];
        OutFutureData_LiveOnly(:,Deal_Status_Col) =[];
        
        %%Excel Write
        try
            OutFutureData_Actual = sortrows(OutFutureData_Actual,[1 2 3 4]);
            OutFutureData_LiveOnly = sortrows(OutFutureData_LiveOnly,[1 2 3 4]);
        catch
            disp('Error while sorting rows: some NaN values are found');
        end
        
        %%Excel Sheet Write
        OutHeader = {'PRODUCT_CODE','PRODUCT_NAME','BROKER','TMONTH','OPEN_SOLD',...
            'OPEN_BOT','NET_OPEN','REALIZED_PL','MTM','VAL','MKTVAL','MKT_RATE',...
            'OPENSOLDRATE','OPENBOTRATE','Currency','MKT_DATE'};
        
        ProductName_Col = cellStrfind_exact(OutHeader,{'PRODUCT_NAME'});
        Currency_Col = cellStrfind_exact(OutHeader,{'Currency'});
        
        % CME Productt Currency Should be USD
        CME_ProductStr = cellfun(@(s) (s(1:3)), OutFutureData_Actual(:,ProductName_Col),'UniformOutput',0);
        CME_ProductIndex = strcmpi(CME_ProductStr,'CME');
        OutFutureData_Actual(CME_ProductIndex,Currency_Col) = {'USD'};
        
        CME_ProductStr = cellfun(@(s) (s(1:3)), OutFutureData_LiveOnly(:,ProductName_Col),'UniformOutput',0);
        CME_ProductIndex = strcmpi(CME_ProductStr,'CME');
        OutFutureData_LiveOnly(CME_ProductIndex,Currency_Col) = {'USD'};
        
        CALL_PUT = repmat({'FUT'},size(OutFutureData_Actual,1),1);
        STRIKE = repmat({0},size(OutFutureData_Actual,1),1);
        %%% %% comment on 2019-11-22
        %OutFutureData_Actual(:,4) = strcat(OutFutureData_Actual(:,4),'.',OutFutureData_Actual(:,4));
        
        %User_OutHeader = {'PRODUCT_NAME','CALL_PUT','BROKER','TMONTH','STRIKE','NET_OPEN','MTM','MKT_RATE','MKT_DATE'};
        FutureData_out = [OutFutureData_Actual(:,2),CALL_PUT,OutFutureData_Actual(:,3),OutFutureData_Actual(:,4),STRIKE,OutFutureData_Actual(:,7),OutFutureData_Actual(:,9),OutFutureData_Actual(:,12),OutFutureData_Actual(:,16)];
        
        FutureData = FutureData_out;
        
    else
        OutErrorMsg = {['No Data for ',char(InBUName)]};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end