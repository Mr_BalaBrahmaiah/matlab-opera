function [OutErrorMsg,OutXLSName] = generate_broker_open_position_trade_recon(InBUName,Temp_Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: Broker_Open_Position_Trade_Recon
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 12 Jun 2019 11:13:00(Hold) // 26 Jul 2019 12:56:00 (re-start
%%% sate)

%%% [OutErrorMsg,OutXLSName] = generate_broker_open_position_trade_recon('qf1')
%%% [OutErrorMsg,OutXLSName] = generate_broker_open_position_trade_recon('cfs','2019-06-12')

try
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['broker_open_position_trade_recon_',char(InBUName)]);
    
    if iscellstr(InBUName)
       InBUName = char(InBUName);
    end

    % Connect to Database
    ObjDB = connect_to_database;
    
    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = Temp_Settle_Date;
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    Today_date = fetch(ObjDB,'select settlement_date from valuation_date_table'); %% added on 27-12-2019
    
    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from Broker open_position Table
    Open_Position_Table = strcat('broker_open_position_table_',char(InBUName));
    SqlQuery_Open_Position_Table = ['select * from ',char(Open_Position_Table),' where st_date = ''',char(Settele_date),''' '] ;
    [Open_Position_Table_Cols,Open_Position_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Open_Position_Table);

    if strcmpi(Open_Position_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Open_Position_Table)]} ;
        return;
    end
 
    Broker_Open_Position_Tbl = cell2table(Open_Position_Table_Data,'VariableNames',Open_Position_Table_Cols);  %% Convert to table
    
    %%% Fetch data from broker product master table
    BProduct_master_Table = ['broker_product_master_table_',char(InBUName)];
    SqlQuery_BProduct_master_Table = ['select * from ',char(BProduct_master_Table)];
    [BProduct_master_Table_Cols,BProduct_master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BProduct_master_Table);

    if strcmpi(BProduct_master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BProduct_master_Table)]} ;
        return;
    end
    
    Broker_Product_master_Tbl = cell2table(BProduct_master_Table_Data,'VariableNames',BProduct_master_Table_Cols);  %% Convert to table
    
    %%% Fetch data from broker_currency_mapping_table for currency mapping
    SqlQuery_currency_mapping = ['select * from broker_currency_mapping_table'];
    [currency_mapping_cols,currency_mapping_data] = Fetch_DB_Data(ObjDB,SqlQuery_currency_mapping);

    if strcmpi(currency_mapping_data,'No Data') 
        OutErrorMsg = {'Data not Available in broker_currency_mapping_table'} ;
        return;
    end
    Currency_Map_Tab_Data = cell2table(currency_mapping_data,'VariableNames',currency_mapping_cols);  %% Convert to table
    
    %%% Fetch data from helper_risk_middle_office_reports (using Option & Future trades)
    %%% Fetch data for current date - helper_risk_middle_office_reports view
    %%% Fetach data for past pate - rmo_future_report_table & rmo_option_report_table table
    
    if datenum(Settele_date,'yyyy-mm-dd') == datenum(Today_date,'yyyy-mm-dd')  %% added on 27-12-2019
        ViewName = 'helper_risk_middle_office_reports';
        [SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

        [~,Futures_Data] = openposition_futures_report(InBUName,SettleColNames,SettleData);
        [~,Options_Data] = openposition_options_report(InBUName,SettleColNames,SettleData);

        Future_Option_Data = [Futures_Data ; Options_Data];

        if isempty(Future_Option_Data)
            OutErrorMsg = {'No OPERA trades found for the trade date!'};
            return;
        end
    
    elseif datenum(Settele_date,'yyyy-mm-dd') < datenum(Today_date,'yyyy-mm-dd')
        %%% Fetch data from rmo_future_report_table
        rmo_future_Table = strcat('rmo_future_report_table_',char(InBUName));
        SqlQuery_rmo_future_Table = ['select product_name,broker,tmonth,net_open,mtm,mkt_rate,mkt_date from ',char(rmo_future_Table),' where mkt_date = ''',char(Settele_date),''' '] ;
        [~,rmo_future_data] = Fetch_DB_Data(ObjDB,SqlQuery_rmo_future_Table);

        if strcmpi(rmo_future_data,'No Data') 
            Rmo_Future_Table_Data = [];
        else
            CALL_PUT = repmat({'FUT'},size(rmo_future_data,1),1);
            STRIKE = repmat({0},size(rmo_future_data,1),1);
            Rmo_Future_Table_Data = [rmo_future_data(:,1), CALL_PUT, rmo_future_data(:,2), rmo_future_data(:,3), STRIKE, rmo_future_data(:,4:end),];
        end
    
        %%% Fetch data from rmo_option_report_table
        rmo_option_Table = strcat('rmo_option_report_table_',char(InBUName));
        SqlQuery_rmo_option_Table = ['select product_code,product_name,call_put,broker,SUBSTRING_INDEX(tmonth,''.'',1) as tmonth,strike,net_open,mtm,mkt_rate,mkt_date,mktval from ',char(rmo_option_Table),' where mkt_date = ''',char(Settele_date),''' '] ;
        [~,rmo_option_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_rmo_option_Table);

        if strcmpi(rmo_option_Table_Data,'No Data')
            Rmo_Option_Table_Data1 = [];
        else
            Rmo_Option_Table_Data = rmo_option_Table_Data;
            
            %%% Newly added on 2020-01-07, M2M value for only QC,DF & QW
            %%% product codes, for remaining all product codes take value
            %%% from MktValue
            Index_MktVal = strcmpi(Rmo_Option_Table_Data(:,1),'QC') | strcmpi(Rmo_Option_Table_Data(:,1),'DF') | strcmpi(Rmo_Option_Table_Data(:,1),'QW');
            Rmo_Option_Table_Data(~Index_MktVal,8) = Rmo_Option_Table_Data(~Index_MktVal,11);
            
            Rmo_Option_Table_Data1 = [Rmo_Option_Table_Data(:,2), Rmo_Option_Table_Data(:,3), Rmo_Option_Table_Data(:,4), Rmo_Option_Table_Data(:,5), Rmo_Option_Table_Data(:,6) ...
                                    Rmo_Option_Table_Data(:,7), Rmo_Option_Table_Data(:,8), Rmo_Option_Table_Data(:,9),Rmo_Option_Table_Data(:,10)];
        end

        Future_Option_Data = [Rmo_Future_Table_Data ; Rmo_Option_Table_Data1];
        if isempty(Future_Option_Data)
            OutErrorMsg = {'No OPERA trades found for the trade date!'};
            return;
        end        
    end
    
    User_OutHeader = {'PRODUCT_NAME','CALL_PUT','BROKER','TMONTH','STRIKE','NET_OPEN','MTM','MKT_RATE','MKT_DATE'};
    %FutureOption_Data = cell2table(Future_Option_Data(2:end,:),'VariableNames',User_OutHeader);  %% Convert to table
    FutureOption_Data = cell2table(Future_Option_Data,'VariableNames',User_OutHeader);  %% Convert to table
     
    %%Fetch data from Year Mapping Table, to change the terminal_month
    Mapping_Tbl_Name = 'year_mapping_table';
    SqlQuery_Mapping_Table = ['select * from ',char(Mapping_Tbl_Name)] ;
    [Mapping_ColNames,Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Mapping_Table);
    Mapping_Tab_Data = cell2dataset([Mapping_ColNames;Mapping_Table_Data]);
     
    %%% OPERA and Master table mapping
    %%% Comparing exchange, product and counterparty, get required data
    [row,~] = size(FutureOption_Data);
    Broker_Opera_Master_Data = []; 
    for i = 1:1:row
        single_product = FutureOption_Data.PRODUCT_NAME(i);
        single_counterparty = FutureOption_Data.BROKER(i);

        Index_opera = strcmpi(single_product,Broker_Product_master_Tbl.product_map) & strcmpi(single_counterparty,Broker_Product_master_Tbl.broker_map);

        Unique_Opera_Master_Data=Broker_Product_master_Tbl(Index_opera,:);
        
        if size(Unique_Opera_Master_Data,1) > 1
            %Index = strcmpi(single_product,Unique_Opera_Master_Data.broker_product_name);
            Unique_Opera_Master_Data =  Unique_Opera_Master_Data(1,:); 
        end
        
        if isempty(Unique_Opera_Master_Data) 
            continue;
        end
        User_RMO_trade_query_Data = FutureOption_Data(i,:);

        User_RMO_trade_query_Data.account_number = Unique_Opera_Master_Data.account_number;
        User_RMO_trade_query_Data.prod_display = Unique_Opera_Master_Data.product_display;
        User_RMO_trade_query_Data.STRIKE = User_RMO_trade_query_Data.STRIKE ./ Unique_Opera_Master_Data.strike_conversion;
        User_RMO_trade_query_Data.MKT_RATE =  User_RMO_trade_query_Data.MKT_RATE ./ Unique_Opera_Master_Data.rate_conversion;

        Broker_Opera_Master_Data = vertcat(Broker_Opera_Master_Data,User_RMO_trade_query_Data);
    end

    %%% consolidation part for Broker_Opera_Master Data
    Consolidate_Data_Header =  Broker_Opera_Master_Data.Properties.VariableNames;
    Consolidate_Data = table2cell(Broker_Opera_Master_Data);

    UniqueFields = {'PRODUCT_NAME','CALL_PUT','BROKER','TMONTH','STRIKE','MKT_RATE','MKT_DATE','account_number','prod_display'}; 
    SumFields = {'NET_OPEN','MTM'};
    OutputFields = Consolidate_Data_Header ;
    WeightedAverageFields = [];
    [Consolidate_Data_Header,Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    %%% After consolidation --> Header and Data
    Broker_Opera_Master_conso = cell2table(Data_Consolidate,'VariableNames',Consolidate_Data_Header);  %% Convert to table
    
    %%% Broker and Master table mapping
    %%% get the exchange_code for Broker_Confirmed deals data
    [r,~] = size(Broker_Open_Position_Tbl);
    Broker_product_display = []; Broker_terminal_month =[];
    for ii = 1:1:r
        single_Baccount = Broker_Open_Position_Tbl.account(ii);
        single_Bbroker_name = Broker_Open_Position_Tbl.broker_name(ii);
        single_Bproduct = Broker_Open_Position_Tbl.product(ii);
        single_Bterminal_month = Broker_Open_Position_Tbl.terminal_month(ii);

        %%% Broker currency mapping
        Index_cur = strcmpi(Broker_Open_Position_Tbl.currency(ii),Currency_Map_Tab_Data.broker_currency);
        if any(Index_cur)
            currency = Currency_Map_Tab_Data.opera_currency(Index_cur);
            if strcmpi(currency,'USDUSD')
                final_currency(ii,1) = {'USD'};
            else
                final_currency(ii,1) = erase(currency,'USD');
            end
        else
            final_currency(ii,1) = Broker_Open_Position_Tbl.currency(ii);
        end
        %%% End code
        
        Index = strcmpi(single_Baccount,Broker_Product_master_Tbl.account_number) & strcmpi(single_Bproduct,Broker_Product_master_Tbl.product_display) & ...
                            strcmpi(single_Bbroker_name,Broker_Product_master_Tbl.broker_name);

        Opera_Master_Data=Broker_Product_master_Tbl(Index,:);
        
        %%% Chnage teminal_month using year mapping table like(0->20, 1->21 etc.
        month = char(single_Bterminal_month);
        last_digit = month(end);
        Mapping_Tab_digit=Mapping_Tab_Data.single_digit_year;
        Index_year = eq(str2double(last_digit),Mapping_Tab_digit);
        if any(Index_year)
            double_digit_year = Mapping_Tab_Data.double_digit_year(Index_year);
            month = strcat(month(1),num2str(double_digit_year));
        end
        
        if isempty(Opera_Master_Data) 
            terminal_month = cellstr(month);
            product_display =single_Bproduct;
            
            Broker_terminal_month = vertcat(Broker_terminal_month,terminal_month);
            Broker_product_display = vertcat(Broker_product_display,product_display);
            continue;
        elseif size(Opera_Master_Data,1) > 1
            Opera_Master_Data =  Opera_Master_Data(1,:); 
        end
        
        terminal_month = cellstr(month);  %%% terminal month
        product_display = Opera_Master_Data.product_display;  %%% Product

        Broker_terminal_month = vertcat(Broker_terminal_month,terminal_month);
        Broker_product_display = vertcat(Broker_product_display,product_display);
    end
    
    Broker_Open_Position_Tbl.terminal_month = Broker_terminal_month;
    Broker_Open_Position_Tbl.product_display = Broker_product_display;
    Broker_Open_Position_Tbl.currency = final_currency;
    
    %%% Broker and Opera mapping process
    [rr,~] = size(Broker_Open_Position_Tbl);
    Not_Matched_Broker_Data = []; Matched_Broker_Data = []; Matched_Opera_Data = []; Index_Match_Opera =[];
    for j = 1:1:rr
        Index_match = (strcmpi(Broker_Open_Position_Tbl.account(j), Broker_Opera_Master_conso.account_number) & ...
                    strcmpi(Broker_Open_Position_Tbl.trade_type(j),Broker_Opera_Master_conso.CALL_PUT) & ...
                    strcmpi(Broker_Open_Position_Tbl.terminal_month(j),Broker_Opera_Master_conso.TMONTH) & ...
                    strcmpi(Broker_Open_Position_Tbl.product_display(j),Broker_Opera_Master_conso.prod_display) & ...
                    ismember(Broker_Opera_Master_conso.STRIKE,Broker_Open_Position_Tbl.strike(j)));
        if any(Index_match) 
                Index_Match_Opera = [Index_Match_Opera ; find(Index_match)];
                Matched_Opera_Data = [Matched_Opera_Data ; Broker_Opera_Master_conso(Index_match,:)];
                Matched_Broker_Data = [Matched_Broker_Data ; Broker_Open_Position_Tbl(j,:)];
        else
                Not_Matched_Broker_Data = [Not_Matched_Broker_Data ; Broker_Open_Position_Tbl(j,:)];
        end
    end
    
    %%% Mached data with broker and Opera
    if ~isempty(Matched_Broker_Data)
        Matched_Broker_Data = sortrows(Matched_Broker_Data,15);
        Matched_Opera_Data = sortrows(Matched_Opera_Data,11);

        Diff_Open_Lots = num2cell(Matched_Broker_Data.net_open_lots - Matched_Opera_Data.NET_OPEN) ;
        Diff_M2M = num2cell(Matched_Broker_Data.m2m - Matched_Opera_Data.MTM) ;
        Diff_Stttlement_Price = num2cell(Matched_Broker_Data.st_rate - Matched_Opera_Data.MKT_RATE) ;

        Trade_Data = [Matched_Broker_Data.desk, Matched_Broker_Data.broker_name, Matched_Broker_Data.account, Matched_Broker_Data.product_display, Matched_Broker_Data.currency, ...
                    Matched_Broker_Data.terminal_month, Matched_Broker_Data.trade_type, num2cell(Matched_Broker_Data.strike), num2cell(Matched_Broker_Data.net_open_lots), num2cell(Matched_Broker_Data.m2m), ...
                    num2cell(Matched_Broker_Data.st_rate),Matched_Opera_Data.BROKER, Matched_Opera_Data.prod_display, Matched_Opera_Data.TMONTH, Matched_Opera_Data.CALL_PUT, ...
                    num2cell(Matched_Opera_Data.STRIKE), num2cell(Matched_Opera_Data.NET_OPEN), num2cell(Matched_Opera_Data.MTM), num2cell(Matched_Opera_Data.MKT_RATE),Diff_Open_Lots, Diff_M2M, Diff_Stttlement_Price];
    end
    
    %%% Broker @ Not matching data with broker and opera
     if ~isempty(Not_Matched_Broker_Data)
        Value_1 = repmat({''},size(Not_Matched_Broker_Data,1),1);
        Value_2 = repmat({[]},size(Not_Matched_Broker_Data,1),1);
        net_open_lots = Not_Matched_Broker_Data.net_open_lots;
        m2m = Not_Matched_Broker_Data.m2m;
        st_rate = Not_Matched_Broker_Data.st_rate;
        
        Not_Match_Broker_Trade_Data = [Not_Matched_Broker_Data.desk, Not_Matched_Broker_Data.broker_name, Not_Matched_Broker_Data.account, Not_Matched_Broker_Data.product_display, Not_Matched_Broker_Data.currency, ...
                    Not_Matched_Broker_Data.terminal_month, Not_Matched_Broker_Data.trade_type, num2cell(Not_Matched_Broker_Data.strike), num2cell(Not_Matched_Broker_Data.net_open_lots), num2cell(Not_Matched_Broker_Data.m2m), ...
                    num2cell(Not_Matched_Broker_Data.st_rate),Value_1,Value_1,Value_1,Value_1,Value_1,Value_2,Value_2,Value_2,num2cell(net_open_lots),num2cell(m2m),num2cell(st_rate)];
     else
        Not_Match_Broker_Trade_Data = [];
     end
     
    %%% Opera @ Remaning Opera Data
    if ~isempty(Index_Match_Opera)
        Broker_Opera_Master_conso(Index_Match_Opera,:) = [];
        Value_1 = repmat({''},size(Broker_Opera_Master_conso,1),1);
        Value_2 = repmat({[]},size(Broker_Opera_Master_conso,1),1);
        Opera_net_open_lots = Broker_Opera_Master_conso.NET_OPEN;
        Opera_m2m = Broker_Opera_Master_conso.MTM;
        Opera_st_rate = Broker_Opera_Master_conso.MKT_RATE;
        Not_Match_Opera_Trade_Data = [Value_1, Value_1, Value_1, Value_1, Value_1, Value_1, Value_1, Value_2, Value_2, Value_2, Value_2, ...
                    Broker_Opera_Master_conso.BROKER, Broker_Opera_Master_conso.prod_display, Broker_Opera_Master_conso.TMONTH, Broker_Opera_Master_conso.CALL_PUT, ...
                    num2cell(Broker_Opera_Master_conso.STRIKE), num2cell(Broker_Opera_Master_conso.NET_OPEN), num2cell(Broker_Opera_Master_conso.MTM), ...
                    num2cell(Broker_Opera_Master_conso.MKT_RATE),num2cell(Opera_net_open_lots), num2cell(Opera_m2m), num2cell(Opera_st_rate)];
    else
        Not_Match_Opera_Trade_Data = [];
    end
    
    Trade_Header = {'BROKER', '', '', '', '', '', '', '', '', '', '','INTERNAL SYSTEM','', '', '', '', '', '', '','DIFFERENCE','', ''; ...
                'Desk', 'Broker', 'Account', 'Product', 'Currency', 'Terminal Month', 'Trade Type', 'Strike', 'Open Lots', 'M2M', 'Settlement Price',...
                'Broker', 'Product', 'Terminal Month', 'Trade type', 'Strike', 'Open Lots', 'M2M', 'Settlement Price','Open Lots', 'M2M', 'Settlement Price'};

    %%% write data into file
    if ~exist('Trade_Data','var')
        xlswrite(OutXLSName,{'No Data Matched'},'Position Reconciliation');
    else
        Sheet_Name = strcat('Position Reconcili_',char(Settele_date));
        xlswrite(OutXLSName,[Trade_Header;[Trade_Data ; Not_Match_Broker_Trade_Data ; Not_Match_Opera_Trade_Data]],Sheet_Name);
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        %%% Merge Cells and provide the colour to cells   
        xlCenter = -4108;
        Excel = actxserver('Excel.Application');
        Excel.Visible = 1; % to see the excel file real time make 1;
        Path = fullfile(pwd,OutXLSName);
        Workbook = Excel.Workbooks.Open(Path);
        Range = Excel.Range('A1:K1');
        Range.Select;
        Range.MergeCells = 1;
        Range.HorizontalAlignment = xlCenter;

        Range1 = Excel.Range('L1:S1');
        Range1.Select;
        Range1.MergeCells = 1;
        Range1.HorizontalAlignment = xlCenter;

        Range2 = Excel.Range('T1:V1');
        Range2.Select;
        Range2.MergeCells = 1;
        Range2.HorizontalAlignment = xlCenter;
        %%% Set the color to cell using RGB colors
        Workbook.Worksheets.Item(1).Range('A1:K1').Interior.ColorIndex = 4;
        Workbook.Worksheets.Item(1).Range('L1:S1').Interior.ColorIndex =8;
        Workbook.Worksheets.Item(1).Range('T1:V1').Interior.ColorIndex = 6;
        Workbook.Worksheets.Item(1).Range('A2:V2').Interior.ColorIndex = 15;

        % Save & close & Quit Workbook
        Workbook.Save();
        Workbook.Close();
        Excel.Quit();
    end
      
    xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Position Reconciliation');

catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end

end
