function [OutErrorMsg,OutFilename] = calculate_vol_change_data

OutErrorMsg = {'No errors'};
OutFilename = '';
try 
update_option_cont_series_table;

ObjDB = connect_to_database;

[ColNames,Data] = read_from_database('helper_calculate_price_vol_change_view',0);
PriceVolData = cell2dataset([ColNames;Data]);

% ProductCode = strtrim(cellfun(@(x) x(1:2), PriceVolData.underlying_id, 'UniformOutput', false));
% PriceVolData.ProductCode = ProductCode;

VDate = fetch(ObjDB,'select value_date from valuation_date_table');
Sys_SDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

% HolDates = {'2015-04-03';'2015-05-25'};
% HolidayVec = datenum(HolDates,'yyyy-mm-dd');
% SDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
% SBusDay = datenum(SDate,'yyyy-mm-dd');
% SSDate = cellstr(datestr(busdate(SBusDay,-1,HolidayVec),'yyyy-mm-dd'));

Unique_ProductCode = fetch(ObjDB,'select distinct(bbg_product_code) from product_master_table where invenio_product_code like ''CM%''');

%%
[FutureContSeriesData_Cols,OptionContSeriesData_All] = Fetch_DB_Data(ObjDB,'select * from option_cont_series_table');
OptionContSeriesData_All =  cell2dataset([FutureContSeriesData_Cols;OptionContSeriesData_All]);

[AssetClass,Remain_ProductCode] = strtok(OptionContSeriesData_All.underlying_vol_id,' ');
[ProductCode,Remain_ProductCode] = strtok(Remain_ProductCode);
ProductCode = strtrim(ProductCode);
OptionContSeriesData_All.ProductCode = ProductCode;

%%
OutVolChangeData = [];
ProductCode_SDate = [];

for i = 1 : length(Unique_ProductCode)
    
    Current_ProductCode = Unique_ProductCode(i);
    SqlQuery_SDate = ['select settle_date from product_busdays_table where product_code = ''',char(Current_ProductCode),''' and value_date = ''',char(VDate),''' '  ] ;
    [~,SDate] = Fetch_DB_Data(ObjDB,SqlQuery_SDate);
    
    SqlQuery_SSDate = ['select settle_date from product_busdays_table where product_code = ''',char(Current_ProductCode),''' and value_date = ''',char(SDate),''' '  ] ;
    [~,SSDate] = Fetch_DB_Data(ObjDB,SqlQuery_SSDate);
    
    IdxSDates = strcmpi(SDate,PriceVolData.settlement_date);
    IdxSSDates = strcmpi(SSDate,PriceVolData.settlement_date);
    
    SDataPriceVol = PriceVolData(IdxSDates,:);
    SSDataPriceVol = PriceVolData(IdxSSDates,:);
    
    %     SqlQuery = ['select * from option_cont_series_table where settlement_date = ''',char(SDate),''''];
    %     % [ColNames,Data] = read_from_database('option_cont_series_table',0,SqlQuery);
    %     [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    %     OptionContSeriesData = cell2dataset([ColNames;Data]);
    
    Id = strcmpi(Current_ProductCode,OptionContSeriesData_All.ProductCode) & strcmpi(SDate,OptionContSeriesData_All.settlement_date);
    OptionContSeriesData  = OptionContSeriesData_All(Id,:);
    
    NumRecords = size(OptionContSeriesData,1);
    
    for iR = 1:NumRecords
        IdxVolId = strcmpi(OptionContSeriesData.underlying_vol_id{iR},SSDataPriceVol.vol_id);
        if any(IdxVolId)
            PrevVolData = SSDataPriceVol.vol(IdxVolId);
            if isnan(PrevVolData)
                PrevVolData = 0;
            end
        else
            PrevVolData = 0;
        end
        
        IdxVolId = strcmpi(OptionContSeriesData.underlying_vol_id{iR},SDataPriceVol.vol_id);
        if any(IdxVolId)
            CurrVolData = SDataPriceVol.vol(IdxVolId);
            if isnan(CurrVolData)
                CurrVolData = 0;
            end
        else
            CurrVolData = 0;
        end
        
        VolChange = CurrVolData - PrevVolData;
        
        OptionContMonth = OptionContSeriesData.underlying_vol_id_cont_month(iR);
        VolId   = OptionContSeriesData.underlying_vol_id(iR);
        
        RowVolChangeData = [SDate,OptionContMonth,VolChange,VolId];
        OutVolChangeData = [OutVolChangeData;RowVolChangeData];
        
    end
    ProductCode_SDate = [ProductCode_SDate ; [Current_ProductCode,SDate] ];
end

VolFilename = getXLSFilename('Vol_Change_Data');

VolHeader = {'settlement_date','cont_month','vol_change','vol_id'};

OutVolChangeData   = [VolHeader;OutVolChangeData];

[OutVolChangeData] = Get_Exact_Product_SDate_Data(OutVolChangeData,ProductCode_SDate,0) ; %% New

CleanedVolData = clean_price_vol_change_data(OutVolChangeData);  %% cell2dataset(OutVolChangeData)

CleanedVolData   = dataset2cell(CleanedVolData);

CleanedVolData(:,1) = Sys_SDate;

OutFilename = getXLSFilename('CleanedVolData');
xlswrite(OutFilename,CleanedVolData);

Data = CleanedVolData(2:end,1:3);
TableName = 'var_cont_vol_change_table';
set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Sys_SDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end
set(ObjDB,'AutoCommit','on');
upload_in_database(TableName,Data);

VolHeader = {'settlement_date','cont_month','vol_change'};
xlswrite(VolFilename,[VolHeader;sortrows(Data)]);

try
    SharedFolder = get_reports_foldername;
    DestinationFile = fullfile(SharedFolder,OutFilename);
    movefile(OutFilename,DestinationFile);   
catch ME
    OutErrorMsg = cellstr(ME.message);
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end