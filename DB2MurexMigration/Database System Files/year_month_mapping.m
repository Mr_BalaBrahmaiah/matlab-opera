function Month_val = year_month_mapping (DSData,Mapping_Tab_Data)

           [rr,cc]=size(DSData);
           Month_val=[];
           for jj= 1:1:rr
           
           Month_col=DSData.Month{jj};
           %Pre_year_month1
           if any(isnan(Month_col))
               %continue; % disp('No Data');
           else
               
            [PreMonth,PostMonth] = strtok(Month_col,'.'); %
            PostMonth = strrep(PostMonth,'.','');
            
             PreMonth_last2digits =PreMonth(end-1:end);
             PostMonth_last2digits = PostMonth(end-1:end);
             
             Pre_NumberIndex = isempty(str2num(char(PreMonth_last2digits)));
             
             Post_NumberIndex = isempty(str2num(char(PostMonth_last2digits)));

             Mapping_Tab_digit=Mapping_Tab_Data.single_digit_year;
                        
            if ~(Pre_NumberIndex)

            else
                Pre_Last_ltr=PreMonth(end);
                
              for j=1:1:length(Mapping_Tab_digit)
                Mapping_Tab_digit_val=Mapping_Tab_digit(j);
                if isequal(str2double(Pre_Last_ltr),Mapping_Tab_digit_val)
                    Pre_Year_ltr = Mapping_Tab_Data.double_digit_year(j);
                    PreMonth=strrep(PreMonth,Pre_Last_ltr,num2str(Pre_Year_ltr));
                    break;
                end   
             end
            
            end

            if ~(Post_NumberIndex)

            else
                Post_Last_ltr=PostMonth(end);
                
                for j=1:1:length(Mapping_Tab_digit)
                Mapping_Tab_digit_val=Mapping_Tab_digit(j);
                if isequal(str2double(Post_Last_ltr),Mapping_Tab_digit_val)
                    Post_Year_ltr = Mapping_Tab_Data.double_digit_year(j);
                    PostMonth=strrep(PostMonth,Post_Last_ltr,num2str(Post_Year_ltr));
                    break;
                end   
               end
            
            end 

            Pre_year_month=string(PreMonth); 
            Post_year_month=string(PostMonth);
            Month_col = char(strcat(Pre_year_month,'.',Post_year_month));
%             Month1_col=string(Month1_col);
           end
              %Month1_val = vertcat(string(Month1_val),string(Month1_col));
              Month_val{jj,1} = Month_col;

           end
           
 end

       