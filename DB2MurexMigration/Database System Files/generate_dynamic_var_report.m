function [OutErrorMsg,OutData] = generate_dynamic_var_report(subportfolio,trade_type,product_code,contract_month,call_put_id,strike,lots)

OutErrorMsg = {'No errors'};
OutData = '';

try
    
    %%  Securiy ID 
    
    FUT_Index = cellStrfind_exact(trade_type , {'FUT'});
    OFUT_Index = cellStrfind_exact(trade_type , {'OFUT'});

    Security_ID = cell(size(subportfolio,1),1); %% Initialize length

    Security_ID(FUT_Index,1) = strcat(product_code(FUT_Index),{' '},contract_month(FUT_Index) );
    Security_ID(OFUT_Index,1) = strcat(product_code(OFUT_Index),{' '},contract_month(OFUT_Index),call_put_id(OFUT_Index),{' '}, strrep(cellstr(num2str(cell2mat(strike(OFUT_Index)))),' ',''));

    %% Output
    OutData = Security_ID ;
    
    
    
catch ME
OutErrorMsg = cellstr(ME.message);

end