function  [OutErrorMsg ,OutXLSName ] = generate_EURUSD_report() %% InBUName

try
    InBUName = 'rms';
    
    OutErrorMsg= {'No Errors'};
    OutXLSName = getXLSFilename('Automate_EURUSD_Report');
    
    %%
    ObjDB = connect_to_database;
    
    View_Name = strcat('helper_reconview_eurusd_positions_json_view_',InBUName);
    
    %% Fetch DB Data
    
    [ColNames,DBData] = Fetch_DB_Data(ObjDB,[],View_Name);
    
    if(strcmpi(DBData,'No Data'))
        OutErrorMsg = {'Not Getting Data from DB'};
        return;
    end
    
    %% Consolidate the Data
    
    UniqueFields = {'maturity_date','counterparty_parent'};
    SumFields = {'original_lots'};
    OutputFields = [UniqueFields, SumFields];
    WeightedAverageFields = [];
    [Consolidate_Header,Consolidate_Data] = consolidatedata(ColNames, DBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Pivot_Data  = pivottable(Consolidate_Data, 1, 2,3, @sum);
    Pivot_Data(1,1) = {'Maturity_Date'};
    
    %% Excel Write
    
    xlswrite(OutXLSName,[ColNames;DBData],'RawData');
    xlswrite(OutXLSName,[Consolidate_Header;Consolidate_Data],'Consolidate_Data');
    
    try
        [RowSum,~] = cell2sum_Row_Col(Pivot_Data(2:end,2:end));
        Pivot_Data = [Pivot_Data , [{'Grand Total'}; RowSum] ];
        [~,ColSum] = cell2sum_Row_Col(Pivot_Data(2:end,2:end));
        Pivot_Data = [Pivot_Data ; [{'Grand Total'} , ColSum] ];
    catch
        
    end
    
    xlswrite(OutXLSName,Pivot_Data,'Pivot_Data');
    
    try
        xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1') ;
    catch
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end