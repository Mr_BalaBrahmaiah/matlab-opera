function mail_qsvtbook_report

QSVTReport = generate_qsvtbook_report;

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

try
configure_mail_settings;

sendmail({'cfsg.mo@olamnet.com','Raghavendra.Sn@olamnet.com'}, 'QSVT Book Report', ['Attached is the QSVT Book Report for COB ''',SettlementDate,''''],{QSVTReport});
catch
end

pause(5);

try
    [~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
    ReportNames = ConfigFileData(2:end,1);
    OutputFolders = ConfigFileData(2:end,2);
    IdxFound = strcmpi('MOReports',ReportNames);
    DestinationFolder = OutputFolders{IdxFound};
    
    LogFilename = ['DB_automation_log_',datestr(today),'.txt'];
    
    try
        TempDestinationFolder = [DestinationFolder,'\QSVT Book Report'];
        OutQSVTReport = fullfile(TempDestinationFolder,QSVTReport);
        move_reports(QSVTReport,OutQSVTReport,'MO QSVT Book Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving QSVT Book Report under MO Reports!');
    end    
    
catch
end