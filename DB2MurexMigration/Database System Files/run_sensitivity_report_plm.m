function run_sensitivity_report_plm(Check_Str)

%%FUTURE M2M with PRICEMOVE REPORT
if(Check_Str)
    InBUName = 'plm';
    InputFilterValues = {...
        'derivative_type',{'future'};'deal_status',{'live'}};
    InputScenarios = [{'on',5,2,'Percentage Move'};{'off',3,5,''};{'off',3,10,''}];
    SummmaryParameters = {'subportfolio','product_code','instrument','contract_month'};
    PricingType = 'settle';
    OutputFields = {'active_lots','price','pricemove','m2m_usd'};
    
else
    
    %%OPTIONS with PRICEMOVE & VOLMOVE REPORT
    InBUName = 'plm';
    InputFilterValues = {...
        'derivative_type',{'vanilla_call','vanilla_put'};'deal_status',{'live'}}; %% deal_status
    InputScenarios = [{'on',5,2,'Percentage Move'};{'on',3,2,''};{'off',3,10,''}];
    SummmaryParameters = {'subportfolio','product_code','instrument','contract_month','strike','derivative_type'};
    PricingType = 'settle';
    OutputFields = {'active_lots','price','pricemove','vol','volmove','m2m_usd','delta','gamma','vega','theta'};
    
end

%%generate_sensitivity_report

tic;
try
    [OutErrorMsg,OutFilename] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields)
    
    %     msgbox(['Without Parfor : ', num2str(toc) ]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

