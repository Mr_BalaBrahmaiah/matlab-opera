function [OutErrorMsg,OutXLSfileName] = cancel_monthly_reset_deals(InBUName,User_Passed_Counterparty)

% InBUName = 'qf5';
% User_Passed_Counterparty = 'MORGAN STANLEY';


try
    OutErrorMsg = {'No Errors'};
    OutXLSfileName = '';
    
    ObjDB = connect_to_database ;
    
    ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %% Validate the Settle Date  & find the Reset Month
    
    Current_Month = str2double(datestr(char(ValueDate),5));
    Current_Year = str2double(datestr(char(ValueDate),10));
    
    if(Current_Month==1)
        %         PrevEOM_Date = datestr( eomdate(Current_Year-1, 12) ,'yyyy-mm-dd'); %% If Jan Comes Shold be Dec
        PrevEOM_Date = datestr( lbusdate(Current_Year-1,12) ,'yyyy-mm-dd');
    else
        PrevEOM_Date = datestr( eomdate(Current_Year, Current_Month-1) ,'yyyy-mm-dd');
    end
    
    Reset_Month = datestr(PrevEOM_Date,'mmm-yy'); %% We need to get Previouse Reset Month to Cancel Deals
    
    
    %% Fetch DB Data
    
    Raw_TableName = strcat('deal_ticket_table_with_latest_versions_active_lots_view_',char(InBUName))  ;
    
    
    Monthly_Reset_Tbl_Name = ['monthly_reset_date_table_',char(InBUName)];
    SqlQuery_1 = ['select distinct(counterparty_parent) from ',char(Monthly_Reset_Tbl_Name)];
    [~,Unique_Counterparty] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    
    Unique_Counterparty = User_Passed_Counterparty ;
    
    %%
    
    %         All_Counterparty = ['''',strrep(convertCell2Char(Unique_Counterparty),',',''','''),'''']; %% if we need without for loop
    
    Overall_ErrorMsg = [];
    Overall_Cancel_ResetDeals = [];
    Overall_Cancel_OffsetDeals = [];
    Overall_Cancel_ActualDeals = [];
    
    for i = 1 : length(Unique_Counterparty)
        
        Current_Counterparty =  Unique_Counterparty(i) ; %% Unique_Counterparty(i) if its Loop; All_Counterparty
        
        Current_Counterparty_Query = strcat('''',strrep(convertCell2Char(Current_Counterparty),',',''','''),'''');
        
        SqlQuery_2 = ['select reset_date from ',char(Monthly_Reset_Tbl_Name),' where reset_month =''',char(Reset_Month),...
            ''' and counterparty_parent in (',char(Current_Counterparty_Query),') '];
        [~,Current_Reset_Date] = Fetch_DB_Data(ObjDB,SqlQuery_2);
        
        %% Find Last Reset Date
        try
            Last_ResetDate_EOM_Date = datestr( eomdate(Current_Year, Current_Month-2) ,'yyyy-mm-dd');
            Last_Reset_Month = datestr(Last_ResetDate_EOM_Date,'mmm-yy'); % We are in AUG-18, it will give JUN-18
            
            SqlQuery_LastReset = ['select reset_date from ',char(Monthly_Reset_Tbl_Name),' where reset_month =''',char(Last_Reset_Month),...
                ''' and counterparty_parent in (',char(Current_Counterparty_Query),') '];
            
            [~,Last_Reset_Date] = Fetch_DB_Data(ObjDB,SqlQuery_LastReset);
            
            SqlQuery_LastReset_TranscDate = ['select settlement_date from vdate_sdate_table where value_date = ''',char(Last_Reset_Date),''' '];
            [~,LastReset_TranscDate] = Fetch_DB_Data(ObjDB,SqlQuery_LastReset_TranscDate);
            
        catch
            
        end
        
        %% Fetch Reset Deals  from RawData
        
        SqlQuery_3 = ['select * from ',char(Raw_TableName),'  where counterparty_parent in (',char(Current_Counterparty_Query),...
            ') and reset_date = ''',char(Current_Reset_Date),''' and trade_id not like ''NET-%'' '];
        [ResetDeal_Cols,ResetDeal_Data] = Fetch_DB_Data(ObjDB,SqlQuery_3);
        
        OrinalLots_ResetDeals_Col = cellStrfind_exact(ResetDeal_Cols,{'original_lots'});
        MarketAction_ResetDeals_Col = cellStrfind_exact(ResetDeal_Cols,{'market_action'});
        Lots_2_Netted_ResetDeals_Col = cellStrfind_exact(ResetDeal_Cols,{'lots_to_be_netted'});
        TransactionDate_ResetDeals_Col = cellStrfind_exact(ResetDeal_Cols,{'transaction_date'});
        
        ResetDeal_Data(:,OrinalLots_ResetDeals_Col) = num2cell(cell2mat(ResetDeal_Data(:,OrinalLots_ResetDeals_Col)) .* -1 );
        ResetDeal_Data(:,Lots_2_Netted_ResetDeals_Col) = num2cell(cell2mat(ResetDeal_Data(:,Lots_2_Netted_ResetDeals_Col)) .* -1 );
        
        Bought_Index = strcmpi(ResetDeal_Data(:,MarketAction_ResetDeals_Col),'Bought');
        Sold_Index = strcmpi(ResetDeal_Data(:,MarketAction_ResetDeals_Col),'Sold');
        ResetDeal_Data(Bought_Index,MarketAction_ResetDeals_Col) = cellstr('Sold');
        ResetDeal_Data(Sold_Index,MarketAction_ResetDeals_Col) = cellstr('Bought');
        
        VersionComment_ResetDeals_Col = cellStrfind_exact(ResetDeal_Cols,{'version_comments'});
        ResetDeal_Data(:,VersionComment_ResetDeals_Col) = cellstr('monthly_reset_cancel');
        
        Transaction_Date = unique(ResetDeal_Data(:,TransactionDate_ResetDeals_Col));
        
        %% Fetch OFFSET Deals
        
        SqlQuery_4 = ['select * from ',char(Raw_TableName),' where counterparty_parent in (',char(Current_Counterparty_Query)...
            ,') and transaction_date = ''',char(Transaction_Date),''' and version_comments =''offset_deal_resetted'' and trade_id like ''DUP%'' '  ];
        
        [OffsetDeal_Cols,OffsetDeal_Data] = Fetch_DB_Data(ObjDB,SqlQuery_4);
        
        %         Offset_TradeID_Col = cellStrfind_exact(OffsetDeal_Cols,{'trade_id'});
        %         Offset_TradeID = OffsetDeal_Data(:,Offset_TradeID_Col);
        %         OffsetDeal_Query = strcat(['delete from ',char(Raw_TableName)],{' '},' where trade_id = ''',Offset_TradeID,''' ;');
        
        %% Fetch Actual Deals
        
        SqlQuery_5 = ['select * from ',char(Raw_TableName),' where counterparty_parent in (',char(Current_Counterparty_Query)...
            ,')  and version_comments =''actual_deal_resetted'' and deal_status = ''dead'' and transaction_date = ''',char(LastReset_TranscDate),...
            ''' and reset_date = ''',char(Last_Reset_Date),''' union all select * from ',char(Raw_TableName),' where counterparty_parent =''',char(Current_Counterparty)...
            ,''' and version_comments =''actual_deal_resetted'' and deal_status = ''dead'' and transaction_date > ''',char(LastReset_TranscDate),''' '];
        
        
        [ActualDeal_Cols,ActualDeal_Data] = Fetch_DB_Data(ObjDB,SqlQuery_5);
        
        DealStatus_ActualDeal_Col = cellStrfind_exact(ActualDeal_Cols,{'deal_status'});
        ActualDeal_Data(:,DealStatus_ActualDeal_Col) = cellstr('live');
        
        VersionComment_ActualDeal_Col = cellStrfind_exact(ActualDeal_Cols,{'version_comments'});
        ActualDeal_Data(:,VersionComment_ActualDeal_Col) = cellstr('actual_deal_reset_cancel');
        
        %         Actual_TradeID_Col = cellStrfind_exact(ActualDeal_Cols,{'trade_id'});
        %         Actual_TradeID = ActualDeal_Data(:,Actual_TradeID_Col);
        %         ActualDeal_Query = strcat(['update ',char(Raw_TableName)],{' '},' set deal_status=''live'' where trade_id = ''',Actual_TradeID,''' ;');
        
        
        
        %% Make it Single Variable
        
        Overall_Cancel_ResetDeals = [Overall_Cancel_ResetDeals ; ResetDeal_Data];
        Overall_Cancel_OffsetDeals = [Overall_Cancel_OffsetDeals ; OffsetDeal_Data];
        Overall_Cancel_ActualDeals = [Overall_Cancel_ActualDeals ; ActualDeal_Data];
        
    end
    
    %% Make Delete & Update Queries for Offset & Actual Deals
    
    Raw_TableName_1 = strcat('deal_ticket_table_',char(InBUName));
    Raw_TableName_2 = strcat('cloned_deal_ticket_table_',char(InBUName));
    
    Offset_TradeID_Col = cellStrfind_exact(OffsetDeal_Cols,{'trade_id'});
    Offset_TradeID = Overall_Cancel_OffsetDeals(:,Offset_TradeID_Col);
    OffsetDeal_Query = cell(size(Offset_TradeID,1),3);
    OffsetDeal_Query(:,1) = strcat(['delete from ',char(Raw_TableName_1)],{' '},' where trade_id = ''',Offset_TradeID,''' ;');
    OffsetDeal_Query(:,3) = strcat(['delete from ',char(Raw_TableName_2)],{' '},' where trade_id = ''',Offset_TradeID,''' ;');
    
    Actual_TradeID_Col = cellStrfind_exact(ActualDeal_Cols,{'trade_id'});
    Actual_TradeID = Overall_Cancel_ActualDeals(:,Actual_TradeID_Col);
    ActualDeal_Query = strcat(['update ',char(Raw_TableName_1)],{' '},' set deal_status=''live'',version_comments=''actual_deal_reset_cancel'' where trade_id = ''',Actual_TradeID,''' ;');
    
    %% Excel SHeet Write
    
    OutXLSfileName = getXLSFilename('Cancel_Monthly_Reset');
    
    xlswrite(OutXLSfileName, [ResetDeal_Cols;Overall_Cancel_ResetDeals],'cancel_reset_deals');
    
    xlswrite(OutXLSfileName, [OffsetDeal_Cols;Overall_Cancel_OffsetDeals],'cancel_offset_deals');
    xlswrite(OutXLSfileName, OffsetDeal_Query,'offset_deals_query');
    
    xlswrite(OutXLSfileName, [ActualDeal_Cols;Overall_Cancel_ActualDeals],'cancel_actual_deals');
    xlswrite(OutXLSfileName, ActualDeal_Query,'acutal_deals_query');
    
    
    try
        xls_delete_sheets(fullfile(pwd,OutXLSfileName)) ;
    catch
        
    end
    
    %%
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end