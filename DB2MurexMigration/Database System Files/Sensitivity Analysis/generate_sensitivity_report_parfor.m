function [OutErrorMsg,OutFilename] = generate_sensitivity_report_parfor(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields,varargin)

OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_sensitivity_report_view_cache_table';
    [InReportHeader,InReportData] = read_from_database(ViewName,0,'',InBUName);
    
    % InputFilterValues = {'subportfolio',{'MM-BO-CBT-OID','MM-BO-CBT-OIV', 'MM-S-CBT-OIV','MM-S-CBT-OID','RMS-C-CBT-OIR'},...
    %     'counterparty',{'Bache','BOM JESUS','Olam International'},...
    %     'strike',{40,1000,24,2500,500.25,400.25,18.5},...
    %     'transaction_date',{'2014-07-01','2014-08-30'}};
    
    % InputScenarios = {'PriceMove',{NumberOfSteps,StepSize},'VolMove',{NumberOfSteps,StepSize},'TimeMove',{NumberOfSteps,StepSize}};
    
    % InputFilterValues = {'transaction_date',{'2014-07-01','2014-09-09'}};
    % InputFilterValues = {'subportfolio',{'MM-BO-CBT-OIV'}};
    % SummmaryParameters = {'subportfolio'};
    
    %% Remove all dead deals and just consider only live deals
    
    TempData = cell2dataset([InReportHeader;InReportData]);
    
    ValueDate    = datenum(TempData.value_date,'yyyy-mm-dd');
    TempMaturity = TempData.maturity_date;
    TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd'));
    MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
    % IdxLive = ValueDate <= MaturityDate;
    IdxDead = ValueDate > MaturityDate;
    TempData(IdxDead,:) = [];
    
    TempData = dataset2cell(TempData);
    
    InReportHeader = TempData(1,:);
    InReportData = TempData(2:end,:);
    
    %%
    if isempty(SummmaryParameters)
        SummmaryParameters = {'subportfolio'};
    end
    % process InputScenarios
    % it is designed as 3 x 4 cell array
    % %                         perform_sensitivity 	noofsteps		stepsize		stepparameter_parameter
    % % pricemove              'on'                     3               2
    % % volitilitymove          []                      []              []
    % % timemove
    PriceNumSteps = 0; PriceStepSize = 0;
    VolNumSteps   = 0; VolStepSize   = 0;
    TimeNumSteps  = 0; TimeStepSize  = 0;
    PriceStepParameter = 'Price Tick';
    if ~isempty(InputScenarios)
        IsPriceMove = strcmpi(InputScenarios{1,1},'on');
        if IsPriceMove
            PriceNumSteps = InputScenarios{1,2};
            if isempty(PriceNumSteps)
                PriceNumSteps = 1;
            end
            PriceStepSize = InputScenarios{1,3};
            if isempty(PriceStepSize)
                PriceStepSize = 0;
            end
            PriceStepParameter = InputScenarios{1,4};
            if isempty(PriceStepParameter)
                PriceStepParameter = 'Price Tick';
            end
        end
        
        IsVolMove = strcmpi(InputScenarios{2,1},'on');
        if IsVolMove
            VolNumSteps   = InputScenarios{2,2};
            if isempty(VolNumSteps)
                VolNumSteps = 1;
            end
            VolStepSize   = InputScenarios{2,3};
            if isempty(VolStepSize)
                VolStepSize = 0;
            else
                VolStepSize = VolStepSize / 100;
            end
        end
        
        IsTimeMove = strcmpi(InputScenarios{3,1},'on');
        if IsTimeMove
            TimeNumSteps  = InputScenarios{3,2};
            if isempty(TimeNumSteps)
                TimeNumSteps = 1;
            end
            TimeStepSize  = InputScenarios{3,3};
            if isempty(TimeStepSize)
                TimeStepSize = 0;
            end
        end
    end
    
    PosMaturity  = strcmpi('maturity_date',InReportHeader);
    IdxEmptyMaturity = cellfun(@isempty,InReportData(:,PosMaturity));
    if any(IdxEmptyMaturity)
        InReportData(IdxEmptyMaturity,PosMaturity) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
    end
    
    PosTradeDate  = strcmpi('transaction_date',InReportHeader);
    TradeDates = datenum(InReportData(:,PosTradeDate),'yyyy-mm-dd');
    StartTradeDate = min(TradeDates);
    EndTradeDate = max(TradeDates);
    
    
    if isempty(InputFilterValues)
        IdxReportData = logical(zeros(size(InReportData,1),1));
    else
        FilterKeyNames = InputFilterValues(:,1);
        
        for iKey = 1:length(FilterKeyNames)
            KeyName = FilterKeyNames{iKey};
            if iKey == 1
                IdxReportData = getValueFromKeyValuePair(KeyName);
            else
                IdxTempKey = getValueFromKeyValuePair(KeyName);
                IdxReportData = IdxReportData & IdxTempKey;
            end
        end
    end
    
    if any(IdxReportData)
        % find the selected records and compute the active lots, net the data
        % before doing the scenario analysis
        SelData = InReportData(IdxReportData,:);
        
        [SelData,SelHeader] = calcActiveLots(SelData,InReportHeader,StartTradeDate,EndTradeDate);
        TradeDates = datenum(SelData(:,PosTradeDate),'yyyy-mm-dd');
        IdxRemove = (TradeDates < StartTradeDate) | (TradeDates > EndTradeDate);
        SelData(IdxRemove,:) = [];
        
        if strcmpi(PriceStepParameter,'Percentage Move')
            OutPriceMoveParameter = 'pricemove_in_percentage';
        else
            OutPriceMoveParameter = 'pricemove_in_pricetick';
        end
        SumFields = {'original_lots','active_lots','delta','delta_in_metric_tonnes','gamma','vega','theta','original_amount_paid',...
            'm2m_usd','m2m_nc', 'realised_m2m_usd','unrealised_m2m_usd','realised_m2m_nc','unrealised_m2m_nc'};
        OutputFields  = [SummmaryParameters,OutputFields];
        WeightedAverageFields = [];
        OutputFields = lower(OutputFields);
        SummmaryParameters = lower(SummmaryParameters);
        % check if the price move is selected to be available in output fields,
        % if selected only, add the pricemove parameter field
        if ismember('pricemove',OutputFields)
            IdxParameter = find(strcmpi('pricemove',OutputFields));
            OutputFields = [OutputFields(1:IdxParameter),OutPriceMoveParameter,OutputFields(IdxParameter+1:end)];
        end
        % check if the delta is selected to be available in output fields,
        % if selected only, add the delta_in_metric_tonnes field
        if ismember('delta',OutputFields)
            IdxParameter = find(strcmpi('delta',OutputFields));
            OutputFields = [OutputFields(1:IdxParameter),'delta_in_metric_tonnes',OutputFields(IdxParameter+1:end)];
        end
        
        if nargin == 7 % output filename is given by user in front end
            Filename = varargin{1};
            [~,OutFilename,Ext] = fileparts(Filename);
            OutFilename = getXLSFilename(OutFilename);
        else % if the output filename is not given by user, assign the default name
            TempName = [char(PricingType),'_SensitivityReport'];
            OutFilename = getXLSFilename(TempName);
        end
        
        %% Parfor Loop Sub Function  %% calcPriceForScenarios_SubFunction_Struct
        OutFilename = calcPriceForScenarios_SubFunction(InBUName,SelData,InReportHeader,PricingType,...
            PriceStepParameter,PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
            SummmaryParameters,SumFields,OutputFields,WeightedAverageFields,OutFilename);
        
        if(strcmpi(InBUName,'cot'))
            [OutFilename] = create_OPS_Cotton_Summary_Report(OutFilename,'Summary Report',SummmaryParameters,InBUName) ; %% Make_OPS_Cotton_Summary_Report
        end
        [OutFilename] = create_risk_matrix(OutFilename,SummmaryParameters,OutputFields,InBUName);
        
        delete(gcp('nocreate'));
    else
        OutErrorMsg = {'No input data selected for the analysis!'};
        OutFilename = '';
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end
    function IdxValue = getValueFromKeyValuePair(KeyName)
        disp('KeyName:'); disp(KeyName);
        IdxKey =  find(strcmpi(InputFilterValues,KeyName));
        if ~isempty(IdxKey)
            InValues = InputFilterValues{IdxKey,2};
            disp('Values');disp(InValues);
            %             if strcmpi(KeyName,'maturity')
            %                 InValues = lower(InValues);
            %                 if ismember('all',InValues)
            %                     IdxValue = logical(ones(size(InReportData,1),1));
            %                 elseif any(ismember({'live','dead'},InValues))
            %                     PosValueDate = strcmpi('value_date',InReportHeader);
            %                     ValueDate    = datenum(InReportData(:,PosValueDate),'yyyy-mm-dd');
            %                     MaturityDate = datenum(InReportData(:,PosMaturity),'yyyy-mm-dd');
            %                     if all(ismember({'live','dead'},InValues)) % if both live and dead are selected
            %                         IdxValue = logical(ones(size(MaturityDate)));
            %                     elseif ismember('dead',InValues)
            %                         IdxValue = ValueDate > MaturityDate;
            %                     else
            %                         IdxValue = ValueDate <= MaturityDate;
            %                     end
            %                 else
            %                     IdxValue = logical(zeros(size(InReportData,1),1));
            %                 end
            %                 return;
            %             end
            PosKeyName = strcmpi(KeyName,InReportHeader);
            DBKeyValues = InReportData(:,PosKeyName);
            % class of this DBKeyValues can be either double or cell
            % double, if the field is numeric in db
            % all other fields will be of class cell
            disp(class(cell2mat(InValues)));
            if strcmpi(class(cell2mat(InValues)),'double')
                InValues = cell2mat(InValues);
                DBKeyValues = utilgetvalidnumericarray(DBKeyValues);
                IdxValue = ismember(DBKeyValues,InValues);
            else
                if ~isempty(cellStrfind(KeyName,'date'))
                    % to handle the dates
                    if strcmpi(KeyName,'transaction_date')
                        IdxValue = logical(ones(size(DBKeyValues)));
                        StartTradeDate = datenum(InValues{1},'yyyy-mm-dd');
                        EndTradeDate   = datenum(InValues{2},'yyyy-mm-dd');
                        %                     else
                        %                         StartDate = datenum(InValues{1},'yyyy-mm-dd');
                        %                         EndDate   = datenum(InValues{2},'yyyy-mm-dd');
                        %                         DBDateValues = datenum(DBKeyValues,'yyyy-mm-dd');
                        %                         IdxValue = (DBDateValues >= StartDate) & (DBDateValues <= EndDate);
                    end
                    
                else % check if all is selected or specific inputs
                    if ismember('all',InValues)
                        IdxValue = logical(ones(size(DBKeyValues)));
                    else
                        IdxKeyFound = cellStrfind(upper(DBKeyValues),upper(InValues));
                        IdxValue = logical(zeros(size(DBKeyValues)));
                        IdxValue(IdxKeyFound) = 1;
                    end
                end
            end
            
        else
            IdxValue = logical(zeros(size(InReportData,1),1));
        end
        
    end
end