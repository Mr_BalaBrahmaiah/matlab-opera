function varargout = sensitivity_analysis(varargin)
% SENSITIVITY_ANALYSIS MATLAB code for sensitivity_analysis.fig
%      SENSITIVITY_ANALYSIS, by itself, creates a new SENSITIVITY_ANALYSIS or raises the existing
%      singleton*.
%
%      H = SENSITIVITY_ANALYSIS returns the handle to a new SENSITIVITY_ANALYSIS or the handle to
%      the existing singleton*.
%
%      SENSITIVITY_ANALYSIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SENSITIVITY_ANALYSIS.M with the given input arguments.
%
%      SENSITIVITY_ANALYSIS('Property','Value',...) creates a new SENSITIVITY_ANALYSIS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sensitivity_analysis_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sensitivity_analysis_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sensitivity_analysis

% Last Modified by GUIDE v2.5 26-Sep-2014 13:42:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sensitivity_analysis_OpeningFcn, ...
                   'gui_OutputFcn',  @sensitivity_analysis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sensitivity_analysis is made visible.
function sensitivity_analysis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sensitivity_analysis (see VARARGIN)

handles.MatFilename = ['SensitivityAnalysisInput_',datestr(today,'yyyy-mm-dd'),'.mat'];

QuestStr = sprintf('Select the way you would like to read the data!\nSelect the option ''From Database'' if you run the report, the first time for the valuation date.\nSelect the option ''From saved view data'' if you have both the input and pricing views saved earlier.\nThe selection will be used for all the runs using this instance');
Choice = questdlg(QuestStr,'Data read selection','From Database','From saved view data','From Database');

handles.IsFromDB = 0;

if strcmpi(Choice,'From Database')   
    handles.IsFromDB = 1;
end

handles.SettlePricingData = [];
handles.SettlePricingHeader = [];
handles.TradersPricingData = [];
handles.TradersPricingHeader = [];

if handles.IsFromDB
    ViewName = 'helper_sensitivity_report_view';
    [InReportHeader,InReportData] = read_from_database(ViewName,0);
    DBReportData = cell2dataset([InReportHeader;InReportData]);
else
    try
        load(handles.MatFilename);
        DBReportData = cell2dataset([InReportHeader;InReportData]);
        if exist('SettlePricingData','var')
            handles.SettlePricingData = SettlePricingData;
        end
        if exist('SettlePricingHeader','var')
            handles.SettlePricingHeader = SettlePricingHeader;
        end
        if exist('TradersPricingData','var')
            handles.TradersPricingData = TradersPricingData;
        end
        if exist('TradersPricingHeader','var')
            handles.TradersPricingHeader = TradersPricingHeader;
        end
    catch
        errordlg('Saved data could not be read, hence reading the data from DB!');
        ViewName = 'helper_sensitivity_report_view';
        [InReportHeader,InReportData] = read_from_database(ViewName,0);
        DBReportData = cell2dataset([InReportHeader;InReportData]);
    end
end

% set all the ui parameters
set(handles.rbTraders,'Value',1);
set(handles.rbSettle,'Value',0);

set(handles.edtValueDate,'string',datestr(DBReportData.value_date{1},'dd-mmm-yyyy'));

set(handles.lbxPortfolio,'string',unique(DBReportData.subportfolio),'value',[]);
Counterparty = unique(upper(DBReportData.counterparty));
Counterparty(cellfun(@isempty,Counterparty)) =[];
set(handles.lbxCounterparty,'string',Counterparty,'value',[]);
ContractMonth = unique(upper(DBReportData.contract_month));
ContractMonth(cellfun(@isempty,ContractMonth)) =[];
set(handles.lbxContractMonth,'string',ContractMonth,'value',[]);
set(handles.lbxDerivativeType,'string',unique(lower(DBReportData.derivative_type)),'value',[]);
set(handles.lbxInstrumentClass,'string',unique(lower(DBReportData.instrument_class)),'value',[]);
set(handles.lbxMarketAction,'string',unique(lower(DBReportData.market_action)),'value',[]);
set(handles.lbxStrike,'string',num2str(unique(DBReportData.strike(~isnan(DBReportData.strike)))),'value',[]);

handles.StartDate = datestr(min(datenum(DBReportData.transaction_date,'yyyy-mm-dd')),'yyyy-mm-dd');
% set(handles.edtFromDate,'string',StartDate);
handles.EndDate = datestr(max(datenum(DBReportData.transaction_date,'yyyy-mm-dd')),'yyyy-mm-dd');
% set(handles.edtToDate,'string',EndDate);
% handles.DBReportData = DBReportData;
handles.InReportHeader = InReportHeader;
handles.InReportData = InReportData;


set(handles.lbxMaturity,'string',{'dead','live'},'value',[]);
set(handles.lbxOutput,'string',{'price','pricemove','vol','volmove','time','timemove','delta','gamma','vega','theta','m2m_usd','m2m_nc','original_lots','active_lots'},'value',[]);

[x,map]=imread('calendar_image.jpg');
% I2=imresize(x, [42 113]);
set(handles.pbFromDate,'cdata',x);
set(handles.pbToDate,'cdata',x);

% Choose default command line output for sensitivity_analysis
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sensitivity_analysis wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sensitivity_analysis_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rbTraders.
function rbTraders_Callback(hObject, eventdata, handles)
% hObject    handle to rbTraders (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbTraders
if get(hObject,'Value')
    set(handles.rbSettle,'value',0);
end

% --- Executes on button press in rbSettle.
function rbSettle_Callback(hObject, eventdata, handles)
% hObject    handle to rbSettle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbSettle
if get(hObject,'Value')
    set(handles.rbTraders,'value',0);
end


function edtValueDate_Callback(hObject, eventdata, handles)
% hObject    handle to edtValueDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtValueDate as text
%        str2double(get(hObject,'String')) returns contents of edtValueDate as a double


% --- Executes during object creation, after setting all properties.
function edtValueDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtValueDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popPriceNoSteps.
function popPriceNoSteps_Callback(hObject, eventdata, handles)
% hObject    handle to popPriceNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popPriceNoSteps contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popPriceNoSteps


% --- Executes during object creation, after setting all properties.
function popPriceNoSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popPriceNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popPriceStepSize.
function popPriceStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to popPriceStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popPriceStepSize contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popPriceStepSize


% --- Executes during object creation, after setting all properties.
function popPriceStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popPriceStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popTimeNoSteps.
function popTimeNoSteps_Callback(hObject, eventdata, handles)
% hObject    handle to popTimeNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popTimeNoSteps contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popTimeNoSteps


% --- Executes during object creation, after setting all properties.
function popTimeNoSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popTimeNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popTimeStepSize.
function popTimeStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to popTimeStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popTimeStepSize contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popTimeStepSize


% --- Executes during object creation, after setting all properties.
function popTimeStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popTimeStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popVolNoSteps.
function popVolNoSteps_Callback(hObject, eventdata, handles)
% hObject    handle to popVolNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popVolNoSteps contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popVolNoSteps


% --- Executes during object creation, after setting all properties.
function popVolNoSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popVolNoSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popVolStepSize.
function popVolStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to popVolStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popVolStepSize contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popVolStepSize


% --- Executes during object creation, after setting all properties.
function popVolStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popVolStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxTimeMove.
function cbxTimeMove_Callback(hObject, eventdata, handles)
% hObject    handle to cbxTimeMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxTimeMove


% --- Executes on button press in cbxVolMove.
function cbxVolMove_Callback(hObject, eventdata, handles)
% hObject    handle to cbxVolMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxVolMove


% --- Executes on button press in cbxPriceMove.
function cbxPriceMove_Callback(hObject, eventdata, handles)
% hObject    handle to cbxPriceMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxPriceMove


% --- Executes on button press in cbxDerivativeTypeSummary.
function cbxDerivativeTypeSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxDerivativeTypeSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxDerivativeTypeSummary


% --- Executes on button press in cbxDerivativeTypeAll.
function cbxDerivativeTypeAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxDerivativeTypeAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxDerivativeTypeAll
NumContents = length(get(handles.lbxDerivativeType,'string'));    
if get(hObject,'Value')   
    set(handles.lbxDerivativeType,'value',1:NumContents);
else
    set(handles.lbxDerivativeType,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxDerivativeType.
function lbxDerivativeType_Callback(hObject, eventdata, handles)
% hObject    handle to lbxDerivativeType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxDerivativeType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxDerivativeType


% --- Executes during object creation, after setting all properties.
function lbxDerivativeType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxDerivativeType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxContractMonthSummary.
function cbxContractMonthSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxContractMonthSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxContractMonthSummary


% --- Executes on button press in cbxContractMonthAll.
function cbxContractMonthAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxContractMonthAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxContractMonthAll
NumContents = length(get(handles.lbxContractMonth,'string'));    
if get(hObject,'Value')   
    set(handles.lbxContractMonth,'value',1:NumContents);
else
    set(handles.lbxContractMonth,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxContractMonth.
function lbxContractMonth_Callback(hObject, eventdata, handles)
% hObject    handle to lbxContractMonth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxContractMonth contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxContractMonth


% --- Executes during object creation, after setting all properties.
function lbxContractMonth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxContractMonth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxCounterpartySummary.
function cbxCounterpartySummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxCounterpartySummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxCounterpartySummary


% --- Executes on button press in cbxCounterpartyAll.
function cbxCounterpartyAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxCounterpartyAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxCounterpartyAll
NumContents = length(get(handles.lbxCounterparty,'string'));    
if get(hObject,'Value')   
    set(handles.lbxCounterparty,'value',1:NumContents);
else
    set(handles.lbxCounterparty,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxCounterparty.
function lbxCounterparty_Callback(hObject, eventdata, handles)
% hObject    handle to lbxCounterparty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxCounterparty contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxCounterparty


% --- Executes during object creation, after setting all properties.
function lbxCounterparty_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxCounterparty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxPortfolioSummary.
function cbxPortfolioSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxPortfolioSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxPortfolioSummary


% --- Executes on button press in cbxPortfolioAll.
function cbxPortfolioAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxPortfolioAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxPortfolioAll
NumContents = length(get(handles.lbxPortfolio,'string'));    
if get(hObject,'Value')   
    set(handles.lbxPortfolio,'value',1:NumContents);
else
    set(handles.lbxPortfolio,'value',[]);
end
guidata(hObject,handles);


% --- Executes on selection change in lbxPortfolio.
function lbxPortfolio_Callback(hObject, eventdata, handles)
% hObject    handle to lbxPortfolio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxPortfolio contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxPortfolio

% --- Executes during object creation, after setting all properties.
function lbxPortfolio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxPortfolio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxMarketActionSummary.
function cbxMarketActionSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxMarketActionSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxMarketActionSummary


% --- Executes on button press in cbxMarketActionAll.
function cbxMarketActionAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxMarketActionAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxMarketActionAll
NumContents = length(get(handles.lbxMarketAction,'string'));    
if get(hObject,'Value')   
    set(handles.lbxMarketAction,'value',1:NumContents);
else
    set(handles.lbxMarketAction,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxMarketAction.
function lbxMarketAction_Callback(hObject, eventdata, handles)
% hObject    handle to lbxMarketAction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxMarketAction contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxMarketAction


% --- Executes during object creation, after setting all properties.
function lbxMarketAction_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxMarketAction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxInstrumentClassSummary.
function cbxInstrumentClassSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxInstrumentClassSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxInstrumentClassSummary


% --- Executes on button press in cbxInstrumentClassAll.
function cbxInstrumentClassAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxInstrumentClassAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxInstrumentClassAll
NumContents = length(get(handles.lbxInstrumentClass,'string'));    
if get(hObject,'Value')   
    set(handles.lbxInstrumentClass,'value',1:NumContents);
else
    set(handles.lbxInstrumentClass,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxInstrumentClass.
function lbxInstrumentClass_Callback(hObject, eventdata, handles)
% hObject    handle to lbxInstrumentClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxInstrumentClass contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxInstrumentClass


% --- Executes during object creation, after setting all properties.
function lbxInstrumentClass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxInstrumentClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxMaturitySummary.
function cbxMaturitySummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxMaturitySummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxMaturitySummary


% --- Executes on button press in cbxMaturityAll.
function cbxMaturityAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxMaturityAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxMaturityAll
NumContents = length(get(handles.lbxMaturity,'string'));    
if get(hObject,'Value')   
    set(handles.lbxMaturity,'value',1:NumContents);
else
    set(handles.lbxMaturity,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxMaturity.
function lbxMaturity_Callback(hObject, eventdata, handles)
% hObject    handle to lbxMaturity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxMaturity contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxMaturity

% --- Executes during object creation, after setting all properties.
function lbxMaturity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxMaturity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxStrikeSummary.
function cbxStrikeSummary_Callback(hObject, eventdata, handles)
% hObject    handle to cbxStrikeSummary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxStrikeSummary


% --- Executes on button press in cbxStrikeAll.
function cbxStrikeAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxStrikeAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxStrikeAll
NumContents = length(get(handles.lbxStrike,'string'));    
if get(hObject,'Value')   
    set(handles.lbxStrike,'value',1:NumContents);
else
    set(handles.lbxStrike,'value',[]);
end
guidata(hObject,handles);

% --- Executes on selection change in lbxStrike.
function lbxStrike_Callback(hObject, eventdata, handles)
% hObject    handle to lbxStrike (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxStrike contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxStrike


% --- Executes during object creation, after setting all properties.
function lbxStrike_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxStrike (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cbxTransactionDateAll.
function cbxTransactionDateAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxTransactionDateAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxTransactionDateAll
if get(hObject,'Value')
    set(handles.edtFromDate,'string',handles.StartDate);
    set(handles.edtToDate,'string',handles.EndDate);
else
	set(handles.edtFromDate,'string','');
    set(handles.edtToDate,'string','');
end
guidata(hObject,handles);

function edtFromDate_Callback(hObject, eventdata, handles)
% hObject    handle to edtFromDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtFromDate as text
%        str2double(get(hObject,'String')) returns contents of edtFromDate as a double


% --- Executes during object creation, after setting all properties.
function edtFromDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtFromDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edtToDate_Callback(hObject, eventdata, handles)
% hObject    handle to edtToDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtToDate as text
%        str2double(get(hObject,'String')) returns contents of edtToDate as a double


% --- Executes during object creation, after setting all properties.
function edtToDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtToDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lbxOutput.
function lbxOutput_Callback(hObject, eventdata, handles)
% hObject    handle to lbxOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxOutput contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxOutput

% --- Executes during object creation, after setting all properties.
function lbxOutput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function tlbrGenerateReport_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrGenerateReport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields] = getUIParameters(handles);

if strcmpi(PricingType,'settle') % settle pricing
    if isempty(handles.SettlePricingData) % the settle pricing data was not read before
        ViewName = 'input_security_settlement_value_view';
        % Read the values from database for derivative pricer
        [InPricerHeading,PricerData] = read_from_database(ViewName,0);
        handles.SettlePricingHeader = InPricerHeading;
        handles.SettlePricingData = PricerData;
    else
        InPricerHeading = handles.SettlePricingHeader;
        PricerData = handles.SettlePricingData;
    end
else % traders pricing
    if isempty(handles.TradersPricingData) % the traders pricing data was not read before
        ViewName = 'input_security_traders_value_view';
        % Read the values from database for derivative pricer
        [InPricerHeading,PricerData] = read_from_database(ViewName,0);
        handles.TradersPricingHeader = InPricerHeading;
        handles.TradersPricingData = PricerData;
    else
        InPricerHeading = handles.TradersPricingHeader;
        PricerData = handles.TradersPricingData;
    end
end

set(handles.figure1,'pointer','watch'); drawnow;
OutFilename = generate_sensitivity_report_ui(handles.InReportHeader,handles.InReportData,InPricerHeading,PricerData,...
    InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
drawnow; set(handles.figure1,'pointer','arrow'); 
if ~isempty(OutFilename)
    msgbox(['Sensitivity report is saved in ',OutFilename]);
end
guidata(hObject,handles);

% --------------------------------------------------------------------
function tlbrSaveView_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrSaveView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields] = getUIParameters(handles); %#ok<NASGU,ASGLU>

[Filename, Pathname] = uiputfile('*.mat', 'Save view as');
if ~isnumeric(Filename)
    ConfigFileName = fullfile(Pathname,Filename);
    save(ConfigFileName,'InputFilterValues','InputScenarios','SummmaryParameters','PricingType','OutputFields');
end
guidata(hObject,handles);

% --------------------------------------------------------------------
function tlbrNewView_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrNewView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% handles = setUIParameters(handles,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields)
handles = setUIParameters(handles,[],[],[],'traders',[]);
guidata(hObject,handles);

% --------------------------------------------------------------------
function tlbrLoadView_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrLoadView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Filename, Pathname] = uigetfile('*.mat', 'Open the view');
if ~isnumeric(Filename)
    ConfigFileName = fullfile(Pathname,Filename);
    load(ConfigFileName);
    handles = setUIParameters(handles,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
end
guidata(hObject,handles);

function SelectedValues = getSelectedValues(hObj)
contents = cellstr(get(hObj,'String'));
SelectedValues = contents(get(hObj,'Value'));
SelectedValues = SelectedValues';

function hObj = setValues(hObj,InValue)
contents = cellstr(get(hObj,'String'));
if isnumeric(InValue)
    SelIndex = find(ismember(str2double(contents),InValue));
else
SelIndex = find(ismember(contents,InValue));
end
set(hObj,'Value',SelIndex);

function [InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields] = getUIParameters(handles)

InputFilterValues= {};
if get(handles.cbxPortfolioAll,'value')
    subportfolio = {'all'};
else
    subportfolio = getSelectedValues(handles.lbxPortfolio);
end
if ~isempty(subportfolio)    
    InputFilterValues = [InputFilterValues;{'subportfolio',subportfolio}];
end

if get(handles.cbxCounterpartyAll,'value')
    counterparty = {'all'};
else
    counterparty = getSelectedValues(handles.lbxCounterparty);
end
if ~isempty(counterparty)
   InputFilterValues = [InputFilterValues;{'counterparty',counterparty}]; 
end

if get(handles.cbxContractMonthAll,'value')
    contract_month = {'all'};
else
    contract_month = getSelectedValues(handles.lbxContractMonth);
end
if ~isempty(contract_month)
    InputFilterValues = [InputFilterValues;{'contract_month',contract_month}];
end

if get(handles.cbxDerivativeTypeAll,'value')
    derivative_type = {'all'};
else
    derivative_type = getSelectedValues(handles.lbxDerivativeType);
end
if ~isempty(derivative_type)
    InputFilterValues = [InputFilterValues;{'derivative_type',derivative_type}];
end

if get(handles.cbxInstrumentClassAll,'value')
    instrument_class = {'all'};
else
    instrument_class = getSelectedValues(handles.lbxInstrumentClass);
end
if ~isempty(instrument_class)
    InputFilterValues = [InputFilterValues;{'instrument_class',instrument_class}];
end

if get(handles.cbxMarketActionAll,'value')
    market_action = {'all'};
else
    market_action = getSelectedValues(handles.lbxMarketAction);
end
if ~isempty(market_action)
    InputFilterValues = [InputFilterValues;{'market_action',market_action}];
end

if get(handles.cbxMaturityAll,'value')
    maturity = {'all'};
else
    maturity = getSelectedValues(handles.lbxMaturity);
end
if ~isempty(maturity)
    InputFilterValues = [InputFilterValues;{'maturity',maturity}];
end

if get(handles.cbxStrikeAll,'value')
    strike = {'all'};
else
    strike = str2double(getSelectedValues(handles.lbxStrike));
end
if ~isempty(strike)
    InputFilterValues = [InputFilterValues;{'strike',num2cell(strike)}];
end

if get(handles.cbxTransactionDateAll,'value')
    transaction_date = {'all'};
else
    StartDate = get(handles.edtFromDate,'string');
    EndDate = get(handles.edtToDate,'string');
    if ~isempty(StartDate) && ~isempty(EndDate)
        transaction_date = {StartDate,EndDate};
    else
        transaction_date = [];
    end
end
if ~isempty(transaction_date)
    InputFilterValues = [InputFilterValues;{'transaction_date',transaction_date}];
end

if get(handles.rbSettle,'value')
    PricingType = 'settle';
else
    PricingType = 'traders';
end

% output fields
OutputFields = getSelectedValues(handles.lbxOutput);

% summary fields or unique fields
SummmaryParameters = {};
if get(handles.cbxPortfolioSummary,'value')
    SummmaryParameters = [SummmaryParameters,'subportfolio'];
end
if get(handles.cbxCounterpartySummary,'value')
    SummmaryParameters = [SummmaryParameters,'counterparty'];
end
if get(handles.cbxContractMonthSummary,'value')
    SummmaryParameters = [SummmaryParameters,'contract_month'];
end
if get(handles.cbxDerivativeTypeSummary,'value')
    SummmaryParameters = [SummmaryParameters,'derivative_type'];
end
if get(handles.cbxMarketActionSummary,'value')
    SummmaryParameters = [SummmaryParameters,'market_action'];
end
if get(handles.cbxInstrumentClassSummary,'value')
    SummmaryParameters = [SummmaryParameters,'instrument_class'];
end
if get(handles.cbxMaturitySummary,'value')
    SummmaryParameters = [SummmaryParameters,'maturity'];
end
if get(handles.cbxStrikeSummary,'value')
    SummmaryParameters = [SummmaryParameters,'strike'];
end

% input scenarios
PriceSteps = str2double(getSelectedValues(handles.popPriceNoSteps));
PriceStepSize = str2double(getSelectedValues(handles.popPriceStepSize));
if get(handles.cbxPriceMove,'value')
    pricemove = {'on',PriceSteps,PriceStepSize,[]};
else
    pricemove = {'off',PriceSteps,PriceStepSize,[]};
end

VolSteps = str2double(getSelectedValues(handles.popVolNoSteps));
VolStepSize =  str2double(getSelectedValues(handles.popVolStepSize));
if get(handles.cbxVolMove,'value')
    volmove = {'on',VolSteps,VolStepSize,[]};
else
    volmove = {'off',VolSteps,VolStepSize,[]};
end

TimeSteps = str2double(getSelectedValues(handles.popTimeNoSteps));
TimeStepSize = str2double(getSelectedValues(handles.popTimeStepSize));
if get(handles.cbxTimeMove,'value')
    timemove = {'on',TimeSteps,TimeStepSize,[]};
else
    timemove = {'off',TimeSteps,TimeStepSize,[]};
end

InputScenarios = [pricemove;volmove;timemove];


function handles = setUIParameters(handles,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields)

% set the input filter values in the ui
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'subportfolio', ...
    'lbxPortfolio', 'cbxPortfolioAll', 'cbxPortfolioSummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'counterparty', ...
    'lbxCounterparty', 'cbxCounterpartyAll', 'cbxCounterpartySummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'contract_month', ...
    'lbxContractMonth', 'cbxContractMonthAll', 'cbxContractMonthSummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'derivative_type', ...
    'lbxDerivativeType', 'cbxDerivativeTypeAll', 'cbxDerivativeTypeSummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'instrument_class', ...
    'lbxInstrumentClass', 'cbxInstrumentClassAll', 'cbxInstrumentClassSummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'market_action', ...
    'lbxMarketAction', 'cbxMarketActionAll', 'cbxMarketActionSummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'maturity', ...
    'lbxMaturity', 'cbxMaturityAll', 'cbxMaturitySummary');
handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,'strike', ...
    'lbxStrike', 'cbxStrikeAll', 'cbxStrikeSummary');

% set the transaction date
Idx = find(strcmpi('transaction_date',InputFilterValues));
if isempty(Idx)
    set(handles.edtFromDate,'string','');
    set(handles.edtToDate,'string','');
    set(handles.cbxTransactionDateAll,'value',0);
else
    InValue = InputFilterValues{Idx,2};
    if strcmpi(InValue,'all')
        set(handles.cbxTransactionDateAll,'value',1);
    else
        set(handles.cbxTransactionDateAll,'value',0);
        if ~isempty(InValue{1})
            set(handles.edtFromDate,'string',InValue{1});
        else
            set(handles.edtFromDate,'string','');
        end
        if ~isempty(InValue{2})
            set(handles.edtToDate,'string',InValue{2});
        else
            set(handles.edtFromDate,'string','');
        end
    end
end

% set the pricing type
if strcmpi(PricingType,'settle')
    set(handles.rbSettle,'value',1);
    set(handles.rbTraders,'value',0);
else
    set(handles.rbTraders,'value',1);
    set(handles.rbSettle,'value',0);
end

% set the output fields
if isempty(OutputFields)
    set(handles.lbxOutput,'value',[]);
else
    handles.lbxOutput = setValues(handles.lbxOutput,OutputFields);
end

% set the input scenarios
if isempty(InputScenarios)
     handles = setInputScenarios(handles,[],'popPriceNoSteps','popPriceStepSize','cbxPriceMove');
    handles = setInputScenarios(handles,[],'popVolNoSteps','popVolStepSize','cbxVolMove');
    handles = setInputScenarios(handles,[],'popTimeNoSteps','popTimeStepSize','cbxTimeMove');
else
    handles = setInputScenarios(handles,InputScenarios(1,:),'popPriceNoSteps','popPriceStepSize','cbxPriceMove');
    handles = setInputScenarios(handles,InputScenarios(2,:),'popVolNoSteps','popVolStepSize','cbxVolMove');
    handles = setInputScenarios(handles,InputScenarios(3,:),'popTimeNoSteps','popTimeStepSize','cbxTimeMove');
end


function handles = setInputScenarios(handles,MoveScenario,popNoStepsObj,popStepSizeObj,cbxObj)

if isempty(MoveScenario)
    set(handles.(popNoStepsObj),'value',1);
    set(handles.(popStepSizeObj),'value',1);
    set(handles.(cbxObj),'value',0);
else
    if strcmpi(MoveScenario{1},'on')
        set(handles.(cbxObj),'value',1);
    else
        set(handles.(cbxObj),'value',0);
    end
    handles.(popNoStepsObj) = setValues(handles.(popNoStepsObj),MoveScenario{2});
    handles.(popStepSizeObj) = setValues(handles.(popStepSizeObj),MoveScenario{3});
end

function handles = setInputFilterValues(handles,InputFilterValues,SummmaryParameters,Keyname, lbxObj, cbxAllObj, cbxSummaryObj)
% set the input filter values
% subportfolio
if isempty(InputFilterValues)
    set(handles.(lbxObj),'value',[]);
    set(handles.(cbxAllObj),'value',0);
else
    Idx = find(strcmpi(Keyname,InputFilterValues));
    if isempty(Idx)
        set(handles.(lbxObj),'value',[]);
        set(handles.(cbxAllObj),'value',0);
    else
        InValue = InputFilterValues{Idx,2};
        if strcmpi(class(cell2mat(InValue)),'double') % for numeric values
            set(handles.(cbxAllObj),'value',0);
            handles.(lbxObj) = setValues(handles.(lbxObj),cell2mat(InValue));
        else
            if strcmpi(InValue,'all')
                set(handles.(cbxAllObj),'value',1);
            else
                set(handles.(cbxAllObj),'value',0);
                handles.(lbxObj) = setValues(handles.(lbxObj),InValue);
            end
        end
    end
end

if isempty(SummmaryParameters)
     set(handles.(cbxSummaryObj),'value',0);
else    
    Idx = find(strcmpi(Keyname,SummmaryParameters));
    if isempty(Idx)
        set(handles.(cbxSummaryObj),'value',0);
    else
        set(handles.(cbxSummaryObj),'value',1);
    end
end


% --- Executes on button press in pbFromDate.
function pbFromDate_Callback(hObject, eventdata, handles)
% hObject    handle to pbFromDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StartDate = get(handles.edtFromDate,'string');  
if isempty(StartDate)
    StartDate = handles.StartDate;
end

SelDate = uical(datenum(StartDate,'yyyy-mm-dd'));
set(handles.edtFromDate,'string',datestr(SelDate,'yyyy-mm-dd'));
guidata(hObject,handles);

% --- Executes on button press in pbToDate.
function pbToDate_Callback(hObject, eventdata, handles)
% hObject    handle to pbToDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
EndDate = get(handles.edtToDate,'string');
if isempty(EndDate)
    EndDate = handles.EndDate;
end

SelDate = uical(datenum(EndDate,'yyyy-mm-dd'));
set(handles.edtToDate,'string',datestr(SelDate,'yyyy-mm-dd'));
guidata(hObject,handles);


% --- Executes on button press in cbxOutputAll.
function cbxOutputAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxOutputAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxOutputAll
NumContents = length(get(handles.lbxOutput,'string'));    
if get(hObject,'Value')   
    set(handles.lbxOutput,'value',1:NumContents);
else
    set(handles.lbxOutput,'value',[]);
end
guidata(hObject,handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
Choice = questdlg('Do you want to save the Input and pricing data, which you would like to use it later from this UI?','Store data','Yes','No','Yes');
if strcmpi(Choice,'Yes')
    InReportData = handles.InReportData;
    InReportHeader = handles.InReportHeader;
    if isfield(handles,'SettlePricingData')
        SettlePricingData = handles.SettlePricingData;
    else
        SettlePricingData = [];
    end
    if isfield(handles,'SettlePricingHeader')
        SettlePricingHeader = handles.SettlePricingHeader;
    else
        SettlePricingHeader = [];
    end
    if isfield(handles,'TradersPricingData')
        TradersPricingData = handles.TradersPricingData;
    else
        TradersPricingData = [];
    end
    if isfield(handles,'TradersPricingHeader')
        TradersPricingHeader = handles.TradersPricingHeader;
    else
        TradersPricingHeader = [];
    end
    save(handles.MatFilename,'InReportHeader','InReportData','SettlePricingHeader','SettlePricingData','TradersPricingHeader','TradersPricingData')
end
delete(hObject);
