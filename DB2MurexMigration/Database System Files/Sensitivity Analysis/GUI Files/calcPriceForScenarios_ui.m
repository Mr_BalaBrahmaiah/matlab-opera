function OutFilename = calcPriceForScenarios_ui(InputData,InputHeader,PricingType,InPricerHeading,PricerData,...
    PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
    SummmaryParameters,SumFields,OutputFields,WeightedAverageFields)

OutFilename = '';
IsSensitivityAnalysis = 1;

% from the netted data, get the list of securities for which we need to
% price
Pos_InSecurity = strcmpi('security_id',InputHeader);
InSecurityId = unique(upper(InputData(:,Pos_InSecurity)),'stable');

% if strcmpi(PricingType,'settle')
%     ViewName = 'input_security_settlement_value_view';
% else
%     ViewName = 'input_security_traders_value_view';
% end
% 
% % Read the values from database for derivative pricer
% [InPricerHeading,PricerData] = read_from_database(ViewName,0);

if isequal(InPricerHeading,{[]}) % no data found in database for the selected criteria
    disp('No data found in database for the selected criteria!');
else
    Pos_SecurityId = strcmpi('security_id',InPricerHeading);
    SecurityId = upper(PricerData(:,Pos_SecurityId));
    
    % for the list of security ids in the selected input record, compute the
    % prices
    IdxSecurity = ismember(SecurityId,InSecurityId);
    InPricerData = PricerData(IdxSecurity,:);
    
    OutPricerHeading = {'security_id','value_date','price','delta_1','delta_2','gamma_11','gamma_12','gamma_21','gamma_22','vega_1','vega_2','theta'};
    if strcmpi(PricingType,'traders')
        OutPricerHeading = ['subportfolio',OutPricerHeading];
        PosVolSurfType = strcmpi('vol_surf_type',InPricerHeading);
        VolSurfType = InPricerData(:,PosVolSurfType);
        Vol = zeros(size(VolSurfType));
        IdxFloat = strcmpi('float_3param',VolSurfType);
        IdxFixed = ~IdxFloat;
        if any(IdxFloat)
            PosFloatVol = strcmpi('atm1_vol',InPricerHeading);
            Vol(IdxFloat) = utilgetvalidnumericarray(InPricerData(IdxFloat,PosFloatVol));
        end
        if any(IdxFixed)
            PosFixedVol = strcmpi('p1_vol',InPricerHeading);
            Vol(IdxFixed) = utilgetvalidnumericarray(InPricerData(IdxFixed,PosFixedVol));
        end
    else
        PosVol = strcmpi('p1_vol',InPricerHeading);
        Vol = utilgetvalidnumericarray(InPricerData(:,PosVol));
    end
    PosPrice = strcmpi('p1_settleprice',InPricerHeading);
    Price = utilgetvalidnumericarray(InPricerData(:,PosPrice));
    PosMaturity = strcmpi('maturity',InPricerHeading);
    Maturity = datenum(InPricerData(:,PosMaturity),'yyyy-mm-dd');
    PosValueDate = strcmpi('value_date',InPricerHeading);
    ValueDate = datenum(InPricerData(1,PosValueDate),'yyyy-mm-dd');
    
    PricerHeading = [OutPricerHeading,'future_price','pricemove','vol','volmove','time','timemove'];
    
    % construct the stepsize vectors
    PriceStepVec = constructStepSizeVec(PriceNumSteps, PriceStepSize,1);
    VolStepVec  = constructStepSizeVec(VolNumSteps, VolStepSize,1);
    TimeStepVec = constructStepSizeVec(TimeNumSteps, TimeStepSize,0);
    
    % Apply the price move in p1_settle_price
    % apply the vol move in settle_vol/traders_vol
    % apply the time move by decreasing the maturity date to that amount
    ConsDeals = [];
    PriceMove = Price;
    for iPrice = 1:length(PriceStepVec)
        PriceMove = PriceMove + PriceStepVec(iPrice);
        VolMove = Vol;
        for iVol = 1:length(VolStepVec)
            VolMove = VolMove + VolStepVec(iVol);
            TimeMove = Maturity;
            
            for iTime = 1:length(TimeStepVec)
                TimeMove = TimeMove - TimeStepVec(iTime);
                
%                 if any([PriceStepVec(iPrice),VolStepVec(iVol),TimeStepVec(iTime)] == 0) && ...
%                        ~all([PriceStepVec(iPrice),VolStepVec(iVol),TimeStepVec(iTime)] == 0) 
%                     continue;
%                 end
                
                InPricerData(:,PosPrice) = num2cell(PriceMove);
                InPricerData(:,PosMaturity) = num2cell(TimeMove);
                if strcmpi(PricingType,'traders')
                    if any(IdxFloat)
                        InPricerData(IdxFloat,PosFloatVol) = num2cell(VolMove(IdxFloat));
                    end
                    if any(IdxFixed)
                        InPricerData(IdxFixed,PosFixedVol) = num2cell(VolMove(IdxFixed));
                    end
                else
                    InPricerData(:,PosVol) = num2cell(VolMove);
                end
                
                % in final output, price = pricemove, pricemove = pricestepsize with
                % direction
                % vol = volmove, volmove = volstepsize with direction
                % time = timemove, timemove = time stepsize with direction
                NumInputs = size(Price);
                OutPriceStepSize = repmat(PriceStepVec(iPrice),NumInputs);
                OutVolStepSize = repmat(VolStepVec(iVol),NumInputs);
                OutTimeStepSize = repmat(TimeStepVec(iTime),NumInputs);
                OutTimeMove = repmat(ValueDate + TimeStepVec(iTime),NumInputs);
                
                OutPricerData = derivativepricer_interpvols(InPricerData, InPricerHeading, OutPricerHeading,PricingType);
                OutPricerData = [OutPricerData,num2cell(PriceMove),num2cell(OutPriceStepSize),num2cell(VolMove),num2cell(OutVolStepSize),num2cell(OutTimeMove),num2cell(OutTimeStepSize)];
                [OutputHeader,OutData] = generate_pricing_views(PricingType,InputHeader,InputData,PricerHeading,OutPricerData,IsSensitivityAnalysis);
                
                % %     UniqueFields  = varargin{1};    SumFields     = varargin{2};    OutputFields  = varargin{3};    WeightedAverageFields = varargin{4};
                [OutputFields,TempConsDeals] = consolidatedata(OutputHeader, OutData,SummmaryParameters,SumFields,OutputFields,WeightedAverageFields);
                
                ConsDeals = [ConsDeals;TempConsDeals];
            end
        end
    end
    
    
    TempName = [char(PricingType),'_SensitivityReport_',datestr(now)];
    TempName = strrep(TempName,':','-');
    TempName = strrep(TempName,' ','_');
    OutFilename =  [TempName,'.csv'];
    customcsvwrite(OutFilename,ConsDeals,OutputFields);
    
end

    function OutSteps = constructStepSizeVec(NumSteps, StepSize,IsBothDirections)
        OutSteps = 0;
        for iS = 1:NumSteps
            OutSteps = [OutSteps, iS * StepSize];            
            if IsBothDirections
                OutSteps = [OutSteps, -(iS * StepSize)];
            end
        end
    end


end

