function OutFilename = generate_sensitivity_report_ui(InReportHeader,InReportData,InPricerHeading,PricerData,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields)

% ViewName = 'helper_sensitivity_report_view';
% [InReportHeader,InReportData] = read_from_database(ViewName,0);

% InputFilterValues = {'subportfolio',{'MM-BO-CBT-OID','MM-BO-CBT-OIV', 'MM-S-CBT-OIV','MM-S-CBT-OID','RMS-C-CBT-OIR'},...
%     'counterparty',{'Bache','BOM JESUS','Olam International'},...
%     'strike',{40,1000,24,2500,500.25,400.25,18.5},...
%     'transaction_date',{'2014-07-01','2014-08-30'}};

% InputScenarios = {'PriceMove',{NumberOfSteps,StepSize},'VolMove',{NumberOfSteps,StepSize},'TimeMove',{NumberOfSteps,StepSize}};

% InputFilterValues = {'transaction_date',{'2014-07-01','2014-09-09'}};
% InputFilterValues = {'subportfolio',{'MM-BO-CBT-OIV'}};
% SummmaryParameters = {'subportfolio'};

if isempty(SummmaryParameters)
    SummmaryParameters = {'subportfolio'};
end
% process InputScenarios
% it is designed as 3 x 4 cell array
% %                         perform_sensitivity 	noofsteps		stepsize		stepparameter_parameter
% % pricemove              'on'                     3               2
% % volitilitymove          []                      []              []
% % timemove
PriceNumSteps = 0; PriceStepSize = 0;
VolNumSteps   = 0; VolStepSize   = 0;
TimeNumSteps  = 0; TimeStepSize  = 0;
if ~isempty(InputScenarios)
    IsPriceMove = strcmpi(InputScenarios{1,1},'on');
    if IsPriceMove
        PriceNumSteps = InputScenarios{1,2};
        if isempty(PriceNumSteps)
            PriceNumSteps = 1;
        end
        PriceStepSize = InputScenarios{1,3};
        if isempty(PriceStepSize)
            PriceStepSize = 0;
        end
    end
    
    IsVolMove = strcmpi(InputScenarios{2,1},'on');
    if IsVolMove
        VolNumSteps   = InputScenarios{2,2};
        if isempty(VolNumSteps)
            VolNumSteps = 1;
        end
        VolStepSize   = InputScenarios{2,3};
        if isempty(VolStepSize)
            VolStepSize = 0;
        else
            VolStepSize = VolStepSize / 100;
        end
    end
    
    IsTimeMove = strcmpi(InputScenarios{3,1},'on');
    if IsTimeMove
        TimeNumSteps  = InputScenarios{3,2};
        if isempty(TimeNumSteps)
            TimeNumSteps = 1;
        end
        TimeStepSize  = InputScenarios{3,3};
        if isempty(TimeStepSize)
            TimeStepSize = 0;
        end
    end
end

PosMaturity  = strcmpi('maturity_date',InReportHeader);
IdxEmptyMaturity = cellfun(@isempty,InReportData(:,PosMaturity));
if any(IdxEmptyMaturity)
    InReportData(IdxEmptyMaturity,PosMaturity) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
end

if isempty(InputFilterValues)
    IdxReportData = zeros(size(InReportData,1),1);
else
    FilterKeyNames = InputFilterValues(:,1);
    
    for iKey = 1:length(FilterKeyNames)
        KeyName = FilterKeyNames{iKey};
        if iKey == 1
            IdxReportData = getValueFromKeyValuePair(KeyName);
        else
            IdxTempKey = getValueFromKeyValuePair(KeyName);
            IdxReportData = IdxReportData & IdxTempKey;
        end
    end
end

if any(IdxReportData)
    % find the selected records and compute the active lots, net the data
    % before doing the scenario analysis
    SelData = InReportData(IdxReportData,:);
    
    [SelData,SelHeader] = calcActiveLots(SelData,InReportHeader);
    
    SumFields = {'original_lots','active_lots','delta','gamma','vega','theta','m2m_usd','m2m_nc'};
    OutputFields  = [SummmaryParameters,OutputFields];
    WeightedAverageFields = [];
    OutputFields = lower(OutputFields);
    SummmaryParameters = lower(SummmaryParameters);
    
    OutFilename = calcPriceForScenarios_ui(SelData,SelHeader,PricingType,InPricerHeading,PricerData,...
        PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
        SummmaryParameters,SumFields,OutputFields,WeightedAverageFields);
else
    errordlg('No input data selected for the analysis!');
    OutFilename = '';
end

    function IdxValue = getValueFromKeyValuePair(KeyName)
        disp('KeyName:'); disp(KeyName);        
        IdxKey =  find(strcmpi(InputFilterValues,KeyName));
        if ~isempty(IdxKey)
            InValues = InputFilterValues{IdxKey,2};
             disp('Values');disp(InValues);
            if strcmpi(KeyName,'maturity')
                InValues = lower(InValues);
                if ismember('all',InValues) 
                    IdxValue = logical(ones(size(InReportData,1),1));
                elseif any(ismember({'live','dead'},InValues))
                    PosValueDate = strcmpi('value_date',InReportHeader);
                    ValueDate    = datenum(InReportData(:,PosValueDate),'yyyy-mm-dd');
                    MaturityDate = datenum(InReportData(:,PosMaturity),'yyyy-mm-dd');
                    if all(ismember({'live','dead'},InValues)) % if both live and dead are selected
                        IdxValue = logical(ones(size(MaturityDate)));
                    elseif ismember('dead',InValues)
                        IdxValue = ValueDate > MaturityDate;
                    else
                        IdxValue = ValueDate <= MaturityDate;
                    end
                else
                    IdxValue = logical(zeros(size(InReportData,1),1));
                end
                return;
            end
            PosKeyName = strcmpi(KeyName,InReportHeader);
            DBKeyValues = InReportData(:,PosKeyName);
            % class of this DBKeyValues can be either double or cell
            % double, if the field is numeric in db
            % all other fields will be of class cell           
            disp(class(cell2mat(InValues)));
            if strcmpi(class(cell2mat(InValues)),'double')
                InValues = cell2mat(InValues);
                DBKeyValues = utilgetvalidnumericarray(DBKeyValues);
                IdxValue = ismember(DBKeyValues,InValues);
            else
                if ~isempty(cellStrfind_exact(KeyName,'date'))
                    % to handle the dates
                    StartDate = datenum(InValues{1},'yyyy-mm-dd');
                    EndDate   = datenum(InValues{2},'yyyy-mm-dd');
                    DBDateValues = datenum(DBKeyValues,'yyyy-mm-dd');
                    IdxValue = (DBDateValues >= StartDate) & (DBDateValues <= EndDate);
                    
                else % check if all is selected or specific inputs
                    if ismember('all',InValues)
                        IdxValue = logical(ones(size(DBKeyValues)));                   
                    else
                        IdxKeyFound = cellStrfind_exact(upper(DBKeyValues),upper(InValues));
                        IdxValue = logical(zeros(size(DBKeyValues)));
                        IdxValue(IdxKeyFound) = 1;
                    end
                end
            end
            
        else            
            IdxValue = logical(zeros(size(InReportData,1),1));
        end
        
    end
end