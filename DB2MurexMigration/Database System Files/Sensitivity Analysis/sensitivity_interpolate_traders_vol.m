function InterpVol = sensitivity_interpolate_traders_vol(InputData,DbData)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/14 07:50:07 $
%  $Revision: 1.2 $
%

InterpVol = InputData.vol;

UniqueKey = unique(cellstr(strcat(char(InputData.subportfolio),',',char(InputData.vol_id))));

for iVol = 1:length(UniqueKey)
    TempVar = strsplit(UniqueKey{iVol},',');    
    SubPortfolio = TempVar{1};
    VolId = TempVar{2};
    
    Index = find(strcmpi(VolId,InputData.vol_id) & strcmpi(SubPortfolio,InputData.subportfolio) & isnan(InputData.vol));
    OutStrike = InputData.strike(Index);
    
    DbIndex = find(ismember(upper(DbData.vol_id),upper(VolId)) & ismember(upper(DbData.subportfolio),upper(SubPortfolio)));
    if ~isempty(DbIndex) && ~isempty(Index)
        XStrike = DbData.strike(DbIndex);
        YVol =  DbData.traders_vol(DbIndex);
        if (length(DbIndex)==1) % if there is one value available in db, then interp1 cannot be invoked for the single value
            if abs(XStrike - OutStrike) < 0.000001 % to check if both strikes are equal
                InterpVol(Index) = YVol;
            end
        else
            OutVol = interp1(XStrike,YVol,OutStrike,'linear','extrap');
            InterpVol(Index) = OutVol;
        end        
    end
    
end

end