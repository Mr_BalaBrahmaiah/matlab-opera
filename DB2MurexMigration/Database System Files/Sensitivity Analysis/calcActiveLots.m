function [InputData,InputHeader] = calcActiveLots(InputData,InputHeader,StartTradeDate,EndTradeDate)
% calc the active lots 

Pos_ParentTradeId = strcmpi('parent_trade_id',InputHeader);
Pos_InitialTrade  = strcmpi('market_action_initial_trade',InputHeader);
Pos_OriginalLots  = strcmpi('original_lots',InputHeader);
Pos_ActiveLots = strcmpi('active_lots',InputHeader);

Pos_TradeDate = strcmpi('transaction_date',InputHeader); 
Pos_ValueDate =  strcmpi('value_date',InputHeader); 
ValueDate = datenum(InputData{1,Pos_ValueDate});
TransactionDate = datenum(InputData(:,Pos_TradeDate),'yyyy-mm-dd');
% IdxFound = TransactionDate >= ValueDate;
IdxFound = (TransactionDate >= ValueDate) | (TransactionDate < StartTradeDate) | (TransactionDate > EndTradeDate);

if ~isempty(find(IdxFound))
SelParentTradeId = InputData(IdxFound,Pos_ParentTradeId);
InputData(IdxFound,Pos_OriginalLots) = num2cell(0);
InParentTradeId = InputData(:,Pos_ParentTradeId);

InOriginalLots  = getvalidarray(InputData(:,Pos_OriginalLots));
InInitialTrade  = getvalidarray(InputData(:,Pos_InitialTrade));
ActiveLots  = getvalidarray(InputData(:,Pos_ActiveLots));

disp('In Active lots computation...');

ParentTradeId = unique(SelParentTradeId);

for iP = 1:length(ParentTradeId)
    IdxP = strcmp(ParentTradeId{iP},InParentTradeId);
    
    ActiveLots(IdxP) = sum(InOriginalLots(IdxP)) .* InInitialTrade(IdxP);    
end

InputData(:,Pos_ActiveLots) = num2cell(ActiveLots);
end
% InputData(IdxFound,:) = [];
end