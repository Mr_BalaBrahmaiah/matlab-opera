function OutFilename = calcPriceForScenarios_SubFunction(InBUName,InputData,InputHeader,PricingType,...
    PriceStepParameter,PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
    SummmaryParameters,SumFields,OutputFields,WeightedAverageFields,OutFilename)

IsSensitivityAnalysis = 1;

% from the netted data, get the list of securities for which we need to
% price
Pos_InSecurity = strcmpi('security_id',InputHeader);
InSecurityId = unique(upper(InputData(:,Pos_InSecurity)),'stable');

if strcmpi(PricingType,'settle')
    ViewName = 'input_security_settlement_value_cache_table';
    TableName = 'settlement_vol_surf_table';
else
    ViewName = 'input_security_traders_value_cache_table';
    TableName = ['traders_vol_surf_fixed_table_',InBUName];
end

% Read the values from database for derivative pricer
[InPricerHeading,PricerData] = read_from_database(ViewName,0,'',InBUName);

if isequal(InPricerHeading,{[]}) % no data found in database for the selected criteria
    disp('No data found in database for the selected criteria!');
else
    [ColNames,Data] = read_from_database(TableName,1);
    DBVolData = cell2dataset([ColNames;Data]);
    
    Pos_SecurityId = strcmpi('security_id',InPricerHeading);
    SecurityId = upper(PricerData(:,Pos_SecurityId));
    
    % for the list of security ids in the selected input record, compute the
    % prices
    IdxSecurity = ismember(SecurityId,InSecurityId);
    InPricerData = PricerData(IdxSecurity,:);
    
    OutPricerHeading = {'security_id','value_date','price','delta_1','delta_2','gamma_11','gamma_12','gamma_21','gamma_22','vega_1','vega_2','theta'};
    if strcmpi(PricingType,'traders')
        OutPricerHeading = ['subportfolio',OutPricerHeading];
        PosVolSurfType = strcmpi('vol_surf_type',InPricerHeading);
        VolSurfType = InPricerData(:,PosVolSurfType);
        Vol = zeros(size(VolSurfType));
        IdxFloat = strcmpi('float_3param',VolSurfType);
        IdxFixed = ~IdxFloat;
        if any(IdxFloat)
            PosFloatVol = strcmpi('atm1_vol',InPricerHeading);
            Vol(IdxFloat) = utilgetvalidnumericarray(InPricerData(IdxFloat,PosFloatVol));
        end
        if any(IdxFixed)
            PosFixedVol = strcmpi('p1_vol',InPricerHeading);
            Vol(IdxFixed) = utilgetvalidnumericarray(InPricerData(IdxFixed,PosFixedVol));
        end
    else
        PosVol = strcmpi('p1_vol',InPricerHeading);
        Vol = utilgetvalidnumericarray(InPricerData(:,PosVol));
    end
    PosPrice = strcmpi('p1_settleprice',InPricerHeading);
    Price = utilgetvalidnumericarray(InPricerData(:,PosPrice));
    PosMaturity = strcmpi('maturity',InPricerHeading);
    Maturity = datenum(InPricerData(:,PosMaturity),'yyyy-mm-dd');
    PosValueDate = strcmpi('value_date',InPricerHeading);
    ValueDate = datenum(InPricerData(1,PosValueDate),'yyyy-mm-dd');
    PosTickSize = strcmpi('tick_size',InPricerHeading);
    TickSize = utilgetvalidnumericarray(InPricerData(:,PosTickSize));
    PosOptType = strcmpi('opt_type',InPricerHeading);
    IdxLiveFuture = strcmpi('future',InPricerData(:,PosOptType)) & (ValueDate <= Maturity);
    
    if strcmpi(PriceStepParameter,'Percentage Move')
        OutPriceMoveParameter = 'pricemove_in_percentage';
    else
        OutPriceMoveParameter = 'pricemove_in_pricetick';
    end
    
    PricerHeading = [OutPricerHeading,'future_price','pricemove',OutPriceMoveParameter,'vol','volmove','time','timemove'];
    
    % construct the stepsize vectors
    PriceStepVec = constructStepSizeVec(PriceNumSteps, PriceStepSize,1);
    VolStepVec  = constructStepSizeVec(VolNumSteps, VolStepSize,1);
    TimeStepVec = constructStepSizeVec(TimeNumSteps, TimeStepSize,0);
    
    %% Changing for Parfor
    
    % Apply the price move in p1_settle_price
    % apply the vol move in settle_vol/traders_vol
    % apply the time move by decreasing the maturity date to that amount
    ConsDeals = [];
    DebugIterationCount = 0;  %% 
    save(strrep(OutFilename,'.xlsx','.mat'));
    Matfile_Path = [pwd filesep strrep(OutFilename,'.xlsx','.mat')];    
    
    [OutFilename,ConsDeals] = calcPriceForScenarios_SubFunction_Struct(Matfile_Path);
    
    %%
    
    try
        ConsDeals = sortrows(ConsDeals,1:length(SummmaryParameters));
    catch 
        disp('Error while sorting rows: some NaN values are found');
    end
    xlswrite(OutFilename,[OutputFields;ConsDeals],'Summary Report');
    TempXLSFileName = fullfile(pwd,OutFilename);
    xls_delete_sheets(TempXLSFileName);
    
    %% Delete Mat File 
    
    delete(Matfile_Path);
    
end

    function OutSteps = constructStepSizeVec(NumSteps, StepSize,IsBothDirections)
        OutSteps = 0;
        for iS = 1:NumSteps
            OutSteps = [OutSteps, iS * StepSize];
            if IsBothDirections
                OutSteps = [OutSteps, -(iS * StepSize)];
            end
        end
    end


end

