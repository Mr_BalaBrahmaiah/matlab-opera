function [OutputHeader,OutData] = generate_pricing_views(PricingType,InputHeader,InputData,PricerOutputHeading,PricerOutput,IsSensitivityAnalysis)
% use the pricer output and the input deal info to compute the greeks and
% book value/mtm of individual deal level

if strcmpi(PricingType,'settle')
    PricerOutputHeading = strrep(PricerOutputHeading,'settle_','');
else
    PricerOutputHeading = strrep(PricerOutputHeading,'traders_','');
end

Pos_InSecurityId  = find(strcmpi('security_id',InputHeader));
if isempty(Pos_InSecurityId)
    disp('security_id is not found in Input deals heading!');
else
    AllInputSec = InputData(:,Pos_InSecurityId);
    InSecurityId = unique(upper(AllInputSec),'stable');
    OutSize = size(AllInputSec);
end

Pos_ActiveLots = find(strcmpi('active_lots',InputHeader));
if isempty(Pos_ActiveLots)
    disp('active_lots is not found in Input deals heading!');
else
    active_lots = getvalidarray(InputData(:,Pos_ActiveLots));
end
    
Pos_OrigLots   = find(strcmpi('original_lots',InputHeader));
if isempty(Pos_OrigLots)
    disp('original_lots is not found in Input deals heading!');
else
    original_lots = getvalidarray(InputData(:,Pos_OrigLots));
end

Pos_LotMult    = find(strcmpi('lot_mult1',InputHeader));
if isempty(Pos_LotMult)
    disp('lot_mult1 is not found in Input deals heading!');
else
    lot_mult1 = getvalidarray(InputData(:,Pos_LotMult));
end

Pos_CurrMult   = find(strcmpi('curr_mult1',InputHeader));
if isempty(Pos_CurrMult)
    disp('curr_mult1 is not found in Input deals heading!');
else
    curr_mult1 = getvalidarray(InputData(:,Pos_CurrMult));
end

Pos_NCFactor   = find(strcmpi('nc_factor1',InputHeader));
if isempty(Pos_NCFactor)
    disp('nc_factor1 is not found in Input deals heading!');
else
    nc_factor1 = getvalidarray(InputData(:,Pos_NCFactor));
end

Pos_OrigPrem   = find(strcmpi('original_premium',InputHeader));
if isempty(Pos_OrigPrem)
    disp('original_premium is not found in Input deals heading!');
else
    original_premium = getvalidarray(InputData(:,Pos_OrigPrem));
end

Pos_CurrPrem   = find(strcmpi('current_premium',InputHeader));
if isempty(Pos_CurrPrem)
    disp('current_premium is not found in Input deals heading!');
else
    current_premium = getvalidarray(InputData(:,Pos_CurrPrem));
end

Pos_PricerSecurityId = find(strcmpi('security_id',PricerOutputHeading));
if isempty(Pos_PricerSecurityId)
    disp('security_id is not found in pricer output heading!');
else
    PricerSecurityId = PricerOutput(:,Pos_PricerSecurityId);
end

Pos_Price      = find(strcmpi('price',PricerOutputHeading));
if isempty(Pos_Price)
    disp('price is not found in pricer output heading!');
else
    calculated_price = getvalidarray(PricerOutput(:,Pos_Price));
end

Pos_Delta1     = find(strcmpi('delta_1',PricerOutputHeading));
if isempty(Pos_Delta1)
    disp('delta_1 is not found in pricer output heading!');
else
    delta_1 = getvalidarray(PricerOutput(:,Pos_Delta1));
end

Pos_Delta2     = find(strcmpi('delta_2',PricerOutputHeading));
if isempty(Pos_Delta2)
    disp('delta_2 is not found in pricer output heading!');
else
    delta_2 = getvalidarray(PricerOutput(:,Pos_Delta2));
end

Pos_Gamma11    = find(strcmpi('gamma_11',PricerOutputHeading));
if isempty(Pos_Gamma11)
    disp('gamma_11 is not found in pricer output heading!');
else
    gamma_11 = getvalidarray(PricerOutput(:,Pos_Gamma11));
end

Pos_Gamma12    = find(strcmpi('gamma_12',PricerOutputHeading));
if isempty(Pos_Gamma12)
    disp('gamma_12 is not found in pricer output heading!');
else
    gamma_12 = getvalidarray(PricerOutput(:,Pos_Gamma12));
end

Pos_Gamma21    = find(strcmpi('gamma_21',PricerOutputHeading));
if isempty(Pos_Gamma21)
    disp('gamma_21 is not found in pricer output heading!');
else
    gamma_21 = getvalidarray(PricerOutput(:,Pos_Gamma21));
end

Pos_Gamma22    = find(strcmpi('gamma_22',PricerOutputHeading));
if isempty(Pos_Gamma22)
    disp('gamma_22 is not found in pricer output heading!');
else
    gamma_22 = getvalidarray(PricerOutput(:,Pos_Gamma22));
end

Pos_Vega1      = find(strcmpi('vega_1',PricerOutputHeading));
if isempty(Pos_Vega1)
    disp('vega_1 is not found in pricer output heading!');
else
    vega_1 = getvalidarray(PricerOutput(:,Pos_Vega1));
end

Pos_Vega2      = find(strcmpi('vega_2',PricerOutputHeading));
if isempty(Pos_Vega2)
    disp('vega_2 is not found in pricer output heading!');
else
    vega_2 = getvalidarray(PricerOutput(:,Pos_Vega2));
end

Pos_Theta      = find(strcmpi('theta',PricerOutputHeading));
if isempty(Pos_Theta)
    disp('theta is not found in pricer output heading!');
else
    theta = getvalidarray(PricerOutput(:,Pos_Theta));
end

if IsSensitivityAnalysis
    Pos_FuturePrice      = find(strcmpi('future_price',PricerOutputHeading));
    if isempty(Pos_FuturePrice)
        disp('future_price is not found in pricer output heading!');
    else
        future_price = getvalidarray(PricerOutput(:,Pos_FuturePrice));
    end
    
    Pos_PriceMove      = find(strcmpi('pricemove',PricerOutputHeading));
    if isempty(Pos_PriceMove)
        disp('pricemove is not found in pricer output heading!');
    else
        pricemove = getvalidarray(PricerOutput(:,Pos_PriceMove));
    end
   
    Pos_Vol      = find(strcmpi('vol',PricerOutputHeading));
    if isempty(Pos_Vol)
        disp('vol is not found in pricer output heading!');
    else
        vol = getvalidarray(PricerOutput(:,Pos_Vol));
    end
    
    Pos_VolMove   = find(strcmpi('volmove',PricerOutputHeading));
    if isempty(Pos_VolMove)
        disp('volmove is not found in pricer output heading!');
    else
        volmove = getvalidarray(PricerOutput(:,Pos_VolMove));
    end
    
    Pos_Time   = find(strcmpi('time',PricerOutputHeading));
    if isempty(Pos_Time)
        disp('time is not found in pricer output heading!');
    else
        time = getvalidarray(PricerOutput(:,Pos_Time));
    end
    
    Pos_TimeMove   = find(strcmpi('timemove',PricerOutputHeading));
    if isempty(Pos_TimeMove)
        disp('timemove is not found in pricer output heading!');
    else
        timemove = getvalidarray(PricerOutput(:,Pos_TimeMove));
    end    
    
    Pos_PricemoveParameter = cellStrfind(lower(PricerOutputHeading),{'pricemove_in_percentage','pricemove_in_pricetick'});
    if ~isempty(Pos_PricemoveParameter)        
        pricemoveparameter = getvalidarray(PricerOutput(:,Pos_PricemoveParameter));
        pricemoveparametername = PricerOutputHeading(Pos_PricemoveParameter);
    end   
end
    
OutData = InputData;
OutputHeader = InputHeader;

CalculatedPrice = zeros(OutSize);
Delta = zeros(OutSize);
Gamma = zeros(OutSize);
Vega = zeros(OutSize);
Theta = zeros(OutSize);
OriginalAmountPaid = zeros(OutSize);
m2m_usd = zeros(OutSize);
m2m_nc = zeros(OutSize);
cumulative_m2m_nc = zeros(OutSize);
cumulative_m2m_usd = zeros(OutSize);
realised_m2m_usd = zeros(OutSize);
unrealised_m2m_usd = zeros(OutSize);
realised_m2m_nc = zeros(OutSize);
unrealised_m2m_nc = zeros(OutSize);

if IsSensitivityAnalysis
    Price = zeros(OutSize);
    PriceMove = zeros(OutSize);
    Vol = zeros(OutSize);
    VolMove = zeros(OutSize);
    Time = zeros(OutSize);
    TimeMove = zeros(OutSize);
    PriceMoveParameter = zeros(OutSize);
    DeltaMT =  zeros(OutSize);
end

for iS = 1:length(InSecurityId)
TempId = InSecurityId{iS};
IdxOut = find(strcmpi(TempId,PricerSecurityId)); % from pricer output
IdxIn = strcmpi(TempId,AllInputSec); % from input data with lots
if length(IdxOut) > 1
    IdxOut = IdxOut(1);
end

if ~isempty(IdxOut)
Mult_Factor = lot_mult1(IdxIn) .* curr_mult1(IdxIn);

% orignial amount paid = active_lots * original_premium * lot_mult1 *
% curr_mult1
OriginalAmountPaid(IdxIn) = active_lots(IdxIn) .* original_premium(IdxIn) .* ...
    Mult_Factor;

% mtm_usd = (calculated_price * active_lots) - (current_premium *
% original_lots) * lot_mult * curr_mult
m2m_usd(IdxIn) = ((calculated_price(IdxOut) .* active_lots(IdxIn))  - (current_premium(IdxIn) .* original_lots(IdxIn))) .* ...
     Mult_Factor;
 
% mtm_nc = (calculated_price * active_lots) - (current_premium *
% original_lots) * lot_mult * curr_mult / nc_factor
m2m_nc(IdxIn) = m2m_usd(IdxIn) .* nc_factor1(IdxIn);

% cumulative_m2m_usd = (calculated_price * active_lots) - (original_premium *
% original_lots) * lot_mult * curr_mult
cumulative_m2m_usd(IdxIn) = ((calculated_price(IdxOut) .* active_lots(IdxIn))  - (original_premium(IdxIn) .* original_lots(IdxIn))) .* ...
    Mult_Factor;

% cumulative_m2m_nc = (calculated_price * active_lots) - (original_premium *
% original_lots) * lot_mult * curr_mult / nc_factor
cumulative_m2m_nc(IdxIn) = cumulative_m2m_usd(IdxIn) .* nc_factor1(IdxIn);

% Calculated Price 
CalculatedPrice(IdxIn) = calculated_price(IdxOut);

% delta = delta_per_lot * active_lots
Delta1 = delta_1(IdxOut) .* active_lots(IdxIn);
Delta2 = delta_2(IdxOut) .* active_lots(IdxIn);
Delta(IdxIn) = Delta1 + Delta2;

% gamma = (gamma_per_lot * active_lots * lot_mult ) / curr_mult;
Gamma11 = (gamma_11(IdxOut) .* active_lots(IdxIn) .* lot_mult1(IdxIn)) ./ curr_mult1(IdxIn);
Gamma12 = (gamma_12(IdxOut) .* active_lots(IdxIn) .* lot_mult1(IdxIn)) ./ curr_mult1(IdxIn);
Gamma21 = (gamma_21(IdxOut) .* active_lots(IdxIn) .* lot_mult1(IdxIn)) ./ curr_mult1(IdxIn);
Gamma22 = (gamma_22(IdxOut) .* active_lots(IdxIn) .* lot_mult1(IdxIn)) ./ curr_mult1(IdxIn);
Gamma(IdxIn) = Gamma11 + Gamma12 + Gamma21 + Gamma22;

%vega = vega_per_lot * active_lots * lot_mult1 * curr_mult
Vega1 = vega_1(IdxOut) .* active_lots(IdxIn) .* Mult_Factor;
Vega2 = vega_2(IdxOut) .* active_lots(IdxIn) .* Mult_Factor;
Vega(IdxIn) = Vega1 + Vega2;

% theta = theta_per_lot * active_lots * lot_mult1 * curr_mult
Theta(IdxIn)  = (theta(IdxOut) .* active_lots(IdxIn) .* Mult_Factor ) .* (7/5);

% (((`chi` * `active_lots`) * `lot_mult1`) * `curr_mult1`) AS `chi`,
% (((`current_premium` * `original_lots`) * `lot_mult1`) * `is_delta_curr_exp`) AS `delta_curr_exp`,
% ((((`current_premium` * `original_lots`) - (`calculated_price` * `active_lots`)) * `lot_mult1`) * `is_pnl_curr_exp`) AS `pnl_curr_exp`

if IsSensitivityAnalysis   
    Price(IdxIn)     = future_price(IdxOut);
    PriceMove(IdxIn) = pricemove(IdxOut);
    Vol(IdxIn) = vol(IdxOut);
    VolMove(IdxIn) = volmove(IdxOut);
    Time(IdxIn) = time(IdxOut);
    TimeMove(IdxIn) = timemove(IdxOut);
    PriceMoveParameter(IdxIn) = pricemoveparameter(IdxOut);
    DeltaMT(IdxIn) = Delta(IdxIn) .* lot_mult1(IdxIn);
end
end
end

PosValueDate = strcmpi('value_date',InputHeader);
PosMaturity = strcmpi('maturity_date',InputHeader);
ValueDate = datenum(InputData(:,PosValueDate),'yyyy-mm-dd');
MaturityDate = datenum(InputData(:,PosMaturity),'yyyy-mm-dd');
IdxDead = ValueDate > MaturityDate;
IdxLive = ValueDate <= MaturityDate;
realised_m2m_usd(IdxDead) = m2m_usd(IdxDead);
unrealised_m2m_usd(IdxLive) = m2m_usd(IdxLive);
realised_m2m_nc(IdxDead) = m2m_nc(IdxDead);
unrealised_m2m_nc(IdxLive) = m2m_nc(IdxLive);

if IsSensitivityAnalysis    
    OutData = [OutData,num2cell(CalculatedPrice),num2cell(Delta),num2cell(DeltaMT),num2cell(Gamma),num2cell(Vega),num2cell(Theta),...
        num2cell(OriginalAmountPaid),num2cell(m2m_usd),num2cell(m2m_nc),...        
        num2cell(cumulative_m2m_usd),num2cell(cumulative_m2m_nc),...
        num2cell(realised_m2m_usd),num2cell(unrealised_m2m_usd),...
        num2cell(realised_m2m_nc),num2cell(unrealised_m2m_nc),...
        num2cell(Price),num2cell(PriceMove),num2cell(PriceMoveParameter),num2cell(Vol),num2cell(VolMove),...
        cellstr(datestr(Time)),num2cell(TimeMove)];
    OutputHeader = [OutputHeader,'calculated_price','delta','delta_in_metric_tonnes','gamma','vega','theta',...
        'original_amount_paid','m2m_usd','m2m_nc',...
        'cumulative_m2m_usd','cumulative_m2m_nc',...
        'realised_m2m_usd','unrealised_m2m_usd',...
        'realised_m2m_nc','unrealised_m2m_nc',...
        'price','pricemove',pricemoveparametername,'vol','volmove','time','timemove'];
else
    OutData = [OutData,num2cell(CalculatedPrice),num2cell(Delta),num2cell(Gamma),num2cell(Vega),num2cell(Theta),...
        num2cell(OriginalAmountPaid),num2cell(m2m_usd),num2cell(m2m_nc),...
        num2cell(cumulative_m2m_usd),num2cell(cumulative_m2m_nc),...
        num2cell(realised_m2m_usd),num2cell(unrealised_m2m_usd),...
        num2cell(realised_m2m_nc),num2cell(unrealised_m2m_nc)];
    OutputHeader = [OutputHeader,'calculated_price','delta','gamma','vega','theta',...
        'original_amount_paid','m2m_usd','m2m_nc',...
        'cumulative_m2m_usd','cumulative_m2m_nc',...
        'realised_m2m_usd','unrealised_m2m_usd',...
        'realised_m2m_nc','unrealised_m2m_nc'];
end




