% function create_risk_matrix(User_Want_Fields,UniqueFields,VariationLength,StepSize)
function [Input_FileName] = create_risk_matrix(Input_FileName,SummmaryParameters,OutputFields,InBUName)

% Input_FileName = 'UAT_settle_SensitivityReport_18-Apr-2017_16-31-09.xlsx';

try
    [~,~,RawData]  = xlsread(Input_FileName,'Summary Report') ;
    
    Header = RawData(1,:);
    RawData = RawData(2:end,:);
    
    %%
    if(strcmpi(InBUName,'cot'))
        User_Required_Fields = {'delta_in_metric_tonnes','diff_m2m_usd','gamma','vega','theta'};
        OutputFields = [OutputFields , {'diff_m2m_usd'} ];
    else
        User_Required_Fields = {'delta','m2m_usd','gamma','vega','theta'};
    end
    User_Want_Fields = OutputFields( cellStrfind_exact(OutputFields,User_Required_Fields) ) ;
    
    % UniqueFields = {'product_code','instrument','subportfolio','counterparty','contract_month','derivative_type','strike','current_premium','exchange_name','active_lots'} ;
    if( strcmpi(InBUName,'coc') || strcmpi(InBUName,'cot') || strcmpi(InBUName,'plm') || strcmpi(InBUName,'rbr')) || (strcmpi(InBUName,'ogp')) || (strcmpi(InBUName,'og2'))
        UniqueFields = [SummmaryParameters , {'active_lots'}];
    else
        UniqueFields = SummmaryParameters;
    end
    
    Unique_Col_Index = cellStrfind_exact(Header , UniqueFields);
    
    Temp_Matrix = RawData(:,Unique_Col_Index);  %% My Work for Decimal Numbers
    [uCA,L1IdxIn,L1IdxOut] = uniqueRowsCA(Temp_Matrix);
    L1IdxIn = sort(L1IdxIn);
    
    %%
    % VariationLength = 20 ;
    % VariationLength_Neg = (VariationLength / 2)*(-1) ;
    % VariationLength_Pos = (VariationLength / 2)*(1) ;
    % StepSize = 0.5 ;
    % Total_Periods = (VariationLength /StepSize) + 1; %% (VariationLength_Neg : StepSize : VariationLength_Pos)
    %
    % New_Header_AddStr = strread(num2str(linspace(VariationLength_Neg,VariationLength_Pos,Total_Periods)),'%s')';
    %
    % New_Header = [];
    % for k = 1 :  length(User_Want_Fields)
    %
    %     Current_Field = User_Want_Fields(k);
    %     Temp_Cell = cell(1,size(New_Header_AddStr,2));
    %     Temp_Cell(:,:) = Current_Field;
    %     New_Header = [ New_Header , strcat(Temp_Cell,{' '},New_Header_AddStr) ];
    %
    % end
    
    %%
    
    New_Header = [];
    Temp_Data = [];
    Overall_Data = [];
    
    for i = 1 : size(L1IdxIn,1)
        
        if(i==1)
            StartCount = 1;
            EndCount = L1IdxIn(i);
        else
            StartCount = L1IdxIn(i-1) + 1;
            EndCount = L1IdxIn(i);
        end
        
        Temp_Matrix = RawData(StartCount:EndCount,:);
        Unique_Row = RawData(StartCount , cellStrfind_exact(Header , UniqueFields));
        
        for ii = 1 :  length(User_Want_Fields)
            
            Current_Field = User_Want_Fields(ii);
            Current_Fields_Index = cellStrfind_exact(Header,Current_Field);
            User_Want_Data =  Temp_Matrix(:,Current_Fields_Index)';
            
            if(i==1)
                if(strcmpi(InBUName,'cot'))
                    PriceHeader_AddStr = cellfun(@num2str, Temp_Matrix(:,cellStrfind_exact(Header,{'pricemove_in_percentage'})), 'UniformOutput', false)';
                else
                    PriceHeader_AddStr = cellfun(@num2str, Temp_Matrix(:,cellStrfind_exact(Header,{'pricemove'})), 'UniformOutput', false)';
                end
                VolHeader_AddStr = cellfun(@num2str, Temp_Matrix(:,cellStrfind_exact(Header,{'volmove'})), 'UniformOutput', false)';
                
                if(~isempty(PriceHeader_AddStr) && ~isempty(VolHeader_AddStr))
                    New_Header_AddStr = strcat('(',PriceHeader_AddStr,')','(',VolHeader_AddStr,')');
                elseif(~isempty(PriceHeader_AddStr))
                    New_Header_AddStr = strcat('(',PriceHeader_AddStr,')');
                else
                    New_Header_AddStr = strcat('(',VolHeader_AddStr,')');
                end
                
                Temp_Cell = cell(1,size(New_Header_AddStr,2));
                Temp_Cell(:,:) = Current_Field;
                New_Header = [New_Header , strcat(Temp_Cell,{' '},New_Header_AddStr) ];
                
            end
            
            
            Temp_Data = [Temp_Data , User_Want_Data ];
            
        end
        
        
        Overall_Data = [Overall_Data ; [Unique_Row,Temp_Data] ];
        
        Temp_Data = [];
        
    end
    
    %%
    
    Overall_Header = [UniqueFields , New_Header];
    
    xlswrite(Input_FileName,[Overall_Header ; Overall_Data],'Risk Matrix');
    
    
    %% Consolidated Output
    
    if(strcmpi(InBUName,'coc') || strcmpi(InBUName,'plm') || strcmpi(InBUName,'rbr') || strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2'))
        if(~isempty(cellStrfind(Overall_Data(:,3),{'-O'})))
            UniqueFields = Overall_Header(cellStrfind_exact(Overall_Header,UniqueFields));
            SumFields = Overall_Header(8:end); %% OFUT
        else
            UniqueFields = Overall_Header(cellStrfind_exact(Overall_Header,UniqueFields));
            SumFields = Overall_Header(6:end); %% FUT
        end
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = '';
        [OutputFields,TempConsDeals] = consolidatedata(Overall_Header,Overall_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        xlswrite(Input_FileName,[OutputFields;TempConsDeals],'Consolidated');
    end
    
catch ME
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

