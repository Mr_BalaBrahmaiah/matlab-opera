function [OutFilename,SummaryData] = calcPriceForScenarios_SubFunction_Struct(Matfile_Path,InBUName)

aa = load(Matfile_Path);  %% aa = load('october28.mat')

Loop_Length = length(aa.PriceStepVec );

SummaryData = [];
parfor iPrice = 1 : Loop_Length
    TStart = tic;
    PriceStepParameter = aa.PriceStepParameter;
    if strcmpi(PriceStepParameter,'Percentage Move')
        if(strcmpi(aa.InBUName,'coc') || strcmpi(aa.InBUName,'cof') || strcmpi(aa.InBUName,'grn') || strcmpi(aa.InBUName,'plm') || strcmpi(aa.InBUName,'rbr')) strcmpi(aa.InBUName,'ogp') || strcmpi(aa.InBUName,'og2')
            PriceMove = aa.Price + (aa.Price * (aa.PriceStepVec (iPrice)) / 100);
        else
            PriceMove = aa.Price + (aa.PriceStepVec (iPrice) / 100);
        end
    else
        PriceMove = aa.Price + (aa.PriceStepVec (iPrice).*aa.TickSize);
    end
    PriceMove(PriceMove<0) = 0;
    
    [bb] = calcPriceForScenarios_SubFunction_2(TStart,aa,iPrice,PriceMove,aa.InBUName);
    
    
    SummaryData = [SummaryData;bb.ConsDeals];
    
    
end

OutFilename = aa.OutFilename;
% xlswrite(aa.OutFilename,SummaryData,'Summary Report');
% TempXLSFileName = fullfile(pwd,aa.OutFilename);
% xls_delete_sheets(TempXLSFileName);
% fprintf('CalcPrice : %f\n',toc);

end


%%
function [aa] = calcPriceForScenarios_SubFunction_2(TStart,aa,iPrice,PriceMove,InBUName)

Second_Loop_Length = length(aa.VolStepVec);

for iVol = 1 : Second_Loop_Length
    if(strcmpi(InBUName,'cof'))
        VolMove = aa.Vol + (aa.Vol * (aa.VolStepVec(iVol) )); %% No need to divide 100 already divided
        VolMove(VolMove<0) = 0;
    else
        VolMove = aa.Vol + aa.VolStepVec(iVol);
        VolMove(VolMove<0) = 0;
    end
    
    aa.VolMove = VolMove;
    
    for iTime = 1:length(aa.TimeStepVec)
        TimeMove = aa.Maturity - aa.TimeStepVec(iTime);
        
        % to fix the time sensitivity issue in future expiry with time move
        IdxFutDead = logical(zeros(size(aa.IdxLiveFuture)));
        IdxFutDead(aa.IdxLiveFuture) = aa.ValueDate > TimeMove(aa.IdxLiveFuture);
        TimeMove(IdxFutDead) = aa.Maturity(IdxFutDead);
        
        aa.TimeMove = TimeMove;
        
        aa.InPricerData(:,aa.PosPrice) = num2cell(PriceMove);
        aa.InPricerData(:,aa.PosMaturity) = num2cell(aa.TimeMove);
        
        
        PricingType = aa.PricingType;
        
        if strcmpi(PricingType,'traders')
            if any(IdxFloat)
                aa.InPricerData(IdxFloat,PosFloatVol) = num2cell(aa.VolMove(IdxFloat));
            end
            if any(IdxFixed)
                aa.InPricerData(IdxFixed,PosFixedVol) = num2cell(aa.VolMove(IdxFixed));
            end
        else
            aa.InPricerData(:,aa.PosVol) = num2cell(aa.VolMove);
        end
        
        aa.DebugIterationCount = aa.DebugIterationCount + 1;
        disp(['Iteration ...',num2str(aa.DebugIterationCount)]);
        % in final output, price = pricemove, pricemove = pricestepsize with
        % direction
        % vol = volmove, volmove = volstepsize with direction
        % time = timemove, timemove = time stepsize with direction
        NumInputs = size(aa.Price);
        OutPriceStepSize = repmat(aa.PriceStepVec (iPrice),NumInputs);
        
        PriceStepParameter = aa.PriceStepParameter;
        
        if strcmpi(PriceStepParameter,'Percentage Move')
            OutPriceMoveValue =  repmat(aa.PriceStepVec (iPrice)/100,NumInputs) ;
        else
            OutPriceMoveValue = aa.PriceStepVec(iPrice).*aa.TickSize;
        end
        OutVolStepSize = repmat(aa.VolStepVec(iVol),NumInputs);
        OutTimeStepSize = repmat(aa.TimeStepVec(iTime),NumInputs);
        OutTimeMove = repmat(aa.ValueDate + aa.TimeStepVec(iTime),NumInputs);
        
        OutPricerData = sensitivity_derivativepricer_interpvols(aa.InBUName,aa.InPricerData, aa.InPricerHeading, aa.OutPricerHeading,aa.PricingType,aa.DBVolData);
        
        OutPricerData = [OutPricerData,num2cell(PriceMove),num2cell(OutPriceStepSize),num2cell(OutPriceMoveValue),num2cell(aa.VolMove),num2cell(OutVolStepSize),num2cell(OutTimeMove),num2cell(OutTimeStepSize)];
        [OutputHeader,OutData] = generate_pricing_views(aa.PricingType,aa.InputHeader,aa.InputData,aa.PricerHeading,OutPricerData,aa.IsSensitivityAnalysis);
        [OutputFields,TempConsDeals] = consolidatedata(OutputHeader, OutData,aa.SummmaryParameters,aa.SumFields,aa.OutputFields,aa.WeightedAverageFields);
        if (aa.PriceStepVec(iPrice)==0) && (aa.VolStepVec(iVol)==0 ) && (aa.TimeStepVec(iTime)==0)
            TElapsed = toc(TStart);
            try
                % Generate the information to be displayed as an intermediate step
                NumSecurities = length(unique(upper(aa.InPricerData(:,aa.Pos_SecurityId))));
                NumBaseRecords = size(OutData,1);
                TimeBaseScenario = TElapsed + 45; % 30 secs - to read data from sensitivity_report_cache table and 15 secs to read from pricing table
                NumScenarios = length(aa.PriceStepVec ) * length(aa.VolStepVec) * length(aa.TimeStepVec);
                NumOutputRecords = NumBaseRecords * NumScenarios;
                NumCalculations = NumSecurities * NumScenarios;
                TExpTotal = (TElapsed * NumScenarios) + 45 + 10;% 30 secs - to read data from sensitivity_report_cache table and 15 secs to read from pricing table; 10 secs to write data in xl
                StringInfo = {' ';'Number of Unique Securities to be priced (Input for Pricing)';...
                    'Number of records in Base Scenario (Output for Sensitivity)';...
                    'Time taken for Base Scenario - Seconds';...
                    'Number of Scenarios';...
                    'Total number of records in Output for all scenarios';...
                    ' ';
                    'Total number of calculations (Calculation Intensity)';...
                    'Texpected Time for All Scenario'};
                StatInfo = {[];NumSecurities;NumBaseRecords;TimeBaseScenario;...
                    NumScenarios;NumOutputRecords;[];NumCalculations;TExpTotal};
                xlswrite(aa.OutFilename,[StringInfo,StatInfo],'Summary Statistics');
            catch
            end
            %                     xlswrite(OutFilename,[OutputHeader;OutData],'Base Scenario Evaluation');
        end
        % %     UniqueFields  = varargin{1};    SumFields     = varargin{2};    OutputFields  = varargin{3};    WeightedAverageFields = varargin{4};
        
        aa.ConsDeals = [aa.ConsDeals;TempConsDeals];
    end
end

end


