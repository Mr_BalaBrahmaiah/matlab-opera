function [OutErrorMsg,SummaryStatisticsInfo] = calculate_sensitivity_summary(InBUName,InputFilterValues,InputScenarios)
% calculate the summary statistics info before generating the actual
% sensitivity report

OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_sensitivity_report_view_cache_table';
    [InReportHeader,InReportData] = read_from_database(ViewName,0,'',InBUName);
    
    PriceNumSteps = 0; PriceStepSize = 0;
    VolNumSteps   = 0; VolStepSize   = 0;
    TimeNumSteps  = 0; TimeStepSize  = 0;
    
    if ~isempty(InputScenarios)
        IsPriceMove = strcmpi(InputScenarios{1,1},'on');
        if IsPriceMove
            PriceNumSteps = InputScenarios{1,2};
            if isempty(PriceNumSteps)
                PriceNumSteps = 1;
            end
            PriceStepSize = InputScenarios{1,3};
            if isempty(PriceStepSize)
                PriceStepSize = 0;
            end
        end
        
        IsVolMove = strcmpi(InputScenarios{2,1},'on');
        if IsVolMove
            VolNumSteps   = InputScenarios{2,2};
            if isempty(VolNumSteps)
                VolNumSteps = 1;
            end
            VolStepSize   = InputScenarios{2,3};
            if isempty(VolStepSize)
                VolStepSize = 0;
            else
                VolStepSize = VolStepSize / 100;
            end
        end
        
        IsTimeMove = strcmpi(InputScenarios{3,1},'on');
        if IsTimeMove
            TimeNumSteps  = InputScenarios{3,2};
            if isempty(TimeNumSteps)
                TimeNumSteps = 1;
            end
            TimeStepSize  = InputScenarios{3,3};
            if isempty(TimeStepSize)
                TimeStepSize = 0;
            end
        end
    end
    
    % construct the stepsize vectors
    PriceStepVec = constructStepSizeVec(PriceNumSteps, PriceStepSize,1);
    VolStepVec  = constructStepSizeVec(VolNumSteps, VolStepSize,1);
    TimeStepVec = constructStepSizeVec(TimeNumSteps, TimeStepSize,0);
    
    if isempty(InputFilterValues)
        IdxReportData = logical(zeros(size(InReportData,1),1));
    else
        FilterKeyNames = InputFilterValues(:,1);
        
        for iKey = 1:length(FilterKeyNames)
            KeyName = FilterKeyNames{iKey};
            if iKey == 1
                IdxReportData = getValueFromKeyValuePair(KeyName);
            else
                IdxTempKey = getValueFromKeyValuePair(KeyName);
                IdxReportData = IdxReportData & IdxTempKey;
            end
        end
    end
    
    InputData = InReportData(IdxReportData,:);
    Pos_InSecurity = strcmpi('security_id',InReportHeader);
    InSecurityId = unique(upper(InputData(:,Pos_InSecurity)));
    
    NumSecurities = length(InSecurityId);
    NumBaseRecords = size(InputData,1);
    TElapsed = (NumBaseRecords/2000); % found that it takes approx 2 secs to process 1000 records; hence this formula is derived-if it takes 2 sec for 1000 records, how much time does it take for NumBaseRecords
    TimeBaseScenario = TElapsed + 45; % time to read data from sensitivity_report_cache table and to read from pricing table
    NumScenarios = length(PriceStepVec) * length(VolStepVec) * length(TimeStepVec);
    NumOutputRecords = NumBaseRecords * NumScenarios;
    NumCalculations = NumSecurities * NumScenarios;
    TExpTotal = (TElapsed * NumScenarios) + 45 + 10;% time to read data from sensitivity_report_cache table and to read from pricing table; 10 secs to write data in xl
    
    SummaryStatisticsInfo = cellstr(strvcat(...
        strcat('Number of Unique Securities to be priced (Input for Pricing) : ',num2str(NumSecurities)),...
        strcat('Number of records in Base Scenario (Output for Sensitivity) : ',num2str(NumBaseRecords)),....
        strcat('Time expected for Base Scenario(in Seconds) :    ',num2str(TimeBaseScenario)),...
        strcat('Number of Scenarios :     ',num2str(NumScenarios)),...
        strcat('Total number of records in Output for all scenarios : ',num2str(NumOutputRecords)),...
        strcat('Total number of calculations (Calculation Intensity) : ',num2str(NumCalculations)),...
        strcat('Time expected for All Scenarios(in Seconds) : ',num2str(TExpTotal))));
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    SummaryStatisticsInfo = {''};
end

    function IdxValue = getValueFromKeyValuePair(KeyName)
        disp('KeyName:'); disp(KeyName);
        IdxKey =  find(strcmpi(InputFilterValues,KeyName));
        if ~isempty(IdxKey)
            InValues = InputFilterValues{IdxKey,2};
            PosKeyName = strcmpi(KeyName,InReportHeader);
            DBKeyValues = InReportData(:,PosKeyName);
            % class of this DBKeyValues can be either double or cell
            % double, if the field is numeric in db
            % all other fields will be of class cell
            disp(class(cell2mat(InValues)));
            if strcmpi(class(cell2mat(InValues)),'double')
                InValues = cell2mat(InValues);
                DBKeyValues = utilgetvalidnumericarray(DBKeyValues);
                IdxValue = ismember(DBKeyValues,InValues);
            else
                if ~isempty(cellStrfind(KeyName,'date'))
                    % to handle the dates
                    if strcmpi(KeyName,'transaction_date')
                        StartDate = datenum(InValues{1},'yyyy-mm-dd');
                        EndDate   = datenum(InValues{2},'yyyy-mm-dd');
                        DBDateValues = datenum(DBKeyValues,'yyyy-mm-dd');
                        IdxValue = (DBDateValues >= StartDate) & (DBDateValues <= EndDate);
                    end
                    
                else % check if all is selected or specific inputs
                    if ismember('all',InValues)
                        IdxValue = logical(ones(size(DBKeyValues)));
                    else
                        IdxKeyFound = cellStrfind(upper(DBKeyValues),upper(InValues));
                        IdxValue = logical(zeros(size(DBKeyValues)));
                        IdxValue(IdxKeyFound) = 1;
                    end
                end
            end
            
        else
            IdxValue = logical(zeros(size(InReportData,1),1));
        end
        
    end

end

function OutSteps = constructStepSizeVec(NumSteps, StepSize,IsBothDirections)
OutSteps = 0;
for iS = 1:NumSteps
    OutSteps = [OutSteps, iS * StepSize];
    if IsBothDirections
        OutSteps = [OutSteps, -(iS * StepSize)];
    end
end
end