function OutFilename = calcPriceForScenarios(InBUName,InputData,InputHeader,PricingType,...
    PriceStepParameter,PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
    SummmaryParameters,SumFields,OutputFields,WeightedAverageFields,OutFilename)

IsSensitivityAnalysis = 1;

% from the netted data, get the list of securities for which we need to
% price
Pos_InSecurity = strcmpi('security_id',InputHeader);
InSecurityId = unique(upper(InputData(:,Pos_InSecurity)),'stable');

if strcmpi(PricingType,'settle')
    ViewName = 'input_security_settlement_value_cache_table';
    TableName = 'settlement_vol_surf_table';
else
    ViewName = 'input_security_traders_value_cache_table';
    TableName = ['traders_vol_surf_fixed_table_',InBUName];
end

% Read the values from database for derivative pricer
[InPricerHeading,PricerData] = read_from_database(ViewName,0,'',InBUName);

if isequal(InPricerHeading,{[]}) % no data found in database for the selected criteria
    disp('No data found in database for the selected criteria!');
else
    [ColNames,Data] = read_from_database(TableName,1);
    DBVolData = cell2dataset([ColNames;Data]);
    
    Pos_SecurityId = strcmpi('security_id',InPricerHeading);
    SecurityId = upper(PricerData(:,Pos_SecurityId));
    
    % for the list of security ids in the selected input record, compute the
    % prices
    IdxSecurity = ismember(SecurityId,InSecurityId);
    InPricerData = PricerData(IdxSecurity,:);
    
    OutPricerHeading = {'security_id','value_date','price','delta_1','delta_2','gamma_11','gamma_12','gamma_21','gamma_22','vega_1','vega_2','theta'};
    if strcmpi(PricingType,'traders')
        OutPricerHeading = ['subportfolio',OutPricerHeading];
        PosVolSurfType = strcmpi('vol_surf_type',InPricerHeading);
        VolSurfType = InPricerData(:,PosVolSurfType);
        Vol = zeros(size(VolSurfType));
        IdxFloat = strcmpi('float_3param',VolSurfType);
        IdxFixed = ~IdxFloat;
        if any(IdxFloat)
            PosFloatVol = strcmpi('atm1_vol',InPricerHeading);
            Vol(IdxFloat) = utilgetvalidnumericarray(InPricerData(IdxFloat,PosFloatVol));
        end
        if any(IdxFixed)
            PosFixedVol = strcmpi('p1_vol',InPricerHeading);
            Vol(IdxFixed) = utilgetvalidnumericarray(InPricerData(IdxFixed,PosFixedVol));
        end
    else
        PosVol = strcmpi('p1_vol',InPricerHeading);
        Vol = utilgetvalidnumericarray(InPricerData(:,PosVol));
    end
    PosPrice = strcmpi('p1_settleprice',InPricerHeading);
    Price = utilgetvalidnumericarray(InPricerData(:,PosPrice));
    PosMaturity = strcmpi('maturity',InPricerHeading);
    Maturity = datenum(InPricerData(:,PosMaturity),'yyyy-mm-dd');
    PosValueDate = strcmpi('value_date',InPricerHeading);
    ValueDate = datenum(InPricerData(1,PosValueDate),'yyyy-mm-dd');
    PosTickSize = strcmpi('tick_size',InPricerHeading);
    TickSize = utilgetvalidnumericarray(InPricerData(:,PosTickSize));
    PosOptType = strcmpi('opt_type',InPricerHeading);
    IdxLiveFuture = strcmpi('future',InPricerData(:,PosOptType)) & (ValueDate <= Maturity);
    
    if strcmpi(PriceStepParameter,'Percentage Move')
        OutPriceMoveParameter = 'pricemove_in_percentage';
    else
        OutPriceMoveParameter = 'pricemove_in_pricetick';
    end
    
    PricerHeading = [OutPricerHeading,'future_price','pricemove',OutPriceMoveParameter,'vol','volmove','time','timemove'];
    
    % construct the stepsize vectors
    PriceStepVec = constructStepSizeVec(PriceNumSteps, PriceStepSize,1);
    VolStepVec  = constructStepSizeVec(VolNumSteps, VolStepSize,1);
    TimeStepVec = constructStepSizeVec(TimeNumSteps, TimeStepSize,0);
    
    % Apply the price move in p1_settle_price
    % apply the vol move in settle_vol/traders_vol
    % apply the time move by decreasing the maturity date to that amount
    ConsDeals = [];DebugIterationCount = 0;
    
    for iPrice = 1:length(PriceStepVec)
        TStart = tic;
        if strcmpi(PriceStepParameter,'Percentage Move')
            if(strcmpi(InBUName,'coc') || strcmpi(InBUName,'cof') || strcmpi(InBUName,'grn') || strcmpi(InBUName,'sgr') || strcmpi(InBUName,'dry') || strcmpi(InBUName,'plm') || strcmpi(InBUName,'rbr') || strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2')) %%% newly added BU's PLM and RBR
                PriceMove = Price + (Price * (PriceStepVec(iPrice) / 100));
            else
                PriceMove = Price + (PriceStepVec(iPrice) / 100);
            end
            
        else
            PriceMove = Price + (PriceStepVec(iPrice).*TickSize);
        end
        PriceMove(PriceMove<0) = 0;
        for iVol = 1:length(VolStepVec)
            %% Below LOC are written for logic given by Jessy - Coffee RO in 2018; 
            %% based on the request from Marius in 2019, vol move for cofee also follows the same logic as other BUs like Cocoa
%             if(strcmpi(InBUName,'cof'))
%                 VolMove = Vol + (Vol * (VolStepVec(iVol))); %% No need to divide 100 already divided
%                 VolMove(VolMove<0) = 0;
%             else
                VolMove = Vol + VolStepVec(iVol);
                VolMove(VolMove<0) = 0;
%             end
            
            for iTime = 1:length(TimeStepVec)
                TimeMove = Maturity - TimeStepVec(iTime);
                
                % to fix the time sensitivity issue in future expiry with time move
                IdxFutDead = logical(zeros(size(IdxLiveFuture)));
                IdxFutDead(IdxLiveFuture) = ValueDate > TimeMove(IdxLiveFuture);
                TimeMove(IdxFutDead) = Maturity(IdxFutDead);
                
                InPricerData(:,PosPrice) = num2cell(PriceMove);
                InPricerData(:,PosMaturity) = num2cell(TimeMove);
                if strcmpi(PricingType,'traders')
                    if any(IdxFloat)
                        InPricerData(IdxFloat,PosFloatVol) = num2cell(VolMove(IdxFloat));
                    end
                    if any(IdxFixed)
                        InPricerData(IdxFixed,PosFixedVol) = num2cell(VolMove(IdxFixed));
                    end
                else
                    InPricerData(:,PosVol) = num2cell(VolMove);
                end
                
                DebugIterationCount = DebugIterationCount + 1;
                disp(['Iteration ...',num2str(DebugIterationCount)]);
                % in final output, price = pricemove, pricemove = pricestepsize with
                % direction
                % vol = volmove, volmove = volstepsize with direction
                % time = timemove, timemove = time stepsize with direction
                NumInputs = size(Price);
                OutPriceStepSize = repmat(PriceStepVec(iPrice),NumInputs);
                if strcmpi(PriceStepParameter,'Percentage Move')
                    OutPriceMoveValue =  repmat(PriceStepVec(iPrice)/100,NumInputs) ;
                else
                    OutPriceMoveValue = PriceStepVec(iPrice).*TickSize;
                end
                OutVolStepSize = repmat(VolStepVec(iVol),NumInputs);
                OutTimeStepSize = repmat(TimeStepVec(iTime),NumInputs);
                OutTimeMove = repmat(ValueDate + TimeStepVec(iTime),NumInputs);
                
                OutPricerData = sensitivity_derivativepricer_interpvols(InBUName,InPricerData, InPricerHeading, OutPricerHeading,PricingType,DBVolData);
                
                OutPricerData = [OutPricerData,num2cell(PriceMove),num2cell(OutPriceStepSize),num2cell(OutPriceMoveValue),num2cell(VolMove),num2cell(OutVolStepSize),num2cell(OutTimeMove),num2cell(OutTimeStepSize)];
                [OutputHeader,OutData] = generate_pricing_views(PricingType,InputHeader,InputData,PricerHeading,OutPricerData,IsSensitivityAnalysis);
                %                 if strcmpi(InBUName,'cot')
                %                     PosTheta = strcmpi('theta',OutputHeader);
                %                     OutData(:,PosTheta) = num2cell(cell2mat(OutData(:,PosTheta)) .* (5/7));
                %                 end
                
                [OutputFields,TempConsDeals] = consolidatedata(OutputHeader, OutData,SummmaryParameters,SumFields,OutputFields,WeightedAverageFields);
                if (PriceStepVec(iPrice)==0) && (VolStepVec(iVol) ==0 ) && (TimeStepVec(iTime)==0)
                    TElapsed = toc(TStart);
                    try
                        % Generate the information to be displayed as an intermediate step
                        NumSecurities = length(unique(upper(InPricerData(:,Pos_SecurityId))));
                        NumBaseRecords = size(OutData,1);
                        TimeBaseScenario = TElapsed + 45; % 30 secs - to read data from sensitivity_report_cache table and 15 secs to read from pricing table
                        NumScenarios = length(PriceStepVec) * length(VolStepVec) * length(TimeStepVec);
                        NumOutputRecords = NumBaseRecords * NumScenarios;
                        NumCalculations = NumSecurities * NumScenarios;
                        TExpTotal = (TElapsed * NumScenarios) + 45 + 10;% 30 secs - to read data from sensitivity_report_cache table and 15 secs to read from pricing table; 10 secs to write data in xl
                        StringInfo = {' ';'Number of Unique Securities to be priced (Input for Pricing)';...
                            'Number of records in Base Scenario (Output for Sensitivity)';...
                            'Time taken for Base Scenario - Seconds';...
                            'Number of Scenarios';...
                            'Total number of records in Output for all scenarios';...
                            ' ';
                            'Total number of calculations (Calculation Intensity)';...
                            'Texpected Time for All Scenario'};
                        StatInfo = {[];NumSecurities;NumBaseRecords;TimeBaseScenario;...
                            NumScenarios;NumOutputRecords;[];NumCalculations;TExpTotal};
                        if(strcmpi(InBUName,'cfs'))
                            xlswrite(OutFilename,[StringInfo,StatInfo],'Summary Statistics');
                        end
                    catch
                    end
                    %                     xlswrite(OutFilename,[OutputHeader;OutData],'Base Scenario Evaluation');
                end
                % %     UniqueFields  = varargin{1};    SumFields     = varargin{2};    OutputFields  = varargin{3};    WeightedAverageFields = varargin{4};
                
                ConsDeals = [ConsDeals;TempConsDeals];
            end
        end
    end
    
    try
        IdxPriceMove = find(strcmpi(OutputFields,'pricemove'));
        IdxVolMove = find(strcmpi(OutputFields,'volmove'));
        SortIndex = 1:length(SummmaryParameters);
        if ~isempty(IdxPriceMove)
            SortIndex = [SortIndex,IdxPriceMove];
        end
        if ~isempty(IdxVolMove)
            SortIndex = [SortIndex,IdxVolMove];
        end
        ConsDeals = sortrows(ConsDeals,SortIndex);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    xlswrite(OutFilename,[OutputFields;ConsDeals],'Summary Report');
    
    %% Uploading Summary Data
    %     ObjDB = connect_to_database;
    %     Value_Settle_Date = fetch(ObjDB,'select * from valuation_date_table');
    %     ValueDate = Value_Settle_Date(2);
    %     Settle_Date = Value_Settle_Date(3);
    %
    %     TempDate_Array = cell(size(ConsDeals,1),2);
    %     TempDate_Array(:,1) = ValueDate;
    %     TempDate_Array(:,2) = Settle_Date;
    %
    %     Uploading_Data = [TempDate_Array,ConsDeals];
    %     TableName = strcat('helper_dbtodw_propbooks_rm_greeks_table_',char(InBUName));
    %     set(ObjDB,'AutoCommit','off');
    %     SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Settle_Date),''''];
    %     curs = exec(ObjDB,SqlQuery);
    %     commit(ObjDB);
    %     if ~isempty(curs.Message)
    %         disp(curs.Message);
    %     end
    %     set(ObjDB,'AutoCommit','on');
    %     upload_in_database(TableName, Uploading_Data);
    %
    %     cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date : ',char(Settle_Date)]);
    
    %%
    try
        TempXLSFileName = fullfile(pwd,OutFilename);
        xls_delete_sheets(TempXLSFileName);
    catch
    end
    
end

    function OutSteps = constructStepSizeVec(NumSteps, StepSize,IsBothDirections)
        OutSteps = 0;
        for iS = 1:NumSteps
            OutSteps = [OutSteps, iS * StepSize];
            if IsBothDirections
                OutSteps = [OutSteps, -(iS * StepSize)];
            end
        end
    end


end

