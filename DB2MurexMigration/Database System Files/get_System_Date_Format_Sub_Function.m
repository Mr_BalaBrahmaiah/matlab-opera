% getsystemdateformat()
%    Returns local system date format in a form that is compatible for
%    use with datenum, datestr and other build-in date functions.
% 
% Copyright (c) 2012, Ivan Smirnov
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of the <organization> nor the
%       names of its contributors may be used to endorse or promote products
%       derived from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
% DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
function df = getsystemdateformat()
	dateInstance = java.text.DateFormat.getDateInstance();
	df = char(dateInstance.toPattern());

	df = regexprep(df, 'EEEE', '{W4}');
	df = regexprep(df, 'EEE', '{W3}');
	df = regexprep(df, 'E', '{W1}');
	df = regexprep(df, 'M', '{MON}');
	df = regexprep(df, 'm', '{MIN}');
	df = regexprep(df, 'dd', '{DAY}');
	df = regexprep(df, 'd', '{DAY}');
	df = regexprep(df, 's', '{SEC}');
	df = regexprep(df, 'a', '{AM}');

	df = regexprep(df, '{W4}', 'dddd');
	df = regexprep(df, '{W3}', 'ddd');
	df = regexprep(df, '{W1}', 'd');
	df = regexprep(df, '{MON}', 'm');
	df = regexprep(df, '{MIN}', 'M');
	df = regexprep(df, '{DAY}', 'dd');
	df = regexprep(df, '{SEC}', 'S');
	df = regexprep(df, '{AM}', 'AM');
 
    %% My Code
    df = strrep(df, 'mmm', 'mm');
    df = strrep(df, 'd', 'dd');
    df = strrep(df, 'ddd', 'dd');
    df = strrep(df, 'dddd', 'dd');
    
%     df = strrep(df,',','');
%     df = strrep(df,' ','/');
    
    df = strtrim(df);
    
%     disp(datestr(today, getsystemdateformat))
%     system('date');
    
    %%
    
    
end