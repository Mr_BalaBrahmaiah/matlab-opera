function [OTC_Swaps_Data_Output_Header,OTC_Swaps_Data,OTC_Swaps_Data_Output] = Make_SIPH_Format(OTC_Swaps_Data)

%% Headers

OTC_Swaps_Data_Output_Header = {'TRADE_DATE','TRADE_ID','AVG_MONTH','REF_MONTH','EXPIRY_DATE','LONG_LOTS','AVG_LONG_PRICE','SHORT_LOTS','AVG_SHORT_PRICE','OR_EDSP_USD',...
    'EUR_USD_FORWARD','OR_EDSP_EUR ','EUR_USD_SPOT ',' ',' ',' ',' ',' ',' ',' ','MTM_EUR'};

%% Consolidate Level for OTC Swaps SIPH

% TradeDate_Col_Swaps = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'TRADE_DATE'});
% TRADE_ID_Col_Swaps = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'TRADE_ID'});
% RefMonth_Col_Swaps = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'REF_MONTH'});
% EXPIRY_DATE_Col_Swaps = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'EXPIRY_DATE'});
% LONG_LOTS_Col_Swaps = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'LONG_LOTS'});
% UniqueFields_Swaps = {'TRADE_ID','EXPIRY_DATE','AVG_LONG_PRICE','AVG_SHORT_PRICE','EDSP'};
% SumFields_Swaps = {'LONG_LOTS','SHORT_LOTS','MTM_USD'};
% OutputFields_Swaps = OTC_Swaps_Data_Output_Header;
% WeightedAverageFields_Swaps = [];

%%

if(~isempty(OTC_Swaps_Data))
    OTC_Swaps_Data_Output = cell2dataset([OTC_Swaps_Data_Output_Header ; cell(size(OTC_Swaps_Data,1),size(OTC_Swaps_Data_Output_Header,2)) ]);
    
    for ii = 1 : size(OTC_Swaps_Data,1)
        OTC_Swaps_Data_Output.TRADE_DATE(ii) = OTC_Swaps_Data.transaction_date(ii);
        OTC_Swaps_Data_Output.TRADE_ID(ii) = OTC_Swaps_Data.contract_number(ii);
        OTC_Swaps_Data_Output.EXPIRY_DATE(ii) = OTC_Swaps_Data.maturity_date(ii);
        OTC_Swaps_Data_Output.AVG_MONTH(ii) = cellstr(datestr(OTC_Swaps_Data.averaging_startdate(ii),'mmm-yy'));
        OTC_Swaps_Data_Output.REF_MONTH(ii) = strcat(OTC_Swaps_Data.product_code(ii),{' '},datestr(OTC_Swaps_Data.future_expiry(ii),'mmm-yy'));
        
        if(OTC_Swaps_Data.active_lots(ii) >= 0)
            OTC_Swaps_Data_Output.LONG_LOTS(ii) = num2cell( OTC_Swaps_Data.active_lots(ii) );
            OTC_Swaps_Data_Output.AVG_LONG_PRICE(ii) = num2cell( round(OTC_Swaps_Data.traded_price(ii)*10,6) ); % Multiply with 10
        else
            OTC_Swaps_Data_Output.SHORT_LOTS(ii) = num2cell( OTC_Swaps_Data.active_lots(ii) );
            OTC_Swaps_Data_Output.AVG_SHORT_PRICE(ii) = num2cell( round(OTC_Swaps_Data.traded_price(ii)*10,6) );% Multiply with 10
        end
        
        OTC_Swaps_Data_Output.OR_EDSP_USD(ii) = num2cell( round(OTC_Swaps_Data.future_price(ii)*10,6) ) ;% Multiply with 10
        OTC_Swaps_Data_Output.EUR_USD_FORWARD(ii) =  num2cell(OTC_Swaps_Data.underlying2_price(ii));
        
        OTC_Swaps_Data_Output.OR_EDSP_EUR(ii) = num2cell(cell2mat( OTC_Swaps_Data_Output.OR_EDSP_USD(ii))./cell2mat(OTC_Swaps_Data_Output.EUR_USD_FORWARD(ii)) );
        OTC_Swaps_Data_Output.EUR_USD_SPOT(ii) = num2cell(OTC_Swaps_Data.curr_mult(ii));
        
        OTC_Swaps_Data_Output.MTM_EUR(ii) = num2cell(OTC_Swaps_Data.mtm_nc(ii));
        %         OTC_Swaps_Data_Output.MTM_EUR(ii) = num2cell((cell2mat(OTC_Swaps_Data_Output.OR_EDSP_EUR(ii)) - cell2mat(OTC_Swaps_Data_Output.OR_EDSP_USD(ii))).* OTC_Swaps_Data.active_lots(ii)*5);
        
    end
    
    %% Reference Month Wise Total
    
    %     [OTC_Swaps_Data_Output_Total] = Find_Statement_ContractMonth_Wise_Swaps_Total(OTC_Swaps_Data_Output_Header,OTC_Swaps_Data_Output);
    
    %%
    
end








