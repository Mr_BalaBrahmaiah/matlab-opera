function [OutErrorMsg,OutFilename] = generate_compliance_trade_view(InBUName)

OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    [SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    SettleData = cell2dataset([SettleColNames;Data]);
    clear Data;
    
    objDB = connect_to_database;
    DBOptTypeData = fetch(objDB,'select derivative_type,group_type,acc_group from derivative_type_table');
    DBOptType = DBOptTypeData(:,1);
    DBGroupType = DBOptTypeData(:,2);
    DBAccGroup = DBOptTypeData(:,3);
    
    InOptType = SettleData.derivative_type;
    InOptGroup =cell(size(InOptType));
    InAccGroup =cell(size(InOptType));
    
    for iOpt = 1:length(DBOptType)
        IdxOpt = strcmpi(DBOptType{iOpt},InOptType);
        InOptGroup(IdxOpt) = DBGroupType(iOpt);
        InAccGroup(IdxOpt) = DBAccGroup(iOpt);
    end
    
    IdxRemove = strcmpi('swap',InAccGroup) | strcmpi('fxd',InAccGroup) |  strcmpi('futures',InAccGroup);
    SettleData(IdxRemove,:) = [];
    InOptGroup(IdxRemove) = [];
    
    DBCallPutData = fetch(objDB,'select derivative_type,call_put_id from derivative_type_call_put_id_mapping_table');
    DBDerivType = DBCallPutData(:,1);
    DBCallPut = upper(DBCallPutData(:,2));
    
    CP_ID = cell(size(SettleData.derivative_type));
    for iOpt = 1:length(DBDerivType)
        IdxOpt = strcmpi(DBDerivType{iOpt},SettleData.derivative_type);
        CP_ID(IdxOpt) = DBCallPut(iOpt);
    end
    
    % retain only the bought/sold deals
    IdxBS = strcmpi('bought',SettleData.market_action) | strcmpi('sold',SettleData.market_action);
    SettleData(~IdxBS,:) = [];
    CP_ID(~IdxBS) = [];
    InOptGroup(~IdxBS) = [];
    
    IdxBuy = strcmpi('bought',SettleData.market_action);
    IdxSell = strcmpi('sold',SettleData.market_action);
    P_S = cell(size(IdxBuy));
    P_S(IdxBuy) = {'P'};
    P_S(IdxSell) = {'S'};
    
    % change the product code of Rubber to OR; since it is currently OR-EUR or
    % OR-USD
    IdxOR = strncmpi(SettleData.product_code,'OR',2);
    SettleData.product_code(IdxOR) = {'OR'};
    
    [OptionMonth,FutureMonth] = strtok(SettleData.contract_month,'.');
    INS_ORG_ID = cellstr([char(SettleData.product_code),char(OptionMonth)]);
    TRD_TIME = repmat({'NA'},size(INS_ORG_ID));
    
    OutputFields = {'TRADE_ID','INS_ORG_ID','BROKER_ACCT','INS_TYPE','STRIKE','CP','P_S','LOTS','TRD_DATE','TRD_TIME'};
    OutData = [SettleData.trade_id,INS_ORG_ID,SettleData.counterparty_parent,...
        InOptGroup,num2cell(SettleData.strike),CP_ID,P_S,num2cell(SettleData.active_lots),SettleData.transaction_date,TRD_TIME];
    
    OutFilename = getXLSFilename(strcat('Compliance_Trade_view_',char(InBUName)));
    xlswrite(OutFilename,[OutputFields;OutData]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end