clc
close all
clear


%% Choosing Uploading Folder

TargetDir = uigetdir('D:\00_MUTHUKUMAR\WORKS\00_Office\03_OlamAnalyticsLibrary\Database System Files\zz_Uploading_SettleVols','Choose Uploading Folder');
TargetFiles = dir(TargetDir);

TargetFiles(strcmpi('.',{TargetFiles.name})) = [];
TargetFiles(strcmpi('..',{TargetFiles.name})) = [];
TargetFiles(strcmpi('Thumbs.db',{TargetFiles.name})) = [];

for i = 1 : length(TargetFiles)
    
    Current_XLS_File = [TargetDir filesep TargetFiles(i).name];
    
    [~,~,RawData] = xlsread(Current_XLS_File);
    
    RawData = RawData(:,1:5);
    
    InData = RawData(2:end,:);
    
    %     %% Change NaN Fileds to NULL in 6 to 13 Columns
    %
    %     aa = InData(:,6:13);    %% Changing NaN to NULL
    %     bb = cellfun(@isnan, aa);
    %     aa(bb) = cellstr('NULL');
    %     InData(:,6:13) = aa;
    
    %% Find NaN Fileds from Date Column  and Remove NaN Fileds in All Columns
    
    NaN_Index = cellfun(@(s) any(isnan(s)),InData(:,1));  %% Find NaN Fields From Date Column
    InData(NaN_Index,:) = [];   %% Remove NaN Fileds Row From All Columns because Overcome DATENUM Failed.
    
    %%
    try
        if(isnumeric(InData{1,1}))
            InData(:,1) = cellstr(datestr(x2mdate(cell2mat(InData(:,1))),'yyyy-mm-dd'));
        else
            InData(:,1) = cellstr(datestr(datenum(InData(:,1),'dd-mm-yyyy'),'yyyy-mm-dd'));
        end
        
        
        Error = upload_in_database_new('settlement_vol_surf_table', InData);
        
        if(Error)
            if(~exist([pwd filesep 'zz_Pending']))
                mkdir([pwd filesep 'zz_Pending']);
            end
            Destination = [pwd filesep 'zz_Pending'];
            copyfile(Current_XLS_File,Destination);
            
        end
        
        fprintf('Finished XLSX File : %s : Current Count of File : %d : Total File Count :%d\n',TargetFiles(i).name,i,length(TargetFiles));
        
    catch ME
        Destination = [pwd filesep 'zz_Pending'];
        copyfile(Current_XLS_File,Destination);
        continue;
    end
    
    
end



%%


