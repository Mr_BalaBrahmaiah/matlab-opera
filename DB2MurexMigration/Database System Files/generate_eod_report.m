function [OutErrorMsg,EODFilename] = generate_eod_report(InBUName)
%  Generate the daily EOD report
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/05/15 07:28:37 $
%  $Revision: 1.8 $
%

OutErrorMsg = {'No errors'};
EODFilename = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_eod_monthend_report_view';
    [SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    SettleData = cell2dataset([SettleColNames;Data]);
    clear Data;
    
    % remove the internal counterparties
    IdxIntCP = strncmpi(SettleData.counterparty_parent,'MM',2) | strncmpi(SettleData.counterparty_parent,'RMS',3);
    SettleData(IdxIntCP,:) = [];
    
    % remove the derivative-type 'fx'
    IdxFx = strncmpi('fx',SettleData.derivative_type,length('fx'));
    SettleData(IdxFx,:) = [];
    
    IdxFuture = strcmpi('future',SettleData.derivative_type);
    SettleData(IdxFuture,:) = [];
    
    %% Include OFUT also as per Sridhar's request in Jan 2017
    %     IdxExchTraded = strcmpi('exchange_traded',SettleData.settlement_source);
    %     SettleData(IdxExchTraded,:) = [];
    %% Remove the MM portfolios for OFUT and retan only RMS OFUT trades
    IdxMMPF = strncmpi(SettleData.subportfolio,'MM',2) ;
    SettleData(IdxMMPF,:) = [];
    
    IdxORSwap = strcmpi('RMS-OR-SCE-OIR',SettleData.subportfolio) & ...
        strncmpi('avg_swap',SettleData.derivative_type,length('avg_swap'));
    SwapData = SettleData(IdxORSwap,:);
    SettleData(IdxORSwap,:) = [];
    
    IdxLondonProd = strcmpi('QC',SettleData.product_code) | strcmpi('DF',SettleData.product_code);
    
    TradeDates = datenum(SettleData.transaction_date,'yyyy-mm-dd');
    ExpDate = datenum('2015-01-01','yyyy-mm-dd');
    IdxDeadOpt = TradeDates >= ExpDate;
    
    IdxLiveOpt = strcmpi('live',SettleData.deal_status);
    
    %     IdxRMS = (strncmpi('RMS-SB',SettleData.subportfolio,length('RMS-SB')) | ...
    %         strncmpi('RMS-CT',SettleData.subportfolio,length('RMS-CT')) | ...
    %         strncmpi('RMS-C-',SettleData.subportfolio,length('RMS-C-')) | ...
    %         strncmpi('RMS-KC',SettleData.subportfolio,length('RMS-KC')));
    
    if isempty(SettleData)
        OutErrorMsg = {'No OTC options found for EOD Report!'};
        return;
    end
    
    NumRows = length(SettleData.subportfolio);
    
    IdxOID = zeros(NumRows,1); IdxOID = logical(IdxOID);
    IdxOID(cellStrfind(SettleData.subportfolio,'OID')) = 1;
    
    IdxRMS = strncmpi('RMS-',SettleData.subportfolio,length('RMS-')) & ~IdxOID;
%     IdxLondonProdPremPaid = IdxLondonProd & (SettleData.is_premium_paid == 1);
     IdxLondonProdPremPaid = IdxLondonProd & strcmpi('options',SettleData.acc_group);
    
    Realised_mtm_usd  = zeros(NumRows,1);
    Unrealised_mtm_usd = zeros(NumRows,1);
    Realised_mtm_nc = zeros(NumRows,1);
    Unrealised_mtm_nc = zeros(NumRows,1);
    
    Total_mtm_usd = zeros(NumRows,1);
    Total_mtm_nc = zeros(NumRows,1);
    
    DB_realised_cumulative_mtm_nc  = zeros(NumRows,1);
    DB_unrealised_cumulative_mtm_nc = zeros(NumRows,1);
    DB_realised_cumulative_mtm_usd = zeros(NumRows,1);
    DB_unrealised_cumulative_mtm_usd = zeros(NumRows,1);
    
    IdxDBDead = strcmpi('dead',SettleData.deal_status);
    IdxDBLive = strcmpi('live',SettleData.deal_status);
    
    DB_realised_cumulative_mtm_nc(IdxDBDead)   = SettleData.cumulative_mtm_nc(IdxDBDead);
    DB_unrealised_cumulative_mtm_nc(IdxDBLive) = SettleData.cumulative_mtm_nc(IdxDBLive);
    DB_realised_cumulative_mtm_usd(IdxDBDead) = SettleData.cumulative_mtm_usd(IdxDBDead);
    DB_unrealised_cumulative_mtm_usd(IdxDBLive) = SettleData.cumulative_mtm_usd(IdxDBLive);
    
    % realised option value = premium_paid of the dead options
    % unrealised option value = mkt value of the live options
    Realised_mtm_usd(IdxDeadOpt) = SettleData.premium_paid_usd(IdxDeadOpt);
    Unrealised_mtm_usd(IdxLiveOpt) = SettleData.mkt_value_usd(IdxLiveOpt);
    Realised_mtm_nc(IdxDeadOpt) = SettleData.premium_paid(IdxDeadOpt);
    Unrealised_mtm_nc(IdxLiveOpt) = SettleData.mkt_value(IdxLiveOpt);
    
    Realised_mtm_usd(IdxRMS) = SettleData.realised_pnl_usd(IdxRMS);
    Unrealised_mtm_usd(IdxRMS) = SettleData.unrealised_pnl_usd(IdxRMS);
    Realised_mtm_nc(IdxRMS) = SettleData.realised_pnl(IdxRMS) ;
    Unrealised_mtm_nc(IdxRMS) = SettleData.unrealised_pnl(IdxRMS) ;
    
    % realised/unrealised values for futures = cumulative_mtm calculation
    Realised_mtm_usd(IdxLondonProd) = DB_realised_cumulative_mtm_usd(IdxLondonProd);
    Unrealised_mtm_usd(IdxLondonProd) = DB_unrealised_cumulative_mtm_usd(IdxLondonProd);
    Realised_mtm_nc(IdxLondonProd) = DB_realised_cumulative_mtm_nc(IdxLondonProd);
    Unrealised_mtm_nc(IdxLondonProd) =DB_unrealised_cumulative_mtm_nc(IdxLondonProd);
    
    Realised_mtm_usd(IdxLondonProdPremPaid) = SettleData.realised_pnl_usd(IdxLondonProdPremPaid);
    Unrealised_mtm_usd(IdxLondonProdPremPaid) = SettleData.unrealised_pnl_usd(IdxLondonProdPremPaid);
    Realised_mtm_nc(IdxLondonProdPremPaid) = SettleData.realised_pnl(IdxLondonProdPremPaid);
    Unrealised_mtm_nc(IdxLondonProdPremPaid) =SettleData.unrealised_pnl(IdxLondonProdPremPaid);
    
    NumRows = length(SettleData.subportfolio);
    RealisedPremiumPaidUSD = zeros(NumRows,1);
    RealisedPremiumPaidNC  = zeros(NumRows,1);
    
    objDB = connect_to_database;
    SqlQuery = 'SELECT financial_year FROM valuation_financial_year_view';
    FinYear = char(fetch(objDB,SqlQuery));
    SqlQuery = ['select subportfolio,instrument,counterparty,contract_number,group_type,realised_premium_paid_usd,realised_premium_paid_nc from eod_realised_premium_table where financial_year = ''',FinYear,''''];
    DBSubportfolioOpenBalData = fetch(objDB,SqlQuery);
    if ~isempty(DBSubportfolioOpenBalData)
        DBSubportfolio = DBSubportfolioOpenBalData(:,1);
        DBInstrument = DBSubportfolioOpenBalData(:,2);
        DBCounterparty = DBSubportfolioOpenBalData(:,3);
        DBContractNo = DBSubportfolioOpenBalData(:,4);
        DBOptGroupType = DBSubportfolioOpenBalData(:,5);
        DBRealisedPremiumPaidUSD = cell2mat(DBSubportfolioOpenBalData(:,6));
        DBRealisedPremiumPaidNC  = cell2mat(DBSubportfolioOpenBalData(:,7));
        
        for iBal = 1:length(DBSubportfolio)
            IdxBal = strcmpi(DBSubportfolio(iBal),SettleData.subportfolio) & ...
                strcmpi(DBInstrument(iBal),SettleData.instrument) & ...
                strcmpi(DBCounterparty(iBal),SettleData.counterparty_parent) & ...
                strcmpi(DBContractNo(iBal),SettleData.contract_number) & ...
                strcmpi(DBOptGroupType(iBal),SettleData.group_type);
            RealisedPremiumPaidUSD(IdxBal) = DBRealisedPremiumPaidUSD(iBal);
            RealisedPremiumPaidNC(IdxBal)  = DBRealisedPremiumPaidNC(iBal);
        end
    end
    
    EODHeader = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType','Currency',...
        'RealisedNC', 'RealisedUSD','UnrealisedNC','UnrealisedUSD','TotalNC','TotalUSD','RealisedPremiumPaidNC','RealisedPremiumPaidUSD'};
    OutData = [upper(SettleData.instrument),upper(SettleData.subportfolio),upper(SettleData.counterparty_parent),...
        upper(SettleData.contract_number),SettleData.group_type,upper(SettleData.currency),...
        num2cell(Realised_mtm_nc),num2cell(Realised_mtm_usd),num2cell(Unrealised_mtm_nc),num2cell(Unrealised_mtm_usd),...
        num2cell(Total_mtm_nc),num2cell(Total_mtm_usd),num2cell(RealisedPremiumPaidNC),num2cell(RealisedPremiumPaidUSD)];
    UniqueFields = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType'};
    SumFields = { 'RealisedNC', 'RealisedUSD','UnrealisedNC','UnrealisedUSD','TotalNC','TotalUSD'};
    WeightedAverageFields = [];
    [~,EODOutData] = consolidatedata(EODHeader, OutData,UniqueFields,SumFields,EODHeader,WeightedAverageFields);
    
    Cons_Realised_mtm_nc = cell2mat(EODOutData(:,7)) - cell2mat(EODOutData(:,13));
    Cons_Realised_mtm_usd  = cell2mat(EODOutData(:,8)) - cell2mat(EODOutData(:,14));
    
    EODOutData(:,7) = num2cell(Cons_Realised_mtm_nc);
    EODOutData(:,8) = num2cell(Cons_Realised_mtm_usd);
    
    Total_mtm_nc = cell2mat(EODOutData(:,7)) +  cell2mat(EODOutData(:,9));
    Total_mtm_usd = cell2mat(EODOutData(:,8)) +  cell2mat(EODOutData(:,10));
    
    EODOutData(:,11) = num2cell(Total_mtm_nc);
    EODOutData(:,12) = num2cell(Total_mtm_usd);
    
    EODOutData(:,13:14) = [];
    
    try
        EODOutData = sortrows(EODOutData,[4,3,2,1]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    
    %%
    % FIX: Add Trade Date
    % To add the trade date for contract numbers; if contract contains more
    % than a single trade date as in structure, we need to take the
    % min(date) as per Sridhar's request
    DB_Date_Format = 'yyyy-mm-dd';
    ContractNoInfo = [SettleData.contract_number,SettleData.group_type,SettleData.transaction_date,num2cell(datenum(SettleData.transaction_date,DB_Date_Format))];
    PosInputContractNo = 1;    PosInputTradeType = 2;    PosInputTradeDate = 3; PosInputTradeDate_Num = 4;
    ContractNoInfo_Loop = uniqueRowsCA(ContractNoInfo(:,1:2));
    NumContracts = size(ContractNoInfo_Loop,1);
    PosEODReportContractNo = 4;   PosEODReportTradeType = 5;
    TradeDate = cell(size(EODOutData,1),1);
    for iC = 1:NumContracts
        Current_ContractNum = ContractNoInfo_Loop{iC,PosInputContractNo};
        Current_TradeType = ContractNoInfo_Loop{iC,PosInputTradeType};
        
        Idx_CNo =  strcmpi(ContractNoInfo(:,PosInputContractNo),Current_ContractNum) & strcmpi(ContractNoInfo(:,PosInputTradeType),Current_TradeType) ;
        IdxCNoMatch = strcmpi(EODOutData(:,PosEODReportContractNo),Current_ContractNum) & strcmpi(EODOutData(:,PosEODReportTradeType),Current_TradeType);
        
        if(~isempty(find(Idx_CNo)))
            if(~isempty(find(IdxCNoMatch)))
                Min_TradeDate_Num =  cellstr(datestr(min(cell2mat(ContractNoInfo(Idx_CNo,PosInputTradeDate_Num))),DB_Date_Format));
                TradeDate(IdxCNoMatch) = Min_TradeDate_Num;
            end
        else
            aa=1;
        end
    end
    EODOutData = [TradeDate,EODOutData];
    % End of FIX: Add Trade Date
    
    EODFilename = getXLSFilename('EODReport');
    EODHeader = {'TradeDate','Instrument','Subportfolio','Counterparty','ContractNo','TradeType','Currency',...
        'Realised NC', 'Realised USD','Unrealised NC','Unrealised USD','Total NC','Total USD'};
    xlswrite(EODFilename,[EODHeader;EODOutData],'Sheet1');
    
    %%
    % SIPH Report
    NumRows = length(SwapData.subportfolio);
    
    Total_mtm_usd = zeros(NumRows,1);
    Total_mtm_nc = zeros(NumRows,1);
    
    DB_realised_cumulative_mtm_nc  = zeros(NumRows,1);
    DB_unrealised_cumulative_mtm_nc = zeros(NumRows,1);
    DB_realised_cumulative_mtm_usd = zeros(NumRows,1);
    DB_unrealised_cumulative_mtm_usd = zeros(NumRows,1);
    
    IdxDBDead = strcmpi('dead',SwapData.deal_status);
    IdxDBLive = strcmpi('live',SwapData.deal_status);
    
    DB_realised_cumulative_mtm_nc(IdxDBDead)   = SwapData.cumulative_mtm_nc(IdxDBDead);
    DB_unrealised_cumulative_mtm_nc(IdxDBLive) = SwapData.cumulative_mtm_nc(IdxDBLive);
    DB_realised_cumulative_mtm_usd(IdxDBDead) = SwapData.cumulative_mtm_usd(IdxDBDead);
    DB_unrealised_cumulative_mtm_usd(IdxDBLive) = SwapData.cumulative_mtm_usd(IdxDBLive);
    
    EODHeader = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType','Currency',...
        'RealisedNC', 'RealisedUSD','UnrealisedNC','UnrealisedUSD','TotalNC','TotalUSD'};
    OutData = [upper(SwapData.instrument),upper(SwapData.subportfolio),upper(SwapData.counterparty_parent),...
        upper(SwapData.contract_number),SwapData.group_type,upper(SwapData.currency),...
        num2cell(DB_realised_cumulative_mtm_nc),num2cell(DB_realised_cumulative_mtm_usd),...
        num2cell(DB_unrealised_cumulative_mtm_nc),num2cell(DB_unrealised_cumulative_mtm_usd),...
        num2cell(Total_mtm_nc),num2cell(Total_mtm_usd)];
    UniqueFields = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType'};
    SumFields = { 'RealisedNC', 'RealisedUSD','UnrealisedNC','UnrealisedUSD','TotalNC','TotalUSD'};
    WeightedAverageFields = [];
    [~,EODOutData] = consolidatedata(EODHeader, OutData,UniqueFields,SumFields,EODHeader,WeightedAverageFields);
    
    Total_mtm_nc = cell2mat(EODOutData(:,7)) +  cell2mat(EODOutData(:,9));
    Total_mtm_usd = cell2mat(EODOutData(:,8)) +  cell2mat(EODOutData(:,10));
    
    EODOutData(:,11) = num2cell(Total_mtm_nc);
    EODOutData(:,12) = num2cell(Total_mtm_usd);
    
    try
        EODOutData = sortrows(EODOutData,[4,3,2,1]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    
    EODHeader = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType','Currency',...
        'Realised NC', 'Realised USD','Unrealised NC','Unrealised USD','Total NC','Total USD'};
    xlswrite(EODFilename,[EODHeader;EODOutData],'Sheet2');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    EODFilename = '';
end