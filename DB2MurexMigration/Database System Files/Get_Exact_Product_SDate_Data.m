function [Overall_Data] = Get_Exact_Product_SDate_Data(OutPriceChangeData,ProductCode_SDate,PriceVol_Identifier)

OutPriceChangeData = cell2dataset(OutPriceChangeData);

if(PriceVol_Identifier)
    [AssetClass,Remain_ProductCode] = strtok(OutPriceChangeData.underlying_id,' ');
    [ProductCode,Remain_ProductCode] = strtok(Remain_ProductCode);
    ProductCode = strtrim(ProductCode);
    
%     ProductCode = strtrim(cellfun(@(x) x(1:2), strtrim(Remain_ProductCode), 'UniformOutput', false)); %% OutPriceChangeData.underlying_id
    OutPriceChangeData.product_code = ProductCode;
else
    [AssetClass,Remain_ProductCode] = strtok(OutPriceChangeData.vol_id,' ');
    [ProductCode,Remain_ProductCode] = strtok(Remain_ProductCode);
    ProductCode = strtrim(ProductCode);
    
%     ProductCode = strtrim(cellfun(@(x) x(1:2), OutPriceChangeData.cont_month, 'UniformOutput', false));
    OutPriceChangeData.product_code = ProductCode;
end

Overall_Data = [];
for i = 1 : size(ProductCode_SDate,1)
    Current_ProductCode = ProductCode_SDate(i,1);
    Current_SDate = ProductCode_SDate(i,2);
    
    Index_Matched =  strcmpi(Current_ProductCode,OutPriceChangeData.product_code) &  strcmpi(Current_SDate,OutPriceChangeData.settlement_date);
    
    Temp_Data = OutPriceChangeData(Index_Matched,:) ;
    Overall_Data = [Overall_Data  ; Temp_Data];
    
end