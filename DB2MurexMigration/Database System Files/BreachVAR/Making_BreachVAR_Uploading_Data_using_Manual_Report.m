function Making_BreachVAR_Uploading_Data_using_Manual_Report()

try
    [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
    InXLSFilename = fullfile(pathname,filename);
    
    [type, sheets] = xlsfinfo(InXLSFilename);
       
    [sheets, theChosenIDX] = uicellect(sheets,'Multi',0);
    if(ischar(sheets))
        sheets = cellstr(sheets);
    end
        
%     OutXLSName = getXLSFilename('Breach_Uploading_Format');
    
    Overall_Data = [];
    for i = 1 : length(sheets)
        
        Current_SheetName = sheets(i);
        
        OutXLSName = getXLSFilename([char(Current_SheetName),'_Breach_Uploading_Format']);
        
        [~,~,RawData] = xlsread(InXLSFilename,char(Current_SheetName));
        
        Pdt_Code_Index = cellStrfind_exact(RawData(2,:),{'Pdt Code'});
        Default_PdtCodeRow = 2;
        for ii = 1 : length(Pdt_Code_Index)
            
            Current_PdtCode_Index = Pdt_Code_Index(ii);
            
            [RowNum,ColNum] = ind2sub(size(RawData),Current_PdtCode_Index);
            
            Row_Label_Index =  cellStrfind_exact(RawData(:,Current_PdtCode_Index),{'Row Labels'});
            
            Current_ProdCode = RawData(Default_PdtCodeRow,Current_PdtCode_Index+1);
            Current_Portfolio = RawData(Default_PdtCodeRow+1,Current_PdtCode_Index+1);
            
            Temp_Data = RawData(Row_Label_Index : end ,Current_PdtCode_Index :Current_PdtCode_Index+5);
            
            %% Removing  Unwanted Rows
            NaN_Row = cellfun(@(V) any(isnan(V(:))), Temp_Data(:,1));
            Empty_Row =  cellfun(@(V) any(isempty(V(:))), Temp_Data(:,end));
            Overall_Removing_Rows = NaN_Row | Empty_Row;
            Temp_Data(Overall_Removing_Rows,:) = [];
            
            %% Making Uploading Data
            Temp_ProdctPortfolio_Data = cell(size(Temp_Data,1)-1 ,2); % Without Header
            Temp_ProdctPortfolio_Data(:,1) = Current_ProdCode;
            Temp_ProdctPortfolio_Data(:,2) = Current_Portfolio;
            
            COB_Date_Data = Temp_Data(2:end,3);
            BookValue_Data = Temp_Data(2:end,2);
            DailyPnL_DrawDown_Data = Temp_Data(2:end,4:end);
            
            Temp_Overall_Data = [COB_Date_Data,Temp_ProdctPortfolio_Data,BookValue_Data,DailyPnL_DrawDown_Data];
            Overall_Data = [Overall_Data ; Temp_Overall_Data];
            
        end
        
        Overall_Header = {'COB_Date','Product_Code','Portfolio','Book_Value','Daily_PnL','Cummulative_PnL','DrawDown'};
        xlswrite(OutXLSName,[Overall_Header ; Overall_Data],char(Current_SheetName));
        
    end
    
    aa = 1;
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end
