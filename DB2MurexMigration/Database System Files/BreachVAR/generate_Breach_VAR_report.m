function [OutErrorMsg,OutFilename,OutVARData] = generate_Breach_VAR_report(InBUName)

try
    OutErrorMsg = {'No errors'};
    OutFilename = '';
    OutVARData = '';
    
    %%
    ObjDB = connect_to_database;
    
    Value_Date = fetch(ObjDB,'select value_date from valuation_date_table');
    TableName = strcat('var_dynamic_report_',char(InBUName));
    
    SqlQuery = ['select * from ',TableName,' where value_date =''',char(Value_Date),''' '];
    
    [ColNames , DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(strcmpi(DBData,'No Data')) %% || size(DBData,2)<=1)
        OutErrorMsg = {['No Data Found in ',TableName,' for Corresponding Value Date']};
        return;
    end
    
    DynamicVAR_Tbl = cell2dataset([ColNames ; DBData]);
    
    Unique_Portfolio = unique(DynamicVAR_Tbl.subportfolio);
    
    %%
    
    Total_PnL_Col = cellStrfind_exact(ColNames,{'total_pnl'});
    Without_TotalPnL_DynamicVAR_Tbl = DynamicVAR_Tbl;
    Without_TotalPnL_DynamicVAR_Tbl(:,Total_PnL_Col) = [];
    
    OutFilename = getXLSFilename('Breach_VaR_Report');
    
    xlswrite(OutFilename,dataset2cell(Without_TotalPnL_DynamicVAR_Tbl),'Original Data');
    
    %%
    Overall_RowDAta = [];
    RowData = [];
    
    for i = 1 : size(Unique_Portfolio)
        
        Current_Portfolio = Unique_Portfolio(i);
        
        Matched_Portfolio_InDex = cellStrfind_exact(DynamicVAR_Tbl.subportfolio,Current_Portfolio);
        if(~isempty(Matched_Portfolio_InDex))
            Temp_Matched_Data = DynamicVAR_Tbl(Matched_Portfolio_InDex,:);
            
            Unique_Product = unique(Temp_Matched_Data.product_code);
            for ii = 1 : size(Unique_Product,1)
                Current_Product =  Unique_Product(ii);
                
                Matched_Product_Index = cellStrfind_exact(Temp_Matched_Data.product_code,Current_Product);
                if(~isempty(Matched_Product_Index))
                    
                    Average_VAR = num2cell(mean(Temp_Matched_Data.sum_total_pnl(Matched_Product_Index))) ;
                    
                    RowData = [RowData ; [Current_Portfolio , Current_Product , Average_VAR] ];
                    
                    Average_VAR = 0;
                    
                end
                
            end
            
            Overall_RowDAta = [Overall_RowDAta ; RowData ];
            RowData = [];
        end
    end
    
    %%
    ValueDate_Temp_Cell = cell(size(Overall_RowDAta,1),1);
    ValueDate_Temp_Cell(:,:) = Value_Date ;
    
    OutVARData = [ValueDate_Temp_Cell , Overall_RowDAta ];
    
    %% Excel Write
    
    Header = {'Value_Date','Portfolio','Product','Avg VAR'};
    
    xlswrite(OutFilename,[Header ; OutVARData]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end



