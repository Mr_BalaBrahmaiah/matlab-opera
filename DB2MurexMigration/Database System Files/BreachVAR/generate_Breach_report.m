function [OutErrorMsg,OutFilename] = generate_Breach_report(InBUName)

%InBUName = 'agf';

try
    
    OutErrorMsg = {'No errors'};
    OutFilename = '';
    
    ObjDB = connect_to_database;
    
    DB_Date_Format = 'yyyy-mm-dd';
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    Previous_SettleDate = fetch(ObjDB,['select settlement_date from vdate_sdate_table where value_date = ''',char(Settle_Date),''' ' ]);
    Previous_SettleDate_Num = datenum(Previous_SettleDate,DB_Date_Format);
    
    Value_Date = fetch(ObjDB,'select value_date from valuation_date_table');
    
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    FileName = strcat(upper(char(InBUName)),'_Breach_Report');
    OutFilename = getXLSFilename(FileName);
    
    ViewName = strcat('ff_portfolio_report_table_',char(InBUName));
    
    [ColNames_All,DBData_All] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    Breach_VaR_TblName = strcat('breach_var_report_table_',char(InBUName)) ;
    [Header,RowData] = Fetch_DB_Data(ObjDB,[],Breach_VaR_TblName);
    Breach_VaR_Tbl_Data = cell2dataset([Header;RowData]) ;
    
    VaR_TblName = strcat('var_report_table_',char(InBUName)) ;
    SqlQuery_1 = ['select * from ',VaR_TblName,' where value_date=''',char(Value_Date),''' '];
    [Header,RowData] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    VaR_Number_Tbl_Data = cell2dataset([Header;RowData]) ;
    
    
    %%
    if ~strcmpi(DBData_All,'No Data')
        
        %% Fetch FF_Portfolio Table for Breach VAR Report
        
        ColNames = {'cob_date','product_code','portfolio','book_value'};
        User_Want_Col = cellStrfind_exact(ColNames_All,ColNames);
        
        DBData = DBData_All(:,User_Want_Col);
        
        if(isempty(DBData))
            OutErrorMsg = {['Not Getting Data from ',char(ViewName)]};
            return;
        end
        if(strcmpi(DBData,'No Data'))
            OutErrorMsg = {['Not Getting Data from ',char(ViewName)]};
            return;
        end
        
        %% Consolidate Level
        UniqueFields = {'cob_date','portfolio','product_code'};
        SumFields = {'book_value'};
        OutputFields = {'cob_date','product_code','portfolio','book_value'};
        WeightedAverageFields = [];
        [Consolidate_Header,Consolidate_DBData] = consolidatedata(ColNames, DBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        FF_PortfolioReport_ProductLevel_Data = cell2dataset([Consolidate_Header ; Consolidate_DBData]);
        
        UniqueFields = {'cob_date','portfolio'};
        SumFields = {'book_value'};
        OutputFields = {'cob_date','product_code','portfolio','book_value'};
        WeightedAverageFields = [];
        [Consolidate_Header,Consolidate_DBData] = consolidatedata(ColNames, DBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        FF_PortfolioReport_PortfolioLevel_Data = cell2dataset([Consolidate_Header ; Consolidate_DBData]);
        
        %% FF portfolio Product Level  Data (Product Level Breach VaR)
        
        Unique_Portfolio = unique(FF_PortfolioReport_ProductLevel_Data.portfolio);
        
        User_Want_Header = {'cob_date','product_code','portfolio','book_value','Daily_PnL','Cumm_PnL','DrawDown'}; %% Breach VaR Format
        
        Overall_ErrorMsg = [];
        
        for i = 1 : length(Unique_Portfolio)
            Current_Portfolio =  Unique_Portfolio(i);
            
            Current_Portfolio_Data = FF_PortfolioReport_ProductLevel_Data(strcmpi(FF_PortfolioReport_ProductLevel_Data.portfolio,Current_Portfolio),:);
            
            if(isempty(Current_Portfolio_Data))
                Overall_ErrorMsg = [Overall_ErrorMsg ; {['Not Getting Portfolio Data for ',char(Current_Portfolio)]}];
                continue;
            end
            
            Unique_ProductCode = unique(Current_Portfolio_Data.product_code);
            
            Overall_Current_ProductCode_Data = [];
            for ii = 1 : length(Unique_ProductCode)
                Current_ProductCode = Unique_ProductCode(ii);
                Current_ProductCode_Data = Current_Portfolio_Data(strcmpi(Current_Portfolio_Data.product_code,Current_ProductCode),:) ;
                
                if(isempty(Current_ProductCode_Data))
                    Overall_ErrorMsg = [Overall_ErrorMsg ; {['Not Getting ProductCode Data for ',char(Current_ProductCode)]}];
                    continue;
                end
                
                SettleDate_Index = find(strcmpi(Current_ProductCode_Data.cob_date,Settle_Date));
                Previous_SettleDate_Index = find(strcmpi(Current_ProductCode_Data.cob_date,Previous_SettleDate));
                
                if(isempty(SettleDate_Index))
                    Error_Msg_1 = {['Not Getting Portfolio Data for ',char(Current_ProductCode),' ',char(Current_Portfolio),' and ',char(Settle_Date)] };
                    Overall_ErrorMsg = [Overall_ErrorMsg ; Error_Msg_1];
                    continue;
                end
                
                if(isempty(Previous_SettleDate_Index))
                    Error_Msg_1 = {['Not Getting Portfolio Data for ',char(Current_ProductCode),' ',char(Current_Portfolio),' and ',char(Previous_SettleDate)] };
                    Overall_ErrorMsg = [Overall_ErrorMsg ; Error_Msg_1];
                    continue;
                end
                
                Matched_BreachVAR_Index = strcmpi(Breach_VaR_Tbl_Data.portfolio,Current_Portfolio) &  strcmpi(Breach_VaR_Tbl_Data.product_code,Current_ProductCode)...
                    & strcmpi(Breach_VaR_Tbl_Data.cob_date,Previous_SettleDate);
                
                if(size(Current_ProductCode_Data,1)==1)
                    Current_ProductCode_Data.Daily_PnL(1) = Current_ProductCode_Data.book_value(1);
                    Current_ProductCode_Data.Cumm_PnL(1) = Current_ProductCode_Data.book_value(1);
                    Current_ProductCode_Data.DrawDown(1) = Current_ProductCode_Data.book_value(1) - max(Current_ProductCode_Data.book_value(1));
                else
                    Current_ProductCode_Data.Daily_PnL(SettleDate_Index,1) = Current_ProductCode_Data.book_value(SettleDate_Index) - Current_ProductCode_Data.book_value(Previous_SettleDate_Index);
                    Current_ProductCode_Data.Cumm_PnL(SettleDate_Index,1) = nansum(Current_ProductCode_Data.Daily_PnL) + nansum(Breach_VaR_Tbl_Data.cumulative_pnl(Matched_BreachVAR_Index));
                    Current_ProductCode_Data.DrawDown(SettleDate_Index,1) = Current_ProductCode_Data.book_value(SettleDate_Index) - max(Current_ProductCode_Data.book_value);
                end
                
                CurrentData = dataset2cell(Current_ProductCode_Data);
                Header = CurrentData(1,:);
                CurrentData(1,:) = [];
                Overall_Current_ProductCode_Data = [Overall_Current_ProductCode_Data ; CurrentData];
            end
            
            %% Uploading into breach_var_report_table
            
            if(isempty(Overall_Current_ProductCode_Data))
                continue;
            end
            
            %xlswrite(OutFilename,[Header ; Overall_Current_ProductCode_Data],char(Current_Portfolio));
            
            Output_Store_TblName = strcat('breach_var_report_table_',char(InBUName));
            
            User_Want_Col = cellStrfind_exact(Header,User_Want_Header);
            User_Want_Data = Overall_Current_ProductCode_Data(:,User_Want_Col);
            
            COB_Date_col = cellStrfind_exact(User_Want_Header,{'cob_date'});
            Output_Uploading_Data = User_Want_Data(strcmpi(User_Want_Data(:,COB_Date_col),Settle_Date),:);
            
            set(ObjDB,'AutoCommit','off');
            
            SqlQuery = ['delete from ',Output_Store_TblName,' where cob_date = ''',char(Settle_Date),''' and portfolio = ''',char(Current_Portfolio),''' '];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            upload_in_database(Output_Store_TblName, Output_Uploading_Data);
            
            cprintf('key','%d : %s : %d finished\n',i,char(Current_Portfolio),length(Unique_Portfolio));
            
        end
        
        %% Product Level VaR
        
        % Fetch Data
        Output_Store_TblName = strcat('breach_var_report_table_',char(InBUName));
        SqlQuery_2 = ['select * from ',char(Output_Store_TblName),' where cob_date = ''',char(Settle_Date),''' '];
        [ColNames , DBData ] = Fetch_DB_Data(ObjDB,SqlQuery_2);
        
        if(isempty(DBData))
            OutErrorMsg = {['Not Getting Data from ',char(Output_Store_TblName)]};
            return;
        end
        if(strcmpi(DBData,'No Data'))
            OutErrorMsg = {['Not Getting Data from ',char(Output_Store_TblName)]};
            return;
        end
        
        Overall_BreahVAR_Data = cell2dataset([ColNames ; DBData]);
        
        Unique_Portfolio = unique(Overall_BreahVAR_Data.portfolio) ;
        
        Overall_Output_Data = [];
        
        for i = 1 : length(Unique_Portfolio)
            Current_Portfolio =  Unique_Portfolio(i);
            
            Current_Portfolio_Data = Overall_BreahVAR_Data(strcmpi(Overall_BreahVAR_Data.portfolio,Current_Portfolio),:);
            
            if(isempty(Current_Portfolio_Data))
                Overall_ErrorMsg = [Overall_ErrorMsg ; {['Not Getting Portfolio Data for ',char(Current_Portfolio),' in ',char(Output_Store_TblName)]}];
                continue;
            end
            
            Unique_ProductCode = unique(Current_Portfolio_Data.product_code);
            
            for ii = 1 : length(Unique_ProductCode)
                Current_ProductCode = Unique_ProductCode(ii);
                Current_ProductCode_Data = Current_Portfolio_Data(strcmpi(Current_Portfolio_Data.product_code,Current_ProductCode),:) ;
                
                Current_AggregationLevel = strcat(Current_Portfolio,{':CM '},Current_ProductCode);
                Matched_Index = find(strcmpi(strtrim(VaR_Number_Tbl_Data.aggregation_level),Current_AggregationLevel));
                if(~isempty(Matched_Index))
                    Current_Portfolio_Data.VaRNumber(ii) = VaR_Number_Tbl_Data.prctileloss95(Matched_Index(1));
                else
                    Current_Portfolio_Data.VaRNumber(ii) = 0;
                end
                
                
            end
            
            
            Output_Sort_Data =  dataset2cell(sortrows(Current_Portfolio_Data,2,{'ascend'}));
            %xlswrite(OutFilename,Output_Sort_Data,char(Current_Portfolio)); % For Every particular portfolio
            
            Header = Output_Sort_Data(1,:);
            Output_Sort_Data(1,:) = [];
            Overall_Output_Data = [Overall_Output_Data ; Output_Sort_Data];
        end
        
        xlswrite(OutFilename,[Header ; Overall_Output_Data],'Overall_Output');
        
        %% Portfolio Level VAR
        
        %         Unique_Portfolio = unique(FF_PortfolioReport_PortfolioLevel_Data.portfolio);
        %         Unique_Portfolio = {'ALPHA-FF1-OIL','ALPHA-FF2-OIL'}; %% Need to Change
        %
        %         User_Want_Header = {'cob_date','portfolio','book_value','Daily_PnL','Cumm_PnL','DrawDown'}; %% Breach VaR Format
        %
        %
        %         for i = 1 : length(Unique_Portfolio)
        %             Current_Portfolio =  Unique_Portfolio(i);
        %
        %             Current_Portfolio_Data = FF_PortfolioReport_ProductLevel_Data(strcmpi(FF_PortfolioReport_ProductLevel_Data.portfolio,Current_Portfolio),:);
        %
        %             if(isempty(Current_Portfolio_Data))
        %                 Overall_ErrorMsg = [Overall_ErrorMsg ; {['Not Getting Portfolio Data for ',char(Current_Portfolio)]}];
        %                 continue;
        %             end
        %
        %
        %         end
        
        %%
        
        try
            xls_delete_sheets(fullfile(pwd,OutFilename)) ;
        catch
            
        end
        
    else
        OutErrorMsg = {'No data found'};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end
