function [OutErrorMsg] = upload_broker_module(InXLSFilename,InBUName,User_Passed_Counterparty,Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: Upload Broker Deal
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 06 Dec 2018 / 17:30:00

%[OutErrorMsg] = upload_broker_module(InXLSFilename,'QF1','NEWEDGE',Settele_Date)
%InXLSFilename = 'D:\Bala\Broker_Module\Broker_Deal\04-Dec-2018\Newedge QF1 & QF2.csv'
try 
    OutErrorMsg = {'No errors'};

    %%% check the all input data is given or not
    %%check File path
    if isempty(InXLSFilename)  
        OutErrorMsg = {'InXLSFilename path was not given by user!'};
        return;
    else
        [~,~,RawData] = xlsread(InXLSFilename);  % input file
    end
    % check Business unit
    if isempty(InBUName)  
        OutErrorMsg = {'InBUName was not given by user!'};
        return;
    else
        if iscellstr(InBUName)
            InBUName = char(InBUName);
        end
    end
    % check Counterparty
    if isempty(User_Passed_Counterparty)  
        OutErrorMsg = {'Counterparty was not given by user!'};
        return;
    end
    % check Settlment Date
    if isempty(Settle_Date)  
        OutErrorMsg = {'Settele_Date was not given by user!'};
        return;
    else
        Settle_Date = datestr(Settle_Date,'yyyy-mm-dd');
    end

    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from Broker Deal Mapping Table
    Deal_Mapping_Table = 'broker_deal_mapping_table';
    SqlQuery_Deal_Mapping_Table = ['select * from ',char(Deal_Mapping_Table),' where businessunit = ''',char(InBUName),''' and counterparty_parent = ''',char(User_Passed_Counterparty),''' '] ;
    [~,Deal_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Deal_Mapping_Table);

    if strcmpi(Deal_Mapping_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Deal_Mapping_Table)]} ;
        return;
    end

    %%% select data for Table_Name and Office_Column and Account_Column
    BDeal_Table_Name = Deal_Mapping_Table_Data(:,3);
    BDeal_Office_Column = Deal_Mapping_Table_Data(:,4);
    BDeal_Account_Column = Deal_Mapping_Table_Data(:,5);
    BDeal_Date_Column = Deal_Mapping_Table_Data(:,6);

    %%Delete data from Database (UAT)
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',char(BDeal_Table_Name),' where settle_date = ''',char(Settle_Date),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
        OutErrorMsg = {curs.Message};
    end
    set(ObjDB,'AutoCommit','on');

    %%% Header and Data for selected input file
    RawData_Header_Names = RawData(1,:);
    RawData_Val = RawData(2:end,:);

%     Idx = isnan(((RawData_Val(:,10))))%,char(cell2mat({NaN})));
%     Replace NaN values with blanks 
    RawData_Val(cellfun(@(cell) any(isnan(cell(:))),RawData_Val)) = {''};
    
    if isempty(RawData_Val)        %%% Checking condition? data is found or not
        OutErrorMsg = {'No data found in selected input file!'} ;
        return;
    end

    %%% find Office_Col and Account_Col
    Office_Col = find(cellfun(@(V) strcmpi(BDeal_Office_Column,V), RawData_Header_Names)) ;
    Account_Col = find(cellfun(@(V) strcmpi(BDeal_Account_Column,V), RawData_Header_Names)) ;
    Date_Col = find(cellfun(@(V) strcmpi(BDeal_Date_Column,V), RawData_Header_Names)) ;
    
    if isempty(Office_Col)
        OutErrorMsg = {'Office_Column not found in selected input file!'} ;
        return;
    else    
        office = RawData_Val(:,Office_Col) ; 
    end

    if isempty(Account_Col)
        OutErrorMsg = {'Account_Column not found in selected input file!'} ;
        return;
    else    
        account = RawData_Val(:,Account_Col) ;
    end

    if isempty(Date_Col)
        OutErrorMsg = {[char(RawData_Header_Names(Date_Col)),' date column is not found in selected input file!']} ;
        return;
    else    
        date_data = cell2mat(RawData_Val(:,Date_Col));

        Check_Settle_Date = strrep({Settle_Date},'-','');
        Check_Settle_Date = str2num(char(Check_Settle_Date));
        
        Index=ismember(date_data,Check_Settle_Date);
        if ~any(Index)
            OutErrorMsg = {['For ',char(Settle_Date),' broker deals are not found in file. Kindly select the appropriate file!']} ;
            return;
        end
        
    end
    
    %%% Concatenate the office and account
    off_acco_mapp = strcat(num2str(cell2mat(office)),num2str(cell2mat(account)));

    %%% Fetch data from Broker Deal Account-cp_bu-Mapping
    Deal_account_cp_bu_Mapping_Table = 'broker_deal_account_cp_bu_mapping';
    SqlQuery_Deal_cp_bu_Mapping_Table = ['select * from ',char(Deal_account_cp_bu_Mapping_Table),' where business_unit = ''',char(InBUName),''' and counterparty_parent = ''',char(User_Passed_Counterparty),''' '];
    [~,Deal_cp_bu_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Deal_cp_bu_Mapping_Table);

    if strcmpi(Deal_cp_bu_Mapping_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Deal_account_cp_bu_Mapping_Table)]} ;
        return;
    end
    %%% required fields
    account_number = Deal_cp_bu_Mapping_Table_Data(:,1);
    sub_counterparty = Deal_cp_bu_Mapping_Table_Data(:,3);

    %%% Comparing account_number and fetch data
    [RA,~] = size(account_number);
    Broker_Deal_Data = []; counterparty =[];
    for jj = 1:1:RA
        single_account_number = account_number(jj);
        single_sub_counterparty = sub_counterparty(jj);
        Unique_Broker_Deal_Data=RawData_Val(strcmpi(single_account_number,off_acco_mapp),:);
        if isempty(Unique_Broker_Deal_Data) 
            %OutErrorMsg = {'account_number was not matching with given data'} ;
            continue;
        end
        [rr,~]=size(Unique_Broker_Deal_Data);
        Final_sub_counterparty = repmat(single_sub_counterparty,rr,1);
        counterparty = vertcat(counterparty,Final_sub_counterparty);
        Broker_Deal_Data = vertcat(Broker_Deal_Data,Unique_Broker_Deal_Data);
    end

    if isempty(Broker_Deal_Data) 
        %OutErrorMsg = {['No data found for account_number (',char(account_number),') mapping process']} ;
        OutErrorMsg = {['No data found for account_number mapping process']} ;
        return;
    end

    %%% get last trade ID from broker_balance_report_table_(InBUName)
    [~,DBTradeId]= getLastTradeId_Broker_Deal(BDeal_Table_Name);

    %%% generate the fist col like exp_id
    [r,~] = size(Broker_Deal_Data);
    for i=1:1:r
        NewTradeId = DBTradeId;
        bd_id(i,1) = cellstr(['BD-',num2str(NewTradeId)]);
        DBTradeId = DBTradeId+1;
    end

    % additional user want data (deal_id, date, Counterparty and desk)
    [row,~] = size(Broker_Deal_Data);
    broker_deal_id = bd_id;
    settle_date = repmat({Settle_Date},row,1);
    Counterparty = counterparty;
    desk = repmat({InBUName},row,1);

    Final_Broker_Deal_Data = [broker_deal_id settle_date Counterparty desk Broker_Deal_Data];
     
    %%% get the table col names from Database
    Broker_Deal_TableColumns = gettablecolumnnames(ObjDB, BDeal_Table_Name);
    Broker_Deal_Header_Names = [{'broker_deal_id'} {'settle_date'} {'Counterparty'} {'desk'} RawData_Header_Names];

    %%% Final user want data --> New Data
    [~,col] = size(Broker_Deal_TableColumns); 
    Upload_Broker_Deal_Data = [];
    for q = 1:1:col
        Index = find(strcmpi(Broker_Deal_TableColumns(q),Broker_Deal_Header_Names));
        %Index = cellStrfind_exact(Broker_Deal_TableColumns,Broker_Deal_Header_Names)
        if any(Index)
            Final_BDeal_Data = Final_Broker_Deal_Data(:,Index);
            Upload_Broker_Deal_Data = [Upload_Broker_Deal_Data Final_BDeal_Data];
        else
            wrngdlg({[char(Broker_Deal_TableColumns(q)),' column is not found in file']});
        end
    end
    
    %%% Upload data into database
%     fastinsert(ObjDB,char(BDeal_Table_Name),Broker_Deal_TableColumns,Upload_Broker_Deal_Data)
%     upload_in_database(char(BDeal_Table_Name),Upload_Broker_Deal_Data,ObjDB);
    
    datainsert(ObjDB,char(BDeal_Table_Name),Broker_Deal_TableColumns,Upload_Broker_Deal_Data)

    disp(['Upload data successfully for ',char(BDeal_Table_Name),' bable for ',Settle_Date]);

catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end

end