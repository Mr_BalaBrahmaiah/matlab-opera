function [OutErrorMsg,OutXLSName] = download_broker_module(InBUName,User_Passed_Counterparty,Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: Download Broker Deal
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 06 Dec 2018 / 17:30:00

%[OutErrorMsg,OutXLSName] = download_broker_module('QF1','NEWEDGE',Settele_Date)

try 
    OutErrorMsg = {'No Errors'};
    OutXLSName = {''};
    %%% check the all input data is given or not
    % check Business unit
    if isempty(InBUName)  
        OutErrorMsg = {'InBUName was not given by user!'};
        return;
    else
        if iscellstr(InBUName)
            InBUName = char(InBUName);
        end
    end
    % check Counterparty
    if isempty(User_Passed_Counterparty)  
        OutErrorMsg = {'Counterparty was not given by user!'};
        return;
    end
    % check Settlment Date
    if isempty(Settle_Date)  
        OutErrorMsg = {'Settele_Date was not given by user!'};
        return;
    else
        Settle_Date = datestr(Settle_Date,'yyyy-mm-dd');
    end

    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from Broker Deal Mapping Table
    Deal_Mapping_Table = 'broker_deal_mapping_table';
    SqlQuery_Deal_Mapping_Table = ['select * from ',char(Deal_Mapping_Table),' where businessunit = ''',char(InBUName),''' and counterparty_parent like ''',char(User_Passed_Counterparty),''' '] ;
    [~,Deal_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Deal_Mapping_Table);

    if strcmpi(Deal_Mapping_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Deal_Mapping_Table)]};
        return;
    end
    
    if isequal(Deal_Mapping_Table_Data,0) 
        OutErrorMsg = {['Data not Available in ',char(Deal_Mapping_Table)]};
        return;
    end

    %%% select data for Table_Name and Office_Column and Account_Column
    BDeal_Table_Name = Deal_Mapping_Table_Data(:,3);
    
    % outfile as Table_Name
    OutXLSName = getXLSFilename(char(BDeal_Table_Name));  % output file name
        
    %%% Fetch data from Broker Deal Table
    SqlQuery_Broker_Deal_Table = ['select * from ',char(BDeal_Table_Name),' where desk = ''',char(InBUName),''' and counterparty like ''',char(User_Passed_Counterparty),'%'' and settle_date = ''',char(Settle_Date),''' '] ;
    [Broker_Deal_Table_Cols,Broker_Deal_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Deal_Table);
 
    if strcmpi(Broker_Deal_Table_Data,'No Data') 
        OutErrorMsg = {['For ',char(Settle_Date),' Data not Available in ',char(BDeal_Table_Name)]} ;
        return;
    end
    
    Broker_Deal_Data = [Broker_Deal_Table_Cols ; Broker_Deal_Table_Data];
    %%% write data into file
    xlswrite(OutXLSName,Broker_Deal_Data,'Broker_Deal');
    
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end

end
