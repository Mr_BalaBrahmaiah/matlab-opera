function [OutErrorMsg,LastTradeId]= getLastTradeId_Broker_Deal(Table_Name)

    OutErrorMsg = {'No errors'};
    LastTradeId = {''};

    try
%         if iscellstr(InBUName)
%             InBUName = char(InBUName);
%         end

        ObjDB = connect_to_database;

        SqlQuery = ['select mid(broker_deal_id,4) from ',char(Table_Name),' where broker_deal_id like ''','BD-','%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);

        if (isempty(DBTradeId))
            DBTradeId = 0;
        end

        TempTradeNum = max(str2num(char(DBTradeId)));

        if(isempty(TempTradeNum))
            TempTradeNum = 0;      %% Default
        end

         if ~isempty(TempTradeNum)
                LastTradeId = (TempTradeNum+1)';
         end 

    catch ME

        OutErrorMsg = cellstr(ME.message);

    end
end

