function [OutErrorMsg,OutData] = generate_tid_basis_daily_pnl_report(InBUName)

OutErrorMsg = {'No errors'};
OutData = [];

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    [ColNames,Data] = read_from_database('dirbook_tid_basis_daily_pnl_table',0,'',InBUName);
    
    if(isempty(Data))
        OutErrorMsg = {['Not Getting data from ','dirbook_tid_basis_daily_pnl_table_',char(InBUName)]};
        return;
    end
    if(strcmpi(Data,'No Data'))
        OutErrorMsg = {['Not Getting data from ','dirbook_tid_basis_daily_pnl_table_',char(InBUName)]};
        return;
    end
    
    TRData = cell2dataset([ColNames;Data]);
    
    
    % ReportName = getXLSFilename('DirBook_Report_TID');
    
    HolDates = {'2016-01-01';'2016-12-25'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    SqlQuery = 'select value_date from valuation_date_table';
    [~,Data] = read_from_database('valuation_date_table',0,SqlQuery);
    TodayDate = char(Data);
    
    VBusDays = busdays(datenum('2016-01-01'), datenum(TodayDate),'daily',HolidayVec);
    VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
    SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));
    
    TID = unique(TRData.tid);
    OutData = [];
    
    for iTID = 1:length(TID)
        IdxTID = strcmpi(TID{iTID},TRData.tid);
        
        TIDData = sortrows(TRData(IdxTID,:),1);
        
        ValueDate = TIDData.value_date;
        M2M = TIDData.mtm_usd;
        
        PnL = nan(size(VDates));
        
        for i = 1:length(VBusDays)
            if i~=1
                IdxToday = strcmpi(VDates{i},ValueDate);
                IdxYest   = strcmpi(SDates{i},ValueDate);
                
                TodaysM2M  = M2M(IdxToday);
                YestM2M    = M2M(IdxYest);
                
                if ~isempty(TodaysM2M) && ~isempty(YestM2M)
                    PnL(i) = TodaysM2M - YestM2M;
                elseif ~isempty(TodaysM2M) && isempty(YestM2M)
                    PnL(i) = TodaysM2M;
                end
            end
        end
        
        OutData = [OutData,num2cell(PnL)];
    end
    
    OutData = [VDates,OutData];
    Header = ['Date',TID'];
    OutData = [Header;OutData];
catch ME
    OutErrorMsg = cellstr(ME.message);
end