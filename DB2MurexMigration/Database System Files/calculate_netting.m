function [OutErrorMessage,NC_PnL,USD_PnL,Parfor_Errormsg] = calculate_netting(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName)

OutErrorMessage = {'No errors'};
NC_PnL = '0';
USD_PnL = '0';
NewNettingId ='';
Parfor_Errormsg = {};
try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %% validations before netting
    if ~isnumeric(BuyLotsToBeNetted)
        BuyLotsToBeNetted = cell2mat(BuyLotsToBeNetted);
    end
    if ~isnumeric(SellLotsToBeNetted)
        SellLotsToBeNetted = cell2mat(SellLotsToBeNetted);
    end
    
    TradeId = [BuyTradeId;SellTradeId];
    LotsToBeNetted = [BuyLotsToBeNetted;SellLotsToBeNetted];
    
    if ~isnumericequal(abs(sum(BuyLotsToBeNetted)),abs(sum(SellLotsToBeNetted)))
        OutErrorMessage = {'Error! BuyLots and SellLots must be equal!'};
        return;
    end
    
    ObjDB = connect_to_database;
    DealTicketTableName = ['deal_ticket_table_',InBUName];
    ClonedDealTicketTableName = ['cloned_deal_ticket_table_',InBUName];
    
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    DBSettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %% New Change for Parfor
    
    [DBName,DBUserName,DBPassword,DBServerIP] = Read_DB_ConfigFile();
    
    javaaddpath('mysql-connector-java-5.1.25-bin.jar');
    %     p=parpool('local',4);
    c =parallel.pool.Constant(@()database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP,'PortNumber',3306),@close);
    
    %%
    IdxRemove = [];
    Subportfolio = cell(size(TradeId,1),1);
    SecurityId = cell(size(TradeId,1),1);
    CounterpartyParent = cell(size(TradeId,1),1);
    ContractNo = cell(size(TradeId,1),1);
    
    parfor iT = 1:length(TradeId)
        ObjDB = c.Value;
%     for iT = 1:length(TradeId)        
        %         disp(iT);
        try
            SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iT},''''];
            MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
            
            %             disp(TradeId{iT}) ; disp(MaxVersion);
        catch
            IdxRemove = [IdxRemove , iT];
            continue;
        end
        try
            SqlQuery = ['select version_comments,subportfolio,security_id,counterparty_parent,contract_number from ',DealTicketTableName,' where trade_id = ''',TradeId{iT},''' and version_no = ',mat2str(MaxVersion)];
            DBData = fetch(ObjDB,SqlQuery);
        catch
            IdxRemove = [IdxRemove , iT];
            continue;
        end
        DBComments       = DBData{1};
        Subportfolio{iT} = DBData{2};
        SecurityId{iT}   = DBData{3};
        CounterpartyParent{iT} = DBData{4};
        ContractNo{iT} = DBData{5};
        
        if strcmpi(DBComments,'fully_netted')
            IdxRemove = [IdxRemove , iT];
        end
    end
    
    ObjDB = connect_to_database;
    
    if ~isempty(IdxRemove)
        TradeId(IdxRemove) = [];
        SecurityId(IdxRemove) = [];
        CounterpartyParent(IdxRemove) = [];
        LotsToBeNetted(IdxRemove) = [];
        Subportfolio(IdxRemove) = [];
        ContractNo(IdxRemove) = [];
    end
    
    if isempty(TradeId)
        OutErrorMessage = {'Error! No data selected for netting!'};
        return;
    end
    
    DistinctCP = unique(upper(CounterpartyParent));
    if length(DistinctCP)~= 1
        OutErrorMessage = {'Error! Counterparty names selected for netting should be the same!'};
        return;
    end
    
    DistinctPF = unique(upper(Subportfolio));
    if length(DistinctPF)~= 1
        OutErrorMessage = {'Error! Portfolios selected for netting should be the same!'};
        return;
    end
    
    ContractMonth = cell(size(SecurityId));
    DerivativeType = cell(size(SecurityId));
    ProductCode = cell(size(SecurityId));
    GroupType = cell(size(SecurityId));
    
    SqlQuery = 'select derivative_type,group_type from derivative_type_table';
    GroupTypeData = fetch(ObjDB,SqlQuery);
    
    Tablename = ['security_info_table_',InBUName];
    for iSec = 1:length(SecurityId)
        SqlQuery = ['select contract_month,derivative_type,product_code from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        SecInfoNetData = fetch(ObjDB,SqlQuery);
        ContractMonth(iSec)  = SecInfoNetData(1);
        DerivativeType(iSec) = SecInfoNetData(2);
        ProductCode(iSec)    = SecInfoNetData(3);
        IdxGroupType         = strcmpi(SecInfoNetData{2},GroupTypeData(:,1));
        GroupType(iSec)      = GroupTypeData(IdxGroupType,2);
        %         SqlQuery = ['select contract_month from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         ContractMonth(iSec) = fetch(ObjDB,SqlQuery);
        %         SqlQuery = ['select derivative_type from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         DerivativeType(iSec) = fetch(ObjDB,SqlQuery);
        %         SqlQuery = ['select product_code from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         ProductCode(iSec) = fetch(ObjDB,SqlQuery);
    end
    
    IdxFX = cellfun(@strncmpi,DerivativeType,repmat({'fx_forward'},size(DerivativeType)),repmat({length('fx_forward')},size(DerivativeType)));
    IdxEQDSWP = cellfun(@strncmpi,DerivativeType,repmat({'equity_swap'},size(DerivativeType)),repmat({length('equity_swap')},size(DerivativeType)));
    IdxIDXSWP = cellfun(@strncmpi,DerivativeType,repmat({'index_swap'},size(DerivativeType)),repmat({length('index_swap')},size(DerivativeType)));
    %%% Newly added line on 2019-12-02
    %%% FX-Option types like fx_vanilla_call and fx_vanilla_put, no need to check the contract month as its not valid 
    IdxFXOPT = cellfun(@strncmpi,DerivativeType,repmat({'fx_vanilla'},size(DerivativeType)),repmat({length('fx_vanilla')},size(DerivativeType)));

    if ~any(IdxFX) && ~any(IdxEQDSWP) && ~any(IdxIDXSWP) && ~any(IdxFXOPT) % FX or EQD_SWAP or IDX_SWAP types and FX-Options, no need to check the contract month as its not valid
        if any(cellfun(@isempty,ContractMonth))
            OutErrorMessage = {'Error! ContractMonth selected for netting cannot be empty!'};
            return;
        end
        
        DistinctCM = unique(upper(ContractMonth));
        if length(DistinctCM)~= 1
            OutErrorMessage = {'Error! ContractMonths selected for netting should be the same!'};
            return;
        end
    end
    
    DistinctOptType = unique(upper(DerivativeType));
    if length(DistinctOptType)~= 1
        OutErrorMessage = {'Error! DerivativeType selected for netting should be the same!'};
        return;
    end
    
    DistinctProdCode = unique(upper(ProductCode));
    if length(DistinctProdCode)~= 1
        OutErrorMessage = {'Error! ProductCode selected for netting should be the same!'};
        return;
    end
    
    %% perform netting
    % to get the last netting id
    SqlQuery = ['select distinct(netting_id) from ',ClonedDealTicketTableName];
    DistinctNettingId = fetch(ObjDB,SqlQuery);
    % NettingId = strrep(DistinctNettingId,'NET-','');
    [NettingId,TokContract] = strtok(DistinctNettingId,'NET-');
    MaxNettingId = max(str2num(char(NettingId)));
    
    if isempty(MaxNettingId)
        MaxNettingId = 0;
    end
    TempNettingId = MaxNettingId + 1;
    %     NewNettingId = ['NET-',mat2str(TempNettingId)]; Old
    %NewNettingId = ['NET-',mat2str(TempNettingId),'-',mat2str(randi([1 1000],1,1))]; % New
    NewNettingId = ['NET-',mat2str(TempNettingId),'-',mat2str(randi([1 10000],1,1))]; % New
    
    %%
    Parfor_Errormsg = {};
    
    parfor iTR = 1:length(TradeId)
        ObjDB = c.Value;
%     for iTR = 1:length(TradeId)
        
        SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
        MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
        SqlQuery = ['select * from deal_ticket_table where trade_id = ''',TradeId{iTR},''' and version_no = ',mat2str(MaxVersion)];
        [DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery,InBUName);
        DealTicketData = cell2dataset([DBFieldNames;DBData]);
        ContraDealTicketData = DealTicketData;
        SqlQuery = ['select * from cloned_deal_ticket_table where trade_id = ''',TradeId{iTR},''''];
        [DBFieldNames,DBData] = read_from_database('cloned_deal_ticket_table',0,SqlQuery,InBUName);
        ClonedDealTicketData = cell2dataset([DBFieldNames;DBData]);
        
        if ClonedDealTicketData.original_lots < 0
            OriginalLotsSign = -1;
        else
            OriginalLotsSign = 1;
        end
        TempOrigLots = abs(ClonedDealTicketData.original_lots);
        
        try
            NettedLots = abs(cell2mat(LotsToBeNetted(iTR)));
        catch
            NettedLots = abs(LotsToBeNetted(iTR));
        end
        if ClonedDealTicketData.active_lots < 0
            ActiveLotSign = -1;
        else
            ActiveLotSign = 1;
        end
        TempActiveLots = abs(ClonedDealTicketData.active_lots);
        TempNettedLots = ClonedDealTicketData.netted_lots;
        %         NewNettingId = ['NET-',mat2str(TempNettingId)];
        
%         if isnumericequal(TempOrigLots,NettedLots)
        if isnumericequal(TempActiveLots,NettedLots)
            NettingStatus  = 'fully_netted';
        else
            NettingStatus  = 'partially_netted';
        end
        
        if strcmpi(ClonedDealTicketData.netting_status,'partially_netted')
            ActiveLots = max(TempActiveLots,NettedLots) - min(TempActiveLots,NettedLots);
        else
%           ActiveLots = TempOrigLots - NettedLots;
            ActiveLots = TempActiveLots - NettedLots;
        end
        
        % multiply the calculated active lots value with market action sign
        % before updating Table
        ActiveLots = ActiveLots * ActiveLotSign;
        
        DealTicketData.version_no = MaxVersion+1;
        DealTicketData.version_created_by = UserName;
        DealTicketData.version_timestamp = getCurrentDateTimestamp;
        % if its partially netted, the netted lots to be counted based on the
        % previous netted lots as well(which is saved in activelots earlier)
        if strcmpi(NettingStatus,'partially_netted')
            DealTicketData.is_netting_done = 0;
            DealTicketData.lots_to_be_netted = ActiveLots;
%             DealTicketData.original_lots = ActiveLots;
            DealTicketData.version_comments = [NettingStatus,' ',mat2str(NettedLots),' lots'];
        else % if its fully_netted
            DealTicketData.is_netting_done = 1;
            DealTicketData.lots_to_be_netted = ActiveLots;
            DealTicketData.version_comments = NettingStatus;
            DealTicketData.deal_status = 'dead';
        end
        
        
        DealTicketData = dataset2cell(DealTicketData);
        fastinsert(ObjDB,DealTicketTableName,DealTicketData(1,:),DealTicketData(2:end,:));
        
        % Create an internal id(Netting_id-Trade_id) in cloned deal_ticket_table with original
        % lots = netted lots and active lots = 0;
        NetContraID = [NewNettingId,'-',TradeId{iTR}];
        ContraDealTicketData.trade_id = NetContraID;
        ContraDealTicketData.parent_trade_id = NetContraID;
        ContraDealTicketData.transaction_date = DBSettleDate;
        ContraDealTicketData.version_no = 0;
        ContraDealTicketData.version_created_by = UserName;
        ContraDealTicketData.version_timestamp = getCurrentDateTimestamp;
        ContraDealTicketData.version_comments = 'fully_netted';
        ContraDealTicketData.is_netting_done = 1;
        ContraDealTicketData.lots_to_be_netted = 0;
        
        
        if strcmpi(NettingStatus,'partially_netted')
             ContraDealTicketData.original_lots = 0;
%             ContraDealTicketData.original_lots = OriginalLotsSign * NettedLots;
            % Below line of code added on 19-FEB-2018 to fix the common issue identifed in QF5;
            % as the realised report does not show the contra netting line as dead
            ContraDealTicketData.deal_status = 'dead';
        else
            ContraDealTicketData.original_lots = ActiveLots;
            ContraDealTicketData.deal_status = 'dead';
        end
        
        ContraDealTicketData = dataset2cell(ContraDealTicketData);
        fastinsert(ObjDB,DealTicketTableName,ContraDealTicketData(1,:),ContraDealTicketData(2:end,:));
        
        % update the netting info and active_lots for the contra-trade after
        % netting
        NettedLots = NettedLots * OriginalLotsSign;
        %%
        %         update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
        %             {NettedLots,'fully_netted',NettedLots,NewNettingId,char(DBValueDate)},{[' where trade_id = ''',NetContraID,'''']});
        
        SqlQuery_Update = ['update ',ClonedDealTicketTableName,' set active_lots = ',mat2str(NettedLots),...
            ', netting_status = ''fully_netted''',...
            ', netted_lots = ',mat2str(NettedLots),...
            ', netting_id = ''',NewNettingId,'''',...
            ', netting_date = ''',char(DBValueDate),'''',...
            'where trade_id = ''',NetContraID,''''];
        
        Curs = exec(ObjDB,SqlQuery_Update);
        if ~isempty(Curs.Message)            
%             Parfor_Errormsg = [Parfor_Errormsg ; [{NetContraID} ,{ClonedDealTicketTableName},{Curs.Message}] ];
              Parfor_Errormsg = [Parfor_Errormsg ; [NetContraID,' : ',ClonedDealTicketTableName,' : ',Curs.Message] ];
        end
        
        %%
        Tablename = ['deal_active_lots_table_',InBUName];
        %         update(ObjDB,Tablename,{'active_lots'},{NettedLots},...
        %             {['where value_date = ''',char(DBValueDate),''' and trade_id = ''', NetContraID,'''']});
        
        SqlQuery_Update = ['update ',Tablename,' set active_lots = ',mat2str(NettedLots),' where value_date = ''',char(DBValueDate),''' and trade_id = ''', NetContraID,''''];
        
        Curs = exec(ObjDB,SqlQuery_Update);
        if ~isempty(Curs.Message)
%             Parfor_Errormsg = [Parfor_Errormsg ; [{NetContraID} ,{Tablename},{Curs.Message}] ];
              Parfor_Errormsg = [Parfor_Errormsg ; [NetContraID,' : ' ,Tablename,' : ',Curs.Message] ];
        end
        
        %% get the current year premium to update in the table for contra id
        FinYear = fetch(ObjDB,'select financial_year from valuation_financial_year_view');
        Tablename = ['premium_fy_table_',InBUName];
        SqlQuery = ['select premium from ',Tablename,' where trade_id = ''',TradeId{iTR},''' and financial_year = ''',char(FinYear),''''];
        CurrentPremium = cell2mat(fetch(ObjDB,SqlQuery));
        
        %         update(ObjDB,Tablename,{'premium'},{CurrentPremium},...
        %             {['where financial_year = ''',char(FinYear),''' and trade_id = ''', NetContraID,'''']});
        
        SqlQuery_Update = ['update ',Tablename,' set premium = ',mat2str(CurrentPremium),' where financial_year = ''',char(FinYear),''' and trade_id = ''', NetContraID,''''];
        
        Curs = exec(ObjDB,SqlQuery_Update);
        if ~isempty(Curs.Message)
%           Parfor_Errormsg = [Parfor_Errormsg ; [{NetContraID} ,{Tablename},{Curs.Message}] ];
            Parfor_Errormsg = [Parfor_Errormsg ; [NetContraID,' : ' ,Tablename,' : ',Curs.Message] ];
        end
        
        
        %% update the netting info and active_lots info for actual trade
        %         update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
        %             {ActiveLots,NettingStatus,NettedLots,NewNettingId,char(DBValueDate)},{['where trade_id = ''',TradeId{iTR},'''']});
        
        SqlQuery_Update = ['update ',ClonedDealTicketTableName,' set active_lots = ',mat2str(ActiveLots),...
            ', netting_status = ''',NettingStatus,'''',...
            ', netted_lots = ',mat2str((NettedLots+TempNettedLots)),...
            ', netting_id = ''',NewNettingId,'''',...
            ', netting_date = ''',char(DBValueDate),'''',...
            ' where trade_id = ''',TradeId{iTR},''''];
        
        Curs = exec(ObjDB,SqlQuery_Update);
        if ~isempty(Curs.Message)           
%             Parfor_Errormsg = [Parfor_Errormsg ; [{TradeId{iTR}} ,{ClonedDealTicketTableName},{Curs.Message}] ];
              Parfor_Errormsg = [Parfor_Errormsg ; [TradeId{iTR},' : ' ,ClonedDealTicketTableName,' : ',Curs.Message] ];
        end
        
        %%
        Tablename = ['deal_active_lots_table_',InBUName];
        %         update(ObjDB,Tablename,{'active_lots'},{ActiveLots},...
        %             {['where value_date = ''',char(DBValueDate),''' and trade_id = ''', TradeId{iTR},'''']});
        
        SqlQuery_Update = ['update ',Tablename,' set active_lots = ',mat2str(ActiveLots),' where value_date = ''',char(DBValueDate),''' and trade_id = ''', TradeId{iTR},''''];
        
        Curs = exec(ObjDB,SqlQuery_Update);
        if ~isempty(Curs.Message)
%             Parfor_Errormsg = [Parfor_Errormsg ; [{TradeId{iTR}} ,{Tablename},{Curs.Message}] ];
            Parfor_Errormsg = [Parfor_Errormsg ; [TradeId{iTR},' : ' ,Tablename,' : ',Curs.Message] ];
        end
        
        %%
        %         Tablename = ['netting_id_table_',InBUName];
        %         fastinsert(ObjDB,Tablename,{'netting_id','trade_id'},{NewNettingId,TradeId{iTR}});
        
        Tablename = ['netting_id_table_',InBUName];
        ColNames = {'netting_date','netting_id','trade_id','security_id','group_type','subportfolio','product_code','counterparty',...
            'contract_month','derivative_type','contract_number','netting_status','netted_lots','active_lots'};
        Data = {DBValueDate,NewNettingId,TradeId{iTR},SecurityId{iTR},GroupType{iTR},Subportfolio{iTR},ProductCode{iTR},CounterpartyParent{iTR},...
            ContractMonth{iTR},DerivativeType{iTR}, ContractNo{iTR},NettingStatus,NettedLots,TempActiveLots * ActiveLotSign};
        fastinsert(ObjDB,Tablename,ColNames,Data);
        
    end
    
    
    
    %% calcualte the netting_pnl value
    %     try
    %         Viewname = ['helper_netting_pnl_view_',InBUName];
    %         SqlQuery = ['select sum(original_premium_paid) from ',Viewname,' where netting_id = ''',NewNettingId,''''];
    %         format bank;
    %         PnL = cell2mat(fetch(ObjDB,SqlQuery));
    %         format short;
    %     catch
    %     end
    
    [OutErrorMessage,NC_PnL,USD_PnL] = calculate_netting_pnl(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName);
    
    %     delete(p);
    %     delete(gcp('nocreate'));
    
catch ME
    OutErrorMessage = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
    %     delete(p);
    %     delete(gcp('nocreate'));
end

