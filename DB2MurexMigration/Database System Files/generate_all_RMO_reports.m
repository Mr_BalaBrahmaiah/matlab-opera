clc;
close;

%%

theCell = {'AGF','COP','QF1','QF2','QF3','QF4','QF5','COP','ORX'};
[Selected_BU, theChosenIDX] = uicellect(theCell,'Multi', 1);


try
    for i = 1 : length(Selected_BU)
        InBUName = char(Selected_BU(i));
        
        if(strcmpi(InBUName,'qf5'))
            [OutErrorMsg2,OutFilename2] = generate_futures_report_qf5(InBUName);
            continue;
        end
        
        [OutErrorMsg1,OutFilename1] = generate_options_report(InBUName);
        [OutErrorMsg2,OutFilename2] = generate_futures_report(InBUName);
        [OutErrorMsg3,OutFilename3] = generate_currency_report(InBUName);
        [OutErrorMsg4,OutFilename4] = generate_trade_query_report(InBUName);
        [OutErrorMsg5,OutFilename5] = generate_riskoffice_rms_report(InBUName);
        [OutErrorMsg6,OutFilename6] = generate_netting_pnl_report(InBUName);
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end