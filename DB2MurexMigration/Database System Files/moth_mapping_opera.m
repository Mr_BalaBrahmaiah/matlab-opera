function[month]= moth_mapping_opera(input_abbreviations)
try
    if iscellstr(input_abbreviations)
        input_abbreviations=char(input_abbreviations);
    end
    
    %% Logic Manipulation.
    if (input_abbreviations=='F')
        month='JAN';
    elseif (input_abbreviations=='G')
        month='FEB';
    elseif(input_abbreviations=='H')
        month='MAR';
    elseif(input_abbreviations=='J')
        month='APR';
    elseif(input_abbreviations=='K')
        month='MAY';
    elseif(input_abbreviations=='M')
        month='JUN';
    elseif(input_abbreviations=='N')
        month='JUL';
    elseif(input_abbreviations=='Q')
        month='AUG';
    elseif(input_abbreviations=='U')
        month='SEP';
    elseif(input_abbreviations=='V')
        month='OCT';
    elseif(input_abbreviations=='X')
        month='NOV';
    elseif (input_abbreviations=='Z')
        month='DEC';
%     else
%         month='-';
    end
    
catch ME
end

end
