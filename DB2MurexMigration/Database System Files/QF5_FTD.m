%% QF5 FTD_Trades
clc
close all

%%
ObjDB = connect_to_database;

OutXLSName = getXLSFilename('QF5_Purchase_Schedule');

ViewName = 'helper_funds_reports_view_qf5' ;
[ColNames,DBData] = Fetch_DB_Data(ObjDB,[],ViewName);

try
    if(iscell(DBData))
        
        User_Want_Header = {'transaction_date','product_code','product_name','original_lots','original_premium'};
        
        OriginalLots_Col = cellStrfind_exact(User_Want_Header,{'original_lots'});
        OriginalPremium_Col = cellStrfind_exact(User_Want_Header,{'original_premium'});
        
        Matched_Cols = cellStrfind_exact(ColNames,User_Want_Header);
        UserWant_Data = DBData(:,Matched_Cols);
                     
        Purchase_Value = num2cell(cell2mat(UserWant_Data(:,OriginalLots_Col)) .* cell2mat(UserWant_Data(:,OriginalPremium_Col))) ;
        
        Output_Header = {'Trade Date','Stock No','Stock Name','Purchase Quantity','Purchase Price','Purchase Value'};
        UserWant_Data = [UserWant_Data , Purchase_Value];
        xlswrite(OutXLSName,[Output_Header ; UserWant_Data],'QF5_Purchase_Schedule');
        
        %%
        try
            OutXLSName = fullfile(pwd,OutXLSName);
            xls_delete_sheets(OutXLSName);
        catch
        end
    else
        msgbox('No Data helper_funds_reports_view_qf5');
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    
end