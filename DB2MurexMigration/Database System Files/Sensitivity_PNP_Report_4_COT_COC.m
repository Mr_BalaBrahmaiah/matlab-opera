function Sensitivity_PNP_Report_4_COT_COC()

try
    theCell = {'COT','COC','COC_PNP'};
    [User_SelectBU, theChosenIDX] = uicellect(theCell,'Multi', 0);
    
    hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');
    
    if(strcmpi(User_SelectBU,'COT'))
        run_sensitivity_report_cot;
        waitbar(1,hWaitbar,'Cotton Sensitivity Finished');
    elseif(strcmpi(User_SelectBU,'COC'))
        run_sensitivity_report_coc(1); %% FUT
        waitbar(0.25,hWaitbar,'Cocoa FUT Sensitivity Finished');       
               
        run_sensitivity_report_coc(0); %% OFUT
        waitbar(0.75,hWaitbar,'Cocoa OFUT Sensitivity Finished');
        
        generate_riskoffice_reports_OPS('coc');
        waitbar(1,hWaitbar,'Cocoa Risk Office Finished');
    else
        InBUName = 'coc';
        [OutErrorMsg,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName); %% PNP
        waitbar(1,hWaitbar,'Cocoa PNP Finished');
    end
    
    msgbox('Process Finished','Finished');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    uiwait(warndlg(errorMessage));
    
end