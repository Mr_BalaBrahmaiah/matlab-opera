function TradersReportFilename = calculate_traders_report_summary
% generate the summary traders report to be sent to all traders
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/26 09:42:47 $
%  $Revision: 1.4 $
%
ViewName = 'helper_5_1_traders_pricing_subportfoliovalues_view';
% ViewName = 'helper_traders_pricing_subportfoliovalues_view';
[TradersColNames,Data] = read_from_database(ViewName,0);
TradersData = cell2dataset([TradersColNames;Data]);
clear Data;  

ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
% ViewName = 'helper_settle_pricing_subportfoliovalues_view';
[SettleColNames,Data] = read_from_database(ViewName,0);
SettleData = cell2dataset([SettleColNames;Data]);
clear Data; 

ValuationDate = TradersData.value_date{1};

% generate Traders report
Subportfolio = unique(upper(TradersData.subportfolio));
TradersReportData = cell(length(Subportfolio),8);

for iS = 1:length(Subportfolio)
    subportfolio_name = Subportfolio(iS);
%     IdxTradersRpt = ismember(TradersData.subportfolio,subportfolio_name);
%     IdxSettleRpt  = ismember(SettleData.subportfolio,subportfolio_name);
    IdxTradersRpt = strcmpi(subportfolio_name,TradersData.subportfolio);
    IdxSettleRpt  = strcmpi(subportfolio_name,SettleData.subportfolio);
    
    TradersReportData(iS,1) = subportfolio_name;
    TradersReportData(iS,2) = num2cell(sum(TradersData.traders_delta_1(IdxTradersRpt)) + sum(TradersData.traders_delta_2(IdxTradersRpt)));
    TradersReportData(iS,3) = num2cell(sum(TradersData.traders_gamma_11(IdxTradersRpt)) + sum(TradersData.traders_gamma_12(IdxTradersRpt)) + ...
        sum(TradersData.traders_gamma_21(IdxTradersRpt)) + sum(TradersData.traders_gamma_22(IdxTradersRpt)));
    TradersReportData(iS,4) = num2cell(sum(TradersData.traders_theta(IdxTradersRpt)) * (7/5));
    TradersReportData(iS,5) = num2cell(sum(TradersData.traders_vega_1(IdxTradersRpt)) + sum(TradersData.traders_vega_2(IdxTradersRpt)));
    TradersReportData(iS,6) = num2cell(sum(TradersData.bv_usd(IdxTradersRpt)));
    
    TradersReportData(iS,8) = num2cell(sum(SettleData.mtm_usd(IdxSettleRpt)));
    
    TradersReportData(iS,7) = num2cell(cell2mat(TradersReportData(iS,8)) - cell2mat(TradersReportData(iS,6)));
end

TableName = 'subportfolio_tr_format_table';
[ColNames,Data] =  read_from_database(TableName,0);
PosPortfolio = strcmpi('subportfolio',ColNames);
TRFormat = Data(:,PosPortfolio);

NumRows = length(TRFormat);
TRFormatOutput = cell(NumRows,8);
for iOut = 1:NumRows
    Idx = find(strcmpi(TRFormat{iOut},TradersReportData(:,1)));
    if ~isempty(Idx)
        TRFormatOutput(iOut,:) = TradersReportData(Idx,:);
    else
        TRFormatOutput(iOut,1) = TRFormat(iOut);
        TRFormatOutput(iOut,2:end) = num2cell(0);
    end
end

TradersReportHeader = {'subportfolio','delta','gamma','theta','vega','bv-usd','mkt-us','mtm-usd'};
% TradersReportFilename = ['Traders_Report_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
% customcsvwrite(TradersReportFilename,TRFormatOutput,TradersReportHeader);

TradersReportFilename = getXLSFilename('Traders_Report');
xlswrite(TradersReportFilename,[TradersReportHeader;TRFormatOutput]);

end