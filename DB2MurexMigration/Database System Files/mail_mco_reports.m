function mail_mco_reports

try
    configure_mail_settings;
    
catch
end

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

InBUName = {'agf','qf1','qf2','qf3'};

for iBU = 1:length(InBUName)
[~,COMPLIANCE_TRADED_LOTS_REPORT,OLAM_CFSG_OTC_DELTA_POSITION_REPORT,INVENIO_DELTA_POSITION_REPORT] = generate_compliance_delta_reports(InBUName{iBU});

try
    
    sendmail({'mco.reporting@olamnet.com','Raghavendra.Sn@olamnet.com'}, [upper(InBUName{iBU}),': OLAM CFSG, INVENIO DELTA and TRADED LOTS Reports'], ...
        ['Attached are the MCO reports for COB ''',SettlementDate,''''],...
        {OLAM_CFSG_OTC_DELTA_POSITION_REPORT,INVENIO_DELTA_POSITION_REPORT,COMPLIANCE_TRADED_LOTS_REPORT});
catch
end

pause(5);
end

[~,OutFilename] = generate_counterparty_delta_report('orx');
try
    
    sendmail({'mco.reporting@olamnet.com','Raghavendra.Sn@olamnet.com'}, 'ORX : OLAM CFSG, INVENIO DELTA and TRADED LOTS Reports', ...
        ['Attached are the MCO reports for COB ''',SettlementDate,''''],...
        {OutFilename});
catch
end

try
    [~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
    ReportNames = ConfigFileData(2:end,1);
    OutputFolders = ConfigFileData(2:end,2);
    IdxFound = strcmpi('ComplianceReports',ReportNames);
    DestinationFolder = OutputFolders{IdxFound};
    
    LogFilename = ['DB_automation_log_',datestr(today),'.txt'];
    
    try
        TempDestinationFolder = [DestinationFolder,'\Delta Reports'];
        OutTradedLotsReport = fullfile(TempDestinationFolder,COMPLIANCE_TRADED_LOTS_REPORT);
        move_reports(COMPLIANCE_TRADED_LOTS_REPORT,OutTradedLotsReport,'COMPLIANCE_TRADED_LOTS_REPORT');
        OutOTCReport = fullfile(TempDestinationFolder,OLAM_CFSG_OTC_DELTA_POSITION_REPORT);
        move_reports(OLAM_CFSG_OTC_DELTA_POSITION_REPORT,OutOTCReport,'OLAM_CFSG_OTC_DELTA_POSITION_REPORT');
        OutInvenioDeltaReport = fullfile(TempDestinationFolder,INVENIO_DELTA_POSITION_REPORT);
        move_reports(INVENIO_DELTA_POSITION_REPORT,OutInvenioDeltaReport,'INVENIO_DELTA_POSITION_REPORT');
    catch
        write_log_file(LogFilename,'Unknown error while saving Delta Reports under Compliance Reports!');
    end
    
catch
end