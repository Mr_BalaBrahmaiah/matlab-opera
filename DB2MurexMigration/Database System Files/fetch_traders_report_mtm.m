[filename, pathname] = uigetfile( ...
    {'*.xlsx';'*.xls';'*.csv';'*.*'}, ...
    'Pick a file','MultiSelect', 'on');

OutFilename = 'Consolidated_Traders_Report_2015.xlsx';

if pathname ~= 0
    StrFilenames = strrep(filename,'Traders_Report_','');
    StrFilenames = StrFilenames';
    StrFilenames = strrep(StrFilenames,'.xlsx','');
    StrFilenames = strrep(StrFilenames,'.xls','');
    StrFilenames = strrep(StrFilenames,'.csv','');
    
    [Dates,Times] = strtok(StrFilenames,'_');
    Times = strrep(Times,'_','');
    
    ObjDB = connect_to_database;
    DBData = fetch(ObjDB,'select subportfolio,directional_portfolio from subportfolio_product_mapping_table');
    DBPortfolio = DBData(:,1);
    DBDIRPortfolio = DBData(:,2);
    
    LoopIter = 1;
    for iFile = 1:length(filename)
        Fullfilename = fullfile(pathname,filename{iFile});
        [~,~,RawData ] = xlsread(Fullfilename);
        TRData = cell2dataset(RawData);
        NumPortfolios = length(TRData.subportfolio);
        for iPF = 1:NumPortfolios
            [IsF,IdxF] = ismember(TRData.subportfolio{iPF},DBDIRPortfolio);
            if ~IsF % if its not a DIR portfolio
                TempPF = regexprep(TRData.subportfolio{iPF},'[^a-zA-Z0-9]','_');
                OutData.(TempPF).ValueDate(LoopIter,1) = Dates(iFile);
                OutData.(TempPF).UpdatedTimestamp(LoopIter,1) = Times(iFile);
                OutData.(TempPF).M2MValue(LoopIter,1) = num2cell(TRData.mtm_usd(iPF));
                OutData.(TempPF).DIRValue(LoopIter,1) = {0};
                
            else
                TempPF = regexprep(DBPortfolio{IdxF},'[^a-zA-Z0-9]','_');
                OutData.(TempPF).DIRValue(LoopIter,1) = num2cell(TRData.mtm_usd(iPF));
            end
            
        end
        LoopIter = LoopIter + 1;
    end
    
    
    StcPF = fieldnames(OutData);
    for iSTC = 1:length(StcPF)
        try
        TempVar = unique(sortrows(struct2dataset(OutData.(StcPF{iSTC})),[1,2]),'ValueDate','legacy');
        catch
        end
        xlswrite(OutFilename,dataset2cell(TempVar),StcPF{iSTC});
    end
    
    try
        xls_delete_sheets(fullfile(pwd,OutFilename));
    catch
    end
end