function [OutErrorMsg,Filename] = process_fx_expiry(InBUName)
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/16 08:29:36 $
%  $Revision: 1.10 $
%

OutErrorMsg = {'No errors'};
Filename = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    % TradeIdPrefix = 'TR-FY16-17-';
    ObjDB = connect_to_database;
    
    SqlQuery = 'select settlement_date from valuation_date_table';
    SettleDate = fetch(ObjDB,SqlQuery);
    
    ViewName = ['helper_fx_expiry_processing_view_',InBUName];
    SqlQuery = ['select * from ',ViewName];
    [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    if strcmpi(Data,'No Data')
        OutErrorMsg = {'No fixing deals found!'};
        return
    end
    ExpiryProcViewData = cell2table(Data);
    ExpiryProcViewData.Properties.VariableNames = ColNames;
    
    DBFxData = [];
    
    ObjDB = connect_to_database;
    
    UniqCP = unique(ExpiryProcViewData.counterparty_parent);
    
%     SqlQuery = 'select start_date from valuation_financial_year_view';
%     StartDate = char(fetch(ObjDB,SqlQuery));
    
    [~,DBTradeId]= getLastTradeId(InBUName);
    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
    TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
    if(isempty(TempTradeId))
        TempTradeId = 0;      %% Default
    end   
    
    for iCP = 1:length(UniqCP)
        IdxCP = strcmpi(UniqCP{iCP},ExpiryProcViewData.counterparty_parent);
        TempExpData = ExpiryProcViewData(IdxCP,:);
        
        if strcmpi(TempExpData.group_type,'FXD')
        i = 1;
        TempTradeId = TempTradeId + 1;
        TempTradeId1 =  TempTradeId;
        else
          TempTradeId1= '';
        end
%         SqlQuery = ['select distinct(contract_number) from cloned_deal_ticket_table_cfs where transaction_date >= ''',StartDate,''' and counterparty_parent = ''',UniqCP{iCP},''''];
        SqlQuery = ['select distinct(contract_number) from cloned_deal_ticket_table_cfs where counterparty_parent = ''',UniqCP{iCP},''''];
        DBContractNumbers = fetch(ObjDB,SqlQuery);
        
        ContractNo = TempExpData.contract_number(i);
        
        if ~isempty(DBContractNumbers)
            
            %%% old code
            %[CLIENTCODES,REMCNO] = strtok(DBContractNumbers,'_');
            %[PRODCODES,REMCNO] = strtok(REMCNO,'_');
        
            %%% newly added logic(30-07-2019), requirement provided by Tarik
            ContractNo = TempExpData.contract_number(i);
            if ~strcmpi(ContractNo,{''})
                ContractNo11 = strsplit(char(ContractNo),'_');
                String = strcat(ContractNo11(1),'_',ContractNo11(2));
                Index = contains(DBContractNumbers,String);

                [CLIENTCODES,REMCNO] = strtok(DBContractNumbers(Index),'_');
                [PRODCODES,REMCNO] = strtok(REMCNO,'_'); 
            else
                [CLIENTCODES,REMCNO] = strtok(DBContractNumbers,'_');
                [PRODCODES,REMCNO] = strtok(REMCNO,'_');
            end
            %%% end code
        
            try
                CNOs = str2num(cell2mat(strrep(REMCNO,'_',''))); % This line will throw an error if the number of digits is not same while using cell2mat(if the contract nos are as 199,200,100 this will work; if the contract nos are as 199,200,1000 this will fail)
            catch
                CNOs = str2double(strrep(REMCNO,'_',''));
            end
            
            NewCNo = max(CNOs)+1;
            
            if isempty(NewCNo)
                ContractNo = TempExpData.contract_number(i);
            else
                %           ContractNo =  [CLIENTCODES{1},'_',PRODCODES{1},'_',num2str(NewCNo)]; % As we consider PRODCODES{1}, this string goes wrong with some other product code other than FX like CVI_KC_110
                NumChars = numel(num2str(NewCNo));
                if NumChars == 1
                    ContractNo =  [CLIENTCODES{1},'_','FX','_00',num2str(NewCNo)]; 
                elseif NumChars == 2
                    ContractNo =  [CLIENTCODES{1},'_','FX','_0',num2str(NewCNo)]; 
                else
                    ContractNo =  [CLIENTCODES{1},'_','FX','_',num2str(NewCNo)]; 
                end
            end
        end
        TradeId = [TradeIdPrefix,num2str(TempTradeId1)];
        
        Lots = sum(TempExpData.active_lots);
        if isnumericequal(Lots,0)
            continue;
        else
            if Lots < 0
                MarketAction = {'bought'};
                FxLots = abs(Lots);
            elseif Lots > 0
                MarketAction = {'sold'};
                FxLots = abs(Lots) * -1;
            end
        end
        
        SqlQuery = ['select spot from currency_spot_mapping_table where currency = ''',(TempExpData.currency{i}),''' '];
        [~,SpotFactor]   = Fetch_DB_Data(ObjDB,SqlQuery);
        
        SqlQuery = ['select settle_value from underlying_settle_value_table where settlement_date = ''',char(SettleDate),...
            ''' and underlying_id = ''',char(SpotFactor),''''];
        SpotRate = fetch(ObjDB, SqlQuery);
        Premium = SpotRate;
        if isempty(Premium)
             OutErrorMsg = {['Spot value is not available for ',char(SpotFactor),' for COB ',char(SettleDate),'!']};
             return;
        end
        
         RowFxData = [cellstr(TradeId),cellstr(TradeId),...
            SettleDate,TempExpData.subportfolio(i),TempExpData.instrument(i),TempExpData.group_type(i),...
            TempExpData.security_id(i),cellstr(MarketAction),FxLots,...
            Premium,TempExpData.counterparty_parent(i), TempExpData.counterparty_bu(i),...
            TempExpData.counterparty_entity(i),TempExpData.execution_broker(i),TempExpData.clearing_broker(i),...
            TempExpData.reconed_with_clearer(i),TempExpData.reconed_completed_by(i),...
            TempExpData.recon_datetimestamp(i),ContractNo,{'NULL'},TempExpData.opt_periodicity(i),0,FxLots,...
            TempExpData.fixing_date(i),{'NULL'},{'NULL'},{'live'},TempExpData.exe_type(i),TempExpData.brokerage_rate_type(i),...
            TempExpData.spcl_rate(i),TempExpData.other_remarks(i),...
            0,{'System'},{'NULL'},{'System generated expiry'},...
            {'NULL'},{'NULL'},{'NULL'}];
        DBFxData = [DBFxData;RowFxData];        
        

    end
           
    if ~isempty(DBFxData)
        try
            DBFxData = sortrows(DBFxData,PosTradeId);
        catch
            
        end
        
        DBHeader = {'trade_id','parent_trade_id','transaction_date','subportfolio','instrument',...
            'trade_group_type','security_id','market_action','original_lots','premium',...
            'counterparty_parent','counterparty_bu','counterparty_entity','execution_broker',...
            'clearing_broker','reconed_with_clearer','reconed_completed_by','recon_datetimestamp',...
            'contract_number','subcontract_number','opt_periodicity','is_netting_done','lots_to_be_netted',...
            'date_to_be_fixed','trader_name','traded_timestamp','deal_status','exe_type','brokerage_rate_type',...
            'spcl_rate','other_remarks','version_no','version_created_by','version_timestamp','version_comments','ops_unique','broker_lots','ops_action'};
        Filename  = getXLSFilename(['FX_Expiry_',InBUName]);
        
        % rajashekhar Added code
        index_fxd=strcmpi(DBHeader,'trade_group_type');
        index_fx_group=strcmpi(DBFxData(:,index_fxd),'FXD');
        DBFxData1=DBFxData(index_fx_group,:);
        DBFxData2=DBFxData(~index_fx_group,:);
        xlswrite(Filename,[DBHeader;DBFxData1]);
        xlswrite(Filename,[ColNames;Data],'FxExpiryDealDump');
    
        if ~isempty(DBFxData2)
            xlswrite(Filename,[ DBHeader;DBFxData2],'Fx_OPT');  
        end
        OutXLSFileName = fullfile(pwd,Filename);
        try
            xls_delete_sheets(OutXLSFileName);
        catch
        end
    else
        OutErrorMsg = {'FX Expiry processing executed already!'};
        return;
    end
%     try
%         SharedFolder = get_reports_foldername;
%         DestinationFile = fullfile(SharedFolder,Filename);
%         movefile(Filename,DestinationFile);
%     catch ME
%         OutErrorMsg = cellstr(ME.message);
%     end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    Filename = '';
end
end