function [ ClientStatementFolder ] = get_clientstatement_foldername
try
DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
IdxFound = cellStrfind(DBConfigData,'clientstatementfolder');
if ~isempty(IdxFound)
    [~,ClientStatementFolder] = strtok(DBConfigData(IdxFound),'=');
    ClientStatementFolder = char(strrep(ClientStatementFolder,'=',''));
else
    CurrentUser = getenv('username');
    ClientStatementFolder = ['C:\Users\',CurrentUser,'\uploads'];
end
catch
    CurrentUser = getenv('username');
    ClientStatementFolder = ['C:\Users\',CurrentUser,'\uploads'];
end

end

