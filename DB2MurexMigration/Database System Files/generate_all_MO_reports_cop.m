function [OutErrorMsg,TradersReportSummaryFilename,VARFilename,TradersReport_MonthWise_Filename,...
    Position_PnL_Filename,MonthEnd_EOD_Filename] = generate_all_MO_reports_cop(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

%% Traders Report Summary

[OutErrorMsg1,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    TradersReportSummaryFilename = char(OutErrorMsg1);
end

cprintf('key','%s finished\n', 'Traders Report Summary');

%% VAR Report

[OutErrorMsg2,VARFilename] = generate_accounting_VAR_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    VARFilename = char(OutErrorMsg2);
end

cprintf('key','%s finished\n', 'VAR Report');

%% Traders Report Monthwise

[OutErrorMsg3,TradersReport_MonthWise_Filename] = generate_detailed_traders_report(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    TradersReport_MonthWise_Filename = char(OutErrorMsg3);
end

cprintf('key','%s finished\n', 'Traders Report Monthwise');

%% Position and PnL Report

[OutErrorMsg4,Position_PnL_Filename] =  generate_dirbook_report(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    Position_PnL_Filename = char(OutErrorMsg4);
end

cprintf('key','%s finished\n', 'Position and PnL Report');

%% Monthend_EOD

[OutErrorMsg5,MonthEnd_EOD_Filename] = generate_monthend_eod_report(InBUName);
if ~strcmpi(OutErrorMsg5,'No errors')
    MonthEnd_EOD_Filename = char(OutErrorMsg5);
end

cprintf('key','%s finished\n', 'Monthend_EOD');

%% Overall Error Message

Overall_ErrorMsg = [OutErrorMsg1;OutErrorMsg2;OutErrorMsg3;OutErrorMsg4;OutErrorMsg5];


end


