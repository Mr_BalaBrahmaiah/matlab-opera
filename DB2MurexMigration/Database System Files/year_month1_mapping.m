function Month1_val = year_month1_mapping (DSData,Mapping_Tab_Data)

           [rr,cc]=size(DSData);
           Month1_val=[];
           for jj= 1:1:rr
           
           Month1_col=DSData.Month1{jj};
           %Pre_year_month1
           if any(isnan(Month1_col))
               %continue; % disp('No Data');
           else
               
            [PreMonth1,PostMonth1] = strtok(Month1_col,'.'); %
            PostMonth1 = strrep(PostMonth1,'.','');
            
             PreMonth1_last2digits =PreMonth1(end-1:end);
             PostMonth1_last2digits = PostMonth1(end-1:end);
             
             Pre1_NumberIndex = isempty(str2num(char(PreMonth1_last2digits)));
             
             Post1_NumberIndex = isempty(str2num(char(PostMonth1_last2digits)));

             Mapping_Tab_digit=Mapping_Tab_Data.single_digit_year;
                        
            if ~(Pre1_NumberIndex)

            else
                Pre_Last_ltr1=PreMonth1(end);
                
              for j=1:1:length(Mapping_Tab_digit)
                Mapping_Tab_digit_val=Mapping_Tab_digit(j);
                if isequal(str2double(Pre_Last_ltr1),Mapping_Tab_digit_val)
                    Pre_Year_ltr1 = Mapping_Tab_Data.double_digit_year(j);
                    PreMonth1=strrep(PreMonth1,Pre_Last_ltr1,num2str(Pre_Year_ltr1));
                    break;
                end   
             end
            
            end

            if ~(Post1_NumberIndex)

            else
                Post_Last_ltr1=PostMonth1(end);
                
                for j=1:1:length(Mapping_Tab_digit)
                Mapping_Tab_digit_val=Mapping_Tab_digit(j);
                if isequal(str2double(Post_Last_ltr1),Mapping_Tab_digit_val)
                    Post_Year_ltr1 = Mapping_Tab_Data.double_digit_year(j);
                    PostMonth1=strrep(PostMonth1,Post_Last_ltr1,num2str(Post_Year_ltr1));
                    break;
                end   
               end
            
            end 

            Pre_year_month1=string(PreMonth1); 
            Post_year_month1=string(PostMonth1);
            Month1_col = char(strcat(Pre_year_month1,'.',Post_year_month1));
%             Month1_col=string(Month1_col);
           end
              %Month1_val = vertcat(string(Month1_val),string(Month1_col));
              Month1_val{jj,1} = Month1_col;

           end
           
 end

       