function [OutErrorMsg,OutFilename1,OutFilename2,OutFilename3] = generate_compliance_delta_reports(InBUName)

OutErrorMsg = {'No errors'};
OutFilename1 = '';
OutFilename2 = '';
OutFilename3 = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    % ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    ViewName = 'helper_compliance_reports_view';
    [SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);
    
    if(strcmpi(SettleData,'No Data'))  %%  || size(SettleData,2)<=1 )
        OutErrorMsg = {'No Data Found..!'};
        return;
    else
        % change the product code of Rubber to OR; since it is currently OR-EUR or
        % OR-USD
        PosProdCode = strcmpi('product_code',SettleColNames);
        IdxOR = strncmpi(SettleData(:,PosProdCode),'OR',2);
        SettleData(IdxOR,PosProdCode) = {'OR'};
        
        PosGroup = strcmpi('group_type',SettleColNames);
        PosTradedLots = strcmpi('active_lots',SettleColNames);
        PosTradedPrice = strcmpi('original_premium',SettleColNames);
        
        IdxFXD  = strcmpi('FXD',SettleData(:,PosGroup));
        USDLots = cell2mat(SettleData(IdxFXD,PosTradedLots));
        TradedPrice = cell2mat(SettleData(IdxFXD,PosTradedPrice));
        NCLots = USDLots .* TradedPrice .* -1;
        SettleData(IdxFXD,PosTradedLots) = num2cell(NCLots);
        
        IdxFXOPT  = strcmpi('FX_OPT',SettleData(:,PosGroup));
        USDLots = cell2mat(SettleData(IdxFXOPT,PosTradedLots));
        % TradedPrice = cell2mat(SettleData(IdxFXOPT,PosTradedPrice));
        % NCLots = USDLots .* TradedPrice .* -1;
        NCLots = USDLots .* -1;
        SettleData(IdxFXOPT,PosTradedLots) = num2cell(NCLots);
        
        SettleColNames = strrep(SettleColNames,'settle_delta_1','delta1');
        SettleColNames = strrep(SettleColNames,'settle_delta_2','delta2');
        SettleColNames = strrep(SettleColNames,'active_lots','traded_lots');
        SettleColNames = strrep(SettleColNames,'group_type','group');
        SettleColNames = strrep(SettleColNames,'contract_month','maturity_month');
        SettleColNames = strrep(SettleColNames,'counterparty_parent','counterparty');
        
        UniqueFields = {'product_code','product_name','asset_class','exchange_name','counterparty','group','maturity_month'}; %%{'asset_class','counterparty','product_code','group','maturity_month'};
        SumFields = {'delta1','delta2','traded_lots'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        
        [OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        OutData = cell2dataset([OutputFields;OutData]);
        
        IdxExchTraded = zeros(size(OutData.counterparty));
        IdxExchTraded = logical(IdxExchTraded);
        
        TempExchIdx   = strcmpi('FUT',OutData.group) | strcmpi('OFUT',OutData.group) | strcmpi('FXFUT',OutData.group);
        %     TempOTCIdx = strcmpi('FXD',SettleData(:,PosGroup));
        
        IdxExchTraded(TempExchIdx) = 1;
        IdxOTC    = ~IdxExchTraded;
        
        OTCData   = OutData(IdxOTC,:);
        
        OTCDelta       = dataset2cell(OTCData);
        OTCFields      = OTCDelta(1,:);
        OTCDeltaData   = OTCDelta(2:end,:);
        
        ExchTradedData  = OutData(IdxExchTraded,:);
        
        ExchDelta = dataset2cell(ExchTradedData);
        ExchFields      = ExchDelta(1,:);
        ExchDeltaData   = ExchDelta(2:end,:);
        
        %     OutFilename1 = getXLSFilename('COMPLIANCE_TRADED_LOTS');
        Filename = ['COMPLIANCE_TRADED_LOTS_',upper(InBUName)];
        OutFilename1 = getXLSFilename(Filename);
        xlswrite(OutFilename1,[ExchFields;ExchDeltaData]);
        
        %     OutFilename2 = getXLSFilename('OLAM_CFSG_OTC_DELTA_POSITION_REPORT');
        Filename = ['OLAM_CFSG_OTC_DELTA_POSITION_REPORT_',upper(InBUName)];
        OutFilename2 = getXLSFilename(Filename);
        xlswrite(OutFilename2,[OTCFields;OTCDeltaData]);
        
        %     OutFilename3 = getXLSFilename('INVENIO_DELTA_POSITION_REPORT');
        Filename = ['INVENIO_DELTA_POSITION_REPORT_',upper(InBUName)];
        OutFilename3 = getXLSFilename(Filename);
        xlswrite(OutFilename3,[ExchFields;ExchDeltaData]);
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename1 = '';
    OutFilename2 = '';
    OutFilename3 = '';
    
end