function [OutErrorMsg,TradersReportFilename] = generate_traders_report_summary(InBUName)
% generate the summary traders report to be sent to all traders
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 06:17:43 $
%  $Revision: 1.9 $
%


OutErrorMsg = {'No errors'};
TradersReportFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutVARData = []; OutDrawdownData =[]; OIDAccTRFormatOutput = []; OutARBData= [];
    ObjDB = connect_to_database;
    
    
    ViewName = 'helper_traders_report_settle_bv_view';
    [ColNames,InData] = read_from_database(ViewName,0,'',InBUName);
    InitColNames = ColNames;
    
    SettleColNames  = ColNames(strmatch('settle_',ColNames));
    TradersColNames = ColNames(strmatch('traders_',ColNames));
    
    ColNames = setdiff(ColNames,SettleColNames,'stable');
    ColNames = setdiff(ColNames,TradersColNames,'stable');
    SettleColNames = [ColNames,SettleColNames];
    TradersColNames = [ColNames,TradersColNames];
    
    IdxSettleCols = ismember(InitColNames,SettleColNames);
    SettleData = InData(:,IdxSettleCols);
    UpdSettleColNames = strrep(SettleColNames,'settle_','');
    OutData = calculate_pricing_views(UpdSettleColNames,SettleData,InBUName);
    SettleData = cell2dataset([SettleColNames;SettleData]);
    SettleData.settle_delta_1 = OutData.delta_1;
    SettleData.settle_delta_2 = OutData.delta_2;
    SettleData.settle_gamma_11 = OutData.gamma_11;
    SettleData.settle_gamma_12 = OutData.gamma_12;
    SettleData.settle_gamma_21 = OutData.gamma_21;
    SettleData.settle_gamma_22 = OutData.gamma_22;
    SettleData.settle_vega_1 = OutData.vega_1;
    SettleData.settle_vega_2 = OutData.vega_2;
    SettleData.settle_theta = OutData.theta;
    SettleData.mtm_nc       = OutData.mtm_nc;
    SettleData.mtm_usd      = OutData.mtm_usd;
    SettleData.cumulative_mtm_usd =  OutData.cumulative_mtm_usd;
    SettleData.cumulative_mtm_nc  =  OutData.cumulative_mtm_nc;
    
    SettleData.premium_paid = OutData.premium_paid;
    SettleData.premium_paid_usd = OutData.premium_paid_usd;
    SettleData.mkt_value = OutData.mkt_value;
    SettleData.mkt_value_usd = OutData.mkt_value_usd;
    
    SettleData.realised_pnl = OutData.realised_pnl;
    SettleData.realised_pnl_usd = OutData.realised_pnl_usd;
    SettleData.unrealised_pnl = OutData.unrealised_pnl;
    SettleData.unrealised_pnl_usd = OutData.unrealised_pnl_usd;
    
    SettleData.settle_gamma_11_mt = OutData.gamma_11_mt;
    SettleData.settle_gamma_12_mt = OutData.gamma_12_mt;
    SettleData.settle_gamma_21_mt = OutData.gamma_21_mt;
    SettleData.settle_gamma_22_mt = OutData.gamma_22_mt;
    
    SData = dataset2cell(SettleData);
    SettleColNames = SData(1,:);
    SData = SData(2:end,:);
    
    IdxTradersCols = ismember(InitColNames,TradersColNames);
    TradersData = InData(:,IdxTradersCols);
    UpdTradersColNames = strrep(TradersColNames,'traders_','');
    OutData = calculate_pricing_views(UpdTradersColNames,TradersData,InBUName);
    TradersData = cell2dataset([TradersColNames;TradersData]);
    TradersData.traders_delta_1 = OutData.delta_1;
    TradersData.traders_delta_2 = OutData.delta_2;
    TradersData.traders_gamma_11 = OutData.gamma_11;
    TradersData.traders_gamma_12 = OutData.gamma_12;
    TradersData.traders_gamma_21 = OutData.gamma_21;
    TradersData.traders_gamma_22 = OutData.gamma_22;
    TradersData.traders_vega_1 =  OutData.vega_1;
    TradersData.traders_vega_2 =  OutData.vega_2;
    TradersData.traders_theta = OutData.theta;
    TradersData.mtm_nc        = OutData.mtm_nc;
    TradersData.mtm_usd = OutData.mtm_usd;
    TradersData.cumulative_mtm_usd =  OutData.cumulative_mtm_usd;
    TradersData.cumulative_mtm_nc =  OutData.cumulative_mtm_nc;
    
    TradersData.premium_paid = OutData.premium_paid;
    TradersData.premium_paid_usd = OutData.premium_paid_usd;
    TradersData.mkt_value = OutData.mkt_value;
    TradersData.mkt_value_usd = OutData.mkt_value_usd;
    
    TradersData.realised_pnl = OutData.realised_pnl;
    TradersData.realised_pnl_usd = OutData.realised_pnl_usd;
    TradersData.unrealised_pnl = OutData.unrealised_pnl;
    TradersData.unrealised_pnl_usd = OutData.unrealised_pnl_usd;
     
    %     store accounting data for var report
    try
        UniqueFields = {'value_date','settlement_date','subportfolio','p1_name','contract_month','derivative_type','strike'};
        SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11_mt',...
            'settle_gamma_12_mt','settle_gamma_21_mt','settle_gamma_22_mt',...
            'settle_vega_1','settle_vega_2','settle_theta','mtm-usd','mtm-nc'};
        OutputFields = [UniqueFields,'p1_settleprice',SumFields];
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(SettleColNames, SData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        Delta = cell2mat(OutData(:,9)) +  cell2mat(OutData(:,10));
        Gamma = cell2mat(OutData(:,11)) +  cell2mat(OutData(:,12)) + cell2mat(OutData(:,13)) +  cell2mat(OutData(:,14));
        Vega =  cell2mat(OutData(:,15)) +  cell2mat(OutData(:,16));
        
        AccVARReportData = [OutData(:,1:8),num2cell(Delta),num2cell(Gamma),num2cell(Vega),OutData(:,17:end)];
        
        TableName = ['var_accounting_table_',InBUName];
        set(ObjDB,'AutoCommit','off');
        DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
        SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, AccVARReportData);
        
        update_curr_mult_table;
        
    catch ME
        disp('error occured while uploading var_accounting_table data!');
    end
    
    %%TODO - Generate the VAR report
    try
        TableName = ['var_aggregation_level_table_',InBUName];
        
        if (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg'))
            SqlQuery = ['select distinct(aggregation_level) from ',TableName,' where subportfolio like ''ZT%'' ']; %%  %% where breach_var_group is null
        else
            SqlQuery = ['select distinct(aggregation_level) from ',TableName];
        end
        
        AggregationLevel = fetch(ObjDB,SqlQuery);
        
        if (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg'))
            NumDays = 100;%100,252,500
            [~,~,Out100VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
            NumDays = 252;
            [~,~,Out252VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
            NumDays = 500;
            [~,~,Out500VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
            
            %     % calculate the average of 100,252 and 500 days VAR
            OutputHeader = {'value_date','Aggregation_level','VAR'};
            ConsolidatedVARData = [Out100VARData(2:end,:);Out252VARData(2:end,:);Out500VARData(2:end,:)];
            DSConsolidatedVARData = cell2dataset([OutputHeader;ConsolidatedVARData]);
            AggrLevel = unique(DSConsolidatedVARData.Aggregation_level);
            
            for i = 1:length(AggrLevel)
                IdxFound = strcmpi(AggrLevel{i},DSConsolidatedVARData.Aggregation_level);
                ValueDate = unique(DSConsolidatedVARData.value_date(IdxFound));
                AvgVAR = mean(DSConsolidatedVARData.VAR(IdxFound));
                RowData = [ValueDate,AggrLevel(i),num2cell(AvgVAR)];
                OutVARData = [OutVARData;RowData];
            end
            OutVARData = [OutputHeader;OutVARData];
        else
            NumDays = 950;
            [~,~,OutVARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
        end
        
        TableName = ['var_report_table_',InBUName];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, OutVARData);
    catch ME
        disp(ME.message);
    end
    %%End of TODO - Generate the VAR report
    
    SettleData = cell2dataset([SettleColNames;SData]);
    %     clear Data;
    
    ValuationDate = TradersData.value_date{1};
    
    [OutputHeader,ConsDeals] = compute_monthend_eod_values(InBUName,SettleData);
    EODSettleData = cell2dataset([OutputHeader;ConsDeals]);
    
    [OutputHeader,ConsDeals] = compute_monthend_eod_values(InBUName,TradersData);
    EODTradersData = cell2dataset([OutputHeader;ConsDeals]);
    
    % generate Traders report
    Subportfolio = unique(upper(TradersData.subportfolio));
    if(strcmpi(InBUName,'cop'))
        TradersReportData = cell(length(Subportfolio),11);
        AccTradersReportData = cell(length(Subportfolio),9);
        AccMktValue = cell(length(Subportfolio),1);
    else
        TradersReportData = cell(length(Subportfolio),8);
        AccTradersReportData = cell(length(Subportfolio),6);
        AccMktValue = cell(length(Subportfolio),1);
    end
    
    
    %%
    for iS = 1:length(Subportfolio)
        subportfolio_name = Subportfolio(iS);
        IdxTradersRpt = strcmpi(subportfolio_name,TradersData.subportfolio);
        IdxSettleRpt  = strcmpi(subportfolio_name,SettleData.subportfolio);
        IdxEODData = strcmpi(subportfolio_name,EODSettleData.PORTFOLIO);
        IdxEodTData = strcmpi(subportfolio_name,EODTradersData.PORTFOLIO);
        
        if(strcmpi(InBUName,'cop'))
            TradersReportData(iS,1) = subportfolio_name;
            TradersReportData(iS,2) = num2cell(nansum(TradersData.traders_delta_1(IdxTradersRpt)) + nansum(TradersData.traders_delta_2(IdxTradersRpt)));
            
            TradersReportData(iS,3) = num2cell(nansum(TradersData.traders_delta_1(IdxTradersRpt) .* TradersData.lot_mult1(IdxTradersRpt)));
            
            TradersReportData(iS,4) = num2cell(nansum(TradersData.traders_gamma_11(IdxTradersRpt)) + nansum(TradersData.traders_gamma_12(IdxTradersRpt)) + ...
                nansum(TradersData.traders_gamma_21(IdxTradersRpt)) + nansum(TradersData.traders_gamma_22(IdxTradersRpt)));
            TradersReportData(iS,5) = num2cell(nansum(TradersData.traders_theta(IdxTradersRpt)) .* (7/5));
            TradersReportData(iS,6) = num2cell(nansum(TradersData.traders_vega_1(IdxTradersRpt)) + nansum(TradersData.traders_vega_2(IdxTradersRpt)));
            
            TradersReportData(iS,7) = num2cell(nansum(EODTradersData.Net_Total_USD(IdxEodTData)));
            TradersReportData(iS,9) = num2cell(nansum(EODSettleData.Net_Total_USD(IdxEODData)));
            
            TradersReportData(iS,8) = num2cell(nansum(cell2mat(TradersReportData(iS,9)) - cell2mat(TradersReportData(iS,7))));
            
            TradersReportData(iS,10) = num2cell(nansum(TradersData.active_lots(IdxTradersRpt) .* TradersData.traders_price(IdxTradersRpt) .* TradersData.mult_factor(IdxTradersRpt)));
            %              TradersReportData(iS,11) = '';
            
            AccTradersReportData(iS,1) = subportfolio_name;
            AccTradersReportData(iS,2) = num2cell(nansum(SettleData.settle_delta_1(IdxSettleRpt)) + nansum(SettleData.settle_delta_2(IdxSettleRpt)));
            
            AccTradersReportData(iS,3) = num2cell(nansum(SettleData.settle_delta_1(IdxSettleRpt) .* SettleData.lot_mult1(IdxSettleRpt)));
            
            AccTradersReportData(iS,4) = num2cell(nansum(SettleData.settle_gamma_11(IdxSettleRpt)) + nansum(SettleData.settle_gamma_12(IdxSettleRpt)) + ...
                nansum(SettleData.settle_gamma_21(IdxSettleRpt)) + nansum(SettleData.settle_gamma_22(IdxSettleRpt)));
            AccTradersReportData(iS,5) = num2cell(nansum(SettleData.settle_theta(IdxSettleRpt)) * (7/5));
            AccTradersReportData(iS,6) = num2cell(nansum(SettleData.settle_vega_1(IdxSettleRpt)) + nansum(SettleData.settle_vega_2(IdxSettleRpt)));
            AccTradersReportData(iS,7) = num2cell(nansum(EODSettleData.Net_Total_USD(IdxEODData)));
            
            AccTradersReportData(iS,8) = num2cell(round(nansum(SettleData.active_lots(IdxSettleRpt) .* SettleData.settle_price(IdxSettleRpt) .* SettleData.mult_factor(IdxSettleRpt))),4);
            
            AccMktValue(iS) = num2cell(nansum(SettleData.mkt_value_usd(IdxSettleRpt)));
        else
            TradersReportData(iS,1) = subportfolio_name;
            TradersReportData(iS,2) = num2cell(nansum(TradersData.traders_delta_1(IdxTradersRpt)) + nansum(TradersData.traders_delta_2(IdxTradersRpt)));
            TradersReportData(iS,3) = num2cell(nansum(TradersData.traders_gamma_11(IdxTradersRpt)) + nansum(TradersData.traders_gamma_12(IdxTradersRpt)) + ...
                nansum(TradersData.traders_gamma_21(IdxTradersRpt)) + nansum(TradersData.traders_gamma_22(IdxTradersRpt)));
            TradersReportData(iS,4) = num2cell(nansum(TradersData.traders_theta(IdxTradersRpt)) * (7/5));
            TradersReportData(iS,5) = num2cell(nansum(TradersData.traders_vega_1(IdxTradersRpt)) + nansum(TradersData.traders_vega_2(IdxTradersRpt)));
            
            TradersReportData(iS,6) = num2cell(nansum(EODTradersData.Net_Total_USD(IdxEodTData)));
            TradersReportData(iS,8) = num2cell(nansum(EODSettleData.Net_Total_USD(IdxEODData)));
            
            TradersReportData(iS,7) = num2cell(cell2mat(TradersReportData(iS,8)) - cell2mat(TradersReportData(iS,6)));
            
            AccTradersReportData(iS,1) = subportfolio_name;
            AccTradersReportData(iS,2) = num2cell(nansum(SettleData.settle_delta_1(IdxSettleRpt)) + nansum(SettleData.settle_delta_2(IdxSettleRpt)));
            AccTradersReportData(iS,3) = num2cell(nansum(SettleData.settle_gamma_11(IdxSettleRpt)) + nansum(SettleData.settle_gamma_12(IdxSettleRpt)) + ...
                nansum(SettleData.settle_gamma_21(IdxSettleRpt)) + nansum(SettleData.settle_gamma_22(IdxSettleRpt)));
            AccTradersReportData(iS,4) = num2cell(nansum(SettleData.settle_theta(IdxSettleRpt)) * (7/5));
            AccTradersReportData(iS,5) = num2cell(nansum(SettleData.settle_vega_1(IdxSettleRpt)) + nansum(SettleData.settle_vega_2(IdxSettleRpt)));
            AccTradersReportData(iS,6) = num2cell(nansum(EODSettleData.Net_Total_USD(IdxEODData)));
            AccMktValue(iS) = num2cell(nansum(SettleData.mkt_value_usd(IdxSettleRpt)));
        end
    end
    
    if(strcmpi(InBUName,'cop'))
        TradersNominal_Sum = nansum(cell2mat(TradersReportData(:,10)));
        TradersReportData(:,11) = num2cell((cell2mat(TradersReportData(:,10)) ./ TradersNominal_Sum) .* 100) ;
        
        AccTradersNominal_Sum = nansum(cell2mat(AccTradersReportData(:,8)));
        AccTradersReportData(:,9) = num2cell((cell2mat(AccTradersReportData(:,8)) ./ AccTradersNominal_Sum) .* 100) ;
    end
    
    try
        % upload the data in traders and accounting traders report tables
        TableName = ['traders_report_data_table_',InBUName];
        TradersReportTableData = construct_report_table(TableName,TradersReportData,ObjDB);
        upload_in_database(TableName, TradersReportTableData);
        
        TableName = ['accounting_traders_report_data_table_',InBUName];
        AccTradersReportTableData = construct_report_table(TableName,AccTradersReportData,ObjDB);
        upload_in_database(TableName, AccTradersReportTableData);
    catch
        disp('error while updating the traders report table');
    end
    
    
    %     TableName = 'subportfolio_tr_format_table';
    TableName = 'tr_report_subportfolio_mapping_table';
    [ColNames,Data] =  read_from_database(TableName,0,'',InBUName);
    if strcmpi(Data,'No Data')
        OutErrorMsg = {'Unable to generate Traders Reports summary as the subportfolio is not defined in master table ''tr_report_subportfolio_mapping_table''!'};
        return
    end
    Data = sortrows(Data,1);
    PosSubportfolio = strcmpi('subportfolio',ColNames);
    TRSubportfolio = Data(:,PosSubportfolio);
    PosTRMapping = strcmpi('tr_mapping',ColNames);
    TRMappingName =  Data(:,PosTRMapping);
    PosTRSurface = strcmpi('surface',ColNames);
    TRSurface =  Data(:,PosTRSurface);
    PosOIDPortfolio = strcmpi('directional_portfolio',ColNames);
    OIDPortfolio =  Data(:,PosOIDPortfolio);
    
    NumRows = length(TRSubportfolio);
    if(strcmpi(InBUName,'cop'))
        TRFormatOutput = cell(NumRows,12);
    else
        TRFormatOutput = cell(NumRows,9);
    end
    for iOut = 1:NumRows
        Idx = find(strcmpi(TRSubportfolio{iOut},TradersReportData(:,1)));
        Surface = TRSurface{iOut};
        if ~isempty(Idx)
            TRFormatOutput(iOut,1) = TRMappingName(iOut);
            if strcmpi(Surface,'accounting')
                if(strcmpi(InBUName,'cop'))
                    IdxAcc = find(strcmpi(TRSubportfolio{iOut},AccTradersReportData(:,1)));
                    TRFormatOutput(iOut,2:end-3) = AccTradersReportData(IdxAcc,2:end); %#ok<FNDSB>
                    TRFormatOutput(iOut,end-2) = num2cell(0);
                    TRFormatOutput(iOut,end-4) = num2cell(0);
                    TRFormatOutput(iOut,end-3) = AccTradersReportData(IdxAcc,end-2);
                    TRFormatOutput(iOut,end-1) = AccTradersReportData(IdxAcc,end-1);
                    TRFormatOutput(iOut,end) = AccTradersReportData(IdxAcc,end);
                else
                    IdxAcc = find(strcmpi(TRSubportfolio{iOut},AccTradersReportData(:,1)));
                    TRFormatOutput(iOut,2:end-3) = AccTradersReportData(IdxAcc,2:end); %#ok<FNDSB>
                    TRFormatOutput(iOut,end-2) = num2cell(0);
                    TRFormatOutput(iOut,end-1) = AccTradersReportData(IdxAcc,end);
                    TRFormatOutput(iOut,end) = num2cell(0);
                    %                     TRFormatOutput(iOut,end) = AccTradersReportData(IdxAcc,end);
                end
            else
                TRFormatOutput(iOut,2:end-1) = TradersReportData(Idx,2:end);
                IdxAcc = find(strcmpi(OIDPortfolio{iOut},AccTradersReportData(:,1)));
                if ~isempty(IdxAcc)
                    TRFormatOutput(iOut,end) = AccTradersReportData(IdxAcc,end); %#ok<FNDSB>
                else
                    TRFormatOutput(iOut,end) = num2cell(0);
                end
            end
        else
            TRFormatOutput(iOut,1) = TRMappingName(iOut);
            TRFormatOutput(iOut,2:end) = num2cell(0);
        end
    end
    
    if(strcmpi(InBUName,'cop'))
        TradersReportHeader = {'subportfolio','delta','delta_MT','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','DIR','notional_in_dollar','notional_in_percentage'};
        AccTradersReportHeader = {'subportfolio','delta','delta_MT','gamma','theta','vega','mtm-usd','notional_in_dollar','notional_in_percentage'};
    else
        TradersReportHeader = {'subportfolio','delta','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','DIR'};
        AccTradersReportHeader = {'subportfolio','delta','gamma','theta','vega','mtm-usd'};
    end
    
    NumRows = length(TRSubportfolio);
    if(strcmpi(InBUName,'cop'))
        AccTRFormatOutput = cell(NumRows,9);
    else
        AccTRFormatOutput = cell(NumRows,6);
    end
    for iOut = 1:NumRows
        Idx = find(strcmpi(TRSubportfolio{iOut},AccTradersReportData(:,1)));
        if ~isempty(Idx)
            AccTRFormatOutput(iOut,1) = TRMappingName(iOut);
            AccTRFormatOutput(iOut,2:end) = AccTradersReportData(Idx,2:end);
        else
            AccTRFormatOutput(iOut,1) = TRMappingName(iOut);
            AccTRFormatOutput(iOut,2:end) = num2cell(0);
        end
    end
    
    %%
    if(strcmpi(InBUName,'cop'))
        UniqueFields = {'subportfolio'};
        SumFields = {'delta','delta_MT','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','DIR','notional_in_dollar','notional_in_percentage'};
        OutputFields = [UniqueFields,SumFields] ;
        WeightedAverageFields = [];
        [TradersReportHeader,TRFormatOutput] = consolidatedata(TradersReportHeader, TRFormatOutput,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        UniqueFields = {'subportfolio'};
        SumFields = {'delta','delta_MT','gamma','theta','vega','mtm-usd','notional_in_dollar','notional_in_percentage'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [AccTradersReportHeader,AccTRFormatOutput] = consolidatedata(AccTradersReportHeader, AccTRFormatOutput,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    else
        UniqueFields = {'subportfolio'};
        SumFields = {'delta','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','DIR'};
        OutputFields = {'subportfolio','delta','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','DIR'} ;
        WeightedAverageFields = [];
        [TradersReportHeader,TRFormatOutput] = consolidatedata(TradersReportHeader, TRFormatOutput,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        UniqueFields = {'subportfolio'};
        SumFields = {'delta','gamma','theta','vega','mtm-usd'};
        OutputFields = {'subportfolio','delta','gamma','theta','vega','mtm-usd'} ;
        WeightedAverageFields = [];
        [AccTradersReportHeader,AccTRFormatOutput] = consolidatedata(AccTradersReportHeader, AccTRFormatOutput,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    end
    
    %%
    try
        %% Brokerage Table Data - Old Logic
        %         if(strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg'))
        %             Value_Date = fetch(ObjDB,'select value_date from valuation_date_table');
        %             Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
        %
        %             ViewName = ['brokerage_numbers_table_',InBUName];
        %             for c = 1 : 2
        %                 if(c == 1)
        %                     SqlQuery_1 = ['select * from ',ViewName,' where value_date = ''',char(Value_Date),''' '];
        %                 else
        %                     SqlQuery_1 = ['select * from ',ViewName,' where value_date = ''',char(Settle_Date),''' '];
        %                 end
        %
        %                 [Brokerage_ColNames,Brokerage_Tbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
        %
        %                 if(~strcmpi(Brokerage_Tbl_Data,'No Data'))
        %                     Brokerage_Data_Check = 1;
        %                     break;
        %                 else
        %                     Brokerage_Data_Check = 0;
        %                     continue;
        %                 end
        %
        %             end
        %         end
        
        %% Brokerage Table Data -  New Logic
        
        if(strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg'))
            ViewName = ['helper_brokerage_sum_numbers_view_',InBUName];
            [Brokerage_ColNames,Brokerage_Tbl_Data] = Fetch_DB_Data(ObjDB,[],ViewName);
            
            if(~strcmpi(Brokerage_Tbl_Data,'No Data'))
                Brokerage_Data_Check = 1;
            else
                Brokerage_Data_Check = 0;
                OutErrorMsg = {['No Data ',char(ViewName)]} ;
            end
            
        end
        
        %%
        Pos_mtm = strcmpi('mtm-usd',TradersReportHeader);
        Pos_DIR = strcmpi('DIR',TradersReportHeader);
        Pos_subportfolio = strcmpi('subportfolio',TradersReportHeader);
        
        if( (strcmpi(InBUName,'agf') || strcmpi(InBUName,'usg')) && Brokerage_Data_Check )
            Brokerage_Tbl_Data = cell2dataset([Brokerage_ColNames;Brokerage_Tbl_Data]);
            Brokerage_Data = cell(size(TRFormatOutput,1),1);
            
            for k = 1 : size(TRFormatOutput,1)
                Current_Portfolio = TRFormatOutput(k,Pos_subportfolio);
                
                Matched_Index = cellStrfind_exact(Brokerage_Tbl_Data.subportfolio,Current_Portfolio);
                if(~isempty(Matched_Index))
                    Brokerage_Data(k,1) = num2cell( Brokerage_Tbl_Data.brokerage(Matched_Index));
                else
                    Brokerage_Data(k,1) = {0};
                end
            end
            
            Total_mtm = cell2mat(TRFormatOutput(:,Pos_mtm)) + cell2mat(TRFormatOutput(:,Pos_DIR)) ;%% + cell2mat(Brokerage_Data);
            TmpValueDate = repmat(cellstr(ValuationDate),size(Total_mtm));
            DrawdownData = [TmpValueDate,TRFormatOutput(:,Pos_subportfolio),num2cell(Total_mtm)];
            
            TradersReportHeader = [TradersReportHeader,{'Brokerage'}];
            TRFormatOutput = [TRFormatOutput,Brokerage_Data];
            
        else
            Total_mtm = cell2mat(TRFormatOutput(:,Pos_mtm)) + cell2mat(TRFormatOutput(:,Pos_DIR));
            TmpValueDate = repmat(cellstr(ValuationDate),size(Total_mtm));
            DrawdownData = [TmpValueDate,TRFormatOutput(:,Pos_subportfolio),num2cell(Total_mtm)];
        end
        
        
        %       TableName = 'accounting_tr_drawdown_table';
        TableName = ['accounting_tr_drawdown_table_',char(InBUName)];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, DrawdownData);
        OutDrawdownData = PnL_Calculation_TR_Reports(InBUName);
        
    catch ME
        disp('Error while calculating and uploading data for drawdown table');
        disp(ME.message);
    end
    
    try
        OIDPortfolios = unique(OIDPortfolio);
        OIDPortfolios(cellfun(@isempty,OIDPortfolios)) = [];
        
        NumRows = length(OIDPortfolios);
        if(strcmpi(InBUName,'cop'))
            OIDAccTRFormatOutput = cell(NumRows,10);
        else
            OIDAccTRFormatOutput = cell(NumRows,7);
        end
        for iOut = 1:NumRows
            Idx = find(strcmpi(OIDPortfolios{iOut},AccTradersReportData(:,1)));
            if ~isempty(Idx)
                OIDAccTRFormatOutput(iOut,1) = OIDPortfolios(iOut);
                OIDAccTRFormatOutput(iOut,2:end-1) = AccTradersReportData(Idx,2:end);
                OIDAccTRFormatOutput(iOut,end) = AccMktValue(Idx);
            else
                OIDAccTRFormatOutput(iOut,1) = OIDPortfolios(iOut);
                OIDAccTRFormatOutput(iOut,2:end-1) = num2cell(0);
                OIDAccTRFormatOutput(iOut,end) = num2cell(0);
            end
        end
    catch ME
        disp('Error while calculating OID Traders Report');
        disp(ME.message);
    end
    
    % Calculate ARB Book report
    % identify the ARB Portfolios data
    try
        TRPortfolio = SettleData.portfolio;
        IdxARBPF = strcmpi(cellfun(@(x) x(end-2:end), TRPortfolio, 'UniformOutput', false),'ARB');
        ARBData = [SettleData.portfolio(IdxARBPF,:),SettleData.subportfolio(IdxARBPF,:),...
            SettleData.product_code(IdxARBPF,:),num2cell(SettleData.settle_delta_1(IdxARBPF,:)),...
            num2cell(SettleData.settle_gamma_11(IdxARBPF,:)),num2cell(SettleData.settle_vega_1(IdxARBPF,:)),...
            num2cell(SettleData.settle_theta(IdxARBPF,:)),num2cell(SettleData.mtm_usd(IdxARBPF,:))];
        
        UniqueFields = {'portfolio','subportfolio','product_code'};
        SumFields = {'settle_delta_1','settle_gamma_11',...
            'settle_vega_1','settle_theta','mtm_usd'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [ARBReportHeader,OutARBData] = consolidatedata(OutputFields, ARBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    catch ME
        disp('Error while calculating ARB Traders Report');
        disp(ME.message);
    end
    
    TradersReportFilename = getXLSFilename(['Traders_Report_',InBUName]);
    xlswrite(TradersReportFilename,[TradersReportHeader;TRFormatOutput],'Traders Report');
    xlswrite(TradersReportFilename,[AccTradersReportHeader;AccTRFormatOutput],'Accounting Traders Report');
    if ~isempty(OIDAccTRFormatOutput)
        AccTradersReportHeader = [AccTradersReportHeader,'MarketValue'];
        xlswrite(TradersReportFilename,[AccTradersReportHeader;OIDAccTRFormatOutput],'OIDBook Traders Report');
    end
    if ~isempty(OutARBData)
        xlswrite(TradersReportFilename,[ARBReportHeader;OutARBData],'ARBBook Traders Report');
    end
    if ~isempty(OutVARData)
        xlswrite(TradersReportFilename,OutVARData,'VAR Report');
    end
    if ~isempty(OutDrawdownData)
        xlswrite(TradersReportFilename,OutDrawdownData,'Drawdown Report');
    end
    
    try
        OutXLSFileName = fullfile(pwd,TradersReportFilename);
        xls_delete_sheets(OutXLSFileName);
    catch
    end
    
    
    if isconnection(ObjDB)
        close(ObjDB);
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
    function OutTableData = construct_report_table(TableName,InData,ObjDB)
        SqlQuery = ['select max(version_no) from ',TableName,' where value_date = ''',ValuationDate,''''];
        Last_version_no = cell2mat(fetch(ObjDB,SqlQuery));
        if isnan(Last_version_no)
            Current_Ver_no = 0;
        else
            Current_Ver_no = Last_version_no + 1;
        end
        Current_Ver_no = num2cell(Current_Ver_no);
        Current_User = cellstr(ObjDB.UserName);
        TimeStamp = cellstr(getCurrentDateTimestamp);
        
        ValueDate  = repmat({ValuationDate},size(Subportfolio));
        VersionNo = repmat(Current_Ver_no,size(Subportfolio));
        VersionCreatedBy = repmat(Current_User,size(Subportfolio));
        VersionTimestamp = repmat(TimeStamp,size(Subportfolio));
        
        OutTableData = [ValueDate,InData,VersionNo,VersionCreatedBy,VersionTimestamp];
        
    end


end

