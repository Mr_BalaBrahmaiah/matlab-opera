function mail_all_rmo_reports

[FuturesReportFilename,OptionReportFilename,FxReportFilename,TradeQueryReportFilename,OTCReport] = generate_all_riskoffice_reports;

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

try
    configure_mail_settings;
    sendmail({'rajasekaran.v@olamnet.com','sirajahmed.s@olamnet.com','chithambara.t@olamnet.com','manikandan.h@olamnet.com',...
        'seshadri.ramanujam@olamnet.com','renjumon.gk@olamnet.com','Raghavendra.Sn@olamnet.com'}, ...
        'Risk Office Reports', ['Attached are the Risk office reports for COB ''',SettlementDate,''''],{FuturesReportFilename,OptionReportFilename,FxReportFilename,TradeQueryReportFilename,OTCReport});
catch
end

pause(5);

try
    [~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
    ReportNames = ConfigFileData(2:end,1);
    OutputFolders = ConfigFileData(2:end,2);
    IdxFound = strcmpi('RiskOfficeReports',ReportNames);
    DestinationFolder = OutputFolders{IdxFound};
    
    LogFilename = ['DB_automation_log_',datestr(today),'.txt'];
    
    try
        TempDestinationFolder = [DestinationFolder,'\Futures Report'];
        OutFuturesReportFilename = fullfile(TempDestinationFolder,FuturesReportFilename);
        move_reports(FuturesReportFilename,OutFuturesReportFilename,'Risk Office Futures Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving Futures Report under Risk Office Reports!');
    end
    
    try
        TempDestinationFolder = [DestinationFolder,'\Options Report'];
        OutOptionReportFilename = fullfile(TempDestinationFolder,OptionReportFilename);
        move_reports(OptionReportFilename,OutOptionReportFilename,'Risk Office Options Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving Options Report under Risk Office Reports!');
    end
    
    try
        TempDestinationFolder = [DestinationFolder,'\FX Report'];
        OutFxReportFilename = fullfile(TempDestinationFolder,FxReportFilename);
        move_reports(FxReportFilename,OutFxReportFilename,'Risk Office FX Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving FX Report under Risk Office Reports!');
    end
    
    try
        TempDestinationFolder = [DestinationFolder,'\Trade Query Report'];
        OutTradeQueryReportFilename = fullfile(TempDestinationFolder,TradeQueryReportFilename);
        move_reports(TradeQueryReportFilename,OutTradeQueryReportFilename,'Risk Office Trade Query Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving Trade Query Report under Risk Office Reports!');
    end
    
    try
        TempDestinationFolder = [DestinationFolder,'\OTC Report'];
        OutOTCReport = fullfile(TempDestinationFolder,OTCReport);
        move_reports(OTCReport,OutOTCReport,'Risk Office OTC Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving OTC Report under Risk Office Reports!');
    end
catch
    
end