function [OutErrorMsg,OutFilename] = generate_qsvtbook_report(InBUName)
% to generate the qsvt book report format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/29 10:11:23 $
%  $Revision: 1.7 $
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

ViewName = 'helper_qsvt_book_report_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

IsDataNotFound = ~iscell(SettleData) && (isequal(SettleData,0) || ...
        strcmpi(SettleData,'No Data'));
    
if IsDataNotFound
    OutErrorMsg = {'No Data found for QSVTBook Report!'};
    return;
end

% change the product code of Rubber to OR; since it is currently OR-EUR or
% OR-USD
PosProdCode = strcmpi('product_code',SettleColNames);
IdxOR = strncmpi(SettleData(:,PosProdCode),'OR',2);
SettleData(IdxOR,PosProdCode) = {'OR'};

PosDealStatus = strcmpi('deal_status',SettleColNames);
IdxLive = strcmpi('live',SettleData(:,PosDealStatus));
IdxDead = ~IdxLive;

Realised           = zeros(size(IdxDead));
UnrealisedPremium  = zeros(size(IdxDead));
UnrealisedMktValue = zeros(size(IdxDead));

PosAccValue    = strcmpi('acc_value',SettleColNames);
PosMktValue    = strcmpi('mkt_value',SettleColNames);
PosPremiumPaid = strcmpi('premium_paid',SettleColNames);

Realised(IdxDead)           = cell2mat(SettleData(IdxDead,PosAccValue));
UnrealisedPremium(IdxLive)  = cell2mat(SettleData(IdxLive,PosPremiumPaid));
UnrealisedMktValue(IdxLive) = cell2mat(SettleData(IdxLive,PosMktValue));

SettleColNames = [SettleColNames,'realised','unrealised_premium','unrealised_mkt_value'];
SettleData     = [SettleData,num2cell(Realised),num2cell(UnrealisedPremium),num2cell(UnrealisedMktValue)];

UniqueFields = {'portfolio','subportfolio','product_code','contract_month'};
SumFields = {'settle_delta_1','settle_gamma_11',...             
             'settle_theta','settle_vega_1','realised','unrealised_premium','unrealised_mkt_value','acc_value'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
try
    OutData = sortrows(OutData,[1 2 3]);
catch
    disp('Error while sorting rows: some NaN values are found');
end

OutputFields = strrep(OutputFields,'settle_delta_1','delta');
OutputFields = strrep(OutputFields,'settle_gamma_11','gamma');
OutputFields = strrep(OutputFields,'settle_vega_1','vega');
OutputFields = strrep(OutputFields,'settle_theta','theta');

try
    [ColNames,Data] = read_from_database('valuation_date_table',0);
    RefDate = cell2dataset([ColNames;Data]);
    SettlementDate = RefDate.settlement_date{1};
    ValueDate = RefDate.value_date{1};
    
    PosPortfolio = strcmpi('portfolio',OutputFields);
    PosSubportfolio = strcmpi('subportfolio',OutputFields);
    PosProductCode = strcmpi('product_code',OutputFields);
    PosContractMonth = strcmpi('contract_month',OutputFields);
    PosAccValue = strcmpi('acc_value',OutputFields);
    
    [NumRows,~] = size(OutData);
    pnl_for_the_day = OutData(:,PosAccValue);
    
    TableName = ['qsvt_report_table_',InBUName];            
    SqlQuery = ['select * from ',TableName,' where value_date = ''',SettlementDate,''''];
    [ColNames,Data] = read_from_database(TableName,0,SqlQuery);
    if ~strcmpi(Data,'No Data')
        RefDirData = cell2dataset([ColNames;Data]);
        
        for iR = 1:NumRows
            IdxRow = strcmpi(OutData(iR,PosPortfolio),RefDirData.portfolio) & ...
                strcmpi(OutData(iR,PosSubportfolio),RefDirData.subportfolio) & ...
                strcmpi(OutData(iR,PosProductCode),RefDirData.product_code) & ...
                strcmpi(OutData(iR,PosContractMonth),RefDirData.contract_month) ;
            if isempty(find(IdxRow)) % if a similar row data does not exist for the previous day due to a product for which contract month is null like USDUSD
                IdxRow = strcmpi(OutData(iR,PosPortfolio),RefDirData.portfolio) & ...
                strcmpi(OutData(iR,PosSubportfolio),RefDirData.subportfolio) & ...
                strcmpi(OutData(iR,PosProductCode),RefDirData.product_code);
            end
            
            if isempty(find(IdxRow)) % if the similar row data does not exist for the previous day
                pnl_for_the_day(iR) = OutData(iR,PosAccValue);
            else
                pnl_for_the_day(iR) = num2cell(cell2mat(OutData(iR,PosAccValue)) - RefDirData.acc_value(IdxRow));
            end
        end
    end
    ValuationDate = repmat({ValueDate},NumRows,1);
    OutputFields = [OutputFields,'pnl_for_the_day'];
    OutData = [OutData,pnl_for_the_day];
    
    DirReportTableData = [ValuationDate,OutData];
    TableName = 'qsvt_report_table';    
    SqlQuery = ['delete from ',TableName,' where value_date =''',ValueDate,''''];
    [ColNames,Data] = read_from_database(TableName,0,SqlQuery);
    upload_in_database(TableName, DirReportTableData);
catch
    disp('error while calculating and uploading qsvt report!');
end

OutFilename = getXLSFilename('QSVTBookReport');
xlswrite(OutFilename,[OutputFields;OutData],'QSVTReportSummary');

%% generate the live report view
try

LiveData = SettleData(IdxLive,:);
PosDerivativeType = strcmpi('derivative_type',SettleColNames);
LiveData(:,PosDerivativeType) = lower(LiveData(:,PosDerivativeType));
IdxCall = cellStrfind(LiveData(:,PosDerivativeType),'call');
LiveData(IdxCall,PosDerivativeType) = {'call'};
IdxPut = cellStrfind(LiveData(:,PosDerivativeType),'put');
LiveData(IdxPut,PosDerivativeType) = {'put'};

UniqueFields = {'portfolio','subportfolio','contract_number','product_code','contract_month','strike','derivative_type','settle_price'};
SumFields = {'active_lots'};
OutputFields = {'portfolio','subportfolio','contract_number','product_code','active_lots','contract_month','strike','derivative_type','settle_price'};
WeightedAverageFields = [];
[NewOutputFields,NewOutData] = consolidatedata(SettleColNames, LiveData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
ActiveLots = cell2mat(NewOutData(:,5));
IdxLots = (ActiveLots == 0); % to remove the net zero position
NewOutData(IdxLots,:) = [];

try
    NewOutData = sortrows(NewOutData,[1,2,4,6]);
catch
    disp('Error while sorting rows: some NaN values are found');
end

NewOutputFields = strrep(NewOutputFields,'active_lots','LOTS');
NewOutputFields = strrep(NewOutputFields,'derivative_type','CALL/PUT/FUT');
NewOutputFields = strrep(NewOutputFields,'settle_price','SETTLE');
NewOutputFields = strrep(NewOutputFields,'contract_number','TID');
NewOutputFields = upper(NewOutputFields);

xlswrite(OutFilename,[NewOutputFields;NewOutData],'LIVE QSVT Report');
catch
    disp('Error while generating Live QSVT Report!');
end

UniqueFields = {'transaction_date','portfolio','subportfolio','contract_number',...
    'product_code','contract_month','strike','derivative_type','current_premium',...
    'maturity_date','settle_price'};
SumFields = {'active_lots','acc_value','acc_value_nc'};
OutputFields = {'transaction_date','portfolio','subportfolio','contract_number',...
    'product_code','active_lots','contract_month','strike','derivative_type',...
    'current_premium','maturity_date','settle_price','acc_value','acc_value_nc'};
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
try
    OutData = sortrows(OutData,[2 3 5 6 8]);
catch
    disp('Error while sorting rows: some NaN values are found');
end
OutputFields = strrep(OutputFields,'contract_number','tid');
OutputFields = strrep(OutputFields,'acc_value','mtm-usd');
OutputFields = strrep(OutputFields,'acc_value_nc','mtm-nc');

xlswrite(OutFilename,[OutputFields;OutData],'Pnp Deal Dump');

try
    xls_delete_sheets(fullfile(pwd,OutFilename));
catch
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
