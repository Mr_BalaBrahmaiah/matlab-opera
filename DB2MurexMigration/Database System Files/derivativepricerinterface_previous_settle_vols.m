function [OutErrorMsg,OutFileName] = derivativepricerinterface_previous_settle_vols(InBUName,PricingType,varargin)
%  PricingType should be 'settle' or 'traders'
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/18 10:51:58 $
%  $Revision: 1.11 $
%

OutErrorMsg = {'No errors'};
OutFileName = '';

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

IsUploadToDB = 0; 
if nargin == 3
    if ~isempty(varargin{1})        
        IsUploadToDB = 1;           
    end  
    DBUsername = 'user';
end
if nargin == 4
    if ~isempty(varargin{1})        
        IsUploadToDB = 1;           
    end   
    if ~isempty(varargin{2})        
        DBUsername = varargin{2};    
    else
        DBUsername = 'user';
    end   
end

if strcmpi(PricingType,'settle')
    ViewName = 'input_security_settlement_value_previous_settle_vols_view';
else
    ViewName = 'input_security_traders_value_view';
end

% Read the values from database for derivative pricer
[Inputheading,Inputmatrix] = read_from_database(ViewName,0,'',InBUName);

Outputheading = {'Security_Id','Value_Date','Price','Delta_1','Delta_2','Gamma_11','Gamma_12','Gamma_21','Gamma_22','Vega_1','Vega_2','Theta','Chi','Curr_Exp'};

if strcmpi(PricingType,'traders')
    Outputheading = ['Subportfolio',Outputheading];
end
    
if isequal(Inputheading,{[]}) % no data found in database for the selected criteria
    disp('No data found in database for the selected criteria!');
    OutFileName = '';
else
    [OutErrorMsg,OutData] = derivativepricer_interpvols_previous_settle_vols(InBUName,Inputmatrix, Inputheading, Outputheading,PricingType);
    Pos_ValueDate    = strcmpi('value_date',Inputheading);
    ValueDate    = Inputmatrix{1,Pos_ValueDate};
           
    if strcmpi(PricingType,'settle')
%         OutFileName =  ['Security_settlement_value_table_',ValueDate,'.csv'];       
%         fid = fopen(OutFileName,'wt');
%         fprintf(fid, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n', Outputheading{1,:});
%         for i =1:size(OutData,1)
%             fprintf(fid, '%s,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n', OutData{i,:});
%         end
%         fclose(fid);
        TableName = ['security_settlement_value_previous_vol_table_',InBUName];
%       OutFileName =  'Security_settlement_value_previous_vol_table.csv';
        OutFileName =  [TableName,'.csv'];
        customcsvwrite(OutFileName,OutData,Outputheading);
        
    else
%         OutFileName =  ['Security_traders_value_table_',ValueDate,'.csv'];
%         fid = fopen(OutFileName,'wt');
%         fprintf(fid, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n', Outputheading{1,:});
%         for i =1:size(OutData,1)
%             fprintf(fid, '%s,%s,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n', OutData{i,:});
%         end
%         fclose(fid);
        TableName = 'security_traders_value_table';
%       OutFileName =  'Security_traders_value_table.csv';
        OutFileName =  [TableName,'.csv'];
        customcsvwrite(OutFileName,OutData,Outputheading);
     
    end
    if IsUploadToDB
        try
            objDB = connect_to_database;
%             SqlQuery = ['select count(*) from ',TableName,' where value_date = ''',ValueDate,''''];
%             NumRecords = cell2mat(fetch(objDB,SqlQuery));
%             if NumRecords ~= 0
                set(objDB,'AutoCommit','off');
%               SqlQuery = ['delete from ',TableName,' where value_date = ''',ValueDate,''''];
                SqlQuery = ['delete from ',TableName];
                curs = exec(objDB,SqlQuery);
                commit(objDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(objDB,'AutoCommit','on');
                if isconnection(objDB)
                    close(objDB);
                end
%             end
        catch ME
            disp(ME.message);
            if isconnection(objDB)
                close(objDB);
            end
        end
        upload_in_database(TableName, OutData);
        % set the data in status_table
        try
            objDB = connect_to_database;
            DBStatusTables = fetch(objDB,['select tablename from status_table where valuation_date = ''',ValueDate,'''']);     
            DBTimeStamp = getCurrentDateTimestamp;
            DBTablename = 'status_table';
            if ismember(TableName,DBStatusTables) % if the tablename exists in the statustable, just update it
                WhereClause = ['where tablename = ''',TableName,''' and valuation_date = ''',ValueDate,''''];
                TableFields = {'status_type','status','updated_by','updated_timestamp'};
                TableValues = {'update_data','true',DBUsername,DBTimeStamp};
                update(objDB,DBTablename,TableFields,TableValues,WhereClause);
            else % if the tablename does not exist in statustable, add it as a new entry
                 TableFields = {'id','tablename','valuation_date','status_type','status','updated_by','updated_timestamp'};
                 ID = fetch(objDB,'select max(id) from status_table');
                 ID =  cell2mat(ID) + 1;
                 TableValues = {ID,TableName,ValueDate,'update_data','true',DBUsername,DBTimeStamp};
                 insert(objDB,DBTablename,TableFields,TableValues);
            end        
            if isconnection(objDB)
                close(objDB);
            end
        catch ME
            disp(ME.message);
            if isconnection(objDB)
                close(objDB);
            end
        end

    end
end

catch ME
    OutErrorMsg = cellstr(ME.message);
end
end