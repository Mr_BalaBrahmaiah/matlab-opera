function varargout = weightedaverageinputsgui(varargin)
% WEIGHTEDAVERAGEINPUTSGUI MATLAB code for weightedaverageinputsgui.fig
%      WEIGHTEDAVERAGEINPUTSGUI, by itself, creates a new WEIGHTEDAVERAGEINPUTSGUI or raises the existing
%      singleton*.
%
%      H = WEIGHTEDAVERAGEINPUTSGUI returns the handle to a new WEIGHTEDAVERAGEINPUTSGUI or the handle to
%      the existing singleton*.
%
%      WEIGHTEDAVERAGEINPUTSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WEIGHTEDAVERAGEINPUTSGUI.M with the given input arguments.
%
%      WEIGHTEDAVERAGEINPUTSGUI('Property','Value',...) creates a new WEIGHTEDAVERAGEINPUTSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before weightedaverageinputsgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to weightedaverageinputsgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help weightedaverageinputsgui

% Last Modified by GUIDE v2.5 08-Jul-2013 12:36:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @weightedaverageinputsgui_OpeningFcn, ...
                   'gui_OutputFcn',  @weightedaverageinputsgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before weightedaverageinputsgui is made visible.
function weightedaverageinputsgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to weightedaverageinputsgui (see VARARGIN)

% Choose default command line output for weightedaverageinputsgui
handles.output = [];
NumInputs = nargin-3;

if ~isempty(varargin)
    if ~iscellstr(varargin{1})
        varargin{1} = cellstr(varargin{1});
    end
    handles.InputFields = varargin{1};
    if NumInputs == 2
        Data = varargin{2};
    else
        Data = cell(1,2);
        Data(1,:) = cellstr('');
    end
    set(handles.tblWghtdAvg,'data',Data);
    handles.isRemove = 0;
else
    errordlg('No input fields are given!');
    delete(handles.figWghtdAvg);
    return;
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes weightedaverageinputsgui wait for user response (see UIRESUME)
uiwait(handles.figWghtdAvg);


% --- Outputs from this function are returned to the command line.
function varargout = weightedaverageinputsgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if isfield(handles,'output')
    varargout{1} = handles.output;
    delete(handles.figWghtdAvg);
    return;
else
    varargout{1} = [];
end
    


% --- Executes on button press in pbOK.
function pbOK_Callback(hObject, eventdata, handles)
% hObject    handle to pbOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data = get(handles.tblWghtdAvg,'data');

if any(cellfun(@isempty,Data(:,1)))
    errordlg('Values cannot be empty! Please enter valid values.');
    return;
end

if any(cellfun(@isempty,Data(:,2)))
    errordlg('Weights cannot be empty! Please enter valid weights.');
    return;
end 

handles.output = Data;
guidata(hObject, handles);
uiresume(handles.figWghtdAvg);

% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figWghtdAvg);


% --- Executes when user attempts to close figWghtdAvg.
function figWghtdAvg_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figWghtdAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uiresume(handles.figWghtdAvg);


% --- Executes when selected cell(s) is changed in tblWghtdAvg.
function tblWghtdAvg_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblWghtdAvg (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

Data = get(handles.tblWghtdAvg,'data');
Idx = eventdata.Indices;
if ~isempty(Idx)
    IdxRow = Idx(1);
    IdxCol = Idx(2);
    if handles.isRemove        
        handles.isRemove = 0;
        Reply = questdlg('Are you sure you want to remove the selected value/weight?','Confirmation to delete rows','yes','no','no');
        if strcmpi(Reply,'yes')
           Data(IdxRow,:)  =[];
        else
           return;
        end
    else     
        if IdxCol == 1
            Values = selectinputs('Select the fields to be considered for Values',...
                'Single',handles.InputFields,'User Inputs Selection');
            if ~isempty(Values)
                Data(IdxRow,IdxCol) = Values;
            end
        elseif IdxCol ==2
            Weights = selectinputs('Select the fields to be considered for Weights',...
                'Single',handles.InputFields,'User Inputs Selection');
            if ~isempty(Weights)
                Data(IdxRow,IdxCol) = Weights;
            end
        end
    end
    set(handles.tblWghtdAvg,'data',Data);
end
guidata(hObject, handles);

function SelectedInputs = selectinputs(PromptString,SelectionMode,ListString,Name)

[SelIdx,IsSelected] = listdlg('PromptString',PromptString,...
    'SelectionMode',SelectionMode,...
    'ListString',ListString,...
    'ListSize',[300 300],...
    'Name',Name);
if IsSelected
    SelectedInputs = ListString(SelIdx);
else
    SelectedInputs = '';
end


% --- Executes on button press in pbInsert.
function pbInsert_Callback(hObject, eventdata, handles)
% hObject    handle to pbInsert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data = get(handles.tblWghtdAvg,'data');
Temp = cell(1,2);
Temp(1,:) = cellstr('');
Data = [Data;Temp];
set(handles.tblWghtdAvg,'data',Data);
guidata(hObject, handles);


% --- Executes on button press in pbRemove.
function pbRemove_Callback(hObject, eventdata, handles)
% hObject    handle to pbRemove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.isRemove = 1;
guidata(hObject, handles);
