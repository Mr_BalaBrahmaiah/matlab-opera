function OutValues = getvalidarray(InValues)
% To handle the below errors in numeric array inputs
% EmptyValue
% ErrNull       #NULL!       -2146826288
% ErrDiv0       #DIV/0!      -2146826281
% ErrValue      #VALUE!      -2146826273
% ErrRef        #REF!        -2146826265
% ErrName       #NAME?       -2146826259
% ErrNum        #NUM!        -2146826252
% ErrNA         #N/A         -2146826246

  
    if iscolumn(InValues)
        isCol = 1;
    else
        isCol = 0;
    end
    
    if iscell(InValues)
        IdxEmpty = cellfun('isempty',InValues);
        InValues(IdxEmpty) = num2cell(NaN); %% {''} ; %%num2cell(NaN); 
        t = [InValues{:}];
    else
        IdxEmpty = arrayfun(@isnan,InValues);
        InValues(IdxEmpty) = NaN;
        t = InValues;
    end
    
    if ischar(t)
        if isempty(InValues)
            OutValues = '';
            return;
        end
        OutValues = InValues;
        IdxNum = cellfun(@isnumeric,InValues);
        if any(IdxNum)
            OutValues(IdxNum) = cellstr(' ');
        end
    else
        if isempty(InValues)
            OutValues = NaN;
            return;
        end
        try
            IdxOut = IdxEmpty ;%| t==-2146826288 | t==-2146826281 | t==-2146826273 | t==-2146826265 | t==-2146826259 | t==-2146826252 | t==-2146826246;
        catch
            IdxOut = IdxEmpty' ;%| t==-2146826288 | t==-2146826281 | t==-2146826273 | t==-2146826265 | t==-2146826259 | t==-2146826252 | t==-2146826246;
        end
        
        if iscell(InValues)
            InValues(IdxOut) = num2cell(NaN);
            OutValues = cell2mat(InValues);
        else
            InValues(IdxOut) = NaN;
            OutValues = InValues;
        end
        
        if isCol && isrow(OutValues)
            OutValues = OutValues';
        end
        
        if ~isCol && iscolumn(OutValues)
            OutValues = OutValues';
        end
    end

end