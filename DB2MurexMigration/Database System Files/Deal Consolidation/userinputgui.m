function varargout = userinputgui(varargin)
% USERINPUTGUI MATLAB code for userinputgui.fig
%      USERINPUTGUI, by itself, creates a new USERINPUTGUI or raises the existing
%      singleton*.
%
%      H = USERINPUTGUI returns the handle to a new USERINPUTGUI or the handle to
%      the existing singleton*.
%
%      USERINPUTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in USERINPUTGUI.M with the given input arguments.
%
%      USERINPUTGUI('Property','Value',...) creates a new USERINPUTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before userinputgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to userinputgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help userinputgui

% Last Modified by GUIDE v2.5 05-Jul-2013 13:02:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @userinputgui_OpeningFcn, ...
                   'gui_OutputFcn',  @userinputgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before userinputgui is made visible.
function userinputgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to userinputgui (see VARARGIN)

handles.output = [];

if ~isempty(varargin)
    if ~iscellstr(varargin{1})
        handles.UserInputFields = cellstr(varargin{1});
    else
        handles.UserInputFields = varargin{1};
    end
    handles.OutputFields = handles.UserInputFields;
    handles.UniqueFields = '';
    handles.SumFields    = '';
    handles.WAFields     = '';
else 
    errordlg('No input fields are given!');
    delete(handles.figUserInputs);
    return;
end
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes userinputgui wait for user response (see UIRESUME)
uiwait(handles.figUserInputs);


% --- Outputs from this function are returned to the command line.
function varargout = userinputgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if isfield(handles,'output')   
    varargout{1} = handles.output;       
    delete(handles.figUserInputs);
    return;
else
    varargout{1} = [];
end


function edtOutput_Callback(hObject, eventdata, handles)
% hObject    handle to edtOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtOutput as text
%        str2double(get(hObject,'String')) returns contents of edtOutput as a double

% --- Executes during object creation, after setting all properties.
function edtOutput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbOutput.
function pbOutput_Callback(hObject, eventdata, handles)
% hObject    handle to pbOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Idx = find(ismember(handles.UserInputFields,handles.OutputFields));
TmpOutput = selectinputs('Select the fields to be considered for Output',...
    'Multiple',handles.UserInputFields,'User Inputs Selection',Idx);
if ~isempty(TmpOutput)
    OutputFields = outputorderselectiongui(TmpOutput);
    handles.OutputFields = OutputFields;
    set(handles.edtOutput,'String',handles.OutputFields);
end
guidata(hObject,handles);

function edtUniqueFields_Callback(hObject, eventdata, handles)
% hObject    handle to edtUniqueFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtUniqueFields as text
%        str2double(get(hObject,'String')) returns contents of edtUniqueFields as a double


% --- Executes during object creation, after setting all properties.
function edtUniqueFields_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtUniqueFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbUniqueFields.
function pbUniqueFields_Callback(hObject, eventdata, handles)
% hObject    handle to pbUniqueFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Idx = find(ismember(handles.OutputFields,handles.UniqueFields));
UniqueFields = selectinputs('Select the fields to be considered as Unique fields',...
    'Multiple',handles.OutputFields,'User Inputs Selection',Idx);
if ~isempty(UniqueFields)
    handles.UniqueFields = UniqueFields;
    set(handles.edtUniqueFields,'String',handles.UniqueFields);
end
guidata(hObject,handles);


function edtSum_Callback(hObject, eventdata, handles)
% hObject    handle to edtSum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtSum as text
%        str2double(get(hObject,'String')) returns contents of edtSum as a double


% --- Executes during object creation, after setting all properties.
function edtSum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtSum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbSum.
function pbSum_Callback(hObject, eventdata, handles)
% hObject    handle to pbSum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Idx = find(ismember(handles.OutputFields,handles.SumFields));
SumFields = selectinputs('Select the fields to be considered for Sum',...
    'Multiple',handles.OutputFields,'User Inputs Selection',Idx);
if ~isempty(SumFields)
    handles.SumFields = SumFields;
    set(handles.edtSum,'String',handles.SumFields);
end
guidata(hObject,handles);


function edtWghtdAvg_Callback(hObject, eventdata, handles)
% hObject    handle to edtWghtdAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtWghtdAvg as text
%        str2double(get(hObject,'String')) returns contents of edtWghtdAvg as a double


% --- Executes during object creation, after setting all properties.
function edtWghtdAvg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtWghtdAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbWghtdAvg.
function pbWghtdAvg_Callback(hObject, eventdata, handles)
% hObject    handle to pbWghtdAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isempty(handles.WAFields)
    WAFields = weightedaverageinputsgui(handles.OutputFields);
else
    WAFields = weightedaverageinputsgui(handles.OutputFields, handles.WAFields);
end

if ~isempty(WAFields)
    handles.WAFields = WAFields;
    for iS = 1: size(handles.WAFields,1)
        NewWAFields{iS} = [handles.WAFields{iS,1},',',handles.WAFields{iS,2}]; %#ok<AGROW>
    end
    set(handles.edtWghtdAvg,'String',NewWAFields);
end

guidata(hObject,handles);

% --- Executes on button press in pbOK.
function pbOK_Callback(hObject, eventdata, handles)
% hObject    handle to pbOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Output{1} = get(handles.edtOutput,'String');
Output{2} = get(handles.edtUniqueFields,'String');
Output{3} = get(handles.edtSum,'String');
if isfield(handles,'WAFields')
    Output{4} = handles.WAFields;
else
    Output{4} = get(handles.edtWghtdAvg,'String');
end

handles.output = Output;
guidata(hObject,handles);

uiresume(handles.figUserInputs);

% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(handles.figUserInputs);

function SelectedInputs = selectinputs(PromptString,SelectionMode,ListString,Name,InitialValue)

if isempty(InitialValue)
    InitialValue = 1;
end

[SelIdx,IsSelected] = listdlg('PromptString',PromptString,...
    'SelectionMode',SelectionMode,...
    'ListString',ListString,...
    'ListSize',[300 300],...
    'Name',Name,...
    'InitialValue',InitialValue);

if IsSelected
    SelectedInputs = ListString(SelIdx);
else
    SelectedInputs = '';
end


% --- Executes when user attempts to close figUserInputs.
function figUserInputs_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figUserInputs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(handles.figUserInputs);


% --- Executes on key press with focus on figUserInputs and none of its controls.
function figUserInputs_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figUserInputs (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background.
function figUserInputs_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figUserInputs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on edtOutput and none of its controls.
function edtOutput_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to edtOutput (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
