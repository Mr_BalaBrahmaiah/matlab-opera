function ConsDeals = consolidatedeals(Inputheading, InputMatrix)

UserReply = questdlg('Do you want to continue with the previous selection of Output fields?','User Input Option','Yes','No','Yes');

if isempty(UserReply)
    UserReply = 'No';
end

if strcmpi(UserReply,'Yes')
    % Read the config file before processing the inputs
    [ConfigFile, ConfigPath] = uigetfile( ...
        {'*.txt','Text Files (*.txt)'}, ...
        'Select the Configuration file to be used for selection of Output fields');
    if ~isnumeric(ConfigFile)
        CONFIGFILENAME = fullfile(ConfigPath,ConfigFile);
    end
    
    if  exist(CONFIGFILENAME) %#ok<*EXIST>
        FileData = textread(CONFIGFILENAME,'%s','delimiter','\n'); %#ok<*REMFF1>
        
        OutputFields = getfieldnames(FileData,'OutputFields');
        UniqueFields = getfieldnames(FileData,'UniqueFields');
        SumFields = getfieldnames(FileData,'SumFields');
        WeightedAverageFields = getfieldnames(FileData,'WeightedAverageFields');
    else
        UserReply = 'No';
    end
end

if strcmpi(UserReply,'No')
    % Let the user select the required fields
    UserInputs = uiinterface(Inputheading);
    
    OutputFields  = UserInputs{1};
    UniqueFields  = UserInputs{2};
    SumFields     = UserInputs{3};
    WeightedAverageFields = UserInputs{4};
    % write the values in config file before proceeding further
    [ConfigFile, ConfigPath] = uiputfile('*.txt', 'Save Configuration as');
    if ~isnumeric(ConfigFile)
        CONFIGFILENAME = fullfile(ConfigPath,ConfigFile);
        FileID = fopen(CONFIGFILENAME,'w');
        
        FileOutputFields = ['OutputFields=',convertCell2Char(OutputFields)];
        FileUniqueFields = ['UniqueFields=',convertCell2Char(UniqueFields)];
        FileSumFields    = ['SumFields=',convertCell2Char(SumFields)];
        FileWAFields = ['WeightedAverageFields=',convertCell2Char(WeightedAverageFields)];
        fprintf(FileID,'%s\n %s\n %s\n %s\n',FileOutputFields,FileUniqueFields,FileSumFields,FileWAFields);
        
        fclose(FileID);
    end
    
end

if isempty(OutputFields)
    ConsDeals = [];
    errordlg('OutputFields are not available! Unable to proceed further...');
    return
end

if ~isempty(WeightedAverageFields)
    Values = WeightedAverageFields(:,1);
    Weights = WeightedAverageFields(:,2);
else
    Values = ''; Weights = '';
end

warning('OFF','MATLAB:nonIntegerTruncatedInConversionToChar');

if ~isempty(UniqueFields)
    ConcatenateStr = '';
    % Get the Input Matrix values
    for iInput = 1:length(UniqueFields)
        StrField = UniqueFields{iInput};
        Pos = strcmp(StrField, Inputheading);
        InValue =  getvalidarray(InputMatrix(:,Pos));
        if isnumeric(InValue)
            ConcatenateStr = strcat(ConcatenateStr,num2str(InValue));
        else
            ConcatenateStr = strcat(ConcatenateStr,InValue);
        end
    end
    
    [~,L1IdxIn,L1IdxOut] = unique(upper(ConcatenateStr),'stable');
    
    ConsDeals = cell(length(L1IdxIn), length(OutputFields));
    ConsDeals(:,:) = {' '};
    
    newIdx = unique(L1IdxOut,'stable');
    
    hWaitbar = waitbar(0,'Please wait, consolidation in progress...');
    for iIdx = 1:length(newIdx)
        AllIdx = find(L1IdxOut == newIdx(iIdx));
        Idx = AllIdx(1);
        
        for iOut = 1:length(OutputFields)
            StrField = OutputFields{iOut};
            Pos = strcmp(StrField, Inputheading);
            if ~isempty(SumFields) && ismember(StrField,SumFields)
                InValue =  getvalidarray(InputMatrix(AllIdx,Pos));
                if isnumeric(InValue)
                    ConsDeals(iIdx,iOut) = num2cell(sum(InValue));
                else
                    ConsDeals(iIdx,iOut) = num2cell(NaN);
                end
            elseif ~isempty(WeightedAverageFields) && ismember(StrField,Values)
                WeightName = Weights(strcmpi(Values,StrField));
                PosWeight = strcmp(WeightName, Inputheading);
                InValue =  getvalidarray(InputMatrix(AllIdx,Pos));
                InWeight = getvalidarray(InputMatrix(AllIdx,PosWeight));
                if isnumeric(InValue) && isnumeric(InWeight)
                    ConsDeals(iIdx,iOut) = num2cell(sum(InValue.*InWeight)./ sum(InWeight));
                else
                    ConsDeals(iIdx,iOut) = num2cell(NaN);
                end
            else
                InValue = getvalidarray(InputMatrix(Idx,Pos));
                if isnumeric(InValue)
                    ConsDeals(iIdx,iOut) = num2cell(InValue);
                else
                    ConsDeals(iIdx,iOut) = InValue;
                end
            end
            
            % weightedAverage
            %     OutWA = sum(values.*weights)./ sum(weights);
            %     Prem = sum(ActiveLots(AllIdx).*TradedPrice(AllIdx))./Lots;
            
        end
        waitbar(iIdx/length(newIdx),hWaitbar);
    end
else
    hWaitbar = waitbar(0,'Please wait, updation in progress');
    ConsDeals = cell(size(InputMatrix,1), length(OutputFields));
    ConsDeals(:,:) = {' '};
    for iOut = 1:length(OutputFields)
        StrField = OutputFields{iOut};
        Pos = strcmp(StrField, Inputheading);
        InValue = getvalidarray(InputMatrix(:,Pos));
        if isnumeric(InValue)
            ConsDeals(:,iOut) = num2cell(InValue);
        else
            ConsDeals(:,iOut) = InValue;
        end
        waitbar(iOut/length(OutputFields),hWaitbar);
    end
end

if iscolumn(OutputFields)
    OutputFields = OutputFields';
end

ConsDeals = [OutputFields;ConsDeals];

delete(hWaitbar);
end

function OutFields = getfieldnames(FileData,FieldName)
IdxFields =  strmatch(FieldName,FileData);
if ~isempty(IdxFields)
    T = char(FileData(IdxFields));
    TempFields = strsplit(T,'=');
    OutFields = TempFields{2};
    OutFields = convertChar2Cell(OutFields,',');
else
    OutFields = '';
end
end

