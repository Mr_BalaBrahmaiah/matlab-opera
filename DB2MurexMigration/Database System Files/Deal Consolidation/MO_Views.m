function varargout = MO_Views(varargin)
% MO_VIEWS MATLAB code for MO_Views.fig
%      MO_VIEWS, by itself, creates a new MO_VIEWS or raises the existing
%      singleton*.
%
%      H = MO_VIEWS returns the handle to a new MO_VIEWS or the handle to
%      the existing singleton*.
%
%      MO_VIEWS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MO_VIEWS.M with the given input arguments.
%
%      MO_VIEWS('Property','Value',...) creates a new MO_VIEWS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MO_Views_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MO_Views_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MO_Views

% Last Modified by GUIDE v2.5 06-Mar-2015 10:23:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MO_Views_OpeningFcn, ...
                   'gui_OutputFcn',  @MO_Views_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MO_Views is made visible.
function MO_Views_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MO_Views (see VARARGIN)

% Choose default command line output for MO_Views
handles.output = hObject;

TableName = 'valuation_date_table';
[ColNames,Data] = read_from_database(TableName,0);
DBData = cell2dataset([ColNames;Data]);

set(handles.edtValueDate,'String',DBData.value_date);
handles.ValueDate = char(DBData.value_date);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MO_Views wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MO_Views_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbTradersPandG.
function pbTradersPandG_Callback(hObject, eventdata, handles)
% hObject    handle to pbTradersPandG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProcessViewData('helper_traders_p_and_g_view', 'Traders_P_and_G_View', handles.ValueDate);


% --- Executes on button press in pbSettlePandG.
function pbSettlePandG_Callback(hObject, eventdata, handles)
% hObject    handle to pbSettlePandG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProcessViewData('helper_settle_p_and_g_view', 'Settle_P_and_G_view',  handles.ValueDate);

% --- Executes on button press in pbOptionSettlePrice.
function pbOptionSettlePrice_Callback(hObject, eventdata, handles)
% hObject    handle to pbOptionSettlePrice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProcessViewData('input_option_settlement_price_view', 'Option_Settlement_Price_View', handles.ValueDate);

% --- Executes on button press in pbFutureSettlePrice.
function pbFutureSettlePrice_Callback(hObject, eventdata, handles)
% hObject    handle to pbFutureSettlePrice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProcessViewData('input_underlying_settle_value_view', 'Underlying_Settle_Value_View', handles.ValueDate);

% --- Executes on button press in pbFutureTradersPrice.
function pbFutureTradersPrice_Callback(hObject, eventdata, handles)
% hObject    handle to pbFutureTradersPrice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ProcessViewData('input_underlying_traders_value_view', 'Underlying_Traders_Value_View', handles.ValueDate);

function edtValueDate_Callback(hObject, eventdata, handles)
% hObject    handle to edtValueDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtValueDate as text
%        str2double(get(hObject,'String')) returns contents of edtValueDate as a double


% --- Executes during object creation, after setting all properties.
function edtValueDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtValueDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ProcessViewData(InputViewname, ViewTitle, ValueDate)

hWaitbar = waitbar(0,'Please wait, reading data...');

[ColNames,Data] = read_from_database(InputViewname,0);

% Filename = [ViewTitle,'_',ValueDate,'.xlsx'];
Filename = getXLSFilename(ViewTitle);

if strcmpi(Data,'No Data')
    xlswrite(Filename,ColNames);
else
    xlswrite(Filename,[ColNames;Data]);
end

clear ColNames; clear Data;
delete(hWaitbar);
msgbox([ViewTitle,' data is saved in ',char(Filename)]);



% --- Executes on button press in pbCalcSettleVol.
function pbCalcSettleVol_Callback(hObject, eventdata, handles)
% hObject    handle to pbCalcSettleVol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

hWaitbar = waitbar(0,'Please wait, processing data...');

[~,DBVolId] = read_from_database('helper_get_unique_vol_ids_view',0);
waitbar(0.5,hWaitbar);
if ~strcmpi(DBVolId,'No Data')
    [SelectedVolIdx,IsSelected] = listdlg('PromptString','Select the VolIds:',...
        'SelectionMode','multiple',...
        'ListString',DBVolId);
    if IsSelected
        SelectedVolID = DBVolId(SelectedVolIdx);
        [OutData] = calculate_settlement_vol(SelectedVolID);
        Filename = getXLSFilename('Calculated_SettleVol_Data');
        OutHeader = {'ValueDate','VolId','Strike','CallPutId','SettleVol'};
        xlswrite(Filename,[OutHeader;OutData]);
        msgbox(['Calculated_SettleVol data is saved in ',char(Filename)]);
    else
        errordlg('No Vol Ids are selected!');
    end
else
    errordlg('No Vol Ids are present!');
end
delete(hWaitbar);