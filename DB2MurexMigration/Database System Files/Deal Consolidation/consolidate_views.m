function ConsDeals = consolidate_views
%  Consolidate the Settle or Traders View deals information
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/05 07:55:32 $
%  $Revision: 1.3 $
%

[Inputheading,InputMatrix] = read_from_database('valuation_date_table',0);
ValueDate = datestr(InputMatrix{2},'dd-mmm-yyyy');

Choice = menu(['Valuation Date: ',ValueDate],'Book Surface(Traders View)','Accounting Surface(Settle View)','Select the Excel File' );

hWaitbar = waitbar(0.2,'Please wait, reading data ...');

if Choice == 1  % Traders view
    ViewName = 'helper_traders_p_and_g_view';
    [Inputheading,InputMatrix] = read_from_database(ViewName,0);
    OutFilename =  ['TradersView_ConsolidatedDeals_',strrep(datestr(now),':','-'),'.xlsx'];
    InFilename = ['TradersView_InputDeals_',strrep(datestr(now),':','-'),'.xlsx'];
elseif Choice == 2 % settle view
    ViewName = 'helper_settle_p_and_g_view';
    [Inputheading,InputMatrix] = read_from_database(ViewName,0);
    OutFilename =  ['SettleView_ConsolidatedDeals_',strrep(datestr(now),':','-'),'.xlsx'];
    InFilename = ['SettleView_InputDeals_',strrep(datestr(now),':','-'),'.xlsx'];
elseif Choice == 3 % excel file
    [Filename, Pathname] = uigetfile( ...
        {'*.xls','Excel files (*.xls)'; ...
        '*.xlsx','Excel files (*.xlsx)'}, ...
        'Select the XL file');
    if ~isnumeric(Filename)
        XLFilename = fullfile(Pathname,Filename);
        Prompt = 'Provide the Sheet name to be used';
        Title = 'Excel Sheet Name Selection';
        SheetName = inputdlg(Prompt,Title);
        if isempty(SheetName)
            [~,~,RawData] = xlsread(XLFilename);
        else
            [~,~,RawData] = xlsread(XLFilename,char(SheetName));
        end
        Inputheading = RawData(1,:); % assuming that the header is present in the first row of excel
        InputMatrix =  RawData(2:end,:);
        OutFilename =  ['ConsolidatedDeals_',strrep(datestr(now),':','-'),'.xlsx'];
        InFilename = ['InputDeals_',strrep(datestr(now),':','-'),'.xlsx'];
    end
else
    waitbar(1,hWaitbar);
    delete(hWaitbar);
    return;
end
waitbar(1,hWaitbar);
delete(hWaitbar);

if exist('Inputheading','var') && exist('InputMatrix','var')
    ConsDeals = consolidatedeals(Inputheading, InputMatrix);
%     if ~isempty(ConsDeals)
%         OutFilename =  ['ConsolidatedDeals_',strrep(datestr(now),':','-'),'.csv'];
%         customcsvwrite(OutFilename,ConsDeals(2:end,:),ConsDeals(1,:));        
%     end
%     InFilename = ['InputDeals_',strrep(datestr(now),':','-'),'.csv'];
%     customcsvwrite(InFilename,InputMatrix,Inputheading);   
    if ~isempty(ConsDeals)        
        xlswrite(OutFilename,ConsDeals);        
    end    
    xlswrite(InFilename,[Inputheading;InputMatrix]);   
end
end

