function varargout = outputorderselectiongui(varargin)
% OUTPUTORDERSELECTIONGUI MATLAB code for outputorderselectiongui.fig
%      OUTPUTORDERSELECTIONGUI, by itself, creates a new OUTPUTORDERSELECTIONGUI or raises the existing
%      singleton*.
%
%      H = OUTPUTORDERSELECTIONGUI returns the handle to a new OUTPUTORDERSELECTIONGUI or the handle to
%      the existing singleton*.
%
%      OUTPUTORDERSELECTIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTPUTORDERSELECTIONGUI.M with the given input arguments.
%
%      OUTPUTORDERSELECTIONGUI('Property','Value',...) creates a new OUTPUTORDERSELECTIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before outputorderselectiongui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to outputorderselectiongui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help outputorderselectiongui

% Last Modified by GUIDE v2.5 11-Jul-2013 13:10:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @outputorderselectiongui_OpeningFcn, ...
                   'gui_OutputFcn',  @outputorderselectiongui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before outputorderselectiongui is made visible.
function outputorderselectiongui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to outputorderselectiongui (see VARARGIN)

if ~isempty(varargin)
    if ~iscellstr(varargin{1})
        varargin{1} = cellstr(varargin{1});
    end
    handles.OutputFields = varargin{1};
    set(handles.lbxInputFields,'String',handles.OutputFields);
    if get(handles.cbxSelectAll,'Value')
        set(handles.lbxInputFields,'Value',1:length(handles.OutputFields));
    else
        set(handles.lbxInputFields,'Value',1);
    end
else
    errordlg('No input fields are given!');
    delete(handles.figOutputOrderSelection);
    return;
end

% Choose default command line output for outputorderselectiongui
handles.output = handles.OutputFields;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes outputorderselectiongui wait for user response (see UIRESUME)
uiwait(handles.figOutputOrderSelection);


% --- Outputs from this function are returned to the command line.
function varargout = outputorderselectiongui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure

if isfield(handles,'output')
    varargout{1} = handles.output;
    delete(handles.figOutputOrderSelection);
    return;
else
    varargout{1} = [];
end


% --- Executes on selection change in lbxInputFields.
function lbxInputFields_Callback(hObject, eventdata, handles)
% hObject    handle to lbxInputFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxInputFields contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxInputFields


% --- Executes during object creation, after setting all properties.
function lbxInputFields_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxInputFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbAddFields.
function pbAddFields_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
OutputFields = get(handles.lbxInputFields,'String');
SelValue = get(handles.lbxInputFields,'Value');
SelOutputs = OutputFields(SelValue);

Data = get(handles.lbxSelOutput,'String');
Idx = ismember(SelOutputs,Data);
SelOutputs(Idx) = [];

Data = [Data;SelOutputs];
set(handles.lbxSelOutput,'String',Data,'value',1);
guidata(hObject, handles);

% --- Executes on button press in pbRemoveFields.
function pbRemoveFields_Callback(hObject, eventdata, handles)
% hObject    handle to pbRemoveFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Data = get(handles.lbxSelOutput,'String');
SelValue = get(handles.lbxSelOutput,'Value');
if ~isempty(Data)
    Data(SelValue) = [];
    set(handles.lbxSelOutput,'String',Data,'value',1);
end
guidata(hObject, handles);

% --- Executes on selection change in lbxSelOutput.
function lbxSelOutput_Callback(hObject, eventdata, handles)
% hObject    handle to lbxSelOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbxSelOutput contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbxSelOutput


% --- Executes during object creation, after setting all properties.
function lbxSelOutput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbxSelOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbOK.
function pbOK_Callback(hObject, eventdata, handles)
% hObject    handle to pbOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SelOutput = get(handles.lbxSelOutput,'String');
handles.output = SelOutput;

if ~isempty(SelOutput)
    DiffFields = setdiff(handles.OutputFields,SelOutput);    
    if ~isempty(DiffFields)
        SelValue = find(ismember(handles.OutputFields,DiffFields));
        set(handles.lbxInputFields,'Value',SelValue);
        Reply = questdlg('Some of the output fields are not selected.These missed output fields will be ignored in Output display.Are you sure you want to continue?','Output Order Selection','Yes','No','No');
        if strcmpi(Reply,'No')            
            return;
        end
    end
else
    Reply = questdlg('Output order is not selected! Hence the current order will be taken for Output display. Are you sure you want to continue?','Output Order Selection','Yes','No','No');
    if strcmpi(Reply,'Yes')
        handles.output = handles.OutputFields;
    else
        return;
    end
end
guidata(hObject, handles);
uiresume(handles.figOutputOrderSelection);


% --- Executes on button press in pbCancel.
function pbCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pbCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figOutputOrderSelection);


% --- Executes when user attempts to close figOutputOrderSelection.
function figOutputOrderSelection_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figOutputOrderSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uiresume(handles.figOutputOrderSelection);


% --- Executes on button press in cbxSelectAll.
function cbxSelectAll_Callback(hObject, eventdata, handles)
% hObject    handle to cbxSelectAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbxSelectAll
SelectOption = get(hObject,'Value');
if SelectOption
    IdxValues = get(handles.lbxInputFields,'String');
    set(handles.lbxInputFields,'Value',1:length(IdxValues));
else
    set(handles.lbxInputFields,'Value',1);
end
