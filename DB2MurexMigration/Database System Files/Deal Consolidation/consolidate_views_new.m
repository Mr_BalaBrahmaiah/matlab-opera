function ConsDeals = consolidate_views_new
%  Consolidate the Settle or Traders View deals information
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/10/01 09:39:43 $
%  $Revision: 1.1 $
%

Choice = menu('Select the view/excel','Book Surface(Traders View)','Accounting Surface(Settle View)','Select the Excel File' );

if Choice == 1 || Choice == 2
    Prompt        = {'Enter the Valuation Date(yyyy-mm-dd format):'};
    Name          = 'Date Input for View Consolidator';
    Numlines      = 1;
    Defaultanswer = {datestr(today,'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('View consolidator will not run since the date input selection was aborted!');
        return;
    end
    
    DBValueDate    = InputDates{1};
    if isempty(DBValueDate)
        DBValueDate  = datestr(today,'yyyy-mm-dd');
    end
end

hWaitbar = waitbar(0.2,'Please wait, reading data ...');

if Choice == 1  || Choice == 2 % Traders view or settle view
    if Choice == 1 % Traders surface
        TableName = 'security_traders_value_table';
        PricingType = 'traders';
    else % accounting surface
        PricingType = 'settle';
        TableName = 'security_settlement_value_table';
    end
    
    SqlQuery = ['select * from ',TableName,' where value_date = ''',DBValueDate,''''];
    [PricerOutputHeading,PricerOutputMatrix] = read_from_database(TableName,0,SqlQuery);
    
    ViewName = 'helper_sensitivity_report_new_view';
    [InDealsHeader,InDealsMatrix] = read_from_database(ViewName,0);
    
    [InDealsMatrix,InDealsHeader] = calcActiveLots(InDealsMatrix,InDealsHeader);
    [Inputheading,InputMatrix]= generate_pricing_views(PricingType,InDealsHeader,InDealsMatrix,PricerOutputHeading,PricerOutputMatrix);
            
    clear PricerOutputMatrix; clear PricerOutputHeading;
    clear InDealsHeader; clear InDealsMatrix;  clear OutData;
    
elseif Choice == 3 % excel file
    [Filename, Pathname] = uigetfile( ...
        {'*.xls','Excel files (*.xls)'; ...
        '*.xlsx','Excel files (*.xlsx)'}, ...
        'Select the XL file');
    if ~isnumeric(Filename)
        XLFilename = fullfile(Pathname,Filename);
        Prompt = 'Provide the Sheet name to be used';
        Title = 'Excel Sheet Name Selection';
        SheetName = inputdlg(Prompt,Title);
        if isempty(SheetName)
            [~,~,RawData] = xlsread(XLFilename);
        else
            [~,~,RawData] = xlsread(XLFilename,char(SheetName));
        end
        Inputheading = RawData(1,:); % assuming that the header is present in the first row of excel
        InputMatrix =  RawData(2:end,:);
    end
else
    waitbar(1,hWaitbar);
    delete(hWaitbar);
    return;
end
waitbar(1,hWaitbar);
delete(hWaitbar);


if exist('Inputheading','var') && exist('InputMatrix','var')
    ConsDeals = consolidatedeals(Inputheading, InputMatrix);
    %     if ~isempty(ConsDeals)
    %         OutFilename =  ['ConsolidatedDeals_',strrep(datestr(now),':','-'),'.csv'];
    %         customcsvwrite(OutFilename,ConsDeals(2:end,:),ConsDeals(1,:));
    %     end
    %     InFilename = ['InputDeals_',strrep(datestr(now),':','-'),'.csv'];
    %     customcsvwrite(InFilename,InputMatrix,Inputheading);
    if ~isempty(ConsDeals)
        OutFilename =  ['ConsolidatedDeals_',strrep(datestr(now),':','-'),'.xlsx'];
        xlswrite(OutFilename,ConsDeals);
    end
    InFilename = ['InputDeals_',strrep(datestr(now),':','-'),'.xlsx'];
    xlswrite(InFilename,[Inputheading;InputMatrix]);
end
end

