function [OutErrorMsg,ExpiryFilename,MonitoringFilename] = process_barrier_expiry(InBUName)
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/28 11:01:33 $
%  $Revision: 1.11 $
%

OutErrorMsg = {'No errors'};
ExpiryFilename = ''; MonitoringFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    ObjDB = connect_to_database;
    
    % get the list of futures for all the barrier deals
    ViewName = 'helper_barrier_expiry_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    if strcmpi(Data,'No Data')
        OutErrorMsg = {'No Barrier data found!'};
        return;
    end
    BarrierDBData = cell2dataset([ColNames;Data]);
    BarrierFutId = unique(upper(BarrierDBData.underlying_id));
    SettlementDate = BarrierDBData.settlement_date{1};
    Barrier_Level = BarrierDBData.barrier_level;
    
%     Tablename = ['barrier_futures_price_table_',InBUName];
    Tablename = 'barrier_futures_price_table';
    SqlQuery = ['select * from ',Tablename,' where settlement_date = ''',SettlementDate,''''];
    BBdata = fetch(ObjDB,SqlQuery);
    
    BarrierExpData =[];
    BarrierMissingPrices = [];
    
    TickerOptions = {'PX_OPEN','PX_HIGH','PX_LOW','PX_LAST','PX_SETTLE'};
    
    for iCode = 1:length(BarrierFutId)
        Idx = strcmpi(BarrierFutId{iCode},BarrierDBData.underlying_id);
        try
            BBdataIdx = strcmpi(BarrierFutId{iCode},BBdata(:,2));
            if any(BBdataIdx)
                Open  = BBdata{BBdataIdx,4};
                High  = BBdata{BBdataIdx,5};
                Low   = BBdata{BBdataIdx,6};
                Close = BBdata{BBdataIdx,7};
                
                True_Range_Low = repmat(Close,size(find(Idx)));
                True_Range_High = repmat(Close,size(find(Idx)));
                
                IdxBarrierType = strcmpi('american',BarrierDBData.barrier_type(Idx));
                if any(IdxBarrierType)
                    True_Range_Low(IdxBarrierType) = min(Open,Low);
                    True_Range_High(IdxBarrierType) = max(Open,High);
                end
                
                TRL_LTE_Barrier_Level = True_Range_Low <= Barrier_Level(Idx);
                TRH_GTE_Barrier_Level = True_Range_High >= Barrier_Level(Idx);
                
                BarrierHit = TRH_GTE_Barrier_Level;
                
                IdxDown = strcmpi('down',BarrierDBData.barrier_up_down(Idx));
                if any(IdxDown)
                    BarrierHit(IdxDown) = TRL_LTE_Barrier_Level(IdxDown);
                end
                
                BBdata_data = repmat(BBdata(BBdataIdx,4:end),size(find(Idx)));
                
                %%% newly added logic (12-02-2020), to handle the Compo Option
                %%% Trades, get Barrier Hit Value
                Idxderivative_type = strncmpi('cb_amer',BarrierDBData.derivative_type(Idx),7) | strncmpi('cb_euro',BarrierDBData.derivative_type(Idx),7);
                if any(Idxderivative_type)
                    Barrier_Data = BarrierDBData(Idx,:);
                    BarrierLevel = Barrier_Level(Idx,:);
                    
                    Compo_BarrierDBData = Barrier_Data(Idxderivative_type,:);
                    Compo_Barrier_Level = BarrierLevel(Idxderivative_type,:);
                    
                    %%% Get settlement_date from valuation_date_table
                    settle_sql_query = 'select settlement_date from valuation_date_table';
                    settlement_date = fetch(ObjDB,settle_sql_query);

                    [r,~] = size(Compo_BarrierDBData);
                    Compo_Barrier_Hit = []; Compo_Market_Price = [];
                    for ii = 1:1:r
                        ContractNumber = ['''',strrep(convertCell2Char(Compo_BarrierDBData.contract_number(ii)),',',''','''),''''];
                        Table_Name = ['cloned_deal_ticket_table_',char(InBUName)]; 
                        Sql_Query = ['select distinct(right(security_id,3)) from ',char(Table_Name),' where contract_number in (',char(ContractNumber),')'];
                        [~,currency] = Fetch_DB_Data(ObjDB,Sql_Query);

                        if strcmpi(currency,'No Data')
                            OutErrorMsg = {['Not getting currency for contract_number:',char(ContractNumber),'from ',char(Table_Name),' table']};
                            return
                        end

                        if size(currency,1)>1
                            OutErrorMsg = {['Getting multiple currency for contract_number:',char(ContractNumber),'from ',char(Table_Name),' table']};
                            return
                        end

                        %%% get spot From currency_spot_mapping_table
                        Sql_query = ['select spot from currency_spot_mapping_table where currency = ''',char(currency),''' '];
                        currency_spot = fetch(ObjDB,Sql_query); 

                        %%% get settle_value From underlying_settle_value_table
                        settle_value_query = ['select settle_value from underlying_settle_value_table where settlement_date= ''',char(settlement_date),''' and underlying_id = ''',char(currency_spot),''' '];
                        Spot_value = fetch(ObjDB,settle_value_query);  

                        Market_Price = cell2mat(Spot_value) * Close; % Spot * Settle_value
                        Barrier_Hit_Value = Market_Price <= Compo_Barrier_Level(ii);
                        
                        Compo_Market_Price = [Compo_Market_Price ; Market_Price];
                        Compo_Barrier_Hit = [Compo_Barrier_Hit ; Barrier_Hit_Value];
                    end  
                    
                    BBdata_data(find(Idxderivative_type),5) = num2cell(Compo_Market_Price);
                    BarrierHit(find(Idxderivative_type)) = Compo_Barrier_Hit;
                end
                %%% End Code
                
                TempData = [BarrierDBData.contract_number(Idx),BarrierDBData.underlying_id(Idx),BarrierDBData.derivative_type(Idx)...
                    BarrierDBData.settlement_date(Idx),BarrierDBData.barrier_type(Idx), BarrierDBData.barrier_up_down(Idx),...
                    BarrierDBData.barrier_in_out(Idx),num2cell(BarrierDBData.barrier_level(Idx)),...
                    BBdata_data,num2cell(BarrierHit)];
                
                BarrierExpData = [BarrierExpData;TempData]; % contract_no und_id bar_level open high low hit
            else
                BarrierMissingPrices = [BarrierMissingPrices;BarrierFutId(iCode)];
            end
        catch
            BarrierMissingPrices = [BarrierMissingPrices;BarrierFutId(iCode)];
            continue;
        end
    end
    
    if ~isempty(BarrierMissingPrices)
        OutErrorMsg = cellstr(['Future prices for ''', convertCell2Char(BarrierMissingPrices), ''' are missing in BBG for the settlement_date ''',SettlementDate,'''. Hence barrier expiry cannot be processed for these futures!']);
        return;
    end
    
    ExpiryFilename = getXLSFilename(['Barrier_Expiry_',InBUName]);
    MonitoringFilename = getXLSFilename(['Barrier_monitoring_table_',InBUName]);
    
    Header = ['contract_number','future_id','derivative_type','settlement_date','barrier_type',...
        'barrier_up_down','barrier_in_out','barrier_level',TickerOptions,'is_barrier_hit'];
    
    
    xlswrite(ExpiryFilename,[Header;BarrierExpData]);
    
    try
        TableName = ['barrier_expiry_table_',InBUName];
        upload_in_database(TableName, BarrierExpData);
    catch ME
        disp(ME.message);
    end
    
    BarrierExpData = cell2dataset([Header;BarrierExpData]);
    
    if any(BarrierExpData.is_barrier_hit)
        ViewName = 'helper_barrier_expiry_security_info_view';
        [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
        try
            BarrierSecInfo = cell2dataset([ColNames;Data]);
        catch
            if strcmpi(Data,'No Data')
                OutErrorMsg = {'No data found!'};
                return;
            end
        end
        
        NumRows= size(BarrierExpData,1);
        OutBarrierMonData = [];
        for iSec = 1:NumRows
            if BarrierExpData.is_barrier_hit(iSec)
                %     TempSettleDate =datestr(datenum(BarrierExpData.settlement_date{iSec}),'yyyy-mm-dd');
                % IdxBarSec = strcmpi(BarrierExpData.contract_number(iSec),BarrierSecInfo.contract_number) & ...
                % strcmpi(BarrierExpData.future_id(iSec),BarrierSecInfo.underlying_id) & ...
                % strcmpi(BarrierExpData.derivative_type(iSec),BarrierSecInfo.derivative_type) & ...
                % isnumericequal(BarrierExpData.barrier_level(iSec),BarrierSecInfo.barrier_level) & ...
                % strcmpi(TempSettleDate,BarrierSecInfo.settlement_date);
                IdxBarSec = strcmpi(BarrierExpData.contract_number(iSec),BarrierSecInfo.contract_number) & ...
                    strcmpi(BarrierExpData.future_id(iSec),BarrierSecInfo.underlying_id) & ...
                    strcmpi(BarrierExpData.derivative_type(iSec),BarrierSecInfo.derivative_type) & ...
                    isnumericequal(BarrierExpData.barrier_level(iSec),BarrierSecInfo.barrier_level) ;
                
                if any(IdxBarSec)
                    TempSize = size(find(IdxBarSec));
                    BarrierRowData = [BarrierSecInfo.contract_number(IdxBarSec),...
                        BarrierSecInfo.value_date(IdxBarSec),BarrierSecInfo.security_id(IdxBarSec),...
                        num2cell(repmat(1,TempSize)),repmat({'NULL'},TempSize)];
                    OutBarrierMonData = [OutBarrierMonData;BarrierRowData];
                end
                
            end
        end
        
        
        if ~isempty(OutBarrierMonData)
            MonHeader = {'contract_number','value_date','security_info','isbarrier1_hit','isbarrier2_hit'};
            xlswrite(MonitoringFilename,[MonHeader;OutBarrierMonData]);
        else
            MonitoringFilename = '';
        end
    else
        MonitoringFilename = '';
    end
    try
        SharedFolder = get_reports_foldername;
        DestinationFile = fullfile(SharedFolder,ExpiryFilename);
        movefile(ExpiryFilename,DestinationFile);
        if ~isempty(MonitoringFilename)
            DestinationFile = fullfile(SharedFolder,MonitoringFilename);
            movefile(MonitoringFilename,DestinationFile);
        end
    catch ME
        OutErrorMsg = cellstr(ME.message);
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
end