function [OutErrorMsg,FuturesReportFilename,OptionReportFilename,...
    FxReportFilename,TradeQueryReportFilename,OTCReport] = generate_all_riskoffice_reports(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

[OutErrorMsg1,FuturesReportFilename] = generate_futures_report(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    FuturesReportFilename = char(OutErrorMsg1);
end

[OutErrorMsg2,OptionReportFilename]  = generate_options_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    OptionReportFilename = char(OutErrorMsg2);
end

[OutErrorMsg3,FxReportFilename]     = generate_currency_report(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    FxReportFilename = char(OutErrorMsg3);
end

[OutErrorMsg4,TradeQueryReportFilename] = generate_trade_query_report(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    TradeQueryReportFilename = char(OutErrorMsg4);
end

[OutErrorMsg5,OTCReport] = generate_riskoffice_rms_report(InBUName);
if ~strcmpi(OutErrorMsg5,'No errors')
    OTCReport = char(OutErrorMsg5);
end

if ~strcmpi(OutErrorMsg1,'No errors') && ...
   ~strcmpi(OutErrorMsg2,'No errors') && ...
   ~strcmpi(OutErrorMsg3,'No errors') && ...
   ~strcmpi(OutErrorMsg4,'No errors') && ...
   ~strcmpi(OutErrorMsg5,'No errors') 
    OutErrorMsg = convertChar2Cell([char(OutErrorMsg1);char(OutErrorMsg2);...
        char(OutErrorMsg3);char(OutErrorMsg4);char(OutErrorMsg5)]);
end

end