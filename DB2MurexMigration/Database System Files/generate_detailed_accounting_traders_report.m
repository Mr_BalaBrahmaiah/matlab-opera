function TradersReportFilename = generate_detailed_accounting_traders_report
% generate the detailed traders report format with counterparty and contract months
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:52 $
%  $Revision: 1.5 $
%

try
    OutErrorMsg = {'No errors'};
    
    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    [ColNames,Data] = read_from_database(ViewName,0);
    
    TradersReportFilename = getXLSFilename('Accounting_Traders_Report_Monthwise');
           
    UniqueFields = {'value_date','subportfolio','contract_month','counterparty_parent'};
    SumFields = {'settle_delta_1','settle_gamma_11','settle_theta','settle_vega_1','mtm-usd'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(TradersReportFilename,[OutputFields;OutData]);    
       
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    TradersReportFilename = '';
end