function [OutErrorMsg,CPFilename] = generate_counterparty_report(InBUName)
%  Generate the counterparty report
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/27 11:32:23 $
%  $Revision: 1.7 $
%

OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %%   generate the counterparty report
    %     ViewName = 'helper_counterparty_report_view';
    %     [SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    ObjDB = connect_to_database;
    Defult_ViewName = 'helper_counterparty_report_view_';
    ViewName = strcat(Defult_ViewName,char(InBUName));
    [SettleColNames,Data] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    %%
    % remove the internal counterparties
    PosCP = find(strcmpi('counterparty_parent',SettleColNames));
    IdxIntCP = strncmpi(Data(:,PosCP),'MM',2) | strncmpi(Data(:,PosCP),'RMS',3);
    Data(IdxIntCP,:) = [];
    
    % remove the derivative-type 'fx'
    PosOptType = find(strcmpi('derivative_type',SettleColNames));
    IdxFx = strncmpi('fx_spot',Data(:,PosOptType),length('fx_spot')) | strncmpi('fx_forward',Data(:,PosOptType),length('fx_forward'));
    Data(IdxFx,:) = [];
    
    PosValueDate = find(strcmpi('value_date',SettleColNames));
    PosMaturity  = find(strcmpi('maturity_date',SettleColNames));
    ValueDate    = datenum(Data(:,PosValueDate),'yyyy-mm-dd'); %#ok<*FNDSB>
    TempMaturity = Data(:,PosMaturity);
    TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
    MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
    
    SettleData = cell2dataset([SettleColNames;Data]);
    
    clear Data;
    
    ValuationDate = SettleData.value_date{1};
    
    IdxKO = strncmpi(SettleData.subportfolio,'RMS-KO',length('RMS-KO'));
    SettleData.nc_factor(IdxKO) = 1;
    SettleData.mtm_usd(IdxKO) = SettleData.mtm_nc(IdxKO) .* SettleData.nc_factor(IdxKO) .* SettleData.fx_nc_factor(IdxKO);
    IdxNetted = strcmpi('fully_netted',SettleData.netting_status); % dead
    IdxPremiumPaid = (SettleData.is_premium_paid == 1);
    %     IdxLive = (ValueDate <= MaturityDate) & ~IdxNetted;
    %     IdxDead = (ValueDate > MaturityDate) | IdxNetted | IdxPremiumPaid;
    IdxDead = (ValueDate > MaturityDate) | IdxNetted ;
    IdxLive = ~IdxDead;
    IdxFut = strcmpi('future',SettleData.derivative_type) | strcmpi('cs_future',SettleData.derivative_type); %added on 12-02-2020, cs_future
    IdxOpt = ~IdxFut;
    
    Counterparty = unique(upper(SettleData.counterparty_parent));
    
    CPOutData =[];
    
    for iS = 1:length(Counterparty)
        CP = Counterparty(iS);
        IdxSettle  = strcmpi(CP,SettleData.counterparty_parent);
        
        ContractNo = unique(upper(SettleData.contract_number(IdxSettle)));
        ContractNo(cellfun(@isempty,ContractNo)) = [];
        for iCNo = 1:length(ContractNo)
            IdxSettleCM  = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM); %#ok<*LOGL>
            
            IdxDeadFut = zeros(size(IdxSettle)); IdxDeadFut = logical(IdxDeadFut);
            IdxLiveFut = zeros(size(IdxSettle)); IdxLiveFut = logical(IdxLiveFut);
            IdxLiveOpt = zeros(size(IdxSettle)); IdxLiveOpt = logical(IdxLiveOpt);
            IdxDeadOpt = zeros(size(IdxSettle)); IdxDeadOpt = logical(IdxDeadOpt);
            IdxPremRealised   = zeros(size(IdxSettle)); IdxPremRealised = logical(IdxPremRealised);
            IdxPremUnrealised = zeros(size(IdxSettle)); IdxPremUnrealised = logical(IdxPremUnrealised);
            
            IdxSettleCM(IdxSettle)   = strcmpi(ContractNo{iCNo},SettleData.contract_number(IdxSettle));
            
            IdxDeadFut(IdxSettleCM) = IdxFut(IdxSettleCM) & IdxDead(IdxSettleCM);
            IdxLiveFut(IdxSettleCM) = IdxFut(IdxSettleCM) & IdxLive(IdxSettleCM);
            
            IdxLiveOpt(IdxSettleCM) = IdxOpt(IdxSettleCM) & IdxLive(IdxSettleCM);
            IdxDeadOpt(IdxSettleCM) = IdxOpt(IdxSettleCM) & IdxDead(IdxSettleCM);
            
            IdxOptAll = IdxLiveOpt | IdxDeadOpt;
            
            IdxPremRealised(IdxSettleCM)   = IdxOpt(IdxSettleCM) & (IdxDead(IdxSettleCM) | IdxPremiumPaid(IdxSettleCM));
            IdxPremUnrealised(IdxSettleCM) = IdxOpt(IdxSettleCM) & IdxLive(IdxSettleCM) & ~IdxPremiumPaid(IdxSettleCM);
            
            Subportfolio  = unique(upper(SettleData.subportfolio(IdxSettleCM)));
            if length(Subportfolio) ~= 1
                Subportfolio = {' '};
            end
            %         LotMult  = unique(SettleData.lot_mult1(IdxSettleCM));
            %         CurrMult = unique(SettleData.curr_mult1(IdxSettleCM));
            %         Mult = LotMult * CurrMult;
            
            MultFactor = unique(SettleData.mult_factor(IdxSettleCM));
            NCFactor = unique(SettleData.nc_factor(IdxSettleCM));
            FxNCFactor = unique(SettleData.fx_nc_factor(IdxSettleCM));
            if length(MultFactor) > 1
                MultFactor = MultFactor(1);
            end
            if length(NCFactor) > 1
                NCFactor = NCFactor(1);
            end
            if length(FxNCFactor) > 1
                FxNCFactor = FxNCFactor(1);
            end
            
            
            EquivLots = sum(SettleData.active_lots(IdxLiveFut));
            RealisedFut = sum(SettleData.mtm_usd(IdxDeadFut));
            UnrealisedFut = sum(SettleData.mtm_usd(IdxLiveFut));
            OptPremPay  = sum(SettleData.premium(IdxOptAll) .* SettleData.original_lots(IdxOptAll) .* MultFactor .* NCFactor .* FxNCFactor);
            OptMktValue = sum(SettleData.settle_price(IdxLiveOpt) .* SettleData.active_lots(IdxLiveOpt) .* MultFactor .* NCFactor .* FxNCFactor);
            OptionM2M = sum(SettleData.mtm_usd(IdxOptAll));
            if strcmpi(InBUName,'cfs')
                RealisedPremium   = sum(SettleData.premium(IdxPremRealised) .* SettleData.active_lots(IdxPremRealised) .* MultFactor .* NCFactor .* FxNCFactor);
                UnrealisedPremium = sum(SettleData.premium(IdxPremUnrealised) .* SettleData.active_lots(IdxPremUnrealised) .* MultFactor .* NCFactor .* FxNCFactor);
            else
                 RealisedPremium   = sum(SettleData.premium(IdxPremRealised) .* SettleData.original_lots(IdxPremRealised) .* MultFactor .* NCFactor .* FxNCFactor);
                 UnrealisedPremium = sum(SettleData.premium(IdxPremUnrealised) .* SettleData.original_lots(IdxPremUnrealised) .* MultFactor .* NCFactor .* FxNCFactor);
            end
            OptDelta = sum(SettleData.settle_delta(IdxLiveOpt));
            
            CPRowData = [Subportfolio,Counterparty(iS),ContractNo(iCNo),EquivLots,...
                RealisedFut,UnrealisedFut,OptPremPay,OptMktValue,OptionM2M,RealisedPremium,UnrealisedPremium,OptDelta];
            CPOutData = [CPOutData;CPRowData];
            
        end
        
    end
    
    CPHeader = {'Subportfolio','Counterparty','ContractNo','Equivalent Lots',...
        'Realised Futures','Unrealised Futures','OFUT Premium Rec Pay','MV of Options','Option M2M','Realised Premium','Unrealised Premium','Option Delta'};
    % CPFilename = ['CounterpartyReport_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
    % customcsvwrite(CPFilename,CPOutData,CPHeader);
    
    CPFilename = getXLSFilename('CounterpartyReport');
    xlswrite(CPFilename,[CPHeader;CPOutData]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    CPFilename = '';
end