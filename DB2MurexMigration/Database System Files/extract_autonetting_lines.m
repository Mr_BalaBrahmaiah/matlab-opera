function [OutIndex, OutputArray] = extract_autonetting_lines(InputArray,Expected_Sum_Value)

OutIndex= [];
OutputArray = [];

OverallSum = sum(InputArray);

TempIndex = []; i = [];

if(Expected_Sum_Value <= OverallSum)
    SumResult = 0;
%     OutputArray = [];
    for i = 1 : size(InputArray,1)
        
        Current_Element = InputArray(i);
        
        SumResult = SumResult + Current_Element;
        
        if(SumResult == Expected_Sum_Value)
            OutputArray = [OutputArray ; Current_Element];
            break;
        elseif(SumResult < Expected_Sum_Value)
            OutputArray = [OutputArray ; Current_Element];
            continue;
        elseif(SumResult > Expected_Sum_Value)
            if(Current_Element > 0)
                SumResult = SumResult - Current_Element;
                Diff_Expected_SumValue = abs(Expected_Sum_Value - SumResult);
                
                Current_Element_Remain = Current_Element - Diff_Expected_SumValue;
                Diff_from_CurrentElement = Current_Element - Current_Element_Remain;
                
                SumResult =  SumResult + Diff_from_CurrentElement;
                
                OutputArray = [OutputArray ; Diff_from_CurrentElement];
                
                break;
                
            else
                continue;
            end
        else
            %Empty
        end
        TempIndex = [TempIndex;i];
    end
    
    
else    
    %Warning
end

if ~isempty(i)
OutIndex = [1:i]';
end