function save_consolidated_traders_report_summary(InBUName)

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

Viewname = 'helper_traders_report_pnl_view';
[ColNames,Data] = read_from_database(Viewname,0,'',InBUName);

Filename = getXLSFilename(['Consolidated_Traders_Report_Summary_',InBUName]);

try
    if (exist(Filename,'file') == 2)
        delete(Filename);
    end
catch
end
save_consolidated_traders_report_summary('agf')
xlswrite(Filename,[ColNames;Data]);

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

try
    configure_mail_settings;    
    sendmail({'cfsg.mo@olamnet.com','Raghavendra.Sn@olamnet.com'}, 'OPERA Consolidated Traders Report Summary', ['Attached is the Consolidated Traders Report Summary till COB ''',SettlementDate,''''],{Filename});    
catch
end

try
    DestinationFolder = '\\10.190.7.222\Share\DBSystem_Reports\Consolidated_TR_Report_Summary';
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Consolidated Traders Report Summary');
catch
end