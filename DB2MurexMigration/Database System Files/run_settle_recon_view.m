function run_settle_recon_view

DestinationFolder = '\\10.190.7.149\Invenio Database System\Syed_Daily_pnl_project';

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try
    ViewName = 'helper_settle_pricing_subportfoliovalues_view_agf';
    SqlQuery = 'select * from helper_settle_pricing_subportfoliovalues_view_agf where subportfolio like ''ZT%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery);
       
    UniqueFields = {'portfolio','subportfolio','product_code','value_date','transaction_date','contract_month','strike'};
    SumFields = {'active_lots','mtm-usd','settle_delta_1','original_lots'};
    OutputFields = {'portfolio','subportfolio','product_code','value_date','transaction_date','maturity_date','contract_month','strike','active_lots','settle_price','mtm-usd','p1_settleprice','settle_delta_1','original_lots'};
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Filename = getXLSFilename('SettleView_ConsolidatedDeals');
    xlswrite(Filename,[OutputFields;OutData]);
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Syed Daily pnl project');
   
     
%     PosTradeId = strcmpi('trade_id',ColNames);
%     IdxNetTrades = strncmpi('NET',Data(:,PosTradeId),length('NET'));
%     Data(IdxNetTrades,:)= [];
%     
%     UniqueFields = {'portfolio','subportfolio','product_code','value_date','transaction_date','contract_month','strike'};
%     SumFields = {'active_lots','mtm-usd','settle_delta_1','original_lots'};
%     OutputFields = {'portfolio','subportfolio','product_code','value_date','transaction_date','maturity_date','contract_month','strike','active_lots','settle_price','mtm-usd','p1_settleprice','settle_delta_1','original_lots'};
%     WeightedAverageFields = [];
%     [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
%     
%     Filename = getXLSFilename('SettleView_ConsolidatedDeals_WithoutNetting');
%     xlswrite(Filename,[OutputFields;OutData]);
%     OutFilename = fullfile(DestinationFolder,Filename);
%     move_reports(Filename,OutFilename,'Syed Daily pnl project');
    
catch ME
    write_log_file(LogFilename,ME.message);
end

