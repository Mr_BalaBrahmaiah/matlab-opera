function [OutErrorMsg,OutXLSfileName] = generate_position_monitoring_report_StoredProcedure_MCO(InBUName)

try
    OutErrorMsg = {'No Errors'};
    OutXLSfileName = getXLSFilename(['Position_Monitoring_Report_',upper(char(InBUName))]);
    
    ObjDB = connect_to_database ;
    
    Current_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    FY_StartDate = {'2017-01-01'} ; %%fetch(ObjDB,'select start_date from valuation_financial_year_view');
    
    %% Overall Day
    SqlQuery_Overall_SettleDate_ValueDate =['select * from vdate_sdate_table where settlement_date >=''',char(FY_StartDate),''' and settlement_date <=''',char(Current_SettleDate),''' ' ];
    [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %% Product Name
    
    %     Unique_ProductName = unique(Overall_DBData.product_name);
    Unique_ProductName = fetch(ObjDB,'select product_name from position_monitoring_report_products_table') ;
    Unique_ProductName = sort(Unique_ProductName);
    
    %% Stored Procedure Name
    StoreProcedure_Name = ['generate_position_monitoring_data_',char(InBUName)];
    
    %% Consolidate
    OutputFields = {'subportfolio','transaction_date','counterparty_parent','contract_month','derivative_type','active_lots'};
    UniqueFields = {'subportfolio','transaction_date','counterparty_parent','contract_month','derivative_type'};
    SumFields = {'active_lots'};
    WeightedAverage = [];
    
    %%
    
    for ii = 1 : size(Unique_ProductName,1)
        
        Current_ProductName = Unique_ProductName(ii);
        
        for i = 1 : length(Overall_ValueDate)
            
            Current_ValueDate = Overall_ValueDate(i);
            Current_SettleDate = Overall_SettleDate(i);
            
            %% Fetch Data
            
            [ColNames,DDBData] = Call_StoredProcedure(ObjDB,StoreProcedure_Name,Current_ValueDate);
            
            if(strcmpi(DDBData,'No Data'))
                OutErrorMsg = {'No Data from Database ,Check DB'};
                continue;
            end
            
            if(isempty(DDBData))
                OutErrorMsg = {'No Data from Database ,Check DB'};
                continue;
            end
            
            
            %%
            
            [ConsolidateFields,ConsolidateData] = consolidatedata(ColNames, DDBData,UniqueFields,SumFields,OutputFields,WeightedAverage);
            
            %% Excel Write
            
            xlswrite(OutXLSfileName,[ConsolidateFields ; ConsolidateData],char(Current_SettleDate));
            
        end
        
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end