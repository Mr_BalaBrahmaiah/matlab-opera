clc
close all
clear

ObjDB = connect_to_database;

%%
hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');

Prompt        = {'Enter the Settle Date(yyyy-mm-dd format):'};
Name          = 'Convert_Prices_to_USD';
Numlines      = 1;
Defaultanswer = fetch(ObjDB,'select settlement_date from valuation_date_table');
User_InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

%%
InBUName = 'qf5';
[OutErrorMsg,OutFilename] =  Convert_Prices_to_USD(InBUName,User_InputDates);

waitbar(1,hWaitbar,'Convert-Prices-to-USD-QF5 Finished');