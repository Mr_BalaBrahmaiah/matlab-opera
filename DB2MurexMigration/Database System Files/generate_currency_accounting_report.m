function [OutErrorMsg,OutFilename] = generate_currency_accounting_report(InBUName)

%%% Project Name        :   Accounting Reports
%%% Module Name         :   Fx Report
%%% Matlab Version(s)   :   R2016b
%%% Company             :   Invenio Commodity Services Private Limited

%%% Author Name         :   INVENIO
%%% Date_Time           :   30 Jan 2019 / 16:12:16
%
%%% [OutErrorMsg,OutFilename] = generate_currency_accounting_report('cfs')

    OutErrorMsg = {'No errors'};
    OutFilename = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Fectch the data from currency report view
    ViewName = 'helper_risk_office_currency_report';
    [SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);

    if ~strcmpi(Data,'No Data')
        SettleData = cell2dataset([SettleColNames;Data]);  %% convert cell 2 dataset
        clear Data;
        
        %%% find the dead and live deals data
        IdxDead = strcmpi('dead',SettleData.deal_status);
        IdxLive = strcmpi('live',SettleData.deal_status);

        %%% initialize with zeros
        Realized = zeros(size(IdxDead));
        MTM = zeros(size(IdxLive));
        CumulativeMTM = zeros(size(IdxLive));

        % TODO :
        % 1. assuming all vaues are needed from mtm-usd; if needed from mtm-nc, needs
        % to be changed
        % End of TODO
        Realized(IdxDead) = SettleData.cumulative_mtm_usd(IdxDead);
        MTM(IdxLive) = SettleData.cumulative_mtm_usd(IdxLive);
        CumulativeMTM(IdxLive) = SettleData.cumulative_mtm_usd(IdxLive);

        %%% Original Lots
        USDExposure = SettleData.original_lots;
        FxExposure = SettleData.original_lots .* SettleData.original_premium;   

        % making the buy currency(USD Exposure) positive and sell currency(FX Exposure) as negative as per RMO
        % comments
        USDExposure = abs(USDExposure);
        FxExposure  = abs(FxExposure);

        %%%% Newly added code
        %%% find the dominating currency
        product_code = char(SettleData.product_code);

        buy_currency = cellstr(product_code(:,1:3));   %%% dominating currency
        sell_currency = cellstr(product_code(:,4:6));  

        %%% index for market_action
        IdxUSDSellCurr = strcmpi('sold',SettleData.market_action);  %% index for sold data
        IdxFxSellCurr  = ~IdxUSDSellCurr; % market_action = bought

        buy_bought_currency = buy_currency;
        sell_sold_currency = sell_currency;

        %%% based dominating currency and market_action, find the
        %%% buy_currency and sell_currency
        buy_currency(IdxFxSellCurr) = buy_bought_currency(IdxFxSellCurr);
        buy_currency(IdxUSDSellCurr) = sell_sold_currency(IdxUSDSellCurr);

        sell_currency(IdxUSDSellCurr) = sell_sold_currency(IdxUSDSellCurr);
        sell_currency(IdxUSDSellCurr) = buy_bought_currency(IdxUSDSellCurr);
        %%% End code

        SettleData.sell_currency = sell_currency;
        SettleData.buy_currency  = buy_currency;

        USDExposure(IdxUSDSellCurr) = USDExposure(IdxUSDSellCurr) * -1;
        FxExposure(IdxFxSellCurr)   = FxExposure(IdxFxSellCurr) * -1;

        %%% consolidation part
        UniqueFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
                        'buy_currency','sell_currency','settlement_date'};
        SumFields = {'USDExposure','FxExposure','Realized','MTM'};
        OutputFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
                        'buy_currency','sell_currency','USDExposure','FxExposure',...
                        'original_premium','settle_price','Realized','MTM','currency','settlement_date'};
        WeightedAverageFields = [];
        InData = [SettleData.counterparty,SettleData.contract_name,SettleData.maturity_date,SettleData.transaction_date,...
                    SettleData.trade_id,SettleData.buy_currency,SettleData.sell_currency,num2cell(USDExposure),...
                    num2cell(FxExposure),num2cell(SettleData.original_premium),num2cell(SettleData.settle_price),...
                    num2cell(Realized),num2cell(MTM),SettleData.currency,SettleData.settlement_date];
        [OutputFields,OutData] = consolidatedata(OutputFields, InData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

        %%% Replace the original_premium column name
        OutputFields = strrep(OutputFields,'original_premium','traded_price');

        %%% output file name
        Filename = ['FX_acc_report_',upper(InBUName)]; 
        OutFilename = getXLSFilename(Filename);
        
        %%% write the data into excel sheet
        xlswrite(OutFilename,[OutputFields;OutData]);
    else
        OutErrorMsg = {'No FX trades found!'};
    end

catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end



