function [OutErrorMsg,OutFilename] = generate_mo_fxfutures_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    Filename = ['FXFutures_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FXFUT'});
        Data = Data(ispresent_FUT_OFUT,:);
        if ~isempty(Data)
            
            UniqueFields = {'asset_class','product_code','product_name','portfolio','maturity_date'};
            SumFields = {'mtm_usd','settle_delta_1'};
            OutputFields = {'asset_class','product_code','product_name','portfolio','maturity_date',...
                'mtm_usd','settle_delta_1','nc_factor'};
            WeightedAverageFields = [];
            [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            OutHeader = {'Type','Product Code','Product','Portfolio','Maturity','Book_Value','Delta','Spot FX Rate'};
            
            xlswrite(OutFilename,[OutHeader;OutData]);
            
            %%% Newly added code
            ObjDB = connect_to_database;
            settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
            settle_date = repmat(settlement_date,size(OutData,1),1);
            %%%% Upload the Final Data
            if strcmpi(InBUName,'qf1') || strcmpi(InBUName,'qf2') || strcmpi(InBUName,'qf3') || strcmpi(InBUName,'qf4')
            try
                Table_Name = strcat('mo_fxfuture_report_store_table_',InBUName);
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');

                upload_in_database(Table_Name,[settle_date,OutData],ObjDB);
                disp('Upload Done Successfully to Database Table');           
            catch
                OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
            end
            end
            %%% end code
            
        else
            OutErrorMsg = {'No data found'};
        end
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end