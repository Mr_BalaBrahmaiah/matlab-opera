function run_recon_views_network

DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\OPERA_RECON_DUMPS\AGF';

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try
    %% generate and write the settle recon dump
    % ViewName = 'helper_settle_p_and_g_view';  helper_settle_p_and_g_view_cache_table_agf ; helper_settle_p_and_g_view_cache_table_cfs;
    ViewName = 'helper_settle_p_and_g_view_agf'; %% helper_settle_p_and_g_view_cache_table_agf
    Filename = [ViewName,'_',datestr(today),'.csv'];
    
    ObjDB = connect_to_database;
    %     [ColNames,Data] = read_from_database(ViewName,0);
    [ColNames,Data] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    % xlswrite(Filename,[ColNames;Data]);
    customcsvwrite(Filename,Data,ColNames);
    
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Settle Recon dump');
    
    %% generate and write the traders recon dump
    
    % ViewName = 'helper_traders_p_and_g_view';
    % % ViewName = 'helper_traders_p_and_g_view_cache_table';
    % Filename = [ViewName,'_',datestr(today),'.csv'];
    % [ColNames,Data] = read_from_database(ViewName,0);
    % % xlswrite(Filename,[ColNames;Data]);
    % customcsvwrite(Filename,Data,ColNames);
    %
    % OutFilename = fullfile(DestinationFolder,Filename);
    % move_reports(Filename,OutFilename,'Traders Recon dump');
    
catch ME
    write_log_file(LogFilename,ME.message);
end