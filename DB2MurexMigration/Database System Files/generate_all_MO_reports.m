function  [OutErrorMsg,TradersReportSummaryFilename,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName,...
    PnpRMSDumpFileName,MonthendEODReportFilename,CounterpartyReportFilename,...
    EODReportFilename,TradersReportMonthwiseFilename,DIRBookReportFilename] = generate_all_MO_reports(InBUName)

OutErrorMsg = {'No errors'};


if iscellstr(InBUName)
    InBUName = char(InBUName);
end

[OutErrorMsg1,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    TradersReportSummaryFilename = char(OutErrorMsg1);
end

[OutErrorMsg2,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    PnpOptionDumpFileName = char(OutErrorMsg2);
    PnpFutureDumpFileName = char(OutErrorMsg2);
    PnpMUsDumpFileName = char(OutErrorMsg2);
end

[OutErrorMsg3,PnpRMSDumpFileName] = generate_rms_dump(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    PnpRMSDumpFileName = char(OutErrorMsg3);
end

[OutErrorMsg4,CounterpartyReportFilename] = generate_counterparty_report(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    CounterpartyReportFilename = char(OutErrorMsg4);
end

[OutErrorMsg5,EODReportFilename] = generate_eod_report(InBUName);
if ~strcmpi(OutErrorMsg5,'No errors')
    EODReportFilename = char(OutErrorMsg5);
end

[OutErrorMsg6,DIRBookReportFilename] = generate_dirbook_report(InBUName);
if ~strcmpi(OutErrorMsg6,'No errors')
    DIRBookReportFilename = char(OutErrorMsg6);
end

[OutErrorMsg7,MonthendEODReportFilename] = generate_monthend_eod_report(InBUName);
if ~strcmpi(OutErrorMsg7,'No errors')
    MonthendEODReportFilename = char(OutErrorMsg7);
end


[OutErrorMsg8,TradersReportMonthwiseFilename] = generate_detailed_traders_report(InBUName);
if ~strcmpi(OutErrorMsg8,'No errors')
    TradersReportMonthwiseFilename = char(OutErrorMsg8);
end

if ~strcmpi(OutErrorMsg1,'No errors') && ...
        ~strcmpi(OutErrorMsg2,'No errors') && ...
        ~strcmpi(OutErrorMsg3,'No errors') && ...
        ~strcmpi(OutErrorMsg4,'No errors') && ...
        ~strcmpi(OutErrorMsg5,'No errors') && ...
        ~strcmpi(OutErrorMsg6,'No errors') && ...
        ~strcmpi(OutErrorMsg7,'No errors') && ...
        ~strcmpi(OutErrorMsg8,'No errors')
    OutErrorMsg = convertChar2Cell([char(OutErrorMsg1);char(OutErrorMsg2);...
        char(OutErrorMsg3);char(OutErrorMsg4);char(OutErrorMsg5);...
        char(OutErrorMsg6);char(OutErrorMsg7);char(OutErrorMsg8)]);
end


end