function [OutErrorMsg] = Refresh_Broker_Recon_table(InBUName,DBValueDate)

%%% Project Name: Broker Module
%%% Module Name: Broker Recon
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 07 Nov 2018 / 11:30:00

OutErrorMsg = {'No Errors'};

ObjDB = connect_to_database;

DBValueDate = datestr(DBValueDate,'yyyy-mm-dd');

%%% Delete data from Database (UAT)
Recon_TableName = strcat('broker_recon_report_table_',char(InBUName));

set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',char(Recon_TableName),' where st_date = ''',char(DBValueDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    %disp(curs.Message);
    OutErrorMsg = {curs.Message};
end
set(ObjDB,'AutoCommit','on');

%%% Connect to Opera DB Channel
ObjDB_Opera_Channel = connect_to_dbchannel;

%%%% Fetch data fron OperaDB
DBVarchar_Date = datestr(DBValueDate,'dd-mmm-yy');

TableName='broker_recon_report_table';
SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

[Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

if(isempty(Broker_Table_Data))
    %OutErrorMsg = {['Data is not available in recon table for ',char(DBValueDate)]};
     OutErrorMsg = {['For ',char(DBValueDate),' Data is not available in ',char(Recon_TableName)]};
    return;
end

if(strcmpi(Broker_Table_Data,'No Data'))
    OutErrorMsg = {['For ',char(DBValueDate),' Data is not available in ',char(Recon_TableName)]};
    return;
end

Pos_ST_Date = strcmpi(Broker_Tab_ColNames,'st_date');
Broker_Table_Data(:,Pos_ST_Date) = cellstr(datestr(Broker_Table_Data(:,Pos_ST_Date),'yyyy-mm-dd'));

%%% get last trade ID from broker_balance_report_table_(InBUName)
[~,DBTradeId]= getLastTradeId_Recon(InBUName);

%%% generate the fist col like rec_id
[r,~] = size(Broker_Table_Data);
for i=1:1:r
    NewTradeId = DBTradeId;
    rec_id(i,1) = cellstr(['REC-',num2str(NewTradeId)]);
    DBTradeId = DBTradeId+1;
end

%%% New Data
Upload_Recon_Data = [rec_id Broker_Table_Data];
upload_in_database(Recon_TableName,Upload_Recon_Data,ObjDB);

disp(['Refresh done successfully for ',Recon_TableName,' for COB:',DBVarchar_Date]);

end
