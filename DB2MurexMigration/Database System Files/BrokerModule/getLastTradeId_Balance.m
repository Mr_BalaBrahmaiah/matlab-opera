function [OutErrorMsg,LastTradeId]= getLastTradeId_Balance(InBUName)

    OutErrorMsg = {'No errors'};
    LastTradeId = {''};

    try
        if iscellstr(InBUName)
            InBUName = char(InBUName);
        end

        ObjDB = connect_to_database;

        Balance_TableName = strcat('broker_balance_report_table_',char(InBUName));
        SqlQuery = ['select mid(exp_id,5) from ',Balance_TableName,' where exp_id like ''','EXP-','%'''];
        DBTradeId = fetch(ObjDB,SqlQuery);

        if (isempty(DBTradeId))
            DBTradeId = 0;
        end

        TempTradeNum = max(str2num(char(DBTradeId)));

        if(isempty(TempTradeNum))
            TempTradeNum = 0;      %% Default
        end

         if ~isempty(TempTradeNum)
                LastTradeId = (TempTradeNum+1)';
         end 

    catch ME

        OutErrorMsg = cellstr(ME.message);

    end
end

