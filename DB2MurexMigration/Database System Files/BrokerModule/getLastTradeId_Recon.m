function [OutErrorMsg,LastTradeId]= getLastTradeId_Recon(InBUName)

OutErrorMsg = {'No errors'};
LastTradeId = {''};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    Balance_TableName = strcat('broker_recon_report_table_',char(InBUName));
    SqlQuery = ['select mid(rec_id,5) from ',Balance_TableName,' where rec_id like ''','REC-','%'''];
    DBTradeId = fetch(ObjDB,SqlQuery);

    if (isempty(DBTradeId))
        DBTradeId = 0;
    end
    
    TempTradeNum = max(str2num(char(DBTradeId)));
    
    if(isempty(TempTradeNum))
        TempTradeNum = 0;      %% Default
    end

     if ~isempty(TempTradeNum)
            LastTradeId = (TempTradeNum+1)';
     end 
     
catch ME
    
    OutErrorMsg = cellstr(ME.message);
    
end
