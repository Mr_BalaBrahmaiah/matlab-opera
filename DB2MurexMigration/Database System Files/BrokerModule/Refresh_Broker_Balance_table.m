function [OutErrorMsg] = Refresh_Broker_Balance_table(InBUName,DBValueDate)

%%% Project Name: Broker Module
%%% Module Name: Broker Balance or Broker Exposure
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 07 Nov 2018 / 11:20:00

OutErrorMsg = {'No Errors'};

ObjDB = connect_to_database;

DBValueDate = datestr(DBValueDate,'yyyy-mm-dd');

%%% Delete data from Database (UAT)
Balance_TableName = strcat('broker_balance_report_table_',char(InBUName));

set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',char(Balance_TableName),' where st_date = ''',char(DBValueDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    %disp(curs.Message);
    OutErrorMsg = {curs.Message};
end
set(ObjDB,'AutoCommit','on');

%%% Connect to Opera DB Channel
ObjDB_Opera_Channel = connect_to_dbchannel;

%%%% Fetch data fron OperaDBChannel
DBVarchar_Date = datestr(DBValueDate,'dd-mmm-yy');

TableName='broker_balance_report_table';
SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

[Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

if(isempty(Broker_Table_Data))
    OutErrorMsg = {['Data is not available in balance table for ',char(DBValueDate)]};
    return;
end

if(strcmpi(Broker_Table_Data,'No Data'))
    OutErrorMsg = {['Data is not available in balance table for ',char(DBValueDate)]};
    return;
end

%%% newly added line
Broker_Table_Data(:,7) = num2cell(cell2mat(Broker_Table_Data(:,7)) .* -1);  %%% newly added line
%%% end line
Pos_ST_Date = strcmpi(Broker_Tab_ColNames,'st_date');
Broker_Table_Data(:,Pos_ST_Date) = cellstr(datestr(Broker_Table_Data(:,Pos_ST_Date),'yyyy-mm-dd'));

%%% get last trade ID from broker_balance_report_table_(InBUName)
[~,DBTradeId]= getLastTradeId_Balance(InBUName);

%%% generate the fist col like exp_id
[r,~] = size(Broker_Table_Data);
for i=1:1:r
    NewTradeId = DBTradeId;
    exp_id(i,1) = cellstr(['EXP-',num2str(NewTradeId)]);
    DBTradeId = DBTradeId+1;
end

%%% New Data
Upload_Balance_Data = [exp_id Broker_Table_Data];
upload_in_database(Balance_TableName,Upload_Balance_Data,ObjDB);

disp(['Refresh done successfully for ',Balance_TableName,' for COB:',DBVarchar_Date]);

end
