objODBC = database.ODBCConnection('inveniotestingdbwebpage','user','invenio@123')

TElap1 = tic;
curs = exec(objODBC,'select * from helper_sensitivity_report_view_cache_table');
curs = fetch(curs);
Data = curs.Data;
Time1 = toc(TElap1)

TElap2 = tic;
curs = exec(objODBC,'select * from input_security_settlement_value_table');
curs = fetch(curs);
Data = curs.Data;
Time2 = toc(TElap2)

TElap5 = tic;
curs = exec(objODBC,'select * from input_security_traders_value_table');
curs = fetch(curs);
Data = curs.Data;
Time5 = toc(TElap5)


TElap3 = tic;
ViewName = 'helper_sensitivity_report_view_cache_table';
[Header,Data] = read_from_database(ViewName,0);
Time3 = toc(TElap3)

TElap4 = tic;
ViewName = 'input_security_settlement_value_table';
[Header,Data] = read_from_database(ViewName,0);
Time4 = toc(TElap4)

TElap6 = tic;
ViewName = 'input_security_traders_value_table';
[Header,Data] = read_from_database(ViewName,0);
Time6 = toc(TElap6)