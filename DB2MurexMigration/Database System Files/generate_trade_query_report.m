function [OutErrorMsg,OutFilename] = generate_trade_query_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    [ColNames,Data] = read_from_database('valuation_date_table',0);
    RefDate = cell2dataset([ColNames;Data]);
    SettlementDate = RefDate.settlement_date{1};
    
    ViewName = 'helper_risk_middle_office_reports';
    if (strcmpi(InBUName,'cfs') || strcmpi(InBUName,'usg') || strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2'))  %% newly added BU - OGP (2019-07-18)
        SqlQuery = ['SELECT * FROM helper_risk_middle_office_reports where transaction_date = ''', SettlementDate ,''' and settlement_source <> ''otc'' and market_action in (''bought'',''sold'') and trade_id not like ''NET%'''];
    else
        SqlQuery = ['SELECT * FROM helper_risk_middle_office_reports where transaction_date = ''', SettlementDate ,''' and market_action in (''bought'',''sold'') and trade_id not like ''NET%'''];
    end
    [SettleColNames,SettleData] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    
    if ~strcmpi(SettleData,'No Data')
        SettleData = cell2dataset([SettleColNames;SettleData]);        
                
        % remove the internal deals
        if strcmpi(InBUName,'cfs')
            IdxRemove = strncmpi(SettleData.counterparty_parent,'MM',2) | ...
                strncmpi(SettleData.counterparty_parent,'RMS',3);
            SettleData(IdxRemove,:) = [];
            
            IdxOR = strncmpi('OR',SettleData.product_code,2);
            SettleData.product_code(IdxOR) = {'OR'};
        end
        
        % IdxFXD = strcmpi(SettleData.group_type,'FXD');
        % SettleData(IdxFXD,:) = [];
        OriginalLots = SettleData.original_lots;
        %%% old logic  (10-04-2019)
%         NettedLots = SettleData.netted_lots;
%         IdxNettingStatus = strcmpi(SettleData.netting_status,'partially_netted');
%         if ~isempty(find(IdxNettingStatus))
%             OriginalLots(IdxNettingStatus) = OriginalLots(IdxNettingStatus) + NettedLots(IdxNettingStatus);
%         end
        
        NumRows = size(SettleData,1);
        
        CALLPUT = cell(NumRows,1);
        ENTITY = repmat(cellstr(InBUName),NumRows,1);
        TradeDate = SettleData.transaction_date{1};
        
%         [OptionMonth,~] = strtok(SettleData.contract_month,'.');
        
        %%% added on 2020-02-13, for handle spread_call and spread_put
        IdxCall = strcmpi('vanilla_call',SettleData.derivative_type) | strcmpi('spread_call',SettleData.derivative_type);
        CALLPUT(IdxCall) = {'CALL'};
        
        IdxPut = strcmpi('vanilla_put',SettleData.derivative_type) | strcmpi('spread_put',SettleData.derivative_type);
        CALLPUT(IdxPut) = {'PUT'};
        
        % OutFilename = getXLSFilename('TradeQuery');
        Filename = ['TradeQuery_',upper(InBUName)];
        OutFilename = getXLSFilename(Filename);
        
        
        Header = {'ENTITY','TRADENO','TRADE_DATE','INSTRUMENT','EXCHANGE','PRODUCT','COUNTERPARTY','TRADE_TYPE','OPT_TYPE','STRIKE','MONTH','LOTS','RATE'};
        OutData = [ENTITY,SettleData.trade_id,SettleData.transaction_date,SettleData.instrument,SettleData.exchange_name,SettleData.product_name,...
            SettleData.counterparty_parent,SettleData.group_type,CALLPUT,num2cell(SettleData.strike),SettleData.contract_month,num2cell(OriginalLots),num2cell(SettleData.original_premium_without_yield)];
        try
            ObjDB = connect_to_database;
            TableName = 'rmo_trade_query_report_table';
            set(ObjDB,'AutoCommit','off');
            SqlQuery = ['delete from ',TableName,' where trade_date = ''',TradeDate,''' and ENTITY = ''',InBUName,''''];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            upload_in_database(TableName, OutData);
        catch
        end
        
        OutData = [Header;OutData];
        xlswrite(OutFilename,OutData,'Sheet1');
        
        %%% Newly added logic on 2019-09-24
        if strcmpi('CFS',InBUName)
            %%% Filter the only OLAM Trades
            Index_olam = contains(SettleData.counterparty_parent,'olam','ignoreCase',true);
            %%% Required Header and Data
            Olam_Header = {'ENTITY','TRADENO','TRADE_DATE','INSTRUMENT','EXCHANGE','PRODUCT','COUNTERPARTY','Contract Number','TRADE_TYPE','OPT_TYPE','STRIKE','MONTH','LOTS','RATE'};
            Olam_OutData = [ENTITY(Index_olam),SettleData.trade_id(Index_olam),SettleData.transaction_date(Index_olam),SettleData.instrument(Index_olam),SettleData.exchange_name(Index_olam),SettleData.product_name(Index_olam),...
                   SettleData.counterparty_parent(Index_olam),SettleData.contract_number(Index_olam),SettleData.group_type(Index_olam),CALLPUT(Index_olam),num2cell(SettleData.strike(Index_olam)),SettleData.contract_month(Index_olam), ...
                   num2cell(OriginalLots(Index_olam)),num2cell(SettleData.original_premium_without_yield(Index_olam))];
            %%% Write inti Excel File
            xlswrite(OutFilename,[Olam_Header ; Olam_OutData],'OLAM Internal Deals');
        end
        %%% End Code

        try
            TempSetFlag = 0;
            if TempSetFlag
                UNIT = repmat({'VOL'},NumRows,1);
                TRD = cell(NumRows,1);
                TYPE1 = repmat({'OnFloor'},NumRows,1);
                BlankColumn = cell(NumRows,1);
                
                BROK = SettleData.counterparty_parent;
                CAT = SettleData.product_code;
                EXCH = SettleData.exchange_name;
                ConvFactor = SettleData.conv_factor;
                Lots = SettleData.original_lots;
                % Rate = SettleData.original_premium_without_yield ./ ConvFactor;
                Rate = SettleData.original_premium_without_yield ;
                
                [OptionMonth,~] = strtok(SettleData.contract_month,'.');
                
                IdxFut = strcmpi('future',SettleData.derivative_type);
                TRD(IdxFut) = {'FUT'};
                IdxFxFut = strcmpi('fx_future',SettleData.derivative_type);
                TRD(IdxFxFut) = {'FXFUT'};
                IdxFXD = strncmpi('fx_forward',SettleData.derivative_type,length('fx_forward'));
                TRD(IdxFXD) = {'FXD'};
                
                IdxCall = strcmpi('vanilla_call',SettleData.derivative_type);
                % CallStrike = SettleData.strike(IdxCall) ./ ConvFactor(IdxCall);
                CallStrike = SettleData.strike(IdxCall);
                
                CallValue = cellstr([repmat('C-',size(CallStrike)),num2str(CallStrike)]);
                TRD(IdxCall) = CallValue;
                
                IdxPut = strcmpi('vanilla_put',SettleData.derivative_type);
                % PutStrike = SettleData.strike(IdxPut) ./ ConvFactor(IdxPut);
                PutStrike = SettleData.strike(IdxPut);
                PutValue = cellstr([repmat('P-',size(PutStrike)),num2str(PutStrike)]);
                TRD(IdxPut) = PutValue;
                
                BOT_EX  = cell(NumRows,1);
                BOT_MON = cell(NumRows,1);
                BOT_LOT = cell(NumRows,1);
                BOT_RATE = cell(NumRows,1);
                
                SOLD_EX  = cell(NumRows,1);
                SOLD_MON = cell(NumRows,1);
                SOLD_LOT = cell(NumRows,1);
                SOLD_RATE = cell(NumRows,1);
                
                IdxBot  = strcmpi('bought',SettleData.market_action);
                IdxSold = strcmpi('sold',SettleData.market_action);
                
                BOT_EX(IdxBot)  = EXCH(IdxBot);
                BOT_MON(IdxBot) = OptionMonth(IdxBot);
                BOT_LOT(IdxBot) = num2cell(abs(SettleData.original_lots(IdxBot)));
                % BOT_RATE(IdxBot) = num2cell(SettleData.original_premium_without_yield(IdxBot) ./ ConvFactor(IdxBot));
                BOT_RATE(IdxBot) = num2cell(SettleData.original_premium_without_yield(IdxBot));
                
                SOLD_EX(IdxSold)  = EXCH(IdxSold);
                SOLD_MON(IdxSold) = OptionMonth(IdxSold);
                SOLD_LOT(IdxSold) = num2cell(abs(SettleData.original_lots(IdxSold)));
                % SOLD_RATE(IdxSold) = num2cell(SettleData.original_premium_without_yield(IdxSold) ./ ConvFactor(IdxSold));
                SOLD_RATE(IdxSold) = num2cell(SettleData.original_premium_without_yield(IdxSold));
                
                [ColNames,Data] = read_from_database('rmo_broker_code_table',0);
                RMOBrokCodeData = cell2dataset([ColNames;Data]);
                BROK_CODE = cell(NumRows,1);
                
                for iCode = 1:length(RMOBrokCodeData.broker_name)
                    IdxBrokCode = strcmpi(RMOBrokCodeData.broker_name(iCode),BROK);
                    BROK_CODE(IdxBrokCode) = RMOBrokCodeData.broker_code(iCode);
                end
                
%                 Filename = ['TradeQuery_',upper(InBUName)];
%                 OutFilename = getXLSFilename(Filename);
                
                % RMO needs this format for QF1,QF3,QF4
                if strcmpi('QF1',InBUName) || strcmpi('QF3',InBUName) || strcmpi('QF4',InBUName)
                    Header = {'TRADENO','ENTITY','INSTRUMENT','PRODUCT','TRADE_DATE','COUNTERPARTY','TRADE_TYPE','EXCHANGE','MONTH','LOTS','RATE'};
                    OutData = [SettleData.trade_id,SettleData.subportfolio,SettleData.instrument,SettleData.product_name,SettleData.transaction_date,...
                        BROK,TRD,EXCH,OptionMonth,num2cell(Lots),num2cell(Rate)];
                    
                    OutData = [Header;OutData];
                    xlswrite(OutFilename,OutData,'Sheet1');
                else  % RMO needs this format for QF2,AGF,CFS
                    Header = {'SL','ENTITY','TRN_DATE','BROKER','PRODUCT','PRODUCT_CODE','CAT','TRD',...
                        'SOLD_EX','SOLD_MON','SOLD_LOT','SOLD_RATE',...
                        'BOT_EX','BOT_MON','BOT_LOT','BOT_RATE',...
                        'REMARK'};
                    
                    OutData = [SettleData.trade_id,SettleData.legal_entity,SettleData.transaction_date,...
                        BROK,SettleData.product_name,BROK_CODE,CAT,TRD,...
                        SOLD_EX,SOLD_MON,SOLD_LOT,SOLD_RATE,...
                        BOT_EX,BOT_MON,BOT_LOT,BOT_RATE,...
                        BlankColumn];
                    
                    OutData = [Header;OutData];
                    xlswrite(OutFilename,OutData,'Sheet2');
                    
                end
                
            end
        catch
        end
        
        %%% Newly added code
        desk = repmat({upper(InBUName)},size(SettleData,1),1);
        trade_id = SettleData.trade_id;
        transaction_date = SettleData.transaction_date;
        product_code = SettleData.product_code;
        product_name = SettleData.product_name;
        exchange_name = SettleData.exchange_name;
        group_type = SettleData.group_type;
        derivative_type = SettleData.derivative_type;
        instrument = SettleData.instrument;
        security_id = SettleData.security_id;
        subportfolio = SettleData.subportfolio;
        counterparty_parent = SettleData.counterparty_parent;
        contract_month = SettleData.contract_month;
        strike = num2cell(SettleData.strike);
        original_lots = num2cell(SettleData.original_lots);
        original_premium = num2cell(SettleData.original_premium);
        current_premium = num2cell(SettleData.current_premium);
        p1_settleprice = num2cell(SettleData.p1_settleprice);
        settle_price = num2cell(SettleData.settle_price);
        mult_factor = num2cell(SettleData.mult_factor);
        currency = SettleData.currency;
        maturity_date = SettleData.maturity_date;
        source_system = repmat({'OPERA'},size(SettleData,1),1);
        Current_Date = datestr(now, 'yyyy-mm-dd');
        updated_date = repmat({Current_Date},size(SettleData,1),1);


        ORS_TradeQuery_Data = [desk, trade_id, transaction_date,product_code, product_name, exchange_name, group_type, derivative_type, ...
                        instrument, security_id, subportfolio, counterparty_parent, contract_month, strike, original_lots, original_premium, ...
                        current_premium, p1_settleprice, settle_price, mult_factor, currency, maturity_date, source_system, updated_date];

        %%% Upload the data into Database when BU is 'USG' & 'OGP' & 'OG2'
        %%% (OG2 added on 24-01-2020)
        if  (strcmpi(InBUName,'usg') || strcmpi(InBUName,'ogp') || strcmpi(InBUName,'og2')) %% newly added BU - OGP (2019-07-18)
            try
                ObjDB = connect_to_database;
                TableName = ['ors_trade_query_report_table_',char(InBUName)];
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',TableName,' where transaction_date = ''',TradeDate,''' and desk = ''',InBUName,''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');

                upload_in_database(TableName, ORS_TradeQuery_Data);
            catch
                errorMessage = {['Data upload got failed in ',char(TableName)]};
                configure_mail_settings;
                sendmail({'Raghavendra.Sn@olamnet.com','amrik.barman@olamnet.com'}, 'USG : Trade Query RMO report',['Error : ',char(errorMessage)]);
            end  
        end
        %%% End code
    else
        OutErrorMsg = {'No trades found for the trade date!'};
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
end