function [OutErrorMsg,OutFilename] = generate_pandl_monitoring_report(InBUName)
%  Generate the options reports in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/28 11:00:27 $
%  $Revision: 1.9 $
%
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('PnL_Monitoring_Report');
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    if ~strcmpi(Data,'No Data')        
       
         Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind_exact(Get_GroupType,{'FUT','OFUT'});
        Data = Data(ispresent_FUT_OFUT,:);
   
        InData = cell2dataset([ColNames;Data]);
        SettleDate = InData.settlement_date(1);
        
        IdxBlankContractMonth = strcmpi('',InData.contract_month);
        InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(char(InBUName),InData,1);
        
        UniqueFields = {'PORTFOLIO','PRODUCT','GRP','CONTRACT_MONTH'};
        SumFields={'Total_USD','ActiveLots','Delta'};
        OutputFields = [UniqueFields,SumFields,'future_price','mult_factor','nc_factor','fx_nc_factor'];
        WeightedAverageFields = [];
        [OutputFields,Data] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;Data]);
        NetPosVal = TempOutData.Delta .*  TempOutData.mult_factor .* TempOutData.future_price  .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        PortfolioVal = abs(TempOutData.Delta) .*  TempOutData.mult_factor .* TempOutData.future_price  .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        SettlementDate = repmat(SettleDate,size(NetPosVal));
        Data(:,end-4:end) = [];
        OutData = [SettlementDate,Data,num2cell(NetPosVal),num2cell(PortfolioVal)];
      ColNames = {'settlement_date','subportfolio','product_name','group_type','contract_month','mtm_usd','active_lots','NetPosVal','PortfolioVal'};
        
        %% ACTUAL CODE NEEDED
%         UniqueFields = {'settlement_date'};
%         SumFields = {'active_lots','mtm_usd','NetPosVal','PortfolioVal'};
%         OutputFields = {'settlement_date','active_lots','mtm_usd',...
%         'mtm_usd','NetPosVal','PortfolioVal'};
%         WeightedAverageFields = [];
%         [~,OutSummaryData] = consolidatedata(ColNames, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
%         
%         OutHeader = {'RPT_DATE','NOMINAL','PNL_DAY','FTY PNL','NETPOS_VAL','PORT_VAL'};            
%         xlswrite(OutFilename,[OutHeader;OutSummaryData],'Summary');
        UniqueFields = {'settlement_date'};
        SumFields = {'NetPosVal','PortfolioVal'};
        OutputFields = {'settlement_date','NetPosVal','PortfolioVal'};
        WeightedAverageFields = [];
        [~,OutSummaryData] = consolidatedata(ColNames, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        OutHeader = {'RPT_DATE','NETPOS_VAL','PORT_VAL'};            
        xlswrite(OutFilename,[OutHeader;OutSummaryData],'Summary');
         %% End of  ACTUAL CODE NEEDED
         
         
%         %% TEMP CODE TO SHOW the BREAKUP
%         UniqueFields = {'settlement_date','subportfolio','product_code','group_type','contract_month'};
%         SumFields = {'active_lots','settle_delta_1','mtm_usd','PortfolioVal','NetPosVal'};
%         OutputFields = {'settlement_date','subportfolio','product_code','group_type','contract_month',...
%             'active_lots','settle_delta_1','mtm_usd',...
%             'mult_factor','future_price','PortfolioVal','NetPosVal'};
%         WeightedAverageFields = [];
%         [OutputFields,OutDetailsData] = consolidatedata(ColNames, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
%         
%         xlswrite(OutFilename,[OutputFields;OutDetailsData],'DetailedReport');
%         %% END OF TEMP CODE
        
        try
            xls_delete_sheets(fullfile(pwd,OutFilename));
        catch
        end
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end