function [OutErrorMsg ,OutXLSName] = generate_average_swap_expiry_report(InBUName)

%%% Project Name        :   Expiry processing
%%% Module Name         :   AVG SWAP Expiry processing
%%% Matlab Version(s)   :   R2016b
%%% Company             :   Invenio Commodity Services Private Limited

%%% Author Name         :   INVENIO
%%% Date_Time           :   25 Feb 2019 / 15:20:45
%
%%% [OutErrorMsg ,OutXLSName ] = generate_average_swap_expiry_report('cfs')

try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['average_swap_expiry_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%% get settlement date
    Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');

    %%% Fetch data from AVG SWAP expiry View
    AVG_SWAP_Expiry_View = ['helper_avg_swap_expiry_processing_view_',char(InBUName)];
    SqlQuery_AVG_SWAP_Expiry_View_Data = ['select * from ',char(AVG_SWAP_Expiry_View)] ;
    [AVG_SWAP_Expiry_View_Cols,AVG_SWAP_Expiry_View_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_AVG_SWAP_Expiry_View_Data);

    if strcmpi(AVG_SWAP_Expiry_View_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(AVG_SWAP_Expiry_View)]} ;
        return;
    end
    %AVG_SWAP_Expiry_View_Data = cell2dataset([AVG_SWAP_Expiry_View_Cols;AVG_SWAP_Expiry_View_Table_Data]); %%% convert cell to dataset
    
    %%% Fetch data from AVG SWAP Trade Table
    AVG_SWAP_Trade_Table = 'input_avgswap_tradecreation_table';
    SqlQuery_AVG_SWAP_Trade_Table_Data = ['select * from ',char(AVG_SWAP_Trade_Table)] ;
    [AVG_SWAP_Trade_Table_Cols,AVG_SWAP_Trade_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_AVG_SWAP_Trade_Table_Data);

    if strcmpi(AVG_SWAP_Trade_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(AVG_SWAP_Trade_Table)]} ;
        return;
    end
    AVG_SWAP_Trade_Data = cell2dataset([AVG_SWAP_Trade_Table_Cols;AVG_SWAP_Trade_Table_Data]); %%% convert cell to dataset
    
    %%% Find holidays list
    Holidays_List_Table = 'holidays_list_table';
    SqlQuery_Holidays_List_Table = ['select invenio_product_code, holidays from ',char(Holidays_List_Table)] ;
    [Holidays_List_Table_Cols,Holidays_List_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Holidays_List_Table);

    if strcmpi(Holidays_List_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Holidays_List_Table)]} ;
        return;
    end 
    Holidays_List_Data = cell2dataset([Holidays_List_Table_Cols;Holidays_List_Table_Data]); %%% convert cell to dataset
    
    %%% consolidation part of AVG SWAP Expiry Data
    Consolidate_AVG_Header =  AVG_SWAP_Expiry_View_Cols;
    Consolidate_AVG_Data = AVG_SWAP_Expiry_View_Table_Data;

    UniqueFields = {'subportfolio', 'security_id', 'market_action','counterparty_parent', 'instrument'}; 
    SumFields = {'active_lots'};
    OutputFields = Consolidate_AVG_Header;
    WeightedAverageFields = [];
    [Consolidate_AVG_Data_Header,Consolidate_AVG_Data] = consolidatedata(Consolidate_AVG_Header, Consolidate_AVG_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    %%% Fetch data from Underlying Settle Value Table
    AVG_SWAP_Expiry_Data= cell2dataset([Consolidate_AVG_Data_Header ; Consolidate_AVG_Data]);
    underlying_id_sql_Query = ['''',strrep(convertCell2Char(AVG_SWAP_Expiry_Data.underlying_id),',',''','''),''''];
    spot_id_sql_Query = ['''',strrep(convertCell2Char(AVG_SWAP_Expiry_Data.spot),',',''','''),''''];
    Spot_underlying_id_sql_query = [underlying_id_sql_Query,spot_id_sql_Query];
    
    %%% Fetch data from Underlying Settle Value Table
    Underlying_settle_value_Table = 'underlying_settle_value_table';
    SqlQuery_Underlying_settle_value = ['select underlying_id, settle_value, settlement_date from ',char(Underlying_settle_value_Table),' where settlement_date=''',char(Settele_date),''' and underlying_id in (',Spot_underlying_id_sql_query,') '] ;
    [Underlying_settle_value_Cols,Underlying_settle_value_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Underlying_settle_value);

    if strcmpi(Underlying_settle_value_Table_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Underlying_settle_value_Table),' for ',char(Settele_date)]} ;
        return;
    end
    Underlying_settle_value_Data = cell2dataset([Underlying_settle_value_Cols;Underlying_settle_value_Table_Data]); %%% convert cell to dataset
    
    %%% Holidays List (Only 1st January dates), to calculate Bussiness days
    DB_Date_Format = 'yyyy-mm-dd';
    HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');

    %%% Find the AVG SWAP data using settele_date (to get total business
    %%% working days)
    Index_trade_date = (datenum(AVG_SWAP_Trade_Data.avg_start_date,DB_Date_Format) <= datenum(Settele_date,DB_Date_Format) & datenum(Settele_date,DB_Date_Format) <= datenum(AVG_SWAP_Trade_Data.avg_end_date,DB_Date_Format));
    if ~any(Index_trade_date) 
        OutErrorMsg = {[char(Settele_date),' date is not found in mapping table ''',char(AVG_SWAP_Trade_Table),''' range']} ;
        return;
    end
    Business_Days_Data = AVG_SWAP_Trade_Data(Index_trade_date,:);
    
    %%% Mapping process
    [row,~] = size(Business_Days_Data);  User_AVG_SWAP_Data = []; Overall_ErrorMsg = [];
    for i = 1:1:row
        
        product_code = Business_Days_Data.product_code(i);
        contract_month = Business_Days_Data.contract_month(i);
        maturity_date = Business_Days_Data.maturity_date(i);
        avg_start_date = Business_Days_Data.avg_start_date(i);
        avg_end_date = Business_Days_Data.avg_end_date(i);

        %%% find the holidays data using product code mapping
        Index_product_code = strcmpi(product_code,Holidays_List_Data.invenio_product_code);
        if any(Index_product_code) 
            Holidays_Data = Holidays_List_Data(Index_product_code,'holidays');
            Holidays_Data_cell = dataset2cell(Holidays_Data);
            Holidays_Data_list = datenum(Holidays_Data_cell(2:end,:),'yyyy-mm-dd');
            HolidayVec_list = [HolidayVec ; Holidays_Data_list];
            
            %%% Find business days from start_date to end_date
            VBusDays = busdays(datenum(avg_start_date,DB_Date_Format), datenum(avg_end_date,DB_Date_Format),'daily',HolidayVec_list);
            BusDays_Array = cellstr(datestr(VBusDays,DB_Date_Format));
            Total_BusDays_Count = size(BusDays_Array,1);
        else
            VBusDays = busdays(datenum(avg_start_date,DB_Date_Format), datenum(avg_end_date,DB_Date_Format),'daily',HolidayVec);
            BusDays_Array = cellstr(datestr(VBusDays,DB_Date_Format));
            Total_BusDays_Count = size(BusDays_Array,1);
        end

        if any(strcmpi(Settele_date,Holidays_Data.holidays))
            Overall_ErrorMsg = [Overall_ErrorMsg;{['For ',char(Settele_date),' No deals found to generate, it may be a holiday or no actual deals']}];
            continue;
        end
        
        %%% find product_code index
        Indx_product_code = regexpi(AVG_SWAP_Expiry_Data.underlying_id,product_code);
        Ind_empty = cellfun(@(v) isempty(v),Indx_product_code);
        Indx_product_code(Ind_empty) = {0}; 

        %%% compare contract month, maturity date and product code
        Matched_Index = strcmpi(contract_month,AVG_SWAP_Expiry_Data.contract_month) & strcmpi(maturity_date,AVG_SWAP_Expiry_Data.maturity_date) & cell2mat(Indx_product_code);
        if any(Matched_Index)
            %%% Matched AVG SWAp data
            Current_AVG_SWAP_E_Data = AVG_SWAP_Expiry_Data(Matched_Index,:);
            
            [row1,~] = size(Current_AVG_SWAP_E_Data);
            for r = 1:1:row1
                %%% Calculation of Original lots (Logic:- Original lots/Total
                %%% Business days)
                original_lots = round((Current_AVG_SWAP_E_Data.active_lots(r) ./ Total_BusDays_Count),6);

                %%% get the market action (just reverse the existing one)
                if strcmpi(Current_AVG_SWAP_E_Data.market_action(r),'BOUGHT')
                    current_market_action = {'SOLD'};
                    original_lots = original_lots * -1;
                elseif strcmpi(Current_AVG_SWAP_E_Data.market_action(r),'SOLD')
                    current_market_action = {'BOUGHT'};
                    original_lots = original_lots * -1;
                end

                %%% find the settele value and spot value using underlying id
                Settle_value = Underlying_settle_value_Data(strcmpi(Current_AVG_SWAP_E_Data.underlying_id(r),Underlying_settle_value_Data.underlying_id),'settle_value');
                Spot_value = Underlying_settle_value_Data(strcmpi(Current_AVG_SWAP_E_Data.spot(r),Underlying_settle_value_Data.underlying_id),'settle_value');

                %%% calculation of premium (Logic:- underlying_id value /
                %%% Spot_value)
                if strcmpi(Current_AVG_SWAP_E_Data.quanto_type(r),'DIVISION')
                    premium = round((Settle_value.settle_value ./ Spot_value.settle_value),6);
                elseif strcmpi(Current_AVG_SWAP_E_Data.quanto_type(r),'MULTIPLICATION')
                    premium = round((Settle_value.settle_value .* Spot_value.settle_value),6);
                end

                %%% required fields
                subportfolio = Current_AVG_SWAP_E_Data.subportfolio(r);
                security_id = Current_AVG_SWAP_E_Data.security_id(r);
                instrument = Current_AVG_SWAP_E_Data.instrument(r);
                counterparty_parent = Current_AVG_SWAP_E_Data.counterparty_parent(r);
                trade_group_type = Current_AVG_SWAP_E_Data.group_type(r);

                %%% user required fileds
                User_Want_AVG_SWAP_Data = [subportfolio, security_id, instrument, counterparty_parent, current_market_action, original_lots, premium, trade_group_type];
                User_AVG_SWAP_Data = vertcat(User_AVG_SWAP_Data, User_Want_AVG_SWAP_Data);
            end
        else
            Overall_ErrorMsg = [Overall_ErrorMsg;{['No mapping found with AVG SWAP data for corresponding Product Code: ',char(product_code),' and Contract Month: ',char(contract_month),' and Maturity Date: ',char(maturity_date)]}];
            continue;
        end
    end
    
    if isempty(User_AVG_SWAP_Data)
        OutErrorMsg = {['For ',char(Settele_date),' No deals found to generate, it may be a holiday or no actual deals']};
        return;
    end
    
    %%% Convert cell to dataset using AVG SWAP Header and Data
    User_Want_AVG_SWAP_Cols = {'subportfolio', 'security_id', 'instrument', 'counterparty_parent', 'market_action', 'original_lots', 'premium', 'trade_group_type'};
    Required_AVG_SWAP_Data = cell2dataset([User_Want_AVG_SWAP_Cols;User_AVG_SWAP_Data]);
    
    %%% get last trade ID without prefix
    [~,~,DBTradeId]= getLastTradeId(InBUName);

    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
    TempTradeId = DBTradeId;
    if(isempty(TempTradeId))
        TempTradeId = 0;      %% Default
    end
    NumRows = size(Required_AVG_SWAP_Data,1);
    NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
    TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
    TradeId = strrep(TradeId,' ','');  %% Remove Spaces

    %%%% user required fields      
    trade_id = TradeId;
    parent_trade_id = TradeId;
    transaction_date = repmat(Settele_date,size(Required_AVG_SWAP_Data,1),1);
    subportfolio = Required_AVG_SWAP_Data.subportfolio;
    instrument = Required_AVG_SWAP_Data.instrument;
    trade_group_type = Required_AVG_SWAP_Data.trade_group_type;
    security_id = Required_AVG_SWAP_Data.security_id;
    market_action = Required_AVG_SWAP_Data.market_action;
    original_lots = num2cell(Required_AVG_SWAP_Data.original_lots);
    premium = num2cell(Required_AVG_SWAP_Data.premium);
    counterparty_parent = Required_AVG_SWAP_Data.counterparty_parent;
    clearing_broker = counterparty_parent;
    is_netting_done = repmat({0},size(Required_AVG_SWAP_Data,1),1);
    lots_to_be_netted = original_lots;
    deal_status = repmat({'live'},size(Required_AVG_SWAP_Data,1),1);
    version_created_by = repmat({'system'},size(Required_AVG_SWAP_Data,1),1);
    version_comments = repmat({'system generated swap'},size(Required_AVG_SWAP_Data,1),1);

    counterparty_bu = cell(size(Required_AVG_SWAP_Data,1),1);
    counterparty_entity = cell(size(Required_AVG_SWAP_Data,1),1);
    execution_broker = cell(size(Required_AVG_SWAP_Data,1),1);
    reconed_with_clearer = cell(size(Required_AVG_SWAP_Data,1),1);
    reconed_completed_by = cell(size(Required_AVG_SWAP_Data,1),1);
    recon_datetimestamp = cell(size(Required_AVG_SWAP_Data,1),1);
    contract_number = cell(size(Required_AVG_SWAP_Data,1),1);
    subcontract_number = cell(size(Required_AVG_SWAP_Data,1),1);
    opt_periodicity = cell(size(Required_AVG_SWAP_Data,1),1);
    date_to_be_fixed = cell(size(Required_AVG_SWAP_Data,1),1);
    trader_name = cell(size(Required_AVG_SWAP_Data,1),1);
    traded_timestamp = cell(size(Required_AVG_SWAP_Data,1),1);
    exe_type = cell(size(Required_AVG_SWAP_Data,1),1);
    brokerage_rate_type = cell(size(Required_AVG_SWAP_Data,1),1);
    spcl_rate = cell(size(Required_AVG_SWAP_Data,1),1);
    other_remarks = cell(size(Required_AVG_SWAP_Data,1),1);
    version_no = cell(size(Required_AVG_SWAP_Data,1),1);
    version_timestamp = cell(size(Required_AVG_SWAP_Data,1),1);
    ops_unique = cell(size(Required_AVG_SWAP_Data,1),1);
    broker_lots = cell(size(Required_AVG_SWAP_Data,1),1);
    ops_action = cell(size(Required_AVG_SWAP_Data,1),1);

    %%% User want Data (Final AVG SWAP Expiry Header)
    AVG_SWAP_Expiry_Final_Header = {'trade_id', 'parent_trade_id', 'transaction_date', 'subportfolio', 'instrument', 'trade_group_type', 'security_id', 'market_action', ...
                                'original_lots', 'premium', 'counterparty_parent', 'counterparty_bu', 'counterparty_entity', 'execution_broker', 'clearing_broker', ...
                                'reconed_with_clearer', 'reconed_completed_by', 'recon_datetimestamp', 'contract_number', 'subcontract_number', 'opt_periodicity', ...
                                'is_netting_done', 'lots_to_be_netted', 'date_to_be_fixed', 'trader_name', 'traded_timestamp', 'deal_status', 'exe_type', 'brokerage_rate_type', ...
                                'spcl_rate', 'other_remarks', 'version_no', 'version_created_by', 'version_timestamp', 'version_comments', 'ops_unique', 'broker_lots', 'ops_action'};
    
    %%% User want Data (Final AVG SWAP Expiry Data)
    AVG_SWAP_Expiry_Final_Data = [trade_id, parent_trade_id, transaction_date, subportfolio, instrument, trade_group_type, security_id, market_action, ...
                                original_lots, premium, counterparty_parent, counterparty_bu, counterparty_entity, execution_broker, clearing_broker, ...
                                reconed_with_clearer, reconed_completed_by, recon_datetimestamp, contract_number, subcontract_number, opt_periodicity, ...
                                is_netting_done, lots_to_be_netted, date_to_be_fixed, trader_name, traded_timestamp, deal_status, exe_type, brokerage_rate_type, ...
                                spcl_rate, other_remarks, version_no, version_created_by, version_timestamp, version_comments, ops_unique, broker_lots, ops_action];
    
    %%% Final cell data with Header and Data & write the data into excel sheet
    Total_AVG_SWAP_Expiry_Data = [AVG_SWAP_Expiry_Final_Header;AVG_SWAP_Expiry_Final_Data];
    xlswrite(OutXLSName,Total_AVG_SWAP_Expiry_Data,'AVG_SWAP_Expiry_Data');
    
    if ~isempty(Overall_ErrorMsg) %% Error meassage
        xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Sheet');
        %%%% Active user sheet
        xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Error_Sheet'); 
    end

    %%%% Delete Empty sheets and active user sheet
    xls_delete_sheets(fullfile(pwd,OutXLSName));
 
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);

end
