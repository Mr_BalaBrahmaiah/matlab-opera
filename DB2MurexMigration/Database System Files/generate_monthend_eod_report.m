function [OutErrorMsg,OutFilename] = generate_monthend_eod_report(InBUName)
%  Generate the monthend EOD report for accounts team
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/05/15 07:28:37 $
%  $Revision: 1.10 $
%

OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    %     ViewName = 'helper_eod_monthend_report_view';
    %     [InputHeader,InputData] = read_from_database(ViewName,0,'',InBUName);
    
    ObjDB = connect_to_database;
    ViewName = strcat('helper_eod_monthend_report_view_',char(InBUName));
    [InputHeader,InputData] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    InData = cell2dataset([InputHeader;InputData]);
    clear InputData;
    
    [OutputFields,ConsDeals] = compute_monthend_eod_values(InBUName,InData);
    
    OutFilename = getXLSFilename(['Monthend_EODReport_',InBUName]);

    try
        ConsDeals = sortrows(ConsDeals,[5,3]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    xlswrite(OutFilename,[OutputFields;ConsDeals]);
    
    %%% Newly added code
    ObjDB = connect_to_database;
    settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    settle_date = repmat(settlement_date,size(ConsDeals,1),1);
    %%%% Upload the Final Data
    if (strcmpi(InBUName,'cfs') || strcmpi(InBUName,'usg'))
    try
        Table_Name = strcat('accounting_report_store_table_',InBUName);
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

        upload_in_database(Table_Name,[settle_date,ConsDeals],ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
    end
    end
    %%% end code
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end