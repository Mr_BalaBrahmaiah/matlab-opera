HolDates = {'2016-01-01';'2016-12-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2017-02-22'), datenum('2017-02-24'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1),'yyyy-mm-dd'));

objDB = connect_to_database;

for i = 1:length(VBusDays)
    
    SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    
    % do pricing before running the reports
    
    derivativepricerinterface('agf','settle',1,'system');
    derivativepricerinterface('qf1','settle',1,'system');
    derivativepricerinterface('qf2','settle',1,'system');
    derivativepricerinterface('qf3','settle',1,'system');
    derivativepricerinterface('orx','settle',1,'system');
    
    %% generate mo reports
    generate_ff_portfolio_report('agf');
%     generate_pandl_monitoring_report('agf');
%     AGF_SecondCut_Reports('agf');
%     generate_consolidated_QF_reports;
%     
%     generate_mo_summary_report('qf1');
%     generate_mo_summary_monthwise_report('qf1');
%     generate_mo_fxforwards_report('qf1');
%     generate_mo_fxfutures_report('qf1');
%     
%      generate_mo_summary_report('qf2');
%     generate_mo_summary_monthwise_report('qf2');
%     generate_mo_fxforwards_report('qf2');
%     generate_mo_fxfutures_report('qf2');
%     
%      generate_mo_summary_report('qf3');
%     generate_mo_summary_monthwise_report('qf3');
%     generate_mo_fxforwards_report('qf3');
%     generate_mo_fxfutures_report('qf3');
    %% generate ro reports
    generate_daily_summary_report('agf');
    generate_daily_greeks_report('agf');
    
    generate_daily_summary_report('qf1');
    generate_daily_greeks_report('qf1');
    
       generate_daily_summary_report('qf2');
    generate_daily_greeks_report('qf2');
    
       generate_daily_summary_report('qf3');
    generate_daily_greeks_report('qf3');
    
       generate_daily_summary_report('orx');
    generate_daily_greeks_report('orx');
    
    % generate accounting reports
    generate_accounting_report('agf');
generate_accounting_report('qf1');
generate_accounting_report('qf2');
generate_accounting_report('qf3');
generate_accounting_report('orx');
end