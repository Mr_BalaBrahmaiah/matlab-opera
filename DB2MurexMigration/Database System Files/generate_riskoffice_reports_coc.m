function OutFilename = generate_riskoffice_reports_coc(InBUName)
% To generate the daily greeks reports

Filename = ['Daily_Greeks_Report_',upper(InBUName)];
OutFilename = getXLSFilename(Filename);

ViewName = ['helper_settle_pricing_subportfoliovalues_view_',InBUName];
[ColNames,Data] = read_from_database(ViewName,0);
SettleData = cell2dataset([ColNames;Data]);

TableName = 'product_master_table' ; %% 'dbtodw_mapping';
[ColNames,Data] = read_from_database(TableName,0);
DBMappingData = cell2dataset([ColNames;Data]);

% Remove all dead deals and just consider only live deals
ValueDate    = datenum(SettleData.value_date,'yyyy-mm-dd');
TempMaturity = SettleData.maturity_date;
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
% IdxLive = ValueDate <= MaturityDate;
IdxDead = ValueDate > MaturityDate;
SettleData(IdxDead,:) = [];

% Consider only FUT,OFUT and OTC trades and ignore others like SWAP,FXD and
% FX_OPT
IdxSelGrp = strcmpi(SettleData.group_type,'FUT') |  ...
    strcmpi(SettleData.group_type,'OFUT') | ...
    strcmpi(SettleData.group_type,'OTC');
SettleData(~IdxSelGrp,:) = [];

% Construct the call/put field from the derivative_type field
CallPut = cell(size(SettleData.derivative_type));
ProductName = cell(size(SettleData.derivative_type));
Exchange = cell(size(SettleData.derivative_type));
IdxCall = cellStrfind(SettleData.derivative_type,'_call');
IdxPut = cellStrfind(SettleData.derivative_type,'_put');
IdxFuture = cellStrfind(SettleData.derivative_type,'future');
CallPut(IdxCall) = {'call'};
CallPut(IdxPut) = {'put'};
CallPut(IdxFuture) = {'future'};

% Construct the Productname and exchange field needed in reports
for iProd = 1:length(DBMappingData.invenio_product_code)
    %     IdxProd = strcmpi(DBMappingData.invenio_product_code{iProd},SettleData.product_code);  %% 'dbtodw_mapping';
    IdxProd = strcmpi(strtrim(DBMappingData.invenio_product_code{iProd}),strtrim(SettleData.invenio_product_code)); %% 'product_master_table'
    
    ProductName(IdxProd) = DBMappingData.product_name(iProd);
    Exchange(IdxProd) = DBMappingData.exchange_name(iProd);
end

% arrive at the calculated fields
Delta = SettleData.settle_delta_1 + SettleData.settle_delta_2;
Gamma = SettleData.settle_gamma_11 + SettleData.settle_gamma_12 + ...
    SettleData.settle_gamma_21 + SettleData.settle_gamma_22;
Vega = SettleData.settle_vega_1 + SettleData.settle_vega_2;
Theta = SettleData.settle_theta;
DeltaMT = Delta .* SettleData.lot_mult1;

% construct the data only with the required fields before proceeding with
% further reports creation
Data = [SettleData.subportfolio,SettleData.instrument,ProductName,...
    Exchange,SettleData.group_type,num2cell(SettleData.strike),...
    CallPut,SettleData.contract_month,...
    num2cell(SettleData.active_lots),num2cell(Delta),...
    num2cell(DeltaMT),num2cell(Gamma),...
    num2cell(Vega),num2cell(Theta),num2cell(SettleData.current_premium),SettleData.invenio_product_code];
ColNames = {'subportfolio','instrument','ProductName','Exchange',...
    'group_type','strike','CallPut','contract_month',...
    'active_lots','Delta','DeltaMT','Gamma','Vega','Theta','Premium','ProductCode'};

% construct the report-1 "Overall"
% assuming we need live FUT, OFUT and OTC trades for this report
UniqueFields = {'ProductName','instrument','Exchange','group_type'};
SumFields = {'active_lots','Delta','DeltaMT','Gamma','Vega','Theta'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[~,ReportData] = consolidatedata(ColNames, Data,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);
ReportHeader = {'Product','Units','Market','Group','Live Pos Lot',...
    'Delta Lot','Delta MT','Gamma USD MT','Vega <USD>','Theta <USD>'};
PosNominal = strcmpi('Live Pos Lot',ReportHeader);
TempZeroVar = zeros(size(ReportData(:,PosNominal),1),1);
IdxZeroActiveLots = isnumericequal(cell2mat(ReportData(:,PosNominal)),TempZeroVar);
ReportData(IdxZeroActiveLots,:) = [];

xlswrite(OutFilename,[ReportHeader;ReportData],'Overall');

% construct the report-2 "Summary"
UniqueFields = {'subportfolio','ProductName','strike','CallPut','contract_month'};
SumFields = {'active_lots','Delta','DeltaMT','Gamma','Vega','Theta'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[~,ReportData] = consolidatedata(ColNames, Data,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);
ReportHeader = {'Portfolio','Product','Strike','Call/Put','Maturity Label',...
    'Nominal','Delta Lots','Delta MT','Gamma USD MT','Vega <USD>','Theta'};
PosNominal = strcmpi('Nominal',ReportHeader);
TempZeroVar = zeros(size(ReportData(:,PosNominal),1),1);
IdxZeroActiveLots = isnumericequal(cell2mat(ReportData(:,PosNominal)),TempZeroVar);
ReportData(IdxZeroActiveLots,:) = [];
xlswrite(OutFilename,[ReportHeader;ReportData],'Summary');

            %%% Newly added code
            %%% Replace empty cells with zeros
            Strike = ReportData(:,3);
            Index_nan = cellfun(@(v) isnan(v),Strike);
            Strike(Index_nan) = {0};
            ReportData(:,3) = Strike;
            
            %%% Connect to database
            ObjDB = connect_to_database;
            settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
            settle_date = repmat(settlement_date,size(ReportData,1),1);
            %%%% Upload the Final Data
            if strcmpi(InBUName,'coc')
            try
                Table_Name = strcat('daily_greeks_report_store_table_',InBUName);
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');

                upload_in_database(Table_Name,[settle_date,ReportData],ObjDB);
                disp('Upload Done Successfully to Database Table');           
            catch
                OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
            end
            end
            %%% end code
            
% assuming we need live OFUT and OTC trades for report-3
PosGroupType = strcmpi('group_type',ColNames);
IdxFut = strcmpi('FUT',Data(:,PosGroupType));
FutureData = Data(IdxFut,:);
Data(IdxFut,:) = [];

% construct the report-3 "Lots"
% UniqueFields = {'subportfolio','instrument','ProductName','strike','CallPut','contract_month'};
UniqueFields = {'subportfolio','ProductCode','contract_month','CallPut','strike'};
SumFields = {'active_lots'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,ReportData] = consolidatedata(ColNames, Data,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);

% ReportHeader = {'CB_Unit','Displayed Label','Product','Strike','Call/Put','Maturity Label','Nominal'};
ReportHeader = {'CB_Unit','Product','Maturity Label','Call/Put','strike','Nominal'};

PosNominal = strcmpi('Nominal',ReportHeader);
TempZeroVar = zeros(size(ReportData(:,PosNominal),1),1);
IdxZeroActiveLots = isnumericequal(cell2mat(ReportData(:,PosNominal)),TempZeroVar);
ReportData(IdxZeroActiveLots,:) = [];
xlswrite(OutFilename,[ReportHeader;ReportData],'Options'); %% 'Lots'

%% Future Data
UniqueFields = {'subportfolio','ProductCode','contract_month'};
SumFields = {'active_lots'};
OutputFields = [UniqueFields,SumFields,{'Premium'}];
WeightedAverageFields = {'Premium','active_lots'};
[OutputFields,ReportData] = consolidatedata(ColNames, FutureData,...
    UniqueFields,SumFields,OutputFields,WeightedAverageFields);

ReportHeader1 = {'CB_Unit','Product','Maturity Label','Nominal','Weighted Average'};

PosNominal = strcmpi('Nominal',ReportHeader1);
TempZeroVar = zeros(size(ReportData(:,PosNominal),1),1);
IdxZeroActiveLots = isnumericequal(cell2mat(ReportData(:,PosNominal)),TempZeroVar);
ReportData(IdxZeroActiveLots,:) = [];
xlswrite(OutFilename,[ReportHeader1;ReportData],'Future'); %% 'Lots'

%%
try
    OutXLSFileName = fullfile(pwd,OutFilename);
    xls_delete_sheets(OutXLSFileName);
catch
end

% configure_mail_settings;
% ObjDB = connect_to_database;
% SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
% sendmail({'Raghavendra.Sn@olamnet.com','ding.zhihui@olamnet.com','risk.reporting@olamnet.com'}, ...
% 'CFSG MO Reports for RiskOffice', ['Attached is the CFSG MO Reports for RiskOffice for COB ''',SettlementDate,''''],{OutFilename});

