function [OutErrorMsg,OutXLSName] = generate_broker_trade_daily_recon(InBUName,User_Passed_Counterparty,Sub_Counterparty,Deal_Status,Transaction_Date)

%%% Project Name: Broker Module
%%% Module Name: Broker Trade Recon
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 21 Dec 2018 / 16:00:00

%%% [OutErrorMsg,OutXLSName] = generate_broker_trade_daily_recon('agf','WELLS FARGO','WELLS FARGO ZETA','live','2018-12-17')
%%% [OutErrorMsg,OutXLSName] = generate_broker_trade_daily_recon('qf1','NEWEDGE','NEWEDGE','live','2018-12-17')

try
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename('broker_trade_daily_recon');
    
    %%% check the all input data is given or not
    % check Business unit
    if isempty(InBUName)
        OutErrorMsg = {'InBUName was not given by user!'};
        return;
    else
        if iscellstr(InBUName)
            InBUName = char(InBUName);
        end
    end
    % check Counterparty
    if isempty(User_Passed_Counterparty)  
        OutErrorMsg = {'Counterparty was not given by user!'};
        return;
    end
    % check Sub_Counterparty
    if isempty(Sub_Counterparty)  
        OutErrorMsg = {'Sub_Counterparty was not given by user!'};
        return;
    end
    % check Deal_Status
    if isempty(Deal_Status)  
        OutErrorMsg = {'Deal_Status was not given by user!'};
        return;
    else
    end
    
    % check Transaction_Date
    if isempty(Transaction_Date)  
        OutErrorMsg = {'Transaction_Date was not given by user!'};
        return;
    else
        %Transaction_Date = datestr(Transaction_Date,'yyyy-mm-dd');
    end

    % Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch data from Broker Deal Mapping Table
    Deal_Mapping_Table = 'broker_deal_mapping_table';
    SqlQuery_Deal_Mapping_Table = ['select * from ',char(Deal_Mapping_Table),' where businessunit = ''',char(InBUName),''' and counterparty_parent = ''',char(User_Passed_Counterparty),''' '] ;
    [~,Deal_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Deal_Mapping_Table);

    if strcmpi(Deal_Mapping_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Deal_Mapping_Table)]} ;
        return;
    end
    
    if isequal(Deal_Mapping_Table_Data,0) 
        OutErrorMsg = {['Data not Available in ',char(Deal_Mapping_Table)]} ;
        return;
    end

    %%% select data for Table_Name and broker_market_action
    BDeal_Table_Name = Deal_Mapping_Table_Data(:,3);
    BDeal_Table_market_action = Deal_Mapping_Table_Data(:,7);

    %%% Fetch data from Broker Deal Table
    if strcmpi(User_Passed_Counterparty , 'WELLS FARGO')   %%% recid is not equal to 'P'
        SqlQuery_Broker_Deal_Table = ['select * from ',char(BDeal_Table_Name),' where desk = ''',char(InBUName),''' and counterparty = ''',char(Sub_Counterparty),''' and settle_date = ''',char(Transaction_Date),''' and recid <>''P'' '] ;
    
    elseif strcmpi(User_Passed_Counterparty , 'NEWEDGE')   %%% psvolume is equal to "Zero"
        SqlQuery_Broker_Deal_Table = ['select * from ',char(BDeal_Table_Name),' where desk = ''',char(InBUName),''' and counterparty = ''',char(Sub_Counterparty),''' and settle_date = ''',char(Transaction_Date),''' and psvolume = 0'] ;
    end
    
    [Broker_Deal_Table_Cols,Broker_Deal_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Deal_Table);

    if strcmpi(Broker_Deal_Table_Data,'No Data') 
            OutErrorMsg = {['For ',char(Transaction_Date),' Data not Available in ',char(BDeal_Table_Name)]} ;
            return;
    end

    %%% Fetch data from Broker Deal Master Mapping Table
    BDeal_Master_Mapping_Table = ['broker_trade_deal_master_table_',char(InBUName)];
    SqlQuery_BDeal_Master_Table = ['select * from ',char(BDeal_Master_Mapping_Table),' where counterparty_parent = ''',char(Sub_Counterparty),''' '] ;
    %SqlQuery_BDeal_Master_Table = ['select * from ',char(BDeal_Master_Mapping_Table),' where counterparty_parent = ''',char(Sub_Counterparty),''' and business_unit = ''',char(InBUName),''' '] ;
    [BDeal_Master_Table_Cols,BDeal_Master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BDeal_Master_Table);

    if strcmpi(BDeal_Master_Table_Data,'No Data') 
        OutErrorMsg = {['Sub_Counterparty not found in ',char(BDeal_Master_Mapping_Table)]} ;
        return;
    end
    
    %%% Connect to Prod/UAT DB Channel
    %%% Get OPERA data
    TableName=['deal_ticket_table_deal_status_view_',char(InBUName)];
    SqlQuery_Opera_Table = ['select * from ',char(TableName),' where transaction_date = ''',char(Transaction_Date),''' and deal_status = ''',char(Deal_Status),''' and counterparty_parent = ''',char(Sub_Counterparty),''' '] ;
    [Opera_Table_Cols, Opera_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Opera_Table);

    if(isempty(Opera_Table_Data))
        OutErrorMsg = {['Data is not available in ',char(TableName)]};
        return;
    end

    if(strcmpi(Opera_Table_Data,'No Data'))
        OutErrorMsg = {['Data is not available in ',char(TableName)]};
        return;
    end
    
    Opera_Deal_Data =[Opera_Table_Cols; Opera_Table_Data];
    
    %%% OPERA deal requied cols
    portfolio = find(cellfun(@(V) strcmpi('portfolio',V), Opera_Table_Cols));
    transaction_date = find(cellfun(@(V) strcmpi('transaction_date',V), Opera_Table_Cols));
    security_id = find(cellfun(@(V) strcmpi('security_id',V), Opera_Table_Cols));
    original_lots = find(cellfun(@(V) strcmpi('original_lots',V), Opera_Table_Cols));
    original_premium = find(cellfun(@(V) strcmpi('original_premium',V), Opera_Table_Cols));
    counterparty_parent = find(cellfun(@(V) strcmpi('counterparty_parent',V), Opera_Table_Cols));
    instrument_opera = find(cellfun(@(V) strcmpi('instrument',V), Opera_Table_Cols));
    maturity_date = find(cellfun(@(V) strcmpi('maturity_date',V), Opera_Table_Cols));
    derivative_type = find(cellfun(@(V) strcmpi('derivative_type',V), Opera_Table_Cols));
    contract_month = find(cellfun(@(V) strcmpi('contract_month',V), Opera_Table_Cols));
    strike = find(cellfun(@(V) strcmpi('strike',V), Opera_Table_Cols));

    if isempty(portfolio)
        OutErrorMsg = {['portfolio column not found in ',char(TableName)]};
        return;
    end  
    if isempty(transaction_date)
        OutErrorMsg = {['transaction_date column not found in ',char(TableName)]};
        return;
    end 
    if isempty(security_id)
        OutErrorMsg = {['security_id column not found in ',char(TableName)]};
        return;
    end 
    if isempty(original_lots)
        OutErrorMsg = {['original_lots column not found in ',char(TableName)]};
        return;
    end 
    if isempty(original_premium)
        OutErrorMsg = {['original_premium column not found in ',char(TableName)]};
        return;
    end 
    if isempty(counterparty_parent)
        OutErrorMsg = {['counterparty_parent column not found in ',char(TableName)]};
        return;
    end 
    if isempty(instrument_opera)
        OutErrorMsg = {['instrument column not found in ',char(TableName)]};
        return;
    end 
    if isempty(maturity_date)
        OutErrorMsg = {['maturity_date column not found in ',char(TableName)]};
        return;
    end 
    if isempty(derivative_type)
        OutErrorMsg = {['derivative_type column not found in ',char(TableName)]};
        return;
    end 
    if isempty(contract_month)
        OutErrorMsg = {['contract_month column not found in ',char(TableName)]};
        return;
    end 
    if isempty(strike)
        OutErrorMsg = {['strike column not found in ',char(TableName)]};
        return;
    end 

    %%% user want data
    Opera_Deal_user_Data = [Opera_Deal_Data(:,portfolio), Opera_Deal_Data(:,transaction_date), Opera_Deal_Data(:,security_id), ...
                            Opera_Deal_Data(:,original_lots), Opera_Deal_Data(:,original_premium), Opera_Deal_Data(:,counterparty_parent), ...
                            Opera_Deal_Data(:,instrument_opera), Opera_Deal_Data(:,maturity_date), Opera_Deal_Data(:,derivative_type), ...
                            Opera_Deal_Data(:,contract_month), Opera_Deal_Data(:,strike)];

    %%% consolidation part for OPERA_Deal Data
    Consolidate_Data_Header =  Opera_Deal_user_Data(1,:);
    Consolidate_Data = Opera_Deal_user_Data(2:end,:);
    
    UniqueFields = {'portfolio','original_premium','instrument','derivative_type','contract_month','strike'}; 
    SumFields = {'original_lots'};
    OutputFields = Consolidate_Data_Header ;
    WeightedAverageFields = [];
    [Consolidate_Data_Header,Shares_Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    %%% After consolidation --> Header and Data
    Opera_Deal_Conso_Data = [Consolidate_Data_Header;Shares_Data_Consolidate];
    Opera_Deal_Table_Conso_Data = cell2dataset(Opera_Deal_Conso_Data);

    %%% Convert 'NaN' to 'Zero'
    Ind_NaN = isnan(Opera_Deal_Table_Conso_Data.strike);
    Opera_Deal_Table_Conso_Data.strike(Ind_NaN) = 0;
    
    %%%% Trade recon calculations
    if strcmpi(User_Passed_Counterparty , 'WELLS FARGO')
        
        Broker_Deal_Data = [Broker_Deal_Table_Cols ; Broker_Deal_Table_Data];

        %%% column name mapping
        Col_Name_Mapping_Table = 'broker_column_name_mapping_table';
        SqlQuery_Col_Name_Mapping_Table = ['select * from ',char(Col_Name_Mapping_Table),' where counterparty_parent = ''',char(User_Passed_Counterparty),''' '] ;
        [~,Col_Name_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Col_Name_Mapping_Table);
    
        if strcmpi(Col_Name_Mapping_Table_Data,'No Data') 
            OutErrorMsg = {['Data not Available in ',char(Col_Name_Mapping_Table)]} ;
            return;
        end
        
        %%% Column mapping and get excat broker column name
        account_number_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'account_number'}),3);
        bbg_root_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'bbg_root'}),3);
        quantity_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'broker_lots'}),3);
        contract_year_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'contract_month'}),3);
        counterparty_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'counterparty_parent'}),3);
        put_call_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'group_type'}),3);
        bbg_sngl_name_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'product'}),3);
        strike_price_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'strike'}),3);
        settle_date_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'trade_date'}),3);
        trade_price_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'trade_price'}),3);

        %%% broker deal requied cols
        settle_date = find(cellfun(@(V) strcmpi(settle_date_col,V), Broker_Deal_Table_Cols)) ;
        counterparty = find(cellfun(@(V) strcmpi(counterparty_col,V), Broker_Deal_Table_Cols)) ;
        buy_sell_code = find(cellfun(@(V) strcmpi(BDeal_Table_market_action,V), Broker_Deal_Table_Cols)) ;
        account_number = find(cellfun(@(V) strcmpi(account_number_col,V), Broker_Deal_Table_Cols)) ;
        quantity = find(cellfun(@(V) strcmpi(quantity_col,V), Broker_Deal_Table_Cols)) ;
        contract_year = find(cellfun(@(V) strcmpi(contract_year_col,V), Broker_Deal_Table_Cols)) ;
        put_call = find(cellfun(@(V) strcmpi(put_call_col,V), Broker_Deal_Table_Cols)) ;
        bbg_root = find(cellfun(@(V) strcmpi(bbg_root_col,V), Broker_Deal_Table_Cols)) ;
        bbg_sngl_name = find(cellfun(@(V) strcmpi(bbg_sngl_name_col,V), Broker_Deal_Table_Cols)) ;
        strike_price = find(cellfun(@(V) strcmpi(strike_price_col,V), Broker_Deal_Table_Cols)) ;
        trade_price = find(cellfun(@(V) strcmpi(trade_price_col,V), Broker_Deal_Table_Cols)) ;
        
        if isempty(settle_date)
               OutErrorMsg = {['settle_date column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(counterparty)
               OutErrorMsg = {['counterparty column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(account_number)
               OutErrorMsg = {['account_number column not found in ',char(BDeal_Table_Name)]};
               return;
        end  
        if isempty(bbg_root)
               OutErrorMsg = {['bbg_root column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(quantity)
               OutErrorMsg = {['quantity column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(contract_year)
               OutErrorMsg = {['contract_year column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(put_call)
               OutErrorMsg = {['put_call column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(bbg_sngl_name)
               OutErrorMsg = {['bbg_sngl_name column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(strike_price)
               OutErrorMsg = {['strike_price column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(trade_price)
               OutErrorMsg = {['trade_price column not found in ',char(BDeal_Table_Name)]};
               return;
        end

        %%% user want data
        Broker_Recon_user_Data = [Broker_Deal_Data(:,settle_date), Broker_Deal_Data(:,counterparty), Broker_Deal_Data(:,buy_sell_code), ...
                                         Broker_Deal_Data(:,account_number), Broker_Deal_Data(:,quantity), Broker_Deal_Data(:,contract_year), ...
                                         Broker_Deal_Data(:,put_call), Broker_Deal_Data(:,bbg_root), Broker_Deal_Data(:,bbg_sngl_name), ...
                                         Broker_Deal_Data(:,strike_price), Broker_Deal_Data(:,trade_price)];

        Broker_Recon_user_Data = cell2dataset(Broker_Recon_user_Data);

        %%%% Market_action and quantity
        Market_action = Broker_Recon_user_Data.buy_sell_code;
        quantity_val = Broker_Recon_user_Data.quantity;

        Bounght_Index = ismember(Market_action,1);
        Sold_Index = ismember(Market_action,2);
        quantity_val(Bounght_Index)  = quantity_val(Bounght_Index) .* 1 ;
        quantity_val(Sold_Index)  = quantity_val(Sold_Index) .* -1;

        Broker_Recon_user_Data.quantity = quantity_val;
    
        %%% get put_call
        Put_Call_Data = Broker_Recon_user_Data.put_call;
        Put_Index = cellStrfind_exact(Put_Call_Data,{'P'}) ;
        Call_Index = cellStrfind_exact(Put_Call_Data,{'C'}) ;
        Fut_Index = cellStrfind_exact(Put_Call_Data,{''}) ;
        Put_Call_Data(Put_Index)  = {'vanilla_put'} ;
        Put_Call_Data(Call_Index)  = {'vanilla_call'} ;
        Put_Call_Data(Fut_Index)  = {'future'} ;

        Broker_Recon_user_Data.put_call = Put_Call_Data;

        %%% contract_year
        [row,~] = size(Broker_Recon_user_Data);
        for j = 1:1:row
            Month_Data = Broker_Recon_user_Data.contract_year(j);
            Month_Format = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
            MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
            Current_Month_Date = cellstr(datestr(datenum(num2str(Month_Data),'yyyymm'),'mmm-yy'));
            Current_Year_Only = cellfun(@(x) x(end), Current_Month_Date, 'UniformOutput', false);
            Contr_YearMonthCode= cellstr([MonthCode{strncmpi(Current_Month_Date,Month_Format,3)},char(Current_Year_Only)]);
            Contr_YearMonth(j,1) = strcat(Contr_YearMonthCode,'.',Contr_YearMonthCode);
        end
        Broker_Recon_user_Data.contract_year = Contr_YearMonth;

        %%%% Broker and Master table mapping
        %%% Comparing account_number and product_code fetch data
        Broker_Recon_Master_Data = []; 
        for i = 1:1:row
            single_account_number = Broker_Recon_user_Data.account_number(i);
            single_product = Broker_Recon_user_Data.bbg_root(i);
            single_bbg_sngl_name = Broker_Recon_user_Data.bbg_sngl_name(i);

            Index_account = find(ismember(str2double(BDeal_Master_Table_Data(:,1)),single_account_number));
            Index_bbg_sngl_name = regexpi(single_bbg_sngl_name,BDeal_Master_Table_Data(:,7));
            Index_product = find(strcmpi(single_product,BDeal_Master_Table_Data(:,6)) & not(cellfun('isempty',Index_bbg_sngl_name)));
            Index = intersect( Index_account, Index_product,'rows');

            Unique_Broker_Master_Data=BDeal_Master_Table_Data(Index,:);
            if isempty(Unique_Broker_Master_Data) 
                %OutErrorMsg = {'account_number was not matching with given data'} ;
                continue;
            end
            User_BDeal_Data = Broker_Recon_user_Data(i,:);
            Broker_Master_Data = [Unique_Broker_Master_Data(3), Unique_Broker_Master_Data(5), Unique_Broker_Master_Data(8), Unique_Broker_Master_Data(9)];
            Broker_Master_Header = [BDeal_Master_Table_Cols(3),BDeal_Master_Table_Cols(5), BDeal_Master_Table_Cols(8), BDeal_Master_Table_Cols(9)];
            Broker_Master_Data = [Broker_Master_Header ; Broker_Master_Data];
            Final_user_data = [User_BDeal_Data,cell2dataset(Broker_Master_Data)];
            Broker_Recon_Master_Data = vertcat(Broker_Recon_Master_Data,Final_user_data);
        end

        if isempty(Broker_Recon_Master_Data)
            OutErrorMsg = {'No broker deal data was not mapping with master data!'} ;
            return;
        end

        %%% Option instrument (based on put and call)
        Option_Future = Broker_Recon_Master_Data.put_call;
        instrument = Broker_Recon_Master_Data.instrument_future;
        put_Index = cellStrfind_exact(Option_Future,{'vanilla_put'}) ;
        call_Index = cellStrfind_exact(Option_Future,{'vanilla_call'}) ;
        instrument(put_Index) = strcat(instrument(put_Index),'-O');
        instrument(call_Index) = strcat(instrument(call_Index),'-O');

        Broker_Recon_Master_Data.instrument_future = instrument;

        %%% consolidation part for Broker_Deal Data
        Broker_Recon_Master_Data_Cell = dataset2cell(Broker_Recon_Master_Data);
        Consolidate_Data_Header =  Broker_Recon_Master_Data_Cell(1,:);
        Consolidate_Data = Broker_Recon_Master_Data_Cell(2:end,:);

        UniqueFields = {'portfolio','instrument_future','account_number','contract_year','put_call','bbg_root','strike_price','trade_price'}; 
        SumFields = {'quantity'};
        OutputFields = Consolidate_Data_Header ;
        WeightedAverageFields = [];
        [Consolidate_Data_Header,Shares_Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

        %%% After consolidation --> Header and Data
        Broker_Recon_Master_conso = [Consolidate_Data_Header;Shares_Data_Consolidate];
        BRecon_Master_Conso_Data = cell2dataset(Broker_Recon_Master_conso);

        BRecon_Master_Conso_Data.strike_price_value = round(BRecon_Master_Conso_Data.strike_price .* BRecon_Master_Conso_Data.strike_conversion,4);
        BRecon_Master_Conso_Data.trade_price_value = round(BRecon_Master_Conso_Data.trade_price .* BRecon_Master_Conso_Data.prem_conversion,4);

        %%%% Broker and Opera table mapping
        %%% Comparing instrument and portfolio fetch data
        [rr,~] = size(BRecon_Master_Conso_Data);
        Not_Matched_Broker_Recon_Opera_Data = []; Matched_Broker_Recon_Opera_Data = [];
        for ii = 1:1:rr
            single_instrument = BRecon_Master_Conso_Data.instrument_future(ii);
            single_portfolio = BRecon_Master_Conso_Data.portfolio(ii);
            single_derivative_type = BRecon_Master_Conso_Data.put_call(ii); 
            single_contract_month = BRecon_Master_Conso_Data.contract_year(ii);
            single_strike = BRecon_Master_Conso_Data.strike_price_value(ii);
            single_trade_price = BRecon_Master_Conso_Data.trade_price_value(ii);
            single_quantity = BRecon_Master_Conso_Data.quantity(ii);

            Ind_Ins_Port = find(strcmpi(single_instrument,Opera_Deal_Table_Conso_Data.instrument) & strcmpi(single_portfolio,Opera_Deal_Table_Conso_Data.portfolio));
            Ind_derty_month = find(strcmpi(single_derivative_type,Opera_Deal_Table_Conso_Data.derivative_type) & strcmpi(single_contract_month,Opera_Deal_Table_Conso_Data.contract_month) & ismember(round(Opera_Deal_Table_Conso_Data.strike,4),single_strike));
            [Ind_trade,~] = find(ismember(round(Opera_Deal_Table_Conso_Data.original_premium,4),single_trade_price));
            Ind_intersect = intersect(intersect(Ind_Ins_Port,Ind_derty_month,'rows') , Ind_trade,'rows');
            
            Unique_BRecon_Master_Data=BRecon_Master_Conso_Data(ii,:);
           if ~isempty(find(Ind_intersect))  
                Unique_Opera_Data=Opera_Deal_Table_Conso_Data(Ind_intersect,:);
                Unique_OPERA_lots = Unique_Opera_Data.original_lots;
                if isequal(Unique_OPERA_lots,single_quantity)
                    %%% Matched Data
                    Comments = {''};
                    Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                        Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, Unique_BRecon_Master_Data.contract_year, Unique_BRecon_Master_Data.strike_price, ...  
                                        Unique_BRecon_Master_Data.trade_price,Unique_OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                    Matched_Broker_Recon_Opera_Data = vertcat(Matched_Broker_Recon_Opera_Data,Matched_Data);
                else
                    OPERA_lots = Unique_OPERA_lots;
                    Comments = {''};
                    %%%% Not Matched Data
                    Not_Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                        Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, Unique_BRecon_Master_Data.contract_year, Unique_BRecon_Master_Data.strike_price, ...  
                                        Unique_BRecon_Master_Data.trade_price,OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                     Not_Matched_Broker_Recon_Opera_Data = vertcat(Not_Matched_Broker_Recon_Opera_Data,Not_Matched_Data); 
                end
           else
                OPERA_lots = 0;
                Comments = {''};
                %%%% Not Matched Data
                Not_Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                        Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, Unique_BRecon_Master_Data.contract_year, Unique_BRecon_Master_Data.strike_price, ...  
                                        Unique_BRecon_Master_Data.trade_price,OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                 Not_Matched_Broker_Recon_Opera_Data = vertcat(Not_Matched_Broker_Recon_Opera_Data,Not_Matched_Data); 
        end
        end

    elseif strcmpi(User_Passed_Counterparty , 'NEWEDGE')
  
        %%% Delete psvolume != 0
%         psvolume = find(cellfun(@(V) strcmpi('psvolume',V), Broker_Deal_Table_Cols));
%         psvolume = Broker_Deal_Table_Data(:,psvolume);
%         [Index_zero] = find(cell2mat(psvolume) == 0);
%         Broker_Deal_Table_Data = Broker_Deal_Table_Data(Index_zero,:);

        Broker_Deal_Data = [Broker_Deal_Table_Cols ; Broker_Deal_Table_Data];

        %%% column name mapping
        Col_Name_Mapping_Table = 'broker_column_name_mapping_table';
        SqlQuery_Col_Name_Mapping_Table = ['select * from ',char(Col_Name_Mapping_Table),' where counterparty_parent = ''',char(User_Passed_Counterparty),''' '] ;
        [~,Col_Name_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Col_Name_Mapping_Table);
    
        if strcmpi(Col_Name_Mapping_Table_Data,'No Data') 
            OutErrorMsg = {['Data not Available in ',char(Col_Name_Mapping_Table)]} ;
            return;
        end
        
        %%% Column mapping and get excat broker column name
        account_number_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'account_number'}),3);
        bbg_root_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'bbg_root'}),3);
        quantity_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'broker_lots'}),3);
        counterparty_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'counterparty_parent'}),3);
        put_call_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'group_type'}),3);
        settle_date_col = Col_Name_Mapping_Table_Data(cellStrfind_exact(Col_Name_Mapping_Table_Data(:,2),{'trade_date'}),3);

        %%% broker deal requied cols
        settle_date = find(cellfun(@(V) strcmpi(settle_date_col,V), Broker_Deal_Table_Cols)) ;
        counterparty = find(cellfun(@(V) strcmpi(counterparty_col,V), Broker_Deal_Table_Cols)) ;
        account_number = find(cellfun(@(V) strcmpi(account_number_col,V), Broker_Deal_Table_Cols)) ;
        quantity = find(cellfun(@(V) strcmpi(quantity_col,V), Broker_Deal_Table_Cols)) ;
        put_call = find(cellfun(@(V) strcmpi(put_call_col,V), Broker_Deal_Table_Cols)) ;
        bbg_root = find(cellfun(@(V) strcmpi(bbg_root_col,V), Broker_Deal_Table_Cols)) ;

        if isempty(settle_date)
               OutErrorMsg = {['settle_date column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(counterparty)
               OutErrorMsg = {['counterparty column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(account_number)
               OutErrorMsg = {['account_number column not found in ',char(BDeal_Table_Name)]};
               return;
        end  
        if isempty(quantity)
               OutErrorMsg = {['quantity column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(put_call)
               OutErrorMsg = {['put_call column not found in ',char(BDeal_Table_Name)]};
               return;
        end
        if isempty(bbg_root)
               OutErrorMsg = {['bbg_root column not found in ',char(BDeal_Table_Name)]};
               return;
        end

        %%% user want data
        Broker_Recon_user_Data = [Broker_Deal_Data(:,settle_date), Broker_Deal_Data(:,counterparty), ...
                                         Broker_Deal_Data(:,account_number), Broker_Deal_Data(:,quantity), ...
                                         Broker_Deal_Data(:,put_call), Broker_Deal_Data(:,bbg_root)];

        Broker_Recon_user_Header =  {'settle_date','counterparty','account_number','quantity','put_call','bbg_root'};
        Broker_Recon_user_Data = cell2dataset([Broker_Recon_user_Header;Broker_Recon_user_Data(2:end,:)]);

        %%% get put_call
        Put_Call_Data = Broker_Recon_user_Data.put_call;
        Put_Index = cellStrfind_exact(Put_Call_Data,{'P'}) ;
        Call_Index = cellStrfind_exact(Put_Call_Data,{'C'}) ;
        Fut_Index = cellStrfind_exact(Put_Call_Data,{'FUT'}) ;
        Put_Call_Data(Put_Index)  = {'vanilla_put'} ;
        Put_Call_Data(Call_Index)  = {'vanilla_call'} ;
        Put_Call_Data(Fut_Index)  = {'future'} ;

        Broker_Recon_user_Data.put_call = Put_Call_Data;

        %%%% Broker and Master table mapping
        %%% Comparing account_number and product_code fetch data
        [row,~] = size(Broker_Recon_user_Data);
        Broker_Recon_Master_Data = []; 
        for i = 1:1:row
            single_account_number = Broker_Recon_user_Data.account_number(i);
            single_product = Broker_Recon_user_Data.bbg_root(i);

            Index_account = ismember(BDeal_Master_Table_Data(:,1),single_account_number);
            Index_product = strcmpi(single_product,BDeal_Master_Table_Data(:,6));
            Index = find(Index_account & Index_product);

            Unique_Broker_Master_Data=BDeal_Master_Table_Data(Index,:);
            if isempty(Unique_Broker_Master_Data) 
                %OutErrorMsg = {'account_number was not matching with given data'} ;
                continue;
            end
            User_BDeal_Data = Broker_Recon_user_Data(i,:);
            Broker_Master_Data = [Unique_Broker_Master_Data(3), Unique_Broker_Master_Data(5), Unique_Broker_Master_Data(8), Unique_Broker_Master_Data(9)];
            Broker_Master_Header = [BDeal_Master_Table_Cols(3),BDeal_Master_Table_Cols(5), BDeal_Master_Table_Cols(8), BDeal_Master_Table_Cols(9)];
            Broker_Master_Data = [Broker_Master_Header ; Broker_Master_Data];
            Final_user_data = [User_BDeal_Data,cell2dataset(Broker_Master_Data)];
            Broker_Recon_Master_Data = vertcat(Broker_Recon_Master_Data,Final_user_data);
        end
    
        if isempty(Broker_Recon_Master_Data)
            OutErrorMsg = {'No broker deal data was not mapping with master data!'} ;
            return;
        end

        %%% Option instrument (based on put and call)
        Option_Future = Broker_Recon_Master_Data.put_call;
        instrument = Broker_Recon_Master_Data.instrument_future;
        put_Index = cellStrfind_exact(Option_Future,{'vanilla_put'}) ;
        call_Index = cellStrfind_exact(Option_Future,{'vanilla_call'}) ;
        instrument(put_Index) = strcat(instrument(put_Index),'-O');
        instrument(call_Index) = strcat(instrument(call_Index),'-O');

        Broker_Recon_Master_Data.instrument_future = instrument;

        %%% consolidation part for Broker_Deal Data
        Broker_Recon_Master_Data_Cell = dataset2cell(Broker_Recon_Master_Data);
        Consolidate_Data_Header =  Broker_Recon_Master_Data_Cell(1,:);
        Consolidate_Data = Broker_Recon_Master_Data_Cell(2:end,:);

        UniqueFields = {'portfolio','instrument_future','account_number','put_call','bbg_root'}; 
        SumFields = {'quantity'};
        OutputFields = Consolidate_Data_Header ;
        WeightedAverageFields = [];
        [Consolidate_Data_Header,Shares_Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

        %%% After consolidation --> Header and Data
        Broker_Recon_Master_conso = [Consolidate_Data_Header;Shares_Data_Consolidate];
        BRecon_Master_Conso_Data = cell2dataset(Broker_Recon_Master_conso);

        Opera_Deal_Table_Conso_Data.original_lots = abs(Opera_Deal_Table_Conso_Data.original_lots);
        %%%% Broker and Opera table mapping
        %%% Comparing instrument and portfolio fetch data
        [rr,~] = size(BRecon_Master_Conso_Data);
        Not_Matched_Broker_Recon_Opera_Data = []; Matched_Broker_Recon_Opera_Data = [];
        for ii = 1:1:rr
            single_instrument = BRecon_Master_Conso_Data.instrument_future(ii);
            single_portfolio = BRecon_Master_Conso_Data.portfolio(ii);
            single_derivative_type = BRecon_Master_Conso_Data.put_call(ii); 
            single_quantity = BRecon_Master_Conso_Data.quantity(ii);
        
            Ind_Ins_Port = find(strcmpi(single_instrument,Opera_Deal_Table_Conso_Data.instrument) & strcmpi(single_portfolio,Opera_Deal_Table_Conso_Data.portfolio));
            Ind_derty_month = find(strcmpi(single_derivative_type,Opera_Deal_Table_Conso_Data.derivative_type));
            Ind_intersect = intersect(Ind_Ins_Port,Ind_derty_month,'rows');

            Unique_BRecon_Master_Data=BRecon_Master_Conso_Data(ii,:);
           if ~isempty(find(Ind_intersect)) 

            Unique_Opera_Data=Opera_Deal_Table_Conso_Data(Ind_intersect,:);
            Unique_OPERA_lots = Unique_Opera_Data.original_lots;

                if isequal(Unique_OPERA_lots,single_quantity)
                    %%% Matched Data
                    Comments = {''};
                    contract_year = {''};
                    strike_price = {''};
                    trade_price = {''};
                    Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                    Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, contract_year, strike_price, ...  
                                    trade_price,Unique_OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                    Matched_Broker_Recon_Opera_Data = vertcat(Matched_Broker_Recon_Opera_Data,Matched_Data);
                else
                    OPERA_lots = Unique_OPERA_lots;
                    Comments = {''};
                    contract_year = {''};
                    strike_price = {''};
                    trade_price = {''};
                    %%%% Not Matched Data
                    Not_Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                        Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, contract_year, strike_price, ...  
                                        trade_price,OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                     Not_Matched_Broker_Recon_Opera_Data = vertcat(Not_Matched_Broker_Recon_Opera_Data,Not_Matched_Data); 
                end
           else
                OPERA_lots = 0;
                Comments = {''};
                contract_year = {''};
                strike_price = {''};
                trade_price = {''};
                %%%% Not Matched Data
                Not_Matched_Data = [Unique_BRecon_Master_Data.settle_date, Unique_BRecon_Master_Data.counterparty, Unique_BRecon_Master_Data.account_number, Unique_BRecon_Master_Data.portfolio, ...    
                                    Unique_BRecon_Master_Data.instrument_future, Unique_BRecon_Master_Data.put_call, contract_year, strike_price, ...  
                                    trade_price,OPERA_lots, Unique_BRecon_Master_Data.quantity, Comments];

                 Not_Matched_Broker_Recon_Opera_Data = vertcat(Not_Matched_Broker_Recon_Opera_Data,Not_Matched_Data); 
        end
        end
    else
        OutErrorMsg = {['Not implemented logic for ',char(User_Passed_Counterparty),' Counterparty']};
        return;
    end
    
    %%%% Required Header
    Broker_Recon_Opera_Header = {'trade_date','counterparty_parent','account_number','portfolio','instrument','type','month','strike','t_price','opera_lots','broker_lots','comments'};
   
    %%% write data into file
    if isempty(Not_Matched_Broker_Recon_Opera_Data)
        xlswrite(OutXLSName,{'No Data'},'Not Match');
    else
        %%%% delete OPERA_lots = 0 and BROKER_lots = 0
        Ind_zero_rows=find(cell2mat(Not_Matched_Broker_Recon_Opera_Data(:,10)) == 0 & cell2mat(Not_Matched_Broker_Recon_Opera_Data(:,11)) == 0);
        Not_Matched_Broker_Recon_Opera_Data(Ind_zero_rows,:) = [];
    
        xlswrite(OutXLSName,[Broker_Recon_Opera_Header;Not_Matched_Broker_Recon_Opera_Data],'Not Match');
    end

    %%% write data into file
    if isempty(Matched_Broker_Recon_Opera_Data)
         xlswrite(OutXLSName,{'No Data'},'Match');
    else
         xlswrite(OutXLSName,[Broker_Recon_Opera_Header;Matched_Broker_Recon_Opera_Data],'Match');
    end   

    xls_delete_sheets(fullfile(pwd,OutXLSName));
    xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Not Match');
  
catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end

end
