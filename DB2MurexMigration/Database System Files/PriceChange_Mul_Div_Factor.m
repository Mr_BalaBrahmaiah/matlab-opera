function [OutErrorMsg,CleanedPriceData] = PriceChange_Mul_Div_Factor(ObjDB,CleanedPriceData,Sys_SDate)

% global ObjDB ;

OutErrorMsg = {'No Errors'};
Sys_SDate = char(Sys_SDate);

%%
SqlQuery_ProductMaster = 'select bbg_product_code,curr_mult_type,curr_mult_factor from product_master_table where invenio_product_code like ''CM%'' and currency <> ''USD'' and bbg_product_code <> ''OR'' ' ;
[ProductMaster_Header,ProductMaster_Data] = Fetch_DB_Data(ObjDB,SqlQuery_ProductMaster);

BBGCode_Col = cellStrfind_exact(ProductMaster_Header,{'bbg_product_code'});
MulType_Col = cellStrfind_exact(ProductMaster_Header,{'curr_mult_type'});
MulFactor_Col = cellStrfind_exact(ProductMaster_Header,{'curr_mult_factor'});

%%

for i = 1 : size(ProductMaster_Data,1)
    Current_ProductCode = ProductMaster_Data(i,BBGCode_Col);
    
    Matched_Index = cellStrfind_exact(CleanedPriceData.product_code,Current_ProductCode) ;
    
    if(~isempty(Matched_Index))
        
        Current_MulType = ProductMaster_Data(i,MulType_Col);
        Current_MulFactor = ProductMaster_Data(i,MulFactor_Col);
        
        SqlQuery1 = ['select settle_value from underlying_settle_value_table where settlement_date = ''',Sys_SDate,''' and underlying_id = ''',char(Current_MulFactor),''''];
        try
            SettlePrice = fetch(ObjDB, SqlQuery1);
        catch
            OutErrorMsg = [char(Current_MulFactor),' Spot ID missing for corresponding Settle Date'];
            return;
        end
        
        if(isempty(SettlePrice))
            OutErrorMsg = [char(Current_MulFactor),' Spot ID missing for corresponding Settle Date'];
            return;
        end
        
        
        if(strcmpi(Current_MulType,'multiplication'))
            CleanedPriceData.price_change(Matched_Index) = CleanedPriceData.price_change(Matched_Index) .* cell2mat(SettlePrice);
        else
            CleanedPriceData.price_change(Matched_Index) = CleanedPriceData.price_change(Matched_Index) ./ cell2mat(SettlePrice);
        end
        
    end
    
end

