function mail_traders_report_monthwise

OutFilename = generate_detailed_traders_report;

HolDates = {'2015-04-03';'2015-05-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');
TradeDate = datestr(busdate(datenum(today),-1,HolidayVec),'yyyy-mm-dd');

configure_mail_settings;
sendmail('Raghavendra.Sn@olamnet.com', 'Monthwise Traders Report', ['The attached file contains the detailed monthwise traders report for the TradeDate ''',TradeDate,''''],{OutFilename});
sendmail('zhang.ying@olamnet.com', 'Monthwise Traders Report', ['The attached file contains the detailed monthwise traders report for the TradeDate ''',TradeDate,''''],{OutFilename});