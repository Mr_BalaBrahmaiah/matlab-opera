function update_curr_mult_table

SqlQuery = 'select invenio_product_code,value_date,curr_mult from curr_mult_view';
[~,CurrMultData] = read_from_database('curr_mult_view',0,SqlQuery);

[ColNames,Data] = read_from_database('var_product_code_mapping_table',0);
ProductCodeMapping = cell2dataset([ColNames;Data]);

IdxCode = ismember(CurrMultData(:,1),ProductCodeMapping.invenio_product_code);
CurrMultTableData = CurrMultData(IdxCode,:);

TableName = 'curr_mult_table';
ObjDB = connect_to_database;
set(ObjDB,'AutoCommit','off');
DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end
set(ObjDB,'AutoCommit','on');

upload_in_database('curr_mult_table', CurrMultTableData);
