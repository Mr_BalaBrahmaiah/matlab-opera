function [OutErrorMsg,Output] = derivativepricer_interpvols(InBUName,Inputmatrix, Inputheading, Outputheading,PricingType)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 10:00:15 $
%  $Revision: 1.11 $
%
%% Checking Purpose

% ObjDB = connect_to_database ;
% SqlQuery = 'select * from input_security_settlement_value_view_agf where security_id = ''CM BO N8.N8C 35''';
% Inputmatrix = fetch(ObjDB,SqlQuery);

% fprintf('InBUName : %s\n',char(InBUName));
% [R,C] = size(Inputmatrix);
% fprintf('Inputmatrix Row : %d : Inputmatrix Col :%d\n',R,C);
% disp(Inputmatrix);
% fprintf('Input Heading\n')
% disp(Inputheading);
% fprintf('Outputheading\n')
% disp(Outputheading);
% fprintf('PricingType : %s\n',char(PricingType));

%%
OutErrorMsg = {'No errors'};
Output = [];

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    NumRows = size(Inputmatrix,1);
    try
        if strcmpi(PricingType,'settle')
            ViewName = 'input_security_settlement_value_view';
        else
            ViewName = 'input_security_traders_value_view';
        end
        SqlQuery = ['select count(*) from ',ViewName];
        [~,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
        DBRows = cell2mat(Data);
        if isequal(DBRows,NumRows)
            IsUpdateVols = 1;
        else
            IsUpdateVols = 0;
        end
        disp(['DBRows:',num2str(DBRows),' NumRows:',num2str(NumRows),' IsUpdateVols:',num2str(IsUpdateVols)]);
    catch
        IsUpdateVols = 0;
        disp('Error while trying to retrive the pricing info!');
    end
    
    % Assign the default values for pricing
    Lots      = num2cell(ones(NumRows,1));
    Lot_Mult  = num2cell(ones(NumRows,1));
    Curr_Mult = num2cell(ones(NumRows,1));
    % Opt_DayCount = repmat(cellstr('trading'),NumRows,1);
    Opt_DayCount = repmat(cellstr('calendar'),NumRows,1);
    
    % % TODO - modified on 01 July 2014 for new TTM convention
    % Pos_ValueDate    = find(strcmpi('value_date',Inputheading));
    % ValueDate    = Inputmatrix(:,Pos_ValueDate);
    % Pos_MaturityDate = find(strcmpi('maturity',Inputheading));
    % MaturityDate = Inputmatrix(:,Pos_MaturityDate); %#ok<*FNDSB>
    % MaturityDate(cellfun(@isempty,MaturityDate)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
    % Time_Bump = num2cell(ones(NumRows,1));
    % Idx = (datenum(cell2mat(MaturityDate)) - datenum(cell2mat(ValueDate))) <= 0;
    % Time_Bump(Idx) =num2cell(1-0.01);
    Time_Bump = num2cell(zeros(NumRows,1));
    % % End of TODO - modified on 01 July 2014 for new TTM convention
    
    
    if strcmpi(PricingType,'settle')
        Vol_Surf_Type = repmat(cellstr('fixed'),NumRows,1);
        Inputheading = ['Lots','Time_Bump','Lot_Mult','Curr_Mult','Opt_DayCount','Vol_Surf_Type',Inputheading];
        Inputmatrix  = [Lots,Time_Bump,Lot_Mult,Curr_Mult,Opt_DayCount,Vol_Surf_Type,Inputmatrix ];
    else
        Inputheading = ['Lots','Time_Bump','Lot_Mult','Curr_Mult','Opt_DayCount',Inputheading];
        Inputmatrix  = [Lots,Time_Bump,Lot_Mult,Curr_Mult,Opt_DayCount,Inputmatrix ];
    end
    
    % computation of vol values
    % For Vol computation
    Pos_Strike1         = find(strcmpi('Strike1',Inputheading));
    Pos_Strike2         = find(strcmpi('Strike2',Inputheading));
    Pos_Vol1_id         = find(strcmpi('Vol1_id',Inputheading));
    Pos_Vol2_id         = find(strcmpi('Vol2_id',Inputheading));
    Pos_P1_Vol          = find(strcmpi('P1_Vol',Inputheading));
    Pos_P2_Vol          = find(strcmpi('P2_Vol',Inputheading));
    Pos_Subportfolio    = find(strcmpi('Subportfolio',Inputheading));
    Pos_Vol_Surf_Type   = find(strcmpi('Vol_Surf_Type',Inputheading));
    
    if ~isempty(Pos_Vol1_id)
        Vol1_id                = Inputmatrix(:, Pos_Vol1_id);
        if ~isempty(Pos_Strike1)
            Strike1            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike1));
        end
        if ~isempty(Pos_P1_Vol)
            P1_Vol            = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_Vol));
        end
    end
    if ~isempty(Pos_Vol2_id)
        Vol2_id                = Inputmatrix(:, Pos_Vol2_id);
        Vol2_id(cellfun(@isempty,Vol2_id)) = {''};
        if ~isempty(Pos_Strike2)
            Strike2            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike2));
        end
        if ~isempty(Pos_P2_Vol)
            P2_Vol            = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_Vol));
        end
    end
    
    % For computation of Vol values
    Pos_Opt_Type = find(strcmpi('Opt_Type',Inputheading));
    OptTypes = Inputmatrix(:, Pos_Opt_Type); %#ok<*FNDSB>
    Idx_OptTypes = cellfun(@isnumeric, OptTypes);
    OptTypes(Idx_OptTypes) = cellstr('');
    OptionTypes = lower(OptTypes);
    Subportfolio     = Inputmatrix(:, Pos_Subportfolio);
    Vol_Surf_Type     = Inputmatrix(:, Pos_Vol_Surf_Type);
    
    IdxVanilla = strcmpi(OptionTypes, 'vanilla_call') | strcmpi(OptionTypes, 'vanilla_put');
    IdxBarOpt  = strcmpi(OptionTypes, 'euro_up_in_call')  | strcmpi(OptionTypes, 'euro_up_in_put') | strcmpi(OptionTypes, 'euro_up_out_call')  | strcmpi(OptionTypes, 'euro_up_out_put') | ...
        strcmpi(OptionTypes, 'euro_down_in_call')  | strcmpi(OptionTypes, 'euro_down_in_put') | strcmpi(OptionTypes, 'euro_down_out_call')  | strcmpi(OptionTypes, 'euro_down_out_put') | ...
        strcmpi(OptionTypes, 'amer_up_in_call')  | strcmpi(OptionTypes, 'amer_up_in_put') | strcmpi(OptionTypes, 'amer_up_out_call')  | strcmpi(OptionTypes, 'amer_up_out_put') | ...
        strcmpi(OptionTypes, 'amer_down_in_call')  | strcmpi(OptionTypes, 'amer_down_in_put') | strcmpi(OptionTypes, 'amer_down_out_call')  | strcmpi(OptionTypes, 'amer_down_out_put');
    IdxSprOpt  = strcmpi(OptionTypes, 'spread_call')  | strcmpi(OptionTypes, 'spread_put');
    
    IdxInterpVol = (strcmpi(Vol_Surf_Type, 'fixed')) & (IdxVanilla | IdxBarOpt | IdxSprOpt);
    if any(IdxInterpVol)
        if strcmpi(PricingType,'settle')
            % interpolate P1_vol
            Call_Put_Index = repmat(cellstr('C'),size(OptionTypes(IdxInterpVol)));
            PutIndex = cellfun(@isempty,strfind(OptionTypes(IdxInterpVol),'call'));
            Call_Put_Index(PutIndex) = cellstr('P');
            InputData.call_put_id = Call_Put_Index;
            InputData.vol_id = Vol1_id(IdxInterpVol);
            InputData.strike = Strike1(IdxInterpVol);
            InputData.vol    = P1_Vol(IdxInterpVol);
            %         if ~all(isnan(P1_Vol)) % when vols are not uploaded then it is not needed to interpolate
            Interp1Vol = interpolate_settlement_vol_strike(InputData);
            Inputmatrix(IdxInterpVol,Pos_P1_Vol) = num2cell(Interp1Vol);
            %         end
            % interpolate P2_vol
            IdxInterpVol2 = IdxInterpVol & ~strcmp(Vol2_id,'');
            if any(IdxInterpVol2)
                Call_Put_Index = repmat(cellstr('C'),size(OptionTypes(IdxInterpVol2)));
                PutIndex = cellfun(@isempty,strfind(OptionTypes(IdxInterpVol2),'call'));
                Call_Put_Index(PutIndex) = cellstr('P');
                InputData.call_put_id = Call_Put_Index;
                InputData.vol_id = Vol2_id(IdxInterpVol2);
                InputData.strike = Strike2(IdxInterpVol2);
                InputData.vol    = P2_Vol(IdxInterpVol2);
                Interp2Vol = interpolate_settlement_vol_strike(InputData);
                Inputmatrix(IdxInterpVol2,Pos_P2_Vol) = num2cell(Interp2Vol);
            end
        else
            % interpolate P1_vol
            InputData.subportfolio = Subportfolio(IdxInterpVol);
            InputData.vol_id = Vol1_id(IdxInterpVol);
            InputData.strike = Strike1(IdxInterpVol);
            InputData.vol    = P1_Vol(IdxInterpVol);
            %         if ~all(isnan(P1_Vol)) % when vols are not uploaded then it is not needed to interpolate
            Interp1Vol = interpolate_traders_vol_strike(InputData,InBUName);
            Inputmatrix(IdxInterpVol,Pos_P1_Vol) = num2cell(Interp1Vol);
            %         end
            
            % interpolate P2_vol
            IdxInterpVol2 = IdxInterpVol & ~strcmp(Vol2_id,'');
            if any(IdxInterpVol2)
                InputData.subportfolio = Subportfolio(IdxInterpVol2);
                InputData.vol_id = Vol2_id(IdxInterpVol2);
                InputData.strike = Strike2(IdxInterpVol2);
                InputData.vol    = P2_Vol(IdxInterpVol2);
                Interp2Vol = interpolate_traders_vol_strike(InputData,InBUName);
                Inputmatrix(IdxInterpVol2,Pos_P2_Vol) = num2cell(Interp2Vol);
            end
        end
    end
    
    if IsUpdateVols
        Output = derivativepricer(InBUName,Inputmatrix, Inputheading, Outputheading,2);
    else
        Output = derivativepricer(InBUName,Inputmatrix, Inputheading, Outputheading);
    end
    
    Idx = strcmp(Output,' ');
    Output(Idx) = num2cell(0);
    NaNIdx  = (cellfun(@isnan,Output));
    Output(NaNIdx) = num2cell(0);
    
    Pos_SecurityID = find(strcmpi('Security_ID',Inputheading));
    Security_Id = Inputmatrix(:, Pos_SecurityID);
    Pos_Subportfolio    = find(strcmpi('Subportfolio',Inputheading));
    Subportfolio     = Inputmatrix(:, Pos_Subportfolio);
    Pos_ValueDate    = find(strcmpi('value_date',Inputheading));
    ValueDate    = Inputmatrix(:,Pos_ValueDate);
    
    Pos_OutSecurityId = find(strcmpi('Security_ID',Outputheading));
    Pos_OutSubportfolio = find(strcmpi('Subportfolio',Outputheading));
    Pos_OutValuedate = find(strcmpi('value_date',Outputheading));
    
    Output(:,Pos_OutSecurityId) = Security_Id;
    Output(:,Pos_OutValuedate) = ValueDate;
    
    %%% Newly added Code
    %%%% In Pricing, Based on ITM and OTM below is the logic to be written for maturity date<= settle_date
    IdxFxVanilla = strcmpi(OptionTypes, 'fx_vanilla_call') | strcmpi(OptionTypes, 'fx_vanilla_put'); 
    IdxCSVanilla = strcmpi(OptionTypes, 'cs_vanilla_call') | strcmpi(OptionTypes, 'cs_vanilla_put');
    IdxCSBarrier  = strcmpi(OptionTypes, 'cb_amer_up_in_call')  | strcmpi(OptionTypes, 'cb_amer_up_in_put') | strcmpi(OptionTypes, 'cb_amer_up_out_call')  |  ...
        strcmpi(OptionTypes, 'cb_amer_up_out_put') | strcmpi(OptionTypes, 'cb_amer_down_in_call')  | strcmpi(OptionTypes, 'cb_amer_down_in_put') | strcmpi(OptionTypes, 'cb_amer_down_out_call')  | strcmpi(OptionTypes, 'cb_amer_down_out_put') | ...
        strcmpi(OptionTypes, 'cb_euro_up_in_call')  | strcmpi(OptionTypes, 'cb_euro_up_in_put') | strcmpi(OptionTypes, 'cb_euro_up_out_call')  | strcmpi(OptionTypes, 'cb_euro_up_out_put') | ...
        strcmpi(OptionTypes, 'cb_euro_down_in_call')  | strcmpi(OptionTypes, 'cb_euro_down_in_put') | strcmpi(OptionTypes, 'cb_euro_down_out_call')  | strcmpi(OptionTypes, 'cb_euro_down_out_put');

    if any(IdxFxVanilla | IdxCSVanilla | IdxCSBarrier)
    Matched_FX_InData = Inputmatrix(IdxFxVanilla,:);  %%% input data from view
    Matched_FX_OutData = Output(IdxFxVanilla,:);      %%% output data from fx_option_pricer
    Indxloc = datetime(Matched_FX_InData(:,11)) == datetime(Matched_FX_InData(:,52)); % maturity = settlement_date
    Matched_InData = Matched_FX_InData(Indxloc,:);  %%% input data from view
    Matched_OutData = Matched_FX_OutData(Indxloc,:);      %%% output data from fx_option_pricer
    
    if any(Indxloc)
        Total_Price_Table_Data = [];
        ObjDB = connect_to_database ;
        for i = 1:1:size(Matched_InData,1)
            %%% get currency from security id
                [~,Str2] = strtok(Matched_InData(i,7),' '); %security_id
                [Str11,~] = strtok(Str2,' ');
                Currency = strrep(Str11,'USD','');   %%% (to get currency, to remove the "USD") 
                %%% fetch spot from currency_spot_mapping_table
                SqlQuery_DVD_FX_Rate = ['select spot from currency_spot_mapping_table where currency = ''',char(Currency),''' '] ;
                Currency_Spot = fetch(ObjDB,SqlQuery_DVD_FX_Rate);
                if isempty(Currency_Spot)
                     disp({[char(Currency),' currency is not found in currency_spot_mapping_table']});
                     continue;
                end
                %%% fetch settle_value from underlying_settle_value_table
                Settele_date = Matched_InData(i,52);%settlement_date
                SqlQuery_Underlying_settle_value = ['select settle_value from underlying_settle_value_table where settlement_date=''',char(Settele_date),''' and underlying_id = ''',char(Currency_Spot),''' '] ;
                Spot_Value = fetch(ObjDB,SqlQuery_Underlying_settle_value);
                if isempty(Spot_Value)
                    disp({[char(Currency),' Currency is not found in underlying_settle_value_table']});
                    continue;
                end
            if ~isempty(cell2mat(strfind(Matched_InData(i,12),'call'))) % CALL option %opt_type
                if (cell2mat(Spot_Value) >= cell2mat(Matched_InData(i,21))) % In The Money - Call %strike
                        Asset_class = {'currency'};
                        Security_id = Matched_OutData(i,1); %Security_id
                        Value_date = Matched_InData(i,11); %maturity
                        Last_price = Matched_OutData(i,3); %price
                    else % Out of the money - Call
                        Asset_class = {'currency'};
                        Security_id = Matched_OutData(i,1);%Security_id
                        Value_date = Matched_InData(i,11); %maturity
                        Last_price = {0};      
                end
            elseif ~isempty(cell2mat(strfind(Matched_InData(i,12),'put')))% PUT Option %opt_type
                if (cell2mat(Spot_Value) <= cell2mat(Matched_InData(i,21))) % In the Money - PUT option %strike
                        Asset_class = {'currency'};
                        Security_id = Matched_OutData(i,1); %Security_id
                        Value_date = Matched_InData(i,11); %maturity
                        Last_price = Matched_OutData(i,3); %price
                    else % Out of the money - PUT option
                        Asset_class = {'currency'};
                        Security_id = Matched_OutData(i,1);%Security_id
                        Value_date = Matched_InData(i,11); %maturity
                        Last_price = {0};     
                end
            end
            price_table_data = [Asset_class,Security_id,Value_date,Last_price];
            Total_Price_Table_Data = [Total_Price_Table_Data ; price_table_data];
        end

        %%%% Upload data into futures_price_table
        try
            Futures_Price_Table_Name = 'futures_price_table';
            set(ObjDB,'AutoCommit','off');
            security_id_sql_Query = ['''',strrep(convertCell2Char(Total_Price_Table_Data(:,2)),',',''','''),''''];
            SqlQuery = ['delete from ',char(Futures_Price_Table_Name),' where underlying_id = ',char(security_id_sql_Query),' '];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');

            upload_in_database(Futures_Price_Table_Name,Total_Price_Table_Data,ObjDB);
            disp('Upload Done Successfully to Database Table');           
        catch
            disp({['Data upload got failed in ',char(Futures_Price_Table_Name)]});
        end
    end
    end
    %%% End Code
    
    if strcmpi(InBUName,'qf5')
        Pos_Counterparty = find(strcmpi('counterparty',Inputheading));
        Counterparty = Inputmatrix(:, Pos_Counterparty);
        Pos_Instrument    = find(strcmpi('instrument',Inputheading));
        Instrument     = Inputmatrix(:, Pos_Instrument);
        Pos_CurrType    = find(strcmpi('curr_type',Inputheading));
        CurrType    = Inputmatrix(:,Pos_CurrType);
        
        Pos_OutInstrument  = find(strcmpi('instrument',Outputheading));
        Pos_OutCounterparty = find(strcmpi('counterparty_parent',Outputheading));
        Pos_OutCurrType  = find(strcmpi('curr_type',Outputheading));
        
        Output(:,Pos_OutInstrument)   = Instrument;
        Output(:,Pos_OutCounterparty) = Counterparty;
        Output(:,Pos_OutCurrType)     = CurrType;
    end
    
    if strcmpi(PricingType,'traders')
        Output(:,Pos_OutSubportfolio) = Subportfolio;
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end

end