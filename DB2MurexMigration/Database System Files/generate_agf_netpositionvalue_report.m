function [OutErrorMsg,OutNPVProductData,OutNPVGroupData]  = generate_agf_netpositionvalue_report(InBUName)

OutErrorMsg = {'No errors'};
OutXLSFileName = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind_exact(Get_GroupType,{'FUT','OFUT'});
        Data = Data(ispresent_FUT_OFUT,:);
        
        UniqueFields = {'subportfolio','product_code','product_name','group_type','contract_month'};
        SumFields={'settle_delta_1'};
        OutputFields = [UniqueFields,SumFields,'future_price','mult_factor','nc_factor','fx_nc_factor'];
        WeightedAverageFields = [];
        [OutputFields,Data] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;Data]);
        NetPosVal = TempOutData.settle_delta_1 .*  TempOutData.mult_factor .* TempOutData.future_price  .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        
        TempOutData.group_type = [];
        TempOutData.contract_month = [];
        TempOutData.settle_delta_1 = [];
        TempOutData.future_price =[];
        TempOutData.mult_factor = [];
        TempOutData.nc_factor = [];
        TempOutData.fx_nc_factor = [];
        
        TempOutData.NetPosVal = NetPosVal;
        
        ViewName = [char(InBUName),'_combined_report_mapping_table'];
        [ColNames,Data] = read_from_database(ViewName,0);
        DBRefPFGroupData = cell2dataset([ColNames;Data]);
        
        ViewName = [char(InBUName),'_product_group_table'];
        [ColNames,Data] = read_from_database(ViewName,0);
        DBRefPrdtGroupData = cell2dataset([ColNames;Data]);
        
        PortfolioGroup = cell(size(TempOutData.subportfolio));
        ProductGroup = cell(size(TempOutData.subportfolio));
        
        for iPF = 1:length(DBRefPFGroupData.subportfolio)
            IdxPF = strcmpi(DBRefPFGroupData.subportfolio{iPF},TempOutData.subportfolio);
            PortfolioGroup(IdxPF) = DBRefPFGroupData.group(iPF);
        end
        
        for iPF = 1:length(DBRefPrdtGroupData.product_code)
            IdxPF = strcmpi(DBRefPrdtGroupData.product_code{iPF},TempOutData.product_code);
            ProductGroup(IdxPF) = DBRefPrdtGroupData.product_group(iPF);
        end
        
        TempOutData.PortfolioGroup = PortfolioGroup;
        TempOutData.ProductGroup = ProductGroup;
        
        CTempOutData = dataset2cell(TempOutData);
        ColNames = CTempOutData(1,:);
        Data = CTempOutData(2:end,:);
        
        UniqueFields = {'PortfolioGroup','product_name'};
        SumFields={'NetPosVal'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        OutNPVProductData = [OutputFields;OutData];
        
        UniqueFields = {'PortfolioGroup','ProductGroup'};
        SumFields={'NetPosVal'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        OutNPVGroupData = [OutputFields;OutData];
        
        %% Make a Report
        Header_1 = OutNPVProductData(1,:);
        Data_1 = OutNPVProductData(2:end,:) ;
        
        PortfolioGroup_Col = find(strcmpi(Header_1,'PortfolioGroup'));
        %         Unique_PortfolioGroup = unique(Data_1(:,PortfolioGroup_Col))' ;  %% Getting From Table
        
        ObjDB = connect_to_database;
        RPGO_TableName = [char(InBUName),'_reports_portfoliogroup_order_table'];
        SqlQuery = ['select * from ',char(RPGO_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        Unique_PortfolioGroup = Table_Data(:,2)' ;
        
        ProductName_Col = find(strcmpi(Header_1,'product_name')) ;
        Unique_ProductName = unique(Data_1(:,ProductName_Col)) ;
        NetPos_Col = find(strcmpi(Header_1,'NetPosVal')) ;
        
        ObjDB = connect_to_database;
        RPO_TableName = [char(InBUName),'_reports_product_order_table'];
        SqlQuery = ['select * from ',char(RPO_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        ProductName_Order = Table_Data(:,2) ;
        
        Unique_ProductName = Unique_ProductName(cellStrfind_exact(Unique_ProductName ,ProductName_Order)) ; %% Get Product OrderWise
        Total_Data = cell(size(Unique_ProductName,1),size(Unique_PortfolioGroup,2)) ;
        
        for i = 1 : size(Unique_ProductName,1)
            Current_Product = Unique_ProductName(i) ;
            
            Matched_Index = cellStrfind_exact(Data_1(:,ProductName_Col),Current_Product) ;
            
            Temp_Matrix = Data_1(Matched_Index,:) ;
            
            for ii = 1 : size(Temp_Matrix,1)
                Current_Portfolio = Temp_Matrix(ii,PortfolioGroup_Col) ;
                
                Matched_Idx = cellStrfind_exact(Unique_PortfolioGroup,Current_Portfolio);
                
                if(~isempty(Matched_Idx))
                    Total_Data(i,Matched_Idx) = Temp_Matrix(ii,NetPos_Col) ;
                end
                
            end
            
        end
        
        [Row_Sum, ~] = cell2sum_Row_Col(Total_Data) ;
        Total_Data = [Total_Data,Row_Sum] ;
        [~, Col_Sum] = cell2sum_Row_Col(Total_Data)  ;
        Total_Data = [Total_Data ; Col_Sum] ;
        
        Header = [{'PRODUCT'},Unique_PortfolioGroup,{'TOTAL'}] ;
        Unique_ProductName = [Unique_ProductName ; {'TOTAL'}] ;
        
        Row_Data = [Unique_ProductName , Total_Data] ;
        OutNPVProductData  =  [ Header ;  Row_Data];
        
        %%
        
        Header_2 = OutNPVGroupData(1,:);
        Data_2 = OutNPVGroupData(2:end,:) ;
        
        PortfolioGroup_Col = find(strcmpi(Header_2,'PortfolioGroup'));
        %         Unique_PortfolioGroup = unique(Data_2(:,PortfolioGroup_Col))' ;  %% Getting From Table in OrderWise
        %
        ProductGroup_Col = find(strcmpi(Header_2,'ProductGroup'));
        %         Unique_ProductGroup = unique(Data_2(:,ProductGroup_Col));  %% Getting From Table in OrderWise
        
        ObjDB = connect_to_database;
        RPGOT_TableName = [char(InBUName),'_reports_productgroup_order_table'];
        SqlQuery = ['select * from ',char(RPGOT_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        Unique_ProductGroup = Table_Data(:,2) ;
        
        NetPos_Col = find(strcmpi(Header_2,'NetPosVal')) ;
        
        Total_Data = cell(size(Unique_ProductGroup,1),size(Unique_PortfolioGroup,2)) ;
        
        for i = 1 : size(Unique_ProductGroup,1)
            Current_Product = Unique_ProductGroup(i) ;
            
            Matched_Index = cellStrfind_exact(Data_2(:,ProductGroup_Col),Current_Product) ;
            
            Temp_Matrix = Data_2(Matched_Index,:)  ;
            
            for ii = 1 : size(Temp_Matrix,1)
                Current_Portfolio = Temp_Matrix(ii,PortfolioGroup_Col) ;
                
                Matched_Idx = cellStrfind_exact(Unique_PortfolioGroup,Current_Portfolio);
                
                if(~isempty(Matched_Idx))
                    Total_Data(i,Matched_Idx) = Temp_Matrix(ii,NetPos_Col) ;
                end
                
            end
            
        end
        
        [Row_Sum, ~] = cell2sum_Row_Col(Total_Data) ;
        Total_Data = [Total_Data,Row_Sum] ;
        [~, Col_Sum] = cell2sum_Row_Col(Total_Data)  ;
        Total_Data = [Total_Data ; Col_Sum] ;
        
        Header = [{'PRODUCT'},Unique_PortfolioGroup,{'TOTAL'}] ;
        Unique_ProductName = [Unique_ProductGroup ; {'TOTAL'}] ;
        
        Row_Data = [Unique_ProductName , Total_Data] ;
        OutNPVGroupData = [Header ; Row_Data] ;
        
    else
        OutErrorMsg = {'No Data found!'};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end