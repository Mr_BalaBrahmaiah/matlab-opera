function Output = derivativepricer_interpvols(Inputmatrix, Inputheading, Outputheading,PricingType)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/29 10:14:28 $
%  $Revision: 1.1 $
%
NumRows = size(Inputmatrix,1);

% Assign the default values for pricing
Lots      = num2cell(ones(NumRows,1));
Lot_Mult  = num2cell(ones(NumRows,1));
Curr_Mult = num2cell(ones(NumRows,1));
% Opt_DayCount = repmat(cellstr('trading'),NumRows,1);
Opt_DayCount = repmat(cellstr('calendar'),NumRows,1);

% % TODO - modified on 01 July 2014 for new TTM convention
% Pos_ValueDate    = find(strcmpi('value_date',Inputheading));
% ValueDate    = Inputmatrix(:,Pos_ValueDate);
% Pos_MaturityDate = find(strcmpi('maturity',Inputheading));
% MaturityDate = Inputmatrix(:,Pos_MaturityDate); %#ok<*FNDSB>
% MaturityDate(cellfun(@isempty,MaturityDate)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
% Time_Bump = num2cell(ones(NumRows,1));
% Idx = (datenum(cell2mat(MaturityDate)) - datenum(cell2mat(ValueDate))) <= 0;
% Time_Bump(Idx) =num2cell(1-0.01);
Time_Bump = num2cell(zeros(NumRows,1));
% % End of TODO - modified on 01 July 2014 for new TTM convention


if strcmpi(PricingType,'settle')
    Vol_Surf_Type = repmat(cellstr('fixed'),NumRows,1);
    Inputheading = ['Lots','Time_Bump','Lot_Mult','Curr_Mult','Opt_DayCount','Vol_Surf_Type',Inputheading];
    Inputmatrix  = [Lots,Time_Bump,Lot_Mult,Curr_Mult,Opt_DayCount,Vol_Surf_Type,Inputmatrix ];
else
    Inputheading = ['Lots','Time_Bump','Lot_Mult','Curr_Mult','Opt_DayCount',Inputheading];
    Inputmatrix  = [Lots,Time_Bump,Lot_Mult,Curr_Mult,Opt_DayCount,Inputmatrix ];
end

Pos_ValueDate    = find(strcmpi('value_date',Inputheading));
ValueDate    = datenum(Inputmatrix(:,Pos_ValueDate),'yyyy-mm-dd');
Pos_MaturityDate = find(strcmpi('maturity',Inputheading));
MaturityDate = Inputmatrix(:,Pos_MaturityDate); %#ok<*FNDSB>
MaturityDate(cellfun(@isempty,MaturityDate)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(MaturityDate,'yyyy-mm-dd');
IdxLiveMonths = MaturityDate >= ValueDate;

% computation of vol values
% For Vol computation
Pos_Strike1         = find(strcmpi('Strike1',Inputheading));
Pos_Strike2         = find(strcmpi('Strike2',Inputheading));
Pos_Vol1_id         = find(strcmpi('Vol1_id',Inputheading));
Pos_Vol2_id         = find(strcmpi('Vol2_id',Inputheading));
Pos_P1_Vol          = find(strcmpi('P1_Vol',Inputheading));
Pos_P2_Vol          = find(strcmpi('P2_Vol',Inputheading));
Pos_Subportfolio    = find(strcmpi('Subportfolio',Inputheading));
Pos_Vol_Surf_Type   = find(strcmpi('Vol_Surf_Type',Inputheading));
Pos_Und_FutId       = find(strcmpi('P1_name',Inputheading));
Pos_Opt_Type        = find(strcmpi('Opt_Type',Inputheading));
Pos_SettlementSource = find(strcmpi('settlement_source',Inputheading));

OptTypes = Inputmatrix(:, Pos_Opt_Type); %#ok<*FNDSB>
Idx_OptTypes = cellfun(@isnumeric, OptTypes);
OptTypes(Idx_OptTypes) = cellstr('');
OptionTypes = lower(OptTypes);
Subportfolio     = Inputmatrix(:, Pos_Subportfolio);
Vol_Surf_Type     = Inputmatrix(:, Pos_Vol_Surf_Type);
SettlementSource  = Inputmatrix(:, Pos_SettlementSource);

IdxVanilla = strcmpi(OptionTypes, 'vanilla_call') | strcmpi(OptionTypes, 'vanilla_put');
IdxBarOpt  = strcmpi(OptionTypes, 'euro_up_in_call')  | strcmpi(OptionTypes, 'euro_up_in_put') | strcmpi(OptionTypes, 'euro_up_out_call')  | strcmpi(OptionTypes, 'euro_up_out_put') | ...
    strcmpi(OptionTypes, 'euro_down_in_call')  | strcmpi(OptionTypes, 'euro_down_in_put') | strcmpi(OptionTypes, 'euro_down_out_call')  | strcmpi(OptionTypes, 'euro_down_out_put') | ...
    strcmpi(OptionTypes, 'amer_up_in_call')  | strcmpi(OptionTypes, 'amer_up_in_put') | strcmpi(OptionTypes, 'amer_up_out_call')  | strcmpi(OptionTypes, 'amer_up_out_put') | ...
    strcmpi(OptionTypes, 'amer_down_in_call')  | strcmpi(OptionTypes, 'amer_down_in_put') | strcmpi(OptionTypes, 'amer_down_out_call')  | strcmpi(OptionTypes, 'amer_down_out_put');
IdxSprOpt  = strcmpi(OptionTypes, 'spread_call')  | strcmpi(OptionTypes, 'spread_put');
IdxSerialVol = strcmpi(SettlementSource, 'otc')  & (IdxVanilla | IdxBarOpt) & IdxLiveMonths;

% TempVolId = Inputmatrix(IdxSerialVol, Pos_Vol1_id);
% SerialVolId = TempVolId;
% TempVol2Id = Inputmatrix(IdxSerialVol, Pos_Vol2_id);
% SerialVol2Id = TempVol2Id;
% TempFutId = Inputmatrix(IdxSerialVol, Pos_Und_FutId);
% TempMaturity = MaturityDate(IdxSerialVol);
% UndFutId = unique(TempFutId);
% IdxEuro = strncmpi('euro',OptTypes(IdxSerialVol),4);
% 
% for iFut = 1:length(UndFutId)
%     IdxVol = find(strcmpi(UndFutId{iFut},TempFutId));
%     [~,Indx] = min(TempMaturity(IdxVol));
%     FirstExpVol = TempVolId(IdxVol(Indx));
%     SerialVolId(IdxVol) = FirstExpVol;
% end
% SerialVol2Id(IdxEuro) = SerialVolId(IdxEuro);
% Inputmatrix(IdxSerialVol, Pos_Vol1_id) = SerialVolId;
% Inputmatrix(IdxSerialVol, Pos_Vol2_id) = SerialVol2Id;

SerialVolId = Inputmatrix(IdxSerialVol, Pos_Vol1_id);
SerialVol2Id = Inputmatrix(IdxSerialVol, Pos_Vol2_id);
SerialFutId = Inputmatrix(IdxSerialVol, Pos_Und_FutId);
UndFutId = unique(upper(SerialFutId));

IdxEuro = strncmpi('euro',OptTypes(IdxSerialVol),4);

ViewName = 'helper_get_underlying_id_for_options';
[ColNames,Data] = read_from_database(ViewName,0);
RefVolData = cell2dataset([ColNames;Data]);
RefMaturity = datenum(RefVolData.expiry_date,'yyyy-mm-dd');

for iFut = 1:length(UndFutId)
    IdxFutId = find(strcmpi(UndFutId{iFut},RefVolData.underlying_id));   
    [~,Indx] = min(RefMaturity(IdxFutId));
    FirstExpVol = RefVolData.vol_id(IdxFutId(Indx));
    IdxVol   = strcmpi(UndFutId{iFut},SerialFutId);
    SerialVolId(IdxVol) = FirstExpVol;
    disp([UndFutId{iFut},' : ',char(FirstExpVol)])
end

SerialVol2Id(IdxEuro) = SerialVolId(IdxEuro);
Inputmatrix(IdxSerialVol, Pos_Vol1_id) = SerialVolId;
Inputmatrix(IdxSerialVol, Pos_Vol2_id) = SerialVol2Id;

if ~isempty(Pos_Vol1_id)
    Vol1_id                = Inputmatrix(:, Pos_Vol1_id);
    if ~isempty(Pos_Strike1)
        Strike1            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike1));
    end
    if ~isempty(Pos_P1_Vol)
        P1_Vol            = utilgetvalidnumericarray(Inputmatrix(:, Pos_P1_Vol));
    end
end
if ~isempty(Pos_Vol2_id)
    Vol2_id                = Inputmatrix(:, Pos_Vol2_id);
    Vol2_id(cellfun(@isempty,Vol2_id)) = {''};
    if ~isempty(Pos_Strike2)
        Strike2            = utilgetvalidnumericarray(Inputmatrix(:, Pos_Strike2));
    end
    if ~isempty(Pos_P2_Vol)
        P2_Vol            = utilgetvalidnumericarray(Inputmatrix(:, Pos_P2_Vol));
    end
end

% For computation of Vol values
IdxInterpVol = (strcmpi(Vol_Surf_Type, 'fixed')) & (IdxVanilla | IdxBarOpt | IdxSprOpt);
if any(IdxInterpVol)
    if strcmpi(PricingType,'settle')
        % interpolate P1_vol
        Call_Put_Index = repmat(cellstr('C'),size(OptionTypes(IdxInterpVol)));
        PutIndex = cellfun(@isempty,strfind(OptionTypes(IdxInterpVol),'call'));
        Call_Put_Index(PutIndex) = cellstr('P');
        InputData.call_put_id = Call_Put_Index;       
        InputData.vol_id = Vol1_id(IdxInterpVol);
        InputData.strike = Strike1(IdxInterpVol);
        InputData.vol    = P1_Vol(IdxInterpVol);
%         if ~all(isnan(P1_Vol)) % when vols are not uploaded then it is not needed to interpolate
            Interp1Vol = interpolate_settlement_vol_strike(InputData);
            Inputmatrix(IdxInterpVol,Pos_P1_Vol) = num2cell(Interp1Vol);
%         end
        % interpolate P2_vol    
        IdxInterpVol2 = IdxInterpVol & ~strcmp(Vol2_id,'');        
        if any(IdxInterpVol2)            
            Call_Put_Index = repmat(cellstr('C'),size(OptionTypes(IdxInterpVol2)));
            PutIndex = cellfun(@isempty,strfind(OptionTypes(IdxInterpVol2),'call'));
            Call_Put_Index(PutIndex) = cellstr('P');
            InputData.call_put_id = Call_Put_Index;
            InputData.vol_id = Vol2_id(IdxInterpVol2);
            InputData.strike = Strike2(IdxInterpVol2);
            InputData.vol    = P2_Vol(IdxInterpVol2);
            Interp2Vol = interpolate_settlement_vol_strike(InputData);
            Inputmatrix(IdxInterpVol2,Pos_P2_Vol) = num2cell(Interp2Vol);            
        end
    else        
        % interpolate P1_vol
        InputData.subportfolio = Subportfolio(IdxInterpVol);
        InputData.vol_id = Vol1_id(IdxInterpVol);
        InputData.strike = Strike1(IdxInterpVol);
        InputData.vol    = P1_Vol(IdxInterpVol);
%         if ~all(isnan(P1_Vol)) % when vols are not uploaded then it is not needed to interpolate
            Interp1Vol = interpolate_traders_vol_strike(InputData);
            Inputmatrix(IdxInterpVol,Pos_P1_Vol) = num2cell(Interp1Vol);
%         end
        
        % interpolate P2_vol       
        IdxInterpVol2 = IdxInterpVol & ~strcmp(Vol2_id,'');
        if any(IdxInterpVol2)
            InputData.subportfolio = Subportfolio(IdxInterpVol2);
            InputData.vol_id = Vol2_id(IdxInterpVol2);
            InputData.strike = Strike2(IdxInterpVol2);
            InputData.vol    = P2_Vol(IdxInterpVol2);
            Interp2Vol = interpolate_traders_vol_strike(InputData);
            Inputmatrix(IdxInterpVol2,Pos_P2_Vol) = num2cell(Interp2Vol);
        end
    end
end

Output = derivativepricer(Inputmatrix, Inputheading, Outputheading);

Idx = strcmp(Output,' ');
Output(Idx) = num2cell(0);
NaNIdx  = (cellfun(@isnan,Output));
Output(NaNIdx) = num2cell(0);

Pos_SecurityID = find(strcmpi('Security_ID',Inputheading));
Security_Id = Inputmatrix(:, Pos_SecurityID);
Pos_Subportfolio    = find(strcmpi('Subportfolio',Inputheading));
Subportfolio     = Inputmatrix(:, Pos_Subportfolio);
Pos_ValueDate    = find(strcmpi('value_date',Inputheading));
ValueDate    = Inputmatrix(:,Pos_ValueDate);

Pos_OutSecurityId = find(strcmpi('Security_ID',Outputheading));
Pos_OutSubportfolio = find(strcmpi('Subportfolio',Outputheading));
Pos_OutValuedate = find(strcmpi('value_date',Outputheading));

Output(:,Pos_OutSecurityId) = Security_Id;
Output(:,Pos_OutValuedate) = ValueDate;

if strcmpi(PricingType,'traders')
    Output(:,Pos_OutSubportfolio) = Subportfolio;
end


end