function [OutErrorMsg,AccountingTRReport] = generate_previous_settle_accounting_traders_report(InBUName)

try
    OutErrorMsg = {'No errors'};
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    derivativepricerinterface_previous_settle_vols(InBUName,'settle',1,'system');

    ObjDB = connect_to_database;
    
    ViewName = 'helper_settle_pricing_previous_settle_vols_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);

    AccountingTRReport = getXLSFilename('Previous_Settle_Accounting_Traders_Report');
    
    UniqueFields = {'value_date','subportfolio'};
    SumFields = {'settle_delta_1','gamma_lots','settle_theta','settle_vega_1','mtm-usd'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'Summary Report');
    
    TableName = ['previous_settle_vol_accounting_tr_summary_table_',InBUName];
        set(ObjDB,'AutoCommit','off');
        DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
        SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, OutData);
        
    UniqueFields = {'value_date','subportfolio','contract_month','counterparty_parent'};
    SumFields = {'settle_delta_1','gamma_lots','settle_theta','settle_vega_1','mtm-usd'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'Detailed Report');
    
       
    try
        xls_delete_sheets(fullfile(pwd,AccountingTRReport));
    catch
    end
    
    [OutErrorMsg,MWTradersRpt] = generate_previous_settle_accounting_nc_monthwise_traders_report(InBUName);
     try         
%       SharedFolder = '\\SGTCX-DBAPP01\Opera\uploads\reports-cfs\MO Reports\Previous Settle Accounting Traders Reports';
        SharedFolder = ['\\SGTCX-DBAPP01\Opera\uploads\reports-',InBUName,'\MO Reports\Previous Settle Accounting Traders Reports'];
        DestinationFile = fullfile(SharedFolder,MWTradersRpt);
        movefile(MWTradersRpt,DestinationFile);
       
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    AccountingTRReport = '';
end