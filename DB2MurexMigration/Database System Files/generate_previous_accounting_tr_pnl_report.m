function [OutErrorMsg,PnlReport] = generate_previous_accounting_tr_pnl_report(InBUName)

try
    OutErrorMsg = {'No errors'};
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_previous_accounting_tr_pnl_view';
%     [ColNames,Data] = read_from_database(ViewName,0);
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
     
    TRData = cell2dataset([ColNames;Data]);
    
    PnlReport = getXLSFilename('Previous_Settle_Accounting_Pnl_Report');
    
    Subportfolios = unique(TRData.subportfolio);
    NumPortfolios = length(Subportfolios);
    
    Header = {'ValueDate','Subportfolio','delta','gamma','theta','vega',' Mtm - ABV-Usd ',' Mtm Prv Settle-Usd ',' Daily P&L ','Gamma P&L',' Vega P&L '};
    
    HolDates = {'2016-01-01';'2016-12-25';'2017-01-02';'2017-01-16';'2017-04-14';'2017-12-25';'2018-03-30'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    SqlQuery = 'select value_date from valuation_date_table';
    [~,Data] = read_from_database('valuation_date_table',0,SqlQuery);
    TodayDate = char(Data);
    
    VBusDays = busdays(datenum('2018-01-01'), datenum(TodayDate),'daily',HolidayVec);
    VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
    SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));
    
    for iPF = 1:NumPortfolios
        TempPF = regexprep(Subportfolios{iPF},'[^a-zA-Z0-9]','_');
        
        IdxPortfolio = strcmpi(Subportfolios{iPF},TRData.subportfolio);
        
        ValueDate = TRData.value_date(IdxPortfolio);
        M2M = TRData.mtm_abv_usd(IdxPortfolio);
        PrevSettleM2M = TRData.mtm_prv_settle_usd(IdxPortfolio);
        
        PortfolioData = [ValueDate,...
            TRData.subportfolio(IdxPortfolio),....
            num2cell(TRData.delta(IdxPortfolio)),...
            num2cell(TRData.gamma(IdxPortfolio)),...
            num2cell(TRData.theta(IdxPortfolio)),...
            num2cell(TRData.vega(IdxPortfolio)),...
            num2cell(M2M),...
            num2cell(PrevSettleM2M)];
        
        PortfolioData = sortrows(PortfolioData,1);
        
        
        DailyPnL = zeros(size(PortfolioData,1),1);
        GammaPnL = zeros(size(PortfolioData,1),1);
        VegaPnL = zeros(size(PortfolioData,1),1);
        
        LoopCntr = 1;
        try
            for i = 1:length(VBusDays)
                
                IdxToday = strcmpi(VDates{i},ValueDate);
                IdxYest   = strcmpi(SDates{i},ValueDate);
                
                TodaysM2M  = M2M(IdxToday);
                YestM2M    = M2M(IdxYest);
                PrevSettleM2MToday = PrevSettleM2M(IdxToday);
                
                if isempty(TodaysM2M) && isempty(YestM2M) && isempty(PrevSettleM2MToday)
                    continue;
                end
                
                if isempty(TodaysM2M) && ~isempty(YestM2M)
                    continue;
                end
                if isempty(TodaysM2M) && ~isempty(PrevSettleM2MToday)
                    continue;
                end
                if isempty(PrevSettleM2MToday) && ~isempty(YestM2M)
                    PrevSettleM2MToday = 0;
                end
                if  isempty(PrevSettleM2MToday) && ~isempty(TodaysM2M)
                    PrevSettleM2MToday = 0;
                end
                if isempty(YestM2M) && ~isempty(PrevSettleM2MToday)
                    YestM2M = 0;
                end
                if isempty(YestM2M) && ~isempty(TodaysM2M)
                    YestM2M = 0;
                end
                
                if i~=1
                    DailyPnL(LoopCntr) = TodaysM2M - YestM2M;
                    GammaPnL(LoopCntr) = PrevSettleM2MToday - YestM2M;
                    VegaPnL(LoopCntr)  = DailyPnL(LoopCntr) - GammaPnL(LoopCntr);
                else
                    DailyPnL(LoopCntr) = TodaysM2M;
                    GammaPnL(LoopCntr) = PrevSettleM2MToday;
                    VegaPnL(LoopCntr)  = DailyPnL(LoopCntr) - GammaPnL(LoopCntr);
                end
                LoopCntr = LoopCntr + 1;
            end
            
            OutData = [PortfolioData,num2cell(DailyPnL),num2cell(GammaPnL),num2cell(VegaPnL)];
            xlswrite(PnlReport,[Header;OutData],TempPF);
        catch
            disp(TempPF);
            continue;
        end
    end
    
    try
        xls_delete_sheets(fullfile(pwd,PnlReport));
    catch
    end
    
    [OutErrorMsg,MWPnlReport] = generate_previous_accounting_nc_monthwise_tr_pnl_report(InBUName);
    try
%       SharedFolder = '\\SGTCX-DBAPP01\Opera\uploads\reports-cfs\MO Reports\Previous Settle Accounting Pnl Report';
        SharedFolder = ['\\SGTCX-DBAPP01\Opera\uploads\reports-',InBUName,'\MO Reports\Previous Settle Accounting Pnl Report'];
        DestinationFile = fullfile(SharedFolder,MWPnlReport);
        movefile(MWPnlReport,DestinationFile);
       
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    PnlReport = '';
end