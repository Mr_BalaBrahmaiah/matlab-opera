function [ReportHeader,ReportData,OutFilename] = xlswrite_sep_sheet_4_unique_fields(Rounded_Header,Two_Decimal_Header,Unique_Header,ReportHeader,ReportData,xlsName,Want_Sep_Sheet)
%%% Example 

% Rounded_Header = {'Live Pos Lot','Delta Lot','Delta MT','Vega <USD>','Theta <USD>'};
% Two_Decimal_Header = {'Gamma USD MT'};
% Unique_Header = 'Product';
% ReportHeader = {'Output_Headers'};
% ReportData = {'Output_Data'};
% xlsName = {'OutFileName'};
% Want_Sep_Sheet = {'1 means Seperate'};

%%

OutFilename = getXLSFilename(xlsName);

% Rounded_Header = {'Live Pos Lot','Delta Lot','Delta MT','Vega <USD>','Theta <USD>'};
% Two_Decimal_Header = {'Gamma USD MT'};


for i = 1 : length(Rounded_Header)
    
    ColNum = find(strcmpi(Rounded_Header{i},ReportHeader));
    ReportData(:,ColNum) = cellfun(@(s) (round(s)), ReportData(:,ColNum),'UniformOutput',false);
    
end

for i = 1 : length(Two_Decimal_Header)
    ColNum = find(strcmpi(Two_Decimal_Header{i},ReportHeader));
    ReportData(:,ColNum) = cellfun(@(s) (sprintf('%.2f',s)), ReportData(:,ColNum),'UniformOutput',false);
    %      ReportData(:,ColNum) = regexprep(ReportData(:,ColNum), '''', '');
end

%%
if(Want_Sep_Sheet)
    
    ColNum = strcmpi(Unique_Header,ReportHeader);  %% 'Product' HeaderName
    Unique_Product = unique(ReportData(:,ColNum));
    
    for k = 1 : length(Unique_Product)
        %     ispresent = cellfun(@(s) ~isempty(strfind(Unique_Product{k}, s)), ReportData(:,ColNum));
        ispresent = cellStrfind_Perfect(ReportData(:,ColNum),Unique_Product(k));
        
        Matching_Data = ReportData(ispresent,:);
        xlswrite(OutFilename,[ReportHeader;Matching_Data],Unique_Product{k});
    end
    
else
    xlswrite(OutFilename,[ReportHeader;ReportData],xlsName);
end

try
    OutXLSFileName = fullfile(pwd,OutFilename);
    xls_delete_sheets(OutXLSFileName);
catch
end

%% Use only when we are using global variable

% OutFilename_Total = strvcat(OutFilename_Total,OutFilename); %% strvcat


end