function [OutErrorMessage,NC_PnL,USD_PnL] = calculate_netting_pnl(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName)

OutErrorMessage = {'No errors'};
NC_PnL = '0'; USD_PnL = '0';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
   
    %% validations before netting
    if ~isnumeric(BuyLotsToBeNetted)
        BuyLotsToBeNetted = cell2mat(BuyLotsToBeNetted);
    end
    if ~isnumeric(SellLotsToBeNetted)
        SellLotsToBeNetted = cell2mat(SellLotsToBeNetted);
    end
    
    TradeId = [BuyTradeId;SellTradeId];
    LotsToBeNetted = [BuyLotsToBeNetted;SellLotsToBeNetted];
    
    if ~isnumericequal(abs(sum(BuyLotsToBeNetted)),abs(sum(SellLotsToBeNetted)))
        OutErrorMessage = {'Error! BuyLots and SellLots must be equal!'};
        return;
    end
    
    ObjDB = connect_to_database;
    ClonedDealTicketTableName = ['cloned_deal_ticket_table_',InBUName];         
    
    IdxRemove = [];    
    DBData = [];
    for iT = 1:length(TradeId)
        try
%             if strcmpi(InBUName,'QF1') || strcmpi(InBUName,'QF4')
%                 SqlQuery = ['select subportfolio,security_id,counterparty_parent from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iT},''''];
%             else
                SqlQuery = ['select subportfolio,security_id,counterparty_parent,premium from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iT},''''];
%             end
            
            Temp = fetch(ObjDB,SqlQuery);
            if isempty(Temp)
                IdxRemove = [IdxRemove , iT];
                continue;
            end
            
            SecurityID = Temp(2);
            IdxYieldNeeded = strncmpi(SecurityID,'BN XM',length('BN XM')) | ...
                strncmpi(SecurityID,'BN YM',length('BN YM')) | ...
                strncmpi(SecurityID,'MM IR',length('MM IR')) | ...
                strncmpi(SecurityID,'MM ZB',length('MM ZB'));
            
            if IdxYieldNeeded
                if strcmpi(InBUName,'QF1') || strcmpi(InBUName,'QF4')
                    Viewname = ['helper_calculate_trade_price_yield_view_',InBUName];
                    SqlQuery = ['select original_premium from ',Viewname,' where trade_id = ''',TradeId{iT},''''];
                    Premium = fetch(ObjDB,SqlQuery);
                    % Temp = [Temp,Premium]; 
                    Temp(end) = Premium;
                end
            end
            DBData = [DBData;Temp]; 
        catch
            IdxRemove = [IdxRemove , iT];
            continue;
        end
    end
    
    if ~isempty(IdxRemove)
        TradeId(IdxRemove) = [];
        LotsToBeNetted(IdxRemove) = [];
    end    
    
    Subportfolio = DBData(:,1);
    SecurityId   = DBData(:,2);
    CounterpartyParent = DBData(:,3);
    Premium = DBData(:,4);
    
    if isempty(TradeId)
        OutErrorMessage = {'Error! No data selected for netting!'};
        return;
    end
   
    
    DistinctCP = unique(upper(CounterpartyParent));
    if length(DistinctCP)~= 1
        OutErrorMessage = {'Error! Counterparty names selected for netting should be the same!'};
        return;
    end
    
    DistinctPF = unique(upper(Subportfolio));
    if length(DistinctPF)~= 1
        OutErrorMessage = {'Error! Portfolios selected for netting should be the same!'};
        return;
    end
    
    ContractMonth  = cell(size(SecurityId));
    DerivativeType = cell(size(SecurityId));
    ProductCode    = cell(size(SecurityId));
    UnderlyingID   = cell(size(SecurityId));
    
    Tablename = ['security_info_table_',InBUName];
    for iSec = 1:length(SecurityId)
        SqlQuery = ['select contract_month,derivative_type,product_code,underlying1_id from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        SecInfoNetData = fetch(ObjDB,SqlQuery);
        ContractMonth(iSec)  = SecInfoNetData(1);
        DerivativeType(iSec) = SecInfoNetData(2);
        ProductCode(iSec)    = SecInfoNetData(3);      
        UnderlyingID(iSec)   = SecInfoNetData(4);
    end
    
    IdxFX = cellfun(@strncmpi,DerivativeType,repmat({'fx_forward'},size(DerivativeType)),repmat({length('fx_forward')},size(DerivativeType)));
    IdxEQDSWP = cellfun(@strncmpi,DerivativeType,repmat({'equity_swap'},size(DerivativeType)),repmat({length('equity_swap')},size(DerivativeType)));
    IdxIDXSWP = cellfun(@strncmpi,DerivativeType,repmat({'index_swap'},size(DerivativeType)),repmat({length('index_swap')},size(DerivativeType)));
    %%% Newly added line on 2019-12-02
    %%% FX-Option types like fx_vanilla_call and fx_vanilla_put, no need to check the contract month as its not valid 
    IdxFXOPT = cellfun(@strncmpi,DerivativeType,repmat({'fx_vanilla'},size(DerivativeType)),repmat({length('fx_vanilla')},size(DerivativeType)));

    if ~any(IdxFX) && ~any(IdxEQDSWP) && ~any(IdxIDXSWP) && ~any(IdxFXOPT) % FX or EQD_SWAP or IDX_SWAP types and FX-Options,, no need to check the contract month as its not valid
        if any(cellfun(@isempty,ContractMonth))
            OutErrorMessage = {'Error! ContractMonth selected for netting cannot be empty!'};
            return;
        end
        
        DistinctCM = unique(upper(ContractMonth));
        if length(DistinctCM)~= 1
            OutErrorMessage = {'Error! ContractMonths selected for netting should be the same!'};
            return;
        end
    end
    
    DistinctOptType = unique(upper(DerivativeType));
    if length(DistinctOptType)~= 1
        OutErrorMessage = {'Error! DerivativeType selected for netting should be the same!'};
        return;
    end            
    
    DistinctProdCode = unique(upper(ProductCode));
    if length(DistinctProdCode)~= 1
        OutErrorMessage = {'Error! ProductCode selected for netting should be the same!'};
        return;
    end

    SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    Premium = cell2mat(Premium);    
    
    SqlQuery = ['select murex_mult,curr_mult_type,curr_mult_factor from product_master_table where invenio_product_code = ''',char(DistinctProdCode),''''];
    DBProdMultData   = fetch(ObjDB,SqlQuery);
    MultFactor       = cell2mat(DBProdMultData(1));
    Curr_MulType     = DBProdMultData(2);
    
    %%% Newly added logic (to get currency, find the string staring with "CR" and remove the "USD") 
    if strncmp(DistinctProdCode,'CR',2)    
        [~,Currencypair] = strtok(DistinctProdCode,' ');
        Currency = strrep(Currencypair ,' USD','');
        SqlQuery = ['select spot from currency_spot_mapping_table where currency = ''',char(Currency),''''];
        Curr_MulFactor = fetch(ObjDB,SqlQuery);
    else
        Curr_MulFactor   = DBProdMultData(3);
    end
    %%% End Code
    
    SqlQuery = ['select settle_value from underlying_settle_value_table where settlement_date = ''',char(SettleDate),...
        ''' and underlying_id = ''',char(Curr_MulFactor),''''];
    SpotRate = fetch(ObjDB, SqlQuery);
  
%     try
%         SqlQuery = ['select settle_value from underlying_settle_value_table where settlement_date = ''',char(SettleDate),...
%             ''' and underlying_id in (''',strrep(convertCell2Char(UnderlyingID),',',''','''),''')']; %% Fix - UnderlyingID is an array; check how this can be used
%         FxForwardRate = fetch(ObjDB, SqlQuery);
%         if isempty(FxForwardRate)
%             FxForwardRate = 1;
%         end
%     catch
%         FxForwardRate = 1;
%     end
    
    if strcmpi(InBUName,'cfs')
       TradeId = [BuyTradeId;SellTradeId];
       CharTradeId = ['''',strrep(convertCell2Char(TradeId),',',''','''),''''];
       SqlQuery = ['select is_premium_paid from rms_premium_paid_table_cfs where trade_id in (',CharTradeId,')'];
       IsPremiumPaid = logical(cell2mat(fetch(ObjDB,SqlQuery)));
       Premium(IsPremiumPaid) = 0;
    end

    format bank;
    NC_PnL = sum(LotsToBeNetted .* Premium .* MultFactor .* -1);    
    
    if(strcmpi(Curr_MulType,'multiplication'))
        USD_PnL = NC_PnL .* cell2mat(SpotRate);
    else
%         USD_PnL = (NC_PnL ./ cell2mat(SpotRate)) .* (1 / cell2mat(FxForwardRate)); %% Fix - To get the correct USD pnl for FXD, 
        USD_PnL = (NC_PnL ./ cell2mat(SpotRate)); %% Fix - To get the correct USD pnl for FXD, 
        % multiply the current value with (constant_nc_factor(1 or from product master table)/FxForwardRate)
    end
    
    if strcmpi(InBUName,'QF5')
        USD_PnL = NC_PnL;
    end
    
    NC_PnL  = sprintf('%f',NC_PnL);
    USD_PnL = sprintf('%f',USD_PnL);
    
catch ME
    OutErrorMessage = {ME.message};
end