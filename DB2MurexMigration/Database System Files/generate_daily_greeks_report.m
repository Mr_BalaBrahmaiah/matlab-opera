function [OutErrorMsg,OutFilename] = generate_daily_greeks_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    try
        if strcmpi(InBUName,'cfs')
            OutFilename = generate_riskoffice_reports_cfs;
            return;
        end
    catch ME
        OutErrorMsg = cellstr(ME.message);
    end
    
    try   %%% Newly added BU (22-05-2019) --> strcmpi(InBUName,'cot')
        if strcmpi(InBUName,'coc') || strcmpi(InBUName,'sgr') || strcmpi(InBUName,'dry') || strcmpi(InBUName,'cot') || strcmpi(InBUName,'plm') || strcmpi(InBUName,'rbr')
            OutFilename = generate_riskoffice_reports_coc(InBUName);
            return;
        end
        
        if strcmpi(InBUName,'cof')
            OutFilename = generate_riskoffice_reports_cof(InBUName);
            return;
        end
        
        if strcmpi(InBUName,'grn')
            OutFilename = generate_riskoffice_reports_grn(InBUName);
            return;
        end
        
    catch ME
        OutErrorMsg = cellstr(ME.message);
    end
    
    %     OutFilename = getXLSFilename('Daily_Greeks_Report');
    Filename = ['Daily_Greeks_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'OFUT','CSO'}); %% newly added on 28-11-2019 (Grp_Type = 'CSO')
        Data = Data(ispresent_FUT_OFUT,:);
        
        if ~isempty(Data)
            Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
            Get_CallPut = Data(:,Find_CallPut_Col);
            ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
            Data(ispresent_Call,Find_CallPut_Col) = {'call'};
            ispresent_Put = cellStrfind(Get_CallPut,'p');
            Data(ispresent_Put,Find_CallPut_Col) = {'put'};        %% Get 'Call/Put' Row only
            
            UniqueFields = {'portfolio','product_name','strike','call_put_id','contract_month'};
            SumFields = {'active_lots','settle_delta_1','settle_delta_1_MT','settle_gamma_11','settle_vega_1'};
            OutputFields = {'portfolio','product_name','strike','call_put_id','contract_month',...
                'active_lots','settle_delta_1','settle_delta_1_MT','settle_gamma_11','settle_vega_1'};
            WeightedAverageFields = [];
            [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            OutHeader = {'Portfolio','Product','Strike','Call/Put','Maturity Label',...
                'Nominal','Delta_Lots','Delta_MT','Gamma','Vega'};  %% Check 'Delta_MT' now its put 'settle_delta_1'
            
            xlswrite(OutFilename,[OutHeader;OutData]);
        else
            OutErrorMsg = {'No Options found!'};
        end
    else
        OutErrorMsg = {'No data found!'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end