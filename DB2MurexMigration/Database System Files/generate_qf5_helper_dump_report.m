function [OutErrorMsg ,OutXLSName ] = generate_qf5_helper_dump_report(InBUName)

%%% Project Name: QF5 Helper Dump
%%% Module Name: QF5 Helper Dump
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 08 Nov 2019 / 14:55:55

%%%% [OutErrorMsg ,OutXLSName ] = generate_qf5_helper_dump_report('qf5')

try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename([lower(char(InBUName)),'_helper_dump_report']);  % output file name
    
    %%% Connect to Database
    ObjDB = connect_to_database;

    %%% Fetch query from Table
    Matlab_Table_Name = 'matlab_query_table';
    SqlQuery_Matlab_Table_Name = ['select query_code from ',char(Matlab_Table_Name),' where query_unique_name = ''','qf5_dump',''' '] ;
    [~,Matlab_Table_Name_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Matlab_Table_Name);

    if strcmpi(Matlab_Table_Name_Data,'No Data') 
        OutErrorMsg = {['No SQL query in ',char(Matlab_Table_Name), ' for QF5 Helper Dump']};
        return;
    end
    
    %%% Fetch data from Qf5 Helper View
    View_query = char(Matlab_Table_Name_Data);
    [Qf5_view_Cols,Qf5_view_Data] = Fetch_DB_Data(ObjDB,View_query);
    
     if strcmpi(Qf5_view_Data,'No Data') 
        OutErrorMsg = {'Data not Available in helper_evaluation_reports_view_qf5'};
        return;
     end
    
    %%%write the data into excel sheet & Delete Empty sheets and active user sheet
    xlswrite(OutXLSName,[Qf5_view_Cols;Qf5_view_Data],'Qf5 Dump');
    xls_delete_sheets(fullfile(pwd,OutXLSName));
     
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end

