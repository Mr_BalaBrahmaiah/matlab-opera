
HolDates = {'2016-01-01';'2016-12-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2016-01-04'), datenum('2016-03-31'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

objDB = connect_to_database;
IsPriceUpdated = 0;

for i = 1:length(VBusDays)
    SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    
    % to do pricing for the value date if not available and remove the data
    % once done
    SqlQuery = ['select count(*) from security_settlement_value_table where value_date = ''',VDates{i},''''];
    NumRecords = cell2mat(fetch(objDB,SqlQuery));
    
    if NumRecords == 0
        derivativepricerinterface('settle',1,'ananthi');
        IsPriceUpdated = 1;   
    end
    
    ViewName = 'helper_dir_book_report_new_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0);

UniqueFields = {'contract_number'};
SumFields = {'acc_value'};
OutputFields = {'value_date','contract_number','acc_value'};
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

 TableName = 'dirbook_tid_basis_daily_pnl_table';    
%     SqlQuery = ['delete from ',TableName,' where value_date =''',VDates{i},''''];
%     curs = exec(objDB,SqlQuery);
upload_in_database(TableName, OutData);

if IsPriceUpdated
    SqlQuery = ['delete from security_settlement_value_table where value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
end

end