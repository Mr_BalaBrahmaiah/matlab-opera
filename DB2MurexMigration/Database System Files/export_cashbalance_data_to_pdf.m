function [OutErrorMsg,OutPDFFile]=export_cashbalance_data_to_pdf(InBUName,client_code,Currency,S_Date,E_Date)

%% Input Parameter Elaboration.
%[OutErrorMsg,OutPDFFile]=export_cashbalance_data_to_pdf('cfs','ada','','2018-10-10','2018-10-31')
% InBUName='Input_Business_Unit_Name...generally we are using 'cfs'.;
% 'S_Date=Start_Date';
% 'E_Date=End Date';
% Currency is the Units.Taken as usd.;
disp('InBUName');disp(InBUName);disp(class(InBUName));
disp('client_code');disp(client_code); disp(class(client_code));
disp('Currency');disp(Currency); disp(class(Currency));
disp('S_Date');disp(S_Date); disp(class(S_Date));
disp('E_Date');disp(E_Date); disp(class(E_Date));

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

OutErrorMsg = {'No Errors'};
OutPDFFile=getXLSFilename(['CashBalanceData_',client_code]);
%% To make DOMCompilable in Order to execute it...we have fixed it.
makeDOMCompilable();

%% DOM API CALLS
import mlreportgen.dom.*;
import mlreportgen.report.*;

%% Cash Balance Document Creation
% Template Doc6 should be placed on the current path of the Matlab.
% Document Name can be changes as per the requirement.
OutPDFFile=Document(['CashBalanceData_',client_code],'docx','Doc6');

%% Document Opening
open(OutPDFFile);

%% Page layout settings.
% Here We Have to Manipulate page size as per the requirement.PLease Change Input Argument If Necessary.
s=OutPDFFile.CurrentPageLayout;
s.PageSize.Height='8.27in'; % Recommended Page size 19.5"*8.27"
s.PageSize.Width='19.5in';
s.PageMargins.Left='0.12in';
s.PageMargins.Right='0.12in';
s.PageMargins.Top='0.55in';
s.PageMargins.Bottom='0.55in';
s.PageMargins.Header='0.3in';
s.PageMargins.Footer='0.3in';

try
    %% Code In Order to get Client Information
    ObjDB = connect_to_database;
    
    ClientInformation_Table = strcat('client_information_table_',char(InBUName));
    [ClientInformation_Table_ColNames,ClientInformation_Table_Data] = Fetch_DB_Data(ObjDB,[],ClientInformation_Table);
    
    if isempty(ClientInformation_Table_Data)
        OutErrorMsg={'No Data Found in ClientInformation_Table'};
    end
    
    
    
    ClientInformation_Table = cell2dataset([ClientInformation_Table_ColNames;ClientInformation_Table_Data]);
    
    
    Current_Client_Name =  ClientInformation_Table.clients_legal_name(strcmpi(ClientInformation_Table.client_code,char(client_code)));
    Current_Client_AccountNo = ClientInformation_Table.account_no(strcmpi(ClientInformation_Table.client_code,char(client_code)));
    Current_Client_Address = ClientInformation_Table.address(strcmpi(ClientInformation_Table.client_code,char(client_code)));
    %Confirmation_Date=E_Date;
    %Current_Client_Data=[ Confirmation_Date; Current_Client_Name;  Current_Client_AccountNo;  Current_Client_Address;];
    
    
    %% Client Information Header.
    Fixed_Header_Col_Size=21;
    Current_Client_Info = cell(7,Fixed_Header_Col_Size);
    Current_Client_Info(3,1) = cellstr('CONFIRMATION DATE:');
    Current_Client_Info(4,1) = cellstr('CLIENT NAME');
    Current_Client_Info(5,1) = cellstr('ACCOUNT NO.');
    Current_Client_Info(6,1) = cellstr('ADDRESS');
    Current_Client_Info(3,2) = cellstr(E_Date);
    
    if isempty(Current_Client_Name)
        Current_Client_Name = {'Clients Legal Name not available!'};
    end
    Current_Client_Info(4,2) = Current_Client_Name;
    
    if isempty(Current_Client_AccountNo)
        Current_Client_AccountNo = {'Clients Account No not available!'};
    end
    Current_Client_Info(5,2) = Current_Client_AccountNo;
    
    if isempty(Current_Client_Address)
        Current_Client_Address = {'Clients Address information not available!'};
    end
    Current_Client_Info(6,2) = Current_Client_Address;
    %Current_Client_Info=[ Current_Client_Info,Current_Client_Data];
    % Empty Space Calculation.
    EMPTY_SPACE_INDEX=find(cellfun(@isempty,  Current_Client_Info(:,1)));
    
    %% Data Handling with Confirmation Date.
    CONFIRMATION_DATE=find(strcmpi('CONFIRMATION DATE:', Current_Client_Info(:,1)));
    CONFIRMATION_DATE_CHECK=~isempty(CONFIRMATION_DATE);
    
    if (CONFIRMATION_DATE_CHECK==1)
        a=find(strcmpi('CONFIRMATION DATE:',Current_Client_Info(:,1)));  %% Position.
        d=EMPTY_SPACE_INDEX-a;
        e=find(d>0, 1 );
        dynamic_space=a+d(e); %% Dynamic space
        
        %% OTC SWAPS SECTION Data Accession
        OTC_CONFIRMATION_SECTION=Current_Client_Info(a:dynamic_space-1,1:end);
    end
    
    %% Client Information appension in Template.
    
    header_data = OTC_CONFIRMATION_SECTION(:,2); % Acessing second column data which is client information.
    page_layout = OutPDFFile.CurrentPageLayout; % Pagelayout settings
    header = page_layout.PageHeaders(1); %% To access page header
    header.moveToNextHole;
    append(header,header_data{1});
    header.moveToNextHole;
    append(header,header_data{2});
    header.moveToNextHole;
    append(header,header_data{3});
    header.moveToNextHole;
    append(header,header_data{4});
    
    Table_Name=['client_cash_account_balance_table_',InBUName];
    disp('In Line 127');
    %% If all the inputs are given by the user.
    if (~isempty(InBUName) && ~isempty(client_code) && ~isempty(Currency) && ~isempty(S_Date) && ~isempty(E_Date))
        SqlQuery=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date between ''',char(S_Date),''' and ''',char(E_Date),''' and currency = ''',char(Currency),''' '];
        [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
        %% Data Check New Section
        if (strcmpi(Data,'No Data'))
            OutErrorMsg={'No Data is available'};
        end
    end
    disp('In Line 133');
    %% if currency is empty
    if isempty(Currency)
        if (~isempty(InBUName) && ~isempty(client_code) && ~isempty(S_Date) && ~isempty(E_Date))
            % Please Check this query.
            SqlQuery1=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date between ''',char(S_Date),''' and ''',char(E_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery1);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end
        
        end
        disp('In Line 141');
        %% if currency and startdate is empty
        if (~isempty(InBUName) && ~isempty(client_code)  && isempty(S_Date) && ~isempty(E_Date))
            SqlQuery=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date <= ''',char(E_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end
            
        end
        disp('In Line 147');
        %% if currency and enddate is empty.
        if (~isempty(InBUName) && ~isempty(client_code) && ~isempty(S_Date) && isempty(E_Date))
            SqlQuery2=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date >= ''',char(S_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery2);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end

        end
        disp('In Line 153');
        %% if currency start date and enddate is empty.
        if (~isempty(InBUName) && ~isempty(client_code)  && isempty(S_Date) && isempty(E_Date))
            SqlQuery=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end
            
        end
        disp('In Line 159');
    end
    
    if ~isempty(Currency)
        if (~isempty(InBUName) && ~isempty(client_code) && ~isempty(S_Date) && ~isempty(E_Date))
            SqlQuery1=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date between ''',char(S_Date),''' and ''',char(E_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery1);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end

        end
        %% if currency and startdate is empty
        if (~isempty(InBUName) && ~isempty(client_code)  && isempty(S_Date) && ~isempty(E_Date))
            SqlQuery=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date <= ''',char(E_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end

        end
        
        %% if currency and enddate is empty.
        if (~isempty(InBUName) && ~isempty(client_code) && ~isempty(S_Date) && isempty(E_Date))
            SqlQuery2=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' and cob_date >= ''',char(S_Date),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery2);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end
            

        end
        
        %% if currency start date and enddate is empty.
        if (~isempty(InBUName) && ~isempty(client_code)  && isempty(S_Date) && isempty(E_Date))
            SqlQuery=['select * from ',char(Table_Name),' where client_code = ''',char(client_code),''' '];
            [SettleColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
            %% Data Check New Section
            if (strcmpi(Data,'No Data'))
                OutErrorMsg={'No Data is available'};
            end
            
           
        end
    end
    %% TWO DECIMAL POINT CODE.
    Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric(x)),Data,'UniformOutput',false));
    Exact_Value = cellfun(@(s) (sprintf('%.2f',s)), Data(Numeric_Index),'UniformOutput',0);
    Data( Numeric_Index)=Exact_Value;
    
    %% Data Formating for PDF generation.
    Overall_Data=[SettleColNames;Data];
    a=strcmpi('client_name',Overall_Data(1,:));
    Overall_Data(:,a)=[];
    b=strcmpi('client_code',Overall_Data(1,:));
    Overall_Data(:,b)=[];
    c=strcmpi('currency',Overall_Data(1,:));
    Overall_Data(:,c)=[];
    
    %% Page layout Management
    p1 = Table(Overall_Data);
    p1.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),Bold};
    p1.Border='None';
    p1.TableEntriesHAlign='Right';
    append(OutPDFFile,p1);
    close(OutPDFFile);
    
    %docview(char(OutPDFFile),'convertdocxtopdf');
    % Please put the same file name as there in Document.
    docview(['CashBalanceData_',client_code],'convertdocxtopdf');
    [Pathname,Filename] = fileparts(OutPDFFile.OutputPath);
    Filename = [Filename,'.pdf'];
    %OutPDFFile = fullfile(Pathname,Filename);
    OutPDFFile = fullfile(Filename);
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end







