function [OutErrorMsg,OutXLSName]=Expiry_position_monitoring(InBUName)
% implemeted for InBUName='qf4'

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

OutErrorMsg={'No Errors'};
OutXLSName = getXLSFilename(['Expiry_position_monitoring_',lower(InBUName)]);

try
    ObjDB = connect_to_database;
    
    TableName='Valuation_date_table';
    sqlquery=['select * from ',TableName];
    [~,Data] = Fetch_DB_Data(ObjDB,sqlquery);
    Value_date=Data(:,2); % This Settle date fetching can me made dynamic by strcmpi command.
    
    %% Error Handling Over Value_Date
    if strcmpi(Value_date,'No Data')
      OutErrorMsg={'No value date avilable in DB'};
      return;
    end
    
    %% Strore Procedure call on the
    Procedure_Name=['generate_position_monitoring_data_expiry_',char(InBUName)];
    sqlquery2=['call ',Procedure_Name,'(''',char(Value_date),''')'];
    [Procedure_Headers,Procedure_Data] = Fetch_DB_Data(ObjDB,sqlquery2);
    
    %% Error Handling Over Procedure_Data
    if strcmpi(Procedure_Data,'No Data')
      OutErrorMsg={['For ',char(Value_date),' No data found in Strore Procedure table']};
      return;
    end
    if isequal(Procedure_Data,0)
      OutErrorMsg={['For ',char(Value_date),' No data found in Strore Procedure table']};
      return;
    end
    
    %% cell2dataset for easy access of data.
    Overall_Data_Pro=cell2dataset([Procedure_Headers;Procedure_Data]);
    
    %% Data Accession as per the report Requirement.
    settlement_date = repmat(Data(3),length(Overall_Data_Pro),1);
    
    %% Headers as per the Report Requirement
    Overall_Data_Req=[ settlement_date,Overall_Data_Pro.counterparty_parent,Overall_Data_Pro.instrument,Overall_Data_Pro.bbg_product_code,Overall_Data_Pro.product_name,Overall_Data_Pro.contract_month,Overall_Data_Pro.derivative_type,Overall_Data_Pro.maturity_date,num2cell(Overall_Data_Pro.active_lots),Overall_Data_Pro.subportfolio];
    Overall_Data_Header={'Settle_date','Counterparty_parent','Instrument','product_code','product_name','contract_month','derivative type','maturity_date','position_lots','BU'};
    
    %% Consolidate Function
    EPM_Header =  Overall_Data_Header;
    EPM_Data = Overall_Data_Req;
    UniqueFields = {'Counterparty_parent','Instrument', 'product_code','contract_month','derivative type'};
    SumFields = {'position_lots'}; % hERE ACTIVE LOTS=POSITION LOTS.
    OutputFields = EPM_Header ;
    WeightedAverageFields = [];
    [EPM_Header,EPM_Data_Consolidate] = consolidatedata(EPM_Header,EPM_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    Overall_Data_Con=[EPM_Header;EPM_Data_Consolidate];
    
    %% Date Logic Manipulation
    % Maturity_Date_Index
    Maturity_Date_Index=strcmpi('maturity_date',Overall_Data_Con(1,:));
    Settle_date_Index=strcmpi('Settle_date',Overall_Data_Con(1,:));
    
    %% Indexes where Maturity Date is less than settle date.
    Index_val = find(datenum(Overall_Data_Con(2:end, Maturity_Date_Index)) < datenum(Overall_Data_Con(2:end,Settle_date_Index)));
    
    EPM_Data_Consolidate (Index_val,:)=[];
    
    Diff_bw_expiry = abs(days252bus(char(EPM_Data_Consolidate(:,Maturity_Date_Index)),char(EPM_Data_Consolidate(:,Settle_date_Index))));
    Diff_bw_expiry_index=find(Diff_bw_expiry<=3);
    
    %% Diff_bw_expiry column addition
    EPM_Data_Final=[EPM_Data_Consolidate, num2cell( Diff_bw_expiry)];
    
    %% Header Values
    EPM_Header_Final= {'Settle_date','Counterparty_parent','Instrument','product_code','product_name','contract_month','derivative type','maturity_date','position_lots','BU','Diff_exp_settle'};
    
    %% Final Data to be written on Excel sheet
    Overall_Final_Data= EPM_Data_Final(Diff_bw_expiry_index,:);
    %index=find(~cellfun(@isempty,Overall_Final_Data(:,9)));
    Overall_Final_Data_with_Headers=[ EPM_Header_Final; Overall_Final_Data];
      
    %% Last Requirement 
    Type_Cast=cell2mat( Overall_Final_Data(1:end,9));
    Non_Zero_Position_Index= find(Type_Cast~=0); % Here If required to fetch Zero position Index just change the logic Type_Cast=0;
        
    if isempty(Non_Zero_Position_Index)
        Overall_Non_Zero_Position_Data =EPM_Header_Final;
        xlswrite(OutXLSName,  Overall_Non_Zero_Position_Data,'Expiry_position');
        %xls_change_activesheet(OutXLSName ,'Expiry_position');
    else
        %Non_Zero_Position_Data= Overall_Data_Con( Non_Zero_Position_Index,:);
        Non_Zero_Position_Data= Overall_Final_Data( Non_Zero_Position_Index,:);
        Overall_Non_Zero_Position_Data=[EPM_Header_Final;  Non_Zero_Position_Data];
        xlswrite(OutXLSName,  Overall_Non_Zero_Position_Data,'Expiry_position');
        %xls_change_activesheet(OutXLSName ,'Expiry_position');
    end

    %% Xlswrite in order to write excelsheet
   xlswrite(OutXLSName,Overall_Final_Data_with_Headers,'Position_LT-3_QF4');
   % xlswrite(OutXLSName,Overall_Final_Data_with_Headers,'position_qf4444');
   %xls_delete_sheets(fullfile(pwd,OutXLSName));
   % xls_change_activesheet(OutXLSName,'position_qf4');
   
    %OutXLSName = fullfile(pwd,OutXLSName) ;
    xls_delete_sheets(fullfile(pwd,OutXLSName)) ;
    xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Expiry_position');
    
catch ME
    
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end
end


