InBUName = 'qf5';

ObjDB = connect_to_database;

DB_System_Format = 'yyyy-mm-dd';

Current_ValueDate = '2018-07-13';

%% Read Input File

[filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
InXLSFilename = fullfile(pathname,filename);

[~,~,RawData] = xlsread(InXLSFilename);

Required_Data = RawData(2:end,:);

%% Need to change Date Format depends upon Table columns

Required_Data(:,1) = cellstr(datestr(datenum(Required_Data(:,1),'dd-mm-yyyy'),DB_System_Format));
Required_Data(:,2) = cellstr(datestr(datenum(Required_Data(:,2),'dd-mm-yyyy'),DB_System_Format));


%% Uploading into DB
tic;

TableName = strcat('unrealized_report_store_table_',char(InBUName));
set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',char(TableName),' where settle_date = ''',char(Current_ValueDate),''' '];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end

set(ObjDB,'AutoCommit','on');
upload_in_database(TableName, Required_Data);

cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName)]);

toc;

