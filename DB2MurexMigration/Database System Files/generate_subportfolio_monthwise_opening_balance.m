function OutFilename = generate_subportfolio_monthwise_opening_balance(InBUName)

ViewName = 'valuation_financial_year_view';
SQlQuery = 'select financial_year from valuation_financial_year_view';
[~,FinancialYear] = read_from_database(ViewName,0,SQlQuery);

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

% ViewName = 'helper_settle_p_and_g_view_cache_table';
% 
% [SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);
[~,~,RawData] = xlsread('helper_settle_p_and_g_view_cache_table_agf.csv');
SettleColNames = RawData(1,:);
SettleData = RawData(2:end,:);

PosProdCode = strcmpi('product_code',SettleColNames);
IdxOR = strncmpi(SettleData(:,PosProdCode),'OR',2);
SettleData(IdxOR,PosProdCode) = {'OR'};

SummmaryParameters = {'asset_class','subportfolio','product_code','counterparty_parent','group_type','contract_month'};
SumFields = {'realised-mtm-nc','unrealised-mtm-nc','realised-mtm-usd','unrealised-mtm-usd'};
OutputFields = {'asset_class','subportfolio','product_code','counterparty_parent','acc_group','group_type','contract_month','realised-mtm-nc','unrealised-mtm-nc','realised-mtm-usd','unrealised-mtm-usd'};
WeightedAverageFields = {};
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,SummmaryParameters,SumFields,OutputFields,WeightedAverageFields);                

FinancialYear = repmat(FinancialYear,size(OutData,1),1);

OutputFields = ['financial_year',OutputFields];
OutData = [FinancialYear,OutData];

try
OutData = sortrows(OutData,[1 2 3 4 5 6]);
catch
    disp('Error while sorting rows: some NaN values are found');
end

TableName = ['subportfolio_opening_balance_monthwise',InBUName];
OutFilename = getXLSFilename('subportfolio_opening_balance');
xlswrite(OutFilename,[OutputFields;OutData]);

end