function [OutErrorMsg ,OutXLSFileName,Final_OutHeader,Final_OutData,OutputHeader_PNL,Overall_OutData] = Calculate_Netting_Validation(InBUName,User_Passed_Counterparty,InXLSFilename)

%% Temp
% InBUName = 'qf5';
% User_Passed_Counterparty = 'MS-AUSTRALIA';
% InXLSFilename = 'Netting_Validation_Input.xlsx';
%
% InXLSFilename = 'Book2.xlsx';
% % InXLSFilename = 'Netting_Validation_25-Jun-2018_17-34-46';



try
    
    OutErrorMsg = {'No errors'};
    OutXLSFileName = '';
    Final_OutHeader = '';
    Final_OutData = '';
    OutputHeader_PNL='';
    Overall_OutData = '';
    
    %% Input Data
    
    [~,~,RawData] = xlsread(InXLSFilename);
    RowData = RawData(2:end,1:3);
    
    InputData_Header = {'Counterparty','security_id','lots_to_be_netted'}; %% Header Fixed for Dataset Purpose
    Input_Data = cell2dataset([InputData_Header ; RowData]);
    
    %% Counterparty Validation
    
    Matched_Counterparty_Index = strcmpi(Input_Data.Counterparty,User_Passed_Counterparty);
    if(isempty(find(Matched_Counterparty_Index,1)))
        OutErrorMsg = {['Input File does not contain counterparty ',char(User_Passed_Counterparty)]};
        return;
    end
    
    Input_Data = Input_Data(Matched_Counterparty_Index,:);
    
    Input_Data_SecurityID =  Input_Data.security_id;
    Input_Data_SecurityID_Query = ['''',strrep(convertCell2Char(Input_Data_SecurityID),',',''','''),''''];
    
    %% Fetch Data
    
    ObjDB = connect_to_database;
    
    % Bought Side
    SqlQuery = ['select security_id,sum(active_lots) as bgt_active_lots,sum(lots_to_be_netted) as bgt_lots_to_be_netted from helper_netting_screen_qf5 where lcase(market_action)=''bought'' and counterparty=''',char(User_Passed_Counterparty),...
        ''' and security_id in (',Input_Data_SecurityID_Query,') group by security_id'] ;
    [aa,bb] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(bb))
        %         OutErrorMsg = {['Not Getting Bought lots data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        OutErrorMsg = {'Bought Side trades are not available for given securities'};
        return;
    end
    if(strcmpi(bb,'No Data'))
        %         OutErrorMsg = {['Not Getting Bought lots data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        OutErrorMsg = {'Bought Side trades are not available for given securities'};
        
        return;
    end
    
    Bought_NettingScreen_Data = cell2dataset([aa;bb] );
    
    % Sold Side
    SqlQuery = ['select security_id,sum(active_lots) as bgt_active_lots,sum(lots_to_be_netted) as bgt_lots_to_be_netted from helper_netting_screen_qf5 where lcase(market_action)=''sold'' and counterparty=''',char(User_Passed_Counterparty),...
        ''' and security_id in (',Input_Data_SecurityID_Query,') group by security_id'] ;
    [aa,bb] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(bb))
        %         OutErrorMsg = {['Not Getting Sold lots data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        OutErrorMsg = {'Sold Side trades are not available for given securities'};
        
        return;
    end
    if(strcmpi(bb,'No Data'))
        %         OutErrorMsg = {['Not Getting Sold lots data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        OutErrorMsg = {'Sold Side trades are not available for given securities'};
        return;
    end
    
    Sold_NettingScreen_Data = cell2dataset([aa;bb] );
    
    %%
    Error_Index = 0;
    
    for i = 1 : size(Input_Data,1)
        Current_Security_ID = Input_Data.security_id(i) ;
        
        Bought_Matched_Index = find(strcmpi(Bought_NettingScreen_Data.security_id,Current_Security_ID),1);
        if(~isempty(Bought_Matched_Index))
            Input_Data.Bought_ActiveLots(i,1) = Bought_NettingScreen_Data.bgt_active_lots(Bought_Matched_Index);
            if(abs(Input_Data.lots_to_be_netted(i)) > abs(Input_Data.Bought_ActiveLots(i)))
                Input_Data.Error_Msg(i,1) = {['Given Lots is greater than Bought Active Lots for ' ,char(Current_Security_ID)]};
                Error_Index = 1;
            end
        else
            Input_Data.Error_Msg(i,1) = {'Sufficient Lots not available in Bought Side'};
            Error_Index = 1;
        end
        
        Sold_Matched_Index = find(strcmpi(Sold_NettingScreen_Data.security_id,Current_Security_ID),1);
        if(~isempty(Sold_Matched_Index))
            Input_Data.Sold_ActiveLots(i,1) = Sold_NettingScreen_Data.bgt_active_lots(Sold_Matched_Index);
            if(abs(Input_Data.lots_to_be_netted(i)) > abs(Input_Data.Sold_ActiveLots(i)) )
                Input_Data.Error_Msg(i,1) = {['Given Lots is greater than Sold Active Lots for ' ,char(Current_Security_ID)]};
                Error_Index = 1;
            end
        else
            Input_Data.Error_Msg(i,1) = {'Sufficient Lots not available in Sold Side'};
            Error_Index = 1;
        end
        
        
    end
    
    %%
    OutXLSFileName = getXLSFilename('Netting_Validation');
    if(Error_Index)
        OutErrorMsg = {'Error Found in given Excel File..!!!'};
        xlswrite(OutXLSFileName,dataset2cell(Input_Data));
        
        return;
    else
        %         xlswrite(OutXLSFileName,dataset2cell(Input_Data));
    end
    
    %% Get TradeID
    
    Tablename = 'helper_netting_screen_qf5'; %% security_id (if need add it query)
    SqlQuery = ['select security_id,market_action,trade_id,transaction_date,contract_month,contract_number,active_lots,premium,active_lots as buy_sell_lots from ',Tablename,...
        ' where security_id in (',Input_Data_SecurityID_Query,') and counterparty = ''',char(User_Passed_Counterparty),''' order by transaction_date,premium'];
    [Overall_DBFieldNames,Overall_DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(Overall_DBData))
        OutErrorMsg = {['Not Getting data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        return;
    end
    if(strcmpi(Overall_DBData,'No Data'))
        OutErrorMsg = {['Not Getting data from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        return;
    end
    
    PosSecurity_ID = strcmpi('security_id',Overall_DBFieldNames);
    
    %%%
    Overall_OutData = [];
    for i = 1 : size(Input_Data,1)
        
        Current_Security_ID = Input_Data.security_id(i) ;
        
        NumLots = Input_Data.lots_to_be_netted(i);
        
        DBFieldNames = Overall_DBFieldNames; %% For Looping Purpose
        DBData = Overall_DBData(strcmpi(Overall_DBData(:,PosSecurity_ID),Current_Security_ID),:);
        
        if(isempty(DBData))
            OutErrorMsg = {['No data available for the selected Filter! from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        end
        if(strcmpi(DBData,'No Data'))
            OutErrorMsg = {['No data available for the selected Filter! from helper_netting_screen_qf5 for ',char(User_Passed_Counterparty)]};
        end
        
        DBFieldNames(PosSecurity_ID) =[]; %% Remove Security ID for Multiple times coming
        DBData(:,PosSecurity_ID) = [];
        
        %%
        
        PosMarketAction = strcmpi('market_action',DBFieldNames);
        BoughtOrSold = DBData(:,PosMarketAction);
        DBData(:,PosMarketAction) = [];
        DBFieldNames(:,PosMarketAction) = [];
        IdxBought = strcmpi('bought',BoughtOrSold);
        IdxSold = strcmpi('sold',BoughtOrSold);
        
        ActualPosActiveLots = find(strcmpi('buy_sell_lots',DBFieldNames)); % active_lots
        PosActiveLots = ActualPosActiveLots; %% ActualPosActiveLots(2);
        
        InBuyGridData = DBData(IdxBought,:);
        LotsInputArray = InBuyGridData(:,PosActiveLots);
        LotsInputArray = cell2mat(LotsInputArray);
        [OutIndex, OutputArray] = extract_autonetting_lines(LotsInputArray,NumLots);
        if isempty(OutIndex)
            OutErrorMessage = {'No data available for the selected number of lots in Buy Side!'};
            return;
        end
        BuyGridData = InBuyGridData(OutIndex,:);
        BuyGridData(OutIndex,PosActiveLots) = num2cell(OutputArray);
        [BuyRows,BuyCols] = size(BuyGridData);
        
        InSellGridData = DBData(IdxSold,:);
        LotsInputArray = InSellGridData(:,PosActiveLots);
        LotsInputArray = abs(cell2mat(LotsInputArray));
        [OutIndex, OutputArray] = extract_autonetting_lines(LotsInputArray,NumLots);
        if isempty(OutIndex)
            OutErrorMessage = {'No data available for the selected number of lots in Sell Side!'};
            return;
        end
        OutputArray = OutputArray .* -1;
        SellGridData = InSellGridData(OutIndex,:);
        SellGridData(OutIndex,PosActiveLots) = num2cell(OutputArray);
        [SellRows,SellCols] = size(SellGridData);
        
        if BuyRows ~= SellRows
            if BuyRows > SellRows
                NumMissingLines = BuyRows - SellRows;
                EmptyData = cell(NumMissingLines,SellCols);
                %             EmptyData(:,:) = {[]};
                SellGridData = [SellGridData;EmptyData];
            else
                NumMissingLines = SellRows - BuyRows;
                EmptyData = cell(NumMissingLines,BuyCols);
                %              EmptyData(:,:) = {[]};
                BuyGridData = [BuyGridData;EmptyData];
            end
        end
        
        Index =num2cell([1:size(BuyGridData,1)]');
        DoNetting = zeros(size(BuyGridData,1),1);
        SecurityID_Array  =  cell(size(BuyGridData,1),1);
        SecurityID_Array(:,:) = Current_Security_ID;
        format long;
        OutData = [Index,SecurityID_Array,BuyGridData,num2cell(DoNetting),SellGridData,num2cell(DoNetting)];
        
        OutputHeader = ['Index','SecurityID',DBFieldNames,'BuyDoNetting',DBFieldNames,'SellDoNetting']; %% Existing
        OutputHeader(strcmpi(OutputHeader,'market_action')) = [];
        
        
        %% Calculate PNL
        
        TradeID_Index = cellStrfind_exact(DBFieldNames,{'trade_id'});
        Lots_Index  = cellStrfind_exact(DBFieldNames,{'buy_sell_lots'});
        
        Buy_DBFieldNames = DBFieldNames ;
        Sell_DBFieldNames = DBFieldNames ;
        Buy_DBFieldNames(TradeID_Index) = {'buy_trade_id'};
        Sell_DBFieldNames(TradeID_Index) = {'sell_trade_id'};
        Buy_DBFieldNames(Lots_Index) = {'buy_lots'};
        Sell_DBFieldNames(Lots_Index) = {'sell_lots'};
        
        OutputHeader_PNL = ['Index','SecurityID',Buy_DBFieldNames,'BuyDoNetting',Sell_DBFieldNames,'SellDoNetting'];
        BuyTradeID = OutData(:,cellStrfind_exact(OutputHeader_PNL,{'buy_trade_id'}));
        SellTradeID = OutData(:,cellStrfind_exact(OutputHeader_PNL,{'sell_trade_id'}));
        BuyLotsToBeNetted =  OutData(:,cellStrfind_exact(OutputHeader_PNL,{'buy_lots'}));
        SellLotsToBeNetted =  OutData(:,cellStrfind_exact(OutputHeader_PNL,{'sell_lots'}));
        UserName = 'system';
        
        BuyTradeID_Empty = cell2mat((cellfun(@(s) (isempty(s)),BuyTradeID,'UniformOutput',0)));
        BuyTradeID(BuyTradeID_Empty) = [];
        BuyLotsToBeNetted(BuyTradeID_Empty) = [];
        SellTradeID_Empty = cell2mat((cellfun(@(s) (isempty(s)),SellTradeID,'UniformOutput',0)));
        SellTradeID(SellTradeID_Empty) = [];
        SellLotsToBeNetted(SellTradeID_Empty) = [];
        
        [OutErrorMessage,NC_PnL,USD_PnL] = calculate_netting_pnl(InBUName,BuyTradeID,BuyLotsToBeNetted,SellTradeID,SellLotsToBeNetted,UserName);
        
        if(strcmpi(OutErrorMessage,'No errors'))
            Input_Data.NC_PnL(i) = str2double(NC_PnL);
            Input_Data.USD_PnL(i) = str2double(USD_PnL);
        end
        
        %% New Change
        if(~isempty(OutData))
            Lot_Numeric_Col = find(strcmpi(OutputHeader,'active_lots')) ;
            Lot_Numeric_Data = OutData(:,Lot_Numeric_Col);
            
            Exact_Value = cellfun(@(s) (sprintf('%.6f\n',s)), Lot_Numeric_Data,'UniformOutput',0);
            %         Exact_Value = cellfun(@str2num,Exact_Value,'un',0);
            OutData(:,Lot_Numeric_Col) = Exact_Value;
        end
        
        Overall_OutData = [Overall_OutData ; OutData];
        
        
    end
    
    %% Final OutData
    Final_Data_Array = dataset2cell(Input_Data);
    Final_Data_Header = Final_Data_Array(1,:);
    Final_Data_Array(1,:) = [];
    
    User_Want_Header = {'Counterparty','security_id','lots_to_be_netted','NC_PnL','USD_PnL'};
    User_Want_Col = cellStrfind_exact(Final_Data_Header,User_Want_Header);
    
    Final_OutHeader = User_Want_Header;
    Final_OutData = Final_Data_Array(:,User_Want_Col);
    
    %% Str 2 Double
    
    Lot_Numeric_Col = find(strcmpi(OutputHeader_PNL,'active_lots')) ;
    Lot_Numeric_Data = Overall_OutData(:,Lot_Numeric_Col);
    Exact_Value = cellfun(@(s) (str2double(s)), Lot_Numeric_Data,'UniformOutput',0);
    Overall_OutData(:,Lot_Numeric_Col) = Exact_Value;
    
    %% Excel Write
    
    xlswrite(OutXLSFileName,[Final_OutHeader;Final_OutData]);
    xlswrite(OutXLSFileName,[OutputHeader_PNL;Overall_OutData],'Complete Data');
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end