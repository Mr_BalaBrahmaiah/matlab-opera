[~,~,RawData ] = xlsread('subportfolio_open_balance.csv');
RawData = RawData(2:end,:);

AllPortfolio = RawData(:,2);
Subportfolio = unique(AllPortfolio);

FinancialYear = unique(RawData(:,1));

OutData = cell(length(Subportfolio),3);
OutData(:,1) = FinancialYear;

for iS = 1:length(Subportfolio)
    OutData(iS,2) = Subportfolio(iS);
    
    Idx = strcmpi(Subportfolio{iS}, AllPortfolio);
    OutData(iS,3) = num2cell(sum(cell2mat(RawData(Idx,3))));
    
end

xlswrite('FY2015-16-Open-Balance.xlsx',OutData);