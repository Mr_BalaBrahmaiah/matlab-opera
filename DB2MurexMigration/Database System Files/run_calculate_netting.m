% InBUName = 'agf';
% BuyTradeId = cell(3,1);
% BuyTradeId{1} = 'TR-FY16-17-160531';
% BuyTradeId{2} = 'TR-FY16-17-160297';
% BuyLotsToBeNetted = {25;25;0};
% SellTradeId= cell(2,1);
% SellTradeId{1} = 'TR-FY16-17-160493';
% SellTradeId{2} = 'TR-FY16-17-160259';
% SellLotsToBeNetted = {-25;-25};
% UserName = 'ananthi_it';

DataFromExcel = importdata('netting_page_table_cfs (1).xlsx'); 

InBUName = 'cfs';
BuyTradeId = DataFromExcel.textdata(2:end,2);
BuyLotsToBeNetted = DataFromExcel.data(:,8);
SellTradeId= DataFromExcel.textdata(2:end,10);
SellLotsToBeNetted = DataFromExcel.data(:,16);
UserName = 'ananthi_it';

[OutErrorMessage,NC_PnL,USD_PnL,Parfor_Errormsg] = calculate_netting(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName)

%[OutErrorMessage,PnL] = calculate_netting_pnl(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName);