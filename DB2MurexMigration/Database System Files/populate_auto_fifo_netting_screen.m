function [OutErrorMessage,OutData] = populate_auto_fifo_netting_screen(InBUName,GroupType,Subportfolio,ProductCode,Counterparty,ContractNo,DerivativeType,ContractMonth,SecurityID,NumLots,varargin)

Query = '';
OutErrorMessage = {'No Errors'};
OutData = [];

try
    
    ObjDB = connect_to_database;
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    if iscellstr(GroupType)
        GroupType = char(GroupType);
    end
    if iscellstr(Subportfolio)
        Subportfolio = char(Subportfolio);
    end
    if iscellstr(ProductCode)
        ProductCode = char(ProductCode);
    end
    if iscellstr(Counterparty)
        Counterparty = char(Counterparty);
    end
    %     if ~strcmpi(InBUName,'cfs')
    %         if iscellstr(ContractNo)
    %             ContractNo = char(ContractNo);
    %         end
    %     end
    if iscellstr(DerivativeType)
        DerivativeType = char(DerivativeType);
    end
    if iscellstr(ContractMonth)
        ContractMonth = char(ContractMonth);
    end
    if iscellstr(SecurityID)
        SecurityID = char(SecurityID);
    end
    
    if ~isempty(GroupType)
        if isempty(Query)
            Query = ['group_type = ''',GroupType,''''];
        else
            Query = [Query,' and group_type = ''',GroupType,''''];
        end
        disp('GroupType:'); disp(GroupType);
        
    end
    
    if ~isempty(Subportfolio)
        if isempty(Query)
            Query = ['subportfolio = ''',Subportfolio,''''];
        else
            Query = [Query,' and subportfolio = ''',Subportfolio,''''];
        end
        disp('Subportfolio:'); disp(Subportfolio);
    end
    
    if ~isempty(ProductCode)
        if isempty(Query)
            Query = ['product_code = ''',ProductCode,''''];
        else
            Query = [Query,' and product_code = ''',ProductCode,''''];
        end
        disp('ProductCode:'); disp(ProductCode);
    end
    
    if ~isempty(Counterparty)
        if isempty(Query)
            Query = ['counterparty = ''',Counterparty,''''];
        else
            Query = [Query,' and counterparty = ''',Counterparty,''''];
        end
        disp('Counterparty:'); disp(Counterparty);
    end
    
    if strcmpi(InBUName,'cfs')
        disp('ContractNo:'); disp(ContractNo);
        if iscolumn(ContractNo)
            ContractNo = ContractNo';
        end
        if ~isempty(cell2mat(ContractNo)) %~isempty(ContractNo)
            
            CNo= '';
            for iCNo = 1:length(ContractNo)
                if iCNo == length(ContractNo)
                    CNo = [CNo,'''',ContractNo{iCNo},''''];
                else
                    CNo = [CNo,'''',ContractNo{iCNo},''','];
                end
            end
            
            if ~isempty(CNo)
                if isempty(Query)
                    Query = ['contract_number in (',CNo,')'];
                else
                    Query = [Query,' and contract_number in (',CNo,')'];
                end
            end
        end
    end
    
    if ~isempty(DerivativeType)
        if isempty(Query)
            Query = ['derivative_type = ''',DerivativeType,''''];
        else
            Query = [Query,' and derivative_type = ''',DerivativeType,''''];
        end
        disp('DerivativeType:'); disp(DerivativeType);
    end
    
    if ~isempty(ContractMonth)
        if isempty(Query)
            Query = ['contract_month = ''',ContractMonth,''''];
        else
            Query = [Query,' and contract_month = ''',ContractMonth,''''];
        end
        disp('ContractMonth:'); disp(ContractMonth);
    end
    
    if ~isempty(SecurityID)
        if isempty(Query)
            Query = ['security_id = ''',SecurityID,''''];
        else
            Query = [Query,' and security_id = ''',SecurityID,''''];
        end
        disp('SecurityID:'); disp(SecurityID);
    end
    
    %     if strcmpi(InBUName,'qf5')
    %         if ~isempty(varargin)
    %             BuyTradeType = varargin{1};
    %             SellTradeType = varargin{2};
    %
    %             if isempty(Query)
    %                 Query = ['( equity_swaps_trade_type = ''',BuyTradeType,'''',' or equity_swaps_trade_type = ''',SellTradeType,''')'];
    %             else
    %                 Query = [Query,' and ( equity_swaps_trade_type = ''',BuyTradeType,'''',' or equity_swaps_trade_type = ''',SellTradeType,''')'];
    %             end
    %             disp('BuyTradeType:'); disp(BuyTradeType);
    %             disp('SellTradeType:'); disp(SellTradeType);
    %         else
    %             OutErrorMessage = {'Equity Swaps Trade Type is not selected!'};
    %             return;
    %         end
    %     end
    
    if ~isempty(Query)
        Tablename = ['helper_netting_screen_',InBUName];
        if strcmpi(InBUName,'cfs')
            SqlQuery = ['select market_action,trade_id,transaction_date,contract_month,contract_number,active_lots,premium,active_lots from ',Tablename,' where ',Query,' order by transaction_date,premium'];
        else
            SqlQuery = ['select market_action,trade_id,transaction_date,contract_month,active_lots,premium,active_lots from ',Tablename,' where ',Query,' order by transaction_date,premium'];
        end
        disp('SqlQuery:'); disp(SqlQuery);
        [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        if isempty(DBData)
            OutErrorMessage = {'No data available for the selected Filter!'};
            return;
        end
        
        PosMarketAction = strcmpi('market_action',DBFieldNames);
        BoughtOrSold = DBData(:,PosMarketAction);
        DBData(:,PosMarketAction) = [];
        DBFieldNames(:,PosMarketAction) = [];
        IdxBought = strcmpi('bought',BoughtOrSold);
        IdxSold = strcmpi('sold',BoughtOrSold);
        
        ActualPosActiveLots = find(strcmpi('active_lots',DBFieldNames));
        PosActiveLots = ActualPosActiveLots(2);
        
        InBuyGridData = DBData(IdxBought,:);
        LotsInputArray = InBuyGridData(:,PosActiveLots);
        LotsInputArray = cell2mat(LotsInputArray);
        [OutIndex, OutputArray] = extract_autonetting_lines(LotsInputArray,NumLots);
        if isempty(OutIndex)
            OutErrorMessage = {'No data available for the selected number of lots in Buy Side!'};
            return;
        end
        BuyGridData = InBuyGridData(OutIndex,:);
        BuyGridData(OutIndex,PosActiveLots) = num2cell(OutputArray);
        [BuyRows,BuyCols] = size(BuyGridData);
        
        InSellGridData = DBData(IdxSold,:);
        LotsInputArray = InSellGridData(:,PosActiveLots);
        LotsInputArray = abs(cell2mat(LotsInputArray));
        [OutIndex, OutputArray] = extract_autonetting_lines(LotsInputArray,NumLots);
        if isempty(OutIndex)
            OutErrorMessage = {'No data available for the selected number of lots in Sell Side!'};
            return;
        end
        OutputArray = OutputArray .* -1;
        SellGridData = InSellGridData(OutIndex,:);
        SellGridData(OutIndex,PosActiveLots) = num2cell(OutputArray);
        [SellRows,SellCols] = size(SellGridData);
        
        if BuyRows ~= SellRows
            if BuyRows > SellRows
                NumMissingLines = BuyRows - SellRows;
                EmptyData = cell(NumMissingLines,SellCols);
                %             EmptyData(:,:) = {[]};
                SellGridData = [SellGridData;EmptyData];
            else
                NumMissingLines = SellRows - BuyRows;
                EmptyData = cell(NumMissingLines,BuyCols);
                %              EmptyData(:,:) = {[]};
                BuyGridData = [BuyGridData;EmptyData];
            end
        end
        
        Index =num2cell([1:size(BuyGridData,1)]');
        DoNetting = zeros(size(BuyGridData,1),1);
        format long;
        OutData = [Index,BuyGridData,num2cell(DoNetting),SellGridData,num2cell(DoNetting)];
        
        OutputHeader = ['Index',DBFieldNames,'BuyDoNetting',DBFieldNames,'SellDoNetting'];
        OutputHeader(strcmpi(OutputHeader,'market_action')) = [];
        
        %% New Change
        if(~isempty(OutData))
            Lot_Numeric_Col = find(strcmpi(OutputHeader,'active_lots')) ;
            Lot_Numeric_Data = OutData(:,Lot_Numeric_Col);
            
            Exact_Value = cellfun(@(s) (sprintf('%.8f\n',s)), Lot_Numeric_Data,'UniformOutput',0);
            %         Exact_Value = cellfun(@str2num,Exact_Value,'un',0);
            OutData(:,Lot_Numeric_Col) = Exact_Value;
        end
        
    else
        OutErrorMessage = {'No values selected from Filter!'};
    end
catch ME
    OutErrorMessage = {ME.message};
end