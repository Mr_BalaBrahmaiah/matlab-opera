HolDates = {'2016-01-01';'2016-12-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2017-02-15'), datenum('2017-02-17'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1),'yyyy-mm-dd'));

objDB = connect_to_database;

for i = 1:length(VBusDays)
    
    SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end

    calculate_price_change_data;
    calculate_vol_change_data;
    
    disp(VDates{i});
end