function [OutNumber1,OutNumber2] = test_func_num_2In_2Out(InNumber1,InNumber2)

OutNumber1 = InNumber1 + InNumber2;
OutNumber2 = InNumber1 - InNumber2;