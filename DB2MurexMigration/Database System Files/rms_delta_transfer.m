function [OutErrorMsg,OutFileName] = rms_delta_transfer(InBUName)
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:53 $
%  $Revision: 1.8 $
%
OutFileName = '';
OutErrorMsg = {'No errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'rms_delta_transfer_surface_table';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    if ~iscell(Data) && (isequal(Data,0) || ...
            strcmpi(Data,'No Data'))
        OutErrorMsg = {'No data found in rms_delta_transfer_surface_table!'};
        return;
    end
    RefDTData = cell2dataset([ColNames;Data]);
    
    ViewName = 'helper_rms_delta_transfer_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    if ~iscell(Data) && (isequal(Data,0) || ...
            strcmpi(Data,'No Data'))
        OutErrorMsg = {'No data found for rms delta transfer!'};
        return;
    end
    
    PosSubportfolio = find(strcmpi('subportfolio',ColNames));
    % consider only RMS portfolios
    IdxRMS = strncmpi('RMS',Data(:,PosSubportfolio),length('RMS')); %#ok<*FNDSB>
    Data(~IdxRMS,:) = [];
    
    IdxRubber = strncmpi('RMS-OR',Data(:,PosSubportfolio),length('RMS-OR')); %#ok<*FNDSB>
    Data(IdxRubber,:) = [];
    
    IdxFx = strncmpi('RMS-USDBRL',Data(:,PosSubportfolio),length('RMS-USDBRL')); %#ok<*FNDSB>
    Data(IdxFx,:) = [];
    
    IdxRMSDir = cellStrfind(Data(:,PosSubportfolio),'OID'); %#ok<*FNDSB>
    Data(IdxRMSDir,:) = [];
    
    PosValueDate = find(strcmpi('value_date',ColNames));
    PosMaturity  = find(strcmpi('maturity_date',ColNames));
    % remove the dead deals and consider only live deals
    ValueDate    = datenum(Data(:,PosValueDate),'yyyy-mm-dd');
    TempMaturity = Data(:,PosMaturity);
    TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today-1,'yyyy-mm-dd')); % happens only in fx_spot
    MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
    % IdxLive = ValueDate <= MaturityDate;
    IdxDead = ValueDate > MaturityDate;
    
    Data(IdxDead,:) = [];
    
    TradersData = cell2dataset([ColNames;Data]);
    clear Data;
    
    Subportfolio = unique(TradersData.subportfolio);
    
    PosPortfolio  = 1;
    PosMonth      = 2;
    PosFutureLots = 3;
    PosOptionLots = 4;
    PosTotalDelta = 5;
    PosFuturePrice = 6;
    PosFutureName = 7;
    PosInstrument = 8;
    
    OutFileName = getXLSFilename('RMS_Delta_Transfer');
    
    RowCntr = 1;
    
    
    for iS = 1:length(Subportfolio)
        IdxPortfolio = strcmpi(Subportfolio{iS},TradersData.subportfolio);
        
        ContractMonth = unique(upper(TradersData.contract_month(IdxPortfolio)));
        ContractMonth(cellfun(@isempty,ContractMonth)) = [];
        
        IdxRefDTPortfolio = strcmpi(Subportfolio{iS},RefDTData.subportfolio);
        DTSurface = RefDTData.dt_surface(IdxRefDTPortfolio);
        
        for iCM = 1:length(ContractMonth)
            IdxCM     = zeros(size(IdxPortfolio)); IdxCM     = logical(IdxCM);
            IdxFuture = zeros(size(IdxPortfolio)); IdxFuture = logical(IdxFuture);
            IdxCall   = zeros(size(IdxPortfolio)); IdxCall   = logical(IdxCall);
            IdxPut    = zeros(size(IdxPortfolio)); IdxPut    = logical(IdxPut);
            
            
            IdxCM(IdxPortfolio) = strcmpi(ContractMonth{iCM},TradersData.contract_month(IdxPortfolio));
            IdxFuture(IdxCM)    = strcmpi('future',TradersData.derivative_type(IdxCM));
            
            TempCallIdx      = strfind(TradersData.derivative_type(IdxCM),'call');
            TempPutIdx       = strfind(TradersData.derivative_type(IdxCM),'put');
            IdxCall(IdxCM)   = ~(cellfun(@isempty,TempCallIdx));
            IdxPut(IdxCM)    = ~(cellfun(@isempty,TempPutIdx));
            IdxOption        = IdxCall | IdxPut;
            
            OutData(RowCntr,PosPortfolio)  = Subportfolio(iS); %#ok<*AGROW>
            OutData(RowCntr,PosMonth)      = ContractMonth(iCM);
            
            if strcmpi(DTSurface,'accounting')
                FutureDelta = sum(TradersData.settle_delta_1(IdxFuture));
                OptionDelta = sum(TradersData.settle_delta_1(IdxOption));
            else
                FutureDelta = sum(TradersData.traders_delta_1(IdxFuture));
                OptionDelta = sum(TradersData.traders_delta_1(IdxOption));
            end
            
            OutData(RowCntr,PosFutureLots) = num2cell(FutureDelta);
            OutData(RowCntr,PosOptionLots) = num2cell(OptionDelta);
            OutData(RowCntr,PosTotalDelta) = num2cell(FutureDelta + OptionDelta);
            
            try
                FuturePrice = unique(TradersData.p1_settleprice(IdxFuture));
                if isempty(FuturePrice)
                    FuturePrice = unique(TradersData.p1_settleprice(IdxOption));
                end
                OutData(RowCntr,PosFuturePrice) = num2cell(FuturePrice);
            catch
                OutData(RowCntr,PosFuturePrice) = num2cell(NaN);
            end
            
            try
                FutureName = unique(upper(TradersData.p1_name(IdxFuture)));
                if isempty(FutureName)
                    FutureName = unique(upper(TradersData.p1_name(IdxOption)));
                end
                OutData(RowCntr,PosFutureName) =FutureName;
            catch
                OutData(RowCntr,PosFutureName) ={''};
            end
            
            try
                Instrument = unique(TradersData.instrument(IdxFuture));
                if isempty(Instrument)
                    Instrument = unique(TradersData.instrument(IdxOption));
                    if ~isempty(Instrument)
                        Instrument = strrep(Instrument,'-O','');
                    end
                end
                OutData(RowCntr,PosInstrument) =Instrument;
            catch
                OutData(RowCntr,PosInstrument) ={''};
            end
            RowCntr = RowCntr + 1;
        end
    end
    
    if ~isempty(OutData)
        Header = {'Subportfolio','Month','Future Lots','Option Lots','Total Delta','Future Price','P1_Name','Instrument'};
        %   customcsvwrite(OutFileName,OutData,Header);
        OutDTData = [Header;OutData];
        
        % generate the DT deals in DB format
        try
            % check the future ids, since the serial month lots calculation to
            % be based on the future months
            FutureIDs = unique(OutData(:,PosFutureName));
            for iD = 1:length(FutureIDs)
                IdxFut = find(strcmpi(FutureIDs{iD},OutData(:,PosFutureName)));
                if length(IdxFut) > 1
                    OutData(IdxFut(1),PosTotalDelta) = num2cell(sum(cell2mat(OutData(IdxFut,PosTotalDelta))));
                    OutData(IdxFut(2:end),:) = [];
                end
            end
            
            % check if the total lots > 0.5
            IdxTR = abs(cell2mat(OutData(:,PosTotalDelta)))>=0.5;
            if any(IdxTR)
                DealData = [];
                %             SqlQuery = 'select trade_id from cloned_deal_ticket_table where transaction_date = (select max(transaction_date) from cloned_deal_ticket_table) order by trade_id DESC Limit 1';
                %             [~,DBTradeId] = read_from_database('cloned_deal_ticket_table',0,SqlQuery);
                [OutErrorMsg,DBTradeId] = getLastTradeId(InBUName);
                %             TradeIdPrefix = 'TR-FY16-17-';
                ObjDB = connect_to_database;
                SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
                TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
                TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));
                TradeDate = TradersData.settlement_date(1);
                
                IdxValues = find(IdxTR);
                for i = 1:length(IdxValues)
                    TempTradeId = TempTradeId + 1;
                    TradeId = [TradeIdPrefix,num2str(TempTradeId)];
                    Subportfolio = OutData(IdxValues(i),PosPortfolio);
                    Counterparty = Subportfolio;
                    Subportfolio = strrep(Subportfolio,'RMS','MM');
                    Subportfolio = strrep(Subportfolio,'OIR','OIV');
                    SecurityID = OutData(IdxValues(i),PosFutureName);
                    LotsValue = round(cell2mat(OutData(IdxValues(i),PosTotalDelta)));
                    if LotsValue < 0
                        MarketAction = {'sold'};
                    else
                        MarketAction = {'bought'};
                    end
                    Lots = num2cell(LotsValue);
                    Premium = OutData(IdxValues(i),PosFuturePrice);
                    Instrument =  OutData(IdxValues(i),PosInstrument);
                    
                    %                 RowData = [Subportfolio,TradeId,TradeId,TradeDate,...
                    %                     SecurityID,MarketAction,Lots,Premium,Counterparty,...
                    %                     {'NULL'},{'NULL'},{'NULL'},Counterparty,...
                    %                     {'NULL'},{'NULL'},{'NULL'},{'NULL'},...
                    %                     0,{'NULL'},{'System Generated DT'},{'NULL'},...
                    %                     {'NULL'},{'NULL'},{'NULL'},{'NULL'},Instrument,...
                    %                     {'NULL'},{'NULL'},0,Lots];
                    RowData = [TradeId,TradeId,TradeDate,Subportfolio,Instrument,...
                        'FUT',SecurityID,MarketAction,Lots,Premium,Counterparty,...
                        {'NULL'},{'NULL'},{'NULL'},Counterparty,...
                        {'NULL'},{'NULL'},{'NULL'},{'NULL'},{'NULL'},{'NULL'},...
                        0,Lots,{'NULL'},{'NULL'},{'NULL'},{'LIVE'},{'NULL'},...
                        {'NULL'},{'NULL'},{'NULL'},0,{'NULL'},{'NULL'},...
                        {'System Generated DT'},{'NULL'},{'NULL'},{'NULL'}];
                    
                    % generate the duplicate entries for internal deals
                    % interchange the counterparty and subportfolio
                    % change the market action inversely and change the sign of
                    % lots accordingly
                    % Add "DUP-" to the trade id
                    TradeId = ['DUP-',TradeId];
                    if strcmpi(MarketAction,'bought')
                        DupMarketAction = {'sold'};
                    else
                        DupMarketAction = {'bought'};
                    end
                    LotsValue = -(LotsValue);
                    Lots = num2cell(LotsValue);
                    %                 DupRowData = [Counterparty,TradeId,TradeId,TradeDate,...
                    %                     SecurityID,DupMarketAction,Lots,Premium,Subportfolio,...
                    %                     {'NULL'},{'NULL'},{'NULL'},Counterparty,...
                    %                     {'NULL'},{'NULL'},{'NULL'},{'NULL'},...
                    %                     0,{'NULL'},{'System Generated DT'},{'NULL'},...
                    %                     {'NULL'},{'NULL'},{'NULL'},{'NULL'},Instrument,...
                    %                     {'NULL'},{'NULL'},0,Lots];
                    DupRowData = [TradeId,TradeId,TradeDate,Counterparty,Instrument,...
                        'FUT',SecurityID,DupMarketAction,Lots,Premium,Subportfolio,...
                        {'NULL'},{'NULL'},{'NULL'},Subportfolio,...
                        {'NULL'},{'NULL'},{'NULL'},{'NULL'},{'NULL'},{'NULL'},...
                        0,Lots,{'NULL'},{'NULL'},{'NULL'},{'LIVE'},{'NULL'},...
                        {'NULL'},{'NULL'},{'NULL'},0,{'NULL'},{'NULL'},...
                        {'System Generated DT'},{'NULL'},{'NULL'},{'NULL'}];
                    
                    DealData = [DealData;RowData;DupRowData]; %
                end
                
                %             DBHeader = {'SubPortfolioID','ParentTradeID','TradeID','TransactionDate',...
                %                 'SecurityID','MarketAction','original_lots','Premium','CounterParty_Parent',...
                %                 'CounterParty_BU','CounterParty_Entity','ExecutionBroker','ClearingBroker',...
                %                 'ReconedWithClearer','ReconedCompletedBy','ReconDateTimeStamp','contract number',...
                %                 'version number','version created by','version comments','version timestamp',...
                %                 'exe_type','brokerage_rate_type','spcl_rate','other_remarks','instrument',...
                %                 'opt_periodicity','subcontract_number','is_netting_done','lots_to_be_netted'};
                DBHeader = {'trade_id','parent_trade_id','transaction_date','subportfolio','instrument',...
                    'trade_group_type','security_id','market_action','original_lots','premium',...
                    'counterparty_parent','counterparty_bu','counterparty_entity','execution_broker',...
                    'clearing_broker','reconed_with_clearer','reconed_completed_by','recon_datetimestamp',...
                    'contract_number','subcontract_number','opt_periodicity','is_netting_done','lots_to_be_netted',...
                    'date_to_be_fixed','trader_name','traded_timestamp','deal_status','exe_type','brokerage_rate_type',...
                    'spcl_rate','other_remarks','version_no','version_created_by','version_timestamp','version_comments','ops_unique','broker_lots','ops_action'};
                xlswrite(OutFileName,[DBHeader;DealData],'DT - DBDeals');
            end
        catch ME
            disp('error while writing the Deal data');
        end
        xlswrite(OutFileName,OutDTData,'Delta Transfer');
        
        OutXLSFileName = fullfile(pwd,OutFileName);
        xls_delete_sheets(OutXLSFileName);
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFileName = '';
end
end