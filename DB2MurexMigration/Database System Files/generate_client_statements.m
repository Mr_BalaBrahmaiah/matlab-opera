function [OutErrorMsg,OutXLSReport,OutPDFReport,OutMissingClients ] = generate_client_statements(InBUName,varargin)

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

LogFilename = getTimeStampedFilename('ML_CSGeneration_Log','.txt');

OutErrorMsg = {'No Errors'};
OutXLSName = getXLSFilename(['Client_Statements_',InBUName]);
OutMissingClients = {};
OutPDFReport = {};
OutXLSReport = {};

try
    [System_Date_Format,System_Date] = get_System_Date_Format();
    
    DB_Date_Format = 'yyyy-mm-dd';
    Client_Statement_Format = 'dd-mmm-yyyy';
    
    %% Fetch Data
    ObjDB = connect_to_database;
    %   RootFolder = get_reports_foldername;
    RootFolder= get_clientstatement_foldername;
    TempDestFolder = [RootFolder,'\reports-',char(InBUName),'\Client Statement Generation\All Client Statements'];
    
    SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    DBValueDate = datestr(datenum(ValueDate,'yyyy-mm-dd'),'dd-mmm-yyyy');
    
    %%  Defult_ViewName = 'helper_client_statement_report_view_';
    Defult_ViewName = 'helper_client_statement_report_view_cache_table_';
    ViewName = strcat(Defult_ViewName,char(InBUName));
    
    [SettleColNames,Data] = Fetch_DB_Data(ObjDB,[],ViewName);
    if(isempty(Data))
        OutErrorMsg = {'Not Getting Counterparty Data from DB'};
        return;
    end
    
    if(strcmpi(Data,'No Data'))
        OutErrorMsg = {'Not Getting Counterparty Data from DB'};
        return;
    end
    
    ClientStatement_View_Data = cell2dataset([SettleColNames;Data]);
    %% Query for Fixing date in OTC FX OPTION SECTION
    %% FX Option value date fix table (Raghav fix dated 13-08-2019)
    Mapping_table='fx_option_fix_value_date_table_';
    Table_Name=strcat( Mapping_table,char(InBUName));
    sql_query_mapping=['select * from ' ,Table_Name];
    [SettleColNames_map,Data_map] = Fetch_DB_Data(ObjDB, sql_query_mapping,[]);
    Map_data_fx_options=cell2dataset([SettleColNames_map;Data_map]);
    clear SettleColNames_map;
    clear Data_map
    %% Exception handling mechanism.
    Table_Name='composwap_mapping_table';
    %Sqlquery=['select * from ', Table_Name];
    [Headers_composwap,Data_composwap]=  Fetch_DB_Data(ObjDB,[],Table_Name);
    Conversion_Data=cell2dataset([Headers_composwap;Data_composwap]);
    
    %% Currency forawrd
    table_ptax=' currency_forward_mapping_table';
    a_ptax=['select * from', table_ptax];
    [colnames_ptax,data_ptax]=Fetch_DB_Data( ObjDB,a_ptax,[]);
    Mapping_table_data=[colnames_ptax;data_ptax];
    Mapping_table_dataset=cell2dataset( Mapping_table_data);
    
    
    
    %% WELLS FARGO PART
    try
        field_names=strtok(ClientStatement_View_Data.contract_number,'_');
        ClientStatement_View_Data.new_field=strrep(field_names,'WELLS FARGO RMS','');
    catch
        for jj=1:length(ClientStatement_View_Data)
            if (~isempty(ClientStatement_View_Data.contract_number(jj)))
                ClientStatement_View_Data.new_field(jj)=strtok(ClientStatement_View_Data.contract_number(jj),'_');
                if (strcmpi(ClientStatement_View_Data.contract_number(jj),'WELLS FARGO RMS'))
                    ClientStatement_View_Data.new_field(jj)=cellstr('-'); %% Here deleteion is not permitable hence we have to replace it by a char than after index cal replace it.
                end
            end
        end
    end
    
    %% Underscore removal in Dataset
    zz= ClientStatement_View_Data.new_field;
    index_under=find(strcmpi( zz,'-'));
    zz(index_under)=cellstr(' ');
    ClientStatement_View_Data.new_field=cellstr(zz);
    
    %% WE are no where using this table.
    %     BookingOrder_DerivativeType_Table = strcat('booking_order_derivative_type_mapping_table_',char(InBUName));
    %     [BookingOrder_DerivativeType_ColNames,BookingOrder_DerivativeType_Data] = Fetch_DB_Data(ObjDB,[],BookingOrder_DerivativeType_Table);
    %     BookingOrder_DerivativeType_Table = cell2dataset([BookingOrder_DerivativeType_ColNames;BookingOrder_DerivativeType_Data]);
    
    
    ClientInformation_Table = strcat('client_information_table_',char(InBUName));
    [ClientInformation_Table_ColNames,ClientInformation_Table_Data] = Fetch_DB_Data(ObjDB,[],ClientInformation_Table);
    ClientInformation_Table = cell2dataset([ClientInformation_Table_ColNames;ClientInformation_Table_Data]);
    
    Client_BookingOrder_Info_Table = strcat('client_statement_booking_order_information_table_',char(InBUName));
    [Client_BookingOrder_Info_Table_ColNames,Client_BookingOrder_Info_Table_Data] = Fetch_DB_Data(ObjDB,[],Client_BookingOrder_Info_Table);
    Client_BookingOrder_Info_Table = cell2dataset([Client_BookingOrder_Info_Table_ColNames;Client_BookingOrder_Info_Table_Data]);
    
    %% Dripping Table fix.
    Table_Name='client_dripping_table_';
    
    Table_Name_with_BU=strcat(Table_Name,InBUName);
    SqlQuery=['select * from ' Table_Name_with_BU    ' where value_date =(select value_date from valuation_date_table)'];
    [Header,Data] = Fetch_DB_Data(ObjDB, SqlQuery, []);
    
    if strcmpi(Data,'No Data')
        dripping_dataset=[];
    elseif  isempty(Data)
        dripping_dataset=[];
    else
        dripping_dataset=cell2dataset([Header;Data]);
    end
    
    %%% fetching data for currency factor
    sql_query = ['select currency,underlying_id,settle_value,quantotype from underlying_settle_value_table '...
        'left outer join currency_spot_mapping_table on (underlying_settle_value_table.underlying_id = currency_spot_mapping_table.spot)'...
        'where underlying_id like' '''%spot%''' 'and value_date =''',char(ValueDate),''''];
    
    [currencyHeader,currencyData] = Fetch_DB_Data(ObjDB, sql_query, []);
    if strcmpi(currencyData,'No Data')
        currency_dataset=[];
    elseif isempty(currencyData)
        currency_dataset=[];
    else
        currency_dataset = cell2dataset([currencyHeader;currencyData]);
    end
    
    clear currencyHeader;
    clear currencyData;
    
    %% Headers as per the sample report statis section.
    OTC_Swaps_Header = {'TRADE_DATE','TRADE_ID','EXPIRY_DATE','REF_MONTH',' ','LONG_LOTS','AVG_LONG_PRICE','SHORT_LOTS','AVG_SHORT_PRICE','EDSP',...
        ' ',' ',' ',' ',' ',' ',' ',' ',' ','MTM_NC','MTM_USD'};
    
    OTC_Option_Header = {'TRADE_DATE','TRADE_ID','EXPIRY_DATE','FUTURES_REF','B/S','CALL/PUT','LOTS','STRIKE','BARRIER_TYPE','BARRIER_STRIKE'...
        ,'EDSP',' ',' ','TRADE_PRICE',' ',' ','PREMIUM_USD',' ','MTM_NC','MTM_USD','DR_CR_USD'};
    
    OTC_Accumulators_Header = {'TRADE_DATE','TRADE_ID','EXPIRY_DATE','FUTURES_REF','STRUCTURE_NAME','B/S','LOTS_PER_DAY','NOTIONAL_QTY_LOTS',...
        'ACC_LEVEL_1','ACC_LEVEL_2/3','EDSP','BARRIER_TYPE','BARRIER_STRIKE','ACC_LEFT_LOTS','ACC_LEFT_DAYS',' ',' ',...
        'PREMIUM_USD','MTM_NC','MTM_USD','DR_CR_USD'};
    
    OTC_FXSwaps_Header = {'TRADE_DATE','TRADE_ID','VALUE_DATE','FX_PAIR','FIX_DATE','B/S','BUY_NOMINAL','BUY_PRICE',...
        'SELL_NOMINAL','SELL_PRICE','SETT_PRICE',' ',' ',' ',' ',' ',' ',' '...
        ,' ','MTM_NC','MTM_USD'};
    OTC_FX_OPTIONS_Header={'TRADE_DATE','TRADE_ID','FIX_DATE','VALUE_DATE','B/S','NOTIONAL (USD)','CALL/PUT','STRIKE','FX PAIR','BARRIER_STRIKE','BARRIER TYPE ','PTAX ASK'...
        ' ',' ','TRADED_PRICE (%)',' ',' ',' ','PREMIUM_USD','MTM_USD','(DR.)/CR.USD'};
    
    KC_Compo_Header={'TRADE_DATE','TRADE_ID','EXPIRY_DATE','REF_MONTH','','','LONG(LOTS)','AVG LONG PRICE RS/BAG','SHORT(LOTS)','AVG SHORT PRICE RS/BAG','KC EDSP US c/lb','KC EDSP BRL c/lb','KC EDSP RS/BAG','USDBRL EDSP','','','','','','','MTM_USD'};
    
    
    %% Empty cell creation and cell indexes
    Fixed_Header_Col_Size = size(OTC_Swaps_Header,2); %% 21
    
    Temp_OTC_Swaps_Header = cell(1,Fixed_Header_Col_Size);
    Temp_OTC_Swaps_Header(1,1) = {'OTC SWAPS'};
    Temp_OTC_Option_Header = cell(1,Fixed_Header_Col_Size);
    Temp_OTC_Option_Header(1,1) = {'OTC OPTIONS'};
    Temp_OTC_Accumulators_Header = cell(1,Fixed_Header_Col_Size);
    Temp_OTC_Accumulators_Header(1,1) = {'OTC ACCUMULATORS'};
    Temp_OTC_FXSwaps_Header = cell(1,Fixed_Header_Col_Size);
    Temp_OTC_FXSwaps_Header(1,1) = {'OTC FX SWAPS'};
    Temp_OTC_FXOptions_Header = cell(1,Fixed_Header_Col_Size);
    Temp_OTC_FXOptions_Header(1,1) = {'OTC FX OPTIONS'};
    Temp_KC_Compos_Header = cell(1,Fixed_Header_Col_Size);
    Temp_KC_Compos_Header(1,1)={'KC/USDBRL Compo SWAPS'};
    Temp_Header = cell(1,Fixed_Header_Col_Size);
    
    %% COnsolidate Level for OTC Swaps
    
    TradeDate_Col_Swaps = cellStrfind_exact(OTC_Swaps_Header,{'TRADE_DATE'});
    TRADE_ID_Col_Swaps = cellStrfind_exact(OTC_Swaps_Header,{'TRADE_ID'});
    RefMonth_Col_Swaps = cellStrfind_exact(OTC_Swaps_Header,{'REF_MONTH'});
    EXPIRY_DATE_Col_Swaps = cellStrfind_exact(OTC_Swaps_Header,{'EXPIRY_DATE'});
    LONG_LOTS_Col_Swaps = cellStrfind_exact(OTC_Swaps_Header,{'LONG_LOTS'});
    UniqueFields_Swaps = {'TRADE_ID','EXPIRY_DATE','AVG_LONG_PRICE','AVG_SHORT_PRICE','EDSP'};
    % SumFields_Swaps = {'LONG_LOTS','SHORT_LOTS','MTM_USD'};
    SumFields_Swaps = {'LONG_LOTS','SHORT_LOTS','MTM_NC','MTM_USD'};
    OutputFields_Swaps = OTC_Swaps_Header;
    WeightedAverageFields_Swaps = [];
    
    %% COnsolidate Level for OTC Options
    TradeDate_Col_Options = cellStrfind_exact(OTC_Option_Header,{'TRADE_DATE'});
    TRADE_ID_Col_Options = cellStrfind_exact(OTC_Option_Header,{'TRADE_ID'});
    RefMonth_Col_Options = cellStrfind_exact(OTC_Option_Header,{'FUTURES_REF'}); % REF_MONTH
    EXPIRY_DATE_Col_Options = cellStrfind_exact(OTC_Option_Header,{'EXPIRY_DATE'});
    UniqueFields_Options = {'TRADE_ID','EXPIRY_DATE','B/S','CALL/PUT','LOTS','STRIKE','EDSP','TRADED_PRICE'};
    % SumFields_Options = {'PREMIUM_USD','MTM_USD','DR_CR_USD'};
    SumFields_Options = {'PREMIUM_USD','MTM_NC','MTM_USD','DR_CR_USD'};
    OutputFields_Options = OTC_Option_Header;
    WeightedAverageFields_Options = [];
    
    %% Consolidate Level for OTC Accumulators
    TradeDate_Col_Accumulator = cellStrfind_exact(OTC_Accumulators_Header,{'TRADE_DATE'});
    TRADE_ID_Col_Accumulator = cellStrfind_exact(OTC_Accumulators_Header,{'TRADE_ID'});
    RefMonth_Col_Accumulator = cellStrfind_exact(OTC_Accumulators_Header,{'FUTURES_REF'});
    EXPIRY_DATE_Col_Accumulator = cellStrfind_exact(OTC_Accumulators_Header,{'EXPIRY_DATE'});
    UniqueFields_Accumulator = {'TRADE_DATE','TRADE_ID','FUTURES_REF','STRUCTURE_NAME','B/S','LOTS_PER_DAY','ACC_LEVEL_1','ACC_LEVEL_2_3','EDSP',...
        'BARRIER_TYPE','BARRIER_STRIKE'};
    SumFields_Accumulator = {'PREMIUM_USD','MTM_NC','MTM_USD','DR_CR_USD'};
    OutputFields_Accumulator = OTC_Accumulators_Header;
    WeightedAverageFields_Accumulator = [];
    
    %% Dated 18-02-2019/ addition / New Requirement
    TradeDate_Col_FX_OPTIONS = cellStrfind_exact(OTC_FX_OPTIONS_Header,{'TRADE_DATE'});
    TRADE_ID_Col_FX_OPTIONS = cellStrfind_exact(OTC_FX_OPTIONS_Header,{'TRADE_ID'});
    
    %% Dated 26-02-2019/ addition
    
    TradeDate_Col_KC_Compos = cellStrfind_exact(KC_Compo_Header ,{'TRADE_DATE'});
    TRADE_ID_Col_KC_Compos = cellStrfind_exact(KC_Compo_Header,{'TRADE_ID'});
    
    %% Logic in order to fetch Data.
    Overall_Client_Data = [];
    
    if isempty(varargin)
        Unique_ClientNames = unique(ClientStatement_View_Data.new_field); %unique(ClientStatement_View_Data.counterparty_parent);
        Unique_ClientNames = Unique_ClientNames(~cellfun('isempty',Unique_ClientNames)); %To remove the empty client codes
    else
        if(ischar(varargin{1}))
            Unique_ClientNames = varargin(1);
        else
            Unique_ClientNames = varargin{1};
        end
    end
    
    Overall_Current_Client_Data = [];
    
    for i = 1 : length(Unique_ClientNames)
        try
            Current_ClientName = Unique_ClientNames{i};
            
            %         Current_ClientName_Data = ClientStatement_View_Data(strcmpi(ClientStatement_View_Data.counterparty_parent,Current_ClientName),:);
            Current_ClientName_Data = ClientStatement_View_Data(strcmpi(ClientStatement_View_Data.new_field,Current_ClientName),:);
            %             Current_ClientName_Data = ClientStatement_View_Data(strcmpi(ClientStatement_View_Data.new_field,Current_ClientName),:);
            if(isempty(Current_ClientName_Data))
                continue;
            end
            
            Current_ClientCode = unique(Current_ClientName_Data.new_field);
            %         Current_ClientCode = ['FTD Confirmataion ',char((Current_ClientCode))];
            Current_ClientCode = char(Current_ClientCode);
            
            %% CALL PUT LOGIC.
            
            %% Structure details taaake from structure detail table
            for mm=1:length(Current_ClientName_Data.contract_number)
                %                for nn=length(Client_BookingOrder_Info_Table.contract_number)
                idx_match_contract=strcmpi(Current_ClientName_Data.contract_number(mm),Client_BookingOrder_Info_Table.contract_number);
                if ~isempty(find(idx_match_contract, 1))
                    Current_ClientName_Data.structure(mm,1) = unique(Client_BookingOrder_Info_Table.structure_name( idx_match_contract));
                    %                end
                end
            end
            
            exist_str = find(strcmpi('structure',get(Current_ClientName_Data,'VarNames')));
            if isempty(exist_str)
                Current_ClientName_Data.structure(size(Current_ClientName_Data,1),1)={''};
            end
            
            %% Dataset2cell conversion for strcmpi in order to handling error.
            cell_Current_ClientName_Data=dataset2cell(Current_ClientName_Data);
            %% Error handling if structure details are not available.
            %% If structure details are no there than function will not work..matrix dimensions mismatch.
            
            idx_structure_exist=find(strcmpi(cell_Current_ClientName_Data(1,:),'structure'));
            %% User waring messsage when strucut detail is empty
            if isempty(idx_structure_exist)
                OutErrorMsg =strcat('No Structure details in client booking order table for client Name_', char(upper(Unique_ClientNames(i))));
                %                 OutErrorMsg={'No Structure details in client booking order table for client',char( Unique_ClientNames(i))};
                continue;
            end
            
            %% Index calculation on the basis of various busines logic to segretate the data.
            OTC_Swaps_Data = Current_ClientName_Data(strcmpi(Current_ClientName_Data.derivative_type,'future'),:);
            OTC_Swaps_SIP_Data = Current_ClientName_Data(strcmpi(Current_ClientName_Data.derivative_type,'avg_swap_cc'),:);
            Vanilla_CallPut_index = strcmpi(Current_ClientName_Data.derivative_type,'vanilla_call') | strcmpi(Current_ClientName_Data.derivative_type,'vanilla_put') ;
            OTC_FX_OPTION_INDEX= strcmpi(Current_ClientName_Data.derivative_type,'fx_vanilla_call') | strcmpi(Current_ClientName_Data.derivative_type,'fx_vanilla_put');
            KC_COMPOS_INDEX= strcmpi(Current_ClientName_Data.derivative_type,'cs_future');
            Amer_Euro_Index =  (strncmpi(Current_ClientName_Data.derivative_type,'amer_',5) | strncmpi(Current_ClientName_Data.derivative_type,'euro_',5))  ;
            
            %% Data Sagregation logic suggested by ragahv.
            OTC_Option_Index = (Vanilla_CallPut_index | Amer_Euro_Index) & strcmpi(Current_ClientName_Data.opt_periodicity,'EOD') & (strcmpi(Current_ClientName_Data.structure,'CALL') | strcmpi(Current_ClientName_Data.structure,'PUT') | strcmpi(Current_ClientName_Data.structure,'0') | strcmpi(Current_ClientName_Data.structure,''));
            %% Operand and operator fix.
            [~,bred_opt]=size(OTC_Option_Index);
            if bred_opt>1
                first_logical_array=OTC_Option_Index(:,bred_opt-1);
                second_logical_array=OTC_Option_Index(:,bred_opt);
                OTC_Option_Index=(first_logical_array | second_logical_array);
            end
            
            not_logic=not(strcmpi(Current_ClientName_Data.structure,'CALL') | strcmpi(Current_ClientName_Data.structure,'PUT') | strcmpi(Current_ClientName_Data.structure,'0') | strcmpi(Current_ClientName_Data.structure,''));
            OTC_Accumulator_Index  = (Vanilla_CallPut_index | Amer_Euro_Index)  &   not_logic;
            %% Operand operator logical fix.
            [~,bred_acc]=size(OTC_Accumulator_Index);
            if bred_acc>1
                acc_first_logical_array=OTC_Accumulator_Index(:,bred_opt-1);
                acc_second_logical_array=OTC_Accumulator_Index(:,bred_opt);
                
                OTC_Accumulator_Index =(acc_first_logical_array | acc_second_logical_array);
            end
            
            %% Data fetching from dump as per the logic.
            OTC_Option_Data = Current_ClientName_Data(OTC_Option_Index,:);
            OTC_Accumulators_Data = Current_ClientName_Data(OTC_Accumulator_Index,:);
            OTC_FXSwaps_Data = Current_ClientName_Data(strcmpi(Current_ClientName_Data.derivative_type,'fx_forward_ndf'),:);
            OTC_FX_OPTIONS= Current_ClientName_Data(OTC_FX_OPTION_INDEX,:);
            KC_COMPOS_SWAPS=Current_ClientName_Data(KC_COMPOS_INDEX,:);
            
            %% OTC FX SWAPS SECTION FILLING.
            
            if(~isempty(OTC_FXSwaps_Data))
                
                OTC_FXSwaps_Data_Output = cell2dataset([OTC_FXSwaps_Header ; cell(size(OTC_FXSwaps_Data,1),size(OTC_FXSwaps_Header,2)) ]);
                
                for ii = 1 : size(OTC_FXSwaps_Data,1)
                    OTC_FXSwaps_Data_Output.TRADE_DATE(ii) = OTC_FXSwaps_Data.transaction_date(ii);
                    OTC_FXSwaps_Data_Output.TRADE_ID(ii) = OTC_FXSwaps_Data.contract_number(ii);
                    
                    
                    OTC_FXSwaps_Data_Output.VALUE_DATE(ii) =   OTC_FXSwaps_Data.maturity_date(ii);
                    OTC_FXSwaps_Data_Output.FX_PAIR(ii) = OTC_FXSwaps_Data.instrument(ii);
                    OTC_FXSwaps_Data_Output.FIX_DATE(ii) = OTC_FXSwaps_Data.fixing_date(ii);
                    
                    if(strcmpi(OTC_FXSwaps_Data.market_action(ii),'Sold')) %% if sold , we need to show opposite sign for FX Swaps
                        OTC_FXSwaps_Data_Output.B_S(ii) = {'B'};
                        OTC_FXSwaps_Data_Output.BUY_NOMINAL(ii) = num2cell(abs(OTC_FXSwaps_Data.active_lots(ii)));
                        OTC_FXSwaps_Data_Output.BUY_PRICE(ii) = num2cell(round(OTC_FXSwaps_Data.traded_price(ii),4));
                    else
                        OTC_FXSwaps_Data_Output.B_S(ii) = {'S'};
                        OTC_FXSwaps_Data_Output.SELL_NOMINAL(ii) = num2cell(abs(OTC_FXSwaps_Data.active_lots(ii)));
                        OTC_FXSwaps_Data_Output.SELL_PRICE(ii) = num2cell(round(OTC_FXSwaps_Data.traded_price(ii),4));
                    end
                    
                    
                    OTC_FXSwaps_Data_Output.SETT_PRICE(ii) = num2cell(round(OTC_FXSwaps_Data.settle_price(ii),4));
                    OTC_FXSwaps_Data_Output.MTM_NC(ii) = num2cell(round(OTC_FXSwaps_Data.mtm_nc(ii),2));
                    
                    % Getting Spot Rate
                    DistinctProdCode = ['''',char(strcat('CR',{' '},strrep(convertCell2Char(unique(OTC_FXSwaps_Data.product_code(ii))),',',''','''))),''''];
                    
                    SqlQuery = ['select currency from product_master_table where invenio_product_code in (',DistinctProdCode,')'];
                    [~,CurrencyType]   = Fetch_DB_Data(ObjDB,SqlQuery);
                    
                    SqlQuery = ['select spot,quantotype from currency_spot_mapping_table where currency = ''',char(CurrencyType),''' '];
                    [~,DBProdMultData]   = Fetch_DB_Data(ObjDB,SqlQuery);
                    Curr_MulFactor     = DBProdMultData(1);
                    Curr_MulType   = DBProdMultData(2);
                    
                    SqlQuery = ['select settle_value from underlying_settle_value_table where settlement_date = ''',char(SettleDate),...
                        ''' and underlying_id = ''',char(Curr_MulFactor),''''];
                    SpotRate = fetch(ObjDB, SqlQuery);
                    
                    if(strcmpi(Curr_MulType,'multiplication'))
                        OTC_FXSwaps_Data_Output.MTM_USD(ii) = num2cell( OTC_FXSwaps_Data.mtm_nc(ii) .* cell2mat(SpotRate) );
                    else
                        OTC_FXSwaps_Data_Output.MTM_USD(ii) = num2cell( round((OTC_FXSwaps_Data.mtm_nc(ii) ./ cell2mat(SpotRate)),2) );
                    end
                    
                    
                end
                
            end
            
            if exist('OTC_FXSwaps_Data_Output')
                if (~isempty(OTC_FXSwaps_Data_Output))
                    OTC_FXSwaps_Data_Output.TRADE_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                        OTC_FXSwaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                    OTC_FXSwaps_Data_Output.VALUE_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                        OTC_FXSwaps_Data_Output.VALUE_DATE,'UniformOutput',0);
                    
                    %% Sorting functionalty
                    Header =  OTC_FXSwaps_Data_Output.Properties.VarNames;
                    value_date = find(strcmpi(Header,'VALUE_DATE'));
                    
                    %% New Requirement by karthik
                    sell_nominal=find(strcmpi(Header,'SELL_NOMINAL'));
                    buy_nominal=find(strcmpi(Header,'BUY_NOMINAL'));
                    
                    bb_bb=OTC_FXSwaps_Data_Output.BUY_NOMINAL;
                    empty_cells= find(cellfun(@isempty,bb_bb));  %% empty status check kar
                    bb_bb(empty_cells)=num2cell(1); % empty cells ko 1 assign kia here cell and non cell status check kar.
                    OTC_FXSwaps_Data_Output.BUY_NOMINAL=bb_bb; % and values ko BUT_NOMINAL ko assign kar dia
                    
                    cc_cc=OTC_FXSwaps_Data_Output.SELL_NOMINAL;
                    tf=isa(cc_cc,'double');
                    
                    if tf==1
                        cc_cc=num2cell(cc_cc);
                        empty_cells= cellfun(@isempty,cc_cc);
                        cc_cc(empty_cells)=num2cell(1);
                        OTC_FXSwaps_Data_Output.SELL_NOMINAL=cc_cc;
                    else
                        empty_cells= find(cellfun(@isempty,cc_cc));
                        cc_cc(empty_cells)=num2cell(1);
                        OTC_FXSwaps_Data_Output.SELL_NOMINAL=cc_cc;
                    end
                    
                    
                    %% sell_nomi
                    OTC_FXSwaps_Data_Output = sortrows(OTC_FXSwaps_Data_Output,[value_date buy_nominal  sell_nominal ],{'ascend'});
                    
                    OTC_FXSwaps_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        OTC_FXSwaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                    OTC_FXSwaps_Data_Output.VALUE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        OTC_FXSwaps_Data_Output.VALUE_DATE,'UniformOutput',0);
                    
                    buy_nominal=OTC_FXSwaps_Data_Output.BUY_NOMINAL;
                    index_buy = find(cell2mat(cellfun(@(x) x==1, buy_nominal, 'UniformOutput', 0)));
                    buy_nominal(index_buy)={[]};
                    
                    sell_nominal=OTC_FXSwaps_Data_Output.SELL_NOMINAL;
                    index_cell = find(cell2mat(cellfun(@(x) x==1, sell_nominal, 'UniformOutput', 0)));
                    sell_nominal(index_cell)={[]};
                    
                    %% Assignment call
                    OTC_FXSwaps_Data_Output.BUY_NOMINAL=buy_nominal;
                    OTC_FXSwaps_Data_Output.SELL_NOMINAL=sell_nominal;
                end
            end
            
            %% OTC SWAPS SECTION DATA MANIPULATION
            
            if(~isempty(OTC_Swaps_Data))
                
                %% New general Dataset Crea
                OTC_Swaps_Data_Output = cell2dataset([OTC_Swaps_Header ; cell(size(OTC_Swaps_Data,1),size(OTC_Swaps_Header,2)) ]);
                % Trade_date=cell2dataset(Client_BookingOrder_Info_Table.trade_date(1:length(OTC_Swaps_Data_Output)));
                for ii = 1 : size(OTC_Swaps_Data,1)
                    
                    IdxMatch_1 = find(strcmpi(OTC_Swaps_Data.transaction_date(ii),Client_BookingOrder_Info_Table.trade_date));
                    %                    disp(OTC_Swaps_Data.transaction_date(ii)); %% TO check the logic of comparison
                    % disp(Client_BookingOrder_Info_Table.trade_date)
                    
                    if ~isempty(find(IdxMatch_1, 1))
                        OTC_Swaps_Data_Output.TRADE_DATE(ii) = unique(Client_BookingOrder_Info_Table.trade_date(IdxMatch_1));
                    else
                        OTC_Swaps_Data_Output.TRADE_DATE(ii) = OTC_Swaps_Data.transaction_date(ii);
                    end
                    
                    OTC_Swaps_Data_Output.TRADE_ID(ii) = OTC_Swaps_Data.contract_number(ii);
                    
                    data_check=char(OTC_Swaps_Data.contract_number(ii));
                    check_l_b=num2str(data_check);
                    l_b=length(check_l_b);
                    
                    if check_l_b(end)=='A' || check_l_b(end)=='B' || check_l_b(end)=='C'
                        zz=data_check(1:end-1);
                    else
                        zz=data_check(1:end);
                    end
                    data_used=cellstr(zz);
                    
                    IdxMatch = strcmpi(data_used,Client_BookingOrder_Info_Table.contract_number);
                    
                    if ~isempty(find(IdxMatch, 1))
                        OTC_Swaps_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.future_expiry_date(IdxMatch));
                    else
                        OTC_Swaps_Data_Output.EXPIRY_DATE(ii) = OTC_Swaps_Data.maturity_date(ii);
                    end
                    
                    SqlQuery_req=['select * from client_statement_booking_order_information_table_cfs where contract_number = ''' , char(data_used),''' '];
                    [SettleColNames_req,Data_req] = Fetch_DB_Data(ObjDB,  SqlQuery_req,[]);
                    % idx_check_Data_req=strcmpi(Data_req,'No Data');
                    % if idx_check_Data_req(1)==0
                    %                     idx_data_pre=strcmpi(Data_req,'No Data');
                    %                     if (idx_data_pre==0)
                    if strcmpi(Data_req,'No Data')
                        OutErrorMsg={'No Matching trade in client statement booking order table fot trade',char(data_used)};
                        continue;
                        %                          required_data_dataset=cell2dataset([SettleColNames_req;Data_req]);
                    else
                        
                        required_data_dataset=cell2dataset([SettleColNames_req;Data_req]);
                    end
                    
                    
                    %% Matching analysis
                    % if exist('required_data_dataset','var')
                    [part1,part2]=strtok(required_data_dataset.contract_month);
                    [trimmed_part2]=strtrim(part2);
                    char_trimmed_part=char(trimmed_part2);
                    first_part=char_trimmed_part(2:4);
                    second_part=char_trimmed_part(6:7);
                    schar='-';
                    
                    % OTC_Swaps_Data_Output.REF_MONTH(ii) = strcat(OTC_Swaps_Data.product_code(ii),{' '},datestr(OTC_Swaps_Data.future_expiry(ii),'mmm-yy'));
                    %                      OTC_Swaps_Data_Output.REF_MONTH(ii)=strcat(part1,{' '},upper(datestr(trimmed_part2,'mmm-yy')));
                    OTC_Swaps_Data_Output.REF_MONTH(ii)=strcat(part1,{' '},cellstr(first_part),cellstr(schar),second_part);
                    
                    if(OTC_Swaps_Data.active_lots(ii) >= 0)
                        OTC_Swaps_Data_Output.LONG_LOTS(ii) = num2cell( OTC_Swaps_Data.active_lots(ii) );
                        OTC_Swaps_Data_Output.AVG_LONG_PRICE(ii) = num2cell( round(OTC_Swaps_Data.traded_price(ii),6) );
                    else
                        OTC_Swaps_Data_Output.SHORT_LOTS(ii) = num2cell( OTC_Swaps_Data.active_lots(ii) );
                        OTC_Swaps_Data_Output.AVG_SHORT_PRICE(ii) = num2cell( round(OTC_Swaps_Data.traded_price(ii),6) );
                    end
                    
                    OTC_Swaps_Data_Output.EDSP(ii) = num2cell(OTC_Swaps_Data.future_price(ii)) ;
                    
                    if(strcmpi(OTC_Swaps_Data.currency(ii),'USD'))
                        OTC_Swaps_Data_Output.MTM_USD(ii) = num2cell( round(OTC_Swaps_Data.mtm_usd(ii),2) );
                    else
                        OTC_Swaps_Data_Output.MTM_NC(ii) = num2cell( round(OTC_Swaps_Data.mtm_nc(ii),2));
                        OTC_Swaps_Data_Output.MTM_USD(ii) = num2cell( round(OTC_Swaps_Data.mtm_usd(ii),2) );
                    end
                end
                
                OTC_Swaps_Data_Output_CellArray = dataset2cell(OTC_Swaps_Data_Output);
                
                OTC_Swaps_Data_Output_CellArray(1,:) = []; %% Remove Header
                Empty_TradeID_Index = find(cell2mat(cellfun(@isempty,OTC_Swaps_Data_Output_CellArray(:,TRADE_ID_Col_Swaps),'UniformOutput',false)));
                OTC_Swaps_Data_Output_CellArray(Empty_TradeID_Index,:) = [] ; %% Remove Empty Contract Number
                Empty_TradeID_Index = []; %% For Next Loop Iteration
                [Consolidate_Header,Consolidate_DBData] = consolidatedata(OTC_Swaps_Header, OTC_Swaps_Data_Output_CellArray,UniqueFields_Swaps,SumFields_Swaps,OutputFields_Swaps,WeightedAverageFields_Swaps);
                
                TradeDate_Empty_Index = find(cell2mat(cellfun(@(s) (length(s<1)), Consolidate_DBData(:,TradeDate_Col_Swaps),'UniformOutput',0))==1);
                GetTradeID = Consolidate_DBData(TradeDate_Empty_Index,TRADE_ID_Col_Swaps);
                
                for c = 1 : length(GetTradeID)
                    
                    Current_TradeID = GetTradeID(c);
                    
                    Current_TradeID_Index  = TradeDate_Empty_Index(c);
                    
                    Current_TradeID_ExpiryDate = OTC_Swaps_Data_Output.TRADE_DATE(strcmpi(OTC_Swaps_Data_Output.TRADE_ID,Current_TradeID));
                    Min_TradeDate = cellstr( datestr(min(datenum(Current_TradeID_ExpiryDate,DB_Date_Format)),DB_Date_Format) ) ;
                    
                    Consolidate_DBData(Current_TradeID_Index,TradeDate_Col_Swaps) = Min_TradeDate;
                    %Consolidate_DBData(Current_TradeID_Index,RefMonth_Col_Swaps) = {'future_expiry'};
                    
                end
                OTC_Swaps_Header = {'TRADE_DATE','TRADE_ID','EXPIRY_DATE','REF_MONTH',' ','LONG_LOTS','AVG_LONG_PRICE','SHORT_LOTS','AVG_SHORT_PRICE','EDSP',...
                    ' ',' ',' ',' ',' ',' ',' ',' ',' ','MTM_NC','MTM_USD'};
                OTC_Swaps_Data_Output = cell2dataset([OTC_Swaps_Header ; Consolidate_DBData]);
                
                %% Sorting based on Product code, Expiry Date, Short & Long lots, Trade Date
                % Convert the dates to number format before sorting
                OTC_Swaps_Data_Output.EXPIRY_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Swaps_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Swaps_Data_Output.TRADE_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Swaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                
                % Sort based on the product code, then by Expiry Date and
                % by Short Lots first
                TempProductCode      = strtok(OTC_Swaps_Data_Output.REF_MONTH,' ');
                OTC_Swaps_Data_Output.product_code = TempProductCode;
                
                Header = OTC_Swaps_Data_Output.Properties.VarNames;
                ProductCode_Col_Swaps = find(strcmpi(Header,'product_code'));
                OTC_Swaps_Data_Output = sortrows(OTC_Swaps_Data_Output,[ProductCode_Col_Swaps,EXPIRY_DATE_Col_Swaps,LONG_LOTS_Col_Swaps,TradeDate_Col_Swaps],{'ascend'});
                
                OTC_Swaps_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                    OTC_Swaps_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Swaps_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                    OTC_Swaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                OTC_Swaps_Data_Output.product_code = [];
                %                 Index_VIT_CC=find(strcmpi(OTC_Swaps_Data_Output.TRADE_ID,'VIT_CC_155'));
                
                %                 if (~isempty(Index_VIT_CC))
                %                     transaction_date=datetime(OTC_Swaps_Data_Output.TRADE_DATE(Index_VIT_CC));
                %                     required_transaction_date=(transaction_date-calmonths(1));
                %                     allocated_date = cellstr( datestr(required_transaction_date,DB_Date_Format ));
                %                     OTC_Swaps_Data_Output.TRADE_DATE(Index_VIT_CC)=allocated_date ;
                %                 end
                
            end
            
            %% Trade Date manipulation OTC_SWAPS
            %% Make SIPH format.
            
            if(~isempty(OTC_Swaps_SIP_Data))
                [OTC_Swaps_Header,OTC_Swaps_Data,OTC_Swaps_Data_Output] = Make_SIPH_Format(OTC_Swaps_SIP_Data);
            end
            
            %% OTC OPTIONS
            
            if(~isempty(OTC_Option_Data))
                %% Dripping Table reading.
                
                
                %% Trailing A checking In OTC OPTIONS
                bb=find(endsWith(OTC_Option_Data.contract_number,'A'));
                if (~isempty(bb))
                    test=cellfun(@(x) x(1:end-1), Client_BookingOrder_Info_Table.contract_number(bb),'UniformOutput',false);
                    Client_BookingOrder_Info_Table.contract_number(bb)=test;
                end
                %% Trailing B checking in OTC OPTIONS
                aa=find(endsWith(OTC_Option_Data.contract_number,'B'));
                if (~isempty(aa))
                    test=cellfun(@(x) x(1:end-1), Client_BookingOrder_Info_Table.contract_number(bb),'UniformOutput',false);
                    Client_BookingOrder_Info_Table.contract_number(aa)=test;
                end
                %% OTC OPTION CREATION
                
                OTC_Option_Data_Output = cell2dataset([OTC_Option_Header ; cell(size(OTC_Option_Data,1),size(OTC_Option_Header,2)) ]);
                
                for ii = 1 : size(OTC_Option_Data,1)
                    OTC_Option_Data_Output.TRADE_DATE(ii) = OTC_Option_Data.transaction_date(ii);
                    OTC_Option_Data_Output.TRADE_ID(ii) = OTC_Option_Data.contract_number(ii);
                    %                     IdxMatch = strcmpi(OTC_Swaps_Data.contract_number(ii),Client_BookingOrder_Info_Table.contract_number);
                    %                     if ~isempty(find(IdxMatch, 1))
                    %                           OTC_Option_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.future_expiry_date(IdxMatch));
                    %                     else
                    %                      OTC_Option_Data_Output .EXPIRY_DATE(ii) =  OTC_Option_Data.maturity_date(ii);
                    %                     end
                    IdxMatch = strcmpi(OTC_Option_Data.contract_number(ii),Client_BookingOrder_Info_Table.contract_number);
                    if ~isempty(find(IdxMatch, 1))
                        OTC_Option_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.option_expiry_date(IdxMatch));
                    else
                        OTC_Option_Data_Output.EXPIRY_DATE(ii) = OTC_Option_Data.maturity_date(ii);
                    end
                    %OTC_Option_Data_Output.EXPIRY_DATE(ii) = OTC_Option_Data.maturity_date(ii);
                    % OTC_Option_Data_Output.FUTURES_REF(ii) = strcat(OTC_Option_Data.product_code(ii),{' '},datestr(OTC_Option_Data.future_expiry(ii),'mmm-yy'));
                    [ fut_month, OutErrorMsg] = fut_refmonth(OTC_Option_Data.contract_month(ii));
                    OTC_Option_Data_Output.FUTURES_REF(ii) = strcat(OTC_Option_Data.product_code(ii),{' '},fut_month);
                    %% Contract number new logic implementation dated 25-04-2019
                    
                    
                    %% Is premium paid 1 Logic has been implemented.
                    if (OTC_Option_Data.is_premium_paid(ii)==1)
                        OTC_Option_Data.premium_paid_usd(ii)=0;
                    end
                    
                    if(OTC_Option_Data.active_lots(ii) >= 0)
                        OTC_Option_Data_Output.B_S(ii) = {'B'};
                    else
                        OTC_Option_Data_Output.B_S(ii) = {'S'};
                    end
                    
                    %                     if(strcmpi(OTC_Option_Data.derivative_type(ii),'vanilla_call'))
                    if cell2mat(strfind(OTC_Option_Data.derivative_type(ii),'call'))~=0
                        OTC_Option_Data_Output.CALL_PUT(ii) = {'CALL'};
                    else
                        OTC_Option_Data_Output.CALL_PUT(ii) = {'PUT'};
                    end
                    
                    OTC_Option_Data_Output.LOTS(ii) = num2cell( OTC_Option_Data.active_lots(ii) );
                    OTC_Option_Data_Output.STRIKE(ii) = num2cell( OTC_Option_Data.strike1(ii) );
                    %                     OTC_Option_Data_Output.BARRIER_TYPE(ii) = BookingOrder_DerivativeType_Table.client_statement_type(strcmpi( BookingOrder_DerivativeType_Table.derivative_type,OTC_Option_Data.derivative_type(ii) ));
                    OTC_Option_Data_Output.BARRIER_TYPE(ii) = Client_BookingOrder_Info_Table.barrier_type(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Option_Data.contract_number(ii) ));
                    %  Rajashekhar commented
                    %OTC_Option_Data_Output.BARRIER_STRIKE(ii) = num2cell( OTC_Option_Data.barrier1_strike(ii) );
                    % end of rajashekhar code
                    
                    if find(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Option_Data.contract_number(ii) ))
                        
                        OTC_Option_Data_Output.BARRIER_STRIKE(ii) = Client_BookingOrder_Info_Table.barrier_level(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Option_Data.contract_number(ii) ));
                    else
                        OTC_Option_Data_Output.BARRIER_STRIKE(ii) = num2cell( OTC_Option_Data.barrier1_strike(ii) );
                    end
                    
                    OTC_Option_Data_Output.EDSP(ii)  = num2cell( OTC_Option_Data.future_price(ii) );
                    OTC_Option_Data_Output.TRADE_PRICE(ii) = num2cell( OTC_Option_Data.traded_price(ii) );
                    OTC_Option_Data_Output.PREMIUM_USD(ii) = num2cell( OTC_Option_Data.premium_paid_usd (ii) );
                    
                    %%% dripping concept for usd
                    if(strcmpi(OTC_Option_Data.currency(ii),'USD'))
                        
                        OTC_Option_Data_Output.MTM_USD(ii) = num2cell( OTC_Option_Data.mkt_value_usd(ii) );
                        
                    else
                        
                        OTC_Option_Data_Output.MTM_NC(ii) = num2cell( OTC_Option_Data.mkt_value_nc(ii) );
                        OTC_Option_Data_Output.MTM_USD(ii) = num2cell( OTC_Option_Data.mkt_value_usd(ii) );
                    end
                   
                    OTC_Option_Data_Output.DR_CR_USD(ii) = num2cell( nansum(OTC_Option_Data.premium_paid_usd(ii) + OTC_Option_Data.mkt_value_usd(ii)) );
                    % OTC_Option_Data_Output.DR_CR_USD(ii)=num2cell(OTC_Option_Data.mtm_usd(ii)); %% New change for DR_CR_USD mismatch
                    
                end
                
                OTC_Option_Data_Output_CellArray = dataset2cell(OTC_Option_Data_Output);
                
                
                %% B addition for pdf output
                OTC_Option_Data_Output_CellArray(1,:) = []; %% Remove Header
                Empty_TradeID_Index = find(cell2mat(cellfun(@isempty,OTC_Option_Data_Output_CellArray(:,TRADE_ID_Col_Options),'UniformOutput',false)));
                OTC_Option_Data_Output_CellArray(Empty_TradeID_Index,:) = [] ; %% Remove Empty Contract Number
                Empty_TradeID_Index = []; %% For Next Loop Iteration
                
                [Consolidate_Header_Option,Consolidate_DBData_Option] = consolidatedata(OTC_Option_Header, OTC_Option_Data_Output_CellArray,UniqueFields_Options,SumFields_Options,OutputFields_Options,WeightedAverageFields_Options);
                
               
                
                TradeDate_Empty_Index = find(cell2mat(cellfun(@(s) (length(s<1)), Consolidate_DBData_Option(:,TradeDate_Col_Options),'UniformOutput',0))==1);
                GetTradeID = Consolidate_DBData_Option(TradeDate_Empty_Index,TRADE_ID_Col_Options);
                
                for c = 1 : length(GetTradeID)
                    Current_TradeID = GetTradeID(c);
                    Current_TradeID_Index  = TradeDate_Empty_Index(c);
                    Current_TradeID_ExpiryDate = OTC_Option_Data_Output.TRADE_DATE( strcmpi(OTC_Option_Data_Output.TRADE_ID,Current_TradeID));
                    Min_TradeDate = cellstr( datestr(min(datenum(Current_TradeID_ExpiryDate,DB_Date_Format)),DB_Date_Format) ) ;
                    
                    Consolidate_DBData_Option(Current_TradeID_Index,TradeDate_Col_Options) = Min_TradeDate;
                    
                end
                OTC_Option_Header = {'TRADE_DATE','TRADE_ID','EXPIRY_DATE','FUTURES_REF','B/S','CALL/PUT','LOTS','STRIKE','BARRIER_TYPE','BARRIER_STRIKE'...
                    ,'EDSP',' ',' ','TRADE_PRICE',' ',' ','PREMIUM_USD',' ','MTM_NC','MTM_USD','DR_CR_USD'};
                OTC_Option_Data_Output = cell2dataset([OTC_Option_Header ; Consolidate_DBData_Option]);
                
                 %%% dripping concept for options
                
                 if ~isempty(dripping_dataset)
                    for ka=1:size(OTC_Option_Data_Output,1)
                        idx_match_drip=find(strcmpi(OTC_Option_Data_Output.TRADE_ID(ka),dripping_dataset.contract_number));
                        if ~isempty( idx_match_drip)
                            id_forcurr=strcmpi(OTC_Option_Data_Output.TRADE_ID(ka),OTC_Option_Data.contract_number);
                            currency = unique(OTC_Option_Data.currency(id_forcurr));
                            if strcmpi(currency,'USD')
                                OTC_Option_Data_Output.MTM_USD(ka) =  dripping_dataset.mtm_post_dripping_today(idx_match_drip);
                                OTC_Option_Data_Output.DR_CR_USD(ka) = OTC_Option_Data_Output.MTM_USD(ka)+OTC_Option_Data_Output.PREMIUM_USD(ka);
                            else
                                id_matchcurre = strcmpi(currency_dataset.currency,currency);
                                quantotype=currency_dataset.quantotype(id_matchcurre);
                                OTC_Option_Data_Output.MTM_NC(ka) =  dripping_dataset.mtm_post_dripping_today(idx_match_drip);
                                if(strcmpi(quantotype,'multiplication'))
                                    OTC_Option_Data_Output.MTM_USD(ka) = OTC_Option_Data_Output.MTM_NC(ka) .* currency_dataset.settle_value(id_matchcurre);
                                    OTC_Option_Data_Output.DR_CR_USD(ka) = OTC_Option_Data_Output.MTM_USD(ka)+OTC_Option_Data_Output.PREMIUM_USD(ka);
                                else
                                    OTC_Option_Data_Output.MTM_USD(ka)=OTC_Option_Data_Output.MTM_NC(ka) ./ currency_dataset.settle_value(id_matchcurre);
                                    OTC_Option_Data_Output.DR_CR_USD(ka) = OTC_Option_Data_Output.MTM_USD(ka)+OTC_Option_Data_Output.PREMIUM_USD(ka);
                                end
                            end                            
                        end
                    end
                end
                %%% end of dripping concept
                
            end
            
            %% OTC ACCUMULATORS
            
            if(~isempty( OTC_Accumulators_Data))
                %% Dripping Table reading.
                
                
                
                %% New Contract number logic implementation Trailing A removal
                bb=find(endsWith(OTC_Accumulators_Data.contract_number,'A'));
                if (~isempty(bb))
                    test=cellfun(@(x) x(1:end-1), OTC_Accumulators_Data.contract_number(bb),'UniformOutput',false);
                    OTC_Accumulators_Data.contract_number(bb)=test;
                end
                
                
                %% Trailing B removal
                
                aa=find(endsWith(OTC_Accumulators_Data.contract_number,'B'));
                if (~isempty(aa))
                    test=cellfun(@(x) x(1:end-1), OTC_Accumulators_Data.contract_number(aa),'UniformOutput',false);
                    OTC_Accumulators_Data.contract_number(aa)=test;
                end
                
                OTC_Accumulators_Data_Output = cell2dataset([OTC_Accumulators_Header ; cell(size(OTC_Accumulators_Data,1),size(OTC_Accumulators_Header,2)) ]);
                
                for ii = 1 : size(OTC_Accumulators_Data,1)
                    
                    OTC_Accumulators_Data_Output.TRADE_DATE(ii) = OTC_Accumulators_Data.transaction_date(ii);
                    %OTC_Accumulators_Data_Output.TRADE_DATE(ii) = OTC_Accumulators_Data.transaction_date(ii);
                    OTC_Accumulators_Data_Output.TRADE_ID(ii) = OTC_Accumulators_Data.contract_number(ii);
                    %                     IdxMatch = strcmpi(OTC_Accumulators_Data.contract_number(ii),Client_BookingOrder_Info_Table.contract_number);
                    %                     if ~isempty(find(IdxMatch, 1))
                    %                           OTC_Accumulators_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.future_expiry_date(IdxMatch));
                    %                     else
                    %                      OTC_Accumulators_Data_Output.EXPIRY_DATE(ii) = OTC_Accumulators_Data.maturity_date(ii);
                    
                    %% Ispremium paid check
                    if (OTC_Accumulators_Data.is_premium_paid(ii)==1)
                        OTC_Accumulators_Data.premium_paid_usd(ii)=0;
                    end
                    
                    %% New Logic
                    IdxMatch = strcmpi(OTC_Accumulators_Data.contract_number(ii),Client_BookingOrder_Info_Table.contract_number);
                    if ~isempty(find(IdxMatch, 1))
                        OTC_Accumulators_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.option_expiry_date(IdxMatch));
                    else
                        OTC_Accumulators_Data_Output.EXPIRY_DATE(ii) = OTC_Accumulators_Data.maturity_date(ii);
                    end
                    %OTC_Accumulators_Data_Output.EXPIRY_DATE(ii) = OTC_Accumulators_Data.maturity_date(ii);
                    %OTC_Accumulators_Data_Output.FUTURES_REF(ii) = strcat(OTC_Accumulators_Data.product_code(ii),{' '},datestr(OTC_Accumulators_Data.future_expiry(ii),'mmm-yy'));
                    
                    %                 OTC_Accumulators_Data_Output.STRUCTURE_NAME(ii) = OTC_Accumulators_Data.strike;
                    [ fut_month, OutErrorMsg] = fut_refmonth(OTC_Accumulators_Data.contract_month(ii));
                    OTC_Accumulators_Data_Output.FUTURES_REF(ii) = strcat(OTC_Accumulators_Data.product_code(ii),{' '},fut_month);
                    % commented rajashekhar
                    %                     if(OTC_Accumulators_Data.active_lots(ii) >= 0)
                    %                         OTC_Accumulators_Data_Output.B_S(ii) = {'B'};
                    %                     else
                    %                         OTC_Accumulators_Data_Output.B_S(ii) = {'S'};
                    %                     end
                    % end of comment
                    % as per karthik inputs we are taking lots_per_day
                    % from client booking table
                    if find(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ))
                        % rajashekhar Added as per karthhik inputa per
                        % taking B/S logic on 13/12/2019
                        Acc_Strcture_name = Client_BookingOrder_Info_Table.structure_name(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ));
                        if ~isempty( Acc_Strcture_name) && length(Acc_Strcture_name{1})>=10
                            required_str=lower(Acc_Strcture_name{1,1}(1:10));
                            if strfind( required_str,'long')
                                OTC_Accumulators_Data_Output.B_S(ii) = {'B'};
                            elseif strfind( required_str,'short')
                                OTC_Accumulators_Data_Output.B_S(ii) = {'S'};
                            else
                                if(OTC_Accumulators_Data.active_lots(ii) >= 0)
                                    OTC_Accumulators_Data_Output.B_S(ii) = {'B'};
                                else
                                    OTC_Accumulators_Data_Output.B_S(ii) = {'S'};
                                end
                            end
                        else
                            if(OTC_Accumulators_Data.active_lots(ii) >= 0)
                                OTC_Accumulators_Data_Output.B_S(ii) = {'B'};
                            else
                                OTC_Accumulators_Data_Output.B_S(ii) = {'S'};
                            end
                        end
                        OTC_Accumulators_Data_Output.LOTS_PER_DAY(ii) =num2cell(Client_BookingOrder_Info_Table.lots_per_day(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) )));
                    else
                        if(OTC_Accumulators_Data.active_lots(ii) >= 0)
                            OTC_Accumulators_Data_Output.B_S(ii) = {'B'};
                        else
                            OTC_Accumulators_Data_Output.B_S(ii) = {'S'};
                        end
                        OTC_Accumulators_Data_Output.LOTS_PER_DAY(ii) = num2cell( OTC_Accumulators_Data.active_lots(ii) );
                    end
                    OTC_Accumulators_Data_Output.NOTIONAL_QTY_LOTS(ii) = num2cell( OTC_Accumulators_Data.active_lots(ii) .* OTC_Accumulators_Data.traded_price(ii) );
                    
                    OTC_Accumulators_Data_Output.ACC_LEVEL_1(ii) = num2cell( OTC_Accumulators_Data.strike1(ii) );
                    OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(ii) = num2cell ( OTC_Accumulators_Data.strike2(ii) );
                    
                    OTC_Accumulators_Data_Output.EDSP(ii)  = num2cell( OTC_Accumulators_Data.future_price(ii) );
                    
                    %                     OTC_Accumulators_Data_Output.BARRIER_TYPE(ii) = BookingOrder_DerivativeType_Table.client_statement_type(strcmpi( BookingOrder_DerivativeType_Table.derivative_type,OTC_Accumulators_Data.derivative_type(ii) ));
                    %                     zero_handling=Client_BookingOrder_Info_Table.barrier_type(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ));
                    
                    OTC_Accumulators_Data_Output.BARRIER_TYPE(ii) = Client_BookingOrder_Info_Table.barrier_type(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ));
                    % rajashekhar commented
                    %                    OTC_Accumulators_Data_Output.BARRIER_STRIKE(ii) = num2cell( OTC_Accumulators_Data.barrier1_strike(ii) );
                    % end rajashekhar
                    
                    % as per muthukarthik statment barrier_strike and  taking
                    % from client booking table
                    
                    if find(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ))
                        OTC_Accumulators_Data_Output.BARRIER_STRIKE(ii) = Client_BookingOrder_Info_Table.barrier_level(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_Accumulators_Data.contract_number(ii) ));
                    else
                        OTC_Accumulators_Data_Output.BARRIER_STRIKE(ii) = num2cell( OTC_Accumulators_Data.barrier1_strike(ii) );
                    end
                    
                    Network_Days = size(busdays(char(OTC_Accumulators_Data.value_date(ii)),char(OTC_Accumulators_Data.maturity_date(ii)),'daily','2016-12-25'),1);
                    OTC_Accumulators_Data_Output.ACC_LEFT_LOTS(ii) =   num2cell( OTC_Accumulators_Data.active_lots(ii) *  Network_Days);
                    OTC_Accumulators_Data_Output.ACC_LEFT_DAYS(ii) = num2cell(Network_Days);
                    
                    OTC_Accumulators_Data_Output.PREMIUM_USD(ii) = num2cell( OTC_Accumulators_Data.premium_paid_usd(ii) );
                    
                    if(strcmpi(OTC_Accumulators_Data.currency(ii),'USD'))
                        OTC_Accumulators_Data_Output.MTM_USD(ii) = num2cell( OTC_Accumulators_Data.mkt_value_usd(ii) );
                    else
                        OTC_Accumulators_Data_Output.MTM_NC(ii) = num2cell( OTC_Accumulators_Data.mkt_value_nc(ii) );
                        OTC_Accumulators_Data_Output.MTM_USD(ii) = num2cell( OTC_Accumulators_Data.mkt_value_usd(ii) );
                    end
                  
                    
                    OTC_Accumulators_Data_Output.DR_CR_USD(ii) = num2cell( nansum(OTC_Accumulators_Data.premium_paid_usd(ii) + OTC_Accumulators_Data.mkt_value_usd(ii)) );
                    %OTC_Accumulators_Data_Output.DR_CR_USD(ii)=num2cell(OTC_Accumulators_Data.mkt_value_usd(ii));
                end
                aa=find(strcmpi(OTC_Accumulators_Data_Output.TRADE_ID,'VIT_SB_456'));
                OTC_Accumulators_Data_Output(aa,:)=[];
                % Contract Number & Future Ref based MAX Expiry Date
                
                ContractNum_FuRef = strcat(OTC_Accumulators_Data_Output.TRADE_ID ,OTC_Accumulators_Data_Output.FUTURES_REF);
                
                Unique_ContractNum_FutRef = unique(ContractNum_FuRef);
                
                for j = 1 : length(Unique_ContractNum_FutRef)
                    Current_ContractNum_FutRef = Unique_ContractNum_FutRef(j);
                    
                    Matched_Index = strcmpi(ContractNum_FuRef,Current_ContractNum_FutRef) ;
                    
                    Max_Expiry_Date = cellstr(datestr( max(datenum(unique(OTC_Accumulators_Data_Output.EXPIRY_DATE(Matched_Index)),DB_Date_Format)) , DB_Date_Format));
                    OTC_Accumulators_Data_Output.EXPIRY_DATE(Matched_Index,:) = Max_Expiry_Date;
                    
                    Max_Acc_LeftDays = num2cell( size(busdays(char(ValueDate),char(Max_Expiry_Date),'daily','2016-12-25'),1) );
                    OTC_Accumulators_Data_Output.ACC_LEFT_DAYS(Matched_Index,:) = Max_Acc_LeftDays;
                    OTC_Accumulators_Data_Output.ACC_LEFT_LOTS(Matched_Index,:) = num2cell(cell2mat(OTC_Accumulators_Data_Output.LOTS_PER_DAY(Matched_Index)) .* cell2mat(Max_Acc_LeftDays));
                    
                end
                
                OTC_Accumulator_Data_Output_CellArray = dataset2cell(OTC_Accumulators_Data_Output);
                OTC_Accumulator_Data_Output_CellArray(1,:) = []; %% Remove Header
                
                Empty_TradeID_Index = find(cell2mat(cellfun(@isempty,OTC_Accumulator_Data_Output_CellArray(:,TRADE_ID_Col_Accumulator),'UniformOutput',false)));
                OTC_Accumulator_Data_Output_CellArray(Empty_TradeID_Index,:) = [] ; %% Remove Empty Contract Number
                Empty_TradeID_Index = []; %% For Next Loop Iteration
                
                % Consolidate Level
                [Consolidate_Header_Accumulator,Consolidate_DBData_Accumulator] = consolidatedata(OTC_Accumulators_Header, OTC_Accumulator_Data_Output_CellArray...
                    ,UniqueFields_Accumulator,SumFields_Accumulator,OutputFields_Accumulator,WeightedAverageFields_Accumulator);
                
                OTC_Accumulators_Data_Output = cell2dataset([Consolidate_Header_Accumulator;Consolidate_DBData_Accumulator]);
                %%% dripping concept for accumulator
                if ~isempty(dripping_dataset)
                    for ka=1:size(OTC_Accumulators_Data_Output,1)
                        idx_match_drip=find(strcmpi(OTC_Accumulators_Data_Output.TRADE_ID(ka),dripping_dataset.contract_number));
                        if ~isempty( idx_match_drip)
                            id_forcurr=strcmpi(OTC_Accumulators_Data_Output.TRADE_ID(ka),OTC_Accumulators_Data.contract_number);
                            currency = unique(OTC_Accumulators_Data.currency(id_forcurr));
                            if strcmpi(currency,'USD')
                                OTC_Accumulators_Data_Output.MTM_USD(ka) =  dripping_dataset.mtm_post_dripping_today(idx_match_drip) ;
                                OTC_Accumulators_Data_Output.DR_CR_USD(ka) = OTC_Accumulators_Data_Output.MTM_USD(ka)+OTC_Accumulators_Data_Output.PREMIUM_USD(ka);
                            else
                                id_matchcurre = strcmpi(currency_dataset.currency,currency);
                                quantotype=currency_dataset.quantotype(id_matchcurre);
                                OTC_Accumulators_Data_Output.MTM_NC(ka) = dripping_dataset.mtm_post_dripping_today(idx_match_drip);
                                if(strcmpi(quantotype,'multiplication'))
                                    OTC_Accumulators_Data_Output.MTM_USD(ka) = OTC_Accumulators_Data_Output.MTM_NC(ka) .* currency_dataset.settle_value(id_matchcurre);
                                    OTC_Accumulators_Data_Output.DR_CR_USD(ka) = OTC_Accumulators_Data_Output.MTM_USD(ka)+OTC_Accumulators_Data_Output.PREMIUM_USD(ka);
                                else
                                    OTC_Accumulators_Data_Output.MTM_USD(ka) = OTC_Accumulators_Data_Output.MTM_NC(ka) ./ currency_dataset.settle_value(id_matchcurre);
                                    OTC_Accumulators_Data_Output.DR_CR_USD(ka) = OTC_Accumulators_Data_Output.MTM_USD(ka)+OTC_Accumulators_Data_Output.PREMIUM_USD(ka);
                                end
                            end                            
                        end
                    end
                end
                
                
                Accumulators_SUM_PREMIUM_USD=sum(OTC_Accumulators_Data_Output.PREMIUM_USD);
                Accumulators_SUM_MTM_NC=sum(OTC_Accumulators_Data_Output.MTM_NC);
                Accumulators_SUM_MTM_USD=sum(OTC_Accumulators_Data_Output.MTM_USD);
                Accumulators_DR_CR_USD=sum(OTC_Accumulators_Data_Output.DR_CR_USD);
                %-------- Using Booking Order Information to make the format %-------
                
                TempCell = cell(size(OTC_Accumulators_Data_Output,1),1);
                TempCell(:,:) = {''}; %% Need to change NaN data types columns to String datatype
                OTC_Accumulators_Data_Output.STRUCTURE_NAME = TempCell;
                % raj added
                OTC_Accumulators_Data_Output.ACC_LEVEL_2_3=num2cell( OTC_Accumulators_Data_Output.ACC_LEVEL_2_3);
                % end
                for c = 1 : size(OTC_Accumulators_Data_Output,1)
                    Current_TradeDate = OTC_Accumulators_Data_Output.TRADE_DATE(c);
                    Current_TradeID = OTC_Accumulators_Data_Output.TRADE_ID(c);
                    
                    Matched_Index = strcmpi(Client_BookingOrder_Info_Table.contract_number,Current_TradeID) & strcmpi(Client_BookingOrder_Info_Table.trade_date,Current_TradeDate);
                    if(~isempty(find(Matched_Index,1)))
                        
                        Matched_Data = Client_BookingOrder_Info_Table(Matched_Index,:);
                        
                        OTC_Accumulators_Data_Output.STRUCTURE_NAME(c) = upper(Matched_Data.structure_name) ;
                        % uncommented raja
                        OTC_Accumulators_Data_Output.LOTS_PER_DAY(c) = Matched_Data.lots_per_day;
                        % end
                        OTC_Accumulators_Data_Output.NOTIONAL_QTY_LOTS(c) = Matched_Data.notional_lots;
                        if iscell(Matched_Data.acc_level_1)
                            %                             OTC_Accumulators_Data_Output.ACC_LEVEL_1(c) = cell2mat(Matched_Data.acc_level_1);
                            %                         else
                            OTC_Accumulators_Data_Output.ACC_LEVEL_1(c) = str2double(char((Matched_Data.acc_level_1)));
                        end
                        
                        %                         end
                        
                        if iscell(Matched_Data.acc_level_2)
                            if cellfun(@isnan,OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c))
                                %                             if isnan(OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c))
                                OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c)=cellstr(' ');
                            end
                            
                            %OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c) = str2double(Matched_Data.acc_level_2);
                            OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c) = cellstr(Matched_Data.acc_level_2);
                        else
                            
                            OTC_Accumulators_Data_Output.ACC_LEVEL_2_3(c) = Matched_Data.acc_level_2;
                        end
                    end
                    
                end
                
                %--------  Making For loop to show required data only ------------%
                
                Final_Required_AccumulatorData = [];
                if exist('OTC_Accumulators_Data_Output','var')
                    
                    Unique_TradeID = unique((OTC_Accumulators_Data_Output.TRADE_ID));
                    for k = 1 : size(Unique_TradeID,1)
                        Current_TradeID =  Unique_TradeID(k);
                        Matched_TradeID_Index = strcmpi(OTC_Accumulators_Data_Output.TRADE_ID,Current_TradeID) ;
                        if(~isempty(find(Matched_TradeID_Index,1)))
                            Matched_Row = OTC_Accumulators_Data_Output(Matched_TradeID_Index,:);
                            %                         Matched_Row =  Matched_Row(~cellfun('isempty',Matched_Row.STRUCTURE_NAME),:); % Remove Empty Structure Name
                            if(isempty(Matched_Row))
                                continue;
                            end
                            
                            if(size(Matched_Row,1) == 1)               %% Like Daily Call
                                
                                Matched_Row.ACC_LEFT_LOTS = abs(Matched_Row.ACC_LEFT_LOTS);
                                Final_Required_AccumulatorData = [Final_Required_AccumulatorData ; Matched_Row];
                                
                            else                       %% (size(Matched_Row,1) > 1) % Like SHORT & LONG STEED
                                Matched_Row_LeftLots = Matched_Row.ACC_LEFT_LOTS(1);
                                Matched_Row_MTM_NC = sum(Matched_Row.MTM_NC);
                                Matched_Row_MTM_USD = sum(Matched_Row.MTM_USD);
                                Matched_Row_DR_CR_USD = sum(Matched_Row.DR_CR_USD);
                                
                                Matched_Row_Unique_StrutName = unique(Matched_Row.STRUCTURE_NAME);
                                if(cellStrfind(Matched_Row_Unique_StrutName,{'SHORT'}))
                                    Matched_Short_Row = find(strcmpi(Matched_Row.B_S,'S'));
                                    Matched_Row = Matched_Row(Matched_Short_Row(1),:); % Some times two Sell & Buy index will come , that time take first index
                                else
                                    Matched_Short_Row = find(strcmpi(Matched_Row.B_S,'B'));
                                    if (~isempty(Matched_Short_Row))
                                        Matched_Row = Matched_Row(Matched_Short_Row(1),:); %  Some times two Sell & Buy index will come , that time take first index
                                    end
                                    
                                end
                                
                                Matched_Row.ACC_LEFT_LOTS = Matched_Row_LeftLots;
                                Matched_Row.MTM_NC = Matched_Row_MTM_NC;
                                Matched_Row.MTM_USD = Matched_Row_MTM_USD;
                                Matched_Row.DR_CR_USD = Matched_Row_DR_CR_USD;
                                
                                Final_Required_AccumulatorData = [Final_Required_AccumulatorData ; Matched_Row];
                                
                            end
                        end
                    end
                end
                
                OTC_Accumulators_Data_Output = Final_Required_AccumulatorData;
                %exact_trade_match=strcmpi(OTC_Accumulators_Data_Output.STRUCTURE_NAME,'CALL')| strcmpi(OTC_Accumulators_Data_Output.STRUCTURE_NAME,'PUT')|strcmpi(OTC_Accumulators_Data_Output.STRUCTURE_NAME,{''})|strcmpi(OTC_Accumulators_Data_Output.STRUCTURE_NAME,'0')
                
            end
            %% Excel Sheet Write
            if(~isempty(OTC_Swaps_Data))
                OTC_Swaps_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Swaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                OTC_Swaps_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Swaps_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                
                [OTC_Swaps_Data_Output] = Find_Statement_ContractMonth_Wise_Swaps_Total(OTC_Swaps_Header,OTC_Swaps_Data_Output); % Put CM wise Total
                
                OTC_Swaps_Data_Output = dataset2cell(OTC_Swaps_Data_Output);
                
                %%%%
                OTC_Swaps_Data_Output(1,cellStrfind(OTC_Swaps_Data_Output(1,:),'x'))= {[]}; % Code added by Ananthi to remove fields containing "x"
                OTC_Swaps_Data_Output(cell2mat(cellfun(@(x)any(isnan(x)),OTC_Swaps_Data_Output,'UniformOutput',false))) = {[]};
                %% sip  batch fix
                
                Overall_Current_Client_Data = [Overall_Current_Client_Data ; [Temp_OTC_Swaps_Header ; OTC_Swaps_Data_Output] ];
                %                 end
            end
            
            if(~isempty(OTC_Option_Data))
                %% Sorting based on Product code, Expiry Date, Short & Long lots, Trade Date
                % Convert the dates to number format before sorting
                OTC_Option_Data_Output.EXPIRY_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Option_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Option_Data_Output.TRADE_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Option_Data_Output.TRADE_DATE,'UniformOutput',0);
                
                % Sort based on the product code, then by Expiry Date and
                % by Short Lots first
                TempProductCode      = strtok(OTC_Option_Data_Output.FUTURES_REF,' ');
                OTC_Option_Data_Output.product_code = TempProductCode;
                
                Header = OTC_Option_Data_Output.Properties.VarNames;
                ProductCode_Col_Options = find(strcmpi(Header,'product_code'));
                OTC_Option_Data_Output = sortrows(OTC_Option_Data_Output,[ProductCode_Col_Options,EXPIRY_DATE_Col_Options,TradeDate_Col_Options],{'ascend'});
                %                 OTC_Option_Data_Output(end,:)=[];
                OTC_Option_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(s,Client_Statement_Format)),...
                    OTC_Option_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Option_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,Client_Statement_Format)),...
                    OTC_Option_Data_Output.TRADE_DATE,'UniformOutput',0);
                OTC_Option_Data_Output.product_code = [];
                
                %                 OTC_Option_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Option_Data_Output.TRADE_DATE,'UniformOutput',0);
                %                 OTC_Option_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Option_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                
                OTC_Option_Data_Output = dataset2cell(OTC_Option_Data_Output);                         %
                
                
                OTC_Option_Data_Output(1,cellStrfind(OTC_Option_Data_Output(1,:),'x'))= {[]}; % Code added by Ananthi to remove fields containing "x"
                OTC_Option_Data_Output(cell2mat(cellfun(@(x)any(isnan(x)),OTC_Option_Data_Output,'UniformOutput',false))) = {[]};
                
                Total_Empty_Array = cell(2,Fixed_Header_Col_Size); %% Find SectionWise Total
                Total_Sum_Required_Col = {'PREMIUM_USD','MTM_NC','MTM_USD','DR_CR_USD'};
                Total_Sum_Required_Col_Index = cellStrfind_exact(OTC_Option_Data_Output(1,:),Total_Sum_Required_Col);
                [~,ColSum] = cell2sum_Row_Col(OTC_Option_Data_Output(2:end,Total_Sum_Required_Col_Index));
                Total_Empty_Array(2,Total_Sum_Required_Col_Index) = ColSum ;
                Total_Empty_Array(2,1) = {'TOTAL'};
                OTC_Option_Data_Output = [OTC_Option_Data_Output ; Total_Empty_Array] ; % append Total Row
                
                Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ; Temp_OTC_Option_Header ;OTC_Option_Data_Output]] ;
                
            end
            if(~isempty(OTC_Accumulators_Data))
                
                %% Sorting based on Product code, Expiry Date, Short & Long lots, Trade Date
                % Convert the dates to number format before sorting
                OTC_Accumulators_Data_Output.EXPIRY_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Accumulators_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Accumulators_Data_Output.TRADE_DATE = cellfun(@(s) (datenum(s,DB_Date_Format)),...
                    OTC_Accumulators_Data_Output.TRADE_DATE,'UniformOutput',0);
                
                % Sort based on the product code, then by Expiry Date and
                % by Short Lots first
                TempProductCode      = strtok(OTC_Accumulators_Data_Output.FUTURES_REF,' ');
                OTC_Accumulators_Data_Output.product_code = TempProductCode;
                
                Header = OTC_Accumulators_Data_Output.Properties.VarNames;
                ProductCode_Col_Accum = find(strcmpi(Header,'product_code'));
                OTC_Accumulators_Data_Output = sortrows(OTC_Accumulators_Data_Output,[ProductCode_Col_Accum,EXPIRY_DATE_Col_Accumulator,TradeDate_Col_Accumulator],{'ascend'});
                
                OTC_Accumulators_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(s,Client_Statement_Format)),...
                    OTC_Accumulators_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                OTC_Accumulators_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,Client_Statement_Format)),...
                    OTC_Accumulators_Data_Output.TRADE_DATE,'UniformOutput',0);
                OTC_Accumulators_Data_Output.product_code = [];
                
                %                 OTC_Accumulators_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Accumulators_Data_Output.TRADE_DATE,'UniformOutput',0);
                %                 OTC_Accumulators_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_Accumulators_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                
                OTC_Accumulators_Data_Output = dataset2cell(OTC_Accumulators_Data_Output);
                OTC_Accumulators_Data_Output(1,cellStrfind(OTC_Accumulators_Data_Output(1,:),'x'))= {[]};  % Code added by Ananthi to remove fields containing "x"
                OTC_Accumulators_Data_Output(cell2mat(cellfun(@(x)any(isnan(x)),OTC_Accumulators_Data_Output,'UniformOutput',false))) = {[]};
                
                Total_Empty_Array = cell(2,Fixed_Header_Col_Size); %% Find SectionWise Total
                Total_Sum_Required_Col = {'PREMIUM_USD','MTM_NC','MTM_USD','DR_CR_USD'};
                Total_Sum_Required_Col_Index = cellStrfind_exact(OTC_Accumulators_Data_Output(1,:),Total_Sum_Required_Col);
                [~,ColSum] = cell2sum_Row_Col(OTC_Accumulators_Data_Output(2:end,Total_Sum_Required_Col_Index));
                Total_Empty_Array(2,Total_Sum_Required_Col_Index) = ColSum ;
                Total_Empty_Array(2,1) = {'TOTAL'};
                OTC_Accumulators_Data_Output = [OTC_Accumulators_Data_Output ; Total_Empty_Array] ; % append Total Row
                
                
                % format long g;
                Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ; Temp_OTC_Accumulators_Header ;OTC_Accumulators_Data_Output]] ;
                
            end
            if(~isempty(OTC_FXSwaps_Data))
                OTC_FXSwaps_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FXSwaps_Data_Output.TRADE_DATE,'UniformOutput',0);
                OTC_FXSwaps_Data_Output.VALUE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FXSwaps_Data_Output.VALUE_DATE,'UniformOutput',0);
                OTC_FXSwaps_Data_Output.FIX_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FXSwaps_Data_Output.FIX_DATE,'UniformOutput',0);
                
                OTC_FXSwaps_Data_Output = dataset2cell(OTC_FXSwaps_Data_Output);
                OTC_FXSwaps_Data_Output(1,cellStrfind(OTC_FXSwaps_Data_Output(1,:),'x'))= {[]}; % Code added by Ananthi to remove fields containing "x"
                OTC_FXSwaps_Data_Output(cell2mat(cellfun(@(x)any(isnan(x)),OTC_FXSwaps_Data_Output,'UniformOutput',false))) = {[]};
                
                Total_Empty_Array = cell(2,Fixed_Header_Col_Size); %% Find SectionWise Total
                Total_Sum_Required_Col = {'MTM_NC','MTM_USD'};
                Total_Sum_Required_Col_Index = cellStrfind_exact(OTC_FXSwaps_Data_Output(1,:),Total_Sum_Required_Col);
                [~,ColSum] = cell2sum_Row_Col(OTC_FXSwaps_Data_Output(2:end,Total_Sum_Required_Col_Index));
                
                %% Dynamism addition
                BUY_NOMINAL_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'BUY_NOMINAL'));
                BUY_PRICE_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'BUY_PRICE'));
                SELL_NOMINAL_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'SELL_NOMINAL'));
                SELL_PRICE_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'SELL_PRICE'));
                SETT_PRICE_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'SETT_PRICE'));
                MTM_NC_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'MTM_NC'));
                MTM_USD_INDEX=find(strcmpi(OTC_FXSwaps_Data_Output(1,:),'MTM_USD'));
                
                [~,Subtotal]=cell2sum_Row_Col(OTC_FXSwaps_Data_Output(2:end,[BUY_NOMINAL_INDEX,BUY_PRICE_INDEX,SELL_NOMINAL_INDEX,SELL_PRICE_INDEX,SETT_PRICE_INDEX,MTM_NC_INDEX,MTM_USD_INDEX]));
                Total_Empty_Array(2,Total_Sum_Required_Col_Index) = ColSum ;
                Total_Empty_Array(1,[BUY_NOMINAL_INDEX,BUY_PRICE_INDEX,SELL_NOMINAL_INDEX,SELL_PRICE_INDEX,SETT_PRICE_INDEX,MTM_NC_INDEX,MTM_USD_INDEX]) = Subtotal ;
                Total_Empty_Array(2,1) = {'TOTAL'};
                OTC_FXSwaps_Data_Output_temp = [OTC_FXSwaps_Data_Output ; Total_Empty_Array] ; % append Total Row
                
                [len_val]=size( OTC_FXSwaps_Data_Output_temp);
                OTC_FXSwaps_Data_Output_trimmed=OTC_FXSwaps_Data_Output_temp(1:len_val-2,:);
                %                 SELL_PRICE_IDX_REQ=find(strcmpi(     OTC_FXSwaps_Data_Output_trimmed(1,:),'SELL_PRICE'));
                %                 SELL_PRICE_DATA=  OTC_FXSwaps_Data_Output_trimmed(2:end, SELL_PRICE_IDX_REQ);
                OTC_FXSwaps_Data_Output=cell2dataset(OTC_FXSwaps_Data_Output_trimmed);
                %% Function call to generate propre dataset format for fxswaps subtotal
                [OTC_FX_Swaps_check_Data_Output_Total]=format_total_fx_swaps(OTC_FXSwaps_Header,OTC_FXSwaps_Data_Output);
                OTC_FX_Swaps_check_Data_Output_Total_ds=dataset2cell(OTC_FX_Swaps_check_Data_Output_Total);
                
                Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ; Temp_OTC_FXSwaps_Header ; OTC_FX_Swaps_check_Data_Output_Total_ds]] ;
                
                
            end
            
            %% New Table data
            
            %% New queries for PTAX FIX.
            %             table_ptax=' currency_forward_mapping_table';
            %             a_ptax=['select * from', table_ptax];
            %             [colnames_ptax,data_ptax]=Fetch_DB_Data( ObjDB,a_ptax,[]);
            %             Mapping_table_data=[colnames_ptax;data_ptax];
            %             Mapping_table_dataset=cell2dataset( Mapping_table_data);
            
            %% Table name for PTAX fill
            Name_table_ptax='underlying_settle_value_table';
            
            %% Product code and spot rate matching...PTAX section any ptax related issue please refer this.
            for i_n=1:length(OTC_FX_OPTIONS)
                str_mat_idx=find(strcmpi(OTC_FX_OPTIONS.product_code(i_n),Mapping_table_dataset.product_code));
                spot_rate=Mapping_table_dataset.spot(str_mat_idx);
                table_query=['select * from ',Name_table_ptax,' where underlying_id like ''','CR ',char(spot_rate), ' spot','%',''' and settlement_date=''',char(OTC_FX_OPTIONS.settlement_date(i_n)),''' '];
                [colnames_PTAX,data_PTAX]=Fetch_DB_Data( ObjDB,table_query,[]);
                ptax_table_data=[colnames_PTAX;data_PTAX];
                ptax_dataset=cell2dataset(ptax_table_data);
                OTC_FX_OPTIONS.underlying1_price(i_n)=ptax_dataset.settle_value;
                
            end
            
            %% OTC_FX_OPTIONS_FIELDS
            UniqueFields_FX_OPTIONS={'TRADE_ID'};
            SumFields_FX_OPTIONS = {'MTM_USD'};
            OutputFields_FX_OPTIONS = OTC_FX_OPTIONS_Header;
            WeightedAverageFields_FX_OPTIONS = [];
            
            
            %% Here we can add OTC_FX_OPTION code
            [row,~]=size(OTC_FX_OPTION_INDEX);
            
            if(~isempty(OTC_FX_OPTIONS))
                
                [~,bre]=size(OTC_FX_OPTIONS_Header);
                
                %OTC_FX_Options_Data_Output = cell2dataset([OTC_FX_OPTIONS_Header ; cell(row-1,bre)]);
                OTC_FX_Options_Data_Output = cell2dataset([OTC_FX_OPTIONS_Header ; cell(size(OTC_FX_OPTIONS,1),size(OTC_FX_OPTIONS_Header,2))]);
                %% New additions
                %                 OTC_FX_Options_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)), OTC_FX_OPTIONS.transaction_date,'UniformOutput',0);
                %                 OTC_FX_Options_Data_Output.F = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)), OTC_FX_OPTIONS.transaction_date,'UniformOutput',0);
                
                for ii = 1 : size(OTC_FX_OPTIONS,1)
                    OTC_FX_Options_Data_Output.TRADE_DATE(ii) = OTC_FX_OPTIONS.transaction_date(ii);
                    
                    OTC_FX_Options_Data_Output.TRADE_ID(ii) = OTC_FX_OPTIONS.contract_number(ii);
                    
                    
                    
                    OTC_FX_Options_Data_Output.FXPAIR(ii) = OTC_FX_OPTIONS.instrument(ii);
                    OTC_FX_Options_Data_Output.PTAXASK(ii)=num2cell(OTC_FX_OPTIONS.underlying1_price(ii)); %% Please check and confirm.
                    OTC_FX_Options_Data_Output.FIX_DATE(ii)=OTC_FX_OPTIONS.fixing_date(ii);
                    %                     IdxMatch = strcmpi(OTC_FX_Options_Data_Output.TRADE_ID(ii),Client_BookingOrder_Info_Table.contract_number);
                    %                     if ~isempty(find(IdxMatch, 1))
                    %                         OTC_FX_Options_Data_Output .EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.future_expiry_date(IdxMatch));
                    %                     else
                    %                         OTC_FX_Options_Data_Output.EXPIRY_DATE(ii) =  OTC_FX_OPTIONS.maturity_date(ii);
                    %                     end
                    %                     OTC_FX_Options_Data_Output.VALUE_DATE(ii)=OTC_FX_OPTIONS.maturity_date(ii);
                    Curr_type=strrep(OTC_FX_OPTIONS.instrument(ii),'/','');
                    headers=Map_data_fx_options.Properties.VarNames;
                    Curr_type_idx=find(strcmpi(  headers,'currency_pair'));
                    fixing_date_idx=find(strcmpi(  headers,'fixing_date'));
                    value_date_idx=find(strcmpi(  headers,'value_date'));
                    if ~isempty(Curr_type_idx) && ~isempty(fixing_date_idx) && ~isempty(value_date_idx)
                        idx_curr_type=find(ismember(Map_data_fx_options.currency_pair,Curr_type));
                        req_data=Map_data_fx_options(idx_curr_type,:);
                        valuedate_index= find(ismember(datenum(req_data.fixing_date),datenum(OTC_FX_OPTIONS.fixing_date(ii))));
                        % commented for check
                        %   date_fixing_index=find(ismember(datenum(req_data.fixing_date),datenum(OTC_FX_OPTIONS.maturity_date(ii))));
                        %end
                        if ~isempty( valuedate_index)
                            OTC_FX_Options_Data_Output.VALUE_DATE(ii)=req_data.value_date(valuedate_index);
                        else
                            OutErrorMsg=[ OTC_FX_OPTIONS.contract_number{ii} '- ' OTC_FX_OPTIONS.fixing_date{ii} ' date not in fx_option_fix_value_date_table_cfs' ];
                            OTC_FX_Options_Data_Output.VALUE_DATE(ii)={''};
                            continue;
                        end
                    else
                        OutErrorMsg=['Missing currency_pair /fixing_date/value_date in the table   ' Table_Name];
                    end
                    %% Premium paid fix
                    if (OTC_FX_OPTIONS.is_premium_paid(ii)==1)
                        OTC_FX_Options_Data_Output.PREMIUM_USD(ii)=num2cell(0);
                    else
                        OTC_FX_Options_Data_Output.PREMIUM_USD(ii)=num2cell(OTC_FX_OPTIONS.premium_paid_usd(ii));
                    end
                    
                    OTC_FX_Options_Data_Output.STRIKE(ii)=num2cell(OTC_FX_OPTIONS.strike1(ii));
                    OTC_FX_Options_Data_Output.MTM_USD(ii)=num2cell(OTC_FX_OPTIONS.mkt_value_usd(ii)); %% This we have to change.
                    %                     OTC_FX_Options_Data_Output.DR_CR_USD(ii)= num2cell( nansum(OTC_FX_OPTIONS.premium_paid_usd(ii) + OTC_FX_OPTIONS.mkt_value_usd(ii)));
                    OTC_FX_Options_Data_Output.x_DR___CR_USD(ii)= num2cell((OTC_FX_OPTIONS.premium_paid_usd(ii)+ OTC_FX_OPTIONS.mkt_value_usd(ii)));
                    
                    OTC_FX_Options_Data_Output.TRADED_PRICE___(ii)=num2cell(OTC_FX_OPTIONS.traded_price(ii).*100);
                    
                    %  Rajashekhar commented
                    %OTC_FX_Options_Data_Output.BARRIER_STRIKE(ii)=num2cell(OTC_FX_OPTIONS.barrier1_strike(ii));
                    % end
                    
                    if find(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_FX_OPTIONS.contract_number(ii) ))
                        
                        OTC_FX_Options_Data_Output.BARRIER_STRIKE(ii) = Client_BookingOrder_Info_Table.barrier_level(strcmpi( Client_BookingOrder_Info_Table.contract_number,OTC_FX_OPTIONS.contract_number(ii) ));
                    else
                        OTC_FX_Options_Data_Output.BARRIER_STRIKE(ii)=num2cell(OTC_FX_OPTIONS.barrier1_strike(ii));
                    end
                    
                    OTC_FX_Options_Data_Output.NOTIONAL_USD_(ii) = num2cell(abs(OTC_FX_OPTIONS.active_lots(ii)));
                    
                    
                    
                    %% B_S Management
                    if(OTC_FX_OPTIONS.active_lots(ii) >= 0)
                        OTC_FX_Options_Data_Output .B_S(ii) = {'B'};
                    else
                        OTC_FX_Options_Data_Output  .B_S(ii) = {'S'};
                    end
                    %% Call/put management
                    %                     if(strcmpi(OTC_FX_OPTIONS.derivative_type(ii),'fx_vanilla_call'))
                    if cell2mat(strfind(OTC_FX_OPTIONS.derivative_type(ii),'call'))~=0
                        OTC_FX_Options_Data_Output .CALL_PUT(ii) = {'CALL'};
                    else
                        OTC_FX_Options_Data_Output .CALL_PUT(ii) = {'PUT'};
                    end
                    
                    OTC_FX_Options_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        OTC_FX_Options_Data_Output.TRADE_DATE,'UniformOutput',0);
                    
                    OTC_FX_Options_Data_Output.FIX_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        OTC_FX_Options_Data_Output.FIX_DATE,'UniformOutput',0);
                    
                    OTC_FX_Options_Data_Output.VALUE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        OTC_FX_Options_Data_Output.VALUE_DATE,'UniformOutput',0);
                    
                    OTC_FX_Options_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FX_Options_Data_Output.TRADE_DATE,'UniformOutput',0);
                    OTC_FX_Options_Data_Output.FIX_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FX_Options_Data_Output.FIX_DATE,'UniformOutput',0);
                    OTC_FX_Options_Data_Output.VALUE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),OTC_FX_Options_Data_Output.VALUE_DATE,'UniformOutput',0);
                    %% OTC
                    OTC_FX_Options_Data_Output_CellArray = dataset2cell(OTC_FX_Options_Data_Output);
                    OTC_FX_Options_Data_Output_CellArray(1,:)=[];
                    
                    %% Consolidate function output
                    [Consolidate_Header,Consolidate_DBData] = consolidatedata(OTC_FX_OPTIONS_Header,OTC_FX_Options_Data_Output_CellArray,UniqueFields_FX_OPTIONS,SumFields_FX_OPTIONS, OutputFields_FX_OPTIONS , WeightedAverageFields_FX_OPTIONS);
                    TradeDate_Empty_Index = find(cell2mat(cellfun(@(s) (length(s<1)), Consolidate_DBData(:,TradeDate_Col_FX_OPTIONS),'UniformOutput',0))==1);
                    GetTradeID = Consolidate_DBData(TradeDate_Empty_Index,TRADE_ID_Col_FX_OPTIONS);
                    
                    for c = 1 : length(GetTradeID)
                        Current_TradeID = GetTradeID(c);
                        Current_TradeID_Index  = TradeDate_Empty_Index(c);
                        Current_TradeID_ExpiryDate = OTC_FX_Options_Data_Output.TRADE_DATE( strcmpi(OTC_FX_Options_Data_Output.TRADE_ID,Current_TradeID));
                        Min_TradeDate = cellstr( datestr(min(datenum(Current_TradeID_ExpiryDate,DB_Date_Format)),DB_Date_Format) ) ;
                        
                        Consolidate_DBData(Current_TradeID_Index,TradeDate_Col_FX_OPTIONS) = Min_TradeDate;
                        %                 Consolidate_DBData(Current_TradeID_Index,RefMonth_Col_Swaps) = {'future_expiry'};
                        % Consolidate_DBData(cell2mat(cellfun(@(x)any(isnan(x)),Consolidate_DBData,'UniformOutput',false))) = {[]};
                        Consolidate_DBData(cell2mat(cellfun(@(x)any(isnan(x)),Consolidate_DBData,'UniformOutput',false))) = {[]};
                        
                        if (~isempty( Consolidate_DBData))
                            l=bre;  %% add dynamism
                            Total_cell_creation=cell(2,l);
                            Total_cell_creation{1,1}='TOTAL';
                            %% Last Three Column sum implementation.
                            Total_cell_creation{1,end}=nansum(cell2mat(Consolidate_DBData(:,end)));
                            Total_cell_creation(1,end-1)=num2cell(nansum(cell2mat(Consolidate_DBData(:,end-1))));
                            Total_cell_creation(1,end-2)=num2cell(nansum(cell2mat(Consolidate_DBData(:,end-2))));
                            % Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ;  Temp_OTC_FXOptions_Header ; OTC_FX_OPTIONS_Header;  Consolidate_DBData; Total_cell_creation ] ];
                            %                             Overall_Current_Client_Data = [Overall_Current_Client_Data; [Temp_Header;Temp_OTC_FXOptions_Header]]
                            
                        end
                        
                    end
                    
                end
                
                if (~isempty( Consolidate_DBData))
                    l=bre;  %% add dynamism
                    Total_cell_creation=cell(2,l);
                    Total_cell_creation{1,1}='TOTAL';
                    %% Last Three Column sum implementation.
                    Total_cell_creation{1,end}=nansum(cell2mat(Consolidate_DBData(:,end)));
                    Total_cell_creation(1,end-1)=num2cell(nansum(cell2mat(Consolidate_DBData(:,end-1))));
                    Total_cell_creation(1,end-2)=num2cell(nansum(cell2mat(Consolidate_DBData(:,end-2))));
                    % Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ;  Temp_OTC_FXOptions_Header ; OTC_FX_OPTIONS_Header;  Consolidate_DBData; Total_cell_creation ] ];
                    %                             Overall_Current_Client_Data = [Overall_Current_Client_Data; [Temp_Header;Temp_OTC_FXOptions_Header]]
                    
                end
                if (isempty( Overall_Current_Client_Data))
                    Overall_Current_Client_Data=[Temp_OTC_FXOptions_Header; OTC_FX_OPTIONS_Header;Consolidate_DBData; Total_cell_creation];
                    [len_check,bre_check]=size(Overall_Current_Client_Data);
                    for k = 1:len_check
                        for j=1:bre_check
                            if isnan(Overall_Current_Client_Data{k,j})
                                Overall_Current_Client_Data{k,j} = '-';
                            end
                            if (Overall_Current_Client_Data{k,11} == '-')
                                Overall_Current_Client_Data{k,11} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,13} == '-')
                                Overall_Current_Client_Data{k,13} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,14} == '-')
                                Overall_Current_Client_Data{k,14} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,16} == '-')
                                Overall_Current_Client_Data{k,16} = ' ';
                                
                            end
                            if (Overall_Current_Client_Data{k,17} == '-')
                                Overall_Current_Client_Data{k,17} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,18} == '-')
                                Overall_Current_Client_Data{k,18} = ' ';
                            end
                        end
                    end
                    %                    Overall_Current_Client_Data(cellfun(@(x) any(isnan(x)),x)) = [];
                    %Overall_Current_Client_Data(cellfun(@(Overall_Current_Client_Data) any(isnan(Overall_Current_Client_Data)),Overall_Current_Client_Data)) = []
                else
                    a=exist('Total_cell_creation');
                    if a==1
                        Overall_Current_Client_Data=[Overall_Current_Client_Data;[Temp_Header;Temp_OTC_FXOptions_Header;OTC_FX_OPTIONS_Header; Consolidate_DBData;Total_cell_creation]];
                    else
                        Overall_Current_Client_Data=[Overall_Current_Client_Data;[Temp_Header;Temp_OTC_FXOptions_Header;OTC_FX_OPTIONS_Header; Consolidate_DBData]];
                    end
                    
                    
                    [len_check,bre_check]=size(Overall_Current_Client_Data);
                    for k = 1:len_check
                        for j=1:bre_check
                            if isnan(Overall_Current_Client_Data{k,j})
                                Overall_Current_Client_Data{k,j} = '-';
                            end
                            if (Overall_Current_Client_Data{k,11} == '-')
                                Overall_Current_Client_Data{k,11} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,13} == '-')
                                Overall_Current_Client_Data{k,13} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,14} == '-')
                                Overall_Current_Client_Data{k,14} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,16} == '-')
                                Overall_Current_Client_Data{k,16} = ' ';
                                
                            end
                            if (Overall_Current_Client_Data{k,17} == '-')
                                Overall_Current_Client_Data{k,17} = ' ';
                            end
                            if (Overall_Current_Client_Data{k,18} == '-')
                                Overall_Current_Client_Data{k,18} = ' ';
                            end
                            
                        end
                    end
                    
                end
            end
            
            
            %% Compo swap mapping Table
            %             Table_Name='composwap_mapping_table';
            %             %Sqlquery=['select * from ', Table_Name];
            %             [Headers,Data]=  Fetch_DB_Data(ObjDB,[],Table_Name);
            %             Conversion_Data=cell2dataset([Headers;Data]);
            
            
            %% KC/USDBRL Compos SWAPS Introduction.
            
            
            [row,~]=size( KC_COMPOS_INDEX);
            if(~isempty(KC_COMPOS_SWAPS))
                %% New Functinality Implementation
                
                KC_Compos_Data_Output = cell2dataset([ KC_Compo_Header; cell(row-1,21)]);
                for ii = 1 : size(KC_COMPOS_SWAPS,1)
                    KC_Compos_Data_Output.TRADE_DATE(ii) = KC_COMPOS_SWAPS.transaction_date(ii);
                    KC_Compos_Data_Output.TRADE_ID(ii)=KC_COMPOS_SWAPS.contract_number(ii);
                    
                    IdxMatch = strcmpi(KC_COMPOS_SWAPS.contract_number(ii),Client_BookingOrder_Info_Table.contract_number);
                    if ~isempty(find(IdxMatch, 1))
                        KC_Compos_Data_Output.EXPIRY_DATE(ii) = unique(Client_BookingOrder_Info_Table.future_expiry_date(IdxMatch));
                    else
                        KC_Compos_Data_Output.EXPIRY_DATE(ii) =KC_COMPOS_SWAPS.maturity_date(ii);
                    end
                    
                    
                    %                     KC_Compos_Data_Output.REF_MONTH(ii)=strcat(KC_COMPOS_SWAPS.product_code(ii),{' '},datestr(KC_COMPOS_SWAPS.future_expiry(ii),'mmm-yy'));
                    [ fut_month, OutErrorMsg] = fut_refmonth(KC_COMPOS_SWAPS.contract_month(ii));
                    KC_Compos_Data_Output.FUTURES_REF(ii) = strcat(KC_COMPOS_SWAPS.product_code(ii),{' '},fut_month);
                    KC_Compos_Data_Output.MTM_USD(ii)=num2cell(KC_COMPOS_SWAPS.mtm_usd(ii));
                    
                    if (KC_COMPOS_SWAPS.active_lots(ii) >= 0)
                        KC_Compos_Data_Output.LONG_LOTS_(ii) = num2cell( KC_COMPOS_SWAPS.active_lots(ii) );
                        KC_Compos_Data_Output.AVGLONGPRICE_BS_BAG_(ii) = num2cell( round(KC_COMPOS_SWAPS.traded_price(ii),6));
                    else
                        KC_Compos_Data_Output.SHORT_LOTS_(ii) = num2cell( KC_COMPOS_SWAPS.active_lots(ii) );
                        % KC_Compos_Data_Output.AVGSHORTPRICERS_BAG(ii) = num2cell((round( round(KC_COMPOS_SWAPS.traded_price(ii).*(Conversion_Data.conversion3(1)).*(0.6),6))),2);
                        KC_Compos_Data_Output.AVGSHORTPRICERS_BAG(ii) = num2cell((round( round(KC_COMPOS_SWAPS.traded_price(ii).*(Conversion_Data.conversion3(1)).*( Conversion_Data.conversion1(1)/100),6))),2);
                    end
                    KC_Compos_Data_Output.KCEDSPUSC_lb(ii)=num2cell(KC_COMPOS_SWAPS.underlying1_price(ii));
                    KC_Compos_Data_Output.USDBRLEDSP=num2cell(KC_COMPOS_SWAPS.underlying2_price);
                    KC_Compos_Data_Output.KCEDSPBRLC_lb(ii)=num2cell((KC_COMPOS_SWAPS.underlying1_price(ii).*KC_COMPOS_SWAPS.underlying2_price(ii)));
                    KC_Compos_Data_Output.KCEDSPRS_BAG(ii)=num2cell((KC_COMPOS_SWAPS.underlying1_price(ii).*KC_COMPOS_SWAPS.underlying2_price).*(Conversion_Data.conversion1(1)).*(Conversion_Data.conversion3(1).*(Conversion_Data.conversion2(1))));
                    
                    %% New Section addition
                    KC_Compos_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        KC_Compos_Data_Output.TRADE_DATE,'UniformOutput',0);
                    
                    KC_Compos_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(s,DB_Date_Format)),...
                        KC_Compos_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                    
                    KC_Compos_Data_Output.TRADE_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),KC_Compos_Data_Output.TRADE_DATE,'UniformOutput',0);
                    KC_Compos_Data_Output.EXPIRY_DATE = cellfun(@(s) (datestr(datenum(s,DB_Date_Format),Client_Statement_Format)),KC_Compos_Data_Output.EXPIRY_DATE,'UniformOutput',0);
                    KC_compos_swaps_output_CellArray = dataset2cell( KC_Compos_Data_Output);
                    KC_compos_swaps_output_CellArray (1,:)=[];
                    %% Filed declaration
                    UniqueFields_kc_compos_swaps={'TRADE_ID'};
                    SumFields_kc_compos={'MTM_USD'};
                    OutputFields_kc_compos= KC_Compo_Header;
                    WeightedAverageFields_FX_OPTIONS=[];
                    
                    %% Consolidate function output
                    [Consolidate_Header,Consolidate_DBData] = consolidatedata(KC_Compo_Header,KC_compos_swaps_output_CellArray, UniqueFields_kc_compos_swaps, SumFields_kc_compos, OutputFields_kc_compos , WeightedAverageFields_FX_OPTIONS);
                    TradeDate_Empty_Index = find(cell2mat(cellfun(@(s) (length(s<1)), Consolidate_DBData(:,TradeDate_Col_FX_OPTIONS),'UniformOutput',0))==1);
                    GetTradeID = Consolidate_DBData(TradeDate_Empty_Index, TRADE_ID_Col_KC_Compos);
                    
                    for c = 1 : length(GetTradeID)
                        Current_TradeID = GetTradeID(c);
                        Current_TradeID_Index  = TradeDate_Empty_Index(c);
                        Current_TradeID_ExpiryDate = KC_Compos_Data_Output.TRADE_DATE( strcmpi(KC_Compos_Data_Output.TRADE_ID,Current_TradeID));
                        Min_TradeDate = cellstr( datestr(min(datenum(Current_TradeID_ExpiryDate,DB_Date_Format)),DB_Date_Format) ) ;
                        
                        Consolidate_DBData(Current_TradeID_Index,TradeDate_Col_KC_Compos) = Min_TradeDate;
                        Consolidate_DBData(cell2mat(cellfun(@(x)any(isnan(x)),Consolidate_DBData,'UniformOutput',false))) = {[]};
                        
                        if (~isempty( Consolidate_DBData))
                            l=length(Consolidate_DBData);
                            Total_cell_creation=cell(2,l);
                            Total_cell_creation{1,1}='TOTAL';
                            %% Last Three Column sum implementation.
                            Total_cell_creation{1,end}=sum(cell2mat(Consolidate_DBData(:,end)));
                            Overall_Current_Client_Data = [Overall_Current_Client_Data ; [ Temp_Header ;  Temp_KC_Compos_Header ; KC_Compo_Header ;  Consolidate_DBData; Total_cell_creation ] ];
                            
                        end
                    end
                end
            end
            
            
            %             Index_OTC_ACCUMULATORS = find(strcmpi(Overall_Current_Client_Data(:,1),'OTC ACCUMULATORS'));
            %
            %             if ~isempty( Index_OTC_ACCUMULATORS)
            %
            %                 OTC_ACCUMULATORS_Data = Overall_Current_Client_Data(Index_OTC_ACCUMULATORS+1:end,:);
            %                 Index_TOTAL = find(strcmpi(OTC_ACCUMULATORS_Data(:,1),'TOTAL'));
            %                 Only_OTC_ACCUMULATORS_Data = OTC_ACCUMULATORS_Data(1:Index_TOTAL(1)-2,:);
            %
            %                 Index_LOTS_PER_DAY = find(strcmpi(Only_OTC_ACCUMULATORS_Data(1,:),'LOTS_PER_DAY'));
            %                 LOTS_PER_DAY_Data = Only_OTC_ACCUMULATORS_Data(2:end,Index_LOTS_PER_DAY);
            %                 LOTS_PER_DAY_Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric((x))),LOTS_PER_DAY_Data,'UniformOutput',false));
            %                 LOTS_PER_DAY_6digit_Value = cellfun(@(s) (sprintf('%.6f',abs((s)))), (LOTS_PER_DAY_Data(LOTS_PER_DAY_Numeric_Index)),'UniformOutput',0);
            %
            %             end
            
            %% Original Data fetching for OTC_FX_SWAPS
            %             OTC_FX_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC FX SWAPS'));
            %             if (~isempty(OTC_FX_SWAPS_INDEX))
            %                 TOTAL_INDEX_FX_SWAPS=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
            %                 SELL_PRICE_INDEX=find(strcmpi(Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+1,:),'SELL_PRICE'));
            %                 SETT_PRICE_INDEX=find(strcmpi(Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+1,:),'SETT_PRICE'));
            %                 BUY_PRICE_INDEX=find(strcmpi(Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+1,:),'BUY_PRICE'));
            %
            %
            %
            %                 %% TOTAL INDEXES WERE MULTIPLE TO RESOLVE THIS BELOW LINE IS ADDED
            %                 TOTAL_INDEX_USED=find(TOTAL_INDEX_FX_SWAPS>  OTC_FX_SWAPS_INDEX);
            %                 %% SETT_PRICE_ORIGINAL_VALUE
            %                 if (~isempty(  TOTAL_INDEX_FX_SWAPS(TOTAL_INDEX_USED(1))))
            %                     SELL_PRICE_ORIGINAL=Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:TOTAL_INDEX_FX_SWAPS(   TOTAL_INDEX_USED(1)),SELL_PRICE_INDEX);
            %                     SETT_PRICE_ORIGINAL=Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:TOTAL_INDEX_FX_SWAPS( TOTAL_INDEX_USED(1)),SETT_PRICE_INDEX);
            %                     BUY_PRICE_ORIGINAL=Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:TOTAL_INDEX_FX_SWAPS( TOTAL_INDEX_USED(1)),BUY_PRICE_INDEX);
            %                 end
            %
            %
            %                 %% Total index to be used
            %                 if (~isempty(TOTAL_INDEX_FX_SWAPS)) && (~isempty(TOTAL_INDEX_FX_SWAPS))
            %                     Total_index_required= find(TOTAL_INDEX_FX_SWAPS > OTC_FX_SWAPS_INDEX,1);
            %                     Total_index_used=  TOTAL_INDEX_FX_SWAPS(Total_index_required);
            %                 end
            %             end
            
            if(~isempty(Overall_Current_Client_Data))
                
                CurrVarname = strrep(Current_ClientCode,'-','_'); %% Current_ClientCode % Current_ClientName
                CurrVarname = strrep(CurrVarname,' ','_');
                CurrVarname = strrep(CurrVarname,'.','_');
                Current_Sheet_Name = char(CurrVarname);
                
                Overall_Client_Data.(Current_Sheet_Name) = Overall_Current_Client_Data;
                
                
                
                %% Need to fix the Decimal Number , because annoying format in When we create a PDF
                
                Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric(x)),Overall_Current_Client_Data,'UniformOutput',false));
                Zero_Index = cell2mat(cellfun(@(x)any(isnumericequal(x,0)),Overall_Current_Client_Data,'UniformOutput',false));
                
                
                Exact_Value = cellfun(@(s) (sprintf('%.6f',s)), Overall_Current_Client_Data(Numeric_Index),'UniformOutput',0);
                Exact_Value_1 = cellfun(@(s) (sprintf('%.2f',abs(s))), Overall_Current_Client_Data(Zero_Index),'UniformOutput',0); % Sometime it provides -0 thats what make it abs
                
                Temp_String = cell(size(Exact_Value_1,1),1); % For put hypen instead of Zero
                Temp_String(:,:) = {'-'};
                
                Overall_Current_Client_Data(Numeric_Index) = Exact_Value; % Put six Decimal Number
                Overall_Current_Client_Data(Zero_Index) = Exact_Value_1; % Put Zero
                Overall_Current_Client_Data(Zero_Index) = Temp_String; % Put hypen instead of Zero
                
                %% KC COMPOS FOUR DECIMAL VALUE MANAGEMEN
                %                 kc_compos_swaps_O=exist('KC_COMPOS_SWAPS');
                %                 if (  kc_compos_swaps_O==1)
                %                     if ~isempty(KC_COMPOS_SWAPS)
                %                         KC_COMPOS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'KC/USDBRL Compo SWAPS'));
                %                         TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                         USDBRL_EDSP=find(strcmpi(Overall_Current_Client_Data(KC_COMPOS_INDEX+1,:),'USDBRL EDSP'));
                %                         Data_USDBRL_EDSP=Overall_Current_Client_Data(KC_COMPOS_INDEX+2:TOTAL_INDEX(end)-2,USDBRL_EDSP);
                %                         values_empty_KC_compos=find(strcmpi(Overall_Current_Client_Data(1,:),''));
                %                         Original_value= KC_compos_swaps_output_CellArray(1:values_empty_KC_compos(1)-1,USDBRL_EDSP);
                %
                %                         Overall_Current_Client_Data(KC_COMPOS_INDEX+2:TOTAL_INDEX(end)-2,USDBRL_EDSP)=cellstr(num2str(cell2mat(Original_value)));
                %                     end
                %                 end
                
                
                
                %% LONG LOTS VALUE MANIPULATIONS IN SWAPS
                %                 OTC_Swaps_Data_O=exist('OTC_Swaps_Data');
                %                 if(   OTC_Swaps_Data_O==1)
                %                     if (~isempty(OTC_Swaps_Data))
                %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                %                         TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                         LONG_LOTS_COLUMN_INDEX=Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX,6);
                %                         for i1=1:length(LONG_LOTS_COLUMN_INDEX)
                %                             if  (LONG_LOTS_COLUMN_INDEX{i1}=='-')
                %                                 LONG_LOTS_COLUMN_INDEX{i1}=0;
                %                             end
                %                         end
                %                         LONG_LOTS_COLUMN_INDEX=OTC_Swaps_Data_Output(OTC_SWAPS_INDEX+1:end,6);
                %                         out=cellfun(@(x) num2str(x,15),  LONG_LOTS_COLUMN_INDEX,'un',0);
                %
                %                         %Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric(x)),    LONG_LOTS_COLUMN_INDEX,'UniformOutput',false));
                %
                %                         Overall_Current_Client_Data(3:TOTAL_INDEX(1),6)= out;
                %
                %                         %% SHORT LOTS VALUE MANIPULATION IN SWAPS
                %                         SHORT_LOTS_COLUMN_INDEX=Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX,8);
                %                         for i2=1:length( SHORT_LOTS_COLUMN_INDEX)
                %                             if  ( SHORT_LOTS_COLUMN_INDEX{i2}=='-')
                %                                 SHORT_LOTS_COLUMN_INDEX {i2}=0;
                %                             end
                %                         end
                %                         SHORT_LOTS_COLUMN_INDEX=OTC_Swaps_Data_Output(OTC_SWAPS_INDEX+1:end,8);
                %                         out1=cellfun(@(x) num2str(x,15),   SHORT_LOTS_COLUMN_INDEX ,'un',0);
                %                         Overall_Current_Client_Data(3:TOTAL_INDEX(1),8)=   out1 ;
                %                     end
                %                 end
                
                
                
                %% OTC OPTION TRADE PRICE FIX
                %                 OTC_Option_Data_check_o=exist('OTC_Option_Data');
                %                 if ( OTC_Option_Data_check_o==1)
                %                     if (~isempty(OTC_Option_Data))
                %                         OTC_OPTIONS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC OPTIONS'));
                %                         TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                         bb=find(TOTAL_INDEX>OTC_OPTIONS_INDEX,1);
                %                         TRADE_PRICE_COLUMN_INDEX=Overall_Current_Client_Data(OTC_OPTIONS_INDEX+2:TOTAL_INDEX(bb),14);
                %                         %  bb=find(TOTAL_INDEX>OTC_OPTIONS_INDEX,1);
                %                         for i3=1:length(TRADE_PRICE_COLUMN_INDEX)
                %                             if  ( TRADE_PRICE_COLUMN_INDEX{i3}=='-')
                %                                 TRADE_PRICE_COLUMN_INDEX {i3}=0;
                %                             end
                %                         end
                %                         TRADE_PRICE_COLUMN_INDEX=OTC_Option_Data_Output(2:end-1,14);
                %                         out_tp=cellfun(@(x) num2str(x,16), TRADE_PRICE_COLUMN_INDEX  ,'un',0);
                %                         Overall_Current_Client_Data(OTC_OPTIONS_INDEX+2:TOTAL_INDEX(bb)-1,14) = out_tp;
                %                     end
                %                 end
                
                
                a=exist('OTC_FX_Options_Data_Output');
                
                if (a==1)
                    if (~isempty(OTC_FX_Options_Data_Output))
                        
                        %% Value date wise sorting fix.
                        
                        %% Please refer this function for fix...EMS Sorting fix.
                        [~,sortindex]=sort(datenum(OTC_FX_Options_Data_Output.VALUE_DATE));
                        OTC_FX_Options_Data_Output=OTC_FX_Options_Data_Output(sortindex,:);
                        %
                        OTC_FX_OPTIONS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC FX OPTIONS'));
                        if (~isempty(  OTC_FX_OPTIONS_INDEX))
                            TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                            find_required_total=find(TOTAL_INDEX> OTC_FX_OPTIONS_INDEX,1);
                            values_empty_KC_compos=find(strcmpi(Overall_Current_Client_Data(1,:),''));
                            %% Bugprone.
                            %                             % added rajashekhar
                            req_data=dataset2cell(OTC_FX_Options_Data_Output);
                            
                            Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric(x)),req_data,'UniformOutput',false));
                            Zero_Index = cell2mat(cellfun(@(x)any(isnumericequal(x,0)),req_data,'UniformOutput',false));
                            
                            Exact_Value = cellfun(@(s) (sprintf('%.6f',s)), req_data(Numeric_Index),'UniformOutput',0);
                            Exact_Value_1 = cellfun(@(s) (sprintf('%.2f',abs(s))), req_data(Zero_Index),'UniformOutput',0); % Sometime it provides -0 thats what make it abs
                            
                            Temp_String = cell(size(Exact_Value_1,1),1); % For put hypen instead of Zero
                            Temp_String(:,:) = {'-'};
                            
                            req_data(Numeric_Index) = Exact_Value; % Put Two Decimal Number
                            req_data(Zero_Index) = Exact_Value_1; % Put Zero
                            req_data(Zero_Index) = Temp_String; % Put hypen instead of Zero
                            Overall_Current_Client_Data( OTC_FX_OPTIONS_INDEX+2:TOTAL_INDEX(find_required_total)-1,:)=req_data(2:end,:);
                            %                             % end
                            OTC_FX_Options_Data_Output_sorted_ca=dataset2cell(OTC_FX_Options_Data_Output);
                            OTC_FX_Options_Data_Output_sorted_ca(1,:)=[];
                            %% before assigning dataset convert it into cell.
                            OTC_FX_Options_Data_Output_CellArray=  OTC_FX_Options_Data_Output_sorted_ca;
                            Original_value= OTC_FX_Options_Data_Output_CellArray(1:end,12);
                            trade_price=OTC_FX_OPTIONS(1:end,26);
                            trade_price_cell=dataset2cell(trade_price);
                            trade_price_cell(1,:)=[];
                            trade_price_original=cell2mat(trade_price_cell).*100;
                            Overall_Current_Client_Data( OTC_FX_OPTIONS_INDEX+2:TOTAL_INDEX(find_required_total)-1,12)=cellstr(num2str(cell2mat(Original_value)));
                            Overall_Current_Client_Data( OTC_FX_OPTIONS_INDEX+2:TOTAL_INDEX(find_required_total)-1,15)=cellstr(num2str(trade_price_original,12));
                            Overall_Current_Client_Data(OTC_FX_OPTIONS_INDEX+2:TOTAL_INDEX(find_required_total)-1,4)=  OTC_FX_Options_Data_Output_CellArray(1:end,4); %% Here 4 refers to value date.
                        end
                        
                    end
                end
                
                %% Original value Manipulation with all the filed names
                %                 OTC_Swaps_Data_Output_Check=exist('OTC_Swaps_Data_Output');
                %                 if  (OTC_Swaps_Data_Output_Check==1)
                %                     if (~isempty(OTC_Swaps_Data_Output))
                %
                %                         %% LONG LOTS MANIPULATIONS
                %                         long_lots_index=find(strcmpi(OTC_Swaps_Data_Output(1,:),'LONG_LOTS'));
                %                         TOtal_index=find(strcmpi(OTC_Swaps_Data_Output(:,1),'TOTAL'));
                %                         LONG_LOTS_ORIGINAL_DATA=OTC_Swaps_Data_Output(2:TOtal_index, long_lots_index);
                %
                %                         %% Overall Client data array fetching to replace
                %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                %                         if (~isempty(   OTC_SWAPS_INDEX))
                %                             TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                             LONG_LOTS_REPLACED_DATA=find(strcmpi(Overall_Current_Client_Data(2,:),'LONG_LOTS'));
                %
                %                             %% REquired format change
                %                             LONG_LOTS_FINAL=cellfun(@(x) num2str(x,15),LONG_LOTS_ORIGINAL_DATA,'UniformOutput', false);
                %
                %                             Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX(1), LONG_LOTS_REPLACED_DATA)=  LONG_LOTS_FINAL;
                %                         end
                %
                %
                %                         %% AVG_LONG_PRICE Manipulation
                %
                %                         Avg_long_index=find(strcmpi(OTC_Swaps_Data_Output(1,:),'AVG_LONG_PRICE'));
                %                         TOtal_index=find(strcmpi(OTC_Swaps_Data_Output(:,1),'TOTAL'));
                %                         AVG_LONG_ORIGINAL_DATA=OTC_Swaps_Data_Output(2:TOtal_index,  Avg_long_index );
                %
                %                         %% Overall Client data array fetching to replace
                %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                %                         if (~isempty(   OTC_SWAPS_INDEX))
                %                             TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                             AVG_LONG_REPLACED_DATA=find(strcmpi(Overall_Current_Client_Data(2,:),'AVG_LONG_PRICE'));
                %                             AVG_LONG_FINAL=cellfun(@(x) num2str(x,15), AVG_LONG_ORIGINAL_DATA,'UniformOutput', false);
                %                             Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX(1),  AVG_LONG_REPLACED_DATA )=   AVG_LONG_FINAL;
                %                         end
                %
                %
                %                         %% Short lots manipulation
                %                         short_lots_index=find(strcmpi(OTC_Swaps_Data_Output(1,:),'SHORT_LOTS'));
                %                         TOtal_index=find(strcmpi(OTC_Swaps_Data_Output(:,1),'TOTAL'));
                %                         SHORT_LOTS_ORIGINAL_DATA=OTC_Swaps_Data_Output(2:TOtal_index,short_lots_index );
                %
                %                         %% Overall Client data array fetching to replace
                %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                %                         if (~isempty(   OTC_SWAPS_INDEX))
                %                             TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                             SHORT_LOTS_REPLACED_DATA=find(strcmpi(Overall_Current_Client_Data(2,:),'SHORT_LOTS'));
                %                             SHORT_LOTS_FINAL=cellfun(@(x) num2str(x,15),SHORT_LOTS_ORIGINAL_DATA,'UniformOutput', false);
                %                             Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX(1),SHORT_LOTS_REPLACED_DATA )=    SHORT_LOTS_FINAL;
                %                         end
                %
                %
                %                         %% Avg short
                %                         Avg_short_index=find(strcmpi(OTC_Swaps_Data_Output(1,:),'AVG_SHORT_PRICE'));
                %                         TOtal_index=find(strcmpi(OTC_Swaps_Data_Output(:,1),'TOTAL'));
                %                         AVG_SHORT_ORIGINAL_DATA=OTC_Swaps_Data_Output(2:TOtal_index,    Avg_short_index );
                %
                %                         %% Overall Client data array fetching to replace
                %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                %                         if (~isempty(   OTC_SWAPS_INDEX))
                %                             TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                %                             AVG_SHORT_REPLACED_DATA=find(strcmpi(Overall_Current_Client_Data(2,:),'AVG_SHORT_PRICE'));
                %                             AVG_SHORT_FINAL=cellfun(@(x) num2str(x,15),  AVG_SHORT_ORIGINAL_DATA,'UniformOutput', false);
                %                             Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX(1),  AVG_SHORT_REPLACED_DATA )=   AVG_SHORT_FINAL;
                %                         end
                %
                %
                %                         EDSP_index=find(strcmpi(OTC_Swaps_Data_Output(1,:),'EDSP'));
                %                         TOtal_index=find(strcmpi(OTC_Swaps_Data_Output(:,1),'TOTAL'));
                %                         EDSP_ORIGINAL_DATA=OTC_Swaps_Data_Output(2:TOtal_index,   EDSP_index );
                %
                %                         %% Overall Client data array fetching to replace
                % %                         OTC_SWAPS_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'OTC SWAPS'));
                % %                         if (~isempty(   OTC_SWAPS_INDEX))
                % %                             TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data(:,1),'TOTAL'));
                % %                             EDSP_REPLACED_DATA=find(strcmpi(Overall_Current_Client_Data(2,:),'EDSP'));
                % %                             EDSP_FINAL=cellfun(@(x) num2str(x,15),   EDSP_ORIGINAL_DATA ,'UniformOutput', false);
                % %                             Overall_Current_Client_Data(OTC_SWAPS_INDEX+2:TOTAL_INDEX(1),EDSP_REPLACED_DATA )= EDSP_FINAL  ;
                % %                         end
                %
                %                     end
                %                 end
                
                
                %% Data Replacement for OTC FX SWAP
                %                 OTC_FXSwaps_variable_exist=exist('OTC_FXSwaps_Data_Output');
                %
                %                 if    OTC_FXSwaps_variable_exist==1
                %
                %                     SETT_PRICE_ORIGINAL_USED=cellfun(@num2str,  SETT_PRICE_ORIGINAL,'un',0);
                %                     %
                %                     SELL_PRICE_ORIGINAL_USED=  cellfun(@(x) num2str(x,'%0.4f'), SELL_PRICE_ORIGINAL,'UniformOutput',false ) ;
                %                     BUY_PRICE_ORIGINAL_USED=cellfun(@num2str,  BUY_PRICE_ORIGINAL,'un',0);
                %                     if (~isempty(OTC_FX_SWAPS_INDEX))
                %                         Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:Total_index_used,SETT_PRICE_INDEX)=SETT_PRICE_ORIGINAL_USED;
                %                         Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:Total_index_used,SELL_PRICE_INDEX)=SELL_PRICE_ORIGINAL_USED;
                %                         %                     Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:Total_index_used,SELL_PRICE_INDEX)=
                %                         Overall_Current_Client_Data(OTC_FX_SWAPS_INDEX+2:Total_index_used,BUY_PRICE_INDEX)=BUY_PRICE_ORIGINAL_USED;
                %                     end
                %                 end
                
                
                %% TOTAL ASSIGNMENT MANIPULATION plese manage
                TOTAL_INDEX=find(strcmpi('TOTAL',  Overall_Current_Client_Data(:,1)));
                TOTAL_VALUE=sum(str2double(Overall_Current_Client_Data(TOTAL_INDEX,end)));
                TOTAL_VALUE=sprintf('%0.2f',TOTAL_VALUE);
                %TOTAL_VALUE=str2double(Overall_Current_Client_Data(TOTAL_INDEX(end),end));
                
                %% SIP management grand total
                SIP_CHECK=strcmpi(Unique_ClientNames,'sip');
                SIP_CHECK_ONE=any(SIP_CHECK);
                tf=isnan(str2double(TOTAL_VALUE));
                if (tf==1 && SIP_CHECK_ONE==1)
                    %if (isnan(TOTAL_VALUE) && (SIP_CHECK==1))
                    
                    TOTAL_VALUE=nansum(str2double(Overall_Current_Client_Data(3:end,end)));
                    TOTAL_VALUE=sprintf('%0.2f',TOTAL_VALUE);
                    %                     fprintf('%.2f',round(TOTAL_VALUE))
                    
                end
                
                
                %% Nan Value Mangement
                if isnan(TOTAL_VALUE)
                    
                    TOTAL_VALUE=str2double(Overall_Current_Client_Data((TOTAL_INDEX(end-1)),end));
                    %                       TOTAL_VALUE=str2double(Overall_Current_Client_Data((TOTAL_INDEX(end)),end));
                    if isnan(TOTAL_VALUE)
                        TOTAL_VALUE=str2double(Overall_Current_Client_Data((TOTAL_INDEX(end)),end));
                    end
                    
                end
                
                %% Six decimal appension in accumulators section
                
                %                 if ~isempty( Index_OTC_ACCUMULATORS)
                %                     Overall_Current_Client_Data((Index_OTC_ACCUMULATORS+2):(Index_OTC_ACCUMULATORS+2+(Index_TOTAL-4)), Index_LOTS_PER_DAY)=LOTS_PER_DAY_6digit_Value;
                %                 end
                
                xlswrite(OutXLSName,Overall_Current_Client_Data,Current_Sheet_Name);
                
                %% Getting Client Address
                
                Current_Client_Name =  ClientInformation_Table.clients_legal_name(strcmpi(ClientInformation_Table.client_code,Current_ClientCode));
                Current_Client_AccountNo = ClientInformation_Table.account_no(strcmpi(ClientInformation_Table.client_code,Current_ClientCode));
                Current_Client_Address = ClientInformation_Table.address(strcmpi(ClientInformation_Table.client_code,Current_ClientCode));
                
                Default_Str = 'OLAM CFSG | RISK MANAGEMENT SOLUTIONS | DAILY STATEMENT';
                Default_Str = strcat({'                                   '},Default_Str,{'                                   '});
                
                Current_Client_Info = cell(7,Fixed_Header_Col_Size);
                Current_Client_Info(1,ceil(Fixed_Header_Col_Size/2)) = Default_Str;
                Current_Client_Info(3,1) = cellstr('CONFIRMATION DATE:');
                Current_Client_Info(4,1) = cellstr('CLIENT NAME');
                Current_Client_Info(5,1) = cellstr('ACCOUNT NO.');
                Current_Client_Info(6,1) = cellstr('ADDRESS');
                
                Current_Client_Info(3,2) = cellstr(datestr(SettleDate,'dd-mmm-yy'));
                if isempty(Current_Client_Name)
                    Current_Client_Name = {'Clients Legal Name not available!'};
                end
                Current_Client_Info(4,2) = Current_Client_Name;
                
                if isempty(Current_Client_AccountNo)
                    Current_Client_AccountNo = {'Clients Account No not available!'};
                end
                Current_Client_Info(5,2) = Current_Client_AccountNo;
                
                if isempty(Current_Client_Address)
                    Current_Client_Address = {'Clients Address information not available!'};
                end
                Current_Client_Info(6,2) = Current_Client_Address;
                
                %% Empty Cell Creation Of specified Size in order to concatenate it with Client Info  (New Edited Section)
                Cash_Statement_Info=cell(11,21);
                
                %% Initialise the positions for Cash Balance information
                PosGrandTotal = 1;
                PosCashOpenBalance = 2;
                PosExpiredCash = 3;
                PosPremiumRecd = 4;
                poscommission=5;
                postransferout=6;
                postransferin=7;
                posinterestcharges=8;
                poscashclosingbalance=9;
                posaccountbalance=10;
                posthreshold=11;
                posmarginexcess=12;
                
                %% Cash balance field creation.
                Cash_Statement_Info{PosGrandTotal,1}='GRAND TOTAL';
                Cash_Statement_Info{PosCashOpenBalance,1}='CASH OPENING BALANCE';
                Cash_Statement_Info{PosExpiredCash,1}='EXPIRED/LIQUIDATED REALIZED CASH';
                Cash_Statement_Info{PosPremiumRecd,1}='PREMIUM PAID/RECEIVED';
                Cash_Statement_Info{poscommission,1}='COMMISSIONS';
                Cash_Statement_Info{postransferout,1}='TRANSFER OUT';
                Cash_Statement_Info{postransferin,1}='TRANSFER IN';
                Cash_Statement_Info{posinterestcharges,1}='INTEREST CHARGES';
                Cash_Statement_Info{poscashclosingbalance,1}='CASH CLOSING BALANCE';
                Cash_Statement_Info{posaccountbalance,1}='ACCOUNT BALANCE';
                Cash_Statement_Info{posthreshold,1}='THRESHOLD';
                Cash_Statement_Info{posmarginexcess,1}='MARGIN EXCESS/(DEFICIT)';
                
                %% To display total everytime independent on if condition
                %Cash_Statement_Info{PosGrandTotal,6}=Overall_Current_Client_Data{end};
                if isnumeric(TOTAL_VALUE)
                    TOTAL_VALUE=num2str(TOTAL_VALUE);
                end
                
                Cash_Statement_Info{PosGrandTotal,6}=TOTAL_VALUE;
                %% Threshold Query
                CTM_Table_Name = ['client_threshold_master_table_',char(InBUName)];
                Sql_Query = ['select * from ',char(CTM_Table_Name),' where client_code = ''',char(Unique_ClientNames(i)),''' '];
                [~, CTM_Table_Data]=Fetch_DB_Data(ObjDB,Sql_Query);
                
                %% Data Fetching for above fields
                %   SqlQuery=['select * from client_cash_account_balance_table_cfs where cob_date =''',char(ClientStatement_View_Data.settlement_date(1)),''' and client_code = ''',char(varargin),''' and currency = ''USD'''];
                %SqlQuery=['select * from client_cash_account_balance_table_cfs where cob_date =''',char(ClientStatement_View_Data.settlement_date(1)),''' and client_code = ''',char(varargin),''''];
                SqlQuery=['select * from client_cash_account_balance_table_cfs where cob_date =''',char(ClientStatement_View_Data.settlement_date(1)),''' and client_code = ''',char( Unique_ClientNames(i)),'''  and currency like ''USD%'''];
                [Headers,Data] = Fetch_DB_Data(ObjDB, SqlQuery);
                if strcmpi(Headers,'Error Data')
                    OutErrorMsg={'No Data in Client cash balance table cfs for particular cob date and client',char( Unique_ClientNames(i))};
                end
                
                if strcmpi(Data,'No Data')
                    Current_Client_Info  = [Current_Client_Info ; Overall_Current_Client_Data;  Cash_Statement_Info];
                    GRAND_TOTAL_INDEX=find(strcmpi(Current_Client_Info(:,1),'GRAND TOTAL'));
                    [sz_gt]=size( Current_Client_Info);
                    cb_data_fetch= Current_Client_Info(GRAND_TOTAL_INDEX:sz_gt(1),6);
                    cb_data_fetch_string= cellfun(@(x) char(x),   cb_data_fetch,'UniformOutput',false);
                    
                    
                    %% Replace the data
                    Current_Client_Info(GRAND_TOTAL_INDEX:sz_gt(1),6)= cb_data_fetch_string;
                    
                else
                    Overall_Data=[Headers;Data];
                    Overall_Fetched_Data=cell2dataset(Overall_Data);
                    
                    %% dynamic data appension
                    
                    Cash_Statement_Info{PosGrandTotal,6}=TOTAL_VALUE;
                    Cash_Statement_Info{PosCashOpenBalance,6}=num2str(Overall_Fetched_Data.opening_balance);
                    Cash_Statement_Info{PosExpiredCash,6}=num2str(Overall_Fetched_Data.expired_or_liquidated_cash);
                    Cash_Statement_Info{PosPremiumRecd,6}=num2str(Overall_Fetched_Data.premium_paid_or_recd);
                    Cash_Statement_Info{poscommission,6}=num2str(Overall_Fetched_Data.commission);
                    Cash_Statement_Info{postransferout,6}=num2str(Overall_Fetched_Data.transfers_out);
                    Cash_Statement_Info{postransferin,6}=num2str(Overall_Fetched_Data.transfers_in);
                    Cash_Statement_Info{posinterestcharges,6}=num2str(Overall_Fetched_Data.interest_charges);
                    Cash_Statement_Info{poscashclosingbalance,6}=num2str(Overall_Fetched_Data.closing_balance);
                    Cash_Statement_Info{posaccountbalance,6}=num2str(str2double(TOTAL_VALUE)+Overall_Fetched_Data.closing_balance);
                    Cash_Statement_Info{posthreshold,6}=num2str(cell2mat(CTM_Table_Data(end)));
                    %Cash_Statement_Info{posmarginexcess,6}=num2str(cell2mat(CTM_Table_Data(end))+  Cash_Statement_Info{posaccountbalance,6});
                    Cash_Statement_Info{posmarginexcess,6}=num2str(cell2mat(CTM_Table_Data(end))+ str2double(TOTAL_VALUE)+Overall_Fetched_Data.closing_balance);
                    % TO populate all the values in cashbalance info to 2
                    % decimals
                    Numeric_Index = cell2mat(cellfun(@(x)any(isnumeric(x)),Cash_Statement_Info,'UniformOutput',false));
                    Exact_Value = cellfun(@(s) (sprintf('%.2f',s)), Cash_Statement_Info(Numeric_Index),'UniformOutput',0);
                    Cash_Statement_Info(Numeric_Index) = Exact_Value;
                    Cash_Statement_Info_string=cellfun(@(x) char(x),   Cash_Statement_Info,'UniformOutput',false);
                    
                    Cash_Statement_Info(:,6)= Cash_Statement_Info_string;
                    
                    %% Empty Data cells for Concatenation of Data
                    
                    Current_Client_Info  = [Current_Client_Info ; Overall_Current_Client_Data;  Cash_Statement_Info]; % will do horizontal concatenation like
                end
                
                % Check and set the flag to decide if mtm_nc column to be
                % included or not
                UniqCurrency = unique(Current_ClientName_Data.currency);
                
                if any(~strcmpi(UniqCurrency,'USD'))
                    % If any non USD products like GBP,BRL are traded, we
                    % need to include the column mtm_nc
                    IncludeMtmNC = 1;
                    
                else
                    IncludeMtmNC = 0;
                    
                end
                
                %% PDF Generator
                
                CurrentDateTime = strsplit(datestr(now),' ');
                Time = CurrentDateTime{2};
                
                TempName = [char(Current_Sheet_Name),'_',DBValueDate,'_',Time];
                TempName = strrep(TempName,':','-');
                TempName = strrep(TempName,' ','_');
                
                % create the individual xl file containing the same data output
                XLSReport = [TempName,'.xlsx'];
                xlswrite(XLSReport,Overall_Current_Client_Data);
                
                try
                    [OutErrorMsg,PDFReport] = generate_client_statement_pdf_report(Current_Client_Info,TempName,IncludeMtmNC);
                catch ME
                    OutErrorMsg = cellstr(ME.message);
                end
                
                %% isdeployed ,Development purpose make it '0' , if it JAR file make it '1'
                
                if isdeployed
                    try
                        DestinationFolder = [TempDestFolder,'\',Current_ClientCode];
                        % check if the folder exists before moving the reports
                        D = dir(DestinationFolder);
                        if length(D) == 0
                            [SUCCESS,MESSAGE] = mkdir(TempDestFolder,Current_ClientCode);
                            if SUCCESS == 1
                                mkdir(DestinationFolder,'EXLS');
                                mkdir(DestinationFolder,'PDF');
                            else
                                write_log_file(LogFilename,['Client Folder Creation ',MESSAGE,DestinationFolder]);
                            end
                        end
                        %       move_reports(SourceFile,DestinationFile,LogTitleMessage,LogFilename);
                        move_reports(PDFReport,[DestinationFolder,'\PDF'] ,[datestr(now),' : ',Current_ClientCode,' : Moving PDF Report to Static Folder'],LogFilename);
                        move_reports(XLSReport,[DestinationFolder,'\EXLS'],[datestr(now),' : ',Current_ClientCode,' : Moving XLS Report to Static Folder'],LogFilename);
                    catch ME
                        write_log_file(LogFilename,['Client Folder Creation Error ',ME.message,DestinationFolder]);
                        OutErrorMsg = cellstr(ME.message);
                    end
                    
                end
                [~,PDFFile]  =fileparts(PDFReport);
                PDFReport = [PDFFile,'.pdf'];
                OutPDFReport = [OutPDFReport,PDFReport];
                OutXLSReport = [OutXLSReport,XLSReport];
                
            else
                
                OutMissingClients = [OutMissingClients,Current_ClientCode];
            end
            
            Overall_Current_Client_Data = []; %% for Next Iteration
        catch ME
            OutMissingClients = [OutMissingClients,Current_ClientCode];
            Overall_Current_Client_Data = [];
            write_log_file(LogFilename,['Client Statement Generation error in ',Current_ClientCode,'. Error is :',ME.message]);
            continue;
        end
        
    end
    if isempty(Overall_Client_Data)
        OutErrorMsg = {'No Data found for the selected Clients!'};
    else
        OutPDFReport = cellstr(convertCell2Char(OutPDFReport));
        OutXLSReport = cellstr(convertCell2Char(OutXLSReport));
    end
    try
        xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message); %% Here we are accessing message properties of ME object.
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end