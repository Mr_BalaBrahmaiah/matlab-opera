function [OutErrorMsg,OutXLSName,OutLogMsg_DVD,OutLogMsg_Shares] = generate_qf5_corporate_action_shares(InBUName,Temp_Settle_Date)
 
%%% [OutErrorMsg,OutXLSName,OutLogMsg_DVD,OutLogMsg_Shares] = generate_qf5_corporate_action_shares('qf5','2018-12-27')

 try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename('corporate_action_shares');  % output file name
    
    %%% for reading sheets.
    OutLogMsg_DVD = {'No Errors'};
    OutLogMsg_Shares = {'No Errors'};
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = Temp_Settle_Date;
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    %%% DVD_PRICE Table   
    DVD_PRICE_Table_Name = strcat('dividend_price_table_',InBUName);
    SqlQuery_DVD_PRICE_Table = ['select * from ',char(DVD_PRICE_Table_Name),' where ex_date = ''',char(Settele_date),''' '] ;
    [DVD_PRICE_ColNames,DVD_PRICE_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_PRICE_Table);

    if strcmpi(DVD_PRICE_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(DVD_PRICE_Table_Name), ' for ',char(Settele_date)]} ;
        return;
    end

    %%%  Stock_Type_Master Table
    DVD_Type_Master_Table_Name = strcat('dividend_stock_type_master_table_',InBUName);
    SqlQuery_stock_Type = ['select * from ',char(DVD_Type_Master_Table_Name),] ;
    Stock_Type_all = fetch(ObjDB,SqlQuery_stock_Type);

    if(isempty(Stock_Type_all))
        OutErrorMsg = {['No Data found in ',char(DVD_Type_Master_Table_Name),' for ',char(Settele_date)]} ;
        return;
    end
    
    %%% finding the new stock_types
    [r1,~]=size(DVD_PRICE_Table_Data);
    Missing_Stock_type = []; DVD_action_type = [];
    for jj = 1:1:r1
        DVD_Stock_type = DVD_PRICE_Table_Data(jj,9);
        Index_1 = strcmpi(DVD_Stock_type,Stock_Type_all(:,1));
        if ~any(Index_1)
            Missing_Stock_type = [Missing_Stock_type DVD_Stock_type];
            continue;
        end
        action_type = Stock_Type_all(Index_1,2);
        DVD_action_type = [DVD_action_type;action_type];
    end
    Missing_Stock_type = convertCell2Char(Missing_Stock_type);

    DVD_PRICE_Table_Data = cell2dataset([DVD_PRICE_ColNames;DVD_PRICE_Table_Data]);
    DVD_PRICE_Table_Data.action = DVD_action_type; 
    %%% Delete upadte_by and update_timestamp columns
    DVD_PRICE_Table_Data.updated_by = [];         
    DVD_PRICE_Table_Data.updated_timestamp = [];
    
    if ~isempty(Missing_Stock_type)
        OutErrorMsg = {['Stock Type ',Missing_Stock_type,' not available in Master Table']};
        return;
    end

    %%% Delete Data where action = ''','ignore','''
    SqlQuery_stock_Type = ['select stock_type from ',char(DVD_Type_Master_Table_Name),' where action = ''','ignore',''' '] ;
    Stock_Type_ignore = fetch(ObjDB,SqlQuery_stock_Type);

    [r,~]=size(Stock_Type_ignore);
    for i = 1:1:r
        Index_No = strcmpi(Stock_Type_ignore(i,1),DVD_PRICE_Table_Data.stock_type);
        DVD_PRICE_Table_Data(Index_No,:) = [];
    end
    % Both dividend and shares data
    %xlswrite(OutXLSName,dataset2cell(DVD_PRICE_Table_Data),'Filter_Sheet');

    %%%% Delete Data where action = ''','shares','''
    SqlQuery_stock_Type = ['select stock_type from ',char(DVD_Type_Master_Table_Name),' where action = ''','shares',''' '] ;
    Stock_Type_shares = fetch(ObjDB,SqlQuery_stock_Type);

    [r,~]=size(Stock_Type_shares);
    DVD_PRICE_Dividend_Table_Data = DVD_PRICE_Table_Data;
    for i = 1:1:r
        Index_No = strcmpi(Stock_Type_shares(i,1),DVD_PRICE_Dividend_Table_Data.stock_type);
        DVD_PRICE_Dividend_Table_Data(Index_No,:) = [];
    end

    %%%% Delete Data where action = ''','dividend','''
    SqlQuery_stock_Type = ['select stock_type from ',char(DVD_Type_Master_Table_Name),' where action = ''','dividend',''' '] ;
    Stock_Type_dividend = fetch(ObjDB,SqlQuery_stock_Type);

    [r,~]=size(Stock_Type_dividend);
    DVD_PRICE_Shares_Table_Data = DVD_PRICE_Table_Data;
    for i = 1:1:r
        Index_No = strcmpi(Stock_Type_dividend(i,1),DVD_PRICE_Shares_Table_Data.stock_type);
        DVD_PRICE_Shares_Table_Data(Index_No,:) = [];
    end

    %%% Vdate_Sdate Table
    %%%% find the EX_date-1 below query
    Table_Name = 'vdate_sdate_table';
    SqlQuery_Ex_Date = ['select settlement_date from ',char(Table_Name),' where value_date = ''',char(Settele_date),''' '] ;
    settlement_date = fetch(ObjDB,SqlQuery_Ex_Date);

    %%% Unrealized_Report_Store Table
    %%%% To read the open lots from EX_date -1
    Unrealized_Report_Table_Name = strcat('unrealized_report_store_table_',InBUName);
    SqlQuery_Unrealized_Report = ['select * from ',char(Unrealized_Report_Table_Name),' where settle_date = ''',char(settlement_date),''' '] ;
    [Unrealized_Report_ColNames,Unrealized_Report_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Unrealized_Report);

    if strcmpi(Unrealized_Report_Table_Data,'No Data')
        OutErrorMsg = {['Data not Available in ',char(Unrealized_Report_Table_Name), ' for ',char(settlement_date)]}; 
        return;
    end

    %%%     Dividend Evaluation Process

    if isempty(DVD_PRICE_Dividend_Table_Data)
        OutLogMsg_DVD = {['INFO:NO DIVIDEND DATA for EX_DATE ',char(Settele_date),]}; 
        xlswrite(OutXLSName,OutLogMsg_DVD,'DVD_FTD_OUTPUT');
    else   
        DVD_PRICE_Dividend_Table_Data=dataset2cell(DVD_PRICE_Dividend_Table_Data); % only dividend data
        
        DVD_PRICE_ColNames = DVD_PRICE_Dividend_Table_Data(1,:);
        DVD_PRICE_Dividend_Table_Data = DVD_PRICE_Dividend_Table_Data(2:end,:);
        DVD_PRICE_Dividend_Table_Data=[DVD_PRICE_ColNames;DVD_PRICE_Dividend_Table_Data]; % only dividend data
        
        %%%% find the Open lots (Open_qty) and Counter party 
        DVD_Data_Group=[]; %OutLogMsg_DVD = [];
        [rr,~]=size(DVD_PRICE_Dividend_Table_Data);
        for j=2:1:rr
            field_bbg_product_code=DVD_PRICE_Dividend_Table_Data(j,2); 
            Indx=find(strcmpi(field_bbg_product_code,Unrealized_Report_Table_Data(:,4)));

            Unrealized_Report_Matched_Data = Unrealized_Report_Table_Data(Indx,:);
            if ~isempty(Unrealized_Report_Matched_Data)
                Unique_Group_Data=DVD_PRICE_Dividend_Table_Data(j,:);
                Unique_Group_Data = repmat(Unique_Group_Data,size(Indx));
                Raw_pro_Matched_Data=[Unrealized_Report_Matched_Data(:,3) Unrealized_Report_Matched_Data(:,5) Unique_Group_Data Unrealized_Report_Matched_Data(:,7)];
                DVD_Data_Group=vertcat(DVD_Data_Group,Raw_pro_Matched_Data);
            end   
        end

        if isempty(DVD_Data_Group)
            OutLogMsg_DVD = {'INFO:No Open Lots available for the corresponding stock_ids!'} ;
            xlswrite(OutXLSName,OutLogMsg_DVD,'DVD_FTD_OUTPUT');
        else
            Master_DVD_Header=[Unrealized_Report_ColNames(3) Unrealized_Report_ColNames(5) DVD_PRICE_ColNames Unrealized_Report_ColNames(7)];
            Master_DVD_Data = [Master_DVD_Header ; DVD_Data_Group];
           
            %%% calculate the DVD_Nc
            Master_DVD_Data = cell2dataset(Master_DVD_Data);

            %%% consolidation part
            Master_DVD_Data = dataset2cell(Master_DVD_Data);
            Consolidate_DVD =Master_DVD_Data;
            Consolidate_DVD_Header =  Consolidate_DVD(1,:);
            Consolidate_DVD_Data = Consolidate_DVD(2:end,:);

            UniqueFields = {'counterparty_parent','instrument','bbg_product_code','stock_type'}; 
            SumFields = {'open_qty'};
            OutputFields = Consolidate_DVD_Header;
            WeightedAverageFields = [];
            [Consolidate_DVD_Data_Header,Shares_DVD_Data_Consolidate] = consolidatedata(Consolidate_DVD_Header, Consolidate_DVD_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

            Master_DVD_Data = [Consolidate_DVD_Data_Header;Shares_DVD_Data_Consolidate];
            Master_DVD_Data = cell2dataset(Master_DVD_Data);
            Master_DVD_Data.dvd_Nc = Master_DVD_Data.amount .* Master_DVD_Data.open_qty;  % DVD_Nc
            %%% DVD_TAX Table
            Tax_Rate_Table_Name = strcat('dividend_tax_table_',InBUName);
            SqlQuery_Tax_Rate = ['select * from ',char(Tax_Rate_Table_Name),' where ex_date = ''',char(Settele_date),''' '] ;
            [~,Tax_Rate_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Tax_Rate);

            if strcmpi(Tax_Rate_Table_Data,'No Data')
                OutErrorMsg = {['No Data found in ' ,char(Tax_Rate_Table_Name),' for ',char(Settele_date)]};
                return;
            end
            %%% Get the Tax_Rate values
            Tax_bbg_product_code = Tax_Rate_Table_Data(:,1);
            Tax_counterparty_parent = Tax_Rate_Table_Data(:,2);
            Tax_ex_date = Tax_Rate_Table_Data(:,4);

            %%%% DVD_FX_RATE
            DVD_FX_Rate_Table_Name = strcat('dividend_fx_rate_table_',InBUName);
            SqlQuery_DVD_FX_Rate = ['select * from ',char(DVD_FX_Rate_Table_Name),' where ex_date = ''',char(Settele_date),''' '] ;
            [~,DVD_FX_Rate_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_FX_Rate);

            if strcmpi(DVD_FX_Rate_Table_Data,'No Data')
                OutErrorMsg = {['No Data found in ' ,char(DVD_FX_Rate_Table_Name),' for ',char(Settele_date)]};
                return;
            end

            %%%% Get the ex_fx_rate and payable_fx_rate values
            FXR_bbg_product_code = DVD_FX_Rate_Table_Data(:,1);
            FXR_counterparty_parent = DVD_FX_Rate_Table_Data(:,2);
            FXR_ex_date = DVD_FX_Rate_Table_Data(:,4);
            FXR_stock_type = DVD_FX_Rate_Table_Data(:,8);

            %%% calculate Net_DVD_Nc value using Tax_Rate
            [row,~]=size(Master_DVD_Data);
            tax_rate_percen = []; ex_fx_rate_val = []; payable_fx_rate_val = []; Net_DVD_NC_val = [];
            Missing_Tax_Rate = []; Missing_EX_Rate = []; Fx_payable_date = [];
            for ii = 1:1:row
                bbg_product_code = Master_DVD_Data.bbg_product_code(ii);
                counterparty_parent = Master_DVD_Data.counterparty_parent(ii);
                ex_date = Master_DVD_Data.ex_date(ii);
                dvd_Nc = Master_DVD_Data.dvd_Nc(ii);
                stock_type = Master_DVD_Data.stock_type(ii);

                IndexTax = strcmpi(bbg_product_code,Tax_bbg_product_code) & strcmpi(counterparty_parent,Tax_counterparty_parent) & strcmpi(ex_date,Tax_ex_date);
                IndexFx = strcmpi(bbg_product_code,FXR_bbg_product_code) & strcmpi(counterparty_parent,FXR_counterparty_parent) & strcmpi(ex_date,FXR_ex_date) & strcmpi(stock_type,FXR_stock_type);
                %%% Tax Rate
                if ~any(IndexTax)
                    Tax_miss_all_values = strcat(bbg_product_code,{' - '},counterparty_parent);
                    Missing_Tax_Rate = [Missing_Tax_Rate,Tax_miss_all_values];
                else
                    tax_rate_val = cell2mat(Tax_Rate_Table_Data(IndexTax,5));
                    Net_DVD_NC = (dvd_Nc - (dvd_Nc .* tax_rate_val) ./ 100);

                    tax_rate_percen = vertcat(tax_rate_percen,tax_rate_val);
                    Net_DVD_NC_val = vertcat(Net_DVD_NC_val,Net_DVD_NC);
                end

                %%% EX Rate
                if ~any(IndexFx)
                    EX_miss_all_values = strcat(bbg_product_code,{' - '},counterparty_parent);
                    Missing_EX_Rate = [Missing_EX_Rate,EX_miss_all_values];  
                else  
                    ex_dvd_fx_rate = DVD_FX_Rate_Table_Data(IndexFx,5);
                    payable_dvd_fx_rate = DVD_FX_Rate_Table_Data(IndexFx,7);
                    Fx_tabl_payable_date = DVD_FX_Rate_Table_Data(IndexFx,6);

                    ex_fx_rate_val = vertcat(ex_fx_rate_val,ex_dvd_fx_rate);
                    payable_fx_rate_val = vertcat(payable_fx_rate_val,payable_dvd_fx_rate);
                    Fx_payable_date = vertcat(Fx_payable_date,Fx_tabl_payable_date);
                end
            end

            Missing_Tax_Rate = convertCell2Char(unique(Missing_Tax_Rate));
            if ~isempty(Missing_Tax_Rate)  
                OutErrorMsg = {[Missing_Tax_Rate,' tax_rate is not found in ',char(Tax_Rate_Table_Name)]};
                return;
            end

            Missing_EX_Rate = convertCell2Char(unique(Missing_EX_Rate));
            if ~isempty(Missing_EX_Rate) 
                OutErrorMsg = {[Missing_EX_Rate,' ex_fx_rate is not found in ',char(DVD_FX_Rate_Table_Name)]};
                return;
            end

            Master_DVD_Data.tax_rate = tax_rate_percen;
            Master_DVD_Data.net_dvd_nc = Net_DVD_NC_val;
            Master_DVD_Data.ex_fx_rate = ex_fx_rate_val; 
            Master_DVD_Data.payable_fx_rate = payable_fx_rate_val;
            Master_DVD_Data.payable_date = Fx_payable_date;

            %%% Currency_Spot_Mapping
            Currency_Spot_Table_Name = 'currency_spot_mapping_table';
            SqlQuery_DVD_FX_Rate = ['select * from ',char(Currency_Spot_Table_Name),] ;
            [~,Currency_Spot_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_FX_Rate);

            if strcmpi(Currency_Spot_Table_Data,'No Data')
                OutErrorMsg = {['No Data found in ',char(Currency_Spot_Table_Name),' for ',char(Settele_date)]} ;
                return;
            end   
            Mapping_currency = Currency_Spot_Table_Data(:,1);

            %%%% To decide the multiplication or division based on curreny
            NET_EX_DVD_USD_val = []; NET_PAYABLE_DVD_USD_val = []; Missing_Currency = [];
            for q = 1:1:row
                Single_currency = Master_DVD_Data.currency(q);
                Index = strcmpi(Single_currency,Mapping_currency);

                if ~any(Index)
                    Missing_Currency = [Missing_Currency,Single_currency];
                    continue;
                end

                quantotype = Currency_Spot_Table_Data(Index,3);
                if (strcmpi(quantotype,'division'))
                    %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
                    NET_EX_DVD_USD = Master_DVD_Data.net_dvd_nc(q) ./ cell2mat(Master_DVD_Data.ex_fx_rate(q));
                    NET_PAYABLE_DVD_USD = Master_DVD_Data.net_dvd_nc(q) ./ cell2mat(Master_DVD_Data.payable_fx_rate(q)); 

                    NET_EX_DVD_USD_val = vertcat(NET_EX_DVD_USD_val,NET_EX_DVD_USD);
                    NET_PAYABLE_DVD_USD_val = vertcat(NET_PAYABLE_DVD_USD_val,NET_PAYABLE_DVD_USD);
                elseif (strcmpi(quantotype,'multiplication'))
                    %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
                    NET_EX_DVD_USD = Master_DVD_Data.net_dvd_nc(q) .* cell2mat(Master_DVD_Data.ex_fx_rate(q));
                    NET_PAYABLE_DVD_USD = Master_DVD_Data.net_dvd_nc(q) .* cell2mat(Master_DVD_Data.payable_fx_rate(q)); 

                    NET_EX_DVD_USD_val = vertcat(NET_EX_DVD_USD_val,NET_EX_DVD_USD);
                    NET_PAYABLE_DVD_USD_val = vertcat(NET_PAYABLE_DVD_USD_val,NET_PAYABLE_DVD_USD);
                end
            end 

            Missing_Currency = convertCell2Char(Missing_Currency);
         if ~isempty(Missing_Currency)
                TempMsg = {['INFO:',Missing_Currency,' currency is not found in ',char(Currency_Spot_Table_Name)]};
                OutLogMsg_DVD = [OutLogMsg_DVD; TempMsg];
                xlswrite(OutXLSName,OutLogMsg_DVD,'DVD_FTD_OUTPUT');
         else
             
             %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
            Master_DVD_Data.net_ex_dvd_usd = NET_EX_DVD_USD_val;
            Master_DVD_Data.net_payable_dvd_usd = NET_PAYABLE_DVD_USD_val;
   
            %%% find the status and calculate the NET_DVD_USD
            status_all = []; NET_DVD_USD_val = [];
            for z = 1:1:row
                %%% Newly added code
                if iscell(Master_DVD_Data.payable_date(z))
                    payable_date = cell2mat(Master_DVD_Data.payable_date(z));
                    if isnan(payable_date)
                        payable_date = '';
                        Exact_payable_date{z,1} = '';
                    end
                    Exact_payable_date{z,1} = payable_date;
                else
                    payable_date = '';
                    Exact_payable_date{z,1} = '';
                end
                %%% end code 
                if strcmp(payable_date,'null') || isempty(payable_date)
                    status = 'live';
                    NET_DVD_USD = Master_DVD_Data.net_ex_dvd_usd(z);
                elseif (payable_date) <= char(Settele_date)
                    status = 'dead';
                    NET_DVD_USD = Master_DVD_Data.net_payable_dvd_usd(z);
                else   
                    status = 'live';
                    NET_DVD_USD = Master_DVD_Data.net_ex_dvd_usd(z);   
                end
                status_all = vertcat(status_all,status);
                NET_DVD_USD_val = vertcat(NET_DVD_USD_val,NET_DVD_USD);
            end

            Master_DVD_Data.status = status_all;
            Master_DVD_Data.net_dvd_usd = NET_DVD_USD_val;
            Master_DVD_Data.action = [];
            Col_Names = dataset2cell(Master_DVD_Data);
            
            if any(strcmp(Col_Names(1,:),'ratio'))
            Master_DVD_Data.ratio = [];
            end
            
            if any(strcmp(Col_Names(1,:),'shares_bbg_product_code'))
            Master_DVD_Data.shares_bbg_product_code = [];
            end
            
           %%% write the variable data into excel sheet
%            Index_nan = isnan(Master_DVD_Data.payable_date);
%            payable_date_string = repmat({''},size(Index_nan,1),1);
           Master_DVD_Data.payable_date =Exact_payable_date;  %% newly added line
           
            Master_DVD_Data = dataset2cell(Master_DVD_Data);
            xlswrite(OutXLSName,Master_DVD_Data,'DVD_FTD_OUTPUT');

            Master_DVD_Data_upload = Master_DVD_Data(2:end,:); %%% data for upload

            try
                DVD_Report_Store_Table_Name = strcat('dividend_report_store_table_',InBUName);
%                 Broker_Deal_TableColumns = gettablecolumnnames(ObjDB, DVD_Report_Store_Table_Name);
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',char(DVD_Report_Store_Table_Name),' where ex_date = ''',char(Settele_date),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');
                
                %datainsert(ObjDB,char(DVD_Report_Store_Table_Name),Broker_Deal_TableColumns,Master_DVD_Data_upload)
                
                upload_in_database(DVD_Report_Store_Table_Name,Master_DVD_Data_upload,ObjDB);
                disp('Upload Done Successfully for DVD Table');           
            catch
                OutErrorMsg = {['Data upload got failed in ',char(DVD_Report_Store_Table_Name)]};
            end
            
         end
    end
    
    end
    
    %%%%    Shares Evalution Process
    %OutLogMsg_Shares = [];
    if isempty(DVD_PRICE_Shares_Table_Data)
            OutLogMsg_Shares = {['INFO:NO SHARES DATA for EX_DATE ',char(Settele_date),]}; 
            xlswrite(OutXLSName,OutLogMsg_Shares,'SHARES_OUTPUT');
    else
        
        DVD_PRICE_Shares_Table_Data=dataset2cell(DVD_PRICE_Shares_Table_Data);
        DVD_PRICE_ColNames = DVD_PRICE_Shares_Table_Data(1,:);
        DVD_PRICE_Shares_Table_Data = DVD_PRICE_Shares_Table_Data(2:end,:);   
        DVD_PRICE_Shares_Table_Data=[DVD_PRICE_ColNames;DVD_PRICE_Shares_Table_Data]; % only shares data
        
        %%%% find the Open lots (Open_qty) and Counter party 
        Shares_Data_Group=[];
        [rr,~]=size(DVD_PRICE_Shares_Table_Data);
        for j=1:1:rr
            field_bbg_product_code=DVD_PRICE_Shares_Table_Data(j,2); 
            Indx=find(strcmpi(field_bbg_product_code,Unrealized_Report_Table_Data(:,4)));

            Unrealized_Report_Matched_Data = Unrealized_Report_Table_Data(Indx,:);
            if ~isempty(Unrealized_Report_Matched_Data)
                Unique_Group_Data=DVD_PRICE_Shares_Table_Data(j,:);
                Unique_Group_Data = repmat(Unique_Group_Data,size(Indx));
                Raw_pro_Matched_Data=[Unrealized_Report_Matched_Data(:,3) Unrealized_Report_Matched_Data(:,5) Unique_Group_Data Unrealized_Report_Matched_Data(:,7)];
                Shares_Data_Group=vertcat(Shares_Data_Group,Raw_pro_Matched_Data);
            end   
        end

        if isempty(Shares_Data_Group)
            OutLogMsg_Shares = {'INFO:No Open Lots available for the corresponding stock_ids!'} ;
            xlswrite(OutXLSName,OutLogMsg_Shares,'SHARES_OUTPUT');
        else   
             %%% Shares Header and Data
            Master_Shares_Header=[Unrealized_Report_ColNames(3) Unrealized_Report_ColNames(5) DVD_PRICE_ColNames Unrealized_Report_ColNames(7)];
            Master_Shares_Data = [Master_Shares_Header ; Shares_Data_Group];
            Master_Shares_Data = cell2dataset(Master_Shares_Data);

            %%% security_id  and instrument
            security_id = strcat({'SW'},{' '},Master_Shares_Data.bbg_product_code);
            instrument = Master_Shares_Data.instrument;

            %%%% Ratio based calculation
            %%%% find new_lots
            original_lots = Master_Shares_Data.open_qty;
            ratio = Master_Shares_Data.ratio;
            shares_bbg_product_code = Master_Shares_Data.shares_bbg_product_code;
            corporate_action_type = Master_Shares_Data.stock_type;
            counterparty_parent = Master_Shares_Data.counterparty_parent;
            ex_date = Master_Shares_Data.ex_date;

            [x,~] = size(Master_Shares_Data);
            MA_lots = []; MA_instrument = []; MA_subportfolio = []; MA_security_id = []; MA_counterpareny = [];
            MA_ex_date = []; MA_Stock_type = []; subportfolio = {''};
            
            for t = 1:1:x
                corporate_action = corporate_action_type(t);
                ratio_val = ratio(t);
                original_lots_val = original_lots(t);
                shares_bbg_pro_code = shares_bbg_product_code(t);
                counterparty_parent1 = counterparty_parent(t);
                ex_date1 = ex_date(t);
                
                if cell2mat(strfind(ratio_val,':'))
                    [preratio, postratio] = strtok(ratio_val,':');
                    postratio = strrep(postratio,':','');
                    preratio = str2num(char(preratio));
                    postratio = str2num(char(postratio));
                else
                    preratio = 0;
                    postratio = 0;
                    entered_ratio = cell2mat(ratio_val);
                end

                if strcmpi(corporate_action,'Stock Split')
                    lots_booked =  round(((original_lots_val .* preratio)/(postratio)),0);
                    New_lots_booked(t,1) = lots_booked - original_lots_val;
                elseif strcmpi(corporate_action,'Rights Issue')
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                elseif strcmpi(corporate_action,'Bonus')
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                elseif strcmpi(corporate_action,'Stock Dividend')
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                elseif strcmpi(corporate_action,'Entitilement')
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                elseif strcmpi(corporate_action,'Spinoff')
                    if isempty(char(shares_bbg_pro_code))
                        New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                    else      
                        New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                        shares_security_id = strcat({'SW'},{' '},shares_bbg_pro_code);

                        spm_Table_Name = strcat('subportfolio_product_mapping_table_',InBUName);
                        SqlQuery_spmt = ['select * from ',char(spm_Table_Name),' where product_code = ''',char(shares_security_id),''' and portfolio = ''',char(InBUName),''' '] ;
                        [~,spm_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_spmt);  
                        
                        instrument(t,1)  = spm_Table_Data(1,1);
                        subportfolio(t,1) = spm_Table_Data(1,2);
                        security_id(t,1) = shares_security_id;
                        counterparty_parent(t,1) = counterparty_parent1;
                        
                    end  
                elseif strcmpi(corporate_action,'Tender')
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                elseif strcmpi(corporate_action,'Merger and acquisition')
                    if isempty(char(shares_bbg_pro_code))
                        New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);
                    else
                        shares_security_id = strcat({'SW'},{' '},shares_bbg_pro_code);
                        New_lots_booked(t,1) = (original_lots_val) .* -1;
                        Merger_Acq_lots = round(((original_lots_val .* preratio)/(postratio)),0);

                        spm_Table_Name = strcat('subportfolio_product_mapping_table_',InBUName);
                        SqlQuery_spmt = ['select * from ',char(spm_Table_Name),' where product_code = ''',char(shares_security_id),''' and portfolio = ''',char(InBUName),''' '] ;
                        [~,spm_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_spmt);  
                        
                        Merger_Acq_instrument = spm_Table_Data(1,1);
                        Merger_Acq_subportfolio = spm_Table_Data(1,2);
                        Merger_Acq_security_id = shares_security_id;
                        
                       MA_lots = [MA_lots ; Merger_Acq_lots];
                       MA_instrument = [MA_instrument ; Merger_Acq_instrument];
                       MA_subportfolio = [MA_subportfolio ; Merger_Acq_subportfolio];
                       MA_security_id = [MA_security_id ; Merger_Acq_security_id];
                       MA_counterpareny = [MA_counterpareny ; counterparty_parent1];
                       MA_ex_date = [MA_ex_date ; ex_date1];
                       MA_Stock_type = [MA_Stock_type ; corporate_action];
                    end
                    
                else
                    New_lots_booked(t,1) =  round(((original_lots_val .* preratio)/(postratio)),0);      
                end
            end
        %%% end ratio code
        Master_Shares_Data.open_qty = [New_lots_booked ; MA_lots];
        Master_Shares_Data.instrument = [instrument ; MA_instrument];
        subportfolio = [subportfolio ; MA_subportfolio];
        Master_Shares_Data.security_id = [security_id ; MA_security_id];
        Master_Shares_Data.counterparty_parent = [counterparty_parent ; MA_counterpareny];
        Master_Shares_Data.stock_type = [corporate_action_type ; MA_Stock_type];
        Master_Shares_Data.ex_date = [ex_date ; MA_ex_date];
            
        %%% subportfolio product mapping
        subportfolio_prod_mapp_Table_Name = strcat('subportfolio_product_mapping_table_',InBUName);
        SqlQuery_subportfolio_prod_mapp_Table = ['select * from ',char(subportfolio_prod_mapp_Table_Name),' where product_code like ''','SW','%''' ' and portfolio = ''',char(InBUName),''' '] ;
        [~,subportfolio_prod_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_subportfolio_prod_mapp_Table);

        if strcmpi(subportfolio_prod_Table_Data,'No Data')
            OutLogMsg_Shares = {['INFO:Data not Available in ',char(subportfolio_prod_mapp_Table_Name),' for ',char(Settele_date)]} ;
            xlswrite(OutXLSName,OutLogMsg_Shares,'SHARES_OUTPUT');
        else      
            %%% get the subportfolio
            Missing_securityid = []; Missing_instrument = [];
            for ii = 1:1:length(security_id)  
                field_securityid = security_id(ii);
                field_instrument = instrument(ii);
                Index = find(strcmpi(field_securityid,subportfolio_prod_Table_Data(:,4)) & strcmpi(field_instrument,subportfolio_prod_Table_Data(:,1)));
                if ~any(Index)
                    Missing_securityid = [Missing_securityid;field_securityid];
                    Missing_instrument = [Missing_instrument;field_instrument];
                    continue;
                end
                subportfolio(ii,1) = subportfolio_prod_Table_Data(Index,2);          
            end

            Missing_securityid = convertCell2Char(Missing_securityid);
            Missing_instrument = convertCell2Char(Missing_instrument);

            if ~isempty(Missing_securityid)  %% Display error or Exit
                OutLogMsg_Shares = {['INFO:subportfolio is not avaliable for corresponding ' ,Missing_securityid,'s and',Missing_instrument]} ;
                xlswrite(OutXLSName,OutLogMsg_Shares,'SHARES_OUTPUT');
            else
                
            Master_Shares_Data.subportfolio = subportfolio;

            %%% consolidation part
            Master_Shares_Data = dataset2cell(Master_Shares_Data);
            Consolidate =Master_Shares_Data;
            Consolidate_Data_Header =  Consolidate(1,:);
            Consolidate_Data = Consolidate(2:end,:);

            UniqueFields = {'counterparty_parent','instrument','security_id','subportfolio','stock_type'}; 
            SumFields = {'open_qty'};
            OutputFields = Consolidate_Data_Header ;
            WeightedAverageFields = [];
            [Consolidate_Data_Header,Shares_Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

            %%% Header and Data
            Master_Shares_Data = [Consolidate_Data_Header;Shares_Data_Consolidate];
            Master_Shares_Data = cell2dataset(Master_Shares_Data);

             %%%  fetch data from Stock_Type_Master Table 
            SqlQuery_stock_Type = ['select * from ',char(DVD_Type_Master_Table_Name),' where action = ''','shares',''' '];
            Stock_Type_for_trades = fetch(ObjDB,SqlQuery_stock_Type);

            if(isempty(Stock_Type_for_trades))
                OutLogMsg_Shares = {'INFO:No data found in ',char(DVD_Type_Master_Table_Name),' for ',char(Settele_date)};
                xlswrite(OutXLSName,OutLogMsg_Shares,'SHARES_OUTPUT');
            else
            DVD_PRICE_Shares_Table_Data = dataset2cell(Master_Shares_Data);
            DVD_PRICE_Shares_Table_Data = DVD_PRICE_Shares_Table_Data(2:end,:);

            %%% shares data based on Trade count
            [r1,~]=size(DVD_PRICE_Shares_Table_Data);
            After_DVD_PRICE_Shares_Table_Data = [];
            for jj = 1:1:r1
                Stock_Type = DVD_PRICE_Shares_Table_Data(jj,11);
                User_Stock_Type_data = Stock_Type_for_trades(strcmpi(Stock_Type,Stock_Type_for_trades(:,1)),:);

                if ~isempty(User_Stock_Type_data)
                    trade_count = cell2mat(User_Stock_Type_data(1,3));
                    DVD_PRICE_Table_Data1 = repmat(DVD_PRICE_Shares_Table_Data(jj,:),trade_count,1);
                    After_DVD_PRICE_Shares_Table_Data =vertcat(After_DVD_PRICE_Shares_Table_Data,DVD_PRICE_Table_Data1);
                end 
            end

            Trade_Data_Consolidate = After_DVD_PRICE_Shares_Table_Data;  % olny shares data based on trades count
            Master_Shares_Data = [Consolidate_Data_Header;Trade_Data_Consolidate];
            Master_Shares_Data = cell2dataset(Master_Shares_Data);
            
            %%% get last trade ID without prefix
            [~,~,DBTradeId]= getLastTradeId(InBUName);

            SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
            TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
            TempTradeId = DBTradeId;
            if(isempty(TempTradeId))
                TempTradeId = 0;      %% Default
            end
            NumRows = size(Master_Shares_Data.counterparty_parent,1);
            NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
            TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
            TradeId = strrep(TradeId,' ','');  %% Remove Spaces

            %%%% find the market_action and equity_swaps_trade_type
            [row,~]=size(Master_Shares_Data.counterparty_parent);
            for ii = 1:1:row
                sign_lots = sign(Master_Shares_Data.open_qty(ii));
                counterparty_parent = Master_Shares_Data.counterparty_parent(ii);
                if eq(sign_lots,1)
                    market_action(ii,1) = {'Bought'};
                    if strcmpi(counterparty_parent,'CICC') || contains(upper(counterparty_parent),'CICC-SUB')  %%% newly added line - CICC-Sub
                        equity_swaps_trade_type(ii,1) = {'BUY'};
                    elseif strncmpi(counterparty_parent,'MS',2) || strncmpi(counterparty_parent,'GS',2)    %%% newly added line - GS
                        equity_swaps_trade_type(ii,1) = {'BUY TO OPEN'};
                    elseif strcmpi(counterparty_parent,'MORGAN STANLEY')
                        equity_swaps_trade_type(ii,1) = {'BUY TO OPEN'};
                    else
                        equity_swaps_trade_type(ii,1) = {'BUY TO OPEN'};  %%% newly added line
                    end
                else
                    market_action(ii,1) = {'Sold'};
                    if strcmpi(counterparty_parent,'CICC') || contains(upper(counterparty_parent),'CICC-SUB')   %%% newly added line - CICC-Sub
                        equity_swaps_trade_type(ii,1) = {'SELL'};
                    elseif strncmpi(counterparty_parent,'MS',2) || strncmpi(counterparty_parent,'GS',2)    %%% newly added line - GS
                        equity_swaps_trade_type(ii,1) = {'SELL TO OPEN'};  
                    elseif strcmpi(counterparty_parent,'MORGAN STANLEY')
                        equity_swaps_trade_type(ii,1) = {'SELL TO OPEN'};
                    else
                        equity_swaps_trade_type(ii,1) = {'SELL TO OPEN'};   %%% newly added line
                    end
                end
            end    

            counterparty_parent = Master_Shares_Data.counterparty_parent;
            %%% user required data
            trade_group_type = repmat({'EQD_SWAP'},size(counterparty_parent));
            deal_status = repmat({'live'},size(counterparty_parent));
            exe_type = repmat({'ELEC'},size(counterparty_parent));
            brokerage_rate_type = repmat({'NORMAL'},size(counterparty_parent));
            is_netting_done = repmat({0},size(counterparty_parent));

            trade_id = TradeId;           
            parent_trade_id = TradeId;
            transaction_date = Master_Shares_Data.ex_date;
            original_lots = Master_Shares_Data.open_qty;
            counterparty_parent = Master_Shares_Data.counterparty_parent;
            execution_broker = Master_Shares_Data.counterparty_parent;
            clearing_broker = Master_Shares_Data.counterparty_parent;
            lots_to_be_netted = Master_Shares_Data.open_qty;
            corporate_action_type = Master_Shares_Data.stock_type;
            subportfolio = Master_Shares_Data.subportfolio;
            instrument = Master_Shares_Data.instrument;
            security_id = Master_Shares_Data.security_id;
            
            
            premium	 = cell(size(counterparty_parent,1),1);
            counterparty_bu	= cell(size(counterparty_parent,1),1);
            counterparty_entity	=cell(size(counterparty_parent,1),1);
            reconed_with_clearer	=cell(size(counterparty_parent,1),1);
            reconed_completed_by	=cell(size(counterparty_parent,1),1);
            recon_datetimestamp	=cell(size(counterparty_parent,1),1);
            contract_number	=cell(size(counterparty_parent,1),1);
            subcontract_number	=cell(size(counterparty_parent,1),1);
            opt_periodicity	=cell(size(counterparty_parent,1),1);
            date_to_be_fixed	=cell(size(counterparty_parent,1),1);
            trader_name	=cell(size(counterparty_parent,1),1);
            traded_timestamp	=cell(size(counterparty_parent,1),1);
            spcl_rate	=cell(size(counterparty_parent,1),1);
            other_remarks	=cell(size(counterparty_parent,1),1);
            corporate_action_id	=cell(size(counterparty_parent,1),1);
            premium_nc	=cell(size(counterparty_parent,1),1);
            reset_date	=cell(size(counterparty_parent,1),1);
            version_no	=cell(size(counterparty_parent,1),1);
            version_created_by	=cell(size(counterparty_parent,1),1);
            version_timestamp	=cell(size(counterparty_parent,1),1);
            version_comments =cell(size(counterparty_parent,1),1);

            %%% Shares Header and Data
            Shares_Header = {'trade_id','parent_trade_id','transaction_date','subportfolio','instrument','trade_group_type','security_id','market_action', ...
                             'original_lots','premium','counterparty_parent','counterparty_bu','counterparty_entity','execution_broker','clearing_broker', ...
                             'reconed_with_clearer','reconed_completed_by','recon_datetimestamp','contract_number','subcontract_number','opt_periodicity', ...
                             'is_netting_done','lots_to_be_netted','date_to_be_fixed','trader_name','traded_timestamp','deal_status','exe_type','brokerage_rate_type', ...
                             'spcl_rate','other_remarks','corporate_action_id','corporate_action_type','equity_swaps_trade_type','premium_nc','reset_date', ...	
                             'version_no','version_created_by','version_timestamp','version_comments'};

            Shares_Data = [trade_id,parent_trade_id,transaction_date,subportfolio,instrument,trade_group_type,security_id,market_action, ...
                           num2cell(original_lots),premium,counterparty_parent,counterparty_bu,counterparty_entity,execution_broker,clearing_broker, ...
                           reconed_with_clearer,reconed_completed_by,recon_datetimestamp,contract_number,subcontract_number,opt_periodicity, ...
                           is_netting_done,num2cell(lots_to_be_netted),date_to_be_fixed,trader_name,traded_timestamp,deal_status,exe_type,brokerage_rate_type, ...
                           spcl_rate,other_remarks,corporate_action_id,corporate_action_type,equity_swaps_trade_type,premium_nc,reset_date, ...	
                           version_no,version_created_by,version_timestamp,version_comments];

            Master_Shares_Data = [Shares_Header ; Shares_Data];
            %%% write the variable data into excel sheet
            xlswrite(OutXLSName,Master_Shares_Data,'SHARES_OUTPUT');

%             %%% generate data based on corporate_action_type (Stock Type)
%             Master_Shares_Data = cell2dataset(Master_Shares_Data);
%             if(exist('corporate_action','var'))
%                 corporate_action_type = {corporate_action};  % user passed corporate_action
%             else
%                 corporate_action_type = Master_Shares_Data.corporate_action_type;  % else default all corporate_actions
%             end
% 
%             %%% Compareing data and write data into file
%             Unique_corporate_action = unique(corporate_action_type,'stable');
%             [UR,~] = size(Unique_corporate_action);
%             
%             for ii = 1:1:UR
%                 single_corporate_action = Unique_corporate_action(ii);
%                 Unique_Shares_Data=Master_Shares_Data(strcmpi(single_corporate_action,Master_Shares_Data.corporate_action_type),:);
%             
%                 %%% write the variable data into excel sheet
%                 xlswrite(OutXLSName,dataset2cell(Unique_Shares_Data),char(single_corporate_action));
%                 
%             end

            end
            end
        end
        end    
    end
        
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end
end   
    
    
     