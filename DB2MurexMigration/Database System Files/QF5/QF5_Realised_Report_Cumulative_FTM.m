function [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTM(InBUName)

OutErrorMsg = {'No Errors'};

OutXLSName = getXLSFilename('QF5_Realised_Report_Cumulative_FTM');

try
    %% Fetching Data
    ObjDB = connect_to_database;
    
    DB_Date_Format = 'yyyy-mm-dd';
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    Monthly_Reset_Table = ['monthly_reset_date_table_',char(InBUName)];
    Unique_CounterParty = fetch(ObjDB,['select distinct(counterparty_parent) from ',Monthly_Reset_Table]);
    
    %% Previous Month Date
    
    Current_Month = str2double(datestr(char(Settle_Date),5));
    Current_Year = str2double(datestr(char(Settle_Date),10));
    
    if(Current_Month==1)
        %         PrevEOM_Date = datestr( eomdate(Current_Year-1, 12) ,'yyyy-mm-dd'); %% If Jan Comes Shold be Dec
        PrevEOM_Date = datestr( lbusdate(Current_Year-1,12) ,'yyyy-mm-dd');
    else
        PrevEOM_Date = datestr( eomdate(Current_Year, Current_Month-1) ,'yyyy-mm-dd');
    end
    
    
    %% Old Logic to Construct Start_Date & End_Date
    
    %     CurrentFOM_Date = datestr( fbusdate(Current_Year, Current_Month) ,'yyyy-mm-dd');
    %     CurrentEOM_Date = datestr( eomdate(Current_Year, Current_Month) ,'yyyy-mm-dd');
    
    %% New Logic to Construct Start_Date & End_Date
    %     try
    %         Monthly_Reset_Table = ['monthly_reset_date_table_',char(InBUName)];
    %         [Monthly_ResetTable_ColNames,Monthly_RessetTable_RowData] = Fetch_DB_Data(ObjDB,[],Monthly_Reset_Table);
    %
    %         Monthly_ResetTable_Data = cell2dataset([Monthly_ResetTable_ColNames;Monthly_RessetTable_RowData]);
    %
    %         Current_Reset_Month = datestr(Settle_Date,'mmm-yy');
    %         Monthly_ResetTable_Data(strcmpi(Monthly_ResetTable_Data.reset_month,Current_Reset_Month),:);
    %
    %         %     Monthly_ResetTable_Data =
    %
    %     catch
    %         OutErrorMsg = {['Failed to fetch ',char(Monthly_Reset_Table)]};
    %         return;
    %     end
    
    %% Fetch DB Data for First Query
    
    Default_Name = 'helper_evaluation_reports_view_';
    ViewName = [Default_Name,InBUName] ;
    
    SqlQuery_1 = ['select * from ',char(ViewName),' where deal_status = ''dead'' and reset_date IS NULL'] ;
    
    [Overall_ColNames,Overall_DBData] = Fetch_DB_Data(ObjDB,SqlQuery_1); %% ViewName
    
    Transaction_Date_Col = cellStrfind_exact(Overall_ColNames,{'transaction_date'});
    Transaction_Date_Num = datenum(Overall_DBData(:,Transaction_Date_Col),DB_Date_Format);
    
    Counterparty_Main_Col =   cellStrfind_exact(Overall_ColNames,{'counterparty_parent'});
    
    %% Second Query Part
    
    SqlQuery_2 = ['select * from ',char(ViewName),' where deal_status = ''dead'' and reset_date IS NOT NULL'] ;
    
    [OverallReset_ColNames,Overall_Reset_DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery_2); %% ViewName
    
    ResetDate_Main_Col = cellStrfind_exact(OverallReset_ColNames,{'reset_date'});
    Overall_ResetDate_Num = datenum(Overall_Reset_DBData_All(:,ResetDate_Main_Col),DB_Date_Format);
    
    
    %%
    
    Overall_ErrorMsg = [];
    Overall_CounterpartyData = [];
    
    for k = 1 : length(Unique_CounterParty)
        
        Current_CounterParty = Unique_CounterParty(k);
        
        %%  Get Start & End Date of Current Month
        try
            Reset_Month = datestr(Settle_Date,'mmm-yy');
            Reset_Tbl_Name = ['monthly_reset_date_table_',char(InBUName)];
            [~,Start_End_Date] = Fetch_DB_Data(ObjDB,['select start_date,end_date from ',char(Reset_Tbl_Name),' where reset_month = ''',char(Reset_Month),''' and counterparty_parent = ''',char(Current_CounterParty),''' ']);
            
            CurrentFOM_Date = Start_End_Date(1,1);
            CurrentEOM_Date = Start_End_Date(1,2);
            CurrentFOM_Date_Num = datenum(CurrentFOM_Date,DB_Date_Format);
            CurrentEOM_Date_Num = datenum(CurrentEOM_Date,DB_Date_Format);
            
            
        catch
            OutErrorMsg = {['Failed to fetch Start & End Date for ',char(Current_CounterParty),' from ',char(Monthly_Reset_Table)]};
            return;
        end
        
        %% SqlQuery_1  Part
        %                 Default_Name = 'helper_evaluation_reports_view_'; %% Old Logic , it takes long time to run
        %         ViewName = [Default_Name,InBUName] ;
        %         SqlQuery = ['select * from ',char(ViewName),' where (transaction_date >= ''',char(CurrentFOM_Date),''' and  transaction_date <= ''',char(CurrentEOM_Date),''') and deal_status = ''dead'' and reset_date IS NULL and counterparty_parent = ''',char(Current_CounterParty),''' '];
        %         [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
        
        Matched_Date_Index  = (Transaction_Date_Num >= CurrentFOM_Date_Num  & Transaction_Date_Num <= CurrentEOM_Date_Num ) ;
        DBData_All = Overall_DBData(Matched_Date_Index,:);
        
        Matched_Current_Counterparty_Index = strcmpi(DBData_All(:,Counterparty_Main_Col),Current_CounterParty);
        DBData_All = DBData_All(Matched_Current_Counterparty_Index,:);
        
        if(isempty(DBData_All))
            OutErrorMsg_1 = {['Not Getting Monthly Data from ',ViewName,' for ',char(Current_CounterParty)]};
            Overall_ErrorMsg = [Overall_ErrorMsg ; OutErrorMsg_1,];
        end
        
        %% Get Reset Deals Using Reset Date , SqlQuery_2 Part
        
        Reset_Month = datestr(PrevEOM_Date,'mmm-yy');
        
        [~,Reset_Date] = Fetch_DB_Data(ObjDB,['select reset_date from ',char(Reset_Tbl_Name),' where reset_month = ''',char(Reset_Month),''' and counterparty_parent = ''',char(Current_CounterParty),''' ']);
        Reset_Date_Num = datenum(Reset_Date,DB_Date_Format);
        
        % Old Format of Code , it takes long time to run
        %         SqlQuery_ResetDeals = ['select * from ',char(ViewName),' where reset_date = ''',char(Reset_Date),''' and deal_status = ''dead'' and counterparty_parent = ''',char(Current_CounterParty),''' '] ;
        %         [Reset_ColNames,Reset_DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery_ResetDeals);
        
        Matched_ResetDate_Index = (Overall_ResetDate_Num == Reset_Date_Num) & strcmpi(Overall_Reset_DBData_All(:,Counterparty_Main_Col),Current_CounterParty) ;
        Reset_DBData_All = Overall_Reset_DBData_All(Matched_ResetDate_Index,:);
        
        if(isempty(Reset_DBData_All))
            OutErrorMsg_1 = {['Not Getting Reset Data from ',ViewName,' for ',char(Current_CounterParty)]};
            Overall_ErrorMsg = [Overall_ErrorMsg ; OutErrorMsg_1,];
        end
        
        %% Concatenate Reset deals and Normal
        
        if(size( DBData_All,2)>1)
            if(size( Reset_DBData_All,2)>1)
                DBData_All = [DBData_All ; Reset_DBData_All] ;
            else
                OutErrorMsg_1 = {['Not Getting Reset Data from ',ViewName,' for ',char(Current_CounterParty)]};
                %                 return;
                
                Overall_ErrorMsg = [Overall_ErrorMsg ; OutErrorMsg_1,];
                %                 xlswrite(OutXLSName,OutErrorMsg,[char(Current_CounterParty),' Error Sheet']);
                %                 continue;
            end
        else
            OutErrorMsg_1 = {['Not Getting Monthly Data from ',ViewName,' for ',char(Current_CounterParty)]};
            %             return;
            
            Overall_ErrorMsg = [Overall_ErrorMsg ; OutErrorMsg_1];
            %             xlswrite(OutXLSName,OutErrorMsg_1,[char(Current_CounterParty),' Error Sheet']);
            continue;
        end
        
        if(isempty(DBData_All))
            continue;
        end
        
        
        %% Initialize Output Col
        
        TradeID_Col = 1 ; %% TradeID
        TransactionDate_Col = 2 ; %% Transaction Date
        Counterparty_Col = 3 ; %% counterparty_parent
        StockNo_Col = 4; %% product_code
        Instrument_Col = 5; %% instrument
        StockName_Col = 6; %% product_name
        
        PurchaseQuantity_Col = 7; %% active_lots
        PurchasePrice_Col = 8; %%original_premium
        SalesQuantity_Col = 9; %% active_lots
        SalesPrice_Col = 10; %% original_premium
        
        RealisedPnL_USD_Col = 11;
        BGN_SpotRate_Col = 12;
        
        %%
        
        if(size(DBData_All,2) > 1)
            
            %% Consolidate Data
            %         UniqueFields = {'transaction_date','product_code','product_name','market_action','original_premium'};
            %         SumFields = {'active_lots'};
            %         OutputFields = [UniqueFields,SumFields];
            %         WeightedAverageFields = [];
            %         [Consolidate_Fields,Consolidate_Data] = consolidatedata(Overall_ColNames,DBData_All,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            Consolidate_Fields = Overall_ColNames ;
            Consolidate_Data = DBData_All ;
            
            Consolidate_TradeID = cellStrfind_exact(Consolidate_Fields,{'trade_id'});
            Consolidate_TransactionDate  = cellStrfind_exact(Consolidate_Fields,{'transaction_date'});
            Consolidate_Counterparty_Col = cellStrfind_exact(Consolidate_Fields,{'counterparty_parent'});
            Consolidate_ProdCode_Col = cellStrfind_exact(Consolidate_Fields,{'product_code'});
            Consolidate_Instrument_Col = cellStrfind_exact(Consolidate_Fields,{'instrument'});
            Consolidate_ProdName_Col = cellStrfind_exact(Consolidate_Fields,{'product_name'});
            
            Consolidate_MarketAction_Col = cellStrfind_exact(Consolidate_Fields,{'market_action'});
            Consolidate_ActiveLots_Col = cellStrfind_exact(Consolidate_Fields,{'active_lots'});
            Consolidate_OriginalPremium_Col = cellStrfind_exact(Consolidate_Fields,{'original_premium'});
            Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(Consolidate_Fields,{'realised_mtm_usd'});
            %         Consolidate_BGNSpot_Col = cellStrfind_exact(Consolidate_Fields,{'bgn_spot_rate'});
            
            OutputData = cell(size(Consolidate_Data,1),10); %% cell(size(Consolidate_Data,1),14);
            
            for i = 1 : size(Consolidate_Data,1)
                
                OutputData(i,TradeID_Col) = Consolidate_Data(i , Consolidate_TradeID);
                OutputData(i,TransactionDate_Col) = Consolidate_Data(i , Consolidate_TransactionDate);
                
                OutputData(i,Counterparty_Col) = Consolidate_Data(i ,Consolidate_Counterparty_Col );
                OutputData(i,StockNo_Col) = Consolidate_Data(i , Consolidate_ProdCode_Col);
                OutputData(i,Instrument_Col) = Consolidate_Data(i ,Consolidate_Instrument_Col );
                OutputData(i,StockName_Col) = Consolidate_Data(i ,Consolidate_ProdName_Col );
                
                if(strcmpi(Consolidate_Data(i , Consolidate_MarketAction_Col),'bought'))
                    OutputData(i,PurchaseQuantity_Col) = Consolidate_Data(i ,Consolidate_ActiveLots_Col);
                    OutputData(i,PurchasePrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
                else
                    OutputData(i,SalesQuantity_Col) = Consolidate_Data(i , Consolidate_ActiveLots_Col);
                    OutputData(i,SalesPrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
                end
                
                OutputData(i,RealisedPnL_USD_Col) = Consolidate_Data(i , Consolidate_RealizedM2M_USD_Col) ;
                %             OutputData(i,BGN_SpotRate_Col) = Consolidate_Data(i , Consolidate_BGNSpot_Col);
                
                
            end
            
            
            %% Excel Sheet Write &  Delete Sheets
            
            Output_Header = {'TradeID','Transaction Date','Counterparty','Stock No','Instrument','Stock Name','Purchase Quantity','Purchase Price','Sales Quantity','Sales Price','Realised PnL (USD)'};    %% ,'BGN Spot Rate'
            
            xlswrite(OutXLSName,[Output_Header ;OutputData ],char(Current_CounterParty));
            
            Overall_CounterpartyData = [Overall_CounterpartyData ; OutputData];
            
            %% Uploading into  'realized_fty_report_store_table_'
            
            try
                Temp_Date = cell(size(OutputData,1),1);
                Temp_Date(:,:) = Settle_Date;
                
                Uploading_Data = [Temp_Date,OutputData];
                
                TableName = strcat('realized_ftm_report_store_table_',char(InBUName));
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',TableName,' where settle_date = ''',char(Settle_Date),''' and counterparty_parent=''',char(Current_CounterParty),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');
                upload_in_database(TableName, Uploading_Data); %% Required_Data %% Overall_Data
                cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date :  ',char(Settle_Date),' and counterparty ',char(Current_CounterParty)]);
            catch
                Overall_ErrorMsg = {[Overall_ErrorMsg ; ['Not uploading Data into ',TableName,' for ',char(Current_CounterParty)]]};
            end
            
            %% New Change
            %         UniqueFields = {'Stock No'};
            %         SumFields = {'Realised PnL (USD)'};
            %         OutputFields = [UniqueFields,SumFields];
            %         WeightedAverageFields = [];
            %
            %         [NetHeader,NetData] = consolidatedata(Output_Header, OutputData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            %         xlswrite(OutXLSName,[NetHeader ;NetData ],'Net Data' );
            %
            %%
            
        else
            OutErrorMsg = {['No Data ',ViewName]};
        end
        
    end
    Output_Header = {'TradeID','Transaction Date','Counterparty','Stock No','Instrument','Stock Name','Purchase Quantity','Purchase Price','Sales Quantity','Sales Price','Realised PnL (USD)'};    %% ,'BGN Spot Rate'

    %% Excel Delete Sheets
    try
        
        Counterparty_Col_Output = cellStrfind(Output_Header,{'Counterparty'});
        %%% Old Code
%         CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC');
%         MS_CounterpartyData = Overall_CounterpartyData(~CICC_Index,:);
%         xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        
%%%% New Logic
        %%% newly added line (01-05-2019)
        CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC') | contains(upper(Overall_CounterpartyData(:,Counterparty_Col_Output)),'CICC-SUB');
        MS_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MORGAN STANLEY') | strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MS',2);
        GS_Index = strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'GS',2);

        CICC_CounterpartyData = Overall_CounterpartyData(CICC_Index,:);  %%% CICC data
        MS_CounterpartyData = Overall_CounterpartyData(MS_Index,:);    %%% MS data
        GS_CounterpartyData = Overall_CounterpartyData(GS_Index,:);  %%% GS data
        
        xlswrite(OutXLSName,[Output_Header ; CICC_CounterpartyData ],'CICC-ALL' );
        xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL');
        xlswrite(OutXLSName,[Output_Header ; GS_CounterpartyData ],'GS-ALL');
%%% End logic
        
        if(~isempty(Overall_ErrorMsg))
            xlswrite(OutXLSName,Overall_ErrorMsg,' Error Sheet');
        end
        
        % OutXLSName = fullfile(pwd,OutXLSName);
        xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
        
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end