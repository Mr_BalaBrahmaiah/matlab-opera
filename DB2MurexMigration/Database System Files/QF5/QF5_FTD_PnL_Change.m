function [OutErrorMsg ,OutXLSName] = QF5_FTD_PnL_Change(InBUName,varargin)

%% Dependancies Report,Not run these report First we should run these

% [OutErrorMsg ,OutXLSName ] = QF5_UnRealised_Report(InBUName);
% [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTY(InBUName);

%%
try
    OutErrorMsg = {'No Errors'};
    
    if ~isempty(varargin) %% exist('Temp_Settle_Date','var')
        Current_SettleDate = varargin{1};
        OutXLSName = getXLSFilename(['QF5_FTD_PnL_Change_',char(Current_SettleDate)]);
    else
        Current_SettleDate = [];
        OutXLSName = getXLSFilename('QF5_FTD_PnL_Change');
    end
    
%     if ~isdeployed %%(exist('Current_SettleDate','var'))
%         OutXLSName = getXLSFilename(['QF5_FTD_PnL_Change_',char(Current_SettleDate)]);
%     else
%         OutXLSName = getXLSFilename('QF5_FTD_PnL_Change');
%     end
    
    %% QF5_UnRealized_FTD_PnL_Report
    
    if(~isempty(Current_SettleDate))
        [OutErrorMsg,UnRealized_OutData,Original_Unrealized_Data,Overall_Consolidate_Data] = QF5_UnRealized_FTD_PnL_Report(InBUName,Current_SettleDate) ;
    else
        [OutErrorMsg,UnRealized_OutData,Original_Unrealized_Data,Overall_Consolidate_Data] = QF5_UnRealized_FTD_PnL_Report(InBUName);
    end
    
    if(~strcmpi(OutErrorMsg,'No Errors'))
        return;
    end
    
    xlswrite(OutXLSName,Original_Unrealized_Data,'Original_Unrealized_Data');
    xlswrite(OutXLSName,dataset2cell(Overall_Consolidate_Data),'Overall_Conso_UnrealData'); %% If Need any Verification ,to write excel sheet
    xlswrite(OutXLSName,UnRealized_OutData,'Unrealised PNL'); %% If Need any Verification ,to write excel sheet
    
    
    %% QF5_Realised_Report_FTY_PnL_Report
    
    if(~isempty(Current_SettleDate))
        [OutErrorMsg,Realized_OutData,Original_Realized_Data,Overall_Consolidate_Data] = QF5_Realised_Report_FTY_PnL_Report(InBUName,Current_SettleDate);
    else
        [OutErrorMsg,Realized_OutData,Original_Realized_Data,Overall_Consolidate_Data] = QF5_Realised_Report_FTY_PnL_Report(InBUName);
    end
    
    if(~strcmpi(OutErrorMsg,'No Errors'))
        return;
    end
    
    xlswrite(OutXLSName,Original_Realized_Data,'Original_Realized_Data');
    xlswrite(OutXLSName,dataset2cell(Overall_Consolidate_Data),'Overall_Conso_RealData'); %% If Need any Verification ,to write excel sheet
    xlswrite(OutXLSName,Realized_OutData,'Realised PNL');%% If Need any Verification ,to write excel sheet
    
    %% Initialization OpenQty & Market Price  in Realized Data
    
    Temp_OpenQty = cell(size(Realized_OutData,1),2);
    Temp_OpenQty(1,:) = {'open_qty','market_price'};
    %     Temp_OpenQty(2:end,0) = {0};
    
    Realized_OutData = [Realized_OutData,Temp_OpenQty];
    
    %%
    Unrealised_Header = UnRealized_OutData(1,:);
    UnRealized_OutData(1,:) = [];
    UserWant_UnReal_Header = {'settle_date','counterparty_parent','stock_number','instrument','stock_name','open_qty','market_price','Realized_Unrealized_Header','unrealized_pnl','PnL_Change'};
    User_Unreal_Col = cellStrfind_exact(Unrealised_Header,UserWant_UnReal_Header);
    UserWant_Unrealised_Data = UnRealized_OutData(:,User_Unreal_Col);
    
    Realised_Header = Realized_OutData(1,:);
    Realized_OutData(1,:) = [];
    UserWant_Real_Header = {'settle_date','counterparty_parent','stock_number','instrument','stock_name','open_qty','market_price','Realized_Unrealized_Header','realized_pnl','PnL_Change'};
    User_Real_Col = cellStrfind_exact(Realised_Header,UserWant_Real_Header);
    UserWant_Realised_Data = Realized_OutData(:,User_Real_Col);
    
    Overall_FTD_Pnl_Data = [UserWant_Unrealised_Data ; UserWant_Realised_Data];
    UserWant_Real_Header = {'settle_date','counterparty_parent','stock_number','instrument','stock_name','open_qty','market_price','Realized_Unrealized_Header','PnL','PnL_Change'}; %% Make common PnL Header
    xlswrite(OutXLSName,[UserWant_Real_Header ; Overall_FTD_Pnl_Data],'FTD PNL');
    
    %% Final Ouput
    
    UniqueFields = {'settle_date','counterparty_parent','stock_number','instrument','stock_name'};
    SumFields={'open_qty','market_price','PnL_Change'};
    OutputFields = [UniqueFields ,SumFields] ; %ColNames;
    WeightedAverageFields = [];
    [ConsHeader,ConsDeals] = consolidatedata(UserWant_Real_Header, Overall_FTD_Pnl_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    xlswrite(OutXLSName,[ConsHeader ; ConsDeals],'Final FTD PNL');
    
    %% Uploading Data
    ObjDB = connect_to_database;
    if ~isdeployed
        TableName = strcat('ftd_pnl_change_report_store_table_',char(InBUName));
        set(ObjDB,'AutoCommit','off');
        DB_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
        SqlQuery = ['delete from ',TableName,' where settle_date = ''',char(DB_SettleDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, ConsDeals);
    end
    
    
    
    %% Delete Excel Sheet
    try
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        xls_change_activesheet(fullfile(pwd,OutXLSName),'Final FTD PNL');
    catch
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end