function [OutErrorMsg ,OutXLSName,Output_Header ,Output_Data,SummarySheet_Header, Summary_Data] = QF5_Borrowing_InterestRate_Calculation_CICC(InBUName,Trade_Date,Settle_Date,User_Passed_Multiplier,Current_CounterParty,OutXLSName)

% Settle_Date is no matter for CICC , We are getting rate & spread using
% for transaction Date
try
    OutErrorMsg = {'No Errors'};
    Output_Header = {''};  %% added on 14-06-2019
    SummarySheet_Header = {''};
    Output_Data = [];
    Summary_Data =[];  %%
    %     OutXLSName = getXLSFilename('QF5_Borrowing_InterestRate');
    
    %% Fetching Data
    ObjDB = connect_to_database;
    
    Trade_Date = cellstr(Trade_Date);
    % Trade_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    Reset_Month = datestr(Trade_Date,'mmm-yy');
   
    %%  Find Range of date to fetch data from Borrowing & interest Tables
    Current_Month = str2double(datestr(char(Trade_Date),5));
    Current_Year = str2double(datestr(char(Trade_Date),10));
    if(Current_Month==1)
        PrevEOM_Date = datestr( lbusdate(Current_Year-1,12) ,'yyyy-mm-dd');
    else
        PrevEOM_Date = datestr( eomdate(Current_Year, Current_Month-1) ,'yyyy-mm-dd');
    end
    
    Previous_Reset_Month = datestr(PrevEOM_Date,'mmm-yy');
    
    Settle_Date_Borrow_Interest = fetch(ObjDB,['select settle_date from monthly_reset_date_table_qf5 where counterparty_parent = ''',char(Current_CounterParty),''' and reset_month =''',char(Previous_Reset_Month),''' ']);
    
    %% Previous Month Date
    Settle_Date = cellstr(Settle_Date);
    
    Current_Month = str2double(datestr(char(Settle_Date),5));
    Current_Year = str2double(datestr(char(Settle_Date),10));
    
    if(Current_Month==1)
        %         PrevEOM_Date = datestr( eomdate(Current_Year-1, 12) ,'yyyy-mm-dd'); %% If Jan Comes Shold be Dec
        PrevEOM_Date = datestr( lbusdate(Current_Year-1,12) ,'yyyy-mm-dd');
    else
        PrevEOM_Date = datestr( eomdate(Current_Year, Current_Month-1) ,'yyyy-mm-dd');
    end
    
    Previous_Reset_Month = datestr(PrevEOM_Date,'mmm-yy'); %% For Find Last month Open Position Only when reset is happened, To exclude the Dead Deals
    
    %%
    
    System_Date_Format = 'dd-mm-yyyy';
    DB_Date_Format = 'yyyy-mm-dd';
    
    %%
    Default_Name = 'helper_evaluation_reports_view_';
    ViewName = [Default_Name,InBUName] ;
    
    % SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''live'' '];
    % [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
    
    StoreProcedure_Name = ['generate_position_monitoring_data_',char(InBUName)];
    
    SqlQuery_ValueDate = ['select value_date from vdate_sdate_table where settlement_date =''',char(Trade_Date),''' '] ;
    Current_ValueDate = fetch(ObjDB,SqlQuery_ValueDate);
    
    %%
    
%     SqlQuery_BorrowingCost = ['select * from borrowing_cost_table_qf5 where settlement_date =''',char(Settle_Date),''' '];
    SqlQuery_BorrowingCost = ['select * from borrowing_cost_table_qf5 where settlement_date >=''',char(Settle_Date_Borrow_Interest),''' and settlement_date <=''',char(Trade_Date),''' and counterparty_parent = ''',char(Current_CounterParty),''' '];
    [BorrowingCostTbl_ColNames,BorrowingCostTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BorrowingCost);
    if(strcmpi(BorrowingCostTbl_Data,'No Data'))
        OutErrorMsg = {['Please upload data in borrowing_cost_table_qf5 for ',char(char(Settle_Date))]};
        return;
    end
    Overall_BorrowingCost_Tbl = cell2dataset([BorrowingCostTbl_ColNames;BorrowingCostTbl_Data]);
    
%     SqlQuery_InterestRate = ['select * from interest_rate_table_qf5 where settlement_date =''',char(Settle_Date),''' ' ];
    SqlQuery_InterestRate = ['select * from interest_rate_table_qf5 where settlement_date >=''',char(Settle_Date_Borrow_Interest),''' and settlement_date <=''',char(Trade_Date),''' and counterparty_parent = ''',char(Current_CounterParty),''' ' ];
    [InterestRateTbl_ColNames,InterestRateTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_InterestRate);
    if(strcmpi(InterestRateTbl_Data,'No Data'))
        OutErrorMsg = {['Please upload data in interest_rate_table_qf5 for ',char(char(Settle_Date))]};
        return;
    end
    Overall_InterestRate_Tbl = cell2dataset([InterestRateTbl_ColNames;InterestRateTbl_Data]);
    
    SqlQuery_SettleDate_Multiplier = 'select * from counterparty_days_per_year_qf5' ;
    [Multiplier_ColNames,Multiplier_Data] = Fetch_DB_Data(ObjDB,SqlQuery_SettleDate_Multiplier);
    Multiplier_Tbl = cell2dataset([Multiplier_ColNames;Multiplier_Data]);
    
    SqlQuery_Calculated_Date = 'select * from counterparty_settle_date_table_qf5' ;
    [CalculatedDate_ColNames,CalculatedDate_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Calculated_Date);
    CalculatedDate_Tbl = cell2dataset([CalculatedDate_ColNames;CalculatedDate_Data]);
    
    SqlQuery_Monthly_Reset_Table = 'select * from monthly_reset_date_table_qf5' ;
    [Monthly_Reset_Table_ColNames,Monthly_Reset_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Monthly_Reset_Table);
    Monthly_Reset_Table = cell2dataset([Monthly_Reset_Table_ColNames;Monthly_Reset_Table_Data]);
    
    Unique_CounterParty = Current_CounterParty ; % fetch(ObjDB,'select distinct(counterparty_parent) from monthly_reset_date_table_qf5');
    
    
    %% Find Offset Oriented Settle Date for Extract The Trades
    
    %     New_SettleDate_Trade_Extract = Trade_Date;
    %
    %     Matched_SettleDate_Index = find(strcmpi(Overall_BorrowingCost_Tbl.settlement_date,Trade_Date), 1);
    %     if(~isempty(Matched_SettleDate_Index))
    %         Offset_Value =  abs( InterestRate_Tbl.offset(strcmpi(InterestRate_Tbl.settlement_date,Trade_Date)) );
    %         if(Offset_Value > 0)
    %             while(Offset_Value)
    %                 fprintf('Entering Settle Date While Loop....\n') ;
    %                 New_SettleDate_Trade_Extract = {datestr(busdate(datenum(New_SettleDate_Trade_Extract,DB_Date_Format),-1),DB_Date_Format)} ;
    %                 Offset_Value = Offset_Value - 1 ;
    %             end
    %             Trade_Date = New_SettleDate_Trade_Extract; %% Settle Date Changed for Fetching Rate & Spread
    %         end
    %     else
    %         OutErrorMsg = {['For the settle date ',char(Trade_Date),' Data is not found in borrowing_cost_table_qf5 ']};
    %         return;
    %     end
    
    %% Fetching Trades
    
    Flag_Status = unique(Monthly_Reset_Table.flag_monthly_reset(strcmpi(Monthly_Reset_Table.reset_month,Reset_Month))) ;
    
    if(strcmpi(Flag_Status,'yes'))
        %         Temp_Table_Name = strcat('helper_evaluation_reports_store_table_',InBUName);  %% When Monthly Reset happen
        %         [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,[],Temp_Table_Name);
        
        Temp_Table_Name = strcat('helper_evaluation_reports_store_proc_table_',InBUName);
        SqlQuery = ['select * from ',Temp_Table_Name,' where value_date = ''',char(Current_ValueDate),''' and counterparty_parent =''',char(Current_CounterParty),''' '];
        [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        if strcmpi(DBData_All,'No Data')
            Overall_ErrorMsg = [Overall_ErrorMsg ;{['No Trades found for corresponding date in ',char(Unique_CounterParty)]}];
            return;
        end
        
    else
        %         SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''live'' and transaction_date <= ''',char(New_SettleDate_Trade_Extract),''' '] ;
        %         [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
        
        [ColNames,DBData_All,ObjDB_ErrorMsg] = Call_StoredProcedure(ObjDB,StoreProcedure_Name,Current_ValueDate); %% StoredProcedure
        
        %         DealStatus_Col = cellStrfind_exact(ColNames,{'deal_status'});
        %         TransactionDate_Col = cellStrfind_exact(ColNames,{'transaction_date'});
        %         Mathched_Index = strcmpi(DBData_All(:,DealStatus_Col),'live') & datenum(DBData_All(:,TransactionDate_Col)) <= datenum(New_SettleDate_Trade_Extract);
        %         DBData_All = DBData_All(Mathched_Index,:);
        
    end
    NettingStatus_Col = cellStrfind_exact(ColNames,{'netting_status'});
    NettingDate_Col = cellStrfind_exact(ColNames,{'netting_date'});
    
    Partially_with_Not_Netted_Index = strcmpi(DBData_All(:,NettingStatus_Col),'partially_netted') | strcmpi(DBData_All(:,NettingStatus_Col),'not_netted');
    
    FullyNetted_Index = strcmpi(DBData_All(:,NettingStatus_Col),'fully_netted') ;
    NettingDate_Index = logical(zeros(size(FullyNetted_Index)));
    NettingDate_Index(FullyNetted_Index) = datenum(DBData_All(FullyNetted_Index,NettingDate_Col)) > datenum(Current_ValueDate);
    
    Mathched_Index = Partially_with_Not_Netted_Index | NettingDate_Index ;
    DBData_All = DBData_All(Mathched_Index,:);
    
    
    %% For Finding Multiplier
    
    % HolDates = {'2016-01-01','2017-01-01'}; %% For Busines Days
    % HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    DayForm = 'long';
    language = 'en_US';
    % [Settle_Date_DayNumber,Settle_Date_DayName] = weekday(Settle_Date,DayForm,language);
    
    %     Multiplier_Settle_Date = CalculatedDate_Tbl.calculated_date(strcmpi(CalculatedDate_Tbl.settle_date,Settle_Date)) ; %% SettleDate + 3
    %     [Multiplier_Settle_Date_DayNumber,Multiplier_Settle_Date_DayName] = weekday(Multiplier_Settle_Date,DayForm,language);
    
    %%
    
    Overall_ErrorMsg = [];
    Counterparty_Col = cellStrfind_exact(ColNames,{'counterparty_parent'});
    TransationDate_Col = cellStrfind_exact(ColNames,{'transaction_date'});
    ResetDate_Col  = cellStrfind_exact(ColNames,{'reset_date'});
    
    for k = 1 : length(Unique_CounterParty)
        
        Current_CounterParty = Unique_CounterParty(k);
        
        Current_Counterparty_Index = strcmpi(DBData_All(:,Counterparty_Col),char(Current_CounterParty));
        Current_CounterParty_Data = DBData_All(Current_Counterparty_Index,:);
        if(isempty(Current_CounterParty_Data))
            Overall_ErrorMsg = [Overall_ErrorMsg ;{['No Trades found for corresponding date in ',char(Current_CounterParty)]}];
            continue;
        end
        
        %% Using Previous Month Reset Date to exclude the Dead Deals
        % ResetDate_Greater_Index - it means trades booked  during the
        % month
        % ResetDate_Less_Index &  ResetDate_Equal_Index - it means reset
        % deals booked previous month
        
        Previuos_ResetMonth_Data =  Monthly_Reset_Table(strcmpi(Monthly_Reset_Table.reset_month,Previous_Reset_Month) & strcmpi(Monthly_Reset_Table.counterparty_parent,Current_CounterParty) ,:) ;
        Previuos_ResetMonth_ResetDate =  Previuos_ResetMonth_Data.reset_date ;
        
        if(isempty(Previuos_ResetMonth_ResetDate))
            Overall_ErrorMsg = [Overall_ErrorMsg ;{['Not getting Reset Date in monthly reset table for  ',char(Current_CounterParty)]}];
            continue;
        end
        
        ResetDate_Greater_Index = datenum(Current_CounterParty_Data(:,TransationDate_Col),DB_Date_Format) >=  datenum(Previuos_ResetMonth_ResetDate,DB_Date_Format) ;
        ResetDate_Less_Index =  datenum(Current_CounterParty_Data(:,TransationDate_Col),DB_Date_Format) <  datenum(Previuos_ResetMonth_ResetDate,DB_Date_Format) ;
        
        ResetDate_Equal_Index = logical(zeros(size(ResetDate_Less_Index)));
        Empty_ResetDate_Index = strcmpi(Current_CounterParty_Data(:,ResetDate_Col),'') ;
        ResetDate_Equal_Index(~Empty_ResetDate_Index) = datenum(Current_CounterParty_Data(~Empty_ResetDate_Index,ResetDate_Col),DB_Date_Format) ==  datenum(Previuos_ResetMonth_ResetDate,DB_Date_Format) ;
        
        Matched_Index = ResetDate_Greater_Index | (ResetDate_Less_Index &  ResetDate_Equal_Index);
        
        Current_CounterParty_Data = Current_CounterParty_Data(Matched_Index,:) ;
        
        %%
        %         Multiplier_Settle_Date = CalculatedDate_Tbl.calculated_date(strcmpi(CalculatedDate_Tbl.settle_date,Settle_Date) & strcmpi(CalculatedDate_Tbl.counterparty_parent,Current_CounterParty)) ; %% SettleDate + 3
        [Multiplier_Settle_Date_DayNumber,Multiplier_Settle_Date_DayName] = weekday(Settle_Date,DayForm,language); %% Multiplier_Settle_Date
        
        %%
        
        if(size(Current_CounterParty_Data,2) > 1)
            
            
            Required_Header = {'transaction_date','instrument','bbg_product_code','product_name','active_lots','counterparty_parent','notional','current_premium'};
            UserWant_Col = cellStrfind_exact(ColNames,Required_Header);
            Required_Data = Current_CounterParty_Data(:,UserWant_Col);
            
            %% Temp Check
            xlswrite(OutXLSName,[Required_Header;Required_Data],'Original_Data');
            Current_Premiuim_Col = cellStrfind_exact(Required_Header,{'current_premium'});
            ActiveLots_Col = cellStrfind_exact(Required_Header,{'active_lots'});
            Notional_Col = cellStrfind_exact(Required_Header,{'notional'});
            
            Notional = num2cell( cell2mat(Required_Data(:,ActiveLots_Col)) .* cell2mat(Required_Data(:,Current_Premiuim_Col)) );
            Required_Data(:,Notional_Col) = Notional;
            
            Required_Header(:,Current_Premiuim_Col) = [];
            Required_Data(:,Current_Premiuim_Col) = [];
            
            %%% Newly added logic (27-06-2019) (if trade date = settele
            %%% date then remove that trade
            Date_index = strcmpi(Required_Data(:,1), Settle_Date);
            Required_Data(Date_index,:) = [];
            %%% End logic
            %% Consolidate Data
            
            UniqueFields = {'transaction_date','instrument','bbg_product_code','product_name','counterparty_parent'};
            SumFields = {'active_lots','notional'};
            OutputFields = Required_Header;
            WeightedAverageFields = [];
            [UserWant_Header,UserWant_Data] = consolidatedata(Required_Header, Required_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            %% Make Swaps_Type Col
            ActiveLots_Col = cellStrfind_exact(UserWant_Header,{'active_lots'});
            Negative_Index = cellfun(@(s) (s<0), UserWant_Data(:,ActiveLots_Col));
            Positive_Index = cellfun(@(s) (s>0), UserWant_Data(:,ActiveLots_Col));
            Zero_Index = cellfun(@(s) (s==0), UserWant_Data(:,ActiveLots_Col));
            
            %         Notional_Col = cellStrfind_exact(UserWant_Header,{'notional'});
            %         Zero_Index_Notional = cellfun(@(s) (s==0), UserWant_Data(:,Notional_Col));
            %         Zero_Index  = Zero_Index_OriginalLots & Zero_Index_Notional ;
            
            Swaps_Type = cell(size(UserWant_Data,1),1);
            Swaps_Type(Negative_Index,1) = {'Short'};
            Swaps_Type(Positive_Index,1) = {'Long'};
            
            UserWant_Header = [UserWant_Header , {'Swaps_Type'}];
            UserWant_Data = [UserWant_Data ,Swaps_Type] ;
            
            UserWant_Data(Zero_Index,:) = [] ; %% Remove Original Lots Zero
            
%             TransactionDate_Col = cellStrfind_exact(UserWant_Header,{'transaction_date'}) ; %% Same Date all Stoks becoz its consolidated without trade date
%             UserWant_Data(:,TransactionDate_Col) = Settle_Date;
            
            %%% Newly added code on 2019-07-16
            %%% if transaction_date = ResetMonth_Data.settle_date then
            %%% ResetMonth_Data.settle_date + 1 or ResetDate
            UserWant_Data = cell2dataset([UserWant_Header ;UserWant_Data ]);
            Index_ResetDate = strcmpi(UserWant_Data.transaction_date,Previuos_ResetMonth_Data.settle_date);
            if any(Index_ResetDate)
                Next_working_day = cellstr(datestr(busdate(Previuos_ResetMonth_Data.settle_date),'yyyy-mm-dd'));
                UserWant_Data.transaction_date(Index_ResetDate) = Next_working_day;
            end
            %%% End Code
            
            %%  Calculations
            
            Borrowing_Cost = cell(size(UserWant_Data,1),1) ;
            InterestRate_Receivable = cell(size(UserWant_Data,1),1) ;
            InterestRate_Payable = cell(size(UserWant_Data,1),1) ;
            
            for i = 1 : size(UserWant_Data,1)
                
                Current_TradeDate = UserWant_Data.transaction_date(i);  %% Same Date all Stoks becoz its consolidated without trade date
                Current_Instrument = UserWant_Data.instrument(i) ;
                Current_CounterParty = UserWant_Data.counterparty_parent(i);
                
                Multiplier_Tbl_Temp = Multiplier_Tbl(strcmpi(Multiplier_Tbl.counterparty_parent,Current_CounterParty),:);
                Current_Multiplier = Multiplier_Tbl_Temp.multiplier(strcmpi(Multiplier_Tbl_Temp.weekday,Multiplier_Settle_Date_DayName)) ;% No Need becoz user pass this multiplier
                Current_Multiplier = User_Passed_Multiplier;
                Current_NoOfDays = Multiplier_Tbl_Temp.no_of_days(strcmpi(Multiplier_Tbl_Temp.weekday,Multiplier_Settle_Date_DayName)) ;
                
                Current_Notional = UserWant_Data.notional(i); %% Notional = Current_OriginalLots *  Current_TradePrice
                Currrent_SwapsType = UserWant_Data.Swaps_Type(i);
                
                Settle_Date = Current_TradeDate; %% Only For CICC
                
                %% Borrowing Cost & InterestRate_Receivable Calculations for  'Short'
                if(strcmpi(Currrent_SwapsType,'Short'))
                    Instrument_Matched_Index = find(strcmpi(Overall_BorrowingCost_Tbl.stock_id,char(Current_Instrument)) & strcmpi(Overall_BorrowingCost_Tbl.counterparty_parent,char(Current_CounterParty)));
                    if(~isempty(Instrument_Matched_Index))
                        BorrowingCost_Tbl = Overall_BorrowingCost_Tbl(Instrument_Matched_Index,:);
                    else
                        OutErrorMsg_1 = {[char(Current_Instrument),' not available in borrowing_cost_table_qf5 for ',char(Current_CounterParty)]};
                        Overall_ErrorMsg = [Overall_ErrorMsg ;OutErrorMsg_1];
                        continue;
                    end
                    
                    %                     Matched_Borrowing_Index =  find(strcmpi(BorrowingCost_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(BorrowingCost_Tbl.settlement_date,Settle_Date), 1);
                    Matched_Borrowing_Index =  find(strcmpi(BorrowingCost_Tbl.stock_id,Current_Instrument) & strcmpi(BorrowingCost_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(BorrowingCost_Tbl.settlement_date,Settle_Date), 1);
                    if(~isempty(Matched_Borrowing_Index))
                        Current_Rate = BorrowingCost_Tbl.rate(Matched_Borrowing_Index);
                        Current_Spread = BorrowingCost_Tbl.spread(Matched_Borrowing_Index);
                        Current_Offset = BorrowingCost_Tbl.offset(Matched_Borrowing_Index);
                        
                        New_SettleDate = Settle_Date;
                        if(Current_Offset>0)
                            while(Current_Offset)
                                fprintf('Entering Borrowing Cost & InterestRate_Receivable While Loop....\n') ;
                                New_SettleDate = {datestr(busdate(datenum(New_SettleDate,DB_Date_Format),-1),DB_Date_Format)} ;
                                Current_Offset = Current_Offset - 1 ;
                            end
                            
                            New_Matched_Borrowing_Index =  find(strcmpi(BorrowingCost_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(BorrowingCost_Tbl.settlement_date,New_SettleDate), 1);
                            
                            % Current_Rate = BorrowingCost_Tbl.rate(New_Matched_Borrowing_Index);
                            % Current_Spread = BorrowingCost_Tbl.spread(New_Matched_Borrowing_Index);
                            %  Current_Offset = BorrowingCost_Tbl.offset(New_Matched_Borrowing_Index);
                            
                   %%% Newly added, round command upto 2 decimal
                   %%% points by new requirement from Aravind
                            if(datenum(Current_TradeDate,DB_Date_Format) <= datenum(New_SettleDate,DB_Date_Format))  %% TradeDate Check
                                Borrowing_Cost(i) = num2cell( round(Current_Notional .* (Current_Spread/100) .* (Current_Multiplier/Current_NoOfDays),2) ) ;
                                InterestRate_Receivable(i) = num2cell( round(Current_Notional .* (Current_Rate/100) .* (-Current_Multiplier/Current_NoOfDays),2) ) ;
                            end
                            
                        else
                            Borrowing_Cost(i) = num2cell( round(Current_Notional .* (Current_Spread/100) .* (Current_Multiplier/Current_NoOfDays),2) ) ;
                            InterestRate_Receivable(i) = num2cell( round(Current_Notional .* (Current_Rate/100) .* (-Current_Multiplier/Current_NoOfDays),2) ) ;
                            
                        end
                        
                    else
                        OutErrorMsg_1 = {[char(Current_Instrument),' :For the settle date ',char(Settle_Date),' Data is not found in borrowing_cost_table_qf5 for ',char(Current_CounterParty)]};
                        Overall_ErrorMsg = [Overall_ErrorMsg ;OutErrorMsg_1];
                        continue;
                    end
                    
                end
                
                %% Borrowing Cost & InterestRate_Payable Calculations for  'Long'
                
                if(strcmpi(Currrent_SwapsType,'Long'))
                    Instrument_Matched_Index = find(strcmpi(Overall_InterestRate_Tbl.stock_id,char(Current_Instrument)) & strcmpi(Overall_InterestRate_Tbl.counterparty_parent,char(Current_CounterParty)));
                    if(~isempty(Instrument_Matched_Index))
                        InterestRate_Tbl = Overall_InterestRate_Tbl(Instrument_Matched_Index,:);
                    else
                        OutErrorMsg_1 = {[char(Current_Instrument),' not available in interest_rate_table_qf5 for ',char(Current_CounterParty)]};
                        Overall_ErrorMsg = [Overall_ErrorMsg ;OutErrorMsg_1];
                        continue;
                    end
                    
                    %                     Matched_Borrowing_Index =  find(strcmpi(InterestRate_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(InterestRate_Tbl.settlement_date,Settle_Date), 1);
                    Matched_Borrowing_Index =  find(strcmpi(InterestRate_Tbl.stock_id,Current_Instrument) & strcmpi(InterestRate_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(InterestRate_Tbl.settlement_date,Settle_Date), 1);
                    if(~isempty(Matched_Borrowing_Index))
                        Current_Rate = InterestRate_Tbl.rate(Matched_Borrowing_Index);
                        Current_Spread = InterestRate_Tbl.spread(Matched_Borrowing_Index);
                        Current_Offset = InterestRate_Tbl.offset(Matched_Borrowing_Index);
                        
                        New_SettleDate = Settle_Date;
                        if(Current_Offset>0)
                            while(Current_Offset)
                                fprintf('Entering Borrowing Cost & InterestRate_Payable While Loop....\n') ;
                                New_SettleDate = {datestr(busdate(datenum(New_SettleDate,DB_Date_Format),-1),DB_Date_Format)} ;
                                Current_Offset = Current_Offset - 1 ;
                            end
                            
                            New_Matched_Borrowing_Index =  find(strcmpi(InterestRate_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(InterestRate_Tbl.settlement_date,New_SettleDate), 1);
                            
                            % Current_Rate = BorrowingCost_Tbl.rate(New_Matched_Borrowing_Index);
                            % Current_Spread = BorrowingCost_Tbl.spread(New_Matched_Borrowing_Index);
                            %  Current_Offset = BorrowingCost_Tbl.offset(New_Matched_Borrowing_Index);
                            
                   %%% Newly added, round command upto 2 decimal
                   %%% points by new requirement from Aravind
                            if(datenum(Current_TradeDate,DB_Date_Format) <= datenum(New_SettleDate,DB_Date_Format))  %% TradeDate Check
                                
                                InterestRate_Payable(i) = num2cell( round(( (Current_Rate/100) +  Current_Spread) .* Current_Notional .* (-Current_Multiplier/Current_NoOfDays),2) )  ;
                                
                            end
                            
                        else
                            
                            InterestRate_Payable(i) = num2cell( round(( (Current_Rate/100) +  Current_Spread) .* Current_Notional .* (-Current_Multiplier/Current_NoOfDays),2) )  ;
                            
                        end
                        
                    else
                        OutErrorMsg_1 = {[char(Current_Instrument),' :For the settle date ',char(Settle_Date),' Data is not found in interest_rate_table_qf5 for ',char(Current_CounterParty)]};
                        Overall_ErrorMsg = [Overall_ErrorMsg ;OutErrorMsg_1];
                        continue;
                    end
                    
                end
                
                
            end
            
            %% Merge Data
            
            UserWant_Data = dataset2cell(UserWant_Data);
            Header = UserWant_Data(1,:);
            UserWant_Data(1,:) = [] ;%% Remove_Header
            
            Output_Data = [UserWant_Data , Borrowing_Cost , InterestRate_Receivable , InterestRate_Payable] ;
            
            Output_Header = {'Settle Date','Stock ID','Stock No','Stock Name','Open Quantity','Counterparty','Notional','Swaps Type','Borrowing Cost','Interest Receivable','Interest Payable'};
            
            %% Uploading to 'borrow_interest_store_table_qf5'
            
            fprintf('Uploading Data into borrow_interest_store_table_qf5.....\n');
            
            TableName = 'borrow_interest_store_table_qf5';
            set(ObjDB,'AutoCommit','off');
            SqlQuery = ['delete from ',TableName,' where settle_date = ''',char(Trade_Date),''' and counterparty_parent = ''',char(Current_CounterParty),''' '];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            xlswrite(OutXLSName,[Output_Header ;Output_Data ],['Trade Date Data ',char(Current_CounterParty)] );
            
            % Consolidate Data            
            UniqueFields = {'Stock ID','Stock No','Stock Name','Counterparty','Swaps Type'};
            SumFields = {'Open Quantity','Notional','Borrowing Cost','Interest Receivable','Interest Payable'};
            OutputFields = [UniqueFields , SumFields];
            WeightedAverageFields = [];
            [~,Output_Data] = consolidatedata(Output_Header, Output_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            SettleDate_Data = cell(size(Output_Data,1),1);
            SettleDate_Data(:,:) = Trade_Date;
            Output_Data = [SettleDate_Data,Output_Data];
            
            Output_Header = [{'Settle Date'},OutputFields];
            
            upload_in_database(TableName, Output_Data);
            
            %% Excel Sheet Write &  Delete Sheets
            
%             Output_Header = {'Settle Date','Stock ID','Stock No','Stock Name','Open Quantity','Counterparty','Notional','Swaps Type','Borrowing Cost','Interest Receivable','Interest Payable'};
            
            
            SummarySheet_Header = {'Settle Date','Stock No','Open Quantity','Notional','Counterparty','Borrowing Cost','Interest Receivable','Interest Payable'};
            SummarySheet_Col = cellStrfind_exact(Output_Header,SummarySheet_Header);
            Summary_Data = Output_Data(:,SummarySheet_Col);
            
            xlswrite(OutXLSName,[Output_Header ;Output_Data ],['Consolidate Data ',char(Current_CounterParty)] );
            xlswrite(OutXLSName,[SummarySheet_Header ;Summary_Data ],['Summary ',char(Current_CounterParty)] );
            
            
            
        else
            %             OutErrorMsg = {['No Trades found for corresponding date in ',ViewName]};
            Overall_ErrorMsg = {['No Trades found for corresponding date in ',char(Current_CounterParty)]};
        end
        
    end
    
    %% Delete Sheets
    try
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        
        if(~isempty(Overall_ErrorMsg))
            xlswrite(OutXLSName,Overall_ErrorMsg,' Error Sheet');
        end
        
    catch
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end