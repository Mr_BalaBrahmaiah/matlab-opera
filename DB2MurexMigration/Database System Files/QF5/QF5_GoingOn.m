InBUName = 'qf5';

OutErrorMsg = {'No Error'};

OutXLSName = getXLSFilename('QF5_Realized_Report_Cumulative-FTM');

%% Fetching Data
ObjDB = connect_to_database;

Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');

Default_Name = 'helper_evaluation_reports_view_';
ViewName = [Default_Name,InBUName] ;

SqlQuery = ['select * from ',char(ViewName),' where transaction_date = ''',char(Settle_Date),''' '];
[ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName

%% Initialize Output Col

TradeDate_Col = 1; %% transaction_date
StockNo_Col = 2; %% product_code
StockName_Col = 3; %% product_name
OpenQuantity_Col = 4; %% active_lots
TradePrice_Col = 5; %% current_premium

MarketPrice_Col = 6; %% settle_price
UnRealisedPnL_USD_Col = 7; %% unrealised_mtm_usd
BGN_SpotRate_Col = 8; %% bgn_spot_rate

%%
try
    if(size(DBData_All,2) > 1)
        
        %% Consolidate Data
        %         UniqueFields = {'transaction_date','product_code','product_name','market_action','original_premium'};
        %         SumFields = {'active_lots'};
        %         OutputFields = [UniqueFields,SumFields];
        %         WeightedAverageFields = [];
        %         [Consolidate_Fields,Consolidate_Data] = consolidatedata(ColNames,DBData_All,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        Consolidate_Fields = ColNames ;
        Consolidate_Data = DBData_All ;
        
        Consolidate_TransDate_Col = cellStrfind_exact(Consolidate_Fields,{'transaction_date'});
        Consolidate_ProdCode_Col = cellStrfind_exact(Consolidate_Fields,{'product_code'});
        Consolidate_ProdName_Col = cellStrfind_exact(Consolidate_Fields,{'product_name'});
        Consolidate_ActiveLots_Col = cellStrfind_exact(Consolidate_Fields,{'active_lots'});
        Consolidate_CurrentPremium_Col = cellStrfind_exact(Consolidate_Fields,{'current_premium'});
        Consolidate_SettlePrice_Col = cellStrfind_exact(Consolidate_Fields,{'settle_price'});
        Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(Consolidate_Fields,{'unrealised_mtm_usd'});
        Consolidate_BGNSpot_Col = cellStrfind_exact(Consolidate_Fields,{'bgn_spot_rate'});
        
        OutputData = cell(size(Consolidate_Data,1),8); 
        
        for i = 1 : size(Consolidate_Data,1)
            
            OutputData(i,TradeDate_Col) = Consolidate_Data(i , Consolidate_TransDate_Col);
            OutputData(i,StockNo_Col) = Consolidate_Data(i , Consolidate_ProdCode_Col);
            OutputData(i,StockName_Col) = Consolidate_Data(i ,Consolidate_ProdName_Col );
            
            OutputData(i,OpenQuantity_Col) = Consolidate_Data(i ,Consolidate_ActiveLots_Col);
            OutputData(i,TradePrice_Col) = Consolidate_Data(i , Consolidate_CurrentPremium_Col);
            OutputData(i,MarketPrice_Col) = Consolidate_Data(i , Consolidate_SettlePrice_Col);
            
            
            OutputData(i,UnRealisedPnL_USD_Col) = Consolidate_Data(i , Consolidate_RealizedM2M_USD_Col) ;
            OutputData(i,BGN_SpotRate_Col) = Consolidate_Data(i , Consolidate_BGNSpot_Col);
            
            
        end
        
        %% Excel Sheet Write &  Delete Sheets
        
        Output_Header = {'Trade Date','Stock No','Stock Name','Open Quantity','Trade Price','Market Price','UnRealised PnL (USD)','BGN Spot Rate'};
                
        xlswrite(OutXLSName,[Output_Header ;OutputData ] );
        try
            OutXLSName = fullfile(pwd,OutXLSName);
            xls_delete_sheets(OutXLSName);
        catch
        end
    else
        msgbox('No Data helper_funds_reports_view_qf5');
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end