function [OutErrorMsg ,OutXLSName ] = QF5_Borrowing_InterestRate_Calculation(InBUName,Trade_Date,Settle_Date,User_Passed_Multiplier)

%%% [OutErrorMsg ,OutXLSName ] = QF5_Borrowing_InterestRate_Calculation('qf5','2019-05-03','2019-05-03',1)

% Using Stored Procecdure
try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_Borrowing_InterestRate');
    
    %% Fetching Data
    
    ObjDB = connect_to_database;
    
    %% Monthly Reset Table
    SqlQuery_Monthly_Reset_Table = 'select * from monthly_reset_date_table_qf5' ;
    [Monthly_Reset_Table_ColNames,Monthly_Reset_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Monthly_Reset_Table);
    Monthly_Reset_Table = cell2dataset([Monthly_Reset_Table_ColNames;Monthly_Reset_Table_Data]);
    
    Unique_CounterParty = fetch(ObjDB,'select distinct(counterparty_parent) from monthly_reset_date_table_qf5');
    %Unique_CounterParty = fetch(ObjDB,'select distinct(counterparty_parent) from monthly_reset_date_table_qf5 where counterparty_parent not like ''GS%''');

    %% User_Passed_Multiplier Check
    
    if(ischar(User_Passed_Multiplier))
        User_Passed_Multiplier = str2double(User_Passed_Multiplier); %% 0 to 9 only
    end
    
    fprintf('User Passed Multiplier : %d\n',User_Passed_Multiplier); %% From FrontEnd
    
    if ~exist('User_Passed_Multiplier','var')
        User_Passed_Multiplier = 1;
    end
    
    if exist('User_Passed_Multiplier','var')
        if(User_Passed_Multiplier == 0)
            User_Passed_Multiplier = 1;
        end
    end
    fprintf('User Passed Multiplier : %d\n',User_Passed_Multiplier); %% After Code Validate
    
    MS_ConsolidateData = []; CICC_ConsolidateData = []; GS_ConsolidateData = [];
    MS_SummaryData=[];       CICC_SummaryData=[];   GS_SummaryData=[];
    
    %%
    for k = 1 : length(Unique_CounterParty)
        
        Current_CounterParty = Unique_CounterParty(k);
        
        %% MORGAN STANLEY & MS
        if (strcmpi(Current_CounterParty,'MORGAN STANLEY') || strncmpi(Current_CounterParty,'MS',2))
            [OutErrorMsg ,OutXLSName,Output_Header ,Output_Data,SummarySheet_Header, Summary_Data ] = QF5_Borrowing_InterestRate_Calculation_MS(InBUName,Trade_Date,Settle_Date,User_Passed_Multiplier,Current_CounterParty,OutXLSName);
            if(~strcmpi(OutErrorMsg,'No Errors'))
                continue; %% added on 12-12-2019, replaceing the "return" command to "continue" command
            end
        % Raghav - To append each counterparty data into overall varialble
            MS_ConsolidateData = [MS_ConsolidateData ; Output_Data];
            MS_SummaryData = [MS_SummaryData ; Summary_Data];
        end
        
        %% GS  ---> NEwly added code
        if (strncmpi(Current_CounterParty,'GS',2))
            [OutErrorMsg ,OutXLSName,Output_Header1 ,Output_Data1,SummarySheet_Header1, Summary_Data1 ] = QF5_Borrowing_InterestRate_Calculation_MS(InBUName,Trade_Date,Settle_Date,User_Passed_Multiplier,Current_CounterParty,OutXLSName);
            if(~strcmpi(OutErrorMsg,'No Errors'))
                continue;
            end
        % Raghav - To append each counterparty data into overall varialble
            GS_ConsolidateData = [GS_ConsolidateData ; Output_Data1];
            GS_SummaryData = [GS_SummaryData ; Summary_Data1];
        end
        
        %% CICC & CICC-SUB
        if (strcmpi(Current_CounterParty,'CICC') || contains(upper(Current_CounterParty),'CICC-SUB'))   %%% newly added line (30-04-2019)
            [OutErrorMsg ,OutXLSName,Output_Header2 ,Output_Data2,SummarySheet_Header2, Summary_Data2] = QF5_Borrowing_InterestRate_Calculation_CICC(InBUName,Trade_Date,Settle_Date,User_Passed_Multiplier,Current_CounterParty,OutXLSName);
            if(~strcmpi(OutErrorMsg,'No Errors'))
                continue;
            end
            CICC_ConsolidateData = [CICC_ConsolidateData ; Output_Data2];   %% Newly added code
            CICC_SummaryData = [CICC_SummaryData ; Summary_Data2];
        end
        
        
    end
    % Raghav - Finally to write the overall variable data into excel sheet
    %%% Old Code
%     xlswrite(OutXLSName,[Output_Header ; Overall_ConsolidateData ],'MS-ALL-ConsolidateData');
%     xlswrite(OutXLSName,[SummarySheet_Header ; Overall_SummaryData ],'MS-ALL-SummaryData');
    
     %%Newly added code  %% Newly added if-end condition on 14-06-2016
     if ~isempty(MS_ConsolidateData)
        Output_Header = {'Settle Date','Stock ID','Stock No','Stock Name','Open Quantity','Counterparty','Notional','Swaps Type','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[Output_Header ; MS_ConsolidateData ],'MS-ALL-ConsolidateData');
     end
     if ~isempty(GS_ConsolidateData)
        Output_Header1 = {'Settle Date','Stock ID','Stock No','Stock Name','Open Quantity','Counterparty','Notional','Swaps Type','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[Output_Header1 ; GS_ConsolidateData ],'GS-ALL-ConsolidateData' );
     end
     if ~isempty(CICC_ConsolidateData)
        Output_Header2 = {'Settle Date','Stock ID','Stock No','Stock Name','Open Quantity','Counterparty','Notional','Swaps Type','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[Output_Header2 ; CICC_ConsolidateData ],'CICC-ALL-ConsolidateData');
     end
     
     if ~isempty(MS_SummaryData)
        SummarySheet_Header = {'Settle Date','Stock No','Open Quantity','Notional','Counterparty','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[SummarySheet_Header ; MS_SummaryData ],'MS-ALL-SummaryData');
     end
     if ~isempty(GS_SummaryData)
        SummarySheet_Header1 = {'Settle Date','Stock No','Open Quantity','Notional','Counterparty','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[SummarySheet_Header1 ; GS_SummaryData ],'GS-ALL-SummaryData' );
     end  
     if ~isempty(CICC_SummaryData)
        SummarySheet_Header2 = {'Settle Date','Stock No','Open Quantity','Notional','Counterparty','Borrowing Cost','Interest Receivable','Interest Payable'};
        xlswrite(OutXLSName,[SummarySheet_Header2 ; CICC_SummaryData ],'CICC-ALL-SummaryData');
     end  
    %%% End code
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end