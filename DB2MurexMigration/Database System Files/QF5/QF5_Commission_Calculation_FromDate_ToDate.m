function [OutErrorMsg] = QF5_Commission_Calculation_FromDate_ToDate()

InBUName = 'qf5';

try
    OutErrorMsg = {'No Errors'};
    
    ObjDB = connect_to_database ;
    Current_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %%
    
    Prompt        = {'Enter the From Date(yyyy-mm-dd format):','Enter the From Date(yyyy-mm-dd format):'};
    Name          = 'Date Input for QF5 Commission Parser';
    Numlines      = 1;
    Defaultanswer = {char(Current_SettleDate),char(Current_SettleDate)};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    FromDate = InputDates(1);
    ToDate = InputDates(2);
    
    %% Overall Day
    SqlQuery_Overall_SettleDate_ValueDate =['select * from vdate_sdate_table where settlement_date >=''',char(FromDate),''' and settlement_date <=''',char(ToDate),''' ' ];
    [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %%
    hWaitbar = waitbar(0,'Please wait...');
    
    for i = 1 : length(Overall_SettleDate)
        Current_ValueDate = Overall_ValueDate(i);
        Current_SettleDate = Overall_SettleDate(i);
        
        [OutErrorMsg ,OutXLSName ] = QF5_Commission_Calculation(InBUName,Current_SettleDate);
        
        if(strcmpi(OutErrorMsg,'No Errors'))
            TimeStamp = strsplit(datestr(now),' ');
            TimeStamp = strrep(TimeStamp(end),':','-');
            Date_TimeStamp = [char(datestr(Current_ValueDate)),'_',char(TimeStamp)];
            Final_XLS_FileName = strcat('QF5_Commission_',Date_TimeStamp,'.xlsx'); %%  We Need to move to Static Report Page
            
            copyfile(OutXLSName,fullfile(pwd,Final_XLS_FileName));
            
            delete(fullfile(pwd,OutXLSName));
            
        else
            continue;
        end
        
        waitbar(i/length(Overall_SettleDate),hWaitbar,[char(Current_SettleDate), ' Finished']);
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end