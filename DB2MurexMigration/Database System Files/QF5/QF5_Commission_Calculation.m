function [OutErrorMsg ,OutXLSName ] = QF5_Commission_Calculation(InBUName,Temp_Settle_Date)

% Settle_Date
try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_Commission');
    
    %% First We need to Check monthly_reset happpen or not
    
    ObjDB = connect_to_database;
    
    if(exist('Temp_Settle_Date','var'))
        Settle_Date = cellstr(Temp_Settle_Date);
        Reset_Month = datestr(Settle_Date,'mmm-yy');
    else
        Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
        Reset_Month = datestr(Settle_Date,'mmm-yy');
    end
    
    %% Check
    MonthlyReset_Tbl_Name = strcat('monthly_reset_date_table_',char(InBUName));
    SqlQuery = ['select flag_monthly_reset from ',MonthlyReset_Tbl_Name,' where reset_month = ''',char(Reset_Month),''' '] ;
    MonthlyReset_Flag_Status = fetch(ObjDB,SqlQuery);
    
    Unique_CounterParty = fetch(ObjDB,['select distinct(counterparty_parent) from ',MonthlyReset_Tbl_Name]);
    
    %     if(strcmpi(MonthlyReset_Flag_Status,'YES'))
    %         OutErrorMsg = {'Commission Report cannot be run after monthly reset process'};
    %         return;
    %     end
    
    
    %% Old Logic
    %     Default_Name = 'helper_evaluation_reports_view_';
    %     ViewName = [Default_Name,InBUName] ;
    %     SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''live'' and transaction_date = ''',char(Settle_Date),''' '];
    
    %% New Logic Change %% Data from commission_weighted_average_deal_table_qf5
    
    Default_Name = 'commission_weighted_average_deal_view_';
    TableName = [Default_Name,InBUName] ;
    SqlQuery = ['select * from ',char(TableName),' where deal_status = ''live'' and transaction_date = ''',char(Settle_Date),''' '];
    
    %% Fetching Data
    
    [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
    
    if(strcmpi(DBData_All,'No Data'))
        OutErrorMsg = {'No Data Found..! for Corresponding Settls Date'};
        return;
    end
    
    SqlQuery_Commision = 'select * from commission_rate_table_qf5' ;
    [CommisionRateTbl_ColNames,CommisionRateTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Commision);
    Overall_CommisionRate_Tbl = cell2dataset([CommisionRateTbl_ColNames;CommisionRateTbl_Data]);
    
    %% For Busines Days
    HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    System_Date_Format = 'dd-mm-yyyy';
    DB_Date_Format = 'yyyy-mm-dd';
    
    %%
    Overall_ErrorMsg = [];
    Counterparty_Col = cellStrfind_exact(ColNames,{'counterparty_parent'});
    CICC_CounterpartyData = []; MS_CounterpartyData = [];  GS_CounterpartyData = [];
    
    for k = 1 : length(Unique_CounterParty)
        
        Current_CounterParty = Unique_CounterParty(k);
        
        Current_Counterparty_Index = strcmpi(DBData_All(:,Counterparty_Col),char(Current_CounterParty));
        Current_CounterParty_Data = DBData_All(Current_Counterparty_Index,:);
        
        if(isempty(Current_CounterParty_Data))
            Overall_ErrorMsg = [Overall_ErrorMsg ; {['No Trades found for corresponding date in ',char(Current_CounterParty)]} ];
            continue;
        end
        
        if(size(Current_CounterParty_Data,2) > 1)
            
            
            UserWant_Header = {'transaction_date','instrument','product_code','product_name','original_lots','current_premium','counterparty_parent',...
                'equity_swaps_trade_type','market_action','notional'};
            UserWant_Col = cellStrfind_exact(ColNames,UserWant_Header);
            UserWant_Data = Current_CounterParty_Data(:,UserWant_Col);
            UserWant_Data = cell2dataset([UserWant_Header ;UserWant_Data ]);
            
            %% Commision Calculations
            %         Commision_Rate = cell(size(UserWant_Data,1),1) ;
            Commission = cell(size(UserWant_Data,1),1) ;
            Exch_Fees = cell(size(UserWant_Data,1),1) ;
            Sales_Tax = cell(size(UserWant_Data,1),1) ;
            Overall_Sum = cell(size(UserWant_Data,1),1) ;
            Amount = cell(size(UserWant_Data,1),1);
            
            for i = 1 : size(UserWant_Data,1)
                
                Current_TradeDate = UserWant_Data.transaction_date(i);  %% For Commison
                Current_Instrument = UserWant_Data.instrument(i) ;
                Current_CounterParty = UserWant_Data.counterparty_parent(i);
                Current_MarketAction = UserWant_Data.market_action(i);
                Current_Notional = UserWant_Data.notional(i);
                Current_TradeType = UserWant_Data.equity_swaps_trade_type(i);
                Current_TradeQuantity = UserWant_Data.original_lots(i);
                Current_TradePrice = UserWant_Data.current_premium(i);
                
                Instrument_Matched_Index = find(strcmpi(Overall_CommisionRate_Tbl.stock_id,char(Current_Instrument)) & strcmpi(Overall_CommisionRate_Tbl.counterparty_parent,char(Current_CounterParty)));
                if(~isempty(Instrument_Matched_Index))
                    CommisionRate_Tbl = Overall_CommisionRate_Tbl(Instrument_Matched_Index,:);
                else
                    OutErrorMsg = {[char(Current_Instrument),' not available in commission_rate_table_qf5']};
                    return;
                end
                
                if(isempty(cellStrfind(lower(Current_TradeType),{'recall'}))) %% No Need Calculation for Recall Trades
                    
                    Matched_CounterParty_Commision = cellStrfind_exact(CommisionRate_Tbl.counterparty_parent,Current_CounterParty);
                    for ii = 1 : length(Matched_CounterParty_Commision)
                        Current_Start_TradeDate = CommisionRate_Tbl.start_trade_date(Matched_CounterParty_Commision(ii)) ;
                        Current_End_TradeDate = CommisionRate_Tbl.end_trade_date(Matched_CounterParty_Commision(ii)) ;
                        Current_Commision = CommisionRate_Tbl.commission(Matched_CounterParty_Commision(ii));
                        Current_MarketFees = CommisionRate_Tbl.market_fees(Matched_CounterParty_Commision(ii));
                        Current_SalesTax = CommisionRate_Tbl.sales_tax((Matched_CounterParty_Commision(ii)));
                        
                        
                        VBusDays = busdays(datenum(Current_Start_TradeDate,DB_Date_Format), datenum(Current_End_TradeDate,DB_Date_Format),'daily',HolidayVec);
                        BusDays_Array = cellstr(datestr(VBusDays,DB_Date_Format));
                        
                        Matched_TradeDate = cellStrfind_exact(BusDays_Array,Current_TradeDate) ;
                        if(~isempty(Matched_TradeDate))
                            Commission(i) = num2cell( Current_TradePrice .* Current_Commision );
                            Exch_Fees(i) = num2cell ( Current_TradePrice .* Current_MarketFees );
                            
                            if(strcmpi(Current_MarketAction,'Bought'))
                                Sales_Tax(i) = num2cell(0);
                            else
                                Sales_Tax(i) = num2cell( Current_TradePrice .* Current_SalesTax );
                            end
                            
                            if(strcmpi(Current_CounterParty,'CICC') || contains(upper(Current_CounterParty),'CICC-SUB'))    %%% newly added line
                                Overall_Sum(i) = num2cell( nansum(cell2mat(Commission(i))) + nansum(cell2mat(Exch_Fees(i))) + nansum(cell2mat(Sales_Tax(i))) );
                                Amount(i) = num2cell( round(cell2mat(Overall_Sum(i)) .*  abs(Current_TradeQuantity), 2) );
                            %%% newly added line 29-11-2019
                            elseif strncmpi(char(Current_CounterParty),'GS',2)
                                %%% remove round off for only GS Trades
                                Overall_Sum(i) = num2cell(nansum(cell2mat(Commission(i))) + nansum(cell2mat(Exch_Fees(i))) + nansum(cell2mat(Sales_Tax(i))));
                                Amount(i) = num2cell( cell2mat(Overall_Sum(i)) .*  abs(Current_TradeQuantity) );
                            else
                                Overall_Sum(i) = num2cell(round( nansum(cell2mat(Commission(i))) + nansum(cell2mat(Exch_Fees(i))) + nansum(cell2mat(Sales_Tax(i))), 4));
                                Amount(i) = num2cell( cell2mat(Overall_Sum(i)) .*  abs(Current_TradeQuantity) );    
                            end
                            
                        else
                            %Messge
                        end
                    end
                    
                end
            end
            
            %% Merge Data
            
            UserWant_Data = dataset2cell(UserWant_Data);
            Header = UserWant_Data(1,:);
            UserWant_Data(1,:) = [] ;%% Remove_Header
            
            %         Amount = num2cell( cell2mat(Commission) + cell2mat(Exch_Fees) + cell2mat(Sales_Tax) );
            
            Output_Data = [UserWant_Data , Commission , Exch_Fees  , Sales_Tax , Amount] ;
            
            %% Uploading Data into 'commission_store_table_qf5'
            
            fprintf('Uploading Data into commission_store_table_qf5.....\n');
            
            TableName = 'commission_store_table_qf5';
            set(ObjDB,'AutoCommit','off');
            SqlQuery = ['delete from ',TableName,' where trade_date = ''',char(Settle_Date),''' and counterparty_parent = ''',char(Current_CounterParty),''' '];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            upload_in_database(TableName, Output_Data);
            
            
            %% Excel Sheet Write &  Delete Sheets
            
            Output_Header = {'Trade Date','Stock ID','Stock No','Stock Name','Trade Quantity','Trade Price','Counterparty','Trade Type','Market Action','Notional','Commision','Exch Fees','Sales Tax','Amount'};
            
            xlswrite(OutXLSName,[Output_Header ;Output_Data ] ,char(Current_CounterParty));
            
            %if(upper(char(Current_CounterParty))~='CICC')
       %%% Old Code
%             if(~strcmpi(char(Current_CounterParty),'CICC') && ~contains(upper(Current_CounterParty),'CICC-SUB'))    %%% newly added line
%                 Overall_CounterpartyData = [Overall_CounterpartyData ; Output_Data];
%             end
            %%% Newly added code
           if(strcmpi(char(Current_CounterParty),'CICC') || contains(upper(Current_CounterParty),'CICC-SUB'))    %%% newly added line
                CICC_CounterpartyData = [CICC_CounterpartyData ; Output_Data];
            end
            if strcmpi(char(Current_CounterParty),'MORGAN STANLEY') || strncmpi(char(Current_CounterParty),'MS',2)    %%% newly added line
                MS_CounterpartyData = [MS_CounterpartyData ; Output_Data];
            end

             if strncmpi(char(Current_CounterParty),'GS',2)    %%% newly added line
                GS_CounterpartyData = [GS_CounterpartyData ; Output_Data];
            end
      %%%% End Code      
        else
            %             OutErrorMsg = {['No Trades found for corresponding date in ',ViewName]};
            Overall_ErrorMsg = [Overall_ErrorMsg ; {['No Trades found for corresponding date in ',char(Current_CounterParty)]} ];
        end
        
    end
    %% Delete Sheets
    try
        xlswrite(OutXLSName,[Output_Header ; CICC_CounterpartyData ],'CICC-ALL' );
        xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        xlswrite(OutXLSName,[Output_Header ; GS_CounterpartyData ],'GS-ALL' );
        
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        
        if(~isempty(Overall_ErrorMsg))
            xlswrite(OutXLSName,Overall_ErrorMsg,' Error Sheet');
        end
        
    catch
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end