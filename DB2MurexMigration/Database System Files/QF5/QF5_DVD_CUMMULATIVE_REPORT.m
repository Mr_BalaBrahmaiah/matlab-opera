function [OutErrorMsg,OutXLSName] = QF5_DVD_CUMMULATIVE_REPORT(InBUName)
%%% [OutErrorMsg,OutXLSName] = QF5_DVD_CUMMULATIVE_REPORT('qf5')

try
    
	OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename('DVD_CUMMULATIVE_REPORT');  %% output file name

    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');

    %%% DVD_Report_Store_Table   
    DVD_Report_Store_Table_Name = strcat('dividend_report_store_table_',InBUName);
    SqlQuery_DVD_Report_Store_Table = ['select * from ',char(DVD_Report_Store_Table_Name),] ;
    [DVD_Report_Store_ColNames,DVD_Report_Store_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_Report_Store_Table);

    if strcmpi(DVD_Report_Store_Table_Data,'No Data')
        OutErrorMsg = {['Data not available in ',char(DVD_Report_Store_Table_Name)]} ;
        return;
    end

    Master_DVD_Cummulative_Data = cell2dataset([DVD_Report_Store_ColNames;DVD_Report_Store_Table_Data]);

    %%% Newly added code  (CUMMULATIVE REPORT is only for current year ex_dates's)
    %%% Fetch data from financial_year_table  
    financial_year_sql_query = 'select * from financial_year_table';
    [financial_year_ColNames,financial_year_Table_Data] = Fetch_DB_Data(ObjDB,financial_year_sql_query);

    if strcmpi(financial_year_Table_Data,'No Data')
        OutErrorMsg = {'Data not available in financial_year_table'} ;
        return;
    end
    
    financial_year_Data = cell2dataset([financial_year_ColNames;financial_year_Table_Data]); %%% cell to dataset

    %%% get the current year
    FY_Year = {['FY-',datestr(Settele_date,'yyyy')]};  
    
    %%% Compare the current year with financial_year_table
    %%% get the start date and end date
    Index_year = strcmpi(FY_Year,financial_year_Data.financial_year);
    Start_End_Data = financial_year_Data(Index_year,:);  %%% 
    
    %%% To find total Bussiness days from start date to end date
    DB_Date_Format = 'yyyy-mm-dd';
    HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd'); 
    
    VBusDays = busdays(datenum(Start_End_Data.start_date,DB_Date_Format), datenum(Start_End_Data.end_date,DB_Date_Format),'daily',HolidayVec);
    BusDays_Array = cellstr(datestr(VBusDays,DB_Date_Format));
    
    %%% check the ex_date's are present in current year or not
    Index_year_days = ismember(Master_DVD_Cummulative_Data.ex_date,BusDays_Array);
    Master_DVD_Cummulative_Data = Master_DVD_Cummulative_Data(Index_year_days,:);
    
    %%% convert dataset 2 cell
    Master_DVD_Cummulative_Year_Data = dataset2cell(Master_DVD_Cummulative_Data);
    DVD_Report_Store_Table_Data = Master_DVD_Cummulative_Year_Data(2:end,:);
    %%% End code
    
    %%%% DVD_FX_RATE
    DVD_FX_Rate_Table_Name = strcat('dividend_fx_rate_table_',InBUName);
    SqlQuery_DVD_FX_Rate = ['select * from ',char(DVD_FX_Rate_Table_Name),] ;
    [~,DVD_FX_Rate_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_FX_Rate);

    if strcmpi(DVD_FX_Rate_Table_Data,'No Data')
        OutErrorMsg = {'No Data found in ' ,char(DVD_FX_Rate_Table_Name),' !'};
        return;
    end

    %%%% Get the ex_fx_rate and payable_fx_rate
    FXR_bbg_product_code = DVD_FX_Rate_Table_Data(:,1);
    FXR_counterparty_parent = DVD_FX_Rate_Table_Data(:,2);
    FXR_ex_date = DVD_FX_Rate_Table_Data(:,4);
    FXR_stock_type = DVD_FX_Rate_Table_Data(:,8);

    [row,~]=size(DVD_Report_Store_Table_Data);
    ex_fx_rate_val = []; payable_fx_rate_val = []; Missing_EX_Rate = []; Fx_payable_date = [];
    for ii = 1:1:row
        bbg_product_code = DVD_Report_Store_Table_Data(ii,4);
        counterparty_parent = DVD_Report_Store_Table_Data(ii,1);
        ex_date = DVD_Report_Store_Table_Data(ii,6);
        stock_type = DVD_Report_Store_Table_Data(ii,11);

        IndexFx = strcmpi(bbg_product_code,FXR_bbg_product_code) & strcmpi(counterparty_parent,FXR_counterparty_parent) & strcmpi(ex_date,FXR_ex_date) & strcmpi(stock_type,FXR_stock_type);
        %%% EX_FX Rate
        if ~any(IndexFx)
            EX_miss_all_values = strcat(bbg_product_code,{' - '},counterparty_parent);
            Missing_EX_Rate = [Missing_EX_Rate,EX_miss_all_values];  
        else  
            ex_dvd_fx_rate = DVD_FX_Rate_Table_Data(IndexFx,5);
            payable_dvd_fx_rate = DVD_FX_Rate_Table_Data(IndexFx,7);
            Fx_tabl_payable_date = DVD_FX_Rate_Table_Data(IndexFx,6);

            ex_fx_rate_val = vertcat(ex_fx_rate_val,ex_dvd_fx_rate);           % ex_fx_rate
            payable_fx_rate_val = vertcat(payable_fx_rate_val,payable_dvd_fx_rate);    % payable_fx_rate
            Fx_payable_date = vertcat(Fx_payable_date,Fx_tabl_payable_date);
        end
    end  

    Missing_EX_Rate = convertCell2Char(unique(Missing_EX_Rate));
    if ~isempty(Missing_EX_Rate)
        OutErrorMsg = {[Missing_EX_Rate,' ex_fx_rate not found in ',char(DVD_FX_Rate_Table_Name)]};
        return;
    end

    Master_DVD_Cummulative_Data.ex_fx_rate = ex_fx_rate_val; 
    Master_DVD_Cummulative_Data.payable_fx_rate = payable_fx_rate_val;
    Master_DVD_Cummulative_Data.payable_date = Fx_payable_date;

    %%% Currency_Spot_Mapping
    Currency_Spot_Table_Name = 'currency_spot_mapping_table';
    SqlQuery_DVD_FX_Rate = ['select * from ',char(Currency_Spot_Table_Name),] ;
    [~,Currency_Spot_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_FX_Rate);

    if strcmpi(Currency_Spot_Table_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Currency_Spot_Table_Name)]} ;
        return;
    end
    Mapping_currency = Currency_Spot_Table_Data(:,1);
    
    %%%% To decide the multiplication or division based on curreny
    NET_EX_DVD_USD_val = []; NET_PAYABLE_DVD_USD_val = []; Missing_Currency = [];
    for q = 1:1:row
        Single_currency = Master_DVD_Cummulative_Data.currency(q);
        Index = strcmpi(Single_currency,Mapping_currency);

        if ~any(Index)
            Missing_Currency = [Missing_Currency,Single_currency];
            continue;
        end

        quantotype = Currency_Spot_Table_Data(Index,3);
        if (strcmpi(quantotype,'division'))
            %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
            NET_EX_DVD_USD = Master_DVD_Cummulative_Data.net_dvd_nc(q) ./ cell2mat(Master_DVD_Cummulative_Data.ex_fx_rate(q));
            NET_PAYABLE_DVD_USD = Master_DVD_Cummulative_Data.net_dvd_nc(q) ./ cell2mat(Master_DVD_Cummulative_Data.payable_fx_rate(q)); 

            NET_EX_DVD_USD_val = vertcat(NET_EX_DVD_USD_val,NET_EX_DVD_USD);
            NET_PAYABLE_DVD_USD_val = vertcat(NET_PAYABLE_DVD_USD_val,NET_PAYABLE_DVD_USD);
        elseif (strcmpi(quantotype,'multiplication'))
            %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
            NET_EX_DVD_USD = Master_DVD_Cummulative_Data.net_dvd_nc(q) .* cell2mat(Master_DVD_Cummulative_Data.ex_fx_rate(q));
            NET_PAYABLE_DVD_USD = Master_DVD_Cummulative_Data.net_dvd_nc(q) .* cell2mat(Master_DVD_Cummulative_Data.payable_fx_rate(q)); 

            NET_EX_DVD_USD_val = vertcat(NET_EX_DVD_USD_val,NET_EX_DVD_USD);
            NET_PAYABLE_DVD_USD_val = vertcat(NET_PAYABLE_DVD_USD_val,NET_PAYABLE_DVD_USD);
        end
    end 

    Missing_Currency = convertCell2Char(Missing_Currency);
    if ~isempty(Missing_Currency)
        OutErrorMsg = {[Missing_Currency,' currency is not found in ',char(Currency_Spot_Table_Name)]} ;
        return;
    end

    %%% Calculate the NET_EX_DVD_USD and NET_PAYABLE_DVD_USD
    Master_DVD_Cummulative_Data.net_ex_dvd_usd = NET_EX_DVD_USD_val;
    Master_DVD_Cummulative_Data.net_payable_dvd_usd = NET_PAYABLE_DVD_USD_val;
    
    %%% find the status and calculate the NET_DVD_USD
    status_all = []; NET_DVD_USD_val = [];
    for z = 1:1:row
    %%% Newly added code
    if iscell(Master_DVD_Cummulative_Data.payable_date(z))
        payable_date = cell2mat(Master_DVD_Cummulative_Data.payable_date(z));
        if isnan(payable_date)
            payable_date = '';
            Exact_payable_date{z,1} = '';
        end
        Exact_payable_date{z,1} = payable_date;
   else
        payable_date = '';
        Exact_payable_date{z,1} = '';
    end
    %%% end code 
        if strcmp(payable_date,'null') || isempty(payable_date)
            status = 'live';
            NET_DVD_USD = Master_DVD_Cummulative_Data.net_ex_dvd_usd(z);
        elseif (payable_date <= char(Settele_date))
            status = 'dead';
            NET_DVD_USD = Master_DVD_Cummulative_Data.net_payable_dvd_usd(z);
        else   
            status = 'live';
            NET_DVD_USD = Master_DVD_Cummulative_Data.net_ex_dvd_usd(z);   
        end
        status_all = vertcat(status_all,status);
        NET_DVD_USD_val = vertcat(NET_DVD_USD_val,NET_DVD_USD);
    end

    Master_DVD_Cummulative_Data.status = status_all;
    Master_DVD_Cummulative_Data.net_dvd_usd = NET_DVD_USD_val;

    %%% write the variable data into excel sheet
    Master_DVD_Cummulative_Data = dataset2cell(Master_DVD_Cummulative_Data);
    xlswrite(OutXLSName,Master_DVD_Cummulative_Data,'DVD_CUMMULATIVE_OUTPUT');
    xls_delete_sheets(fullfile(pwd,OutXLSName));

catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end
