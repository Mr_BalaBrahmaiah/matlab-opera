function [OutErrorMsg,UnRealized_OutData,Original_Unrealized_Data,Overall_Consolidate_Data] = QF5_UnRealized_FTD_PnL_Report(InBUName,Temp_Settle_Date)

% [OutErrorMsg ,OutXLSName] = QF5_UnRealized_FTD_PnL_Report(InBUName)
% InBUName = 'qf5';

try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_UnRealized_FTD_PnL_Report');
    
    UnRealized_OutData = '';
    Original_Unrealized_Data = '';
    Overall_Consolidate_Data ='';
    
    
    %% Fetching Data
    ObjDB = connect_to_database;
    
    if(exist('Temp_Settle_Date','var'))
        Settle_Date = cellstr(Temp_Settle_Date);
    else
        Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end
    
    Previous_SettleDate = fetch(ObjDB,['select settlement_date from vdate_sdate_table where value_date = ''',char(Settle_Date),''' ' ]);
    
    TableName = strcat('unrealized_report_store_table_',char(InBUName));
    SqlQuery = ['select * from ',TableName,' where settle_date in ( ''',char(Previous_SettleDate),''',''',char(Settle_Date),''') '];
    [ColNames,RowData] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(RowData))
        OutErrorMsg = {['Not Getting Data from ',TableName]};
        return;
    end
    if(strcmpi(RowData,'No Data'))
        OutErrorMsg = {['Not Getting Data from ',TableName]};
        return;
    end
    
    Original_Unrealized_Data = [ColNames;RowData] ; %% For Temporary purpose
    
    UniqueFields = {'settle_date','counterparty_parent','stock_number','instrument','stock_name'};
    SumFields={'open_qty','unrealized_pnl'};
    OutputFields = [UniqueFields ,{'market_price'},SumFields] ; %ColNames;
    WeightedAverageFields = [];
    [ConsHeader,ConsDeals] = consolidatedata(ColNames, RowData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Overall_Consolidate_Data = cell2dataset([ConsHeader;ConsDeals]);
    
    Today_Data = Overall_Consolidate_Data(strcmpi(Overall_Consolidate_Data.settle_date,Settle_Date),:);
    if(isempty(Today_Data))
        OutErrorMsg = {['Not Getting Data from ',TableName,' for ',char(Settle_Date)]};
        return;
    end
    
    Previousday_Data = Overall_Consolidate_Data(strcmpi(Overall_Consolidate_Data.settle_date,Previous_SettleDate),:);
    if(isempty(Today_Data))
        OutErrorMsg = {['Not Getting Data from ',TableName,' for ',char(Previous_SettleDate)]};
        return;
    end
    
    %%
    
    for i = 1 : size(Today_Data,1)
        
        Current_Counterparty = Today_Data.counterparty_parent(i);
        Current_StockNumber = Today_Data.stock_number(i);
        Current_StockName = Today_Data.stock_name(i);
        Current_Instrument = Today_Data.instrument(i);
        
        Matched_Index = strcmpi(Previousday_Data.counterparty_parent,Current_Counterparty) & strcmpi(Previousday_Data.stock_number,Current_StockNumber)...
            & strcmpi(Previousday_Data.stock_name,Current_StockName) & strcmpi(Previousday_Data.instrument,Current_Instrument);
        
        if(~isempty(find(Matched_Index)))
            Index = find(Matched_Index,1);
            PreviousDay_UnReal_PnL = Previousday_Data.unrealized_pnl(Index);
            Today_Data.PnL_Change(i) = Today_Data.unrealized_pnl(i) - PreviousDay_UnReal_PnL;
        else
            Today_Data.PnL_Change(i) = Today_Data.unrealized_pnl(i);
        end
        
    end
    
    %% Missing Stock Number for today but its there in Yesterday We need show that
    
    aa = strcat(Previousday_Data.counterparty_parent ,Previousday_Data.instrument); %% Previousday_Data.instrument
    bb = strcat(Today_Data.counterparty_parent ,Today_Data.instrument); %% Today_Data.instrument
    Previousday_Missing_Stock = setdiff(aa,bb);
    Previousday_Missing_Index = cellStrfind_exact(aa,Previousday_Missing_Stock) ;
    
    Previousday_MissingData = Previousday_Data(Previousday_Missing_Index,:);
    Previousday_MissingData.PnL_Change =  0 - Previousday_MissingData.unrealized_pnl;
    Previousday_MissingData.settle_date(:,:) = Settle_Date;
    Previousday_MissingData.open_qty(:,:) = 0;
    
    Today_Data = [Today_Data ; Previousday_MissingData];
    
    %% Excel Sheet Write
    
    UnWanted_Col = {'transaction_date','trade_price'}; %% Because it not unique fields when consolidated
    UnWanted_Col_Index = cellStrfind_exact(ConsHeader,UnWanted_Col);
    
    ConsHeader(UnWanted_Col_Index) = [];
    Today_Data(:,UnWanted_Col_Index) = [];
    
    ConsHeader = [ConsHeader , {'PnL Change'}];
    
    %     xlswrite(OutXLSName,dataset2cell(Today_Data));
    
    %% Making Ouput
    
    UnRealized_OutData = dataset2cell(Today_Data);
    Temp_Cell = cell(size(UnRealized_OutData,1),1);
    Temp_Cell(1,1) = {'Realized_Unrealized_Header'};
    Temp_Cell(2:end,1) = {'UnRealized_PnL_Change'};
    
    UnRealized_OutData = [UnRealized_OutData,Temp_Cell];
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    UnRealized_OutData = '';
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end