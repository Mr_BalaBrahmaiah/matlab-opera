function [OutErrorMsg , OutXLSName] = QF5_Realised_Report_FTD(InBUName)

OutErrorMsg = {'No Errors'};

OutXLSName = getXLSFilename('QF5_Realised_Report_FTD');

%% Fetching Data
ObjDB = connect_to_database;

Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');

Default_Name = 'helper_evaluation_reports_view_';
ViewName = [Default_Name,InBUName] ;

SqlQuery = ['select * from ',char(ViewName),' where transaction_date = ''',char(Settle_Date),'''  and deal_status = ''dead'' '];
[ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName

%% Initialize Output Col

TradeDate_Col = 1; %% transaction_date
Counterparty_Col = 2 ; %% counterparty_parent
StockNo_Col = 3; %% product_code
StockName_Col = 4; %% product_name
PurchaseQuantity_Col = 5; %% active_lots
PurchasePrice_Col = 6; %%original_premium
SalesQuantity_Col = 7; %% active_lots
SalesPrice_Col = 8; %% original_premium

RealisedPnL_USD_Col = 9;
BGN_SpotRate_Col = 10;

RealisedPnL_NC_Col = 11;
BrokerageCost_Col =12;
NetPNL_NC_Col = 13;
BrokerageCostUSD_Col = 14;
NetPNL_USD_Col = 115;

%%

try
    if(size(DBData_All,2) > 1)
        
        %% Consolidate Data
        %         UniqueFields = {'transaction_date','product_code','product_name','market_action','original_premium'};
        %         SumFields = {'active_lots'};
        %         OutputFields = [UniqueFields,SumFields];
        %         WeightedAverageFields = [];
        %         [Consolidate_Fields,Consolidate_Data] = consolidatedata(ColNames,DBData_All,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        Consolidate_Fields = ColNames ;
        Consolidate_Data = DBData_All ;
        
        Consolidate_TransDate_Col = cellStrfind_exact(Consolidate_Fields,{'transaction_date'});
        Consolidate_Counterparty_Col = cellStrfind_exact(Consolidate_Fields,{'counterparty_parent'});
        Consolidate_ProdCode_Col = cellStrfind_exact(Consolidate_Fields,{'product_code'});
        Consolidate_ProdName_Col = cellStrfind_exact(Consolidate_Fields,{'product_name'});
        Consolidate_MarketAction_Col = cellStrfind_exact(Consolidate_Fields,{'market_action'});
        Consolidate_ActiveLots_Col = cellStrfind_exact(Consolidate_Fields,{'active_lots'});
        Consolidate_OriginalPremium_Col = cellStrfind_exact(Consolidate_Fields,{'original_premium'});
        Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(Consolidate_Fields,{'realised_mtm_usd'});
        %         Consolidate_BGNSpot_Col = cellStrfind_exact(Consolidate_Fields,{'bgn_spot_rate'});
        
        % Unique_ProductName = unique(Consolidate_Data(:,Consolidate_ProdName_Col));
        
        OutputData = cell(size(Consolidate_Data,1),9); %% cell(size(Consolidate_Data,1),14);
        
        for i = 1 : size(Consolidate_Data,1)
            %Current_ProductName = Unique_ProductName(i);
            % Matched_ProductData = Consolidate_Data(cellStrfind_exact(Consolidate_Data(:,Consolidate_ProductName_Col),Current_ProductName),:);
            
            OutputData(i,TradeDate_Col) = Consolidate_Data(i , Consolidate_TransDate_Col);
            OutputData(i,Counterparty_Col) = Consolidate_Data(i ,Consolidate_Counterparty_Col );
            OutputData(i,StockNo_Col) = Consolidate_Data(i , Consolidate_ProdCode_Col);
            OutputData(i,StockName_Col) = Consolidate_Data(i ,Consolidate_ProdName_Col );
            
            if(strcmpi(Consolidate_Data(i , Consolidate_MarketAction_Col),'bought'))
                OutputData(i,PurchaseQuantity_Col) = Consolidate_Data(i ,Consolidate_ActiveLots_Col);
                OutputData(i,PurchasePrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
            else
                OutputData(i,SalesQuantity_Col) = Consolidate_Data(i , Consolidate_ActiveLots_Col);
                OutputData(i,SalesPrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
            end
            
            OutputData(i,RealisedPnL_USD_Col) = Consolidate_Data(i , Consolidate_RealizedM2M_USD_Col) ;
            
            %             OutputData(i,BGN_SpotRate_Col) = Consolidate_Data(i , Consolidate_BGNSpot_Col);
            
            %             OutputData(i,RealisedPnL_NC_Col) = {NaN};
            %             OutputData(i,BrokerageCost_Col) = {NaN} ;
            %             OutputData(i,NetPNL_NC_Col) = {NaN} ;
            %             OutputData(i,BrokerageCostUSD_Col) = {NaN} ;
            %             OutputData(i,NetPNL_USD_Col) = {NaN} ;
            
            
        end
        
        %% Excel Sheet Write &  Delete Sheets
        
        Output_Header = {'Trade Date','Counterparty','Stock No','Stock Name','Purchase Quantity','Purchase Price','Sales Quantity','Sales Price','Realised PnL (USD)'}; %% 'BGN Spot Rate'
        %         {'Realised Pnl(NC)','Brokerage cost (NC)','Net PNL (NC)','Brokerage cost (USD)','Net PNL (USD)'};
        
        xlswrite(OutXLSName,[Output_Header ;OutputData ] );
        try
            % OutXLSName = fullfile(pwd,OutXLSName);
            xls_delete_sheets(fullfile(pwd,OutXLSName));
        catch
        end
    else
        OutErrorMsg = {['No Data ',ViewName]};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end