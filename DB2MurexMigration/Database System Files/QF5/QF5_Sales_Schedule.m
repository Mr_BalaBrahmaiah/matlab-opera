function [OutErrorMsg,OutXLSName] = QF5_Sales_Schedule(InBUName)

% QF5_Sales_Schedule

OutErrorMsg = {'No Errors'};

%%
ObjDB = connect_to_database;

OutXLSName = getXLSFilename('QF5_Sales_Schedule');

Default_Name = 'helper_evaluation_reports_view_' ; %'helper_funds_reports_view_';
ViewName = [Default_Name,InBUName] ;
[ColNames,DBData] = Fetch_DB_Data(ObjDB,[],ViewName);

try
    if(size(DBData,2)>1)
        
        User_Want_Header = {'transaction_date','counterparty_parent','product_code','product_name','active_lots','original_premium','market_action'};
        
        ActiveLots_Col = cellStrfind_exact(User_Want_Header,{'active_lots'});
        OriginalPremium_Col = cellStrfind_exact(User_Want_Header,{'original_premium'});
        MarketAction_Col =  cellStrfind_exact(User_Want_Header,{'market_action'});
        
        Matched_Cols = cellStrfind_exact(ColNames,User_Want_Header);
        UserWant_Data = DBData(:,Matched_Cols);
        
        Sale_Index = cellStrfind_exact(UserWant_Data(:,MarketAction_Col),{'Sold'});
        UserWant_Data = UserWant_Data(Sale_Index,:);
        
        UserWant_Data(:,MarketAction_Col) = []; %% For User Purpose
        
        Sales_Value = num2cell(cell2mat(UserWant_Data(:,ActiveLots_Col)) .* cell2mat(UserWant_Data(:,OriginalPremium_Col))) ;
        
        Output_Header = {'Trade Date','Counterparty','Stock No','Stock Name','Sales Quantity','Sales Price','Sale Proceeds'};
        UserWant_Data = [UserWant_Data , Sales_Value];
        
        %% Sum Col
        Temp_Row = cell(size(Output_Header,1),size(Output_Header,2));
        Temp_Row(1,1) = {'TOTAL'};
        Sum_Col = cellStrfind_exact(Output_Header,{'Sales Quantity','Sale Proceeds'});
        [RowSum,ColSum] = cell2sum_Row_Col(UserWant_Data(:,Sum_Col));
        Temp_Row(:,Sum_Col) = ColSum;
        UserWant_Data = [UserWant_Data;Temp_Row];
        
        %% Excel Write
        xlswrite(OutXLSName,[Output_Header ; UserWant_Data],'QF5_Sales_Schedule');
        
        %%
        try
            % OutXLSName = fullfile(pwd,OutXLSName);
            xls_delete_sheets(fullfile(pwd,OutXLSName));
        catch
        end
    else
        OutErrorMsg = {['No Data ',ViewName]};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end