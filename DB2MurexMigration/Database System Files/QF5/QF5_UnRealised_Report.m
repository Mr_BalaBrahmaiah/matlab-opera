function [OutErrorMsg ,OutXLSName ] = QF5_UnRealised_Report(InBUName)

try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_UnRealised_Report');
    
    %% Fetching Data
    ObjDB = connect_to_database;
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    Default_Name = 'helper_evaluation_reports_view_';
    ViewName = [Default_Name,InBUName] ;
    SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''live'' '];
    [ColNames,Overall_DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(Overall_DBData_All))
        OutErrorMsg = {['Not Getting Data from ',char(ViewName)]};
        return;
    end
    if(strcmpi(Overall_DBData_All,'No Data'))
        OutErrorMsg = {['Not Getting Data from ',char(ViewName)]};
        return;
    end
    
    Monthly_Reset_Table = ['monthly_reset_date_table_',char(InBUName)];
    Unique_CounterParty = fetch(ObjDB,['select distinct(counterparty_parent) from ',Monthly_Reset_Table]);
    
    %% Initialize Output Col
    
    TradeDate_Col = 1; %% transaction_date
    Counterparty_Col = 2 ; %% counterparty_parent
    StockNo_Col = 3; %% product_code
    Instrument_Col = 4 ; %% instrument
    StockName_Col = 5; %% product_name
    OpenQuantity_Col = 6; %% active_lots
    TradePrice_Col = 7; %% current_premium
    
    MarketPrice_Col = 8; %% settle_price
    UnRealisedPnL_USD_Col = 9; %% unrealised_mtm_usd
    
       % TradeType_Col = 11 ; %% equity_swaps_trade_type
    
    BGN_SpotRate_Col = 12; %% bgn_spot_rate
    
    %%
    Overall_ErrorMsg = [];
    Overall_CounterpartyData = [];
    CounterParty_Col_Index = cellStrfind_exact(ColNames,{'counterparty_parent'});
    
    for k = 1 : length(Unique_CounterParty)
        
        Current_CounterParty = Unique_CounterParty(k);
        
        %%
        %         SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''live'' and counterparty_parent = ''',char(Current_CounterParty),''' '];
        %         [ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
        
        Current_Counterparty_Index = strcmpi(Overall_DBData_All(:,CounterParty_Col_Index),Current_CounterParty);
        DBData_All = Overall_DBData_All(Current_Counterparty_Index,:);
        
        
        %%
        if(size(DBData_All,2) > 1)
            
            %% Consolidate Data
            %         UniqueFields = {'transaction_date','product_code',instrument,'product_name','market_action','original_premium'};
            %         SumFields = {'active_lots'};
            %         OutputFields = [UniqueFields,SumFields];
            %         WeightedAverageFields = [];
            %         [Consolidate_Fields,Consolidate_Data] = consolidatedata(ColNames,DBData_All,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            Consolidate_Fields = ColNames ;
            Consolidate_Data = DBData_All ;
            
            Consolidate_TransDate_Col = cellStrfind_exact(Consolidate_Fields,{'transaction_date'});
            Consolidate_Counterparty_Col = cellStrfind_exact(Consolidate_Fields,{'counterparty_parent'});
            Consolidate_ProdCode_Col = cellStrfind_exact(Consolidate_Fields,{'product_code'});
            Consolidate_Instrument_Col = cellStrfind_exact(Consolidate_Fields,{'instrument'});
            Consolidate_ProdName_Col = cellStrfind_exact(Consolidate_Fields,{'product_name'});
            Consolidate_ActiveLots_Col = cellStrfind_exact(Consolidate_Fields,{'active_lots'});
            Consolidate_CurrentPremium_Col = cellStrfind_exact(Consolidate_Fields,{'current_premium'});
            Consolidate_SettlePrice_Col = cellStrfind_exact(Consolidate_Fields,{'settle_price'});
            Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(Consolidate_Fields,{'unrealised_mtm_usd'});
            
            %         Consolidate_Counterparty_Col = cellStrfind_exact(Consolidate_Fields,{'counterparty_parent'});
            %         Consolidate_TradeType_Col = cellStrfind_exact(Consolidate_Fields,{'equity_swaps_trade_type'});
            %         Consolidate_BGNSpot_Col = cellStrfind_exact(Consolidate_Fields,{'bgn_spot_rate'});
            
            OutputData = cell(size(Consolidate_Data,1),9);
            
            for i = 1 : size(Consolidate_Data,1)
                
                OutputData(i,TradeDate_Col) = Consolidate_Data(i , Consolidate_TransDate_Col);
                OutputData(i,Counterparty_Col) = Consolidate_Data(i ,Consolidate_Counterparty_Col );
                OutputData(i,StockNo_Col) = Consolidate_Data(i , Consolidate_ProdCode_Col);
                OutputData(i,Instrument_Col) = Consolidate_Data(i ,Consolidate_Instrument_Col );
                OutputData(i,StockName_Col) = Consolidate_Data(i ,Consolidate_ProdName_Col );
                
                OutputData(i,OpenQuantity_Col) = Consolidate_Data(i ,Consolidate_ActiveLots_Col);
                OutputData(i,TradePrice_Col) = Consolidate_Data(i , Consolidate_CurrentPremium_Col);
                OutputData(i,MarketPrice_Col) = Consolidate_Data(i , Consolidate_SettlePrice_Col);
                
                OutputData(i,UnRealisedPnL_USD_Col) = Consolidate_Data(i , Consolidate_RealizedM2M_USD_Col) ;
                
                %             OutputData(i,CounterParty_Col) = Consolidate_Data(i , Consolidate_Counterparty_Col) ;
                %             OutputData(i,TradeType_Col) = Consolidate_Data(i , Consolidate_TradeType_Col) ;
                %             OutputData(i,BGN_SpotRate_Col) = Consolidate_Data(i , Consolidate_BGNSpot_Col);
                
                
            end
            
            %% Excel Sheet Write &  Delete Sheets
            
            Output_Header = {'Trade Date','Counterparty','Stock No','Instrument','Stock Name','Open Quantity','Trade Price','Market Price','UnRealised PnL (USD)'}; %% 'Counterparty','Trade Type','BGN Spot Rate'
            
            xlswrite(OutXLSName,[Output_Header ;OutputData ],char(Current_CounterParty));
            
            Overall_CounterpartyData = [Overall_CounterpartyData ; OutputData];
            
            %             xls_delete_sheets(fullfile(pwd,OutXLSName));
            
            %% Uploading into  'unrealized_report_store_table_qf5'
            
            try
                Temp_Date = cell(size(OutputData,1),1);
                Temp_Date(:,:) = Settle_Date;
                
                Uploading_Data = [Temp_Date,OutputData];
                
                TableName = strcat('unrealized_report_store_table_',char(InBUName));
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',TableName,' where settle_date = ''',char(Settle_Date),''' and counterparty_parent=''',char(Current_CounterParty),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');
                upload_in_database(TableName, Uploading_Data); %% Required_Data %% Overall_Data
                cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date :  ',char(Settle_Date),' and counterparty ',char(Current_CounterParty)]);
            catch
                Overall_ErrorMsg = {[Overall_ErrorMsg ; ['Not uploading Data into ',TableName,' for ',char(Current_CounterParty)]]};
            end
            
        else
            %   OutErrorMsg = {['No Data ',ViewName]};
            Overall_ErrorMsg = {[Overall_ErrorMsg ; ['No Data ',ViewName,' for ',char(Current_CounterParty)]]};
        end
    end
    %% Excel Sheet Delete
    
    try
        
        Counterparty_Col_Output = cellStrfind(Output_Header,{'Counterparty'});
        %%% Old Code
%         CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC');
%         MS_CounterpartyData = Overall_CounterpartyData(~CICC_Index,:);
%         xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        
%%%% New Logic
        %%% newly added line (01-05-2019)
        CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC') | contains(upper(Overall_CounterpartyData(:,Counterparty_Col_Output)),'CICC-SUB');
        MS_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MORGAN STANLEY') | strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MS',2);
        GS_Index = strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'GS',2);

        CICC_CounterpartyData = Overall_CounterpartyData(CICC_Index,:);  %%% CICC data
        MS_CounterpartyData = Overall_CounterpartyData(MS_Index,:);    %%% MS data
        GS_CounterpartyData = Overall_CounterpartyData(GS_Index,:);  %%% GS data
        
        xlswrite(OutXLSName,[Output_Header ; CICC_CounterpartyData ],'CICC-ALL' );
        xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        xlswrite(OutXLSName,[Output_Header ; GS_CounterpartyData ],'GS-ALL' );
%%% End logic
        if(~isempty(Overall_ErrorMsg))
            xlswrite(OutXLSName,Overall_ErrorMsg,' Error Sheet');
        end
        
        % OutXLSName = fullfile(pwd,OutXLSName);
        xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end