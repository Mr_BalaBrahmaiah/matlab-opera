function [OutErrorMsg,OutXLSName] = QF5_FTD_Trades(InBUName)

% QF5 FTD_Trades

OutErrorMsg = {'No Errors'};

%%
ObjDB = connect_to_database;

OutXLSName = getXLSFilename('QF5_FTD_Trades');

Default_Name = 'helper_evaluation_reports_view_' ; %'helper_funds_reports_view_';
ViewName = [Default_Name,InBUName] ;
Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
SqlQuery = ['select * from ',char(ViewName),' where transaction_date = ''',char(Settle_Date),''' '] ;
[ColNames,DBData_All] = Fetch_DB_Data(ObjDB,SqlQuery);

%%
Monthly_Reset_Table = ['monthly_reset_date_table_',char(InBUName)];
Unique_CounterParty = fetch(ObjDB,['select distinct(counterparty_parent) from ',Monthly_Reset_Table]);

%% Output Header Initialization

Output_Header = {'Trade Date','Counterparty','Transaction Number','Stock No','Stock/Instrument Name','Bought/Sold','Traded Volume','Traded Price'};

%%
try
    Overall_ErrorMsg = [];
    Overall_Data = [];
    Counterparty_Col = cellStrfind_exact(ColNames,{'counterparty_parent'});
    
    if(size(DBData_All,2)>1)
        for k = 1 : length(Unique_CounterParty)
            
            Current_CounterParty = Unique_CounterParty(k);
            Current_CounterParty_Index = strcmpi(DBData_All(:,Counterparty_Col),char(Current_CounterParty));
            DBData = DBData_All(Current_CounterParty_Index,:) ;
            
            if(isempty(DBData))
                Overall_ErrorMsg = [Overall_ErrorMsg,{['Not Getting FTD Data from ',ViewName,' for ',char(Current_CounterParty)]}] ;
                continue;
            end
            
            
            UserWant_Header = {'transaction_date','counterparty_parent','trade_id','product_code','product_name','market_action','active_lots','original_premium'};
            Matched_Cols = cellStrfind_exact(ColNames,UserWant_Header);
            UserWant_Data = DBData(:,Matched_Cols);
            
            
            Overall_Data = [Overall_Data ; UserWant_Data];
            %             xlswrite(OutXLSName,[Output_Header ; UserWant_Data],char(Current_CounterParty)); % Seperate Counterparty Wise
            
        end
        
        %% Excel SHeet Write
        
        try
            xlswrite(OutXLSName,[Output_Header ; Overall_Data],'QF5_FTD_Trades'); % ALl counterparty
            
            % OutXLSName = fullfile(pwd,OutXLSName);
            if(~isempty(Overall_ErrorMsg))
                xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Sheet');
            end
            
            xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
        catch
        end
        
        
    else
        OutErrorMsg = {['No Data ',ViewName]};
        
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end