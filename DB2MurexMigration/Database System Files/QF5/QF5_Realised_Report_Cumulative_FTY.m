function [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTY(InBUName)

try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_Realised_Report_Cumulative_FTY');
    
    %% Fetching Data
    ObjDB = connect_to_database;
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    Current_Month = 1;
    Current_Year = str2double(datestr(char(Settle_Date),10));
    FTY_StartDate = datestr(lbusdate(Current_Year-1,12),'yyyy-mm-dd');

    Default_Name = 'helper_evaluation_reports_view_';
    ViewName = [Default_Name,InBUName] ;
    
    %%% newly added(2020-02-03) logic for GS-KOREA, FTY_StartDate is Dec 30th because Dec 31st is Holiday in KOREA
    SqlQuery1 = ['select * from ',char(ViewName),' where transaction_date = ''','2019-12-30',''' and deal_status = ''dead'' and counterparty_parent = ''','GS-KOREA',''' '];
    [~,GSK_DBData] = Fetch_DB_Data(ObjDB,SqlQuery1); %% ViewName
    %%% end
    
    SqlQuery = ['select * from ',char(ViewName),' where transaction_date >= ''',FTY_StartDate,''' and deal_status = ''dead'' '];
    [ColNames,DBData_All1] = Fetch_DB_Data(ObjDB,SqlQuery); %% ViewName
    
    DBData_All = [DBData_All1;GSK_DBData]; %% Data from Dec 30th
    %%
    Monthly_Reset_Table = ['monthly_reset_date_table_',char(InBUName)];
    Unique_CounterParty = fetch(ObjDB,['select distinct(counterparty_parent) from ',Monthly_Reset_Table]);
    
    %%
    %% Initialize Output Col
    
    TradeID_Col = 1 ; %% TradeID
    TransactionDate_Col = 2 ; %% Transaction Date
    
    Counterparty_Col = 3 ; %% counterparty_parent
    StockNo_Col = 4; %% product_code
    Instrument_Col = 5 ; %% instrument
    StockName_Col = 6; %% product_name
    
    PurchaseQuantity_Col = 7; %% active_lots
    PurchasePrice_Col = 8; %%original_premium
    SalesQuantity_Col = 9; %% active_lots
    SalesPrice_Col = 10; %% original_premium
    
    RealisedPnL_USD_Col = 11;
    BGN_SpotRate_Col = 12;
    
    Output_Header = {'TradeID','Transaction Date','Counterparty','Stock No','Instrument','Stock Name','Purchase Quantity','Purchase Price','Sales Quantity','Sales Price','Realised PnL (USD)'};    %% ,'BGN Spot Rate'
    
    %%
    Overall_ErrorMsg = [];
    Overall_CounterpartyData = [];
    Counterparty_Col_Index = cellStrfind_exact(ColNames,{'counterparty_parent'});
    
    if(size(DBData_All,2) > 1)
        for k = 1 : length(Unique_CounterParty)
            
            Current_CounterParty = Unique_CounterParty(k);
            Current_CounterParty_Index = strcmpi(DBData_All(:,Counterparty_Col_Index),char(Current_CounterParty));
            DBData = DBData_All(Current_CounterParty_Index,:) ;
            
            if(isempty(DBData))
                Overall_ErrorMsg = [Overall_ErrorMsg,{['Not Getting FTY Data from ',ViewName,' for ',char(Current_CounterParty)]}] ;
                continue;
            end
            
            %% Consolidate Data
            %         UniqueFields = {'transaction_date','product_code','instrument','product_name','market_action','original_premium'};
            %         SumFields = {'active_lots'};
            %         OutputFields = [UniqueFields,SumFields];
            %         WeightedAverageFields = [];
            %         [Consolidate_Fields,Consolidate_Data] = consolidatedata(ColNames,DBData_All,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            Consolidate_Fields = ColNames ;
            Consolidate_Data = DBData ;
            
            Consolidate_TradeID = cellStrfind_exact(Consolidate_Fields,{'trade_id'});
            Consolidate_TransactionDate  = cellStrfind_exact(Consolidate_Fields,{'transaction_date'});
            Consolidate_Counterparty_Col = cellStrfind_exact(Consolidate_Fields,{'counterparty_parent'});
            
            Consolidate_ProdCode_Col = cellStrfind_exact(Consolidate_Fields,{'product_code'});
            Consolidate_Instrument_Col = cellStrfind_exact(Consolidate_Fields,{'instrument'});
            Consolidate_ProdName_Col = cellStrfind_exact(Consolidate_Fields,{'product_name'});
            
            Consolidate_MarketAction_Col = cellStrfind_exact(Consolidate_Fields,{'market_action'});
            Consolidate_ActiveLots_Col = cellStrfind_exact(Consolidate_Fields,{'active_lots'});
            Consolidate_OriginalPremium_Col = cellStrfind_exact(Consolidate_Fields,{'original_premium'});
            Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(Consolidate_Fields,{'realised_mtm_usd'});
            %         Consolidate_BGNSpot_Col = cellStrfind_exact(Consolidate_Fields,{'bgn_spot_rate'});
            
            OutputData = cell(size(Consolidate_Data,1),10); %% cell(size(Consolidate_Data,1),14);
            
            for i = 1 : size(Consolidate_Data,1)
                
                OutputData(i,TradeID_Col) = Consolidate_Data(i , Consolidate_TradeID);
                OutputData(i,TransactionDate_Col) = Consolidate_Data(i , Consolidate_TransactionDate);
                OutputData(i,Counterparty_Col) = Consolidate_Data(i ,Consolidate_Counterparty_Col );
                
                OutputData(i,StockNo_Col) = Consolidate_Data(i , Consolidate_ProdCode_Col);
                OutputData(i,Instrument_Col) = Consolidate_Data(i ,Consolidate_Instrument_Col );
                OutputData(i,StockName_Col) = Consolidate_Data(i ,Consolidate_ProdName_Col );
                
                if(strcmpi(Consolidate_Data(i , Consolidate_MarketAction_Col),'bought'))
                    OutputData(i,PurchaseQuantity_Col) = Consolidate_Data(i ,Consolidate_ActiveLots_Col);
                    OutputData(i,PurchasePrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
                else
                    OutputData(i,SalesQuantity_Col) = Consolidate_Data(i , Consolidate_ActiveLots_Col);
                    OutputData(i,SalesPrice_Col) = Consolidate_Data(i , Consolidate_OriginalPremium_Col);
                end
                
                OutputData(i,RealisedPnL_USD_Col) = Consolidate_Data(i , Consolidate_RealizedM2M_USD_Col) ;
                %             OutputData(i,BGN_SpotRate_Col) = Consolidate_Data(i , Consolidate_BGNSpot_Col);
                
                
            end
            
            %% Excel Sheet Write &  Delete Sheets
            
            xlswrite(OutXLSName,[Output_Header ;OutputData ],char(Current_CounterParty));
            
            Overall_CounterpartyData = [Overall_CounterpartyData ; OutputData];
            
            %% Uploading into  'realized_fty_report_store_table_'
            
            try
                Temp_Date = cell(size(OutputData,1),1);
                Temp_Date(:,:) = Settle_Date;
                
                Uploading_Data = [Temp_Date,OutputData];
                
                TableName = strcat('realized_fty_report_store_table_',char(InBUName));
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',TableName,' where settle_date = ''',char(Settle_Date),''' and counterparty_parent=''',char(Current_CounterParty),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');
                upload_in_database(TableName, Uploading_Data); %% Required_Data %% Overall_Data
                cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date :  ',char(Settle_Date),' and counterparty ',char(Current_CounterParty)]);
            catch
                Overall_ErrorMsg = {[Overall_ErrorMsg ; ['Not uploading Data into ',TableName,' for ',char(Current_CounterParty)]]};
            end
            
        end
        
        %%
        try
            
            Counterparty_Col_Output = cellStrfind(Output_Header,{'Counterparty'});
         %%% Old Code
%         CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC');
%         MS_CounterpartyData = Overall_CounterpartyData(~CICC_Index,:);
%         xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        
%%%% New Logic
        %%% newly added line (01-05-2019)
        CICC_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'CICC') | contains(upper(Overall_CounterpartyData(:,Counterparty_Col_Output)),'CICC-SUB');
        MS_Index = strcmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MORGAN STANLEY') | strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'MS',2);
        GS_Index = strncmpi(Overall_CounterpartyData(:,Counterparty_Col_Output),'GS',2);

        CICC_CounterpartyData = Overall_CounterpartyData(CICC_Index,:);  %%% CICC data
        MS_CounterpartyData = Overall_CounterpartyData(MS_Index,:);    %%% MS data
        GS_CounterpartyData = Overall_CounterpartyData(GS_Index,:);  %%% GS data
        
        xlswrite(OutXLSName,[Output_Header ; CICC_CounterpartyData ],'CICC-ALL' );
        xlswrite(OutXLSName,[Output_Header ; MS_CounterpartyData ],'MS-ALL' );
        xlswrite(OutXLSName,[Output_Header ; GS_CounterpartyData ],'GS-ALL' );
%%% End logic
            
            % OutXLSName = fullfile(pwd,OutXLSName);
            if(~isempty(Overall_ErrorMsg))
                xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Sheet');
            end
            xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
        catch
        end
        
        
    else
        OutErrorMsg = {['No Data ',ViewName]};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end