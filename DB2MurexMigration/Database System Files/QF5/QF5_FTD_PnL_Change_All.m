function QF5_FTD_PnL_Change_All()

InBUName = 'qf5';
try
    
    ObjDB = connect_to_database ;
    
    SettleDate_All = fetch(ObjDB,'select settlement_date from vdate_sdate_table where settlement_date >=''2018-07-10'' and settlement_date <=''2018-07-31'' ');
    
    for i = 1 : length(SettleDate_All)
        
       Current_SettleDate =  SettleDate_All(i);
       
       %% FTD PNL Channge
       
       [OutErrorMsg ,OutXLSName] = QF5_FTD_PnL_Change(InBUName,Current_SettleDate);
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

