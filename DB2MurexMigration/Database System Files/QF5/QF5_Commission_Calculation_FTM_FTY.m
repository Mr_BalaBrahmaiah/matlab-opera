function [OutErrorMsg ,OutXLSName ] = QF5_Commission_Calculation_FTM_FTY(InBUName,From_Date,End_Date)

try
    OutErrorMsg = {'No Errors'};
%   OutXLSName = getXLSFilename(['QF5_FTM_FTY_Commission_',From_Date,'_to_',End_Date]);
    OutXLSName = getXLSFilename(['QF5_Commission_FTM_FTY_',From_Date,'_to_',End_Date]);

    ObjDB = connect_to_database;
    
    %% Date Check
    FromDate_Num = datenum(From_Date,'yyyy-mm-dd');
    EndDate_Num = datenum(End_Date,'yyyy-mm-dd');
    
    [~,Value_Date] = Fetch_DB_Data(ObjDB,'select value_date from valuation_date_table');
    Value_Date_Num = datenum(Value_Date,'yyyy-mm-dd');
    
    if(FromDate_Num > EndDate_Num)
        OutErrorMsg = {'From Date should be less than or equal to End Date'} ;
        return;
    else
        if(FromDate_Num == EndDate_Num)
            if(FromDate_Num > Value_Date_Num)
                OutErrorMsg = {'Both Date should be less than or equal to Opera Value Date'} ;
                return;
            end
        end
    end
    
    %% Fetch Data
        
    TableName = strcat('commission_store_table_',char(InBUName));
    SqlQuery = ['select * from ',TableName,' where trade_date >=''',From_Date,''' and trade_date <=''',End_Date,''' '] ;
    [ColNames , DBData] = Fetch_DB_Data(ObjDB,SqlQuery) ;
    
    
    %% Consolidate Data
    if(size(DBData,2) > 1)
        
        UniqueFields = {'stock_id','stock_number','counterparty_parent','trade_type'};
        SumFields = {'commission','exch_fees','salex_tax','amount'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [ReportHeader,ReportData] = consolidatedata(ColNames, DBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
    else
        OutErrorMsg = {['No Trades for corresponding date range in ',TableName]};
        
    end
    %% Excel Write
    
    xlswrite(OutXLSName,[ColNames ; DBData],'Original');
    xlswrite(OutXLSName,[ReportHeader ; ReportData],'Consolidate Data');
    
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end
