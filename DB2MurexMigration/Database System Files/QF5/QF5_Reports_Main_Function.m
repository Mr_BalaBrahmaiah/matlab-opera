clc;
clear;

InBUName = 'qf5';

try
    %%
    if(0) %% Before_Reset
        [OutErrorMsg,OutXLSName] = QF5_FTD_Trades(InBUName);
        
        [OutErrorMsg,OutXLSName] = QF5_Purchase_Schedule(InBUName);
        
        [OutErrorMsg,OutXLSName] = QF5_Sales_Schedule(InBUName);
        
        [OutErrorMsg ,OutXLSName ] = QF5_UnRealised_Report(InBUName);
         
    else %% After_Reset
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_FTD(InBUName);
        
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTM(InBUName);
        
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTY(InBUName);
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end