% function parse_bbg_vols_file
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/29 10:14:28 $
%  $Revision: 1.1 $
%
% try
    
    hWaitbar = waitbar(0,'Please wait...');
    
    Prompt        = {'Enter the Valuation Date(yyyy-mm-dd format):','Enter the Settlement Date for US products(yyyy-mm-dd format):','Enter the Settlement Date for UK products(yyyy-mm-dd format):'};
    Name          = 'Date Input for Murex Vols Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(today,'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    DBValueDate    = InputDates(1);
    DBSettleDateUS = InputDates(2);
    DBSettleDateUK = InputDates(3);
    
    if isempty(DBValueDate)
        DBValueDate  = {datestr(today,'yyyy-mm-dd')};
    end
    DBSettleDate = [DBSettleDateUS,DBSettleDateUK];
    if isempty(DBSettleDate)
        DBSettleDate = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    
    [OptionFileNames,OptionPathName] = uigetfile('*.*','Select the Vols Option Dump File (namr/eur)','Multiselect','on');
    
    [DBFieldNames,DBData] = read_from_database('vol_id_table',0);
    VolDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_OptCode = VolDataDB.bbg_opt_ticker;
    DBVolId = VolDataDB.vol_id;
    
    
    %% Option dump parser
    if isnumeric(OptionFileNames) && OptionFileNames==0
        errordlg('Option Dump file not selected!','FTP Parser');
    else        
        RequiredOptionFields = {'TICKER','OPT_PUT_CALL','OPT_STRIKE_PX',...            
            'OPT_IMPLIED_VOLATILITY_LAST','PX_SETTLE_LAST_DT','OPT_UNDL_PX'};
        
        if ~iscellstr(OptionFileNames)
            OptionFileNames = cellstr(OptionFileNames);
        end
        if ~iscellstr(OptionPathName)
            OptionPathName = cellstr(OptionPathName);
        end
        
        FileName = fullfile(OptionPathName,OptionFileNames);
        NumOptFiles = length(FileName);
        OptionData = [];
        waitbar(0.1,hWaitbar,'Please wait, parsing option dump file');
        for iFile = 1:NumOptFiles
            OutData = parse_option_file(FileName{iFile},BBG_OptCode);
            if isempty(OutData)
                continue;
            end
%             xlswrite(['raw_option_dump_',OptionFileNames{iFile},'.xlsx'],dataset2cell(OutData));
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            DumpSettleDate = cellstr(datestr(datenum(OutData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
            IdxDate = ismember(DumpSettleDate,DBSettleDate);
            IdxComdty = strcmpi('Comdty',OutData.MARKET_SECTOR_DES); % to ignore the others like curncy and index market types, since they return irrelevant data
            IdxFound = IdxDate & IdxComdty;
            for iField = 1:length(RequiredOptionFields)
                TempField = RequiredOptionFields{iField};
                if isfield(OptionData,TempField)
                    OptionData.(TempField) = [OptionData.(TempField);OutData.(TempField)(IdxFound)];
                else
                    OptionData.(TempField) = OutData.(TempField)(IdxFound);
                end
            end
            clear OutData;
        end
        
             
        % last imp vols
        VolID = cell(size(OptionData.TICKER));
        for iCode = 1:length(BBG_OptCode)
            Idx = strmatch(BBG_OptCode{iCode},OptionData.TICKER);           
            VolID(Idx) = DBVolId(iCode);
        end             
       
        SettleVols  = str2double(OptionData.OPT_IMPLIED_VOLATILITY_LAST)/100;
        Strike       = str2double(OptionData.OPT_STRIKE_PX);
       
        % Check the NaN vols and ignore them before updating in excel,
        % since they cannot be uploaded in database
        NonNanIdx  = ~isnan(SettleVols);
        Value_Date = repmat(DBValueDate,size(SettleVols(NonNanIdx)));
        
        SettleVolHeader = {'Value Date','VolId','Strike','Call_Put_Id','Settle Vol'};
        SettleVolData = [Value_Date,VolID(NonNanIdx),num2cell(Strike(NonNanIdx)),OptionData.OPT_PUT_CALL(NonNanIdx),num2cell(SettleVols(NonNanIdx))];
        xlswrite(['Settlement_LastVol_Surf_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettleVolHeader;SettleVolData]);
        
%         [~,~,RawData] = xlsread('murex_strike_atm_factors.xlsx');   
%         RefProductCode = RawData(2:end,1);
%         RefStrikeFactor = cell2mat(RawData(2:end,2));
%         RefMurexATM = cell2mat(RawData(2:end,3));
%         
%         SettleVolData = cell2dataset([SettleVolHeader;SettleVolData]);
%         AssetPrice = zeros(size(SettleVolData.Strike));
%         [ProductCode,Rem] = strtok(SettleVolData.VolId,' ');
%         for iP = 1:length(RefProductCode)
%             IdxProd = strcmpi(strtrim(RefProductCode(iP)),ProductCode);
%             AssetPrice(IdxProd) = RefMurexATM(iP) * RefStrikeFactor(iP);
%         end
%                 SettleVolData.AssetPrice = AssetPrice;

        
        AssetPrice = str2double(OptionData.OPT_UNDL_PX(NonNanIdx));
        SettleVolData = [SettleVolData,num2cell(AssetPrice)];
        SettleVolHeader = [SettleVolHeader,'AssetPrice'];
        SettleVolData = [SettleVolHeader;SettleVolData];
        save(['RawSettleVol_',datestr(DBValueDate,'yyyymmdd')],'SettleVolData');
        
        SettleVolData = cell2dataset(SettleVolData);
        uniqueComb = cellstr([char(SettleVolData.VolId),num2str(SettleVolData.Strike)]);
        UniqueVolComb = unique(uniqueComb,'stable');
        
        CalcSettleVols = SettleVolData.SettleVol;
        
        for i = 1:length(UniqueVolComb)
            IdxITM       = logical(zeros(size(uniqueComb)));
            IdxITMCallId = logical(zeros(size(uniqueComb)));
            IdxITMPutId  = logical(zeros(size(uniqueComb)));
            
            IdxOTM       = logical(zeros(size(uniqueComb)));
            IdxOTMCallId = logical(zeros(size(uniqueComb)));
            IdxOTMPutId  = logical(zeros(size(uniqueComb)));
            
            IdxComb = strcmpi(UniqueVolComb(i),uniqueComb);
            
            % if strike > asset_price, use put_vol
            IdxITM(IdxComb)       = SettleVolData.Strike(IdxComb) > SettleVolData.AssetPrice(IdxComb);
            IdxITMPutId(IdxComb)  = IdxITM(IdxComb) & strcmpi('P',SettleVolData.Call_Put_Id(IdxComb));
            IdxITMCallId(IdxComb) = IdxITM(IdxComb) & strcmpi('C',SettleVolData.Call_Put_Id(IdxComb));
            if any(IdxITMCallId) && any(IdxITMPutId)
                CalcSettleVols(IdxITMCallId) = CalcSettleVols(IdxITMPutId);
            end
            
            % if strike <= asset_price, use call_vol
            IdxOTM(IdxComb)       = SettleVolData.Strike(IdxComb) <= SettleVolData.AssetPrice(IdxComb);
            IdxOTMPutId(IdxComb)  = IdxOTM(IdxComb) & strcmpi('P',SettleVolData.Call_Put_Id(IdxComb));
            IdxOTMCallId(IdxComb) = IdxOTM(IdxComb) & strcmpi('C',SettleVolData.Call_Put_Id(IdxComb));
            if any(IdxOTMPutId) && any(IdxOTMCallId)
                CalcSettleVols(IdxOTMPutId) = CalcSettleVols(IdxOTMCallId);
            end
            
        end
        
        SettleVolData.CalcSettleVols = CalcSettleVols;
        
        xlswrite(['Calculated_SettleVol_for_murex_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],dataset2cell(SettleVolData));
    end % end of option dump file selection
    
   delete(hWaitbar);
    
    
    
% catch ME
%     errordlg(ME.message);
%     delete(hWaitbar);
% end
% end