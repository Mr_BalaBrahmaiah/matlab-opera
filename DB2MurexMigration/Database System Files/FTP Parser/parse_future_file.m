function OutData = parse_future_file(FileName,BBG_UndId)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:24:36 $
%  $Revision: 1.1 $
%

RawData = textread(FileName,'%s','delimiter','\n');

% Below lines are needed to remove the lines where values are not fetched
% from bloomberg
IdxRemove = strmatch('SECTYP',RawData);
RawData(IdxRemove) = [];

Start_Of_Data = strmatch('START-OF-DATA',RawData);
End_Of_Data = strmatch('END-OF-DATA',RawData);
IdxData = (Start_Of_Data + 1) : (End_Of_Data - 1);
InData = RawData(IdxData);

clear RawData; clear IdxData;

SearchPattern = strjoin(strcat('^',BBG_UndId,'.')','|');

Index = regexp(InData,SearchPattern);
TempIndex = ~(cellfun(@isempty,Index));
FoundIndex = find(TempIndex);

for i = 1:length(FoundIndex)    
    Data(i,:) = strsplit(InData{FoundIndex(i)},'|');
end
   
OutData.FutureName = strrep(Data(:,1),' Comdty','');
OutData.FuturePrice = Data(:,4);
OutData.SettlementDate = Data(:,5);

end




