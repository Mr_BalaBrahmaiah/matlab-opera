% function parse_ftp_dump_new
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 05:24:36 $
%  $Revision: 1.1 $
%
% try
    
    hWaitbar = waitbar(0,'Please wait...');
    
    Prompt        = {'Enter the Valuation Date(yyyy-mm-dd format):','Enter the Settlement Date for US products(yyyy-mm-dd format):','Enter the Settlement Date for UK products(yyyy-mm-dd format):'};
    Name          = 'Date Input for FTP Parser';
    Numlines      = 1;
    Defaultanswer = {datestr(today,'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    DBValueDate    = InputDates(1);
    DBSettleDateUS = InputDates(2);
    DBSettleDateUK = InputDates(3);
    
    if isempty(DBValueDate)
        DBValueDate  = {datestr(today,'yyyy-mm-dd')};
    end
    DBSettleDate = [DBSettleDateUS,DBSettleDateUK];
    if isempty(DBSettleDate)
        DBSettleDate = {datestr(busdate(today,-1),'yyyy-mm-dd')};
    end
    
    [OptionFileNames,OptionPathName] = uigetfile('*.*','Select the Option Dump File (namr/eur)','Multiselect','on');
    
    [FutureFileName,FuturePathName]  = uigetfile('*.*','Select the Future Dump File');
    
    PnpFileName = ['PnPSettleVol_',datestr(DBValueDate,'yyyymmdd'),'.xls'];

    [DBFieldNames,DBData] = read_from_database('underlying_list_table',0);
    FutDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_UndId = FutDataDB.bbg_underlying_id;
    UndId = FutDataDB.underlying_id;
    DBCurrency = FutDataDB.currency;
    DBProductcode = FutDataDB.invenio_product_code;
    IdxEmpty = cellfun(@isempty,BBG_UndId);
    BBG_UndId(IdxEmpty) = [];
    UndId(IdxEmpty) = [];
    DBCurrency(IdxEmpty) = [];
    DBProductcode(IdxEmpty) = [];
    
    [DBFieldNames,DBData] = read_from_database('vol_id_table',0);
    VolDataDB = cell2dataset([DBFieldNames;DBData]);
    BBG_OptCode = VolDataDB.bbg_opt_ticker;
    
    SqlQuery = 'select invenio_product_code,option_tick_size from product_master_table';
    [DBFieldNames,DBData] = read_from_database('product_master_table',0,SqlQuery);
    DBInvProductCode = DBData(:,1);
    DBOptionTickSize = cell2mat(DBData(:,2));
    
    %% Option dump parser
    if isnumeric(OptionFileNames) && OptionFileNames==0
        errordlg('Option Dump file not selected!','FTP Parser');
    else        
        RequiredOptionFields = {'TICKER','OPT_PUT_CALL','OPT_UNDL_PX','OPT_STRIKE_PX',...
            'PX_LAST','PX_OPEN','PX_HIGH','PX_LOW','PX_VOLUME','OPT_OPEN_INT',...
            'OPT_UNDL_TICKER','OPT_IMPLIED_VOLATILITY_MID',...
            'PX_SETTLE_LAST_DT','PX_SETTLE','OPT_EXPIRE_DT',...
            'PX_VOLUME_1D','SECURITY_TYP','CRNCY'};
        
        if ~iscellstr(OptionFileNames)
            OptionFileNames = cellstr(OptionFileNames);
        end
        if ~iscellstr(OptionPathName)
            OptionPathName = cellstr(OptionPathName);
        end
        
        FileName = fullfile(OptionPathName,OptionFileNames);
        NumOptFiles = length(FileName);
        OptionData = [];
        waitbar(0.1,hWaitbar,'Please wait, parsing option dump file');
        for iFile = 1:NumOptFiles
            OutData = parse_option_file(FileName{iFile},BBG_OptCode);
            xlswrite(['raw_option_dump_',OptionFileNames{iFile},'.xlsx'],dataset2cell(OutData));
            % ignore the records for whcih the settlement date does not
            % match with the user entered settlement date
            DumpSettleDate = cellstr(datestr(datenum(OutData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
%           IdxDate = strcmp(DBSettleDate,DumpSettleDate);
            IdxDate = ismember(DumpSettleDate,DBSettleDate);
            IdxComdty = strcmpi('Comdty',OutData.MARKET_SECTOR_DES); % to ignore the others like curncy and index market types, since they return irrelevant data
            IdxFound = IdxDate & IdxComdty;
            for iField = 1:length(RequiredOptionFields)
                TempField = RequiredOptionFields{iField};
                if isfield(OptionData,TempField)
                    OptionData.(TempField) = [OptionData.(TempField);OutData.(TempField)(IdxFound)];
                else
                    OptionData.(TempField) = OutData.(TempField)(IdxFound);
                end
            end
            clear OutData;
        end
        %%        
        SettlePrice  = str2double(OptionData.PX_SETTLE);        
        Strike       = str2double(OptionData.OPT_STRIKE_PX);
        AssetPrice   = str2double(OptionData.OPT_UNDL_PX);
        % TODO - verify the below code
        AssetPrice(AssetPrice<0) = 0.1;
        Strike(Strike<0) = 0;        
        % End of TODO       
               
        % TODO - For Verification - Remove Later
        SettlePriceActual = str2double(OptionData.PX_SETTLE); 
        SettlePriceActual(SettlePriceActual<0) = 0;        
        % End of ToDO - For Verification
        
        % To find the underlying_future_ids for the option ids
        UnderlyingID = cell(size(OptionData.TICKER));
        Currency = cell(size(OptionData.TICKER));
        ProdCode = cell(size(OptionData.TICKER));
        OptionTickSize = zeros(size(OptionData.TICKER));
        for iCode = 1:length(BBG_UndId)
            Idx = strmatch(BBG_UndId{iCode},OptionData.OPT_UNDL_TICKER);            
            UnderlyingID(Idx) = UndId(iCode);
            Currency(Idx) = DBCurrency(iCode);
            ProdCode(Idx) = DBProductcode(iCode);
            IdxTS = find(strcmpi(DBProductcode(iCode),DBInvProductCode)); 
            OptionTickSize(Idx) = DBOptionTickSize(IdxTS);
        end
        
        % Fix: Added on 20 Aug for Settle price/vol correction as per
        % Sumit's mail
         OptType = OptionData.OPT_PUT_CALL;
         IdxCall = strcmpi(OptType,'C');
         IdxPut = strcmpi(OptType,'P');
% % %          Settle Vol/Price Correction:
% % % Call:
% % %                 AssetPrice - Strike >= 0.0
% % %                                 If OptionPrice = AssetPrice - Strike
% % %                                                 OptionPrice = OptionPrice+OptionTickSize
% % %                 AssetPrice - Strike <0.0
% % %                                 If OptionPrice = 0.0
% % %                                                 OptionPrice = OptionTickSize      
% % % 
% % % Put:
% % %                 Strike � AssetPrice <= 0.0
% % %                                 If OptionPrice = Strike - AssetPrice
% % %                                                 OptionPrice = OptionPrice+OptionTickSize
% % %                 Strike � AssetPrice >0
% % %                                 If OptionPrice = 0.0
% % %                                                 OptionPrice = OptionTickSize   

        Idx1Call  = logical(zeros(size(IdxCall))); 
        Idx2Call  = logical(zeros(size(IdxCall)));
        Val1Call  = zeros(size(IdxCall)); 
        Idx1Equal = logical(zeros(size(IdxCall)));
        Idx2Equal = logical(zeros(size(IdxCall)));
                
        Idx1Put = logical(zeros(size(IdxPut)));  
        Idx2Put = logical(zeros(size(IdxPut)));
        Val1Put = zeros(size(IdxPut));
        Idx3Equal = logical(zeros(size(IdxPut)));
        Idx4Equal = logical(zeros(size(IdxPut)));
        
        Idx1Call(IdxCall) = AssetPrice(IdxCall) - Strike(IdxCall) >= 0;
        Val1Call(Idx1Call) = AssetPrice(Idx1Call) - Strike(Idx1Call);
%       Idx1Equal(Idx1Call) = isequal(Val1Call(Idx1Call),SettlePrice(Idx1Call));       
        Idx1Equal(Idx1Call) = arrayfun(@isnumericequal,Val1Call(Idx1Call),SettlePrice(Idx1Call));
        SettlePrice(Idx1Equal) = SettlePrice(Idx1Equal) + OptionTickSize(Idx1Equal);
        
        Idx2Call(IdxCall) = AssetPrice(IdxCall) - Strike(IdxCall) < 0;
%       Idx2Equal(Idx2Call) = isequal(SettlePrice(Idx2Call),0);
        TempVar = zeros(size(SettlePrice(Idx2Call)));
        Idx2Equal(Idx2Call) = arrayfun(@isnumericequal,SettlePrice(Idx2Call),TempVar);
        SettlePrice(Idx2Equal) = OptionTickSize(Idx2Equal);
        
        Idx1Put(IdxPut) = Strike(IdxPut) - AssetPrice(IdxPut) <= 0;
        Val1Put(Idx1Put) = Strike(Idx1Put) - AssetPrice(Idx1Put);
%       Idx3Equal(Idx1Put) = isequal(Val1Put(Idx1Put),SettlePrice(Idx1Put));
        Idx3Equal(Idx1Put) = arrayfun(@isnumericequal,Val1Put(Idx1Put),SettlePrice(Idx1Put));
        SettlePrice(Idx3Equal) = SettlePrice(Idx3Equal) + OptionTickSize(Idx3Equal);
        
        Idx2Put(IdxPut) = Strike(IdxPut) - AssetPrice(IdxPut)  > 0;
%       Idx4Equal(Idx2Put) = isequal(SettlePrice(Idx2Put),0);
        TempVar = zeros(size(SettlePrice(Idx2Put)));
        Idx4Equal(Idx2Put) = arrayfun(@isnumericequal,SettlePrice(Idx2Put),TempVar);
        SettlePrice(Idx4Equal) = OptionTickSize(Idx4Equal);
        % End of Fix: Added on 20 Aug for Settle price/vol correction
        
        % Settlement Price
        SecurityID = cell(size(OptionData.TICKER));
        VolID = cell(size(OptionData.TICKER));
        for iCode = 1:length(BBG_OptCode)
            Idx = strmatch(BBG_OptCode{iCode},OptionData.TICKER);
            for iIdx = 1:length(Idx)
                SecurityID{Idx(iIdx)} = [VolDataDB.vol_id{iCode},OptionData.OPT_PUT_CALL{Idx(iIdx)},' ',num2str(Strike(Idx(iIdx)))];
            end
            VolID(Idx) = VolDataDB.vol_id(iCode);
        end
        Value_Date = repmat(DBValueDate,size(SecurityID));
        DataSource = repmat(cellstr('bbg_dumpfile'),size(SecurityID));
        Settle_Date = cellstr(datestr(datenum(OptionData.PX_SETTLE_LAST_DT,'yyyymmdd'),'yyyy-mm-dd'));
        SettlePriceHeader = {'Security ID','Value Date','Settlement Price',...
            'Data Source','Settlement Date',...
            'Last Price','Open Price','High Price','Low Price','Open Int',...
            'Volume','Volume 1D','Imp Vol Mid'};
        SettlePriceData = [SecurityID,Value_Date,num2cell(SettlePrice),...
            DataSource,Settle_Date,...
            num2cell(str2double(OptionData.PX_LAST)),num2cell(str2double(OptionData.PX_OPEN)),...
            num2cell(str2double(OptionData.PX_HIGH)),num2cell(str2double(OptionData.PX_LOW)),...
            num2cell(str2double(OptionData.OPT_OPEN_INT)),num2cell(str2double(OptionData.PX_VOLUME)),...
            num2cell(str2double(OptionData.PX_VOLUME_1D)),num2cell(str2double(OptionData.OPT_IMPLIED_VOLATILITY_MID)/100)]; 
        
        xlswrite(['Settlement_Price_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettlePriceHeader;SettlePriceData]);
        
        % TODO - For Verification - Remove Later
        SettlePriceDataActual = [SecurityID,Value_Date,num2cell(SettlePriceActual),...
            DataSource,Settle_Date,...
            num2cell(str2double(OptionData.PX_LAST)),num2cell(str2double(OptionData.PX_OPEN)),...
            num2cell(str2double(OptionData.PX_HIGH)),num2cell(str2double(OptionData.PX_LOW)),...
            num2cell(str2double(OptionData.OPT_OPEN_INT)),num2cell(str2double(OptionData.PX_VOLUME)),...
            num2cell(str2double(OptionData.PX_VOLUME_1D)),num2cell(str2double(OptionData.OPT_IMPLIED_VOLATILITY_MID)/100)]; 
        xlswrite(['Settlement_Price_Table_Actual_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettlePriceHeader;SettlePriceDataActual]);
        % End of ToDO - For Verification
        
        
        %%
        % security_info 
        OptionType = OptionData.OPT_PUT_CALL;
        OptionType = strrep(OptionType,'C','vanilla_call');
        OptionType = strrep(OptionType,'P','vanilla_put');
        MaturityDate = datenum(OptionData.OPT_EXPIRE_DT,'yyyymmdd');
       
        OptExpiry_Date = cellstr(datestr(MaturityDate,'yyyy-mm-dd'));
        Temp = repmat(cellstr('NULL'),size(OptionData.TICKER));
        SettlementType = repmat(cellstr('physical_settled'),size(OptionData.TICKER));
        SettlementSource = repmat(cellstr('exchange_traded'),size(OptionData.TICKER));
                
        SecurityInfo_Header = {'security_id','strike','vol1_id','vol2_id',...
            'maturity_date','derivative_type','settlement_type','underlying1_id',...
            'underlying2_id','quanto_type','settlement_source','currency',...
            'barrier_type','barrier_obs_type','barrier1_level','barrier1_obs_startdate',...
            'barrier1_obs_enddate','barrier1_rebate','barrier2_level','barrier2_obs_startdate',...
            'barrier2_obs_enddate','barrier2_rebate','averaging_frequency','averaging_startdate',...
            'averaging_enddate','accumulation_frequency','accumulation_startdate','accumulation_enddate','ContractMonth'};
        SecNull  = repmat(cellstr('NULL'),size(OptionData.TICKER,1),16);
        [~,CM] = strtok(VolID,' ');
        ContractMonth = strtrim(CM);
        
        SecurityInfo = [SecurityID,num2cell(Strike),VolID,Temp,...
            OptExpiry_Date,OptionType,SettlementType,UnderlyingID,...
            Temp,Temp,SettlementSource,Currency,SecNull,ContractMonth];
        xlswrite(['Security_info_from_bbgdump_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SecurityInfo_Header;SecurityInfo]);    
        
        waitbar(0.4,hWaitbar,'Please wait, calculating settle vols');
        % Settlement Vol
        OptionType = OptionData.OPT_PUT_CALL;
        OptionType = strrep(OptionType,'C','call');
        OptionType = strrep(OptionType,'P','put');
        
%         TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today +1;
        TimeShift = datenum(DBValueDate,'yyyy-mm-dd') - today ; % TODO - added on 01-July-2014 for new TTM convention
        
        SettleVols = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, SettlePrice);
        
        % Check the NaN vols and ignore them before updating in excel,
        % since they cannot be uploaded in database
        NonNanIdx  = ~(cellfun(@isnan,SettleVols));
        Value_Date = repmat(DBValueDate,size(SettleVols(NonNanIdx)));
        
        SettleVolHeader = {'Value Date','VolId','Strike','Call_Put_Id','Settle Vol'};
        SettleVolData = [Value_Date,VolID(NonNanIdx),num2cell(Strike(NonNanIdx)),OptionData.OPT_PUT_CALL(NonNanIdx),SettleVols(NonNanIdx)];
        
        xlswrite(['Settlement_Vol_Surf_Table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettleVolHeader;SettleVolData]);    
        
        % TODO - For Verification - Remove Later
        SettleVolsActual = vanillaimpvolbybs(AssetPrice, Strike, OptionType, MaturityDate, TimeShift, SettlePriceActual);
        Value_Date = repmat(DBValueDate,size(SettleVolsActual));
        SettleVolDataActual = [Value_Date,VolID,num2cell(Strike),OptionData.OPT_PUT_CALL,SettleVolsActual];
        
        xlswrite(['Settlement_Vol_Surf_Table_Actual_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[SettleVolHeader;SettleVolDataActual]);    
        % End of ToDO - For Verification
        
        % PnP Vols format
        IdxPStrike = (Strike <= AssetPrice) & strcmpi('put',OptionType); % required condition
        IdxCStrike = (Strike > AssetPrice) & strcmpi('call',OptionType); % required condition
        IdxICStrike = (Strike <= AssetPrice) & strcmpi('call',OptionType); % incorrect or inverse condition
        IdxIPStrike = (Strike > AssetPrice) & strcmpi('put',OptionType); % incorrect or inverse condition
        Pnp_Vol = SettleVols;
        Pnp_Vol(IdxPStrike) = SettleVols(IdxPStrike);
        Pnp_Vol(IdxCStrike) = SettleVols(IdxCStrike);
        Pnp_Vol(IdxIPStrike) = {NaN};
        Pnp_Vol(IdxICStrike) = {NaN};
        
        NonNanIdx  = ~(cellfun(@isnan,Pnp_Vol));
        
        PnpHeader = {'Vol_ID','Strike','Settle_Vol'};
        PnpData = [VolID(NonNanIdx),num2cell(Strike(NonNanIdx)),Pnp_Vol(NonNanIdx)];
        % all data for verification
        Pnp_RawData = [VolID,num2cell(Strike),num2cell(AssetPrice),OptionType,num2cell(SettlePrice),SettleVols];
        Pnp_RawHeader = {'VolID','Strike','AssetPrice','OptionType','OptionPrice','SettleVol'};
        
        % write in xls files       
        xlswrite(PnpFileName,[PnpHeader;PnpData],'Pnp_Settlement_Vol');
        xlswrite(PnpFileName,[Pnp_RawHeader;Pnp_RawData],'Pnp_RawData');
    end % end of option dump file selection
    
   
    
    %% Future dump parser
    if isnumeric(FutureFileName) && FutureFileName==0
        errordlg('Future Dump file not selected!','FTP Parser');
    else          
        FileName = fullfile(FuturePathName,FutureFileName);
        
        waitbar(0.8,hWaitbar,'Please wait, parsing future dump file');
        
        FutureData = parse_future_file(FileName,BBG_UndId);
        % ignore the records for whcih the settlement date does not
        % match with the user entered settlement date
        % To find the invalid date entries that cannot be handled by
        % datenum function
        InvalidIdx = strcmpi(FutureData.SettlementDate,'N.A.') | strcmpi(FutureData.SettlementDate,'N.A') | ...
            strcmpi(FutureData.SettlementDate,'') | strcmpi(FutureData.SettlementDate,' ');
        FutureData.SettlementDate(InvalidIdx) = {'01/01/1900'};
        DumpSettleDate = cellstr(datestr(datenum(FutureData.SettlementDate,'mm/dd/yyyy'),'yyyy-mm-dd'));
%       IdxDate = ~strcmp(DBSettleDate,DumpSettleDate);
        IdxDate = ~ismember(DumpSettleDate,DBSettleDate);
         
        % underlying settle value computation
        FutureData.FutureName(IdxDate) = [];
        FutureData.FuturePrice(IdxDate)= [];
        
        FutureID = cell(size(FutureData.FutureName));
        for iCode = 1:length(BBG_UndId)
            Idx = strmatch(BBG_UndId{iCode},FutureData.FutureName); %#ok<*MATCH2>
            FutureID(Idx) = UndId(iCode);
        end
        % check for the redundant entries if at all present for the same
        % settlement date 
        [~,UniqIdx,TempIdx] = unique(FutureID,'stable');
        TempVar = zeros(size(UniqIdx));
        Value_Date = repmat(DBValueDate,size(UniqIdx));
        % TODO - Below lines are to divide OR price by 100. Remove them
        % once it is handled in database
        IdxOR = strmatch('OR',FutureID(UniqIdx));
        FutPrice = str2double(FutureData.FuturePrice(UniqIdx));
        FutPrice(IdxOR) = FutPrice(IdxOR)/100;
        % End of TODO
        UndSettleHeader = {'Underlying_Id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield'};
%         UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(str2double(FutureData.FuturePrice(UniqIdx))),num2cell(TempVar),num2cell(TempVar)];
        UndSettleData = [FutureID(UniqIdx),Value_Date,num2cell(FutPrice),num2cell(TempVar),num2cell(TempVar)];

        % write in xls file
        xlswrite(['underlying_settle_value_table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndSettleHeader;UndSettleData]);
        
        % underlying traders value computation
        subportfolio_mapping = {'BO ','MM-BO-CBT-OIV';'C  ','MM-C-CBT-OIV';'C  ','RMS-C-CBT-OIR';...
            'CA ','MM-CA-EOP-OIV';'CC ','MM-CC-NYB-OIV';'CT ','MM-CT-NYB-OIV';'CT ','RMS-CT-NYB-OIR';...
            'DA ','MM-DA-CME-OIV';'DF ','MM-DF-LIF-OIV';'DF ','RMS-DF-LIF-OIR';'FC ','MM-FC-CME-OIV';...
            'FDT','MM-FDT-NYM-OIV';'IJ ','MM-IJ-EOP-OIV';'ISB','MM-ISB-ICE-OIV';'KC ','MM-KC-NYB-OIV';...
            'KC ','RMS-KC-NYB-OIR';'KW ','MM-KW-KCB-OIV';'LC ','MM-LC-CME-OIV';'LH ','MM-LH-CME-OIV';...
            'MO ','MM-MO-ICE-OIV';'OR ','RMS-OR-SCE-OIR';'QC ','MM-QC-LIF-OIV';'S  ','MM-S-CBT-OIV';...
            'S  ','RMS-S-CBT-OIR';'SB ','MM-SB-NYB-OIV';'SB ','RMS-SB-NYB-OIR';'SM ','MM-SM-CBT-OIV';...
            'W  ','MM-W-CBT-OIV';'W  ','W-MM-FY';'W  ','RMS-W-CBT-OIR';...
            'BO ','MM-BO-CBT-OID';'C  ','MM-C-CBT-OID';'CA ','MM-CA-EOP-OID';'CT ','MM-CT-NYB-OID';...
            'DF ','MM-DF-LIF-OID';'IJ ','MM-IJ-EOP-OID';'KC ','MM-KC-NYB-OID';'LC ','MM-LC-CME-OID';...
            'LH ','MM-LH-CME-OID';'MO ','MM-MO-ICE-OID';'QC ','MM-QC-LIF-OID';'S  ','MM-S-CBT-OID';...
            'SB ','MM-SB-NYB-OID';'SM ','MM-SM-CBT-OID';'W  ','MM-W-CBT-OID';'CT ','RMS-CT-NYB-OID';...
            'CC ','MM-CC-NYB-OID';'KW ','MM-KW-KCB-OID'};
        num_pf = length(subportfolio_mapping);
        UndTradersData =[];
        for iP = 1:num_pf
            pf_code = subportfolio_mapping{iP,1};
            subportfolio = subportfolio_mapping(iP,2);
            Index = strncmp(pf_code,UndSettleData(:,1),length(pf_code));
            temp_portfolio = repmat(subportfolio,size(find(Index)));
            temp_data = [temp_portfolio UndSettleData(Index,:)];
            UndTradersData = [UndTradersData;temp_data]; %#ok<AGROW>
        end
        UndTradersHeader = ['Subportfolio',UndSettleHeader];
        
        % write in xls file
        xlswrite(['underlying_traders_value_table_',datestr(DBValueDate,'yyyymmdd'),'.xlsx'],[UndTradersHeader;UndTradersData]);
        
        %% End of Future file parsing
    end    
    
    waitbar(1,hWaitbar,'Parser completed');
    delete(hWaitbar);
    msgbox(['Settlement Price and Pnp Vols are saved successfully in the directory ',pwd],'FTP Parser');
    
% catch ME
%     if ishandle(hWaitbar)
%         delete(hWaitbar);
%     end
%     errordlg(ME.message);
% end
% end