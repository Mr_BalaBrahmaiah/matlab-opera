function [OutErrorMsg,OutXLSfileName] = generate_monthly_reset_deals(InBUName,varargin)

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

if strcmpi(InBUName,'qf5')
    [OutErrorMsg,OutXLSfileName] = generate_monthly_reset_deals_qf5(InBUName);
else
    [OutErrorMsg,OutXLSfileName] = generate_monthly_reset_deals_indexswap(InBUName);
end