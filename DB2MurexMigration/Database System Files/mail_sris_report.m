function mail_sris_report

AccountingTRReport = generate_previous_settle_accounting_traders_report;
PnlReport = generate_previous_accounting_tr_pnl_report;

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

try
configure_mail_settings;

sendmail({'Raghavendra.Sn@olamnet.com','syedakbar.shah@olamnet.com','Poonacha.sp@olamnet.com','madhusudhan.km@olamnet.com','cfsg.mo@olamnet.com'}, ...
    'OPERA-Previous Settlement Vol Report - FOR SYEDS AUTOMATION - ALL USD NUMBERS - WITHOUT DEAL PROFIT', ['Attached is the Accounting Pnl report based on previous settlement vol for COB ''',SettlementDate,''''],{PnlReport});
catch
end

pause(5);

try
    [~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
    ReportNames = ConfigFileData(2:end,1);
    OutputFolders = ConfigFileData(2:end,2);
    IdxFound = strcmpi('MOReports',ReportNames);
    DestinationFolder = OutputFolders{IdxFound};
    
    LogFilename = ['DB_automation_log_',datestr(today),'.txt'];
    
    try
        TempDestinationFolder = [DestinationFolder,'\Previous Settle Accounting Traders Report'];
        OutAccountingTRReport = fullfile(TempDestinationFolder,AccountingTRReport);
        move_reports(AccountingTRReport,OutAccountingTRReport,'MO Previous Settle Accounting Traders Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving Previous Settle Accounting Traders Report under MO Reports!');
    end
    
    try
        TempDestinationFolder = [DestinationFolder,'\Previous Settle Accounting Pnl Report'];
        OutAccountingPnlReport = fullfile(TempDestinationFolder,PnlReport);
        move_reports(PnlReport,OutAccountingPnlReport,'MO Previous Settle Accounting Pnl Report');
    catch
        write_log_file(LogFilename,'Unknown error while saving Previous Settle Accounting Pnl Report under MO Reports!');
    end
    
catch
end