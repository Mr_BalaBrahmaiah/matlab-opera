function [OutErrorMsg,OutFilename] = generate_riskoffice_rms_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_risk_office_otc_report';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    if strcmpi(Data,'No Data')
        OutErrorMsg = {'No OTC data found!'};
        return;
    end
    
    PosProdCode = strcmpi('product_code',ColNames);
    IdxOR = strncmpi(Data(:,PosProdCode),'OR',2);
    Data(IdxOR,PosProdCode) = {'OR'};
    % Data(IdxOR,:) = [];
    
    TableName = 'murex_master_table';
    [MxColNames,MxData] = read_from_database(TableName,0);
    CData = cell2dataset([MxColNames;MxData]);
    RefProdCode = CData.product_code;
    RefProdName = CData.product_name;
    RefMultFactor  = CData.mult_factor;
    
    %% No Need
    
    % NumRows = size(Data,1);
    % ProductName = cell(NumRows,1);
    % MultFactor = zeros(NumRows,1);
    %
    % for iP = 1:length(RefProdCode)
    %     IdxProd = strcmpi(RefProdCode{iP},Data(:,PosProdCode));
    %     ProductName(IdxProd) = RefProdName(iP);
    %     MultFactor(IdxProd) = RefMultFactor(iP);
    % end
    %
    
    %
    % Data = [Data,ProductName];
    % ColNames = [ColNames,'product_name'];
    
    %%    
    % OutFilename = getXLSFilename('RMO_OTC_Report');
    Filename = ['RMO_OTC_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    %%
    UniqueFields = {'product_code','counterparty_parent','group_type','contract_month'};
    % SumFields = {'active_lots','realised_pnl','unrealised_pnl','total_pnl'};
    % OutputFields = {'product_code','product_name','counterparty_parent',...
    %     'group_type','active_lots','realised_pnl','unrealised_pnl',...
    %     'total_pnl','settlement_date'};
    SumFields = {'active_lots','ytd_realised_pnl','ytd_unrealised_pnl','realised_pnl','unrealised_pnl',...
        'ytd_realised_pnl_usd','ytd_unrealised_pnl_usd','realised_pnl_usd','unrealised_pnl_usd'};
    OutputFields = {'product_code','product_name','counterparty_parent',...
        'group_type','contract_month','active_lots','ytd_realised_pnl','ytd_unrealised_pnl','realised_pnl','unrealised_pnl',...
        'ytd_realised_pnl_usd','ytd_unrealised_pnl_usd','realised_pnl_usd','unrealised_pnl_usd','currency','settlement_date'};
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    OutputHeader = {'Product Code','Product Name','Counterparty','Group Type','Contract Month','Active Lots',...
        'Realised YTD NC','UnRealised YTD NC','Cumulative Realised NC','Cumulative M2M NC',...
        'Realised YTD USD','UnRealised YTD USD','Cumulative Realised USD','Cumulative M2M USD','Currency','COB Date'};
    
    xlswrite(OutFilename,[OutputHeader;OutData],'Summary Report');
    
    Pos_SettlePrice = strcmpi('settle_price',ColNames);
    settle_price   = cell2mat(Data(:,Pos_SettlePrice));
    % Data(:,Pos_SettlePrice) = num2cell(settle_price ./ MultFactor);
    Data(:,Pos_SettlePrice) = num2cell(settle_price);
    
    UniqueFields = {'product_code','counterparty_parent','group_type','contract_month','contract_number'};
    SumFields = {'active_lots','ytd_realised_pnl','ytd_unrealised_pnl','realised_pnl','unrealised_pnl',...
        'ytd_realised_pnl_usd','ytd_unrealised_pnl_usd','realised_pnl_usd','unrealised_pnl_usd'};
    OutputFields = {'product_code','product_name','counterparty_parent',...
        'group_type','contract_month','contract_number','active_lots',...
        'ytd_realised_pnl','ytd_unrealised_pnl','realised_pnl','unrealised_pnl',...
        'ytd_realised_pnl_usd','ytd_unrealised_pnl_usd','realised_pnl_usd','unrealised_pnl_usd',...
        'settle_price','currency','settlement_date'};
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    OutputHeader = {'Product Code','Product Name','Counterparty','Group Type',...
        'Contract Month','Contract Number','Active Lots',...
        'Realised YTD NC','UnRealised YTD NC','Cumulative Realised NC','Cumulative M2M NC',...
        'Realised YTD USD','UnRealised YTD USD','Cumulative Realised USD','Cumulative M2M USD','Market Rate','Currency','COB Date'};
    
    xlswrite(OutFilename,[OutputHeader;OutData],'Detailed Report');
    
    try
        OutXLSFileName = fullfile(pwd,OutFilename);
        xls_delete_sheets(OutXLSFileName);
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end