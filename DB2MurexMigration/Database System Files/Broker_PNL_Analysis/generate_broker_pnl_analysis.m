function [OutErrorMsg ,OutXLSName ] = generate_broker_pnl_analysis(InBUName,Temp_Settle_Date)

%%% Project Name        :   Broker Module
%%% Module Name         :   Broker PnL Analysis
%%% Matlab Version(s)   :   R2016b
%%% Company             :   Invenio Commodity Services Private Limited

%%% Author Name         :   INVENIO
%%% Date_Time           :   10 Sep 2019 / 13:15:15
%
%%% [OutErrorMsg ,OutXLSName ] = generate_broker_pnl_analysis('qf1','2019-09-19')

try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['broker_pnl_analysis_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = Temp_Settle_Date;
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    %%% Fetch data from Broker Recon Column Master Table
    Broker_Recon_Table = ['broker_recon_report_table_',char(InBUName)];
    SqlQuery_Broker_Recon_Table_Data = ['select desk,broker,account,st_date,currency,fut_realised,commision,platform_chg,interest,margin_call,call_back,cash_adj,give_up_comm,currency_conv from ',char(Broker_Recon_Table),' where st_date = ''',char(Settele_date),''' '] ;
    [Broker_Recon_Table_Cols,Broker_Recon_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Recon_Table_Data);

    if strcmpi(Broker_Recon_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Broker_Recon_Table)]} ;
        return;
    end
    %%% consolidation part for Broker Recon Data
    UniqueFields = {'broker','account','st_date','currency'}; 
    SumFields = {'fut_realised','commision','platform_chg','interest','margin_call','call_back','currency_conv','cash_adj','give_up_comm'};
    OutputFields = Broker_Recon_Table_Cols ;
    WeightedAverageFields = [];
    [Consolidate_BR_Data_Header,BR_Data_Consolidate] = consolidatedata(Broker_Recon_Table_Cols, Broker_Recon_Table_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    Broker_Recon_Column_Table_Data = cell2dataset([Consolidate_BR_Data_Header;BR_Data_Consolidate]); %%% convert cell to dataset
    
    %%% Fetch data from Broker Cash Entry Portfolio_Counterparty_Master Table
    Boker_Cash_Entry_Master_Table = ['broker_cashentry_master_table_',char(InBUName)];
    SqlQuery_Boker_Cash_Entry_Master_Table = ['select account_number,currency,instrument,portfolio,counterparty_parent,spot from ',char(Boker_Cash_Entry_Master_Table)] ;
    [Boker_Cash_Entry_Master_Table_Cols,Boker_Cash_Entry_Master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Boker_Cash_Entry_Master_Table);

    if strcmpi(Boker_Cash_Entry_Master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Boker_Cash_Entry_Master_Table)]} ;
        return;
    end 
    Boker_Cash_Entry_Master_Data = cell2dataset([Boker_Cash_Entry_Master_Table_Cols;Boker_Cash_Entry_Master_Table_Data]); %%% convert cell to dataset
    
    %%% Fetch data from Broker Cash Entry Recon Column Master Table
    BCE_Recon_Master_Table = ['broker_balance_report_table_',char(InBUName)];
    SqlQuery_BCE_Recon_Master_Table_Data = ['select desk,broker,account,st_date,currency,variation_margin,non_pass_through_m2m,initial_margin from ',char(BCE_Recon_Master_Table),' where st_date = ''',char(Settele_date),''' '] ;
    [BCE_Recon_Master_Table_Cols,BCE_Recon_Master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BCE_Recon_Master_Table_Data);

    if strcmpi(BCE_Recon_Master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BCE_Recon_Master_Table)]} ;
        return;
    end   
    %%% consolidation part for Broker Recon Data
    UniqueFields = {'broker','account','st_date','currency'}; 
    SumFields = {'variation_margin','non_pass_through_m2m','initial_margin'};
    OutputFields = BCE_Recon_Master_Table_Cols ;
    WeightedAverageFields = [];
    [Consolidate_BB_Data_Header,BB_Data_Consolidate] = consolidatedata(BCE_Recon_Master_Table_Cols, BCE_Recon_Master_Table_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    BCE_Recon_Master_Data = cell2dataset([Consolidate_BB_Data_Header;BB_Data_Consolidate]); %%% convert cell to dataset
    
    %%% Fetch data from Underlying Settle Value Table
    Underlying_settle_value_Table = 'underlying_settle_value_table';
    SqlQuery_Underlying_settle_value = ['select underlying_id, settle_value, settlement_date from ',char(Underlying_settle_value_Table),' where settlement_date=''',char(Settele_date),''' and underlying_id like ''%','spot','%'' '];
    [~,Underlying_settle_value_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Underlying_settle_value);

    if strcmpi(Underlying_settle_value_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Underlying_settle_value_Table),' for ',char(Settele_date)]} ;
        return;
    end
    
    %%% Fetch data from currency_spot_mapping_table
    SqlQuery_currency_spot_mapping = 'select currency,spot from currency_spot_mapping_table';
    [~,currency_spot_mapping_Data] = Fetch_DB_Data(ObjDB,SqlQuery_currency_spot_mapping);

    if strcmpi(currency_spot_mapping_Data,'No Data')
        OutErrorMsg = {'No Data found in currency_spot_mapping_table'} ;
        return;
    end
    
    %%Delete data from Database
    Broker_pnl_analysis_Table = ['broker_pnl_analysis_table_',char(InBUName)];
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',char(Broker_pnl_analysis_Table),' where settle_date = ''',char(Settele_date),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    %%% Fetch data from Broker PnL Analysis Table
    SqlQuery_Broker_pnl_analysis_Table = ['select * from ',char(Broker_pnl_analysis_Table),' where settle_date < ''',char(Settele_date),''' '];
    [Broker_pnl_analysis_Cols,Broker_pnl_analysis_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_pnl_analysis_Table);

    if strcmpi(Broker_pnl_analysis_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Broker_pnl_analysis_Table)]} ;
        return;
    end
    
    Broker_pnl_analysis_Table_Data = cell2dataset([Broker_pnl_analysis_Cols;Broker_pnl_analysis_Data]); %%% convert cell to dataset
    
    %%% Fetch data from Broker PnL Analysis Table unique rows
    Unique_Broker_pnl_analysis = ['broker_pnl_analysis_table_',char(InBUName)];
    Unique_SqlQuery_Broker_pnl_analysis = ['select counterpartyname,account_number,currency from ',char(Unique_Broker_pnl_analysis),' group by counterpartyname,account_number,currency'];
    [Unique_Broker_pnl_analy_Cols,Unique_Broker_pnl_analy_Data] = Fetch_DB_Data(ObjDB,Unique_SqlQuery_Broker_pnl_analysis);

    if strcmpi(Unique_Broker_pnl_analy_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Unique_Broker_pnl_analysis)]} ;
        return;
    end
    
    Unique_Broker_pnl_analy_Tab_Data = cell2dataset([Unique_Broker_pnl_analy_Cols;Unique_Broker_pnl_analy_Data]); %%% convert cell to dataset
    
    
    %%% Headers 
    Up_Header = {'','','Cash Account with broker(Native Currency Books of Account)','','','','','','','','','','','','','','','Balance Sheet (Native Curency books of Accounts)', ...
                 '','','','','','','PnL (in Native Currency books of Accounts)','','','','','','','','','','','','','','','','CCY','YES / NO', ...
                 'Balance Sheet(in Native Currency books of accounts in Equivalent USD Terms)','','','','','','Cash Account with broker(USD Books of Account)', ...
                 '','','','','','','PnL (in USD books of Accounts)','','','','','','','','','','','','','','','','','','','','','','',''};
             
    Down_Header = {'Month','Date','Opening Cash Balance','SS Comm','SS Exchnage Fee','SS Div','SS W.Tax','SS Overnight/ Other','SS Sales', ...
                 'SS  Purchases','Fwd Realized Pnl','Futures Realized PnL','Fut Brokerage/ Other Cost','Fwd Brokerage / Other Cost','TT Charges', ...
                 'Intt','Conversion','Closing Cash Balance','Open Position SS(Cost)','Open PositionSS(Mkt)','Futures Unrealised M2M', ...
                 'Forward Unrealised M2M','Contra Account in NC Books','B/S Control','SS UnRealized PnL FTD','SS Realized PnL FTD','SS Comm', ...
                 'SS Exchange Fee','SS Overnight/ Other','SS Dividend','SS W. Tax','Fut Unrealized PnL FTD','Fut Realized PnL FTD','Fut Brokerage/ Other Cost', ...
                 'Fwd Unrealized PnL FTD','Fwd Realized PnL FTD','Fwd Brokerage /Cost','TT Charges','Intt','Total PnL','BGN','BGN per unit of FC/USD', ...
                 'Closing','Open PositionSS(Mkt)','Futures Unrealised M2M','Forward Unrealised M2M','Contra Account in NC Books','B/S Control', ...
                 'Opening Balance','Movement of funds','PnL FTD','Closing Balance before fx gain','Fx Gain and Loss','Closing Balance after fx gain/loss', ...
                 'B/S Control','SS UnRealized PnL FTD','SS Realized PnL FTD','SS Comm','SS Exchange Fee','SS Overnight/ Other','SS Dividend','SS W. Tax', ...
                 'Fut Unrealized PnL FTD','Fut Realized PnL FTD','Fut Brokerage/ Other Cost','Fwd Unrealized PnL FTD','Fwd Realized PnL FTD', ...
                 'Fwd Brokerage /Cost','TT Charges','Intt','Total PnL before fx gain or loss','PnL Control','Fx Gain or Loss','Initial Margin', ...
                 'FX impact on Cash','FX impact on M2M','FX impact on Conversion','Total FX','check'};

    Main_Header = [Up_Header;Down_Header];

    %%% get the previous working day without considering the holidays
    Busday = busdate(Settele_date, -1);
    Last_wrkg_day = datestr(Busday,'yyyy-mm-dd');
    
    %%%% Broker Cash Entry and Broker Recon Mapping
    %%% Comparing Account Number and Currency and Counterparty
    [row,~] = size(Broker_Recon_Column_Table_Data);
    for ii = 1:1:row
        BRC_broker = Broker_Recon_Column_Table_Data.broker(ii);
        BRC_acc_number = Broker_Recon_Column_Table_Data.account(ii);
        BRC_currency = Broker_Recon_Column_Table_Data.currency(ii);
        
        Matched_Index = strcmpi(BRC_acc_number,Boker_Cash_Entry_Master_Data.account_number) & strcmpi(BRC_currency,Boker_Cash_Entry_Master_Data.currency) & ...
                        contains(Boker_Cash_Entry_Master_Data.counterparty_parent,BRC_broker,'IgnoreCase',true);
        if any(Matched_Index)
            instrument = Boker_Cash_Entry_Master_Data.instrument(Matched_Index);
            underlying_spot_id(ii,1) = Boker_Cash_Entry_Master_Data.spot(Matched_Index);

            %%% get the currency from instrument
            if contains(instrument,'USDUSD')
                final_currency(ii,1) = {'USD'};
            else
                final_currency(ii,1) = erase(strtok(instrument),'USD');
            end
        else
            continue;
        end
    end

    Broker_Recon_Column_Table_Data.underlying_spot_id = underlying_spot_id;
    Broker_Recon_Column_Table_Data.final_currency = final_currency;
    
    %%%% Broker PNL Analysis and Broker Recon Mapping
    %%% Comparing Account Number and Currency and Counterparty
    [row1,~] = size(Unique_Broker_pnl_analy_Tab_Data);
    Overall_ErrorMsg = []; Upload_Required_Tab_Data = [];
    for i = 1:1:row1
        Current_broker = Unique_Broker_pnl_analy_Tab_Data.counterpartyname(i);
        Current_acc_number = Unique_Broker_pnl_analy_Tab_Data.account_number(i);
        Current_currency = Unique_Broker_pnl_analy_Tab_Data.currency(i);
        
        %%%% Broker Exposure and  Broker PNL Analysis Mapping
        %%% Comparing Account Number and Currency and Counterparty 
        Matched_Index_exposure = strcmpi(Current_acc_number,BCE_Recon_Master_Data.account) & strcmpi(Current_currency,BCE_Recon_Master_Data.currency) & ...
                        contains(Current_broker,BCE_Recon_Master_Data.broker,'IgnoreCase',true);
         
        if any(Matched_Index_exposure)
            Matched_exposure_data = BCE_Recon_Master_Data(Matched_Index_exposure,:);
            %%% required fields to sum at Broker Exposure Table
            Futures_Unrealised_M2M1 = sum(Matched_exposure_data.variation_margin) + sum(Matched_exposure_data.non_pass_through_m2m);
            Initial_Margin1 = sum(Matched_exposure_data.initial_margin);
        else
            Futures_Unrealised_M2M1 = 0;
            Initial_Margin1= 0;
        end

        unique_Matched_Index = strcmpi(Current_acc_number,Broker_Recon_Column_Table_Data.account) & strcmpi(Current_currency,Broker_Recon_Column_Table_Data.final_currency) & ...
                        contains(Current_broker,Broker_Recon_Column_Table_Data.broker,'IgnoreCase',true);
                    
        if any(unique_Matched_Index)
            underlying_spot_id = Broker_Recon_Column_Table_Data.underlying_spot_id(unique_Matched_Index);
            Futures_Unrealised_M2M = Futures_Unrealised_M2M1;
            Initial_Margin = Initial_Margin1;
            
            %%% comparing underlying_id
            Spot_Index = strcmpi(underlying_spot_id,Underlying_settle_value_Data(:,1));
            if any(Spot_Index)
                BGN = Underlying_settle_value_Data(Spot_Index,2);
            else
                Overall_ErrorMsg = [Overall_ErrorMsg;{['underlying_id: ',char(underlying_spot_id),' is not found in ',char(Underlying_settle_value_Table)]}];
            end
            
            Matched_data_Sum_fields = Broker_Recon_Column_Table_Data(strcmpi(Current_acc_number,Broker_Recon_Column_Table_Data.account) & strcmpi(Current_currency,Broker_Recon_Column_Table_Data.final_currency) & contains(Current_broker,Broker_Recon_Column_Table_Data.broker,'IgnoreCase',true),:);

            if isempty(Matched_data_Sum_fields)
                Cash_Futures_Realized_PnL = 0;
                Cash_Fut_Brokerage_Other_Cost = 0;
                Cash_TT_Charges = 0;
                Cash_Intt = 0;
                Cash_Conversion = 0;
            else
                %%% required fields to sum at Broker Recon Table
                if strcmpi(Current_currency,'JPY') %%% new logic 24-12-2019, if JPY --> Futures_Realized_PnL / 2
                    Cash_Futures_Realized_PnL = sum(Matched_data_Sum_fields.fut_realised) / 2;
                else
                    Cash_Futures_Realized_PnL = sum(Matched_data_Sum_fields.fut_realised);
                end
                %%% new logic 02-12-2019 (Fut_Brokerage_Other_Cost =
                %%% commision + cash_adj + give_up_comm)
                Cash_Fut_Brokerage_Other_Cost = sum(Matched_data_Sum_fields.commision) + sum(Matched_data_Sum_fields.cash_adj) + sum(Matched_data_Sum_fields.give_up_comm);
                Cash_TT_Charges = sum(Matched_data_Sum_fields.platform_chg);
                Cash_Intt = sum(Matched_data_Sum_fields.interest);
                Cash_Conversion = sum(Matched_data_Sum_fields.margin_call) + sum(Matched_data_Sum_fields.call_back) + sum(Matched_data_Sum_fields.currency_conv);
            end
            Month = datestr(datenum(Settele_date,'yyyy-mm-dd'),'mmm''yy');
            Date = datestr(datenum(Settele_date,'yyyy-mm-dd'),'dd-mmm-yy');
            
        else
            %%% required fields to sum at Broker Recon Table
            Month = datestr(datenum(Settele_date,'yyyy-mm-dd'),'mmm''yy');
            Date = datestr(datenum(Settele_date,'yyyy-mm-dd'),'dd-mmm-yy');
            Futures_Unrealised_M2M = Futures_Unrealised_M2M1;
            Initial_Margin = Initial_Margin1;
            
            %%% SPOT Value
            Currency_Spot_Index = strcmpi(Current_currency,currency_spot_mapping_Data(:,1));
            if any(Currency_Spot_Index)
                Currency_Spot = currency_spot_mapping_Data(Currency_Spot_Index,2);
            else
                Overall_ErrorMsg = [Overall_ErrorMsg;{['Currency: ',char(Current_currency),' is not found in currency_spot_mapping_table']}];
            end
            Spot_Index = strcmpi(Currency_Spot,Underlying_settle_value_Data(:,1));
            if any(Spot_Index)
                BGN = Underlying_settle_value_Data(Spot_Index,2);
            else
                Overall_ErrorMsg = [Overall_ErrorMsg;{['underlying_id: ',char(Currency_Spot),' is not found in ',char(Underlying_settle_value_Table)]}];
            end
            
            Cash_Futures_Realized_PnL = 0;
            Cash_Fut_Brokerage_Other_Cost = 0;
            Cash_TT_Charges = 0;
            Cash_Intt = 0;
            Cash_Conversion = 0;
            
            Overall_ErrorMsg = [Overall_ErrorMsg;{['INFO:- No Data found and we uploaded values with zeros for corresponding Account NUmber: ',char(Current_acc_number),' and Counterparty: ',char(Current_broker),' and Currency: ',char(Current_currency)]}];
        end

        %%% Filter the matched data using counterpartyname, account_number
        %%% and currency from broker_pnl_report table
        Unique_Match_Index = strcmpi(Current_broker,Broker_pnl_analysis_Table_Data.counterpartyname) & strcmpi(Current_acc_number,Broker_pnl_analysis_Table_Data.account_number) & ...
                                strcmpi(Current_currency,Broker_pnl_analysis_Table_Data.currency);
        if any(Unique_Match_Index)
            Unique_Count_acc_cur_data = Broker_pnl_analysis_Table_Data(Unique_Match_Index,:);
            
            %%% get the last working day trade values
            Previous_Match_Index = strcmpi(Current_broker,Unique_Count_acc_cur_data.counterpartyname) & strcmpi(Current_acc_number,Unique_Count_acc_cur_data.account_number) & ...
                      strcmpi(Current_currency,Unique_Count_acc_cur_data.currency) & strcmpi(Last_wrkg_day,Unique_Count_acc_cur_data.settle_date);
        
            if any(Previous_Match_Index)
                Curr_Opening_Cash_Balance = Unique_Count_acc_cur_data.closing_cash_balance_bnc(Previous_Match_Index);
                Previous_Native_Open_Position_SS_Cost = Unique_Count_acc_cur_data.open_position_ss_cost_bnc(Previous_Match_Index);
                Previous_Native_Open_PositionSS_Mkt = Unique_Count_acc_cur_data.open_positionss_mkt_bnc(Previous_Match_Index);
                Previous_Native_Futures_Unrealised_M2M = Unique_Count_acc_cur_data.futures_unrealised_m2m_bnc(Previous_Match_Index);
                Previous_Native_Forward_Unrealised_M2M = Unique_Count_acc_cur_data.forward_unrealised_m2m_bnc(Previous_Match_Index);
                Previous_Native_Contra_Account_in_NC_Books = Unique_Count_acc_cur_data.contra_account_in_nc_books_bnc(Previous_Match_Index);
                Previous_BGN_per_unit_of_FC_USD = Unique_Count_acc_cur_data.bgn_per_unit_of_fc_usd(Previous_Match_Index);
                Curr_Opening_Balance = Unique_Count_acc_cur_data.closing_balance_after_fx_gain_loss_cusd(Previous_Match_Index);
            else
                Curr_Opening_Cash_Balance = 0;
                Previous_Native_Open_Position_SS_Cost = 0;
                Previous_Native_Open_PositionSS_Mkt = 0;
                Previous_Native_Futures_Unrealised_M2M = 0;
                Previous_Native_Forward_Unrealised_M2M = 0;
                Previous_Native_Contra_Account_in_NC_Books = 0;
                Previous_BGN_per_unit_of_FC_USD = 0;
                Curr_Opening_Balance = 0;
            end
            
            %%% check column
            Unique_check = Unique_Count_acc_cur_data.fx_gain_or_loss_pusd - Unique_Count_acc_cur_data.total_fx;
            %%% delete the counterpartyname, account_number and currency
            %%% columns for matching columns with output file headers
            Unique_Count_acc_cur_data.counterpartyname = [];
            Unique_Count_acc_cur_data.account_number = [];
            Unique_Count_acc_cur_data.currency = [];
            %%% convert dataset to cell
            Cell_Count_acc_cur_data = dataset2cell(Unique_Count_acc_cur_data);
            Count_acc_cur_data = [Cell_Count_acc_cur_data(2:end,:) , num2cell(Unique_check)];
        else
            Overall_ErrorMsg = [Overall_ErrorMsg;{['New combination of Account NUmber: ',char(Current_acc_number),' & Counterparty: ',char(Current_broker),' & Currency: ',char(Current_currency), 'kindly add to Master']}];
            continue;
        end
        
        %%% Cash Account with broker(Native Currency Books of Account)
        Opening_Cash_Balance = Curr_Opening_Cash_Balance;
        SS_Comm = 0;
        SS_Exchnage_Fee = 0;
        SS_Div = 0;
        SS_W_Tax = 0;
        SS_Overnight_Other = 0;
        SS_Sales = 0;
        SS_Purchases = 0;
        Fwd_Realized_Pnl = 0;
        Futures_Realized_PnL = Cash_Futures_Realized_PnL;
        Fut_Brokerage_Other_Cost = Cash_Fut_Brokerage_Other_Cost;
        Fwd_Brokerage_Other_Cost = 0;
        TT_Charges = Cash_TT_Charges;
        Intt = Cash_Intt;
        Conversion = Cash_Conversion;
        
        %%% Balance Sheet (Native Curency books of Accounts)
        Native_Closing_Cash_Balance = Opening_Cash_Balance + SS_Comm + SS_Exchnage_Fee +SS_Div + SS_W_Tax + SS_Overnight_Other + SS_Sales + SS_Purchases + Fwd_Realized_Pnl + ...
                                    Futures_Realized_PnL + Fut_Brokerage_Other_Cost + Fwd_Brokerage_Other_Cost + TT_Charges + Intt + Conversion;
        Native_Open_Position_SS_Cost = 0;
        Native_Open_PositionSS_Mkt = 0;
        Native_Futures_Unrealised_M2M = Futures_Unrealised_M2M;
        Native_Forward_Unrealised_M2M = 0;
        %Native_Contra_Account_in_NC_Books = (Previous_Native_Contra_Account_in_NC_Books - Conversion) + Total_PnL;
        
        %%% PnL (in Native Currency books of Accounts)
        SS_UnRealized_PnL_FTD = Native_Open_Position_SS_Cost - Native_Open_PositionSS_Mkt - (Previous_Native_Open_Position_SS_Cost - Previous_Native_Open_PositionSS_Mkt);
        SS_Realized_PnL_FTD = Previous_Native_Open_Position_SS_Cost - SS_Purchases - SS_Sales - Native_Open_Position_SS_Cost;
        Native_SS_Comm = -1 * SS_Comm;
        SS_Exchange_Fee = -1 * SS_Exchnage_Fee;
        Native_SS_Overnight_Other = -1 * SS_Overnight_Other;
        SS_Dividend = -1 * SS_Div;
        Native_SS_W_Tax = -1 * SS_W_Tax;
        Fut_Unrealized_PnL_FTD = -1 * (Native_Futures_Unrealised_M2M - Previous_Native_Futures_Unrealised_M2M);
        Native_Fut_Realized_PnL_FTD = -1 * Futures_Realized_PnL;
        Native_Fut_Brokerage_Other_Cost = -1 * Fut_Brokerage_Other_Cost;
        Fwd_Unrealized_PnL_FTD = -1 * (Native_Forward_Unrealised_M2M - Previous_Native_Forward_Unrealised_M2M);
        Fwd_Realized_PnL_FTD = -1 * Fwd_Realized_Pnl;
        Fwd_Brokerage_Cost = -1 * Fwd_Brokerage_Other_Cost;
        Native_TT_Charges = -1 * TT_Charges;
        Native_Intt = -1 * Intt;
        Total_PnL = SS_UnRealized_PnL_FTD + SS_Realized_PnL_FTD + Native_SS_Comm + SS_Exchange_Fee + Native_SS_Overnight_Other + SS_Dividend + ...
                    Native_SS_W_Tax + Fut_Unrealized_PnL_FTD + Native_Fut_Realized_PnL_FTD + Native_Fut_Brokerage_Other_Cost + Fwd_Unrealized_PnL_FTD + ...
                    Fwd_Realized_PnL_FTD + Fwd_Brokerage_Cost + Native_TT_Charges + Native_Intt;

        %%% Balance Sheet (Native Curency books of Accounts) --> Contra_Account_in_NC_Books field
        Native_Contra_Account_in_NC_Books = (Previous_Native_Contra_Account_in_NC_Books - Conversion) + Total_PnL;
        
        %%% B/S Control
        Native_B_S_Control = Native_Closing_Cash_Balance + Native_Open_PositionSS_Mkt + Native_Futures_Unrealised_M2M + ...
                            Native_Forward_Unrealised_M2M + Native_Contra_Account_in_NC_Books;
             
        %%% BGN_per_unit_of_FC_USD
%         Previous_BGN_per_unit_of_FC_USD = Broker_pnl_analysis_Table_Data.bgn_per_unit_of_fc_usd(Settele_date-1);
        if strcmpi(Current_currency,'AUD') || strcmpi(Current_currency,'EUR') || strcmpi(Current_currency,'GBP') || strcmpi(Current_currency,'NZD') || strcmpi(Current_currency,'USD')
            BGN_per_unit_of_FC_USD = cell2mat(BGN);
        else
            BGN_per_unit_of_FC_USD = 1 / cell2mat(BGN);
        end

        %%% Balance Sheet(in Native Currency books of accounts in Equivalent USD Terms)
        Closing = BGN_per_unit_of_FC_USD * Native_Closing_Cash_Balance;
        Open_PositionSS_Mkt = BGN_per_unit_of_FC_USD * Native_Open_PositionSS_Mkt;
        Futures_Unrealised_M2M = BGN_per_unit_of_FC_USD * Native_Futures_Unrealised_M2M;
        Forward_Unrealised_M2M = BGN_per_unit_of_FC_USD * Native_Forward_Unrealised_M2M;
        Contra_Account_in_NC_Books = BGN_per_unit_of_FC_USD * Native_Contra_Account_in_NC_Books;
        Native_usd_B_S_Control = Closing + Open_PositionSS_Mkt + Futures_Unrealised_M2M + Forward_Unrealised_M2M + Contra_Account_in_NC_Books;
        
        %%% Cash Account with broker(USD Books of Account)
        Opening_Balance = Curr_Opening_Balance;
        Movement_of_funds = BGN_per_unit_of_FC_USD * Conversion;
        PnL_Total_PnL_before_fx_gain_or_loss = Total_PnL * BGN_per_unit_of_FC_USD;
        PnL_FTD = -1 * PnL_Total_PnL_before_fx_gain_or_loss;
        Closing_Balance_before_fx_gain = Opening_Balance + Movement_of_funds + PnL_FTD;
        Fx_Gain_and_Loss = Closing + Open_PositionSS_Mkt + Futures_Unrealised_M2M + Forward_Unrealised_M2M - Closing_Balance_before_fx_gain;
        Closing_Balance_after_fx_gain_loss = Closing_Balance_before_fx_gain + Fx_Gain_and_Loss;
        
        %%% B/S Control
        B_S_Control = Contra_Account_in_NC_Books  + Closing_Balance_after_fx_gain_loss;
        
        %%% PnL (in USD books of Accounts)
        PnL_SS_UnRealized_PnL_FTD = SS_UnRealized_PnL_FTD * BGN_per_unit_of_FC_USD;
        PnL_SS_Realized_PnL_FTD = SS_Realized_PnL_FTD * BGN_per_unit_of_FC_USD;
        PnL_SS_Comm = Native_SS_Comm * BGN_per_unit_of_FC_USD;
        PnL_SS_Exchange_Fee = SS_Exchange_Fee * BGN_per_unit_of_FC_USD;
        PnL_SS_Overnight_Other = Native_SS_Overnight_Other * BGN_per_unit_of_FC_USD;
        PnL_SS_Dividend = SS_Dividend * BGN_per_unit_of_FC_USD;
        PnL_SS_W_Tax = Native_SS_W_Tax * BGN_per_unit_of_FC_USD;
        PnL_Fut_Unrealized_PnL_FTD = Fut_Unrealized_PnL_FTD * BGN_per_unit_of_FC_USD;
        Fut_Realized_PnL_FTD = Native_Fut_Realized_PnL_FTD * BGN_per_unit_of_FC_USD;
        PnL_Fut_Brokerage_Other_Cost = Native_Fut_Brokerage_Other_Cost * BGN_per_unit_of_FC_USD;
        PnL_Fwd_Unrealized_PnL_FTD = Fwd_Unrealized_PnL_FTD * BGN_per_unit_of_FC_USD;
        PnL_Fwd_Realized_PnL_FTD = Fwd_Realized_PnL_FTD * BGN_per_unit_of_FC_USD;
        PnL_Fwd_Brokerage_Cost = Fwd_Brokerage_Cost * BGN_per_unit_of_FC_USD;
        PnL_TT_Charges = Native_TT_Charges * BGN_per_unit_of_FC_USD;
        PnL_Intt = Native_Intt * BGN_per_unit_of_FC_USD;
        PnL_Total_PnL_before_fx_gain_or_loss = Total_PnL * BGN_per_unit_of_FC_USD;
        PnL_Control = (PnL_SS_UnRealized_PnL_FTD + PnL_SS_Realized_PnL_FTD + PnL_SS_Comm + PnL_SS_Exchange_Fee + PnL_SS_Overnight_Other + ... 
                    PnL_SS_Dividend + PnL_SS_W_Tax + PnL_Fut_Unrealized_PnL_FTD + Fut_Realized_PnL_FTD + PnL_Fut_Brokerage_Other_Cost + ...
                    PnL_Fwd_Unrealized_PnL_FTD + PnL_Fwd_Realized_PnL_FTD + PnL_Fwd_Brokerage_Cost + PnL_TT_Charges + PnL_Intt) - PnL_Total_PnL_before_fx_gain_or_loss;
        PnL_Fx_Gain_or_Loss = -1 * Fx_Gain_and_Loss;
        
        %%% Other Fields
        FX_impact_on_Cash = Opening_Cash_Balance * (Previous_BGN_per_unit_of_FC_USD - BGN_per_unit_of_FC_USD);
        FX_impact_on_M2M = Previous_Native_Futures_Unrealised_M2M * (Previous_BGN_per_unit_of_FC_USD - BGN_per_unit_of_FC_USD);
        FX_impact_on_Conversion = Conversion * ((Movement_of_funds / Conversion) - BGN_per_unit_of_FC_USD);
        if isnan(FX_impact_on_Conversion)
            FX_impact_on_Conversion = 0;
        end
        Total_FX = FX_impact_on_Cash + FX_impact_on_M2M + FX_impact_on_Conversion;
        check = PnL_Fx_Gain_or_Loss - Total_FX;
    
        %%% Total Required Fields Data 
        Final_Required_Data = [Month,Date,Opening_Cash_Balance,SS_Comm,SS_Exchnage_Fee,SS_Div,SS_W_Tax,SS_Overnight_Other,SS_Sales,SS_Purchases, ...
                Fwd_Realized_Pnl,Futures_Realized_PnL,Fut_Brokerage_Other_Cost,Fwd_Brokerage_Other_Cost,TT_Charges,Intt,Conversion,Native_Closing_Cash_Balance, ...
                Native_Open_Position_SS_Cost,Native_Open_PositionSS_Mkt,Native_Futures_Unrealised_M2M,Native_Forward_Unrealised_M2M,Native_Contra_Account_in_NC_Books, ...
                Native_B_S_Control,SS_UnRealized_PnL_FTD,SS_Realized_PnL_FTD,Native_SS_Comm,SS_Exchange_Fee,Native_SS_Overnight_Other,SS_Dividend,Native_SS_W_Tax, ...
                Fut_Unrealized_PnL_FTD,Native_Fut_Realized_PnL_FTD,Native_Fut_Brokerage_Other_Cost,Fwd_Unrealized_PnL_FTD,Fwd_Realized_PnL_FTD,Fwd_Brokerage_Cost, ...
                Native_TT_Charges,Native_Intt,Total_PnL,BGN,BGN_per_unit_of_FC_USD,Closing,Open_PositionSS_Mkt,Futures_Unrealised_M2M,Forward_Unrealised_M2M, ...
                Contra_Account_in_NC_Books,Native_usd_B_S_Control,Opening_Balance,Movement_of_funds,PnL_FTD,Closing_Balance_before_fx_gain,Fx_Gain_and_Loss, ...
                Closing_Balance_after_fx_gain_loss,B_S_Control,PnL_SS_UnRealized_PnL_FTD,PnL_SS_Realized_PnL_FTD,PnL_SS_Comm,PnL_SS_Exchange_Fee,PnL_SS_Overnight_Other, ...
                PnL_SS_Dividend,PnL_SS_W_Tax,PnL_Fut_Unrealized_PnL_FTD,Fut_Realized_PnL_FTD,PnL_Fut_Brokerage_Other_Cost,PnL_Fwd_Unrealized_PnL_FTD,PnL_Fwd_Realized_PnL_FTD, ...
                PnL_Fwd_Brokerage_Cost,PnL_TT_Charges,PnL_Intt,PnL_Total_PnL_before_fx_gain_or_loss,PnL_Control,PnL_Fx_Gain_or_Loss,Initial_Margin,FX_impact_on_Cash, ...
                FX_impact_on_M2M,FX_impact_on_Conversion,Total_FX,check];

        Upload_Required_Data = [Current_broker,Current_acc_number,Current_currency,Month,Settele_date,Opening_Cash_Balance,SS_Comm,SS_Exchnage_Fee,SS_Div,SS_W_Tax,SS_Overnight_Other,SS_Sales,SS_Purchases, ...
                Fwd_Realized_Pnl,Futures_Realized_PnL,Fut_Brokerage_Other_Cost,Fwd_Brokerage_Other_Cost,TT_Charges,Intt,Conversion,Native_Closing_Cash_Balance, ...
                Native_Open_Position_SS_Cost,Native_Open_PositionSS_Mkt,Native_Futures_Unrealised_M2M,Native_Forward_Unrealised_M2M,Native_Contra_Account_in_NC_Books, ...
                Native_B_S_Control,SS_UnRealized_PnL_FTD,SS_Realized_PnL_FTD,Native_SS_Comm,SS_Exchange_Fee,Native_SS_Overnight_Other,SS_Dividend,Native_SS_W_Tax, ...
                Fut_Unrealized_PnL_FTD,Native_Fut_Realized_PnL_FTD,Native_Fut_Brokerage_Other_Cost,Fwd_Unrealized_PnL_FTD,Fwd_Realized_PnL_FTD,Fwd_Brokerage_Cost, ...
                Native_TT_Charges,Native_Intt,Total_PnL,BGN,BGN_per_unit_of_FC_USD,Closing,Open_PositionSS_Mkt,Futures_Unrealised_M2M,Forward_Unrealised_M2M, ...
                Contra_Account_in_NC_Books,Native_usd_B_S_Control,Opening_Balance,Movement_of_funds,PnL_FTD,Closing_Balance_before_fx_gain,Fx_Gain_and_Loss, ...
                Closing_Balance_after_fx_gain_loss,B_S_Control,PnL_SS_UnRealized_PnL_FTD,PnL_SS_Realized_PnL_FTD,PnL_SS_Comm,PnL_SS_Exchange_Fee,PnL_SS_Overnight_Other, ...
                PnL_SS_Dividend,PnL_SS_W_Tax,PnL_Fut_Unrealized_PnL_FTD,Fut_Realized_PnL_FTD,PnL_Fut_Brokerage_Other_Cost,PnL_Fwd_Unrealized_PnL_FTD,PnL_Fwd_Realized_PnL_FTD, ...
                PnL_Fwd_Brokerage_Cost,PnL_TT_Charges,PnL_Intt,PnL_Total_PnL_before_fx_gain_or_loss,PnL_Control,PnL_Fx_Gain_or_Loss,Initial_Margin,FX_impact_on_Cash, ...
                FX_impact_on_M2M,FX_impact_on_Conversion,Total_FX];
            
        Upload_Required_Tab_Data = [Upload_Required_Tab_Data ; Upload_Required_Data]; 

        Sheet_Name = strcat(Current_broker,Current_acc_number,Current_currency);
        %%% write data into file
        xlswrite(OutXLSName,[Main_Header ; [Count_acc_cur_data ; Final_Required_Data]],char(Sheet_Name));
        xls_delete_sheets(fullfile(pwd,OutXLSName));
        %%% Merge Cells and provide the colour to cells   
        xlCenter = -4108;
        Excel = actxserver('Excel.Application');
        Excel.Visible = 0; % to see the excel file real time make 1;
        Workbook = Excel.Workbooks.Open(fullfile(pwd,OutXLSName),0,false);
        Range = Excel.Range('C1:Q1');
        Range.Select;
        Range.MergeCells = 1;
        Range.HorizontalAlignment = xlCenter;

        Range1 = Excel.Range('R1:W1');
        Range1.Select;
        Range1.MergeCells = 1;
        Range1.HorizontalAlignment = xlCenter;

        Range2 = Excel.Range('Y1:AN1');
        Range2.Select;
        Range2.MergeCells = 1;
        Range2.HorizontalAlignment = xlCenter;
        
        Range3 = Excel.Range('AQ1:AV1');
        Range3.Select;
        Range3.MergeCells = 1;
        Range3.HorizontalAlignment = xlCenter;

        Range4 = Excel.Range('AW1:BB1');
        Range4.Select;
        Range4.MergeCells = 1;
        Range4.HorizontalAlignment = xlCenter;

        Range5 = Excel.Range('BD1:BU1');
        Range5.Select;
        Range5.MergeCells = 1;
        Range5.HorizontalAlignment = xlCenter;
        
        %%% Set the color to cell using RGB colors
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('C1:Q1').Interior.ColorIndex = 4;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('R1:W1').Interior.ColorIndex =8;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('Y1:AN1').Interior.ColorIndex = 6;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('AQ1:AV1').Interior.ColorIndex = 15;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('AW1:BB1').Interior.ColorIndex = 24;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('BD1:BU1').Interior.ColorIndex = 12;
        Workbook.Worksheets.Item(char(Sheet_Name)).Range('A2:CA2').Interior.ColorIndex = 22;

        % Save & close & Quit Workbook
        Workbook.Save();
        Workbook.Close();
        Excel.Quit();
        
    end

    %%%% Upload the Final Broker PNL Analysis Data
    try
        upload_in_database(Broker_pnl_analysis_Table,Upload_Required_Tab_Data,ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Broker_pnl_analysis_Table)]};
    end
    
    if ~isempty(Overall_ErrorMsg)
        xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Sheet');
        %%%% Active user sheet
        xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Error_Sheet'); 
    end
 
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);

end
