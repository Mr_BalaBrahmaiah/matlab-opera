function [OutErrorMsg,OutFilename] = generate_netting_pnl_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
%     OutFilename = getXLSFilename('Netting_pnl_report');
    Filename = ['Netting_pnl_report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_netting_pnl_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        ObjDB = connect_to_database;
        DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
        
        UniqueFields = {'subportfolio','product_code','counterparty_parent','group_type','contract_month','contract_number','netting_date'};
        SumFields = {'original_premium_paid'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [SummaryFields,SummaryData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        SummaryFields = strrep(SummaryFields,'original_premium_paid','pnl');
        try
            SummaryData = sortrows(SummaryData,[1 2 3 4 5]);
        catch
            disp('Error while sorting rows: some NaN values are found');
        end
        xlswrite(OutFilename,[SummaryFields;SummaryData],'Total Pnl Summary');
        
        PosNettingDate = strcmpi('netting_date',ColNames);
        IdxNettingDate = strcmpi(DBValueDate,Data(:,PosNettingDate));
        TodaysNetData = Data(IdxNettingDate,:);
        if ~isempty(TodaysNetData)
            UniqueFields = {'subportfolio','product_code','counterparty_parent','group_type','contract_month','contract_number','transaction_date','trade_id','netting_date','netting_id','premium'};
            SumFields = {'active_lots','original_premium_paid'};
            OutputFields = [UniqueFields,SumFields];
            WeightedAverageFields = [];
            [OutputFields,OutData] = consolidatedata(ColNames, TodaysNetData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            OutputFields = strrep(OutputFields,'original_premium_paid','pnl');
            xlswrite(OutFilename,[OutputFields;OutData],'ForTheDay Pnl Summary');        
        end
        
        try
            xls_delete_sheets(fullfile(pwd,OutFilename));
        catch
        end
        
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end