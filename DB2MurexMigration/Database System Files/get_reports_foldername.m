function [ ReportsFolder ] = get_reports_foldername
try
DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
IdxFound = cellStrfind(DBConfigData,'reportsfolder');
if ~isempty(IdxFound)
    [~,ReportsFolder] = strtok(DBConfigData(IdxFound),'=');
    ReportsFolder = char(strrep(ReportsFolder,'=',''));
else
    ReportsFolder = '\\SGTCX-DBAPP01\Opera\download';
end
catch
    ReportsFolder = '\\SGTCX-DBAPP01\Opera\download';
end

end

