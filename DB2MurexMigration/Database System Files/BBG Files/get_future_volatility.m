function get_future_volatility

Prompt        = {'Enter the Settlement Date - Start Date(yyyy-mm-dd format):','Enter the Settlement Date - End Date(yyyy-mm-dd format):'};
Name          = 'Date Input';
Numlines      = 1;
Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

if isempty(InputDates)
    errordlg('Prices will not be fetched since the date input was not given!');
    return;
end

StartSettlementDate = InputDates{1};
EndSettlementDate = InputDates{2};

if isempty(StartSettlementDate)
    StartSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isempty(EndSettlementDate)
    EndSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isequal(StartSettlementDate,EndSettlementDate)
    SettlementDate = cellstr(StartSettlementDate);
else
    BusDates = busdays(datenum(StartSettlementDate), datenum(EndSettlementDate),'daily','2014-12-25');
    SettlementDate = cellstr(datestr(BusDates,'yyyy-mm-dd'));
end

javaaddpath('C:\bbgexe\blpapi3.jar');
objBB = blp;

try
    [~,~,FutureMonths] = xlsread('Futures_Volatility.xlsx','FutureMonths');   
    [~,~,VolTickers] = xlsread('Futures_Volatility.xlsx','VolatilityTickers');
    [~,~,TickerOptions] = xlsread('Futures_Volatility.xlsx','Tickers');
    
catch
    errordlg('Futures_Volatility.xlsx is not available or the file doesnot contain the tabs - FutureMonths ,Tickers,VolatilityTickers ');
    return;
end

FutureMonths = unique(FutureMonths,'stable');
VolTickers = unique(VolTickers,'stable');
TickerOptions = unique(TickerOptions,'stable');

BBGTicker = {};

for iCode = 1:length(FutureMonths)   
        for jTicker = 1:length(VolTickers)
            TempTicker = [FutureMonths{iCode},' ',VolTickers{jTicker}];
            BBGTicker = [BBGTicker;TempTicker];
        end   
end

BBGTicker = strcat(BBGTicker,' VOL BVOL COMDTY');

OutFilename = ['Future_VolData_',datestr(today,'yyyy-mm-dd'),'.xlsx'];
Header = ['Tickername','settlement date',TickerOptions];

hWaitbar = waitbar(0,'Please wait, reading the Futures volatility data...');
for iT = 1:length(BBGTicker)
    OutData = [];
    for iDate = 1:length(SettlementDate)
        SettleDate = SettlementDate{iDate};
        Data = history(objBB, BBGTicker{iT},TickerOptions,SettleDate,SettleDate);
        if ~isempty(Data)
            TempData = [BBGTicker{iT},SettleDate,num2cell(Data(:,2:end))];
            OutData = [OutData;TempData];
        end
    end
    if ~isempty(OutData)
    xlswrite(OutFilename,[Header;OutData],BBGTicker{iT});
    end
    waitbar(iT/length(BBGTicker),hWaitbar);
    
end
    
OutXLSFileName = fullfile(pwd,OutFilename);
try
xls_delete_sheets(OutXLSFileName);
catch
end

delete(hWaitbar);

% for iDate = 1:length(SettlementDate)
%     SettleDate = SettlementDate{iDate};
%     
%     OutData = [];
%     %     hWaitbar = waitbar(0,'Please wait, reading the Futures volatility data...');
%     for iT = 1:length(BBGTicker)
%     Data = history(objBB, BBGTicker{iT},TickerOptions,SettleDate,SettleDate);
%     
%     if ~isempty(Data)
%         TempData = [BBGTicker(iT),cellstr(SettleDate),num2cell(Data(:,2:end))];
%         OutData = [OutData;TempData];
%     end
%     end
%     
%     Header = ['Tickername','settlement date',TickerOptions];    
%     OutFilename = ['Future_VolData_',SettleDate,'.xlsx'];
%     xlswrite(OutFilename,[Header;OutData]);
%     
% end