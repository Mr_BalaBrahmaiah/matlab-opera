function get_option_prices
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/12/17 06:06:13 $
%  $Revision: 1.1 $
%

Prompt        = {'Enter the Settlement Date - Start Date(yyyy-mm-dd format):','Enter the Settlement Date - End Date(yyyy-mm-dd format):'};
Name          = 'Date Input';
Numlines      = 1;
Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

if isempty(InputDates)
    errordlg('Prices will not be fetched since the date input was not given!');
    return;
end

StartSettlementDate = InputDates{1};
EndSettlementDate = InputDates{2};

if isempty(StartSettlementDate)
    StartSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isempty(EndSettlementDate)
    EndSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isequal(StartSettlementDate,EndSettlementDate)
    SettlementDate = cellstr(StartSettlementDate);
else
    BusDates = busdays(datenum(StartSettlementDate), datenum(EndSettlementDate),'daily','2014-12-25');
    SettlementDate = cellstr(datestr(BusDates,'yyyy-mm-dd'));
end

javaaddpath('C:\bbgexe\blpapi3.jar');
objBB = blp;

% [ColNames,Data] = read_from_database('underlying_list_table',0);
% RefData = cell2dataset([ColNames;Data]);
% UndId = RefData.underlying_id;
% BBG_UndId = RefData.bbg_underlying_id;

[~,~,XLOptionsData] = xlsread('Options_Master.xlsx','Options');
RefOptionsData = cell2dataset(XLOptionsData);
Options_BBG_Id = RefOptionsData.BBG_Code;
SecurityId = RefOptionsData.Security_id;

try
    [~,~,TickerOptions] = xlsread('Options_Master.xlsx','OptionTickers');
    if iscolumn(TickerOptions)
        TickerOptions = TickerOptions';
    end
catch
    TickerOptions = {'PX_SETTLE','PX_LAST','PX_OPEN','PX_HIGH','PX_LOW','OPT_OPEN_INT','PX_VOLUME','PX_VOLUME_1D','OPT_IMPLIED_VOLATILITY_MID'};
end

% BBGId = cellfun(@strcat,BBG_UndId,repmat({' Comdty'},size(BBG_UndId)),'UniformOutput',false);
% Data = history(objBB, BBGId,TickerOptions,StartSettlementDate,SettlementDate);
% if ~isempty(Data)
%     TempData = [UndId,cellstr(SettlementDate),num2cell(Data(:,2:end))];
%     OutData = [OutData;TempData];
% end

for iDate = 1:length(SettlementDate)
    SettleDate = SettlementDate{iDate};
    TempValueDate = cellstr(datestr(busdate(SettleDate,1,'2014-12-25'),'yyyy-mm-dd'));
    TempDataSource = {'bbg'};
    hWaitbar = waitbar(0,'Please wait, reading the Option prices...');
    
    OptionsData = fetch_price_from_bbg('Comdty',SecurityId,Options_BBG_Id,TickerOptions,SettleDate,hWaitbar,objBB);
    
    NumRows = size(OptionsData,1);
    DataSource = repmat(TempDataSource,NumRows,1);
    ValueDate = repmat(TempValueDate,NumRows,1);
    
    OutFilename = ['Options_Data_',SettleDate,'.xlsx'];
    
%     Header = ['Options Name','settlement date',TickerOptions];      
%     xlswrite(OutFilename,[Header;OptionsData],'sheet1');
%     
    OutData = [OptionsData(:,1),ValueDate,OptionsData(:,3),DataSource,OptionsData(:,2),OptionsData(:,4:end)];
    OptHeader = {'Security ID','Value Date','Settlement Price','Data Source','Settlement Date','Last Price','Open Price','High Price','Low Price','Open Int','Volume','Volume 1D','Imp Vol Mid'};
    xlswrite(OutFilename,[OptHeader;OutData]);
    
%     msgbox(['BBG data file is saved in ',OutFilename]);
end
end
