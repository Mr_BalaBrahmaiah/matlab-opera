function get_index_prices
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/11 11:36:59 $
%  $Revision: 1.1 $
%

Prompt        = {'Enter the Settlement Date - Start Date(yyyy-mm-dd format):','Enter the Settlement Date - End Date(yyyy-mm-dd format):'};
Name          = 'Date Input';
Numlines      = 1;
Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

if isempty(InputDates)
    errordlg('Prices will not be fetched since the date input was not given!');
    return;
end

StartSettlementDate = InputDates{1};
EndSettlementDate = InputDates{2};

if isempty(StartSettlementDate)
    StartSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isempty(EndSettlementDate)
    EndSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isequal(StartSettlementDate,EndSettlementDate)
    SettlementDate = cellstr(StartSettlementDate);
else
    BusDates = busdays(datenum(StartSettlementDate), datenum(EndSettlementDate),'daily','2015-12-25');
    SettlementDate = cellstr(datestr(BusDates,'yyyy-mm-dd'));
end

javaaddpath('C:\bbgexe\blpapi3.jar');
objBB = blp;

try
    [~,~,TickerOptions] = xlsread('Index_Master.xlsx','Tickers');
    if iscolumn(TickerOptions)
        TickerOptions = TickerOptions';
    end
catch
    errordlg('Index_Master.xlsx is not available or the file doesnot contain Tickers tab');
    return;
end

[~,~,XLIndexData] = xlsread('Index_Master.xlsx','Index');
RefIndexData = cell2dataset(XLIndexData);
Index_DBId = RefIndexData.ticker;
Index_Product = RefIndexData.product;

IdxEmpty = cellfun(@isempty,Index_DBId);
Index_DBId(IdxEmpty) = [];
Index_Product(IdxEmpty) = [];

for iDate = 1:length(SettlementDate)
    SettleDate = SettlementDate{iDate};
    
    Header = ['product','ticker','settlement date',TickerOptions];   
        
    IndexData = [];
    hWaitbar = waitbar(0,'Please wait, reading the Index values...'); 
    
    for iFut = 1:length(Index_DBId)
        try
            BBGId = char(Index_DBId{iFut});
            Data = history(objBB, BBGId,TickerOptions,SettleDate,SettleDate);
            
            if ~isempty(Data)
                TempData = [Index_Product(iFut),Index_DBId(iFut),cellstr(SettleDate),num2cell(Data(:,2:end))];
                IndexData = [IndexData;TempData];
            end
            waitbar(iFut/length(Index_DBId),hWaitbar);
        catch ME
            errordlg(ME.message);
            errordlg([ME.stack(1).file,' :: ',ME.stack(1).name,' :: ',num2str(ME.stack(1).line)]);
            errordlg(ME.identifier);           
            continue;
        end
        
    end
    
    delete(hWaitbar);        
       
    OutFilename = ['Index_Data_',SettleDate,'.xlsx'];
    xlswrite(OutFilename,[Header;IndexData]);
    
    %     msgbox(['BBG data file is saved in ',OutFilename]);
end
end
