function OutData = fetch_price_from_bbg(Type,DBId,BBG_Id,TickerOptions,SettlementDate,hWaitbar,objBB)

IdxEmpty = cellfun(@isempty,BBG_Id);
BBG_Id(IdxEmpty) = [];
DBId(IdxEmpty) = [];

OutData = [];

for iFut = 1:length(DBId)
    try
        BBGId = char([BBG_Id{iFut},' ',Type]);
        Data = history(objBB, BBGId,TickerOptions,SettlementDate,SettlementDate);
        
        if ~isempty(Data)
            TempData = [DBId(iFut),cellstr(SettlementDate),num2cell(Data(:,2:end))];
            OutData = [OutData;TempData];
        end
        waitbar(iFut/length(DBId),hWaitbar);
    catch ME
        errordlg(ME.message);
        errordlg([ME.stack(1).file,' :: ',ME.stack(1).name,' :: ',num2str(ME.stack(1).line)]);
        errordlg(ME.identifier);
        %         errordlg(ME.cause);
        continue;
    end
    
end

delete(hWaitbar);
end