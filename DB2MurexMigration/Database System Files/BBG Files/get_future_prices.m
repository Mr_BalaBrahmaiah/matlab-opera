function get_future_prices
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/11 11:35:36 $
%  $Revision: 1.2 $
%

Prompt        = {'Enter the Settlement Date - Start Date(yyyy-mm-dd format):','Enter the Settlement Date - End Date(yyyy-mm-dd format):'};
Name          = 'Date Input';
Numlines      = 1;
Defaultanswer = {datestr(busdate(today,-1),'yyyy-mm-dd'),datestr(busdate(today,-1),'yyyy-mm-dd')};
InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

if isempty(InputDates)
    errordlg('Prices will not be fetched since the date input was not given!');
    return;
end

StartSettlementDate = InputDates{1};
EndSettlementDate = InputDates{2};

if isempty(StartSettlementDate)
    StartSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isempty(EndSettlementDate)
    EndSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
end

if isequal(StartSettlementDate,EndSettlementDate)
    SettlementDate = cellstr(StartSettlementDate);
else
    BusDates = busdays(datenum(StartSettlementDate), datenum(EndSettlementDate),'daily','2014-12-25');
    SettlementDate = cellstr(datestr(BusDates,'yyyy-mm-dd'));
end

javaaddpath('C:\bbgexe\blpapi3.jar');
objBB = blp;

try
    [~,~,TickerOptions] = xlsread('Futures_Master.xlsx','FutureTickers');
    if iscolumn(TickerOptions)
        TickerOptions = TickerOptions';
    end
catch
    %     TickerOptions = {'PX_LAST','PX_SETTLE','PX_OPEN','PX_HIGH','PX_LOW','PX_VOLUME','rk074','open_int','volatility_10d','volatility_20d','volatility_30d'};
    errordlg('Futures_Master.xlsx is not available or the file doesnot contain FutureTickers tab');
    return;
end

% [ColNames,Data] = read_from_database('underlying_list_table',0);
% RefData = cell2dataset([ColNames;Data]);
% UndId = RefData.underlying_id;
% BBG_UndId = RefData.bbg_underlying_id;

[~,~,XLFuturesData] = xlsread('Futures_Master.xlsx','Futures');
RefFuturesData = cell2dataset(XLFuturesData);
Futures_DBId = RefFuturesData.underlying_id;
Futures_BBG_Id = RefFuturesData.bbg_underlying_id;
    
[~,~,XLCurrencyData] = xlsread('Futures_Master.xlsx','Currency');
RefCurrencyData = cell2dataset(XLCurrencyData);
Currency_DBId = RefCurrencyData.underlying_id;
Currency_BBG_Id = RefCurrencyData.bbg_underlying_id;

[~,~,XLIndexData] = xlsread('Futures_Master.xlsx','Index');
RefIndexData = cell2dataset(XLIndexData);
Index_DBId = RefIndexData.underlying_id;
Index_BBG_Id = RefIndexData.bbg_underlying_id;

% BBGId = cellfun(@strcat,BBG_UndId,repmat({' Comdty'},size(BBG_UndId)),'UniformOutput',false);
% Data = history(objBB, BBGId,TickerOptions,StartSettlementDate,SettlementDate);
% if ~isempty(Data)
%     TempData = [UndId,cellstr(SettlementDate),num2cell(Data(:,2:end))];
%     OutData = [OutData;TempData];
% end
CurrencyTickerOptions = {'PX_LAST'};

for iDate = 1:length(SettlementDate)
    SettleDate = SettlementDate{iDate};
    
    hWaitbar = waitbar(0,'Please wait, reading the Futures prices...');
    FutureData = fetch_price_from_bbg('Comdty',Futures_DBId,Futures_BBG_Id,TickerOptions,SettleDate,hWaitbar,objBB);
    
    Header = ['Currency/Futures Name','settlement date',TickerOptions];
        
    hWaitbar = waitbar(0,'Please wait, reading the Currency values...');    
    CurrencyData = fetch_price_from_bbg('Curncy',Currency_DBId,Currency_BBG_Id,CurrencyTickerOptions,SettleDate,hWaitbar,objBB);
    TempVar = num2cell(nan(size(CurrencyData,1),10));
    CurrencyData = [CurrencyData,TempVar];
    
    hWaitbar = waitbar(0,'Please wait, reading the Index values...');    
    IndexData = fetch_price_from_bbg('Index',Index_DBId,Index_BBG_Id,CurrencyTickerOptions,SettleDate,hWaitbar,objBB);
    TempVar = num2cell(nan(size(IndexData,1),10));
    IndexData = [IndexData,TempVar];
    
    OutData = [CurrencyData;IndexData;FutureData];
    
    OutFilename = ['Future_Data_',SettleDate,'.xlsx'];
    xlswrite(OutFilename,[Header;OutData]);
    
%     msgbox(['BBG data file is saved in ',OutFilename]);
end
end
