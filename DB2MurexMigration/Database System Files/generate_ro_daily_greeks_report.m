function [OutErrorMsg,OutFilename] = generate_ro_daily_greeks_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('RO Daily Greeks Report');
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'OFUT'});
        Data = Data(ispresent_FUT_OFUT,:);
        
        %         Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
        %         Get_CallPut = Data(:,Find_CallPut_Col);
        %         ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
        %         Data(ispresent_Call,Find_CallPut_Col) = {'call'};
        %         ispresent_Put = cellStrfind(Get_CallPut,'p');
        %         Data(ispresent_Put,Find_CallPut_Col) = {'put'};        %% Get 'Call/Put' Row only
        
        %         ispresent_Call_Put = sort([ispresent_Call;ispresent_Put]);
        %         Data = Data(ispresent_Call_Put,:);         %% Get 'Call/Put' Row only
        
        UniqueFields = {'portfolio','asset_class','call_put_id','strike'};
        SumFields = {'original_lots','settle_delta_1','settle_delta_1','settle_gamma_11','settle_vega_1'};
        OutputFields = {'portfolio','asset_class','call_put_id','strike',...
            'original_lots','settle_delta_1','settle_gamma_11','settle_vega_1'};
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        OutHeader = {'Portfolio','Product','Strike','Call/Put',...
            'Nominal','Delta_Lots','Delta_MT','Gamma','Vega'};  %% Check 'Delta_MT' now its put 'settle_delta_1'
        
        xlswrite(OutFilename,[OutHeader;OutData]);
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end