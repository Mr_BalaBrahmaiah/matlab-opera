function [OutErrorMsg, OutXLSName] = generate_deals_upload_status_report(InBUName,Temp_Settle_Date)

%%% Project Name: Deals Upload
%%% Module Name: Deals Upload Status Report
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 20 Feb 2019 / 13:06:00
%
%%% [OutErrorMsg , OutXLSName] = generate_deals_upload_status_report('qf1','2019-02-20')
try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = {''};
    OutXLSName = getXLSFilename(['broker_deal_ticket_table_',char(InBUName)]);
    
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = {Temp_Settle_Date};
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end
    
    %%% fetch data from deal_ticket_table
    DTT_LVrV_TableName=['deal_ticket_table_with_latest_versions_view_',char(InBUName)];
    SqlQuery_DTT_LVrV_Table = ['select * from ',char(DTT_LVrV_TableName),' where transaction_date = ''',char(Settele_date),''' '];

    [DTT_LVrV_Table_Cols,DTT_LVrV_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DTT_LVrV_Table);

    if(strcmpi(DTT_LVrV_Table_Data,'No Data'))
        OutErrorMsg = {['Data not Available in ',char(DTT_LVrV_TableName)]};
        return;
    end
    
    %%% Nelwy added code
    %%% Delete the below col's like recon_datetimestamp, traded_timestamp,
    %%% date_to_be_fixed and reset_date(if qf2) and add the updated_by, updated_timestamp
    DTT_LVrV_Table_Data_dataset = cell2dataset([DTT_LVrV_Table_Cols;DTT_LVrV_Table_Data]);
    
    DTT_LVrV_Table_Data_dataset.recon_datetimestamp = [];
    DTT_LVrV_Table_Data_dataset.traded_timestamp = [];
    DTT_LVrV_Table_Data_dataset.date_to_be_fixed = []; 
    DTT_LVrV_Table_Data_dataset.updated_by = repmat({'system'},size(DTT_LVrV_Table_Data_dataset,1),1);
    DTT_LVrV_Table_Data_dataset.updated_timestamp = repmat({getCurrentDateTimestamp},size(DTT_LVrV_Table_Data_dataset,1),1);
    
    if strcmpi(InBUName,'qf2') || strcmpi(InBUName,'qf1') %% Newly added logic (02-07-2019)
      DTT_LVrV_Table_Data_dataset.reset_date = [];
      DTT_LVrV_Table_Data_dataset.reset_status = []; 
    end

    DTT_LVrV_Table_Data_cell = dataset2cell(DTT_LVrV_Table_Data_dataset);
    DTT_LVrV_Table_Data = DTT_LVrV_Table_Data_cell(2:end,:);
    %%% End Code
    
    %%% Upload the data into Database
    try
        Broker_deal_ticket_Table_Name = strcat('broker_deal_ticket_table_',InBUName);
        set(ObjDB,'AutoCommit','off');
        %%%% Delete data from table
        SqlQuery = ['delete from ',char(Broker_deal_ticket_Table_Name),' where transaction_date = ''',char(Settele_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

        upload_in_database(Broker_deal_ticket_Table_Name,DTT_LVrV_Table_Data,ObjDB);  %%% Upload
        disp('Upload Done Successfully for Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Broker_deal_ticket_Table_Name)]};
    end
    
   %%% Fetch data from deal table
   SqlQuery_Broker_Deal_Table = ['select * from ',char(Broker_deal_ticket_Table_Name),' where transaction_date = ''',char(Settele_date),''' '];
   [Broker_Deal_Table_ColNames,Broker_Deal_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Deal_Table);

    if(strcmpi(Broker_Deal_Table_Data,'No Data'))
        OutErrorMsg = {['Data not Available in ',char(Broker_deal_ticket_Table_Name)]};
        return;
    end
    
    %%% write data into OutFile
    xlswrite(OutXLSName,[Broker_Deal_Table_ColNames;Broker_Deal_Table_Data],'Broker Deal Data');
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
   %%% Check Deals_Count
   Deals_Count = size(Broker_Deal_Table_Data,1);  %%% Find the total count of deals (deals are available or not)
    if eq(Deals_Count,0)
        Deals_Available = {'NO'};
    else
        Deals_Available = {'YES'};
    end
    
    %%% Process for Upload the stastus of deal table
    Deal_Upload_Status_TableName = ['broker_deal_upload_status_table_',char(InBUName)];
    
    %%% Value date
    Value_date = fetch(ObjDB,['select value_date from vdate_sdate_table where settlement_date = ''',char(Settele_date),'''']);  
    %%% Uplaod sytatus is always TRUE (whenever user runs MATLAB File)
    Deal_Status = {'TRUE'};  

    %%% Deal Upload status Data
    Deal_Upload_Status_Table_Data = [Value_date, Settele_date, Deal_Status, Deals_Available];
    
    %%% Upload the Data
     try
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Deal_Upload_Status_TableName),' where settle_date = ''',char(Settele_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

         upload_in_database(Deal_Upload_Status_TableName,Deal_Upload_Status_Table_Data,ObjDB);
         disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Deal_Upload_Status_TableName)]};
     end
    
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end
end
