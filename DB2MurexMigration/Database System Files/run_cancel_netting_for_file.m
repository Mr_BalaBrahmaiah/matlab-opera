try
InBUName='qf5';

UserName='system';

%% Read input file

[filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
InXLSFilename = fullfile(pathname,filename);

[~,~,RawData] = xlsread(InXLSFilename);

InNettingIDs = RawData(2:end,:);

[OutErrorMessage,Overall_Parfor_Errormsg] = cancel_netting(InBUName,InNettingIDs,UserName);

catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end