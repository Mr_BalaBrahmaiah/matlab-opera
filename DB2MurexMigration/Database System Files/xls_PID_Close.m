function xls_PID_Close()

% [status,result] = system('taskkill /F /FI "WINDOWTITLE eq BOOK*"'); %% BOOK means Excel Name 

name = 'excel'; %for example
p = System.Diagnostics.Process.GetProcessesByName(name);

Loop_Length = p.Length ;
for i = 1 : Loop_Length
    
    pid = p(i).Id; %You must index into p (not p.Id), as this changes the class type
    
    pid = p(i).CloseMainWindow ;    
    
end