function [OutErrorMsg ,OutXLSName ] = generate_option_premium_report(InBUName)

try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename(['Option_Premium_Report_',char(upper(InBUName))]);
    
    %% Fetch DB Data
    
    ObjDB = connect_to_database;
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    ViewName = strcat('helper_eod_monthend_report_view_',char(InBUName));
    [InputHeader,InputData] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    
    %% Directional deals Removal
    
    SqlQuery_1 = ['select distinct(subportfolio) from subportfolio_product_mapping_table_',char(InBUName)] ;
    [~,Subportfolio_1] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    
    SqlQuery_2 = ['select distinct(directional_portfolio) from subportfolio_product_mapping_table_',char(InBUName)];
    [~,Subportfolio_2] = Fetch_DB_Data(ObjDB,SqlQuery_2);
    
    Overall_Subportfolio = [Subportfolio_1 ; Subportfolio_2];
    
    Counterparty_Col = cellStrfind_exact(InputHeader,{'counterparty_parent'});
    Matched_Subportfolio_Index = cellStrfind_exact(InputData(:,Counterparty_Col),Overall_Subportfolio);
    
    if(~isempty(Matched_Subportfolio_Index))
        InputData(Matched_Subportfolio_Index,:) = [];
    end
    
    Overall_InData = cell2dataset([InputHeader;InputData]);
    clear InputData;
    
    %% FTY Data
    FTY_Data = Overall_InData;
    
    %% FTD Data
    FTD_Index = cellStrfind_Perfect(Overall_InData.transaction_date,Settle_Date);
    FTD_Data = Overall_InData(FTD_Index,:);
    
    %% FTM Data
    DB_Date_Format = 'yyyy-mm-dd';
    
    Current_Month = str2double(datestr(char(Settle_Date),5));
    Current_Year = str2double(datestr(char(Settle_Date),10));
    CurrentFOM_Date = datestr( fbusdate(Current_Year, Current_Month) ,'yyyy-mm-dd');
    CurrentEOM_Date = datestr( eomdate(Current_Year, Current_Month) ,'yyyy-mm-dd');
    
    HolDates = {'2018-01-01','2018-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    VBusDays = busdays(datenum(CurrentFOM_Date,DB_Date_Format), datenum(CurrentEOM_Date,DB_Date_Format),'daily',HolidayVec);
    
    Month_DateVec = cellstr(datestr(VBusDays,DB_Date_Format));
    
    FTM_Index = cellStrfind_Perfect(Overall_InData.transaction_date,Month_DateVec);
    FTM_Data = Overall_InData(FTM_Index,:);
    
    %%
    
    for i = 1 : 3
        
        %% Required Data
        if(i==1)
            InData = FTY_Data;
            SheetName = 'FTY Data';
        elseif(i==2)
            InData = FTM_Data;
            SheetName = 'FTM Data';
        else
            InData = FTD_Data;
            SheetName = 'FTD Data';
        end
        
        if(isempty(InData))
            xlswrite(OutXLSName,{'No Data'},SheetName);
            continue;
            
        end
        
        %% Compute Monthend EOD
        
        [OutputFields,ConsDeals] = compute_monthend_eod_values(InBUName,InData,SheetName);
        
        %%
        GroupType_Col = cellStrfind_exact(OutputFields,{'GRP'});
        Required_GroupType = {'OFUT','OTC','CSO'}; %% newly added GRP Type :- CSO on 14-06-2019
        Required_GroupType_Index = cellStrfind_exact(ConsDeals(:,GroupType_Col),Required_GroupType);
        
        if(isempty(Required_GroupType_Index))
            % OutErrorMsg = {'Options & OTC Trades not Found..!'};
            % return;
            
            xlswrite(OutXLSName,{'Options & OTC Trades not Found..!'},SheetName);
            continue;
        else
            Required_Data = ConsDeals(Required_GroupType_Index,:);
        end
        
        %% Make Report
        
        UserWant_Header = {'COUNTERPARTY','UNDERLYING','PRODUCT','GRP','CURRENCY','Realized_NC'} ;
        UserWant_Col = cellStrfind_exact(OutputFields,UserWant_Header);
        UserWant_Data = Required_Data(:,UserWant_Col);
        
        COB_Data = cell(size(UserWant_Data,1),1);
        COB_Data(:,:) = Settle_Date;
        
        Report_Header = {'COB Date','Broker name','Instrument','Product name','OPT/OFUT','Native Currency','Month end EOD report Realized'};
        Report_Data = [COB_Data,UserWant_Data];
        
        %% Excel Write
        
        xlswrite(OutXLSName,[Report_Header ; Report_Data],SheetName);
        
    end
    
    %% Delete Sheets
    
    try
        xls_delete_sheets(fullfile(pwd,OutXLSName),'Sheet1');
    catch
        
    end
    
    %%
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end