function OutErrorMessage = cancel_netting_parfor(InBUName,InNettingIDs,UserName)

OutErrorMessage = {'No Errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %     ObjDB = connect_to_database;
    [DBName,DBUserName,DBPassword,DBServerIP,IsFromServer] = Read_DB_ConfigFile();
    javaaddpath('mysql-connector-java-5.1.25-bin.jar');
    p=parpool('local',4);
    c =parallel.pool.Constant(@()database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP,'PortNumber',3306),@close);
    
    DealTicketTableName = ['deal_ticket_table_',InBUName];
    ClonedDealTicketTableName = ['cloned_deal_ticket_table_',InBUName];
    NettingIdTableName = ['netting_id_table_',InBUName];
    
    
    ObjDB_1 = connect_to_database;
    
    for iD = 1:length(InNettingIDs)
        SqlQuery = ['select trade_id from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''''];
        TradeId = fetch(ObjDB_1,SqlQuery);
        if ~isempty(TradeId)
            parfor iTR = 1:length(TradeId)
                ObjDB = c.Value;
                CurrentTradeId = [InNettingIDs{iD},'-',TradeId{iTR}];
                SqlQuery = ['delete from ',DealTicketTableName,' where trade_id = ''',CurrentTradeId,''''];
                Curs = exec(ObjDB,SqlQuery);
                %                 if ~isempty(Curs.message)
                %                     OutErrorMessage = {Curs.message};
                %                 end
                
                SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
                MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
                SqlQuery = ['select * from deal_ticket_table where trade_id = ''',TradeId{iTR},''' and version_no = ',num2str(MaxVersion)];
                [DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery,InBUName);
                DealTicketData = cell2dataset([DBFieldNames;DBData]);
                
                SqlQuery = ['select netting_status,netted_lots,original_lots from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
                OutData = fetch(ObjDB,SqlQuery);
                if strcmpi(OutData{1},'partially_netted')
                    DealTicketData.original_lots = OutData{2} + OutData{3};
                end
                
                DealTicketData.version_no = MaxVersion+1;
                DealTicketData.version_created_by = UserName;
                DealTicketData.version_timestamp = getCurrentDateTimestamp;
                DealTicketData.version_comments = 'netting_cancelled';
                DealTicketData.is_netting_done = 0;
                DealTicketData.lots_to_be_netted = DealTicketData.original_lots;
                
                DealTicketData = dataset2cell(DealTicketData);
                fastinsert(ObjDB,DealTicketTableName,DealTicketData(1,:),DealTicketData(2:end,:));
                
                update(ObjDB,ClonedDealTicketTableName,{'netting_status','netted_lots','netting_id'},...
                    {'not_netted',0,''},{['where trade_id = ''',TradeId{iTR},'''']});
                
                SqlQuery = ['delete from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''''];
                Curs = exec(ObjDB,SqlQuery);
                %                 if ~isempty(Curs.message)
                %                     OutErrorMessage = {Curs.message};
                %                 end
            end
        end
    end
    
    
    delete(p);
    delete(gcp('nocreate'));
    
catch ME
    OutErrorMessage = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end