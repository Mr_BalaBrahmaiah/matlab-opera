
try
    %% Input
    
    InBUName = 'qf5';
    User_Passed_Counterparty = 'MS-KOREA';
    
    InXLSFilename = 'Book2.xlsx';
    
    
    %% Netting Validation
    
    tic;
    [OutErrorMsg ,OutXLSFileName,Final_OutHeader,Final_OutData,OutputHeader_PNL,Overall_OutData] = Calculate_Netting_Validation(InBUName,User_Passed_Counterparty,InXLSFilename);
    
    msgbox(['Netting Validation : ', num2str(toc) ]);
    %     time_string=secs2hms(432446);
    
    %% Calculate  Netting
    
    tic;
    [OutErrorMessage] = calculate_netting_qf5(InBUName,OutputHeader_PNL,Overall_OutData);
    msgbox(['Calculate  Netting : ', num2str(toc) ]);
    
    %% Cancel Netting
    
     [OutErrorMessage,Overall_Parfor_Errormsg] = cancel_netting(InBUName,{'NET-198-914'},'system');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

