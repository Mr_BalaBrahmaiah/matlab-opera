function [DBName,DBUserName,DBPassword,DBServerIP,IsFromServer] = Read_DB_ConfigFile()

%% Default
DBName = 'inveniodbwebpage';
DBUserName = 'user';
DBPassword = 'invenio@123';
DBServerIP = '10.190.7.71';
IsFromServer = 1;

%% DB Config File
try
    DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
    IdxFound = cellStrfind(DBConfigData,'databasename');
    if ~isempty(IdxFound)
        [~,DBName] = strtok(DBConfigData(IdxFound),'=');
        DBName = char(strrep(DBName,'=',''));
    end
    IdxFound = cellStrfind(DBConfigData,'username');
    if ~isempty(IdxFound)
        [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
        DBUserName = char(strrep(DBUserName,'=',''));
    end
    IdxFound = cellStrfind(DBConfigData,'password');
    if ~isempty(IdxFound)
        [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
        DBPassword = char(strrep(DBPassword,'=',''));
    end
    IdxFound = cellStrfind(DBConfigData,'useserver');
    if ~isempty(IdxFound)
        [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
        IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
    end
    IdxFound = cellStrfind(DBConfigData,'serverip');
    if ~isempty(IdxFound)
        [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
        DBServerIP = char(strrep(DBServerIP,'=',''));
    end
    disp(['Using the database ',DBName]);
catch
    disp('Error occured in reading the DatabaseConfiguration.txt, hence using the default configuration!');
end

