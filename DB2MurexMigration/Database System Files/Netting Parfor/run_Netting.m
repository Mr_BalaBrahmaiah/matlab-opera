
try
    InBUName = 'cfs';
    BuyTradeId = {};
    BuyLotsToBeNetted={};
    SellTradeId={};
    SellLotsToBeNetted={};
    UserName = 'system';
    
    %% Load MAT File
    
    %     load('Netting_Model_Input.mat') ;
    
    %     BuyTradeId = BuyTradeId(1:5);
    %     BuyLotsToBeNetted = BuyLotsToBeNetted(1:5);
    %     SellTradeId = SellTradeId(1:5);
    %     SellLotsToBeNetted = SellLotsToBeNetted(1:5);
    
    %% Read Excel Sheet
    [Input_FileName,File_PathName]  = uigetfile('*.xlsx','Select Netting Input File');
    Input_FileName = [File_PathName filesep Input_FileName] ;
    
    [~,~,Excel_Data] = xlsread(Input_FileName);
    
    Header = Excel_Data(1,:);
    RawData = Excel_Data(2:end,:);
    
    BuyTradeId_Col = cellStrfind_exact(Header,{'buy_trade_id'});
    BuyLotsToBeNetted_Col = cellStrfind_exact(Header,{'buy_lots_to_be_netted'});
    SellTradeId_Col = cellStrfind_exact(Header,{'sell_trade_id'});
    SellLotsToBeNetted_Col = cellStrfind_exact(Header,{'sell_lots_to_be_netted'});
    
    BuyTradeId = RawData(:,BuyTradeId_Col);
    BuyLotsToBeNetted = RawData(:,BuyLotsToBeNetted_Col);
    SellTradeId = RawData(:,SellTradeId_Col);
    SellLotsToBeNetted = RawData(:,SellLotsToBeNetted_Col);
    
    BuyTradeId = BuyTradeId(1:10);
    BuyLotsToBeNetted = BuyLotsToBeNetted(1:10);
    SellTradeId = SellTradeId(1:10);
    SellLotsToBeNetted = SellLotsToBeNetted(1:10);
    
    %% Netting without parfor
    
    tic;
    [OutErrorMessage,NC_PnL,USD_PnL,Overall_Parfor_Errormsg] = calculate_netting(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName);
    msgbox(['Without Parfor Calculate Netting : ', num2str(toc) ]);
    delete(gcp('nocreate'));
    
    tic;
    [OutErrorMessage,Overall_Parfor_Errormsg] = cancel_netting(InBUName,{'NET-2425'},'system');
    msgbox(['Without Parfor Cancel Netting : ', num2str(toc) ]);
    
    delete(gcp('nocreate'));
    
    %% Netting with parfor
    
    %     tic;
    %     [OutErrorMessage,NewNettingId,PnL] = calculate_netting_parfor(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName);
    %     msgbox(['With Parfor Calculate Netting : ', num2str(toc) ]);
    %
    %     tic;
    %     cancel_netting_parfor(InBUName,{NewNettingId},'system')
    %     msgbox(['With Parfor Cancel Netting : ', num2str(toc) ]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end



