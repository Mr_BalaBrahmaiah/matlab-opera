function [OutErrorMessage,PnL] = calculate_netting_without_parfor(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName)

OutErrorMessage = {'No errors'};
PnL = '0';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %% validations before netting
    if ~isnumeric(BuyLotsToBeNetted)
        BuyLotsToBeNetted = cell2mat(BuyLotsToBeNetted);
    end
    if ~isnumeric(SellLotsToBeNetted)
        SellLotsToBeNetted = cell2mat(SellLotsToBeNetted);
    end
    
    TradeId = [BuyTradeId;SellTradeId];
    LotsToBeNetted = [BuyLotsToBeNetted;SellLotsToBeNetted];
    
    if ~isnumericequal(abs(sum(BuyLotsToBeNetted)),abs(sum(SellLotsToBeNetted)))
        OutErrorMessage = {'Error! BuyLots and SellLots must be equal!'};
        return;
    end
    
    ObjDB = connect_to_database;
    DealTicketTableName = ['deal_ticket_table_',InBUName];
    ClonedDealTicketTableName = ['cloned_deal_ticket_table_',InBUName];
    
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    DBSettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    IdxRemove = [];
    Subportfolio = cell(size(TradeId,1),1);
    SecurityId = cell(size(TradeId,1),1);
    CounterpartyParent = cell(size(TradeId,1),1);
    
    
    for iT = 1:length(TradeId)
        try
            SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iT},''''];
            MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
        catch
            IdxRemove = [IdxRemove , iT];
            continue;
        end
        try
            SqlQuery = ['select version_comments,subportfolio,security_id,counterparty_parent from ',DealTicketTableName,' where trade_id = ''',TradeId{iT},''' and version_no = ',num2str(MaxVersion)];
            DBData = fetch(ObjDB,SqlQuery);
        catch
            IdxRemove = [IdxRemove , iT];
            continue;
        end
        DBComments       = DBData{1};
        Subportfolio{iT} = DBData{2};
        SecurityId{iT}   = DBData{3};
        CounterpartyParent{iT} = DBData{4};
        
        if strcmpi(DBComments,'fully_netted')
            IdxRemove = [IdxRemove , iT];
        end
    end
    
    if ~isempty(IdxRemove)
        TradeId(IdxRemove) = [];
        SecurityId(IdxRemove) = [];
        CounterpartyParent(IdxRemove) = [];
        LotsToBeNetted(IdxRemove) = [];
        Subportfolio(IdxRemove) = [];
    end
    
    if isempty(TradeId)
        OutErrorMessage = {'Error! No data selected for netting!'};
        return;
    end
    
    DistinctCP = unique(upper(CounterpartyParent));
    if length(DistinctCP)~= 1
        OutErrorMessage = {'Error! Counterparty names selected for netting should be the same!'};
        return;
    end
    
    DistinctPF = unique(upper(Subportfolio));
    if length(DistinctPF)~= 1
        OutErrorMessage = {'Error! Portfolios selected for netting should be the same!'};
        return;
    end
    
    ContractMonth = cell(size(SecurityId));
    DerivativeType = cell(size(SecurityId));
    ProductCode = cell(size(SecurityId));
    GroupType = cell(size(SecurityId));
    
    SqlQuery = 'select derivative_type,group_type from derivative_type_table';
    GroupTypeData = fetch(ObjDB,SqlQuery);
    
    Tablename = ['security_info_table_',InBUName];
    for iSec = 1:length(SecurityId)
        SqlQuery = ['select contract_month,derivative_type,product_code from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        SecInfoNetData = fetch(ObjDB,SqlQuery);
        ContractMonth(iSec)  = SecInfoNetData(1);
        DerivativeType(iSec) = SecInfoNetData(2);
        ProductCode(iSec)    = SecInfoNetData(3);
        IdxGroupType         = strcmpi(SecInfoNetData{2},GroupTypeData(:,1));
        GroupType(iSec)      = GroupTypeData(IdxGroupType,2);
        %         SqlQuery = ['select contract_month from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         ContractMonth(iSec) = fetch(ObjDB,SqlQuery);
        %         SqlQuery = ['select derivative_type from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         DerivativeType(iSec) = fetch(ObjDB,SqlQuery);
        %         SqlQuery = ['select product_code from ',Tablename,' where security_id = ''',SecurityId{iSec},''''];
        %         ProductCode(iSec) = fetch(ObjDB,SqlQuery);
    end
    
    IdxFX = cellfun(@strncmpi,DerivativeType,repmat({'fx_forward'},size(DerivativeType)),repmat({length('fx_forward')},size(DerivativeType)));
    
    if ~any(IdxFX) % FX types, no need to check the contract month as its not valid
        if any(cellfun(@isempty,ContractMonth))
            OutErrorMessage = {'Error! ContractMonth selected for netting cannot be empty!'};
            return;
        end
        
        DistinctCM = unique(upper(ContractMonth));
        if length(DistinctCM)~= 1
            OutErrorMessage = {'Error! ContractMonths selected for netting should be the same!'};
            return;
        end
    end
    
    DistinctOptType = unique(upper(DerivativeType));
    if length(DistinctOptType)~= 1
        OutErrorMessage = {'Error! DerivativeType selected for netting should be the same!'};
        return;
    end
    
    DistinctProdCode = unique(upper(ProductCode));
    if length(DistinctProdCode)~= 1
        OutErrorMessage = {'Error! ProductCode selected for netting should be the same!'};
        return;
    end
    
    %% perform netting
    % to get the last netting id
    SqlQuery = ['select distinct(netting_id) from ',ClonedDealTicketTableName];
    DistinctNettingId = fetch(ObjDB,SqlQuery);
    % NettingId = strrep(DistinctNettingId,'NET-','');
    [NettingId,TokContract] = strtok(DistinctNettingId,'NET-');
    MaxNettingId = max(str2num(char(NettingId)));
    
    if isempty(MaxNettingId)
        MaxNettingId = 0;
    end
    TempNettingId = MaxNettingId + 1;
    
    for iTR = 1:length(TradeId)
        SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
        MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
        %       SqlQuery = ['select * from deal_ticket_table where trade_id = ''',TradeId{iTR},''' and version_no = ',num2str(MaxVersion)];
        %       [DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery,InBUName);
        %       DealTicketData = cell2dataset([DBFieldNames;DBData]);
        SqlQuery = ['select * from ',DealTicketTableName,' where trade_id = ''',TradeId{iTR},''' and version_no = ',num2str(MaxVersion)];
        [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
        DealTicketData = cell2table(DBData);
        DealTicketData.Properties.VariableNames = DBFieldNames;
        DealTicketTableFields = DBFieldNames;
        ContraDealTicketData = DealTicketData;
        
        %       SqlQuery = ['select * from cloned_deal_ticket_table where trade_id = ''',TradeId{iTR},''''];
        %       [DBFieldNames,DBData] = read_from_database('cloned_deal_ticket_table',0,SqlQuery,InBUName);
        %       ClonedDealTicketData = cell2dataset([DBFieldNames;DBData]);
        SqlQuery = ['select * from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
        [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
        ClonedDealTicketData = cell2table(DBData);
        ClonedDealTicketData.Properties.VariableNames = DBFieldNames;
        %         ClonedDealTicketTableFields = DBFieldNames;
        
        if ClonedDealTicketData.original_lots < 0
            OriginalLotsSign = -1;
        else
            OriginalLotsSign = 1;
        end
        TempOrigLots = abs(ClonedDealTicketData.original_lots);
        
        try
            NettedLots = abs(cell2mat(LotsToBeNetted(iTR)));
        catch
            NettedLots = abs(LotsToBeNetted(iTR));
        end
        if ClonedDealTicketData.active_lots < 0
            ActiveLotSign = -1;
        else
            ActiveLotSign = 1;
        end
        TempActiveLots = abs(ClonedDealTicketData.active_lots);
        
        NewNettingId = ['NET-',num2str(TempNettingId)];
        
        if isnumericequal(TempOrigLots,NettedLots)
            NettingStatus  = 'fully_netted';
        else
            NettingStatus  = 'partially_netted';
        end
        
        if strcmpi(ClonedDealTicketData.netting_status,'partially_netted')
            ActiveLots = max(TempActiveLots,NettedLots) - min(TempActiveLots,NettedLots);
        else
            ActiveLots = TempOrigLots - NettedLots;
        end
        
        % multiply the calculated active lots value with market action sign
        % before updating Table
        ActiveLots = ActiveLots * ActiveLotSign;
        
        DealTicketData.version_no = MaxVersion+1;
        DealTicketData.version_created_by = UserName;
        DealTicketData.version_timestamp = getCurrentDateTimestamp;
        % if its partially netted, the netted lots to be counted based on the
        % previous netted lots as well(which is saved in activelots earlier)
        if strcmpi(NettingStatus,'partially_netted')
            DealTicketData.is_netting_done = 0;
            DealTicketData.lots_to_be_netted = ActiveLots;
            DealTicketData.original_lots = ActiveLots;
            DealTicketData.version_comments = [NettingStatus,' ',num2str(NettedLots),' lots'];
        else % if its fully_netted
            DealTicketData.is_netting_done = 1;
            DealTicketData.lots_to_be_netted = ActiveLots;
            DealTicketData.version_comments = NettingStatus;
            DealTicketData.deal_status = 'dead';
        end
        
        
        %       DealTicketData = dataset2cell(DealTicketData);
        DealTicketData = table2cell(DealTicketData);
        fastinsert(ObjDB,DealTicketTableName,DealTicketTableFields,DealTicketData);
        
        % Create an internal id(Netting_id-Trade_id) in cloned deal_ticket_table with original
        % lots = netted lots and active lots = 0;
        NetContraID = [NewNettingId,'-',TradeId{iTR}];
        ContraDealTicketData.trade_id = NetContraID;
        ContraDealTicketData.parent_trade_id = NetContraID;
        ContraDealTicketData.transaction_date = DBSettleDate;
        ContraDealTicketData.version_no = 0;
        ContraDealTicketData.version_created_by = UserName;
        ContraDealTicketData.version_timestamp = getCurrentDateTimestamp;
        ContraDealTicketData.version_comments = 'fully_netted';
        ContraDealTicketData.is_netting_done = 1;
        ContraDealTicketData.lots_to_be_netted = 0;
        
        
        if strcmpi(NettingStatus,'partially_netted')
            ContraDealTicketData.original_lots = OriginalLotsSign * NettedLots;
        else
            ContraDealTicketData.original_lots = ActiveLots;
            ContraDealTicketData.deal_status = 'dead';
        end
        
        %       ContraDealTicketData = dataset2cell(ContraDealTicketData);
        ContraDealTicketData = table2cell(ContraDealTicketData);
        fastinsert(ObjDB,DealTicketTableName,DealTicketTableFields,ContraDealTicketData);
        
        % update the netting info and active_lots for the contra-trade after
        % netting
        NettedLots = NettedLots * OriginalLotsSign;
        update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
            {NettedLots,'fully_netted',NettedLots,NewNettingId,char(DBValueDate)},{['where trade_id = ''',NetContraID,'''']});
        
        Tablename = ['deal_active_lots_table_',InBUName];
        update(ObjDB,Tablename,{'active_lots'},{NettedLots},...
            {['where value_date = ''',char(DBValueDate),''' and trade_id = ''', NetContraID,'''']});
        
        % get the current year premium to update in the table for contra id
        FinYear = fetch(ObjDB,'select financial_year from valuation_financial_year_view');
        Tablename = ['premium_fy_table_',InBUName];
        SqlQuery = ['select premium from ',Tablename,' where trade_id = ''',TradeId{iTR},''' and financial_year = ''',char(FinYear),''''];
        CurrentPremium = cell2mat(fetch(ObjDB,SqlQuery));
        update(ObjDB,Tablename,{'premium'},{CurrentPremium},...
            {['where financial_year = ''',char(FinYear),''' and trade_id = ''', NetContraID,'''']});
        
        % update the netting info and active_lots info for actual trade
        update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
            {ActiveLots,NettingStatus,NettedLots,NewNettingId,char(DBValueDate)},{['where trade_id = ''',TradeId{iTR},'''']});
        
        Tablename = ['deal_active_lots_table_',InBUName];
        update(ObjDB,Tablename,{'active_lots'},{ActiveLots},...
            {['where value_date = ''',char(DBValueDate),''' and trade_id = ''', TradeId{iTR},'''']});
        
        Tablename = ['netting_id_table_',InBUName];
        ColNames = {'netting_date','netting_id','trade_id','security_id','group_type','subportfolio','product_code','counterparty','contract_month','derivative_type'};
        Data = {DBValueDate,NewNettingId,TradeId{iTR},SecurityId{iTR},GroupType{iTR},Subportfolio{iTR},ProductCode{iTR},CounterpartyParent{iTR},ContractMonth{iTR},DerivativeType{iTR}};
        fastinsert(ObjDB,Tablename,ColNames,Data);
    end
    
    %% calcualte the netting_pnl value
    %     try
    %         Viewname = ['helper_netting_pnl_view_',InBUName];
    %         SqlQuery = ['select sum(original_premium_paid) from ',Viewname,' where netting_id = ''',NewNettingId,''''];
    %         format bank;
    %         PnL = cell2mat(fetch(ObjDB,SqlQuery));
    %         format short;
    %     catch
    %     end
    
    [OutErrorMessage,PnL] = calculate_netting_pnl(InBUName,BuyTradeId,BuyLotsToBeNetted,SellTradeId,SellLotsToBeNetted,UserName);
    
catch ME
    OutErrorMessage = {ME.message};
end

