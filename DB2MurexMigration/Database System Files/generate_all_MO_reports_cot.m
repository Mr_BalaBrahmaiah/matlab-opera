function [OutErrorMsg,Summary_Filename,Monthwise_Filename] = generate_all_MO_reports_cot(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

%% Summary Report

[OutErrorMsg1,Summary_Filename] = generate_mo_summary_report(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    Summary_Filename = char(OutErrorMsg1);
end

cprintf('key','%s finished\n', 'Summary Report');

%% Monthwise Summary Report

[OutErrorMsg2,Monthwise_Filename] = generate_mo_summary_monthwise_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    Monthwise_Filename = char(OutErrorMsg2);
end

cprintf('key','%s finished\n', 'Monthwise Summary Report');

%% Overall ErrorMsg

Overall_ErrorMsg = [OutErrorMsg1;OutErrorMsg2] ;

end

