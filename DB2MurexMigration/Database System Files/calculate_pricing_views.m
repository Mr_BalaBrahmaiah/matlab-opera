function OutData = calculate_pricing_views(ColNames,InData,InBUName)

DSData = cell2dataset([ColNames;InData]);

%% calculations
OutData.delta_1    =  DSData.delta_1 .* DSData.active_lots;
OutData.delta_2    =  DSData.delta_2 .* DSData.active_lots;

OutData.gamma_11   =  DSData.gamma_11 .* DSData.active_lots .* DSData.nc_factor .* DSData.fx_nc_factor .* DSData.future_price .* 0.01;
OutData.gamma_12   =  DSData.gamma_12 .* DSData.active_lots .* DSData.nc_factor .* DSData.fx_nc_factor .* DSData.future_price .* 0.01;
OutData.gamma_21   =  DSData.gamma_21 .* DSData.active_lots .* DSData.nc_factor .* DSData.fx_nc_factor .* DSData.future_price .* 0.01;
OutData.gamma_22   =  DSData.gamma_22 .* DSData.active_lots .* DSData.nc_factor .* DSData.fx_nc_factor .* DSData.future_price .* 0.01;
% OutData.gamma_11   = (DSData.gamma_11 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
% OutData.gamma_12   = (DSData.gamma_12 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
% OutData.gamma_21  = (DSData.gamma_21 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
% OutData.gamma_22   = (DSData.gamma_22 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;

OutData.vega_1     =  DSData.vega_1 .* DSData.active_lots .* DSData.mult_factor .* DSData.nc_factor;
OutData.vega_2     =  DSData.vega_2 .* DSData.active_lots .* DSData.mult_factor .* DSData.nc_factor;

OutData.theta      =  DSData.theta .* DSData.active_lots .* DSData.mult_factor .* DSData.nc_factor;

OutData.gamma_11_mt   = (DSData.gamma_11 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
OutData.gamma_12_mt   = (DSData.gamma_12 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
OutData.gamma_21_mt  = (DSData.gamma_21 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;
OutData.gamma_22_mt   = (DSData.gamma_22 .* DSData.active_lots .* DSData.lot_mult1) ./ DSData.curr_mult1;

temp_original_premium_paid = DSData.original_lots .* DSData.original_premium;

%temp_premium_paid          = DSData.original_lots .* DSData.current_premium;
temp_mkt_value             = DSData.active_lots .* DSData.price;

%%% Old Code
% mtm_nc                     = (temp_mkt_value - temp_premium_paid) .* DSData.mult_factor;
% cumulative_mtm_nc          = (temp_mkt_value - temp_original_premium_paid) .* DSData.mult_factor;

%%% newly added logic (03-05-2019)
temp_cumm_nc = DSData.active_lots .* DSData.original_premium;
temp_mtm_nc = DSData.active_lots .* DSData.current_premium;

mtm_nc                     = (temp_mkt_value - temp_mtm_nc) .* DSData.mult_factor;
mtm_usd                    = mtm_nc .* DSData.nc_factor .* DSData.fx_nc_factor;
cumulative_mtm_nc          = (temp_mkt_value - temp_cumm_nc) .* DSData.mult_factor;
cumulative_mtm_usd         = cumulative_mtm_nc .* DSData.nc_factor .* DSData.fx_nc_factor;

premium_paid     = temp_original_premium_paid .* DSData.mult_factor .* -1;
premium_paid_usd = premium_paid .* DSData.nc_factor .* DSData.fx_nc_factor;
mkt_value        = temp_mkt_value .* DSData.mult_factor;
mkt_value_usd    = mkt_value .* DSData.nc_factor .* DSData.fx_nc_factor;

realised_pnl       = zeros(size(mtm_nc));
realised_pnl_usd   = zeros(size(mtm_nc));
unrealised_pnl     = zeros(size(mtm_nc));
unrealised_pnl_usd = zeros(size(mtm_nc));

IdxDead = strcmpi('dead',DSData.deal_status) | (DSData.is_premium_paid ==1);
IdxLive = strcmpi('live',DSData.deal_status) & (DSData.is_premium_paid ~=1);


realised_pnl(IdxDead) = premium_paid(IdxDead);
unrealised_pnl(IdxDead) = mkt_value(IdxDead);
realised_pnl_usd(IdxDead) = premium_paid_usd(IdxDead);
unrealised_pnl_usd(IdxDead) = mkt_value_usd(IdxDead);

%%% Newly added code(09-05-2019)
if strcmpi(InBUName,'cfs')
    temp_netted_premium_paid = DSData.netted_lots .* DSData.original_premium;
    premium_paid_pn     = temp_netted_premium_paid .* DSData.mult_factor .* -1;
    premium_paid_pn_usd = premium_paid_pn .* DSData.nc_factor .* DSData.fx_nc_factor;
    Idxpartially_netted = strcmpi('partially_netted',DSData.netting_status) & (DSData.is_premium_paid ~=1);    %%%% Newly added Code
    realised_pnl(Idxpartially_netted) = premium_paid_pn(Idxpartially_netted);
    realised_pnl_usd(Idxpartially_netted) = premium_paid_pn_usd(Idxpartially_netted);
end
%%% End Code

unrealised_pnl(IdxLive) = cumulative_mtm_nc(IdxLive);
unrealised_pnl_usd(IdxLive) = cumulative_mtm_usd(IdxLive);

OutData.mtm_nc = mtm_nc;
OutData.mtm_usd = mtm_usd;
OutData.cumulative_mtm_nc = cumulative_mtm_nc;
OutData.cumulative_mtm_usd = cumulative_mtm_usd;

OutData.premium_paid = premium_paid;
OutData.premium_paid_usd = premium_paid_usd;
OutData.mkt_value = mkt_value;
OutData.mkt_value_usd = mkt_value_usd;

OutData.realised_pnl = realised_pnl;
OutData.unrealised_pnl = unrealised_pnl;
OutData.realised_pnl_usd = realised_pnl_usd;
OutData.unrealised_pnl_usd = unrealised_pnl_usd;

