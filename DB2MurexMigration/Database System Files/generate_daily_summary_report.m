function [OutErrorMsg,OutFilename] = generate_daily_summary_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    Filename = ['Daily_Summary_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        ObjDB = connect_to_database;
        InData = cell2dataset([ColNames;Data]);
        
        IdxBlankContractMonth = strcmpi('',InData.contract_month);
        InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(InBUName,InData,1);
        
        %%
        if strcmpi(InBUName,'cfs')
            UniqueFields = {'PORTFOLIO','ASSET_CLASS','GRP','UNDERLYING','CONTRACT_MONTH','CURRENCY'};
        else
            UniqueFields = {'PORTFOLIO','ASSET_CLASS','UNDERLYING','CONTRACT_MONTH','CURRENCY'};
        end
        
        SumFields = {'Total_USD','0','Total_USD','Delta','ActiveLots','M2M_USD'};
        OutputFields = [UniqueFields,SumFields,'SettlePrice','mult_factor','nc_factor','fx_nc_factor'];
        WeightedAverageFields = [];
        [OutputFields,ConsDeals] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;ConsDeals]);
        
        TableName = 'currency_spot_mapping_table';
        [~,Data] = read_from_database(TableName,0);
        DBCurrency = Data(:,1);
        DBSpot = Data(:,2);
        
        ValueDate = fetch(ObjDB, 'select value_date from valuation_date_table');
        
        SpotRate = cell(size(TempOutData.CURRENCY,1),1);
        for iCurr = 1:length(DBCurrency)
            IdxCurr = strcmpi(DBCurrency{iCurr},TempOutData.CURRENCY);
            SqlQuery = ['select settle_value from underlying_settle_value_table where value_date  = ''',char(ValueDate),''' and underlying_id = ''',DBSpot{iCurr},''''];
            SpotRate(IdxCurr) = fetch(ObjDB,SqlQuery);
        end
        
        TempOutData.SpotRate = cell2mat(SpotRate);
%         TempOutData.MktExpUSD = TempOutData.SettlePrice .* TempOutData.ActiveLots .* TempOutData.mult_factor...
%             .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        TempOutData.CURRENCY = [];
        TempOutData.ActiveLots = [];
        TempOutData.SettlePrice = [];
        TempOutData.mult_factor = [];
        TempOutData.nc_factor = [];
        TempOutData.fx_nc_factor = [];
        
        MappingData = fetch(ObjDB, 'SELECT distinct(instrument),option_instrument,contract_size FROM dbtodw_mapping');                      
        MappingData = cell2table(MappingData);
        MappingData.Properties.VariableNames = {'instrument','option_instrument','contract_size'};
        
        NotionalDelta = cell(size(TempOutData.UNDERLYING,1),1);
        for iInst = 1:length(MappingData.instrument)
            IdxFound = strcmpi(MappingData.instrument(iInst),TempOutData.UNDERLYING) | strcmpi(MappingData.option_instrument(iInst),TempOutData.UNDERLYING);
            NotionalDelta(IdxFound) = num2cell(TempOutData.Delta(IdxFound) .* MappingData.contract_size(iInst));
        end
        TempOutData.NotionalDelta = NotionalDelta;
        TempOutData = dataset2cell(TempOutData);
        
        %%
        
        if strcmpi(InBUName,'cfs')
            OutHeader = {'Portfolio','Product Type','Group_Type','Underlying','Maturity Label','Book_Value','Brokerage','Net_PL','Delta','MKT_EXP_USD','CurConvert','NotionalDelta'};
        else
            OutHeader = {'Portfolio','Product Type','Underlying','Maturity Label','Book_Value','Brokerage','Net_PL','Delta','MKT_EXP_USD','CurConvert','NotionalDelta'};  %% No need 'Index_Currency'
        end
        
        OutData = TempOutData(2:end,:);
        
        xlswrite(OutFilename,[OutHeader;OutData]);
        
            %%% Newly added code
            settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
            settle_date = repmat(settlement_date,size(OutData,1),1);
            %%%% Upload the Final Data
            if strcmpi(InBUName,'coc')
            try
                Table_Name = strcat('daily_summary_report_store_table_',InBUName);
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');

                upload_in_database(Table_Name,[settle_date,OutData],ObjDB);
                disp('Upload Done Successfully to Database Table');           
            catch
                OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
            end
            end
            %%% end code
            
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end