function refresh_and_save_settle_pricing_view

ObjDB = connect_to_database;

SqlQuery = 'truncate TABLE helper_4_1_settle_pricing_subportfoliovalues_view_cache_table';
Curs = exec(ObjDB,SqlQuery);

SqlQuery = 'insert into helper_4_1_settle_pricing_subportfoliovalues_view_cache_table SELECT * FROM helper_4_1_settle_pricing_subportfoliovalues_view';
Curs = exec(ObjDB,SqlQuery);
