function save_accounting_tr_pnl_report

Filename = generate_accounting_tr_pnl_report(InBUName);

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

try
    configure_mail_settings;    
    sendmail({'Raghavendra.Sn@olamnet.com'}, 'Accounting Pnl Report', ['Attached is the Accounting Pnl Report for COB ''',SettlementDate,''''],{Filename});    
catch
end

try
    DestinationFolder = '\\10.190.7.222\Share\DBSystem_Reports\Accounting_PnL_Report';
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Accounting Pnl Report');
catch
end