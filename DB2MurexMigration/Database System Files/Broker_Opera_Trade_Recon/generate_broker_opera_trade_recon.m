function [OutErrorMsg,OutXLSName] = generate_broker_opera_trade_recon(InBUName,Temp_Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: Broker RMO Trade Daily Recon or  Broker Opera Trade Daily Recon
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 09 Apr 2019 / 14:55:55

%%% [OutErrorMsg,OutXLSName] = generate_broker_opera_trade_recon('cfs','2019-11-20')

try
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['broker_opera_trade_recon_',lower(char(InBUName))]);
    
    if iscellstr(InBUName)
       InBUName = char(InBUName);
    end

    % Connect to Database
    ObjDB = connect_to_database;
    
    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = Temp_Settle_Date;
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end
    
    Today_date = fetch(ObjDB,'select settlement_date from valuation_date_table'); %% added on 24-12-2019
    
   %%% Fetch data from broker confirmed deal table
    Confirmed_deal_Table = ['broker_confirmed_deal_table_',char(InBUName)];
    SqlQuery_Confirmed_deal_Table = ['select * from ',char(Confirmed_deal_Table),' where st_date = ''',char(Settele_date),''' '] ;
    [Confirmed_deal_Table_Cols,Confirmed_deal_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Confirmed_deal_Table);

    if strcmpi(Confirmed_deal_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Confirmed_deal_Table)]} ;
        return;
    end
    
    %%% Newly added logic on 17-09-2019
    %%% consolidation part for Broker Recon Data
    UniqueFields = {'trade_date','account','broker_name','trade_type','product','terminal_month','strike','rate'}; 
    SumFields = {'bought_lots','sold_lots'};
    OutputFields = Confirmed_deal_Table_Cols ;
    WeightedAverageFields = [];
    [~,Confirmed_deal_Table_Data] = consolidatedata(Confirmed_deal_Table_Cols, Confirmed_deal_Table_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    %%% end
    Broker_Confirmed_deal_Tbl = cell2table(Confirmed_deal_Table_Data,'VariableNames',Confirmed_deal_Table_Cols);  %% Convert to table
    
    %%% Newly added logic on 29-11-2019
    %%% if bought_lots != 0 & sold_lots != 0, split signle trade in to two
    %%% trades, one is bought_lots = 0 and another is sold_lots = 0
    Index = find(~(eq(Broker_Confirmed_deal_Tbl.bought_lots,0) | eq(Broker_Confirmed_deal_Tbl.sold_lots,0)));
    [r,~] = size(Index); Final_Data = [];
    for i = 1:1:r
        Data1 = Broker_Confirmed_deal_Tbl(Index(i),:);
        Data2 = Broker_Confirmed_deal_Tbl(Index(i),:);
        Data1.bought_lots = 0;
        Data2.sold_lots = 0;
        Final_Data = [Final_Data ; [Data1 ; Data2]];
    end
    Broker_Confirmed_deal_Tbl(Index,:) = [];
    Broker_Confirmed_deal_Tbl = [Broker_Confirmed_deal_Tbl ; Final_Data];
    %%% End Code
    
    %%% Find the buy_sell using lots sign
    Broker_Confirmed_deal_Tbl.broker_lots = Broker_Confirmed_deal_Tbl.bought_lots + (Broker_Confirmed_deal_Tbl.sold_lots .* -1);
    Index_Bsign = eq(sign(Broker_Confirmed_deal_Tbl.broker_lots),1);
    Broker_Buy_Sell(Index_Bsign,1) = {'BUY'};
    Broker_Buy_Sell(~Index_Bsign,1) = {'SELL'};
    Broker_Confirmed_deal_Tbl.Broker_Buy_Sell = Broker_Buy_Sell;
    
    %%% Remove the unwanted column's data
    Broker_Confirmed_deal_Tbl.deal_id = [];
    Broker_Confirmed_deal_Tbl.desk = [];
    Broker_Confirmed_deal_Tbl.st_date = [];
    Broker_Confirmed_deal_Tbl.bought_lots = [];
    Broker_Confirmed_deal_Tbl.sold_lots = [];
    
    %%% Fetch data from RMO trade query table (using generate_trade_query_report M-file)
    %%% Fetch data for current date - helper_risk_middle_office_reports view
    %%% Fetach data for past pate - rmo_trade_query_report_table table
    if datenum(Settele_date,'yyyy-mm-dd') == datenum(Today_date,'yyyy-mm-dd')  %% added on 24-12-2019
        ViewName = 'helper_risk_middle_office_reports';
        if (strcmpi(InBUName,'cfs') || strcmpi(InBUName,'usg'))
            SqlQuery = ['SELECT * FROM helper_risk_middle_office_reports where transaction_date = ''', Settele_date ,''' and settlement_source <> ''otc'' and market_action in (''bought'',''sold'') and trade_id not like ''NET%'''];
        else
            SqlQuery = ['SELECT * FROM helper_risk_middle_office_reports where transaction_date = ''', Settele_date ,''' and market_action in (''bought'',''sold'') and trade_id not like ''NET%'''];
        end
        [SettleColNames,SettleData] = read_from_database(ViewName,0,SqlQuery,InBUName);
        
        if ~strcmpi(SettleData,'No Data')
            SettleData = cell2dataset([SettleColNames;SettleData]);        

            % remove the internal deals
            if strcmpi(InBUName,'cfs')
                IdxRemove = strncmpi(SettleData.counterparty_parent,'MM',2) | ...
                    strncmpi(SettleData.counterparty_parent,'RMS',3);
                SettleData(IdxRemove,:) = [];

                IdxOR = strncmpi('OR',SettleData.product_code,2);
                SettleData.product_code(IdxOR) = {'OR'};
            end

            %%%% get OriginalLots and derivative_type
            NumRows = size(SettleData,1);
            CALLPUT = cell(NumRows,1);
            ENTITY = repmat(cellstr(InBUName),NumRows,1);
            IdxCall = strcmpi('vanilla_call',SettleData.derivative_type);
            CALLPUT(IdxCall) = {'CALL'};
            IdxPut = strcmpi('vanilla_put',SettleData.derivative_type);
            CALLPUT(IdxPut) = {'PUT'};
            %%% newly added logic OFUT --> CALL/PUT (2019-09-17)
            IdxFUT = strcmpi('FUT',SettleData.group_type);
            SettleData.group_type(IdxFUT) = {'FUT'};
            SettleData.group_type(IdxCall) = {'CALL'};
            SettleData.group_type(IdxPut) = {'PUT'};
            %%% newly added logic MARKET_ACTION --> BUY/SELL (2019-11-13)
            Idx_market_action = strcmpi('bought',SettleData.market_action);
            SettleData.market_action(Idx_market_action) = {'BUY'};
            SettleData.market_action(~Idx_market_action) = {'SELL'};

            OriginalLots = SettleData.original_lots;

            RMO_trade_query_Table_Cols = lower({'ENTITY','TRADE_NO','TRADE_DATE','INSTRUMENT','EXCHANGE','PRODUCT','COUNTERPARTY','TRADE_TYPE','OPT_TYPE','STRIKE','MONTH','LOTS','RATE','MARKET_ACTION'});
            RMO_trade_query_Table_Data = [ENTITY,SettleData.trade_id,SettleData.transaction_date,SettleData.instrument,SettleData.exchange_name,SettleData.product_name,...
                SettleData.counterparty_parent,SettleData.group_type,CALLPUT,num2cell(SettleData.strike),SettleData.contract_month,num2cell(OriginalLots),num2cell(SettleData.original_premium_without_yield),SettleData.market_action];
        else
            OutErrorMsg = {'No RMO trades found for the trade date!'};
            return;
        end
    
        RMO_trade_query_Tbl = cell2table(RMO_trade_query_Table_Data,'VariableNames',RMO_trade_query_Table_Cols);  %% Convert to table
    
    elseif datenum(Settele_date,'yyyy-mm-dd') < datenum(Today_date,'yyyy-mm-dd') %% added on 24-12-2019
        %%% To get the past dated reports
        Table_name = 'rmo_trade_query_report_table';
        SqlQuery_past = ['select * from ',char(Table_name),' where trade_date = ''',char(Settele_date),''' and ENTITY = ''',char(InBUName),''''];
        [Table_Cols,Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_past);
        
        if strcmpi(Table_Data,'No Data')
            OutErrorMsg = {'No RMO trades found for the trade date!'};
            return;
        end
        
        RMO_trade_query_Tbl = cell2table(Table_Data,'VariableNames',Table_Cols);  %% Convert to table
        
        IdxCall = strcmpi('CALL',RMO_trade_query_Tbl.opt_type);
        IdxPut = strcmpi('PUT',RMO_trade_query_Tbl.opt_type);
        %%% OFUT --> CALL/PUT
        IdxFUT = strcmpi('FUT',RMO_trade_query_Tbl.trade_type);
        RMO_trade_query_Tbl.trade_type(IdxFUT) = {'FUT'};
        RMO_trade_query_Tbl.trade_type(IdxCall) = {'CALL'};
        RMO_trade_query_Tbl.trade_type(IdxPut) = {'PUT'};
        %%% MARKET_ACTION --> BUY/SELL
        Index_sign = eq(sign(RMO_trade_query_Tbl.lots),+1);
        mark_action(Index_sign,1) = {'BUY'};
        mark_action(~Index_sign,1) = {'SELL'};
        RMO_trade_query_Tbl.market_action = mark_action;
        RMO_trade_query_Tbl.market_action = mark_action;
    end
    %%% End Code
    
    %%% Remove the unwanted column's data
    RMO_trade_query_Tbl.entity =[];
    RMO_trade_query_Tbl.trade_no = [];
           
    %%% Fetch data from broker product master table
    BProduct_master_Table = ['broker_product_master_table_',char(InBUName)];
    SqlQuery_BProduct_master_Table = ['select * from ',char(BProduct_master_Table)];
    [BProduct_master_Table_Cols,BProduct_master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BProduct_master_Table);

    if strcmpi(BProduct_master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BProduct_master_Table)]} ;
        return;
    end
    
    Broker_Product_master_Tbl = cell2table(BProduct_master_Table_Data,'VariableNames',BProduct_master_Table_Cols);  %% Convert to table
    
     %%Fetch data from Year Mapping Table, to change the terminal_month
     Mapping_Tbl_Name = 'year_mapping_table';
     SqlQuery_Mapping_Table = ['select * from ',char(Mapping_Tbl_Name)] ;
     [Mapping_ColNames,Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Mapping_Table);
     Mapping_Tab_Data = cell2dataset([Mapping_ColNames;Mapping_Table_Data]);
        
    %%% OPERA and Master table mapping
    %%% Comparing exchange, product and counterparty, get required data
    [row,~] = size(RMO_trade_query_Tbl);
    Broker_Opera_Master_Data = []; 
    for i = 1:1:row
        single_exchange = RMO_trade_query_Tbl.exchange(i);
        single_product = RMO_trade_query_Tbl.product(i);
        single_counterparty = RMO_trade_query_Tbl.counterparty(i);

        Index_opera = strcmpi(single_exchange,Broker_Product_master_Tbl.exchange_code) & strcmpi(single_product,Broker_Product_master_Tbl.product_map) & ...
                        strcmpi(single_counterparty,Broker_Product_master_Tbl.broker_map);

        Unique_Opera_Master_Data=Broker_Product_master_Tbl(Index_opera,:);
        
        if size(Unique_Opera_Master_Data,1) > 1
            Unique_Opera_Master_Data =  Unique_Opera_Master_Data(1,:); 
        end
        
        if isempty(Unique_Opera_Master_Data) 
            continue;
        end
        User_RMO_trade_query_Data = RMO_trade_query_Tbl(i,:);

        User_RMO_trade_query_Data.account_number = Unique_Opera_Master_Data.account_number;
        User_RMO_trade_query_Data.product = Unique_Opera_Master_Data.product_display;
        User_RMO_trade_query_Data.strike = User_RMO_trade_query_Data.strike ./ Unique_Opera_Master_Data.strike_conversion;
        User_RMO_trade_query_Data.rate =  User_RMO_trade_query_Data.rate ./ Unique_Opera_Master_Data.rate_conversion;

        Broker_Opera_Master_Data = vertcat(Broker_Opera_Master_Data,User_RMO_trade_query_Data);
    end
        
    %%% consolidation part for Broker_Opera_Master Data
    Consolidate_Data_Header =  Broker_Opera_Master_Data.Properties.VariableNames;
    Consolidate_Data = table2cell(Broker_Opera_Master_Data);

    UniqueFields = {'trade_date','instrument','exchange','product','counterparty','trade_type','market_action','strike','month','rate','account_number'}; 
    SumFields = {'lots'};
    OutputFields = Consolidate_Data_Header ;
    WeightedAverageFields = [];
    [Consolidate_Data_Header,Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    %%% After consolidation --> Header and Data
    Broker_Opera_Master_conso = cell2table(Data_Consolidate,'VariableNames',Consolidate_Data_Header);  %% Convert to table
    
    %%% Change FXFUT to FUT
    Index_trade_type = strcmpi(Broker_Opera_Master_conso.trade_type, 'FXFUT');
    Broker_Opera_Master_conso.trade_type(Index_trade_type) = {'FUT'};
    
    Broker_Opera_Master_conso.month = strtok(Broker_Opera_Master_conso.month,'.');
    
    %%% Find the buy_sell using lots sign
    Index_Osign = eq(sign(Broker_Opera_Master_conso.lots),1);
    Opera_Buy_Sell(Index_Osign,1) = {'BUY'};
    Opera_Buy_Sell(~Index_Osign,1) = {'SELL'};
    Broker_Opera_Master_conso.Opera_Buy_Sell = Opera_Buy_Sell;
    
    %%% Remove NaN data from strike col
    Index_nan = isnan(Broker_Opera_Master_conso.strike);
    Broker_Opera_Master_conso.strike(Index_nan) = 0;

    %%% Broker and Master table mapping
    %%% get the exchange_code for Broker_Confirmed deals data
    [r,~] = size(Broker_Confirmed_deal_Tbl);
    Broker_exchange_code = []; Broker_product_display = []; Broker_terminal_month =[];
    for ii = 1:1:r
        single_Baccount = Broker_Confirmed_deal_Tbl.account(ii);
        single_Bbroker_name = Broker_Confirmed_deal_Tbl.broker_name(ii);
        single_Bproduct = Broker_Confirmed_deal_Tbl.product(ii);
        single_Bterminal_month = Broker_Confirmed_deal_Tbl.terminal_month(ii);

        Index = strcmpi(single_Baccount,Broker_Product_master_Tbl.account_number) & strcmpi(single_Bproduct,Broker_Product_master_Tbl.product_display) & ...
                            contains(Broker_Product_master_Tbl.broker_name,single_Bbroker_name,'IgnoreCase',true);

        Opera_Master_Data=Broker_Product_master_Tbl(Index,:);

        if isempty(Opera_Master_Data) 
            Broker_terminal_month = vertcat(Broker_terminal_month,{''}); % Newly added line on 2019-11-19
            Broker_product_display = vertcat(Broker_product_display,{''});
            Broker_exchange_code = vertcat(Broker_exchange_code,{''});
            continue;
        elseif size(Opera_Master_Data,1) > 1
            Opera_Master_Data =  Opera_Master_Data(2,:);
        end

         %%% Chnage teminal_month using year mapping table like(0->20, 1->21 etc.
        month = char(single_Bterminal_month);
        last_digit = month(end);
        Mapping_Tab_digit=Mapping_Tab_Data.single_digit_year;
        Index_year = eq(str2double(last_digit),Mapping_Tab_digit);
        if any(Index_year)
            double_digit_year = Mapping_Tab_Data.double_digit_year(Index_year);
            month = strcat(month(1),num2str(double_digit_year));
        end
        terminal_month = strcat(cellstr(month));  %%% terminal month
        
        product_display = Opera_Master_Data.product_display;  %%% Product
        Opera_exchange_code = Opera_Master_Data.exchange_code;  %%% Exchange code

        Broker_terminal_month = vertcat(Broker_terminal_month,terminal_month);
        Broker_product_display = vertcat(Broker_product_display,product_display);
        Broker_exchange_code = vertcat(Broker_exchange_code,Opera_exchange_code);
    end
    
    Broker_Confirmed_deal_Tbl.terminal_month = Broker_terminal_month; 
    Broker_Confirmed_deal_Tbl.product_display = Broker_product_display;
    Broker_Confirmed_deal_Tbl.exchange_code = Broker_exchange_code;

    %%% Broker and Opera mapping process
    [rr,~] = size(Broker_Confirmed_deal_Tbl);
    Past_Date_Trade_Data = []; UnMatched_Broker_Trade_Opera_Data = []; Matched_Broker_Trade_Opera_Data = [];
    Index_Opera_Not_Match = [];
    for j = 1:1:rr
    single_trade_date = Broker_Confirmed_deal_Tbl.trade_date(j);
        
        if datenum(single_trade_date) < datenum(Settele_date)  %%% Past Date Trade Data
            Source = {'Broker'};
            trade_date = datestr(Broker_Confirmed_deal_Tbl.trade_date(j),'dd-mmm-yy');

            PastDate_Trade_Data = [Source, Broker_Confirmed_deal_Tbl.broker_name(j), Broker_Confirmed_deal_Tbl.account(j), trade_date, ...
                                Broker_Confirmed_deal_Tbl.product_display(j), Broker_Confirmed_deal_Tbl.exchange_code(j), Broker_Confirmed_deal_Tbl.trade_type(j), Broker_Confirmed_deal_Tbl.terminal_month(j), ...
                                Broker_Confirmed_deal_Tbl.strike(j), Broker_Confirmed_deal_Tbl.Broker_Buy_Sell(j), Broker_Confirmed_deal_Tbl.broker_lots(j),Broker_Confirmed_deal_Tbl.rate(j)];

            Past_Date_Trade_Data = vertcat(Past_Date_Trade_Data,PastDate_Trade_Data); 

        else  %%% Matched and Un-Matched Trade Data
            Index_match = (strcmpi(Broker_Confirmed_deal_Tbl.trade_date(j),Broker_Opera_Master_conso.trade_date) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.exchange_code(j),Broker_Opera_Master_conso.exchange) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.trade_type(j), Broker_Opera_Master_conso.trade_type) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.terminal_month(j),Broker_Opera_Master_conso.month) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.account(j),Broker_Opera_Master_conso.account_number) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.product_display(j),Broker_Opera_Master_conso.product) & ...
                                strcmpi(Broker_Confirmed_deal_Tbl.Broker_Buy_Sell(j),Broker_Opera_Master_conso.Opera_Buy_Sell) & ...
                                ismember(Broker_Opera_Master_conso.rate,Broker_Confirmed_deal_Tbl.rate(j)) & ...
                                ismember(Broker_Opera_Master_conso.strike,Broker_Confirmed_deal_Tbl.strike(j)));
                                
            if any(Index_match) 
                Index_Opera_Not_Match = vertcat(Index_Opera_Not_Match,find(Index_match));
                if isequal(Broker_Confirmed_deal_Tbl.broker_lots(j),Broker_Opera_Master_conso.lots(Index_match))   %%% Matched Data
                    %%% Matched Data
                    trade_date = datestr(Broker_Confirmed_deal_Tbl.trade_date(j),'dd-mmm-yy');
                    Source = {'Broker'};

                    Matched_Trade_Data = [Source,  trade_date,Broker_Confirmed_deal_Tbl.broker_name(j), Broker_Confirmed_deal_Tbl.account(j), ...
                                          Broker_Confirmed_deal_Tbl.exchange_code(j), Broker_Confirmed_deal_Tbl.product_display(j), Broker_Confirmed_deal_Tbl.terminal_month(j), Broker_Confirmed_deal_Tbl.trade_type(j), ...
                                          Broker_Confirmed_deal_Tbl.strike(j), Broker_Confirmed_deal_Tbl.Broker_Buy_Sell(j), Broker_Confirmed_deal_Tbl.broker_lots(j), ... 
                                          Broker_Confirmed_deal_Tbl.rate(j)];

                    Matched_Broker_Trade_Opera_Data = vertcat(Matched_Broker_Trade_Opera_Data,Matched_Trade_Data);

                else   %%%% Not Matched Data
                    trade_date = datestr(Broker_Confirmed_deal_Tbl.trade_date(j),'dd-mmm-yy');
                    Source = {'Broker'};
                    System_Lot = Broker_Opera_Master_conso.lots(Index_match);
                    RMO_Remarks = {'Lot mismatch between Broker and System'};
                    Trader_Remarks = {''};

                    UnMatched_Trade_Data = [Source,  trade_date,Broker_Confirmed_deal_Tbl.broker_name(j), Broker_Confirmed_deal_Tbl.account(j), ...
                                            Broker_Confirmed_deal_Tbl.product_display(j), Broker_Confirmed_deal_Tbl.exchange_code(j), Broker_Confirmed_deal_Tbl.trade_type(j), Broker_Confirmed_deal_Tbl.terminal_month(j), ...
                                            Broker_Confirmed_deal_Tbl.strike(j), Broker_Confirmed_deal_Tbl.Broker_Buy_Sell(j), Broker_Confirmed_deal_Tbl.broker_lots(j), ... 
                                            Broker_Confirmed_deal_Tbl.rate(j),System_Lot, RMO_Remarks, Trader_Remarks];

                    UnMatched_Broker_Trade_Opera_Data = vertcat(UnMatched_Broker_Trade_Opera_Data,UnMatched_Trade_Data);
                end

           else     %%%% Not Matched Data
                    trade_date = datestr(Broker_Confirmed_deal_Tbl.trade_date(j),'dd-mmm-yy');
                    Source = {'Broker'};
                    System_Lot = {''};
                    RMO_Remarks = {'Missing Trade in Opera Statement'};
                    Trader_Remarks = {''};

                    UnMatched_Trade_Data = [Source,  trade_date,Broker_Confirmed_deal_Tbl.broker_name(j), Broker_Confirmed_deal_Tbl.account(j), ...
                                            Broker_Confirmed_deal_Tbl.product_display(j), Broker_Confirmed_deal_Tbl.exchange_code(j), Broker_Confirmed_deal_Tbl.trade_type(j), Broker_Confirmed_deal_Tbl.terminal_month(j), ...
                                            Broker_Confirmed_deal_Tbl.strike(j), Broker_Confirmed_deal_Tbl.Broker_Buy_Sell(j), Broker_Confirmed_deal_Tbl.broker_lots(j), ... 
                                            Broker_Confirmed_deal_Tbl.rate(j),System_Lot, RMO_Remarks, Trader_Remarks];

                    UnMatched_Broker_Trade_Opera_Data = vertcat(UnMatched_Broker_Trade_Opera_Data,UnMatched_Trade_Data);
            end
        end 
    end

    %%% Opera Trades --> Missing Trades in Broker Statements (opera remaining
    %%% trades)
    %%%% Not Matched Data
    Broker_Opera_Master_conso(unique(Index_Opera_Not_Match),:) = [];
    if ~isempty(Broker_Opera_Master_conso)
        trade_date = datestr(Broker_Opera_Master_conso.trade_date,'dd-mmm-yy');
        Source = repmat({'Opera'},size(Broker_Opera_Master_conso,1),1);
        broker_lots = repmat({''},size(Broker_Opera_Master_conso,1),1);
        RMO_Remarks = repmat({'Missing Trade in Broker Statement'},size(Broker_Opera_Master_conso,1),1);
        Trader_Remarks = repmat({''},size(Broker_Opera_Master_conso,1),1);

        Opera_UnMatched_Trade_Data = [Source,  cellstr(trade_date),Broker_Opera_Master_conso.counterparty, Broker_Opera_Master_conso.account_number, ...
                                 Broker_Opera_Master_conso.product, Broker_Opera_Master_conso.exchange, Broker_Opera_Master_conso.trade_type, Broker_Opera_Master_conso.month, ...
                                 num2cell(Broker_Opera_Master_conso.strike), Broker_Opera_Master_conso.Opera_Buy_Sell, broker_lots, ... 
                                 num2cell(Broker_Opera_Master_conso.rate),num2cell(Broker_Opera_Master_conso.lots), RMO_Remarks, Trader_Remarks];

        UnMatched_Broker_Trade_Opera_Data = vertcat(UnMatched_Broker_Trade_Opera_Data,Opera_UnMatched_Trade_Data);
    end              
    %%% Header's
    Past_Date_Trade_Header = {'Source',	'Broker',	'Account',	'Trade_Date',	'Product',	'Exchange',	'Trade_Type',	'Terminal_Month',	'Strike',	'Buy_Sell',	'Broker_Lot',	'Trade_Price'};
    UnMatched_Trade_Header = {'Source',	'Trade_Date', 'Broker',	'Account',	'Product',	'Exchange',	'Trade_Type',	'Terminal_Month',	'Strike',	'Buy_Sell',	'Broker_Lot',	'Trade_Price',	'System_Lot',	'RMO_Remarks',	'Trader_Remarks'};
    Matched_Trade_Header = {'Source',	'Trade_Date',	'Broker',	'Account',	'Exchange',	'Product',	'Terminal_Month',	'Trade_Type',	'Strike',	'Buy_Sell',	'Broker_Lot',	'Trade_Price'};

    %%% write data into file
    if isempty(Past_Date_Trade_Data)
        xlswrite(OutXLSName,{'No Data'},'Past Date Trade');
    else
        xlswrite(OutXLSName,[Past_Date_Trade_Header;Past_Date_Trade_Data],'Past Date Trade');
    end

    %%% write data into file
    if isempty(Matched_Broker_Trade_Opera_Data)
        xlswrite(OutXLSName,{'No Data'},'Matched');
    else
        xlswrite(OutXLSName,[Matched_Trade_Header;Matched_Broker_Trade_Opera_Data],'Matched');
    end  
    
    %%% write data into file
    if isempty(UnMatched_Broker_Trade_Opera_Data)
        xlswrite(OutXLSName,{'No Data'},'UnMatched');
    else
        xlswrite(OutXLSName,[UnMatched_Trade_Header;UnMatched_Broker_Trade_Opera_Data],'UnMatched');
    end 

    xls_delete_sheets(fullfile(pwd,OutXLSName));
    xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Past Date Trade');
    
catch ME
        OutErrorMsg = cellstr(ME.message);

        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
end

end
