function [OutErrorMsg, Header , Row_Data] = AGF_SecondCut_Reports_FXD(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

OutErrorMsg = {'No errors'};
Header = {'No Data'};
Row_Data = {' '};
% global ObjDB;

try
    GroupType_Index = strcmpi(ColNames,'group_type');
    GroupType_Data =  Data(:,GroupType_Index);
    Matched_GroupType = strcmpi(GroupType_Data,'FXD');    %% Only FXD Index
    
    User_Want_Col = {'group_type','instrument','subportfolio','maturity_date','active_lots','original_premium'};
    Matched_Col = cellStrfind_Perfect(ColNames,User_Want_Col);
    Filterd_Data = Data(Matched_GroupType,Matched_Col);    %% Get User Want Column with Matched FXD Index
    
    if(~isempty(Filterd_Data))
        Instrument_Index = strcmpi(User_Want_Col,'instrument');
        Instrument_Data = Filterd_Data(:,Instrument_Index);
        Unique_Instrument_Data = unique(Instrument_Data);
        Unique_Instrument_Data = Unique_Instrument_Data';
        % Unique_Instrument_Data = {'EUR/USD','USD/BRL','USD/MYR','USD/CNY'};  %% 'blank'
        
        Subportfolio_Index = strcmpi(User_Want_Col,'subportfolio');
        Subportfolio_Data = Filterd_Data(:,Subportfolio_Index);
        Unique_Subportfolio_Data  = unique(Subportfolio_Data);
        
        MaturityDate_Index = strcmpi(User_Want_Col,'maturity_date');
        Active_Lots_Index = strcmpi(User_Want_Col,'active_lots');
        Premium_Index =  strcmpi(User_Want_Col,'original_premium');
        
        
        
        count = 1;
        for i = 1 : length(Unique_Subportfolio_Data)
            
            Current_Subportfolio = Unique_Subportfolio_Data{i};
            Labels{count,1} = Current_Subportfolio;
            
            Matched_Subportfolio = find(strcmpi(Subportfolio_Data,Current_Subportfolio));
            
            Temp_Matrix = Filterd_Data(Matched_Subportfolio,:);
            
            
            for ii = 1 : length(Unique_Instrument_Data)
                Current_Instrument = Unique_Instrument_Data(ii);
                Current_Instrument_Col = cellStrfind_Perfect(Unique_Instrument_Data,Current_Instrument) ;
                Current_Instrument_Matched = cellStrfind_Perfect(Temp_Matrix(:,Instrument_Index),Current_Instrument);
                
                if(Current_Instrument_Matched)
                    %USD_MYR{count,Current_Instrument_Col} = sum(cell2mat(Temp_Matrix(Current_Instrument_Matched,Active_Lots_Index))); %% Sum_ActiveLots_Portfolio;
                    USD_MYR{count,Current_Instrument_Col} = sum((cell2mat(Temp_Matrix(Current_Instrument_Matched,Active_Lots_Index)).* cell2mat(Temp_Matrix(Current_Instrument_Matched,Premium_Index))) .* -1);
                else
                    USD_MYR{count,Current_Instrument_Col} = [];
                end
            end
            
            Unique_MaturityDate_Data = unique(Temp_Matrix(:,MaturityDate_Index));
            count = count + 1;
            
            for k = 1 : length(Unique_MaturityDate_Data)
                Current_MaturityDate = Unique_MaturityDate_Data{k};
                Labels{count,1} = Current_MaturityDate;
                
                for ii = 1 : length(Unique_Instrument_Data)
                    Cur_Ins = Unique_Instrument_Data(ii);
                    Cur_Ins_Col = cellStrfind_Perfect(Unique_Instrument_Data,Cur_Ins);
                    
                    Current_Maturity_Index = find(strcmpi(Cur_Ins, Temp_Matrix(:,Instrument_Index)) & strcmpi(Current_MaturityDate, Temp_Matrix(:,MaturityDate_Index)));
                    
                    if(Current_Maturity_Index)
                        %USD_MYR{count,Cur_Ins_Col} = sum(cell2mat(Temp_Matrix(Current_Maturity_Index,Active_Lots_Index))); %% Sum_ActiveLots_Portfolio;
                        USD_MYR{count,Cur_Ins_Col} = sum((cell2mat(Temp_Matrix(Current_Maturity_Index,Active_Lots_Index)).* cell2mat(Temp_Matrix(Current_Maturity_Index,Premium_Index))) .* -1);
                    else
                        USD_MYR{count,Cur_Ins_Col} = [];
                    end
                    
                end
                
                count = count + 1;
            end
            
        end
        
        Temp_Var = cell(length(Labels),1);   %% Using for blank Column
        
        [GrandTotal_Row,~] = cell2sum_Row_Col(USD_MYR);
        
        %%
        %     Header = [{'Sum of Net Notional','Column Labels','','','','',''};{'Row Labels','EUR/USD','USD/BRL','USD/MYR','USD/CNY','Blank','Grand Total'}];
        Header = cell(2,size(Unique_Instrument_Data,2)+ 2);
        Header(1,1:2) = {'Sum of Net Notional','Column Labels'};
        
        %     Header(2,:) = ['Row Labels',Unique_Instrument_Data,'Blank','Grand Total']; %% Including Blank Column
        %     Row_Data = [Labels, USD_MYR, Temp_Var, GrandTotal_Row];
        
        Header(2,:) = ['Row Labels',Unique_Instrument_Data,'Grand Total']; %% Excluding Blank Column
        Row_Data = [Labels, USD_MYR, GrandTotal_Row];
        
        %     xlswrite(OutXLSFileName,[Header;Row_Data],'FXD Report');
    end
    
catch ME
    Header = {'No Data'};
    Row_Data = {'No Data'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end
end