clc
clear

InBUName = 'agf';  %% 'agf' or 'usg'

%%
try
    [OutErrorMsg1,OutXLSFileName1] = generate_ff_portfolio_report(InBUName);
    cprintf('key','%s finished\n', 'generate_ff_portfolio_report');
    
    [OutErrorMsg2,OutXLSFileName2] = AGF_SecondCut_Reports(InBUName);
    cprintf('key','%s finished\n', 'AGF Main Report');
    
    [OutErrorMsg3,OutXLSFileName3] = AGF_SecondCut_Reports_Portfolio(InBUName);
    cprintf('key','%s finished\n', 'AGF Portfolio Report');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

%%


% InBUName = 'agf';
% OutXLSFileName = getXLSFilename('AGF-Report');
% [Subportfolio_Total_Data , OneDay_Before_portfoliogroup_Data , OneWeek_Before_portfoliogroup_Data ,...
%     Total_Change_PNL_portfoliogroup_FTW , Total_Change_PNL_portfoliogroup_FTD,...
%     PortfolioGroup_Total_Data, OneDay_Before_portfoliowise_Data , OneWeek_Before_portfoliowise_Data , ...
%     Total_Change_PNL_portfoliowise_FTW ,  Total_Change_PNL_portfoliowise_FTD ] = AGF_SecondCut_Reports_PnL_Move_Table(OutXLSFileName,InBUName);
%
% msgbox('Finish')

