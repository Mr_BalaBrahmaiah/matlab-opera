function [Overall_ProductWiseData , Overall_ProductGroupWiseData] = AGF_SecondCut_Reports_Exposure(InBUName)

%global ObjDB;
ObjDB = connect_to_database;
Overall_ProductWiseData = 'No Data';
Overall_ProductGroupWiseData = 'No Data';

try
    
    update_future_cont_series_table;
    update_option_cont_series_table;
    
    % ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    % SqlQuery = 'select * from helper_settle_pricing_subportfoliovalues_view where derivative_type not like ''fx%'' and subportfolio not like ''conso%'' and p1_name not like ''CM OR%''';
    % [SettleColNames,SData] = read_from_database(ViewName,0,SqlQuery,InBUName);
    ViewName = ['helper_settle_pricing_subportfoliovalues_view_',char(InBUName)];
    SqlQuery = ['select * from ',ViewName,' where derivative_type not like ''fx%'' and subportfolio not like ''conso%'' and p1_name not like ''CM OR%'''];
    [SettleColNames,SData] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    % ObjDB = connect_to_database;
    %     store accounting data for var report
    
    UniqueFields = {'value_date','settlement_date','subportfolio','p1_name','contract_month','derivative_type','strike'};
    SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11',...
        'settle_gamma_12','settle_gamma_21','settle_gamma_22',...
        'settle_vega_1','settle_vega_2','settle_theta','mtm-usd','mtm-nc'};
    OutputFields = [UniqueFields,'p1_settleprice',SumFields];
    WeightedAverageFields = [];
    [~,OutData] = consolidatedata(SettleColNames, SData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Delta = cell2mat(OutData(:,9)) +  cell2mat(OutData(:,10));
    Gamma = cell2mat(OutData(:,11)) +  cell2mat(OutData(:,12)) + cell2mat(OutData(:,13)) +  cell2mat(OutData(:,14));
    Vega =  cell2mat(OutData(:,15)) +  cell2mat(OutData(:,16));
    
    AccVARReportData = [OutData(:,1:8),num2cell(Delta),num2cell(Gamma),num2cell(Vega),OutData(:,17:end)];
    
    TableName = ['var_accounting_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, AccVARReportData);
    
    update_curr_mult_table;
    
    TableName = ['var_aggregation_level_table_',InBUName];
    SqlQuery = ['select distinct(aggregation_level) from ',TableName];
    AggregationLevel = fetch(ObjDB,SqlQuery);
    NumDays = 100;%100,252,500
    [~,~,Out100VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
    NumDays = 252;
    [~,~,Out252VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
    NumDays = 500;
    [~,~,Out500VARData] = generate_VAR_report(InBUName,AggregationLevel,char(DBValueDate),char(DBValueDate),NumDays);
    
    %     % calculate the average of 100,252 and 500 days VAR
    OutputHeader = {'value_date','Aggregation_level','VAR'};
    ConsolidatedVARData = [Out100VARData(2:end,:);Out252VARData(2:end,:);Out500VARData(2:end,:)];
    DSConsolidatedVARData = cell2dataset([OutputHeader;ConsolidatedVARData]);
    AggrLevel = unique(DSConsolidatedVARData.Aggregation_level);
    OutVARData = [];
    for i = 1:length(AggrLevel)
        IdxFound = strcmpi(AggrLevel{i},DSConsolidatedVARData.Aggregation_level);
        ValueDate = unique(DSConsolidatedVARData.value_date(IdxFound));
        AvgVAR = mean(DSConsolidatedVARData.VAR(IdxFound));
        RowData = [ValueDate,AggrLevel(i),num2cell(AvgVAR)];
        OutVARData = [OutVARData;RowData];
    end
    
    RawData = OutVARData(:,2:3);
    
    TableName = ['var_report_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutVARData);
    
    
    %% Get From Database
    RPO_TableName = [char(InBUName),'_reports_product_order_table'];
    SqlQuery = ['select * from ',char(RPO_TableName),' where product_type in (''COMMODITY'')'] ; %% ''FXFUT''
    Table_Data  = fetch(ObjDB,SqlQuery);
    Table_Data  = sortrows(Table_Data);
    Unique_ProductName = Table_Data(:,2) ; %% Only FUT and OFUT
    
    RPGO_TableName = [char(InBUName),'_reports_productgroup_order_table'];
    SqlQuery = ['select * from ',char(RPGO_TableName)] ;
    Table_Data  = fetch(ObjDB,SqlQuery);
    Table_Data  = sortrows(Table_Data);
    Unique_ProductGroup = Table_Data(:,2) ;
    
    RPro_GO_TableName = [char(InBUName),'_reports_portfoliogroup_order_table'];
    SqlQuery = ['select * from ',char(RPro_GO_TableName)];
    Table_Data  = fetch(ObjDB,SqlQuery);
    Table_Data  = sortrows(Table_Data);
    Unique_PortfolioGroup = Table_Data(:,2)' ;
    
    Unique_ProductName = [Unique_ProductName ; {'TOTAL'}];  
    Unique_PortfolioGroup = [Unique_PortfolioGroup , {'TOTAL'}] ;
    Unique_ProductGroup = [Unique_ProductGroup ; {'TOTAL'}] ;
    
    %% ProductWise Report
    Total_ProductWise_Data = cell(size(Unique_ProductName,1) , size(Unique_PortfolioGroup,2)) ;
    
    for i = 1 : size(Unique_ProductName,1)
        
        Current_ProductName = Unique_ProductName(i) ;
        
        for ii = 1 : size(Unique_PortfolioGroup,2)
            
            Current_PortfolioGroup = Unique_PortfolioGroup(ii) ;
            
            if(strcmpi(Current_ProductName,'TOTAL'))
                Product_Portfolio_Name = strcat(Current_ProductName,{':'},Current_PortfolioGroup) ;
            else
                Product_Portfolio_Name = strcat(Current_PortfolioGroup,{':'},Current_ProductName) ;
            end
            
            Matched_Product_Portfolio_Index = cellStrfind_exact(RawData(:,1) , Product_Portfolio_Name);
            
            if(~isempty(Matched_Product_Portfolio_Index))
                
                Total_ProductWise_Data(i,ii) = RawData(Matched_Product_Portfolio_Index,2) ;
                
            end
            
        end
        
    end
    
    ProductWise_Header = [{'PRODUCT'} ,Unique_PortfolioGroup ];
    ProductWise_Header(end) = {'CONSOLIDATED'};
    ProductWiseData = [Unique_ProductName , Total_ProductWise_Data];
    
    Overall_ProductWiseData = [ProductWise_Header ; ProductWiseData] ;
    
    
    %% Product GroupWise Report
    
    Total_ProductGroup_Data = cell(size(Unique_ProductGroup,1) , size(Unique_PortfolioGroup,2)) ;
    
    for i = 1 : size(Unique_ProductGroup,1)
        
        Current_ProductGroup = Unique_ProductGroup(i) ;
        
        for ii = 1 : size(Unique_PortfolioGroup,2)
            
            Current_PortfolioGroup = Unique_PortfolioGroup(ii) ;
            
            if(strcmpi(Current_ProductGroup,'TOTAL'))
                Product_Portfolio_Name = strcat(Current_ProductGroup,{':'},Current_PortfolioGroup) ;
            else
                Product_Portfolio_Name = strcat(Current_PortfolioGroup,{':'},Current_ProductGroup) ;
            end
            
            Matched_Product_ProductGroup_Index = cellStrfind_exact(RawData(:,1) , Product_Portfolio_Name);
            
            if(~isempty(Matched_Product_ProductGroup_Index))
                
                Total_ProductGroup_Data(i,ii) = RawData(Matched_Product_ProductGroup_Index,2) ;
                
            end
            
        end
        
    end
    
    ProductGroupWise_Header = [{'PRODUCT'} ,Unique_PortfolioGroup ];
    ProductGroupWise_Header(end) = {'CONSOLIDATED'};
    ProductGroupWiseData = [Unique_ProductGroup , Total_ProductGroup_Data];
    
    Overall_ProductGroupWiseData = [ProductGroupWise_Header ; ProductGroupWiseData] ;
    
           
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
    Overall_ProductWiseData = {'Error Data'};
    Overall_ProductGroupWiseData = {'Error Data'};
end






