function [OutErrorMsg,Header1,New_RowData,NewHeader,New_RowData_Delta] = AGF_SecondCut_Reports_PNP_Portfolio(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName,Selected_Portfolio,User_Want_Portfolio)

OutErrorMsg = {'No errors'};

global ObjDB;

try
    
    %     ViewName = 'helper_eod_monthend_report_view';
    %     SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    %     [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,'agf');
    
    if ~strcmpi(Data,'No Data')
        
        InData = cell2dataset([ColNames;Data]);
        
        IdxBlankContractMonth = strcmpi('',InData.contract_month);
        InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(char(InBUName),InData,1);
        
        UniqueFields = {'PORTFOLIO','PRODUCT','GRP','CONTRACT_MONTH'};
        SumFields={'Total_USD','Gamma','Theta','Vega','Delta','gamma_lots'};
        OutputFields = [UniqueFields,SumFields,'future_price','mult_factor','nc_factor','fx_nc_factor'];
        WeightedAverageFields = [];
        [OutputFields,Data] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;Data]);
        NetPosVal = TempOutData.Delta .*  TempOutData.mult_factor .* TempOutData.future_price  .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        Data(:,end-3:end) = [];
        Data = [Data,num2cell(NetPosVal)];
        ColNames = {'subportfolio','product_name','group_type','contract_month','mtm_usd','settle_gamma_11','settle_theta','settle_vega_1','settle_delta_1','gamma_lots','NetPosVal'};
    end
       
    
    %% Summary Report  %% Read 'combined_report_mapping_table'
    Subportfolio_Index =  strcmpi(ColNames,'subportfolio');
    Subportfolio_Data = Data(:,Subportfolio_Index);
    
    %     [MappingFieldNames,MappingData] = read_from_database('agf_combined_report_mapping_table',0);
    CRM_TableName = [char(InBUName),'_combined_report_mapping_table'];
    [MappingFieldNames,MappingData] = Fetch_DB_Data(ObjDB,[],CRM_TableName);
    
    Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
    Portfolio_Data = MappingData(:,Portfolio_Col);
    Unique_Portfolio = unique(Portfolio_Data);
    
    Map_Subportfolio_Index =  strcmpi(MappingFieldNames,'subportfolio');
    Map_Subportfolio_Data = MappingData(:,Map_Subportfolio_Index);
    
    Category_Col = strcmpi(MappingFieldNames,'category');
    Category_Data = MappingData(:,Category_Col);
    %         Unique_Category = unique(Category_Data);
    
    for i = 1 :length(Unique_Portfolio)
        
        Index = find(strcmpi(Portfolio_Data,Unique_Portfolio{i}));
        
        Grouped_Portfolio = Map_Subportfolio_Data(Index)';
        
        CurrVarname = [Unique_Portfolio{i},'_',char(unique(Category_Data(Index)))];
        CurrVarname = strrep(CurrVarname,'-','_');
        CurrVarname = strrep(CurrVarname,' ','_');
        
        RequiredField_Index = cellStrfind(Subportfolio_Data,Grouped_Portfolio);
        PortfolioGroup.(CurrVarname) = Data(RequiredField_Index,:);
        
    end
    
    FieldNames =  fieldnames(PortfolioGroup);
    
    clear Category_Data;
    %%
    ProductName_Index = strcmpi(ColNames,'product_name');
    ProductName_Data = Data(:,ProductName_Index);
    
    %% Find Unique Product Name
    
    % Unique_ProductName = unique(ProductName_Data);    %% Find Unique Product Name
    
    %     ObjDB = connect_to_database;
    RPO_TableName = [char(InBUName),'_reports_product_order_table'];
    SqlQuery = ['select * from ',char(RPO_TableName)] ;
    Table_Data  = fetch(ObjDB,SqlQuery);
    
    Table_Data  = sortrows(Table_Data);
    Unique_ProductName = Table_Data(:,2) ;
    
    %%
    MtmUsd_Index = find(strcmpi(ColNames,'mtm_usd')); %% Find mtm_usd Index
    Delta_Index = find(strcmpi(ColNames,'settle_delta_1')); %% Find settle_delta_1 Index
    Gamma_Index = find(strcmpi(ColNames,'gamma_lots')); %% Find settle_gamma_11 Index  %% 'settle_gamma_11'
    Vega_Index = find(strcmpi(ColNames,'settle_vega_1')); %% Find settle_vega_1 Index
    Theta_Index = find(strcmpi(ColNames,'settle_theta')); %% Find settle_theta Index
    NetPos_Index = find(strcmpi(ColNames,'NetPosVal'));
    ContractMonth_Index = find(strcmpi(ColNames,'contract_month'));
    
    for ii = 1 : length(FieldNames)
        
        CurrentGroup_ProductCol = PortfolioGroup.(FieldNames{ii})(:,ProductName_Index);  %% Get Portfolio Group and Matched Index for Particular Product
        
        for i = 1 : length(Unique_ProductName)
            
            Matched_Index = find(strcmpi(CurrentGroup_ProductCol,Unique_ProductName{i}));
            
            CurrentGroup_ProductData = PortfolioGroup.(FieldNames{ii})(Matched_Index,:);  %% Get Matched Row
            
            GroupType_Index = strcmpi(ColNames,'group_type');         %% Get Group Type Index and GroupType Col Data
            GroupType_Data = CurrentGroup_ProductData(:,GroupType_Index);
            
            MatchedIndex_FUT = find(strcmpi(GroupType_Data,'FUT'));
            MatchedIndex_OPT = find(strcmpi(GroupType_Data,'OFUT'));
            MatchedIndex_FXD = find(strcmpi(GroupType_Data,'FXD'));
            MatchedIndex_FXFUT = find(strcmpi(GroupType_Data,'FXFUT'));
            
            if(~isempty(MatchedIndex_FUT) || ~isempty(MatchedIndex_OPT) || ~isempty(MatchedIndex_FXD) || ~isempty(MatchedIndex_FXFUT))
                if(~isempty(MatchedIndex_FUT))
                    Category_Type =  'FUT';
                end
                if(~isempty(MatchedIndex_OPT))
                    Category_Type =  'OFUT';
                end
                if(~isempty(MatchedIndex_FXD))
                    Category_Type =  'FXD';
                end
                if(~isempty(MatchedIndex_FXFUT))
                    Category_Type =  'FXFUT';
                end
            else
                Category_Type =  ' ';
            end
            
            FUT_Index_Only = [MatchedIndex_FUT;MatchedIndex_FXD;MatchedIndex_FXFUT];
            Total_Index = [MatchedIndex_FUT;MatchedIndex_OPT;MatchedIndex_FXD;MatchedIndex_FXFUT];
            
            FUT_Data{i} = sum(cell2mat(CurrentGroup_ProductData(FUT_Index_Only,MtmUsd_Index)));   %% Matched FUT and Calculate mtm_usd Particular Index
            OPT_Data{i} = sum(cell2mat(CurrentGroup_ProductData(MatchedIndex_OPT,MtmUsd_Index)));   %% Matched OPT and Calculate mtm_usd Particular Index
            Total_Data{i} = sum(cell2mat(CurrentGroup_ProductData(Total_Index,MtmUsd_Index)));
            
            if(strcmpi(FieldNames{ii},'MM_FF2_OIL_CALENDAR_SPREADS'))
                
                FUT_Data_Delta{i} = AGF_abs_Value_4_MaturityWise_Calender_Spreads(ContractMonth_Index,FUT_Index_Only,Delta_Index,CurrentGroup_ProductData) ;
                %   FUT_Data_Delta{i} = sum(abs(cell2mat(CurrentGroup_ProductData(FUT_Index_Only,Delta_Index))))./2 ;
                
                %   OPT_Data_Delta{i} = AGF_abs_Value_4_MaturityWise_Calender_Spreads(ContractMonth_Index,MatchedIndex_OPT,Delta_Index,CurrentGroup_ProductData) ;
                OPT_Data_Delta{i} = sum(cell2mat(CurrentGroup_ProductData(MatchedIndex_OPT,Delta_Index))) ; %% Get Actual Value
                
                %    Total_Data_Delta{i} = AGF_abs_Value_4_MaturityWise_Calender_Spreads(ContractMonth_Index,Total_Index,Delta_Index,CurrentGroup_ProductData) ;
                Total_Data_Delta{i} =  AGF_abs_Value_4_MaturityWise_Calender_Spreads(ContractMonth_Index,FUT_Index_Only,Delta_Index,CurrentGroup_ProductData) + sum(cell2mat(CurrentGroup_ProductData(MatchedIndex_OPT,Delta_Index)));
                
            else
                FUT_Data_Delta{i} = sum(cell2mat(CurrentGroup_ProductData(FUT_Index_Only,Delta_Index)));
                OPT_Data_Delta{i} = sum(cell2mat(CurrentGroup_ProductData(MatchedIndex_OPT,Delta_Index)));
                Total_Data_Delta{i} = sum(cell2mat(CurrentGroup_ProductData(Total_Index,Delta_Index)));
                
            end
            
            Delta{i} = sum(cell2mat(CurrentGroup_ProductData(:,Delta_Index)));   %% For Second Sheet
            Gamma{i} = sum(cell2mat(CurrentGroup_ProductData(:,Gamma_Index)));
            Vega{i} = sum(cell2mat(CurrentGroup_ProductData(:,Vega_Index)));
            Theta{i} = sum(cell2mat(CurrentGroup_ProductData(:,Theta_Index)));
            NetPos{i} = sum(cell2mat(CurrentGroup_ProductData(:,NetPos_Index)));
            
            Category_Data{i} = Category_Type;
            
        end
        
        FUTData.(FieldNames{ii}) = FUT_Data';                    %% Make Structure for Corresponding Data for Every Product
        OPTData.(FieldNames{ii}) = OPT_Data';
        TOTALData.(FieldNames{ii}) = Total_Data';
        
        FUTData_Delta.(FieldNames{ii}) = FUT_Data_Delta';
        OPTData_Delta.(FieldNames{ii}) = OPT_Data_Delta';
        TOTALData_Delta.(FieldNames{ii}) = Total_Data_Delta';
        
        Total_Delta.(FieldNames{ii}) = Delta';                %% %% For Second Sheet
        Total_Gamma.(FieldNames{ii}) = Gamma';
        Total_Vega.(FieldNames{ii}) = Vega';
        Total_Theta.(FieldNames{ii}) = Theta';
        Total_NetPos.(FieldNames{ii}) = NetPos';
        
        Total_Category_Data.(FieldNames{ii}) = Category_Data';
        
        
    end
    
    TotalBV = num2cell(cell2mat(TOTALData.MM_FF1_OIL_DIRECTIONAL) + cell2mat(TOTALData.MM_FF2_OIL_CALENDAR_SPREADS) + ...
        cell2mat(TOTALData.MM_FF3_OIL_PRODUCT_SPREADS) + cell2mat(TOTALData.MM_FF4_OIL_STRATEGIES) + cell2mat(TOTALData.MM_FF5_OIL_VOLATILITY) + cell2mat(TOTALData.MM_FF6_OIL_WORLD_CHINA_ARB));
    
    Brokerage = cell(length(TotalBV),1);  %% Temporary
    Brokerage(:,:) = {0};
    Net_Accounting = num2cell(cell2mat(TotalBV) + cell2mat(Brokerage));
    
    %% Get Product Category Type for Seperations
    Product_Category_Type = cell(length(Unique_ProductName),1);
    
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF1_OIL_DIRECTIONAL));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF1_OIL_DIRECTIONAL(Matched_ID,1);
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF2_OIL_CALENDAR_SPREADS));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF2_OIL_CALENDAR_SPREADS(Matched_ID,1);
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF3_OIL_PRODUCT_SPREADS));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF3_OIL_PRODUCT_SPREADS(Matched_ID,1);
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF4_OIL_STRATEGIES));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF4_OIL_STRATEGIES(Matched_ID,1);
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF5_OIL_VOLATILITY));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF5_OIL_VOLATILITY(Matched_ID,1);
    Matched_ID = find(cellfun(@(s) (length(s)>1), Total_Category_Data.MM_FF6_OIL_WORLD_CHINA_ARB));
    Product_Category_Type(Matched_ID,1) = Total_Category_Data.MM_FF6_OIL_WORLD_CHINA_ARB(Matched_ID,1);
    
    %% Header
    
    CommonHeader = [{'','','DIRECTIONAL','','','CALENDAR_SPREADS','','','PRODUCT_SPREADS',...
        '','','STRATEGIES','','','VOLATILITY','','','CHINA','',''};...
        {'PRODUCT','TOTAL BV','MM_FF1_OIL','','',...
        'MM_FF2_OIL','','',...
        'MM_FF3_OIL','','',...
        'MM_FF4_OIL','','',...
        'MM_FF5_OIL','','',...
        'MM_FF6_OIL','',''};...
        {'','','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL'}];
    
    
    Header1 = [CommonHeader(1,:),'Brokerage','Net Accounting BV'];
    Header1 = [Header1;CommonHeader(2,:),' ',' '];
    Header1 = [Header1;CommonHeader(3,:),' ',' '];
    
    %% RowData For Book Value Report
    
    RowData = [Unique_ProductName,TotalBV,FUTData.MM_FF1_OIL_DIRECTIONAL,OPTData.MM_FF1_OIL_DIRECTIONAL,TOTALData.MM_FF1_OIL_DIRECTIONAL,...
        FUTData.MM_FF2_OIL_CALENDAR_SPREADS,OPTData.MM_FF2_OIL_CALENDAR_SPREADS,TOTALData.MM_FF2_OIL_CALENDAR_SPREADS,...
        FUTData.MM_FF3_OIL_PRODUCT_SPREADS,OPTData.MM_FF3_OIL_PRODUCT_SPREADS,TOTALData.MM_FF3_OIL_PRODUCT_SPREADS,...
        FUTData.MM_FF4_OIL_STRATEGIES,OPTData.MM_FF4_OIL_STRATEGIES,TOTALData.MM_FF4_OIL_STRATEGIES,...
        FUTData.MM_FF5_OIL_VOLATILITY,OPTData.MM_FF5_OIL_VOLATILITY,TOTALData.MM_FF5_OIL_VOLATILITY,...
        FUTData.MM_FF6_OIL_WORLD_CHINA_ARB,OPTData.MM_FF6_OIL_WORLD_CHINA_ARB,TOTALData.MM_FF6_OIL_WORLD_CHINA_ARB,Brokerage,Net_Accounting];
    
    %% Seperate Product Type Wise Data and Uploading to 'agf_productwise_combined_pnl_table'
    
    [Header1,New_RowData] = AGF_SecondCut_Reports_PNP_Portfolio_SepData_ProductTypeWise(Product_Category_Type, Header1, RowData, 1,Selected_Portfolio); %% 1 0r 0 means Upload to DB Status
    % xlswrite(OutXLSFileName,[Header1;New_RowData],'PNP Book Value Report');
    
    %%
    NetDelta = num2cell(cell2mat(Total_Delta.MM_FF1_OIL_DIRECTIONAL) + cell2mat(Total_Delta.MM_FF3_OIL_PRODUCT_SPREADS) ...
        + cell2mat(Total_Delta.MM_FF4_OIL_STRATEGIES) + cell2mat(Total_Delta.MM_FF5_OIL_VOLATILITY) + cell2mat(Total_Delta.MM_FF6_OIL_WORLD_CHINA_ARB)); %% Ignore cell2mat(Total_Delta.MM_FF2_OIL_CALENDAR_SPREADS)
    
    CombinedGamma =  num2cell(cell2mat(Total_Gamma.MM_FF1_OIL_DIRECTIONAL) + cell2mat(Total_Gamma.MM_FF2_OIL_CALENDAR_SPREADS) + cell2mat(Total_Gamma.MM_FF3_OIL_PRODUCT_SPREADS) ...
        + cell2mat(Total_Gamma.MM_FF4_OIL_STRATEGIES) + cell2mat(Total_Gamma.MM_FF5_OIL_VOLATILITY) + cell2mat(Total_Gamma.MM_FF6_OIL_WORLD_CHINA_ARB));
    CombinedVega =  num2cell(cell2mat(Total_Vega.MM_FF1_OIL_DIRECTIONAL) + cell2mat(Total_Vega.MM_FF2_OIL_CALENDAR_SPREADS) + cell2mat(Total_Vega.MM_FF3_OIL_PRODUCT_SPREADS) ...
        + cell2mat(Total_Vega.MM_FF4_OIL_STRATEGIES) + cell2mat(Total_Vega.MM_FF5_OIL_VOLATILITY) + cell2mat(Total_Vega.MM_FF6_OIL_WORLD_CHINA_ARB));
    CombinedTheta =  num2cell(cell2mat(Total_Theta.MM_FF1_OIL_DIRECTIONAL) + cell2mat(Total_Theta.MM_FF2_OIL_CALENDAR_SPREADS) + cell2mat(Total_Theta.MM_FF3_OIL_PRODUCT_SPREADS) ...
        + cell2mat(Total_Theta.MM_FF4_OIL_STRATEGIES) + cell2mat(Total_Theta.MM_FF5_OIL_VOLATILITY) + cell2mat(Total_Theta.MM_FF6_OIL_WORLD_CHINA_ARB));
    
    % NetPosition = cell(length(CombinedTheta),1);
    % NetPosition(:,:) = num2cell(NaN);
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF1_OIL_DIRECTIONAL));
    Total_NetPos.MM_FF1_OIL_DIRECTIONAL(NaN_Index,1) = {0};
    
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF2_OIL_CALENDAR_SPREADS));
    Total_NetPos.MM_FF2_OIL_CALENDAR_SPREADS(NaN_Index,1) = {0};
    
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF3_OIL_PRODUCT_SPREADS));
    Total_NetPos.MM_FF3_OIL_PRODUCT_SPREADS(NaN_Index,1) = {0};
    
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF4_OIL_STRATEGIES));
    Total_NetPos.MM_FF4_OIL_STRATEGIES(NaN_Index,1) = {0};
    
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF5_OIL_VOLATILITY));
    Total_NetPos.MM_FF5_OIL_VOLATILITY(NaN_Index,1) = {0};
    
    NaN_Index = find(cellfun(@(V) any(isnan(V(:))), Total_NetPos.MM_FF6_OIL_WORLD_CHINA_ARB));
    Total_NetPos.MM_FF6_OIL_WORLD_CHINA_ARB(NaN_Index,1) = {0};
    
    NetPosition =  num2cell(cell2mat(Total_NetPos.MM_FF1_OIL_DIRECTIONAL) + cell2mat(Total_NetPos.MM_FF2_OIL_CALENDAR_SPREADS) + cell2mat(Total_NetPos.MM_FF3_OIL_PRODUCT_SPREADS) ...
        + cell2mat(Total_NetPos.MM_FF4_OIL_STRATEGIES) + cell2mat(Total_NetPos.MM_FF5_OIL_VOLATILITY) + cell2mat(Total_NetPos.MM_FF6_OIL_WORLD_CHINA_ARB));
    
    %%
    NewHeader = [CommonHeader(1,:),'COMBINED GREEKS',' ',' ','Net Position Value'];
    NewHeader = [NewHeader; CommonHeader(2,:),'COMBINED GREEKS','COMBINED GREEKS','COMBINED GREEKS','Net Position Value'];
    NewHeader = [NewHeader; CommonHeader(3,:),'GAMMA','THETA','VEGA','Net Position Value'];
    NewHeader = strrep(NewHeader,'TOTAL BV','NET DELTA');
    
    %% Row Data For DELTA Report
    
    % RowData_2 = [Unique_ProductName,NetDelta,FUTData.MM_FF1_OIL_DIRECTIONAL,OPTData.MM_FF1_OIL_DIRECTIONAL,TOTALData.MM_FF1_OIL_DIRECTIONAL,...
    %     FUTData.MM_FF2_OIL_CALENDAR_SPREADS,OPTData.MM_FF2_OIL_CALENDAR_SPREADS,TOTALData.MM_FF2_OIL_CALENDAR_SPREADS,...
    %     FUTData.MM_FF3_OIL_PRODUCT_SPREADS,OPTData.MM_FF3_OIL_PRODUCT_SPREADS,TOTALData.MM_FF3_OIL_PRODUCT_SPREADS,...
    %     FUTData.MM_FF4_OIL_STRATEGIES,OPTData.MM_FF4_OIL_STRATEGIES,TOTALData.MM_FF4_OIL_STRATEGIES,...
    %     FUTData.MM_FF5_OIL_VOLATILITY,OPTData.MM_FF5_OIL_VOLATILITY,TOTALData.MM_FF5_OIL_VOLATILITY,...
    %     CombinedGamma,CombinedTheta,CombinedVega,NetPosition];
    
    
    RowData_2 = [Unique_ProductName,NetDelta,FUTData_Delta.MM_FF1_OIL_DIRECTIONAL,OPTData_Delta.MM_FF1_OIL_DIRECTIONAL,TOTALData_Delta.MM_FF1_OIL_DIRECTIONAL,...
        FUTData_Delta.MM_FF2_OIL_CALENDAR_SPREADS,OPTData_Delta.MM_FF2_OIL_CALENDAR_SPREADS,TOTALData_Delta.MM_FF2_OIL_CALENDAR_SPREADS,...
        FUTData_Delta.MM_FF3_OIL_PRODUCT_SPREADS,OPTData_Delta.MM_FF3_OIL_PRODUCT_SPREADS,TOTALData_Delta.MM_FF3_OIL_PRODUCT_SPREADS,...
        FUTData_Delta.MM_FF4_OIL_STRATEGIES,OPTData_Delta.MM_FF4_OIL_STRATEGIES,TOTALData_Delta.MM_FF4_OIL_STRATEGIES,...
        FUTData_Delta.MM_FF5_OIL_VOLATILITY,OPTData_Delta.MM_FF5_OIL_VOLATILITY,TOTALData_Delta.MM_FF5_OIL_VOLATILITY,...
        FUTData_Delta.MM_FF6_OIL_WORLD_CHINA_ARB,OPTData_Delta.MM_FF6_OIL_WORLD_CHINA_ARB,TOTALData_Delta.MM_FF6_OIL_WORLD_CHINA_ARB,...
        CombinedGamma,CombinedTheta,CombinedVega,NetPosition];
    
    %% Seperate Product Type Wise
    
    [NewHeader,New_RowData_Delta] = AGF_SecondCut_Reports_PNP_Portfolio_SepData_ProductTypeWise(InBUName,Product_Category_Type, NewHeader, RowData_2, 0,Selected_Portfolio); %% 1 0r 0 means Upload to DB Status
    % xlswrite(OutXLSFileName,[NewHeader;New_RowData_Delta],'PNP Delta Report');
    
catch ME
    Header1 = {'No Data'};
    New_RowData ={'No Data'};
    NewHeader = {'No Data'};
    New_RowData_Delta = {'No Data'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end
end






