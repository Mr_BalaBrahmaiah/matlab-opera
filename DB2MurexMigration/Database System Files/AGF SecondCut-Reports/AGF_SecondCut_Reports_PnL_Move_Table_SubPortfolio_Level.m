function AGF_SecondCut_Reports_PnL_Move_Table_SubPortfolio_Level(InBUName) %% ColNames,Data,PortfolioGroup,FieldNames

OutErrorMsg = {'No errors'};

try
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,char(InBUName));
    
    if ~strcmpi(Data,'No Data')
        
        InData = cell2dataset([ColNames;Data]);
        
        IdxBlankContractMonth = strcmpi('',InData.contract_month);
        InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(char(InBUName),InData,1);
        
        UniqueFields = {'PORTFOLIO','PRODUCT','PRODUCT_CODE','GRP','CONTRACT_MONTH'};
        SumFields={'Total_USD','Gamma','Theta','Vega','Delta'};
        OutputFields = [UniqueFields,SumFields,'future_price','mult_factor','nc_factor','fx_nc_factor'];
        WeightedAverageFields = [];
        [OutputFields,Data] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;Data]);
        NetPosVal = TempOutData.Delta .*  TempOutData.mult_factor .* TempOutData.future_price  .* TempOutData.nc_factor .* TempOutData.fx_nc_factor;
        Data(:,end-3:end) = [];
        Data = [Data,num2cell(NetPosVal)];
        ColNames = {'subportfolio','product_name','product_code','group_type','contract_month','mtm_usd','settle_gamma_11','settle_theta','settle_vega_1','settle_delta_1','NetPosVal'};
    end
    
    %% Summary Report  %% Read 'combined_report_mapping_table'
    Subportfolio_Index =  strcmpi(ColNames,'subportfolio');
    Subportfolio_Data = Data(:,Subportfolio_Index);
    
    CRM_TableName = [char(InBUName),'_combined_report_mapping_table'];
    [MappingFieldNames,MappingData] = read_from_database(CRM_TableName,0);
    
    Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
    Portfolio_Data = MappingData(:,Portfolio_Col);
    Unique_Portfolio = unique(Portfolio_Data);
    
    Map_Subportfolio_Index =  strcmpi(MappingFieldNames,'subportfolio');
    Map_Subportfolio_Data = MappingData(:,Map_Subportfolio_Index);
    
    Category_Col = strcmpi(MappingFieldNames,'category');
    Category_Data = MappingData(:,Category_Col);
    %         Unique_Category = unique(Category_Data);
    
    for i = 1 :length(Unique_Portfolio)
        
        Index = find(strcmpi(Portfolio_Data,Unique_Portfolio{i}));
        
        Grouped_Portfolio = Map_Subportfolio_Data(Index)';
        
        CurrVarname = [Unique_Portfolio{i},'_',char(unique(Category_Data(Index)))];
        CurrVarname = strrep(CurrVarname,'-','_');
        CurrVarname = strrep(CurrVarname,' ','_');
        
        RequiredField_Index = cellStrfind(Subportfolio_Data,Grouped_Portfolio);
        PortfolioGroup.(CurrVarname) = Data(RequiredField_Index,:);
        
    end
    
    FieldNames =  fieldnames(PortfolioGroup);
    
    clear Category_Data;
    
    %%  Fetch Settle Date
    SqlQuery = 'select settlement_date from valuation_date_table ' ;
    [~,Current_Settlement_Date] = read_from_database([],0,SqlQuery);
    
    HolDates = {'2016-01-01','2017-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    OneDay_Before_Settlement_Date = datestr(busdate(today-3,-1,HolidayVec),'yyyy-mm-dd');
    OneWeek_Before_Settlement_Date = datestr(busdate(today-6,-1,HolidayVec),'yyyy-mm-dd');
    
    %%
    for k = 1 : 3
        
        if(k==1)
            Settlement_Date = Current_Settlement_Date ;
        elseif(k==2)
            Settlement_Date = OneDay_Before_Settlement_Date;
        else
            Settlement_Date = OneWeek_Before_Settlement_Date ;
        end
        
        
        %% Fetch Data
        %         SqlQuery = ['SELECT * FROM helper_funds_reports_view_agf where settlement_date = ''',char(Settlement_Date),''' ' ] ;
        %         [ColNames,Data] = read_from_database([],0,SqlQuery);
        
        TempOutData = cell2dataset([ColNames;Data]);
        
        %% Make ProductGroup and PortfolioGroup Information for Input Data
        
        ViewName = [char(InBUName),'_combined_report_mapping_table'];
        [ColNames1,Data1] = read_from_database(ViewName,0);
        DBRefPFGroupData = cell2dataset([ColNames1;Data1]);
        
        ViewName = [char(InBUName),'_product_group_table'];
        [ColNames1,Data1] = read_from_database(ViewName,0);
        DBRefPrdtGroupData = cell2dataset([ColNames1;Data1]);
        
        PortfolioGroup_Name = cell(size(TempOutData.subportfolio));
        ProductGroup_Name = cell(size(TempOutData.subportfolio));
        
        for iPF = 1:length(DBRefPFGroupData.subportfolio)
            IdxPF = strcmpi(DBRefPFGroupData.subportfolio{iPF},TempOutData.subportfolio);
            PortfolioGroup_Name(IdxPF) = DBRefPFGroupData.group(iPF);
        end
        
        for iPF = 1:length(DBRefPrdtGroupData.product_code)
            IdxPF = strcmpi(DBRefPrdtGroupData.product_code{iPF},TempOutData.product_code);
            ProductGroup_Name(IdxPF) = DBRefPrdtGroupData.product_group(iPF);
        end
        
        TempOutData.PortfolioGroup = PortfolioGroup_Name;
        TempOutData.ProductGroup = ProductGroup_Name;
        
        % TempOutData = dataset2cell(TempOutData);
        
        %%
        ObjDB = connect_to_database;
        
        RProGO_TableName = [char(InBUName),'_reports_portfoliogroup_order_table'];
        SqlQuery = ['select * from ',char(RProGO_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        Unique_PortfolioGroup = Table_Data(:,2)' ;
        
        RPGO_TableName = [char(InBUName),'_reports_productgroup_order_table'];
        SqlQuery = ['select * from ',char(RPGO_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        Unique_ProductGroup = Table_Data(:,2) ;
        
        %%
        
        Subportfolio_Output_Data = cell(size(Unique_ProductGroup,1),size(Unique_PortfolioGroup,2));
        
        for i = 1 : size(Unique_PortfolioGroup,2)
            Current_PortfolioGroup =  Unique_PortfolioGroup(i);
            
            Matched_PortfolioGroup_Index = cellStrfind_exact(TempOutData.PortfolioGroup ,Current_PortfolioGroup) ;
            
            if(~isempty(Matched_PortfolioGroup_Index))
                Matched_PortfolioGroup_Data = TempOutData(Matched_PortfolioGroup_Index,:);
                
                for ii = 1 : size(Unique_ProductGroup,1)
                    Current_ProductGroup = Unique_ProductGroup(ii);
                    
                    Matched_ProductGroup_Index = cellStrfind_exact(Matched_PortfolioGroup_Data.ProductGroup , Current_ProductGroup) ;
                    Matched_ProductPortfolioGroup_Data = Matched_PortfolioGroup_Data(Matched_ProductGroup_Index,:);
                    
                    Subportfolio_Output_Data{ii,i} = sum( Matched_ProductPortfolioGroup_Data.mtm_usd ) ;
                end
                
            end
        end
        
        [Row_Sum,~]  = cell2sum_Row_Col(Subportfolio_Output_Data) ;
        Subportfolio_Output_Data = [Subportfolio_Output_Data , Row_Sum] ;
        [~,Col_Sum]  = cell2sum_Row_Col(Subportfolio_Output_Data) ;
        Subportfolio_Output_Data = [Subportfolio_Output_Data ; Col_Sum] ;
        
        %Temp = Output_Data ; Change in PnL
        
        Header_Col = [{'PRODUCT'},Unique_PortfolioGroup,{'TOTAL'}];
        Header_Row = [Unique_ProductGroup ; {'TOTAL'}];
        
        RowData = [Header_Row , Subportfolio_Output_Data];
        Total_Data = [Header_Col ; RowData] ;
        
        
        %% Storing Output
        CurrVarname = strrep(char(Settlement_Date),'-','_');
        CurrVarname = strrep(CurrVarname,' ','_');
        CurrVarname = strcat('SettleDate_',CurrVarname);
        
        SettleStruct.(CurrVarname) = Total_Data;
        
        
    end
    
catch ME
    
end

end





