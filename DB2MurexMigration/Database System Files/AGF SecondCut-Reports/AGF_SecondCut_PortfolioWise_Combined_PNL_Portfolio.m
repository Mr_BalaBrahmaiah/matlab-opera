function [OutErrorMsg,Header_1 , Summary_RowData_1 , Header_2 , Summary_RowData_2] = AGF_SecondCut_PortfolioWise_Combined_PNL_Portfolio(InBUName, Selected_Portfolio)
%%

OutErrorMsg = {'No errors'};
Header_1 = {''} ;
Header_2 = {'' } ;
Summary_RowData_1 = {''};
Summary_RowData_2 = {''};

TableName = [char(InBUName),'_portfoliowise_combined_pnl_table_',char(Selected_Portfolio)];

global ObjDB;
try
    CRM_TableName = [char(InBUName),'_combined_report_mapping_table'];
    [MappingFieldNames,MappingData] = Fetch_DB_Data(ObjDB,[],char(CRM_TableName));
    
    Portfolio_Col = cellStrfind_exact(MappingFieldNames,{'portfolio'});
    Category_Col = cellStrfind_exact(MappingFieldNames,{'category'});
    
    Unique_Category_Data = unique(strcat(MappingData(:,Portfolio_Col),{'_'},MappingData(:,Category_Col)));
    
    Unique_Category_Data = [Unique_Category_Data ; {'GROSS BOOK VALUE'} ];
    
    
    %% Get Value Date  and settle Date
    %     ObjDB = connect_to_database ;
    DBValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
    
    DBSettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
    
    %% Get portfoliowise_combined_pnl_table
    SqlQuery = ['select * from ',TableName,''];
    %     [ColNames,Data] = read_from_database(TableName,0);
    [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if ~strcmpi(Data,'No Data')
        %%
        ValueDate_Col = cellStrfind_exact(ColNames,{'value_date'}) ;
        Cumulative_Col = cellStrfind_exact(ColNames,{'cumulative_bv'}) ;
        YTD_BV_Col = cellStrfind_exact(ColNames,{'ytd_bv'}) ;
        
        Portfolio_Col = cellStrfind_exact(ColNames,{'portfolio'}) ;
        Unique_Portfolio = unique(Data(:,Portfolio_Col)) ;
        
        %%
        % for i = 1 : length(Unique_Portfolio)
        
        ValueDate_Index = cellStrfind_exact(Data(:,ValueDate_Col),{DBValueDate}) ;
        Today_BV_Cumulative = Data (ValueDate_Index,Cumulative_Col);
        Today_YTD = Data (ValueDate_Index,YTD_BV_Col) ;
        
        %         SettleDate_Index = cellStrfind_exact(Data(:,ValueDate_Col),{DBSettlementDate}) ;
        %         YesDay_BV_Cumulative = Data (SettleDate_Index,Cumulative_Col);
        %         YesDay_YTD = Data (SettleDate_Index,YTD_BV_Col) ;
        
        Unique_Portfolio = unique(Data(ValueDate_Index,Portfolio_Col));
        YesDay_BV_Cumulative = cell(size(Unique_Portfolio,1),1);
        YesDay_YTD = cell(size(Unique_Portfolio,1),1);
        for i = 1 : length(Unique_Portfolio)
            
            Current_Portfolio = Unique_Portfolio(i);
            Current_Portfolio_Data = Data(cellStrfind_exact(Data(:,Portfolio_Col),Current_Portfolio),:);
            
            SettleDate_Index = cellStrfind_exact(Current_Portfolio_Data(:,ValueDate_Col),{DBSettlementDate}) ;
            if(~isempty(SettleDate_Index))
                YesDay_BV_Cumulative(i,1) = Current_Portfolio_Data (SettleDate_Index,Cumulative_Col);
                YesDay_YTD(i,1) = Current_Portfolio_Data (SettleDate_Index,YTD_BV_Col) ;
            else
                YesDay_BV_Cumulative(i,1) = {0};
                YesDay_YTD(i,1) = {0};
            end
        end
        
        PNL_For_ValueDate = num2cell(cell2mat(Today_BV_Cumulative) - cell2mat(YesDay_BV_Cumulative)) ;
        
        Unique_Category_Data = [Unique_Category_Data(cellStrfind(Unique_Category_Data,Unique_Portfolio)) ; {'GROSS BOOK VALUE'}];
        Summary_RowData_1 = [Unique_Category_Data , Today_BV_Cumulative , Today_YTD , PNL_For_ValueDate];
        Summary_RowData_2 = [Unique_Category_Data , YesDay_YTD , Today_YTD ] ;
        
        % end
        
        %% Make Header
        Header_1 = {'Portfolio','BV_Cumulative','BV_YTD','P&L for the DAY'} ;
        Header_2 = {'Portfolio', 'Yesterday''s BV' ,'Today''s BV' } ;
        
        %% Excel Write
        % XLSFileName_1 = getXLSFilename('Portfoliowise_Combined_PNL_report');
        %
        % xlswrite(XLSFileName_1,[Header_1 ; Summary_RowData_1],'Summary Report') ;
        %
        % XLSFileName_2 = getXLSFilename('PNP_Summary_Report');
        % xlswrite(XLSFileName_2,[Header_2 ; Summary_RowData_2],' PNP Summary Report') ;
        %
        % XLSFileName_1 = fullfile(pwd,XLSFileName_1);
        % XLSFileName_2 = fullfile(pwd,XLSFileName_2);
        %
        % try
        %     xls_delete_sheets(XLSFileName_1);
        %     xls_delete_sheets(XLSFileName_2);
        % catch
        % end
        
    else
        warndlg(['No Data in ',TableName,' DB']);
    end
    
catch ME
    Header_1 = {'No Data'};
    Summary_RowData_1 = {'Error Data'};
    Header_2 = {'No Data'};
    Summary_RowData_2 = {'Error Data'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end
