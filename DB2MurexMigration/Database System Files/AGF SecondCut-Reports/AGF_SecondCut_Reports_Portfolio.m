function [OutErrorMsg,OutputXLSFileNames] = AGF_SecondCut_Reports_Portfolio(InBUName)

OutErrorMsg = {'No errors'};
% OutputXLSFileNames = '';

% User_Select_Portfolio = {'ALPHA'};

global ObjDB;
ObjDB = connect_to_database;

%% Fetch Value Date & Settlement Date

SqlQuery = 'select * from valuation_date_table' ;
[~,DateData] = Fetch_DB_Data(ObjDB,SqlQuery);
Current_ValueDate = DateData(2);
Current_Settlement_Date = DateData(3);

TextFile = strcat('LogFile_Portfolio_',char(Current_ValueDate),'_',char(strrep(datestr(now,'HH:MM:SS'),':','_')),'.txt');
diary(TextFile) ;

%% Fetch ProductGroup and PortfolioGroup Order Table
RPO_TableName = [char(InBUName),'_reports_product_order_table'];
SqlQuery = ['select * from ',char(RPO_TableName)] ;
Table_Data  = fetch(ObjDB,SqlQuery);
Table_Data  = sortrows(Table_Data);
Unique_ProductOrder = Table_Data(:,2) ;

RPGO_TableName = [char(InBUName),'_reports_productgroup_order_table'];
SqlQuery = ['select * from ',char(RPGO_TableName)] ;
Table_Data  = fetch(ObjDB,SqlQuery);
Table_Data  = sortrows(Table_Data);
Unique_ProductGroupOrder = Table_Data(:,2) ;



%%
try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    %     [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    ViewName = [ViewName,'_',InBUName];
    [ColNames,Data_HelperFunds_All] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    Data_All = Data_HelperFunds_All;  %% this variable for preprocessing and 'Data_HelperFunds_All' Used in Net Position Report
    
    if ~strcmpi(Data_All,'No Data')
        
        %% Remove CASH Trades
        %TODO - Add the code : find product_code containing the string
        %"CASH" and remove those lines
        
        ProductCode_Index = strcmpi(ColNames,'product_code');
        ProductCode_Data = Data_All(:,ProductCode_Index);
        CASH_Index = cellStrfind(ProductCode_Data,{'CASH'});
        
        Data_All(CASH_Index,:) = [];
        
        %% Remove Dead Fileds
        
        %         ColNames = [ColNames,'deal_status'];
        Pos_Maturity = strcmpi('maturity_date',ColNames);
        Pos_ValueDate = strcmpi('value_date',ColNames);
        PosNettingStatus = strcmpi('netting_status',ColNames);
        
        IdxNetted = strcmpi('fully_netted',Data_All(:,PosNettingStatus));
        MaturityDate = datenum(Data_All(:,Pos_Maturity),'yyyy-mm-dd');
        ValueDate = datenum(Data_All(:,Pos_ValueDate),'yyyy-mm-dd');
        IdxDeadOpt = (ValueDate > MaturityDate) | IdxNetted;
        
        Data_All(IdxDeadOpt,:) = [];
        
        %% helper_eod_monthend_report_view
        
        ViewName = ['helper_eod_monthend_report_view_',char(InBUName)];
        SqlQuery = ['select * from ',ViewName,' where subportfolio not like ''conso%'' '];
        [ColNames_PnL_Move,Data_PnL_ALL] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        PortfolioCol_PNL = cellStrfind_exact(ColNames_PnL_Move,{'subportfolio'});
        
        %% Summary Report  %% Read 'combined_report_mapping_table'
        Subportfolio_Index =  strcmpi(ColNames,'subportfolio');
        Subportfolio_Data = Data_All(:,Subportfolio_Index);
        
        %         [MappingFieldNames,MappingData_All] = read_from_database('agf_combined_report_mapping_table',0);
        CMP_TableName = [char(InBUName),'_combined_report_mapping_table'];
        [MappingFieldNames,MappingData_All] = Fetch_DB_Data(ObjDB,[],CMP_TableName);
        
        Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
        PortfolioData_All = MappingData_All(:,Portfolio_Col);
        Unique_Portfolio = unique(PortfolioData_All);
        
        Group_Col = strcmpi(MappingFieldNames,'group');
        GroupData_All = MappingData_All(:,Group_Col);
        Unique_PortfolioNames = unique(GroupData_All); %% User_Select_Portfolio ; %% unique(GroupData_All);
        
        for c = 1 : length(Unique_PortfolioNames)
            
            User_Select_Portfolio = Unique_PortfolioNames(c);
            MappingData = MappingData_All(cellStrfind_exact(MappingData_All(:,Group_Col),User_Select_Portfolio),:) ;
            
            Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
            Portfolio_Data = MappingData(:,Portfolio_Col);
            
            Map_Subportfolio_Index =  strcmpi(MappingFieldNames,'subportfolio');
            Map_Subportfolio_Data = MappingData(:,Map_Subportfolio_Index);
            
            Category_Col = strcmpi(MappingFieldNames,'category');
            Category_Data = MappingData(:,Category_Col);
            %         Unique_Category = unique(Category_Data);
            
            Group_Data = MappingData(:,Group_Col);
            
            User_Want_Portfolio = Map_Subportfolio_Data(cellStrfind_exact(Group_Data,User_Select_Portfolio));
            
            for i = 1 :length(Unique_Portfolio)
                
                Index = find(strcmpi(Portfolio_Data,Unique_Portfolio{i}));
                
                Grouped_Portfolio = Map_Subportfolio_Data(Index)';
                
                CurrVarname = [Unique_Portfolio{i},'_',char(unique(Category_Data(Index)))];
                CurrVarname = strrep(CurrVarname,'-','_');
                CurrVarname = strrep(CurrVarname,' ','_');
                
                RequiredField_Index = cellStrfind_exact(Subportfolio_Data,Grouped_Portfolio);
                Temp_Data = Data_All(RequiredField_Index,:);
                PortfolioGroup.(CurrVarname) = Temp_Data(cellStrfind(Temp_Data(:,Subportfolio_Index),User_Want_Portfolio),:);
                
            end
            
            FieldNames =  fieldnames(PortfolioGroup);    %% For find the below for loop Length
            
            BV_Cumulative = cell(length(FieldNames),1);  %% Initialize the Variable
            BV_Cumulative(:,:) = {''};
            BV_YTD = cell(length(FieldNames),1);
            BV_YTD(:,:) = {''};
            
            for i = 1 : length(FieldNames)
                
                CurrentData = PortfolioGroup.(FieldNames{i});
                
                Cumulative_Index =  strcmpi(ColNames,'cumulative_mtm_usd');
                MtmUsd_Index = strcmpi(ColNames,'mtm_usd');
                
                BV_Cumulative{i}  = sum(cell2mat(CurrentData(:,Cumulative_Index)));
                BV_YTD{i} = sum(cell2mat(CurrentData(:,MtmUsd_Index)));
                
            end
            
            %% Get Selected Portfolio Data
            
            Data = Data_All(cellStrfind_exact(Data_All(:,Subportfolio_Index),User_Want_Portfolio),:);
            
            Data_PnL_Move = Data_PnL_ALL(cellStrfind_exact(Data_PnL_ALL(:,PortfolioCol_PNL),User_Want_Portfolio),:);
            
            %% PNP Report
            
            OutXLSFileName = getXLSFilename([upper(char(InBUName)),'-Report-Format-',char(User_Select_Portfolio)]);
            OutputXLSFileNames(c,1) = {OutXLSFileName};
            
            
            [OutErrorMsg,BookValue_Header,BookValue_RowData,Delta_Header,Delta_RowData] = AGF_SecondCut_Reports_PNP_Portfolio(InBUName,ColNames_PnL_Move,Data_PnL_Move,PortfolioGroup,FieldNames,OutXLSFileName,User_Select_Portfolio,User_Want_Portfolio);
            
            xlswrite(OutXLSFileName,[BookValue_Header;BookValue_RowData],'pnp_bv_report');
            xlswrite(OutXLSFileName,[Delta_Header;Delta_RowData],'pnp_delta_report');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' PNP Report']);
            
            %% PRODUCTWISE & PORTFOLIOWISE COMBINED PNL REPORT
            
            [OutErrorMsg,CommonHeader , Total_Data] = AGF_SecondCut_ProductWise_Change_PNL_Portfolio(InBUName,User_Select_Portfolio) ;
            xlswrite(OutXLSFileName,[CommonHeader ; Total_Data],'pnl_change') ;
            
            [OutErrorMsg,Header_1 , Summary_RowData_1 , Header_2 , Summary_RowData_2] = AGF_SecondCut_PortfolioWise_Combined_PNL_Portfolio(InBUName,User_Select_Portfolio) ;
            xlswrite(OutXLSFileName,[Header_1 ; Summary_RowData_1],'Summary') ;
            %  xlswrite(OutXLSFileName,[Header_2 ; Summary_RowData_2],' PNP Summary Report') ; %% No Need
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' pnl_change & Summary']);
            
            %% POSITION DETAILS  (DELTA REPORT)
            
            [OutErrorMsg,Header_Position_Details,RowData_Position_Details] = AGF_SecondCut_Reports_Postion_Details(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            xlswrite(OutXLSFileName,[Header_Position_Details;RowData_Position_Details],'delta_report');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' delta_report']);
            
            %% POSITION DETAILS OPTIONS DELTA REPORT
            
            [OutErrorMsg,Header_Position_Details_OFUT,RowData_Position_Details_OFUT] = AGF_SecondCut_Reports_Postion_Details_Option_Delta(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header_Position_Details_OFUT;RowData_Position_Details_OFUT],'options_delta');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' options_delta']);
            
            %% FUTURES POSITION - MONTH WISE
            
            % [Header_Position_Details_FUT,RowData_Position_Details_FUT] = AGF_SecondCut_Reports_Future_Position_MonthWise(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            [OutErrorMsg,Header_Position_Details_FUT,RowData_Position_Details_FUT] = AGF_SecondCut_Reports_Future_Position_MonthWise_1(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header_Position_Details_FUT;RowData_Position_Details_FUT],'future_position_monthwise');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' future_position_monthwise']);
            
            %% POSITION DETAILS Combined DELTA REPORT
            
            [OutErrorMsg, Header , FUT_MonthWise_Final] = AGF_SecondCut_Reports_Combined_Delta(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header;FUT_MonthWise_Final],'combined_delta_report');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' combined_delta_report']);
            
            %% OPTIONS POSITIONS REPORT
            
            [OutErrorMsg, Header , Total_Data] = AGF_SecondCut_Reports_Options_Position(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header;Total_Data],'options_positions');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' options_positions']);
            
            %% OPTIONS POSITIONS WITH GREEKS REPORT
            
            [OutErrorMsg,Header , Row_Data] = AGF_SecondCut_Reports_Options_Position_1(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header;Row_Data],'options_positions_greeks');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' options_positions_greeks']);
            
            %% FXD REPORT
            
            [OutErrorMsg, Header , Row_Data] = AGF_SecondCut_Reports_FXD(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
            
            xlswrite(OutXLSFileName,[Header;Row_Data],'fxd_report');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' fxd_report']);
            
            %% NPV Report
            
            %             [OutErrorMsg,OutNPVProductData,OutNPVGroupData]  = AGF_SecondCut_Reports_NetPosition_Value(InBUName,ColNames,Data_HelperFunds_All);
            
            ReturnHeader1 = ['product_name',',',[char(lower(User_Select_Portfolio)),'_pnl']];
            NPV_TableName = [char(InBUName),'_netpositionvalue_product_table'];
            SqlQuery1 = ['select ',ReturnHeader1,' from ',char(NPV_TableName),' where value_date = ''',char(Current_ValueDate),''' '];
            [OutNPVProductColNames,OutNPVProductData] = Fetch_DB_Data(ObjDB,SqlQuery1);
            OutNPVProductData = OutNPVProductData(cellStrfind_exact(OutNPVProductData(:,1),[Unique_ProductOrder;{'TOTAL'}]),:); %% Change the OrderWise
            
            ReturnHeader2 = ['product_group',',',[char(lower(User_Select_Portfolio)),'_pnl']];
            NPV_PG_TableName = [char(InBUName),'_netpositionvalue_productgroup_table'];
            SqlQuery2 = ['select ',ReturnHeader2,' from ',char(NPV_PG_TableName),' where value_date = ''',char(Current_ValueDate),''' '];
            [OutNPVGroupColNames,OutNPVGroupData] = Fetch_DB_Data(ObjDB,SqlQuery2);
            OutNPVGroupData = OutNPVGroupData(cellStrfind_exact(OutNPVGroupData(:,1),[Unique_ProductGroupOrder;{'TOTAL'}]),:); %% Change the OrderWise
            
            xlswrite(OutXLSFileName, [OutNPVProductColNames; OutNPVProductData] ,'NET POSITION VALUE-PRODUCT');
            xlswrite(OutXLSFileName, [OutNPVGroupColNames; OutNPVGroupData] ,'NET POSITION VALUE-GROUP');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' NPV Report']);
            
            %% Exposure Report
            
            %             [OutVARProductData , OutVARProductGroupData] = AGF_SecondCut_Reports_Exposure;
            VarP_TableName = [char(InBUName),'_var_product_table'];
            SqlQuery3 = ['select ',ReturnHeader1,' from ',char(VarP_TableName),' where value_date = ''',char(Current_ValueDate),''' '];
            [OutVARProductColNames,OutVARProductData] = Fetch_DB_Data(ObjDB,SqlQuery3);
            OutVARProductData = OutVARProductData(cellStrfind_exact(OutVARProductData(:,1),[Unique_ProductOrder;{'TOTAL'}]),:); %% Change the OrderWise
            
            Var_PG_TableName = [char(InBUName),'_var_productgroup_table'];
            SqlQuery4 = ['select ',ReturnHeader2,' from ',char(Var_PG_TableName),' where value_date = ''',char(Current_ValueDate),''' '];
            [OutVARProductGroupColNames,OutVARProductGroupData] = Fetch_DB_Data(ObjDB,SqlQuery4);
            OutVARProductGroupData = OutVARProductGroupData(cellStrfind_exact(OutVARProductGroupData(:,1),[Unique_ProductGroupOrder;{'TOTAL'}]),:); %% Change the OrderWise
            
            xlswrite(OutXLSFileName,[OutVARProductColNames ; OutVARProductData],'Product_VAR_Table');
            xlswrite(OutXLSFileName,[OutVARProductGroupColNames ; OutVARProductGroupData],'ProductGroup_VAR_Table');
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' Exposure Report']);
            
            %% PNL MOVE Table
            
            [Subportfolio_Total_Data , OneDay_Before_portfoliogroup_Data , OneWeek_Before_portfoliogroup_Data ,...
                Total_Change_PNL_portfoliogroup_FTW , Total_Change_PNL_portfoliogroup_FTD,...
                PortfolioGroup_Total_Data, OneDay_Before_portfoliowise_Data , OneWeek_Before_portfoliowise_Data , ...
                Total_Change_PNL_portfoliowise_FTW ,  Total_Change_PNL_portfoliowise_FTD ] = AGF_SecondCut_Reports_PnL_Move_Table_Portfolio(OutXLSFileName,InBUName,ColNames_PnL_Move,Data_PnL_Move,User_Select_Portfolio);
            
            cprintf('key','%s finished\n', [char(User_Select_Portfolio),' PNL MOVE Table']);
            
            %% Delete Empty Sheets
            OutXLSFileName = fullfile(pwd,OutXLSFileName);
            try
                xls_delete_sheets(OutXLSFileName);
            catch
            end
            
            %% Restart Automatically
            
            %             !matlab &
            %             exit
            
        end
    else
        warndlg('No Data in DB');
    end
    
    % catch ME
    %     OutErrorMsg = cellstr(ME.message);
    %
    %     errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    %         ME.stack(1).name, ME.stack(1).line, ME.message);
    %     fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

diary off ;

clearvars -global ;




