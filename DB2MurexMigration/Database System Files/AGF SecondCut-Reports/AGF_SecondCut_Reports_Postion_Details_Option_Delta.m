function [OutErrorMsg,Header_Position_Details_OFUT,RowData_Position_Details_OFUT] = AGF_SecondCut_Reports_Postion_Details_Option_Delta(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    ProductCode_Index = strcmpi(ColNames,'product_code');
    ProductCode_Data = Data(:,ProductCode_Index);
    %%
    Unique_ProductCode_Data_Input  = unique(ProductCode_Data);
    
    %     ObjDB = connect_to_database;
    RPO_TableName = [char(InBUName),'_reports_product_order_table'];
    SqlQuery = ['select * from ',char(RPO_TableName),' where product_type in (''COMMODITY'',''FXFUT'')'] ;
    Table_Data  = fetch(ObjDB,SqlQuery);
    
    Table_Data  = sortrows(Table_Data);
    Unique_ProductCode_Data = Table_Data(:,3) ; %% Only FUT and OFUT
    
    Unique_ProductCode_Data = Unique_ProductCode_Data(sort(cellStrfind_exact(Unique_ProductCode_Data,Unique_ProductCode_Data_Input)));
    
    %%
    ContractMonth_Index = strcmpi(ColNames,'contract_month');
    SettleDelta_Index = strcmpi(ColNames,'settle_delta_1');
    GroupType_Index = strcmpi(ColNames,'group_type');
    
    if(~isempty(cellStrfind_exact(unique(Data(:,GroupType_Index)),{'OFUT'})))  %% Check OFUT Only
        
        count = 1;
        Overall_Group_DeltaCount_OFUT = [];
        Maturity_Label = [];
        
        for i = 1 : length(Unique_ProductCode_Data)
            
            Current_ProductCode = Unique_ProductCode_Data{i};
            
            ProductCode = Current_ProductCode ; %% [~,ProductCode]  = strtok(Current_ProductCode);   %% Get Product Code Only  like CM BO & CM C
            Pdt_Code_OFUT{count,1} = strtrim(ProductCode);
            
            Temp_Group_Count = [];
            
            for ii = 1 : length(FieldNames)
                
                CurrentGroup_ProductCode = PortfolioGroup.(FieldNames{ii})(:,ProductCode_Index); %% Get Every Group ProductCode
                
                Matched_ProductCode_Index = find(strcmpi(CurrentGroup_ProductCode,Current_ProductCode));  %% Get Matched Index compare First Loop and Third Loop
                
                CurrentGroup_GroupType = PortfolioGroup.(FieldNames{ii})(Matched_ProductCode_Index,GroupType_Index);
                OFUT_Index = Matched_ProductCode_Index(find(strcmpi('OFUT',CurrentGroup_GroupType)));   %% Get Options Index
                
                CurrentGroup_SettleDelta = PortfolioGroup.(FieldNames{ii})(OFUT_Index,SettleDelta_Index);
                %         CurrentGroup_SettleDelta = CurrentGroup_SettleDelta(OFUT_Index);
                
                CurrentGroup_ContractMonth = PortfolioGroup.(FieldNames{ii})(OFUT_Index,ContractMonth_Index);
                Unique_ContractMonth_Data = unique(CurrentGroup_ContractMonth);
                if(~isempty(Unique_ContractMonth_Data))
                    
                    Group_Count_OFUT = cell(length(Unique_ContractMonth_Data),length(FieldNames));
                    
                    for k = 1 : length(Unique_ContractMonth_Data)
                        
                        Current_ContractMonth = Unique_ContractMonth_Data{k};
                        
                        Matched_ContractMonth_Index = find(strcmpi(CurrentGroup_ContractMonth,Current_ContractMonth));
                        
                        Group_Count_OFUT{k,ii} = sum(cell2mat(CurrentGroup_SettleDelta(Matched_ContractMonth_Index)));
                        
                        count = count +  1;
                    end
                    
                    Maturity_Label = [Maturity_Label ; Unique_ContractMonth_Data];
                    Overall_Group_DeltaCount_OFUT = [Overall_Group_DeltaCount_OFUT;Group_Count_OFUT];
                    
                    Temp_Group_Count = [Temp_Group_Count ; Group_Count_OFUT] ;
                    check = 4 ;  %% For Report
                end
                
            end
            
            if(exist('check','var'))
                Pdt_Code_OFUT{count,1}  = [ProductCode ,' TOTAL'] ;
                Maturity_Label{count,1} = {''} ;
                
                [~,Col_Sum] = cell2sum_Row_Col(Temp_Group_Count) ;
                Overall_Group_DeltaCount_OFUT(count,:) = Col_Sum ;
                
                count = count +  1;
                
                clear check;
                
            else
                Pdt_Code_OFUT(count) = [];
            end
            
        end
        
        % Pdt_Code_OFUT = Pdt_Code_OFUT(1:end-1)';
        MM_FF1_FY = Overall_Group_DeltaCount_OFUT(:,1);
        MM_FF2_FY = Overall_Group_DeltaCount_OFUT(:,2);
        MM_FF3_FY = Overall_Group_DeltaCount_OFUT(:,3);
        MM_FF4_FY = Overall_Group_DeltaCount_OFUT(:,4);
        MM_FF5_FY = Overall_Group_DeltaCount_OFUT(:,5);
        MM_FF6_FY = Overall_Group_DeltaCount_OFUT(:,6);
        
        [Grand_Total_Row,Grand_Total_Col] = cell2sum_Row_Col(Overall_Group_DeltaCount_OFUT);
        
        %%
        Header_Position_Details_OFUT = {'Pdt Code','Maturity Label','MM_FF1_FY','MM_FF2_FY','MM_FF3_FY','MM_FF4_FY','MM_FF5_FY','MM_FF6_FY','Grand Total'};
        RowData_Position_Details_OFUT = [Pdt_Code_OFUT,Maturity_Label,MM_FF1_FY,MM_FF2_FY,MM_FF3_FY,MM_FF4_FY,MM_FF5_FY,MM_FF6_FY,Grand_Total_Row];
        
        % xlswrite(OutXLSFileName,[Header_Position_Details_OFUT;RowData_Position_Details_OFUT],'OPTIONS DELTA');
        
    else
        Header_Position_Details_OFUT = {'No OFUT Data'};
        RowData_Position_Details_OFUT = {' '};
    end
    
catch ME
    Header_Position_Details_OFUT = {'No Data (Error) '};
    RowData_Position_Details_OFUT = {' '};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end
end