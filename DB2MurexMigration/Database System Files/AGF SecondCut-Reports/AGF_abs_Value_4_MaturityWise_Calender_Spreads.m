function  [abs_Value] = AGF_abs_Value_4_MaturityWise_Calender_Spreads(ContractMonth_Index,FUT_Index_Only,Delta_Index,CurrentGroup_ProductData)


if(~isnan(cell2mat(CurrentGroup_ProductData(FUT_Index_Only,ContractMonth_Index))))
    
Unique_ContractMonth = unique(CurrentGroup_ProductData(FUT_Index_Only,ContractMonth_Index));

for i = 1 : length(Unique_ContractMonth)
    
   Current_ContractMonth = Unique_ContractMonth(i);
   Current_CM_Index = cellStrfind_Perfect(CurrentGroup_ProductData(:,ContractMonth_Index),Current_ContractMonth);
   
   
   CurrentGroup_ProductData(Current_CM_Index,Delta_Index) = num2cell(abs(cell2mat(CurrentGroup_ProductData(Current_CM_Index,Delta_Index)))) ;
    
    
end



abs_Value = sum(cell2mat(CurrentGroup_ProductData(FUT_Index_Only,Delta_Index)))./2 ;

else
    
    abs_Value = 0;

end