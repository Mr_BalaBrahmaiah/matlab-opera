function   [Subportfolio_Total_Data , OneDay_Before_portfoliogroup_Data , OneWeek_Before_portfoliogroup_Data ,...
    Total_Change_PNL_portfoliogroup_FTW , Total_Change_PNL_portfoliogroup_FTD,...
    PortfolioGroup_Total_Data, OneDay_Before_portfoliowise_Data , OneWeek_Before_portfoliowise_Data , ...
    Total_Change_PNL_portfoliowise_FTW ,  Total_Change_PNL_portfoliowise_FTD ] = AGF_SecondCut_Reports_PnL_Move_Table_Portfolio(XLS_Name,InBUName,ColNames,Data,User_Selected_Portfolio)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    
    %     ViewName = 'helper_eod_monthend_report_view';
    %     SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    %     [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,char(InBUName));
    
    if ~strcmpi(Data,'No Data')
        
        InData = cell2dataset([ColNames;Data]);
        
        IdxBlankContractMonth = strcmpi('',InData.contract_month);
        InData.contract_month(IdxBlankContractMonth) = InData.maturity_date(IdxBlankContractMonth);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(char(InBUName),InData,0);
        
        UniqueFields = {'PORTFOLIO','PRODUCT','PRODUCT_CODE'};
        SumFields={'Total_USD'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [~,Data] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        ColNames = {'subportfolio','product_name','product_code','mtm_usd'};
    end
    
    %% Summary Report  %% Read 'combined_report_mapping_table'
    
    Subportfolio_Index =  strcmpi(ColNames,'subportfolio');
    Subportfolio_Data = Data(:,Subportfolio_Index);
    
    CRM_TableName = [char(InBUName),'_combined_report_mapping_table'];
    [MappingFieldNames,MappingData] = read_from_database(CRM_TableName,0);
    
    Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
    Portfolio_Data = MappingData(:,Portfolio_Col);
    Unique_Portfolio = unique(Portfolio_Data);
    
    Map_Subportfolio_Index =  strcmpi(MappingFieldNames,'subportfolio');
    Map_Subportfolio_Data = MappingData(:,Map_Subportfolio_Index);
    
    Category_Col = strcmpi(MappingFieldNames,'category');
    Category_Data = MappingData(:,Category_Col);
    %         Unique_Category = unique(Category_Data);
    
    for i = 1 :length(Unique_Portfolio)
        
        Index = find(strcmpi(Portfolio_Data,Unique_Portfolio{i}));
        
        Grouped_Portfolio = Map_Subportfolio_Data(Index)';
        
        CurrVarname = [Unique_Portfolio{i},'_',char(unique(Category_Data(Index)))];
        CurrVarname = strrep(CurrVarname,'-','_');
        CurrVarname = strrep(CurrVarname,' ','_');
        
        RequiredField_Index = cellStrfind(Subportfolio_Data,Grouped_Portfolio);
        PortfolioGroup.(CurrVarname) = Data(RequiredField_Index,:);
        
    end
    
    FieldNames =  fieldnames(PortfolioGroup);
    
    clear Category_Data;
    
    %% Make ProductGroup and PortfolioGroup Information for PortfolioGroup Input Data
    
    ViewName = [char(InBUName),'_combined_report_mapping_table'];
    %     [ColNames1,Data1] = read_from_database(ViewName,0);
    [ColNames1,Data1] = Fetch_DB_Data(ObjDB,[],ViewName);
    DBRefPFGroupData = cell2dataset([ColNames1;Data1]);
    
    ViewName = [char(InBUName),'_product_group_pnl_table'];
    %     [ColNames1,Data1] = read_from_database(ViewName,0);
    [ColNames1,Data1] = Fetch_DB_Data(ObjDB,[],ViewName);
    DBRefPrdtGroupData = cell2dataset([ColNames1;Data1]);
    
    
    for i = 1 : length(FieldNames)
        
        Current_Group_Data = PortfolioGroup.(FieldNames{i});
        
        PortfolioGroup_Data = cell2dataset([ColNames;Current_Group_Data]);
        
        PortfolioGroup_Name = cell(size(PortfolioGroup_Data.subportfolio));
        ProductGroup_Name = cell(size(PortfolioGroup_Data.subportfolio));
        
        for iPF = 1:length(DBRefPFGroupData.subportfolio)
            IdxPF = strcmpi(DBRefPFGroupData.subportfolio{iPF},PortfolioGroup_Data.subportfolio);
            PortfolioGroup_Name(IdxPF) = DBRefPFGroupData.group(iPF);
        end
        
        for iPF = 1:length(DBRefPrdtGroupData.product_code)
            IdxPF = strcmpi(DBRefPrdtGroupData.product_code{iPF},PortfolioGroup_Data.product_code);
            ProductGroup_Name(IdxPF) = DBRefPrdtGroupData.product_group(iPF);
        end
        
        PortfolioGroup_Data.PortfolioGroup = PortfolioGroup_Name;
        PortfolioGroup_Data.ProductGroup = ProductGroup_Name;
        
        PortfolioGroup.(FieldNames{i}) = dataset2cell(PortfolioGroup_Data);
        
    end
    
    
    %%  Fetch Settle Date
    
    SqlQuery = 'select * from valuation_date_table' ;
    %     [~,DateData] = read_from_database([],0,SqlQuery);
    [~,DateData] = Fetch_DB_Data(ObjDB,SqlQuery);
    Current_ValueDate = DateData(2);
    Current_Settlement_Date = DateData(3);
    
    %     HolDates = {'2016-01-01','2017-01-01'};
    %     HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    %     OneDay_Before_Settlement_Date = datestr(busdate(today-3,-1,HolidayVec),'yyyy-mm-dd');
    %     OneWeek_Before_Settlement_Date = datestr(busdate(today-6,-1,HolidayVec),'yyyy-mm-dd');
    
    [OneDay_Before_Settlement_Date, ~, ~] = Get_User_Want_Business_Days([],3);
    [OneWeek_Before_Settlement_Date, ~, ~] = Get_User_Want_Business_Days([],7);
    
    [System_Date_Format,~] = get_System_Date_Format();
    
    %     [OneDay_Before_Settlement_Date, ~, ~] = Get_User_Want_Business_Days_Old(2,Current_ValueDate);
    %     [OneWeek_Before_Settlement_Date, ~, ~] = Get_User_Want_Business_Days_Old(6,Current_ValueDate);
    %     OneDay_Before_Settlement_Date = datestr(datenum(OneDay_Before_Settlement_Date,System_Date_Format),'yyyy-mm-dd');
    %     OneWeek_Before_Settlement_Date = datestr(datenum(OneWeek_Before_Settlement_Date,System_Date_Format),'yyyy-mm-dd');
    
    %%
    for k = 1 : 1
        
        if(k==1)
            Settlement_Date = Current_Settlement_Date ;
        elseif(k==2)
            Settlement_Date = OneDay_Before_Settlement_Date;
        else
            Settlement_Date = OneWeek_Before_Settlement_Date ;
        end
        
        
        %% Get Matching Data
        %         ViewName = ['helper_funds_reports_view_',char(InBUName)];
        %         SqlQuery = ['SELECT * FROM ',char(ViewName),' where settlement_date = ''',char(Settlement_Date),''' ' ] ;
        %         [ColNames,Data] = read_from_database([],0,SqlQuery);
        
        TempOutData = cell2dataset([ColNames;Data]);
        
        %% Make ProductGroup and PortfolioGroup Information for Settlement Date Input Data
        
        PortfolioGroup_Name = cell(size(TempOutData.subportfolio));
        ProductGroup_Name = cell(size(TempOutData.subportfolio));
        
        for iPF = 1:length(DBRefPFGroupData.subportfolio)
            IdxPF = strcmpi(DBRefPFGroupData.subportfolio{iPF},TempOutData.subportfolio);
            PortfolioGroup_Name(IdxPF) = DBRefPFGroupData.group(iPF);
        end
        
        for iPF = 1:length(DBRefPrdtGroupData.product_code)
            IdxPF = strcmpi(DBRefPrdtGroupData.product_code{iPF},TempOutData.product_code);
            ProductGroup_Name(IdxPF) = DBRefPrdtGroupData.product_group(iPF);
        end
        
        TempOutData.PortfolioGroup = PortfolioGroup_Name;
        TempOutData.ProductGroup = ProductGroup_Name;
        
        % TempOutData = dataset2cell(TempOutData);
        
        
        %% Fetch ProductGroup and PortfolioGroup Order Table
        
        %         ObjDB = connect_to_database;
        
        %         SqlQuery = 'select * from agf_reports_portfoliogroup_order_table' ;
        %         Table_Data  = fetch(ObjDB,SqlQuery);
        %         Table_Data  = sortrows(Table_Data);
        %         Unique_PortfolioGroup = Table_Data(:,2)' ;
        Unique_PortfolioGroup = User_Selected_Portfolio ;
        
        RPGO_TableName = [char(InBUName),'_reports_productgroup_order_table'];
        SqlQuery = ['select * from ',char(RPGO_TableName)] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        Table_Data  = sortrows(Table_Data);
        Unique_ProductGroup = Table_Data(:,2) ;
        
        %% Make Subportfolio PNL Report
        
        Subportfolio_Output_Data = cell(size(Unique_ProductGroup,1),size(Unique_PortfolioGroup,2));
        
        for i = 1 : size(Unique_PortfolioGroup,2)
            Current_PortfolioGroup =  Unique_PortfolioGroup(i);
            
            Matched_PortfolioGroup_Index = cellStrfind_exact(TempOutData.PortfolioGroup ,Current_PortfolioGroup) ;
            
            if(~isempty(Matched_PortfolioGroup_Index))
                Matched_PortfolioGroup_Data = TempOutData(Matched_PortfolioGroup_Index,:);
                
                for ii = 1 : size(Unique_ProductGroup,1)
                    Current_ProductGroup = Unique_ProductGroup(ii);
                    
                    Matched_ProductGroup_Index = cellStrfind_exact(Matched_PortfolioGroup_Data.ProductGroup , Current_ProductGroup) ;
                    Matched_ProductPortfolioGroup_Data = Matched_PortfolioGroup_Data(Matched_ProductGroup_Index,:);
                    
                    Subportfolio_Output_Data{ii,i} = sum( Matched_ProductPortfolioGroup_Data.mtm_usd ) ;
                end
                
            end
        end
        
        [Row_Sum,~]  = cell2sum_Row_Col(Subportfolio_Output_Data) ;
        Subportfolio_Output_Data = [Subportfolio_Output_Data , Row_Sum] ;
        [~,Col_Sum]  = cell2sum_Row_Col(Subportfolio_Output_Data) ;
        Subportfolio_Output_Data = [Subportfolio_Output_Data ; Col_Sum] ;
        
        Header_Col = [{'Settlement Date','PRODUCT'},Unique_PortfolioGroup,{'TOTAL'}];
        Header_Row = [Unique_ProductGroup ; {'TOTAL'}];
        
        RowData = [Header_Row , Subportfolio_Output_Data];
        
        Settle_Data = cell(size(RowData,1),1);
        Settle_Data(:,:) = Settlement_Date ;
        Uplodaing_SubportfolioData = [Settle_Data,RowData];         %% upload Data
        
        Subportfolio_Total_Data = [Header_Col ; Uplodaing_SubportfolioData] ;
        
        
        %% Storing Output
        CurrVarname = strrep(char(Settlement_Date),'-','_');
        CurrVarname = strrep(CurrVarname,' ','_');
        CurrVarname = strcat('SettleDate_',CurrVarname);
        
        SubportfolioStruct.(CurrVarname) = Subportfolio_Total_Data;
        
        %% Make PortfolioGroup PNL Report
        
        PortfolioGroup_Output_Data = cell(size(Unique_ProductGroup,1),size(FieldNames,1));
        
        for i = 1 : size(FieldNames,1)
            
            Current_PortfolioGroup_Data = cell2dataset(PortfolioGroup.(FieldNames{i}));
            
            for ii = 1 : size(Unique_ProductGroup,1)
                
                Current_ProductGroup = Unique_ProductGroup(ii);
                
                Matched_ProductGroup_Index = cellStrfind_exact(Current_PortfolioGroup_Data.ProductGroup , Current_ProductGroup) ;
                Matched_ProductPortfolioGroup_Data = Current_PortfolioGroup_Data(Matched_ProductGroup_Index,:);
                
                PortfolioGroup_Output_Data{ii,i} = sum( Matched_ProductPortfolioGroup_Data.mtm_usd ) ;
            end
            
        end
        
        [Row_Sum,~]  = cell2sum_Row_Col(PortfolioGroup_Output_Data) ;
        PortfolioGroup_Output_Data = [PortfolioGroup_Output_Data , Row_Sum] ;
        [~,Col_Sum]  = cell2sum_Row_Col(PortfolioGroup_Output_Data) ;
        PortfolioGroup_Output_Data = [PortfolioGroup_Output_Data ; Col_Sum] ;
        
        Header_Col = [{'Settlement Date','PRODUCT'},FieldNames',{'TOTAL'}];
        Header_Row = [Unique_ProductGroup ; {'TOTAL'}];
        
        RowData = [Header_Row , PortfolioGroup_Output_Data];
        
        Settle_Data = cell(size(RowData,1),1);
        Settle_Data(:,:) = Settlement_Date ;
        Uplodaing_PortfolioGroupData = [Settle_Data,RowData];  %% upload Data
        
        PortfolioGroup_Total_Data = [Header_Col ; Uplodaing_PortfolioGroupData] ;
        
        PortfolioGroupStruct.(CurrVarname) = PortfolioGroup_Total_Data;
        
        %     clear Total_Data RowData Header_Col Header_Row;
        
    end
    
    %% Upload Current Settlement Data to Table
    
    %     TableName = 'agf_productgroup_portfoliogroup_pnl_table';
    %     set(ObjDB,'AutoCommit','off');
    %     SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Current_Settlement_Date),''''];
    %     curs = exec(ObjDB,SqlQuery);
    %     commit(ObjDB);
    %     if ~isempty(curs.Message)
    %         disp(curs.Message);
    %     end
    %     set(ObjDB,'AutoCommit','on');
    %
    %     upload_in_database(TableName, Uplodaing_SubportfolioData);
    %
    %     TableName = 'agf_productgroup_portfoliowise_pnl_table';
    %     set(ObjDB,'AutoCommit','off');
    %     SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Current_Settlement_Date),''''];
    %     curs = exec(ObjDB,SqlQuery);
    %     commit(ObjDB);
    %     if ~isempty(curs.Message)
    %         disp(curs.Message);
    %     end
    %     set(ObjDB,'AutoCommit','on');
    %
    %     upload_in_database(TableName, Uplodaing_PortfolioGroupData);
    
    
    %% Calculate Change in PNL for Subportfolio
    
    ReturnHeader = ['settlement_date',',','product_group',',',[char(lower(User_Selected_Portfolio)),'_pnl']];
    
    %     SqlQuery = ['select * from agf_productgroup_portfoliogroup_pnl_table where settlement_date = ''',OneDay_Before_Settlement_Date,''' '];
    PGPO_TableName = [char(InBUName),'_productgroup_portfoliogroup_pnl_table'];
    SqlQuery = ['select ',ReturnHeader,' from ',char(PGPO_TableName),' where settlement_date = ''',char(OneDay_Before_Settlement_Date),''' '];
    %     [ColNames1,OneDay_Before_portfoliogroup_Data] = read_from_database([],[],SqlQuery);
    [ColNames1,OneDay_Before_portfoliogroup_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    OrderWise_Index = cellStrfind_exact(OneDay_Before_portfoliogroup_Data(:,2),[Unique_ProductGroup ; {'TOTAL'}]);
    OneDay_Before_portfoliogroup_Data = OneDay_Before_portfoliogroup_Data(OrderWise_Index,:);
    
    %     SqlQuery = ['select * from agf_productgroup_portfoliogroup_pnl_table where settlement_date = ''',OneWeek_Before_Settlement_Date,''' '];
    SqlQuery = ['select ',ReturnHeader,' from ',char(PGPO_TableName),' where settlement_date = ''',char(OneWeek_Before_Settlement_Date),''' '];
    %     [ColNames1,OneWeek_Before_portfoliogroup_Data] = read_from_database([],[],SqlQuery);
    [ColNames1,OneWeek_Before_portfoliogroup_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    OrderWise_Index = cellStrfind_exact(OneWeek_Before_portfoliogroup_Data(:,2),[Unique_ProductGroup ; {'TOTAL'}]);
    OneWeek_Before_portfoliogroup_Data = OneWeek_Before_portfoliogroup_Data(OrderWise_Index,:);
    
    Change_PNL_portfoliogroup_FTD = num2cell(cell2mat(Uplodaing_SubportfolioData(:,3)) - cell2mat(OneDay_Before_portfoliogroup_Data(:,3:end))) ; %% Change
    Total_Change_PNL_portfoliogroup_FTD = [Uplodaing_SubportfolioData(:,1:2) , Change_PNL_portfoliogroup_FTD ] ;
    Change_PNL_portfoliogroup_FTW = num2cell(cell2mat(Uplodaing_SubportfolioData(:,3)) - cell2mat(OneWeek_Before_portfoliogroup_Data(:,3:end))) ; %% Change
    Total_Change_PNL_portfoliogroup_FTW = [Uplodaing_SubportfolioData(:,1:2) , Change_PNL_portfoliogroup_FTW ] ;
    
    %% Calculate Change in PNL for PortfolioGroup Wise
    
    PGPW_TableName = [char(InBUName),'_productgroup_portfoliowise_pnl_table'];
    SqlQuery = ['select * from ',char(PGPW_TableName),' where settlement_date = ''',char(OneDay_Before_Settlement_Date),''' '];
    %     [ColNames2,OneDay_Before_portfoliowise_Data] = read_from_database([],[],SqlQuery);
    [ColNames2,OneDay_Before_portfoliowise_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    OrderWise_Index = cellStrfind_exact(OneDay_Before_portfoliowise_Data(:,2),[Unique_ProductGroup ; {'TOTAL'}]);
    OneDay_Before_portfoliowise_Data = OneDay_Before_portfoliowise_Data(OrderWise_Index,:);
    
    SqlQuery = ['select * from ',char(PGPW_TableName),' where settlement_date = ''',char(OneWeek_Before_Settlement_Date),''' '];
    %     [ColNames2,OneWeek_Before_portfoliowise_Data] = read_from_database([],[],SqlQuery);
    [ColNames2,OneWeek_Before_portfoliowise_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    OrderWise_Index = cellStrfind_exact(OneWeek_Before_portfoliowise_Data(:,2),[Unique_ProductGroup ; {'TOTAL'}]);
    OneWeek_Before_portfoliowise_Data = OneWeek_Before_portfoliowise_Data(OrderWise_Index,:);
    
    Change_PNL_portfoliowise_FTD = num2cell(cell2mat(Uplodaing_PortfolioGroupData(:,3:end)) - cell2mat(OneDay_Before_portfoliowise_Data(:,3:end))) ;
    Total_Change_PNL_portfoliowise_FTD = [Uplodaing_PortfolioGroupData(:,1:2) , Change_PNL_portfoliowise_FTD ] ;
    Change_PNL_portfoliowise_FTW = num2cell(cell2mat(Uplodaing_PortfolioGroupData(:,3:end)) - cell2mat(OneWeek_Before_portfoliowise_Data(:,3:end))) ;
    Total_Change_PNL_portfoliowise_FTW = [Uplodaing_PortfolioGroupData(:,1:2) , Change_PNL_portfoliowise_FTW ] ;
    
    %%Assign all Output variables
    OneDay_Before_portfoliogroup_Data = [ColNames1;OneDay_Before_portfoliogroup_Data];
    OneWeek_Before_portfoliogroup_Data = [ColNames1;OneWeek_Before_portfoliogroup_Data];
    Total_Change_PNL_portfoliogroup_FTW = [ColNames1 ; Total_Change_PNL_portfoliogroup_FTW];
    Total_Change_PNL_portfoliogroup_FTD = [ColNames1 ; Total_Change_PNL_portfoliogroup_FTD];
    
    OneDay_Before_portfoliowise_Data = [ColNames2;OneDay_Before_portfoliowise_Data];
    OneWeek_Before_portfoliowise_Data = [ColNames2;OneWeek_Before_portfoliowise_Data];
    Total_Change_PNL_portfoliowise_FTW = [ColNames2 ; Total_Change_PNL_portfoliowise_FTW];
    Total_Change_PNL_portfoliowise_FTD = [ColNames2 ; Total_Change_PNL_portfoliowise_FTD];
    
    %% EXCEL WRITE
    %     XLS_Name = getXLSFilename('Exposure_Change_in_PNL');
    
    xlswrite(XLS_Name , Subportfolio_Total_Data ,'PNL-Today-PortfolioGroup');
    xlswrite(XLS_Name , OneDay_Before_portfoliogroup_Data ,'PNL-Yesterday-PortfolioGroup');
    xlswrite(XLS_Name , OneWeek_Before_portfoliogroup_Data ,'PNL-WeekBefore-PortfolioGroup');
    xlswrite(XLS_Name , Total_Change_PNL_portfoliogroup_FTW ,'FTW-ChangeInPNL-PortfolioGroup');
    xlswrite(XLS_Name , Total_Change_PNL_portfoliogroup_FTD ,'FTD-ChangeInPNL-PortfolioGroup');
    
    %     xlswrite(XLS_Name , PortfolioGroup_Total_Data ,'PNL-Today-PortfolioWise');
    %     xlswrite(XLS_Name , OneDay_Before_portfoliowise_Data ,'PNL-Yesterday-PortfolioWise');
    %     xlswrite(XLS_Name , OneWeek_Before_portfoliowise_Data ,'PNL-WeekBefore-PortfolioWise');
    %     xlswrite(XLS_Name , Total_Change_PNL_portfoliowise_FTW ,'FTW-ChangeInPNL-PortfolioWise');
    %     xlswrite(XLS_Name , Total_Change_PNL_portfoliowise_FTD ,'FTD-ChangeInPNL-PortfolioWise');
    
    %     XLS_Name = fullfile(pwd,XLS_Name);
    %     xls_delete_sheets(XLS_Name) ;
    %     xls_change_activesheet(XLS_Name , 'PNL-Today-PortfolioGroup');
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end





