function [Header_Position_Details_OFUT,RowData_Position_Details_OFUT] = AGF_Future_MonthWise(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

GroupType_Index = strcmpi(ColNames,'group_type');

%%
ProductCode_Index = strcmpi(ColNames,'product_code');
ProductCode_Data = Data(:,ProductCode_Index);
Unique_ProductCode_Data  = unique(ProductCode_Data);   %% For Pdt Code Variable and first Loop

ContractMonth_Index = strcmpi(ColNames,'contract_month'); %% Get Contract Month Data from Source Table
ContractMonth_Data = Data(:,ContractMonth_Index);
Unique_ContractMonth_Data = unique(ContractMonth_Data);  %% Find Unique Contract Month for Second Loop for matching purpose of every group

WithoutEmpty = cellfun(@(s) (length(s)>1), Unique_ContractMonth_Data); %% Remove Empty Index
Unique_ContractMonth_Data = Unique_ContractMonth_Data(WithoutEmpty);

% Maturity_Code = cellfun(@(x) x(end-1:end), Unique_ContractMonth_Data, 'UniformOutput', false); %% Unique For all Product
Maturity_Code = Unique_ContractMonth_Data;

SettleDelta_Index = find(strcmpi(ColNames,'settle_delta_1'));

count = 1;
Maturity = [];
Overall_Group_Count = [];
Grand_Total = [];
GroupColCount = [];

for i = 1 : length(Unique_ProductCode_Data)
    
    Current_ProductCode = Unique_ProductCode_Data{i};
    
    ProductCode = Current_ProductCode ; %% [~,ProductCode]  = strtok(Current_ProductCode);   %% Get Product Code Only  like CM BO & CM C
    Pdt_Code{count} = strtrim(Current_ProductCode);
    
    for k = 1 : length(Unique_ContractMonth_Data)
        
        Current_ContractMonth = Unique_ContractMonth_Data{k};
        %Maturity_Code = cellfun(@(x) x(end-1:end), Unique_ContractMonth_Data, 'UniformOutput', false);
        
        for ii = 1 : length(FieldNames)
            
            CurrentGroup_ProductCode = PortfolioGroup.(FieldNames{ii})(:,ProductCode_Index); %% Get Every Group ProductCode
            
            Matched_ProductCode_Index = find(strcmpi(CurrentGroup_ProductCode,Current_ProductCode));  %% Get Matched Index compare First Loop and Third Loop
            
            CurrentGroup_ContractMonth = PortfolioGroup.(FieldNames{ii})(Matched_ProductCode_Index,ContractMonth_Index); %% Get Contract Month for corresponding ProductCode Matched
            
            %             Matched_ContractMonth = find(strcmpi(CurrentGroup_ContractMonth,Current_ContractMonth));   %% Get Evert Group Matched Contract Month using Loop 2 and 3
            %             Group_Count{ii} = length(Matched_ContractMonth);     %% Length for Counting Corresponding Contract Month
            %             GroupColCount{k,ii} = length(Matched_ContractMonth);    %% For sum of every Columns
            
            Matched_ContractMonth = Matched_ProductCode_Index(find(strcmpi(CurrentGroup_ContractMonth,Current_ContractMonth)));
            
            %% New
            Temp_Matrix = PortfolioGroup.(FieldNames{ii})(Matched_ContractMonth,:);
            OPT_Index = cellStrfind_exact(Temp_Matrix(:,GroupType_Index),{'OFUT'});
            Temp_Matrix(OPT_Index,:) = [];
            Group_Count{ii} = sum(cell2mat(Temp_Matrix(:,SettleDelta_Index)));
            GroupColCount{k,ii} = sum(cell2mat(Temp_Matrix(:,SettleDelta_Index)));
            
            %% Old
            %             Group_Count{ii} = sum(cell2mat(PortfolioGroup.(FieldNames{ii})(Matched_ContractMonth,SettleDelta_Index)));
            %             GroupColCount{k,ii} = sum(cell2mat(PortfolioGroup.(FieldNames{ii})(Matched_ContractMonth,SettleDelta_Index)));
            
        end
        
        Overall_Group_Count = [Overall_Group_Count;Group_Count];
        Grand_Total = [Grand_Total;sum(cell2mat(Group_Count))];
        count = count +  1;
        
    end
    
    Pdt_Code{count} = [strtrim(ProductCode),' Total'];
    
    Maturity = [Maturity;Maturity_Code];
    Maturity{count} = {''};
    
    Overall_Group_ColCount = num2cell(sum(cell2mat(GroupColCount)));
    Overall_Group_Count = [Overall_Group_Count;Overall_Group_ColCount];  %% Append Column wise Total
    
    Grand_Total(count) =  sum(cell2mat(Overall_Group_ColCount)); %% Count Row Wise Total and Append Grand Total Variable
    
    count = count +  1;
    
end

Pdt_Code = Pdt_Code';
MM_FF1_FY = Overall_Group_Count(:,1);
MM_FF2_FY = Overall_Group_Count(:,2);
MM_FF3_FY = Overall_Group_Count(:,3);
MM_FF4_FY = Overall_Group_Count(:,4);
MM_FF5_FY = Overall_Group_Count(:,5);
MM_FF6_FY = Overall_Group_Count(:,6);

Grand_Total = num2cell(Grand_Total);

Header_Position_Details = {'Pdt Code','Maturity','MM_FF1_FY','MM_FF2_FY','MM_FF3_FY','MM_FF4_FY','MM_FF5_FY','MM_FF6_FY','Grand Total'};
RowData_Position_Details = [Pdt_Code,Maturity,MM_FF1_FY,MM_FF2_FY,MM_FF3_FY,MM_FF4_FY,MM_FF5_FY,MM_FF6_FY,Grand_Total];

%% Remove Grand Total Zero Rows

% Temp = RowData_Position_Details(:,3:7);
% [r,~] = cell2sum_Row_Col(Temp);
% Zero_Index = cellfun(@(x) find(x==0), r, 'UniformOutput', false);
% [Zero_Row_Index] = find(cell2num_logical(Zero_Index));

aa = cellfun(@(s) (length(s)<1), RowData_Position_Details(:,1));
bb = cell2num_logical( cellfun(@(x) find(x==0), RowData_Position_Details(:,8), 'UniformOutput', false) );
Empty_Prod_and_Grand_Total = find((aa & bb));
RowData_Position_Details(Empty_Prod_and_Grand_Total,:) = [];               %% Remove Grand Total Zero Rows

%%

% xlswrite(OutXLSFileName,[Header_Position_Details;RowData_Position_Details],'DELTA REPORT');

clear MM_FF1_FY MM_FF2_FY MM_FF3_FY MM_FF4_FY MM_FF5_FY MM_FF6_FY;

end