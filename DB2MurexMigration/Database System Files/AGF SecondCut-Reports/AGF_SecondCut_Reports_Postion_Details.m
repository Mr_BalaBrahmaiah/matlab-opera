function [OutErrorMsg,Header_Position_Details,RowData_Position_Details] = AGF_SecondCut_Reports_Postion_Details(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    if(~isempty(Data))
        %% No Need FXD Product
        GroupType_Index = strcmpi(ColNames,'group_type');
        FXD_Index = cellStrfind_exact(Data(:,GroupType_Index),{'FXD'}) ;
        
        Data(FXD_Index,:) = [];
        
        %%
        ProductCode_Index = strcmpi(ColNames,'product_code');
        ProductCode_Data = Data(:,ProductCode_Index);
        
        %%
        Unique_ProductCode_Data_Input  = unique(ProductCode_Data);   %% For Pdt Code Variable and first Loop
        
        %     ObjDB = connect_to_database;
        RPO_TableName = [char(InBUName),'_reports_product_order_table'];
        SqlQuery = ['select * from ',char(RPO_TableName),' where product_type in (''COMMODITY'',''FXFUT'')'] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        
        Table_Data  = sortrows(Table_Data);
        Unique_ProductCode_Data = Table_Data(:,3) ; %% Only FUT and OFUT
        
        
        misMatch = setdiff(Unique_ProductCode_Data,Unique_ProductCode_Data_Input);
        misMatch_index = cellStrfind_exact(Unique_ProductCode_Data,misMatch);
        Unique_ProductCode_Data(misMatch_index) = [];
        
        %%
        
        ContractMonth_Index = strcmpi(ColNames,'contract_month'); %% Get Contract Month Data from Source Table
        %     ContractMonth_Data = Data(:,ContractMonth_Index);
        %     Unique_ContractMonth_Data = unique(ContractMonth_Data);  %% Find Unique Contract Month for Second Loop for matching purpose of every group
        %
        %     WithoutEmpty = cellfun(@(s) (length(s)>1), Unique_ContractMonth_Data); %% Remove Empty Index
        %     Unique_ContractMonth_Data = Unique_ContractMonth_Data(WithoutEmpty);
        %     Maturity_Code = Unique_ContractMonth_Data;
        
        %%
        SettleDelta_Index = find(strcmpi(ColNames,'settle_delta_1'));
        
        count = 1;
        Maturity = [];
        Overall_Group_Count = [];
        Grand_Total = [];
        GroupColCount = [];
        
        for i = 1 : length(Unique_ProductCode_Data)
            
            Current_ProductCode = Unique_ProductCode_Data{i};
            
            ProductCode = Current_ProductCode ; %% [~,ProductCode]  = strtok(Current_ProductCode);   %% Get Product Code Only  like CM BO & CM C
            Pdt_Code{count,1} = strtrim(Current_ProductCode);
            
            Matched_Product_Code = cellStrfind_exact(ProductCode_Data , {ProductCode}); %% New Change Contract Month
            ContractMonth_Data = Data(Matched_Product_Code,ContractMonth_Index);
            Unique_ContractMonth_Data = unique(ContractMonth_Data);
            
            Maturity_Code = Unique_ContractMonth_Data;
            
            Temp_Array  =  cell(size(Unique_ContractMonth_Data,1),size(FieldNames,1));
            
            for k = 1 : length(Unique_ContractMonth_Data)
                
                Current_ContractMonth = Unique_ContractMonth_Data{k};
                
                Maturity{count,1} = Current_ContractMonth ;
                
                for ii = 1 : length(FieldNames)
                    
                    CurrentGroup_ProductCode = PortfolioGroup.(FieldNames{ii})(:,ProductCode_Index); %% Get Every Group ProductCode
                    
                    Matched_ProductCode_Index = find(strcmpi(CurrentGroup_ProductCode,Current_ProductCode));  %% Get Matched Index compare First Loop and Third Loop
                    
                    CurrentGroup_ContractMonth = PortfolioGroup.(FieldNames{ii})(Matched_ProductCode_Index,ContractMonth_Index); %% Get Contract Month for corresponding ProductCode Matched
                    
                    Matched_ContractMonth = Matched_ProductCode_Index(find(strcmpi(CurrentGroup_ContractMonth,Current_ContractMonth)));
                    
                    %                 aa = PortfolioGroup.(FieldNames{ii})(Matched_ContractMonth,:) ; %% For Verification
                    
                    Temp_Array(k,ii) = num2cell( sum(cell2mat(PortfolioGroup.(FieldNames{ii})(Matched_ContractMonth,SettleDelta_Index))) );
                    
                    
                end
                
                count = count +  1;
                
            end
            
            Pdt_Code{count,1} = [strtrim(ProductCode),' Total'];
            Maturity{count,1} = {''};
            
            [~,Col_Sum] = cell2sum_Row_Col(Temp_Array);
            Temp_Array = [Temp_Array ; Col_Sum ];
            
            Overall_Group_Count = [Overall_Group_Count ; Temp_Array] ;
            
            count = count +  1;
            
        end
        
        MM_FF1_FY = Overall_Group_Count(:,1);
        MM_FF2_FY = Overall_Group_Count(:,2);
        MM_FF3_FY = Overall_Group_Count(:,3);
        MM_FF4_FY = Overall_Group_Count(:,4);
        MM_FF5_FY = Overall_Group_Count(:,5);
        MM_FF6_FY = Overall_Group_Count(:,6);
        
        [Grand_Total,~] = cell2sum_Row_Col(Overall_Group_Count);
        
        Header_Position_Details = {'Pdt Code','Maturity','MM_FF1_FY','MM_FF2_FY','MM_FF3_FY','MM_FF4_FY','MM_FF5_FY','MM_FF6_FY','Grand Total'};
        RowData_Position_Details = [Pdt_Code,Maturity,MM_FF1_FY,MM_FF2_FY,MM_FF3_FY,MM_FF4_FY,MM_FF5_FY,MM_FF6_FY,Grand_Total];
        
        %% Remove Grand Total Zero Rows
        
        % Temp = RowData_Position_Details(:,3:7);
        % [r,~] = cell2sum_Row_Col(Temp);
        % Zero_Index = cellfun(@(x) find(x==0), r, 'UniformOutput', false);
        % [Zero_Row_Index] = find(cell2num_logical(Zero_Index));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     aa = cellfun(@(s) (length(s)<1), RowData_Position_Details(:,1));
        %     bb = cell2num_logical( cellfun(@(x) find(x==0), RowData_Position_Details(:,8), 'UniformOutput', false) );
        %     Empty_Prod_and_Grand_Total = find((aa & bb));
        %     RowData_Position_Details(Empty_Prod_and_Grand_Total,:) = [];               %% Remove Grand Total Zero Rows
        
        %%
        
        % xlswrite(OutXLSFileName,[Header_Position_Details;RowData_Position_Details],'DELTA REPORT');
        
        clear MM_FF1_FY MM_FF2_FY MM_FF3_FY MM_FF4_FY MM_FF5_FY;
        
    else
        Header_Position_Details = {'No Data'};
        RowData_Position_Details = {' '};
    end
    
catch ME
    Header_Position_Details = {'No Data'};
    RowData_Position_Details = {'Error'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end