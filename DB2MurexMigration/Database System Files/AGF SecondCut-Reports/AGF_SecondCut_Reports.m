function [OutErrorMsg,OutXLSFileName] = AGF_SecondCut_Reports(InBUName)

OutErrorMsg = {'No errors'};
OutXLSFileName = '';

global ObjDB;
ObjDB = connect_to_database;

%% Fetch Value Date & Settlement Date

SqlQuery = 'select * from valuation_date_table' ;
[~,DateData] = Fetch_DB_Data(ObjDB,SqlQuery);
Current_ValueDate = DateData(2);
Current_Settlement_Date = DateData(3);

TextFile = strcat('LogFile_',char(Current_ValueDate),'_',char(strrep(datestr(now,'HH:MM:SS'),':','_')),'.txt');
diary(TextFile) ;

%%

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    %     [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    ViewName = [ViewName,'_',InBUName];
    [ColNames,Data] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    if ~strcmpi(Data,'No Data')
        
        %% Remove CASH Trades
        %TODO - Add the code : find product_code containing the string
        %"CASH" and remove those lines
        
        ProductCode_Index = strcmpi(ColNames,'product_code');
        ProductCode_Data = Data(:,ProductCode_Index);
        CASH_Index = cellStrfind(ProductCode_Data,{'CASH'});
        
        Data(CASH_Index,:) = [];
        
        %% Remove Dead Fileds
        
        %         ColNames = [ColNames,'deal_status'];
        Pos_Maturity = strcmpi('maturity_date',ColNames);
        Pos_ValueDate = strcmpi('value_date',ColNames);
        PosNettingStatus = strcmpi('netting_status',ColNames);
        
        IdxNetted = strcmpi('fully_netted',Data(:,PosNettingStatus));
        MaturityDate = datenum(Data(:,Pos_Maturity),'yyyy-mm-dd');
        ValueDate = datenum(Data(:,Pos_ValueDate),'yyyy-mm-dd');
        IdxDeadOpt = (ValueDate > MaturityDate) | IdxNetted;
        
        Data(IdxDeadOpt,:) = [];
        
        
        %% Summary Report  %% Read 'combined_report_mapping_table'
        Subportfolio_Index =  strcmpi(ColNames,'subportfolio');
        Subportfolio_Data = Data(:,Subportfolio_Index);
        
        %         [MappingFieldNames,MappingData] = read_from_database('agf_combined_report_mapping_table',0);
        CMP_TableName = [char(InBUName),'_combined_report_mapping_table'];
        [MappingFieldNames,MappingData] = Fetch_DB_Data(ObjDB,[],CMP_TableName);
        
        Portfolio_Col = strcmpi(MappingFieldNames,'portfolio');
        Portfolio_Data = MappingData(:,Portfolio_Col);
        Unique_Portfolio = unique(Portfolio_Data);
        
        Map_Subportfolio_Index =  strcmpi(MappingFieldNames,'subportfolio');
        Map_Subportfolio_Data = MappingData(:,Map_Subportfolio_Index);
        
        Category_Col = strcmpi(MappingFieldNames,'category');
        Category_Data = MappingData(:,Category_Col);
        %         Unique_Category = unique(Category_Data);
        
        for i = 1 :length(Unique_Portfolio)
            
            Index = find(strcmpi(Portfolio_Data,Unique_Portfolio{i}));
            
            Grouped_Portfolio = Map_Subportfolio_Data(Index)';
            
            CurrVarname = [Unique_Portfolio{i},'_',char(unique(Category_Data(Index)))];
            CurrVarname = strrep(CurrVarname,'-','_');
            CurrVarname = strrep(CurrVarname,' ','_');
            
            RequiredField_Index = cellStrfind_exact(Subportfolio_Data,Grouped_Portfolio); %% cellStrfind
            PortfolioGroup.(CurrVarname) = Data(RequiredField_Index,:);
            
        end
        
        FieldNames =  fieldnames(PortfolioGroup);    %% For find the below for loop Length
        
        BV_Cumulative = cell(length(FieldNames),1);  %% Initialize the Variable
        BV_Cumulative(:,:) = {''};
        BV_YTD = cell(length(FieldNames),1);
        BV_YTD(:,:) = {''};
        
        for i = 1 : length(FieldNames)
            
            CurrentData = PortfolioGroup.(FieldNames{i});
            
            Cumulative_Index =  strcmpi(ColNames,'cumulative_mtm_usd');
            MtmUsd_Index = strcmpi(ColNames,'mtm_usd');
            
            BV_Cumulative{i}  = sum(cell2mat(CurrentData(:,Cumulative_Index)));
            BV_YTD{i} = sum(cell2mat(CurrentData(:,MtmUsd_Index)));
            
        end
        
        %% Debugging
        %         [OutErrorMsg, Header , Total_Data] = AGF_SecondCut_Reports_Options_Position(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        %   OutXLSFileName = getXLSFilename('AGF-Report Format');
        %   AGF_SecondCut_Reports_PnL_Move_Table()
        
        %% PNP Report
        
        OutXLSFileName = getXLSFilename([upper(char(InBUName)),'-Report Format']);
        
        [OutErrorMsg,BookValue_Header,BookValue_RowData,Delta_Header,Delta_RowData] = AGF_SecondCut_Reports_PNP(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);  %% AGF_SecondCut_Reports_PNP
        
        xlswrite(OutXLSFileName,[BookValue_Header;BookValue_RowData],'pnp_bv_report');
        xlswrite(OutXLSFileName,[Delta_Header;Delta_RowData],'pnp_delta_report');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' PNP Report']);
        
        %% PRODUCTWISE & PORTFOLIOWISE COMBINED PNL REPORT
        
        [OutErrorMsg,CommonHeader , Total_Data] = AGF_SecondCut_Reports_ProductWise_Change_PNL(InBUName) ;
        if(~strcmpi(OutErrorMsg,'No errors'))
            OutErrorMsg = {'Error in PnL Change Sheet'};
            return;
        end
        xlswrite(OutXLSFileName,[CommonHeader ; Total_Data],'pnl_change') ;
        
        [OutErrorMsg,Header_1 , Summary_RowData_1 , Header_2 , Summary_RowData_2] = AGF_SecondCut_Reports_PortfolioWise_Combined_PNL(InBUName) ;
        xlswrite(OutXLSFileName,[Header_1 ; Summary_RowData_1],'Summary') ;
        %  xlswrite(OutXLSFileName,[Header_2 ; Summary_RowData_2],' PNP Summary Report') ; %% No Need
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' pnl_change & Summary']);
        
        %% POSITION DETAILS  (DELTA REPORT)
        
        [OutErrorMsg,Header_Position_Details,RowData_Position_Details] = AGF_SecondCut_Reports_Postion_Details(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        xlswrite(OutXLSFileName,[Header_Position_Details;RowData_Position_Details],'delta_report');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' delta_report']);
        
        %% POSITION DETAILS OPTIONS DELTA REPORT
        
        [OutErrorMsg,Header_Position_Details_OFUT,RowData_Position_Details_OFUT] = AGF_SecondCut_Reports_Postion_Details_Option_Delta(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header_Position_Details_OFUT;RowData_Position_Details_OFUT],'options_delta');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' options_delta']);
        
        %% FUTURES POSITION - MONTH WISE
        
        % [Header_Position_Details_FUT,RowData_Position_Details_FUT] = AGF_SecondCut_Reports_Future_Position_MonthWise(ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        [OutErrorMsg,Header_Position_Details_FUT,RowData_Position_Details_FUT] = AGF_SecondCut_Reports_Future_Position_MonthWise_1(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header_Position_Details_FUT;RowData_Position_Details_FUT],'future_position_monthwise');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' future_position_monthwise']);
        
        %% POSITION DETAILS Combined DELTA REPORT
        
        [OutErrorMsg, Header , FUT_MonthWise_Final] = AGF_SecondCut_Reports_Combined_Delta(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header;FUT_MonthWise_Final],'combined_delta_report');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' combined_delta_report']);
        
        %% OPTIONS POSITIONS REPORT
        
        [OutErrorMsg, Header , Total_Data] = AGF_SecondCut_Reports_Options_Position(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header;Total_Data],'options_positions');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' options_positions']);
        
        %% OPTIONS POSITIONS WITH GREEKS REPORT
        
        [OutErrorMsg,Header , Row_Data] = AGF_SecondCut_Reports_Options_Position_1(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header;Row_Data],'options_positions_greeks');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' options_positions_greeks']);
        
        %% FXD REPORT
        
        [OutErrorMsg, Header , Row_Data] = AGF_SecondCut_Reports_FXD(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName);
        
        xlswrite(OutXLSFileName,[Header;Row_Data],'fxd_report');
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' fxd_report']);
        
        %% NPV Report
        
        [OutErrorMsg,OutNPVProductData,OutNPVGroupData]  = AGF_SecondCut_Reports_NetPosition_Value(InBUName);
        xlswrite(OutXLSFileName, OutNPVProductData ,'NET POSITION VALUE-PRODUCT');
        xlswrite(OutXLSFileName, OutNPVGroupData ,'NET POSITION VALUE-GROUP');
        
        TableName_1 = [char(InBUName),'_netpositionvalue_product_table'];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName_1,' where value_date = ''',char(Current_ValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        aa = OutNPVProductData(2:end,:);
        Temp_ValueDate = cell(size(aa,1),1);
        Temp_ValueDate(:,:) = Current_ValueDate;
        upload_in_database(TableName_1, [Temp_ValueDate,aa]);
        
        TableName_2 = [char(InBUName),'_netpositionvalue_productgroup_table'];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName_2,' where value_date = ''',char(Current_ValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        bb = OutNPVGroupData(2:end,:);
        Temp_ValueDate = cell(size(bb,1),1);
        Temp_ValueDate(:,:) = Current_ValueDate;
        upload_in_database(TableName_2, [Temp_ValueDate,bb]);
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' NPV Report']);
        
        %% Exposure Report
        
        [OutVARProductData , OutVARProductGroupData] = AGF_SecondCut_Reports_Exposure(InBUName);
        xlswrite(OutXLSFileName,OutVARProductData,'Product_VAR_Table');
        xlswrite(OutXLSFileName,OutVARProductGroupData,'ProductGroup_VAR_Table');
        
        TableName_3 = [char(InBUName),'_var_product_table'];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName_3,' where value_date = ''',char(Current_ValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        aa = OutVARProductData(2:end,:);
        Temp_ValueDate = cell(size(aa,1),1);
        Temp_ValueDate(:,:) = Current_ValueDate;
        upload_in_database(TableName_3, [Temp_ValueDate,aa]);
        
        TableName_4 = [char(InBUName),'_var_productgroup_table'];
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName_4,' where value_date = ''',char(Current_ValueDate),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        bb = OutVARProductGroupData(2:end,:);
        Temp_ValueDate = cell(size(bb,1),1);
        Temp_ValueDate(:,:) = Current_ValueDate;
        upload_in_database(TableName_4, [Temp_ValueDate,bb]);
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' Exposure Report']);
        
        %% PNL MOVE Table
        
        [Subportfolio_Total_Data , OneDay_Before_portfoliogroup_Data , OneWeek_Before_portfoliogroup_Data ,...
            Total_Change_PNL_portfoliogroup_FTW , Total_Change_PNL_portfoliogroup_FTD,...
            PortfolioGroup_Total_Data, OneDay_Before_portfoliowise_Data , OneWeek_Before_portfoliowise_Data , ...
            Total_Change_PNL_portfoliowise_FTW ,  Total_Change_PNL_portfoliowise_FTD ] = AGF_SecondCut_Reports_PnL_Move_Table(OutXLSFileName,InBUName);
        
        cprintf('key','%s finished\n', [upper(char(InBUName)),' PNL MOVE Table']);
        
        %% Delete Empty Sheets
        OutFileName = fullfile(pwd,OutXLSFileName);
        try
            xls_delete_sheets(OutFileName);
        catch
        end
        
    else
        warndlg('No Data in DB');
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %         uiwait(warndlg(errorMessage));
end

diary off ;

clearvars -global ;


