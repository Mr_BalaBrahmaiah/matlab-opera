function [OutErrorMsg,Header_1 , Summary_RowData_1 , Header_2 , Summary_RowData_2] = AGF_SecondCut_Reports_PortfolioWise_Combined_PNL(InBUName)
%%

OutErrorMsg = {'No errors'};
global ObjDB;

try
    %     [MappingFieldNames,MappingData] = read_from_database('agf_combined_report_mapping_table',0);
    CMP_TableName = [char(InBUName),'_combined_report_mapping_table'];
    [MappingFieldNames,MappingData] = Fetch_DB_Data(ObjDB,[],CMP_TableName);
    
    Portfolio_Col = cellStrfind_exact(MappingFieldNames,{'portfolio'});
    Category_Col = cellStrfind_exact(MappingFieldNames,{'category'});
    
    Unique_Category_Data = unique(strcat(MappingData(:,Portfolio_Col),{'_'},MappingData(:,Category_Col)));
    
    Unique_Category_Data = [Unique_Category_Data ; {'GROSS BOOK VALUE'} ];
    
    
    %% Get Value Date  and settle Date
    %     ObjDB = connect_to_database ;
    DBValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
    
    DBSettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
    
    %% Get portfoliowise_combined_pnl_table
    %     SqlQuery = 'select * from agf_portfoliowise_combined_pnl_table';
    %     [ColNames,Data] = read_from_database('agf_portfoliowise_combined_pnl_table',0);
    PWC_pnl_TableName = [char(InBUName),'_portfoliowise_combined_pnl_table'];
    [ColNames,Data] = Fetch_DB_Data(ObjDB,[],PWC_pnl_TableName);
    
    if ~strcmpi(Data,'No Data')
        %%
        ValueDate_Col = cellStrfind_exact(ColNames,{'value_date'}) ;
        Cumulative_Col = cellStrfind_exact(ColNames,{'cumulative_bv'}) ;
        YTD_BV_Col = cellStrfind_exact(ColNames,{'ytd_bv'}) ;
        
        Portfolio_Col = cellStrfind_exact(ColNames,{'portfolio'}) ;
        Unique_Portfolio = unique(Data(:,Portfolio_Col)) ;
        
        %%
        % for i = 1 : length(Unique_Portfolio)
        
        ValueDate_Index = cellStrfind_exact(Data(:,ValueDate_Col),{DBValueDate}) ;
        Today_BV_Cumulative = Data (ValueDate_Index,Cumulative_Col);
        Today_YTD = Data (ValueDate_Index,YTD_BV_Col) ;
        
        SettleDate_Index = cellStrfind_exact(Data(:,ValueDate_Col),{DBSettlementDate}) ;
        YesDay_BV_Cumulative = Data (SettleDate_Index,Cumulative_Col);
        YesDay_YTD = Data (SettleDate_Index,YTD_BV_Col) ;
        
        PNL_For_ValueDate = num2cell(cell2mat(Today_BV_Cumulative) - cell2mat(YesDay_BV_Cumulative)) ;
        
        Summary_RowData_1 = [Unique_Category_Data , Today_BV_Cumulative , Today_YTD , PNL_For_ValueDate];
        Summary_RowData_2 = [Unique_Category_Data , YesDay_YTD , Today_YTD ] ;
        
        % end
        
        %% Make Header
        Header_1 = {'Portfolio','BV_Cumulative','BV_YTD','P&L for the DAY'} ;
        Header_2 = {'Portfolio', 'Yesterday''s BV' ,'Today''s BV' } ;
        
        %% Excel Write
        % XLSFileName_1 = getXLSFilename('Portfoliowise_Combined_PNL_report');
        %
        % xlswrite(XLSFileName_1,[Header_1 ; Summary_RowData_1],'Summary Report') ;
        %
        % XLSFileName_2 = getXLSFilename('PNP_Summary_Report');
        % xlswrite(XLSFileName_2,[Header_2 ; Summary_RowData_2],' PNP Summary Report') ;
        %
        % XLSFileName_1 = fullfile(pwd,XLSFileName_1);
        % XLSFileName_2 = fullfile(pwd,XLSFileName_2);
        %
        % try
        %     xls_delete_sheets(XLSFileName_1);
        %     xls_delete_sheets(XLSFileName_2);
        % catch
        % end
        
    else
        warndlg(['No Data in ',char(PWC_pnl_TableName),' DB']);
    end
    
catch ME
    Header_1 = {'No Data'};
    Summary_RowData_1 = {'No Data'};
    Header_2 = {'No Data'};
    Summary_RowData_2 = {'No Data'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end
