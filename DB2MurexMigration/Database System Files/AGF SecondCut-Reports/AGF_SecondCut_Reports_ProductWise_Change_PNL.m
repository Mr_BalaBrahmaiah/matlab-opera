function [OutErrorMsg,CommonHeader , Total_Data] = AGF_SecondCut_Reports_ProductWise_Change_PNL(InBUName)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    
    %% Get Value Date  and settle Date
%     ObjDB = connect_to_database ;
    DBValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
    
    DBSettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
    
    %% Get AGF Productwise PNL Table
    PWC_pnl_TableName = [char(InBUName),'_productwise_combined_pnl_table'];
    Today_SQL_Query = ['select * from ',char(PWC_pnl_TableName),' where value_date = ', ['''' DBValueDate ''''] ];
%     [TodayHeader,Today_Data]  = read_from_database('agf_productwise_combined_pnl_table',0,Today_SQL_Query);
    [TodayHeader,Today_Data] = Fetch_DB_Data(ObjDB,Today_SQL_Query);
    
    YesterDay_SQL_Query = ['select * from ',char(PWC_pnl_TableName),' where value_date = ', ['''' DBSettlementDate ''''] ];
%     [YesDayHeader,YesDay_Data]  = read_from_database('agf_productwise_combined_pnl_table',0,YesterDay_SQL_Query);
    [YesDayHeader,YesDay_Data] = Fetch_DB_Data(ObjDB,YesterDay_SQL_Query);
    
    %%
    Product_Col = cellStrfind_exact(TodayHeader,{'product'});
    % Product_Data = Today_Data(:,Product_Col);
    
    Type_Col = cellStrfind_exact(TodayHeader,{'type'});
    Unique_Type = unique(Today_Data(:,Type_Col)) ;
    
    % Calculation_Header = cellStrfind_exact(TodayHeader,{'total_bv','ff1_fut_pnl','ff1_opt_pnl','ff1_total_pnl','ff2_fut_pnl','ff2_opt_pnl','ff2_total_pnl',...
    %    'ff3_fut_pnl','ff
    
    Calculation_Header = 4:22 ;
    
    %%
    for ii = 1 : length(Unique_Type)
        
        Current_Type = Unique_Type(ii) ;
        
        Matched_Type_Index_Today = cellStrfind_exact(Today_Data(:,Type_Col),Current_Type) ;
        Matched_Type_Index_YesDay = cellStrfind_exact(YesDay_Data(:,Type_Col),Current_Type);
        
        Today_CurrentType_Data = Today_Data(Matched_Type_Index_Today,:) ;
        YesDay_CurrentType_Data = YesDay_Data(Matched_Type_Index_YesDay,:);
        
        Row_Data = [];
        
        for i = 1 : size(Today_CurrentType_Data,1)
            
            Current_Product =  Today_CurrentType_Data(i,Product_Col);
            Today_CurrentProduct_Row  = cellStrfind_exact(Today_CurrentType_Data(:,Product_Col),Current_Product);
            YesDay_CurrentProduct_Row  = cellStrfind_exact(YesDay_CurrentType_Data(:,Product_Col),Current_Product);
            
            if(isempty(YesDay_CurrentProduct_Row))
                Change_PNL = num2cell(cell2mat(Today_CurrentType_Data(Today_CurrentProduct_Row,Calculation_Header)) );
            else
                Change_PNL = num2cell(cell2mat(Today_CurrentType_Data(Today_CurrentProduct_Row,Calculation_Header)) - cell2mat(YesDay_CurrentType_Data(YesDay_CurrentProduct_Row,Calculation_Header)));
            end
            
            Temp_Data = [Current_Product , Change_PNL] ;
            
            Row_Data = [Row_Data ; Temp_Data] ;
        end
        
        Current_Type = char(Current_Type);
        Current_Type = strrep(Current_Type,'-','_');
        Current_Type = strrep(Current_Type,' ','_');
        Change_PNL_Group.(Current_Type)  = Row_Data ;
        
    end
    
    %% Make Empty Rows For Report
    FX_FUT_Trades = cell(1 , size(Change_PNL_Group.COMMODITY,2)) ;
    FX_FUT_Trades{1,1} = 'FXFUT TRADES' ;
    CASHFLOW_Trades = cell(1 , size(Change_PNL_Group.COMMODITY,2)) ;
    CASHFLOW_Trades{1,1} = 'CASHFLOW TRADES' ;
    FXD_Trades = cell(1 , size(Change_PNL_Group.COMMODITY,2)) ;
    FXD_Trades {1,1} = 'FXD TRADES' ;
    
    % Total_Data = [Change_PNL_Group.COMMODITY ; Change_PNL_Group.COMMODITY_TOTAL ; FX_FUT_Trades ; Change_PNL_Group.FXFUT ; FXD_Trades ; Change_PNL_Group.FXD ; ...
    %     CASHFLOW_Trades ; Change_PNL_Group.CASHFLOW ; Change_PNL_Group.TOTAL] ;
    
    %% Make Data for Product Order Wise
    
%     ObjDB = connect_to_database;
    RPO_TableName = [char(InBUName),'_reports_product_order_table'];
    SqlQuery = ['select * from ',char(RPO_TableName)] ;
    Table_Data  = fetch(ObjDB,SqlQuery);
    
    Table_Data  = sortrows(Table_Data);
    Unique_ProductName = Table_Data(:,2) ;
    
    Matched_Index_Order = cellStrfind_exact(Change_PNL_Group.COMMODITY(:,1),Unique_ProductName) ;
    Change_PNL_Group.COMMODITY = Change_PNL_Group.COMMODITY(Matched_Index_Order,:) ;
    
    Matched_Index_Order = cellStrfind_exact(Change_PNL_Group.FXFUT(:,1),Unique_ProductName) ;
    Change_PNL_Group.FXFUT = Change_PNL_Group.FXFUT(Matched_Index_Order,:) ;
    
    Matched_Index_Order = cellStrfind_exact(Change_PNL_Group.FXD(:,1),Unique_ProductName) ;
    Change_PNL_Group.FXD = Change_PNL_Group.FXD(Matched_Index_Order,:) ;
    
    Matched_Index_Order = cellStrfind_exact(Change_PNL_Group.CASHFLOW(:,1),Unique_ProductName) ;
    Change_PNL_Group.CASHFLOW = Change_PNL_Group.CASHFLOW(Matched_Index_Order,:) ;
    
    
    % for i = 1 : length(Unique_ProductName)  %% IF Needed Use For loop to make orderwise
    %    Current_ProdName = Unique_ProductName(i) ;
    %
    %
    % end
    
    
    %%
    CommonHeader = [{'','','DIRECTIONAL','','','CALENDAR_SPREADS','','','PRODUCT_SPREADS',...
        '','','STRATEGIES','','','VOLATILITY','','','CHINA','',''};...
        {'PRODUCT','TOTAL BV','MM_FF1_OIL','','',...
        'MM_FF2_OIL','','',...
        'MM_FF3_OIL','','',...
        'MM_FF4_OIL','','',...
        'MM_FF5_OIL','','',...
        'MM_FF6_OIL','',''};...
        {'','','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL','FUT','OPT','TOTAL'}];
    
    Total_Data = [Change_PNL_Group.COMMODITY ; Change_PNL_Group.COMMODITY_TOTAL ; FX_FUT_Trades ; Change_PNL_Group.FXFUT ; FXD_Trades ; Change_PNL_Group.FXD ; ...
        CASHFLOW_Trades ; Change_PNL_Group.CASHFLOW ; Change_PNL_Group.TOTAL] ;
    
    
    
    %% Excel Write
    % XLSFileName = getXLSFilename('Productwise_Combined_PNL_report');
    % % xlswrite(XLSFileName,[CommonHeader ; Total_Data],'Productwise_Combined_PNL') ;
    %
    % XLSFileName = fullfile(pwd,XLSFileName);
    % try
    %     xls_delete_sheets(XLSFileName);
    % catch
    % end
    
catch ME
    CommonHeader = {'No Data'};
    Total_Data = {'No Data'};
    
    OutErrorMsg = cellstr(ME.message);
    %     continue;
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
%     uiwait(warndlg(errorMessage));
end

end

