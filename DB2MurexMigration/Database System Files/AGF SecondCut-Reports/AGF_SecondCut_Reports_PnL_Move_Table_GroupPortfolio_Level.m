function AGF_SecondCut_Reports_PnL_Move_Table_GroupPortfolio_Level(InBUName,ColNames,Data,PortfolioGroup,FieldNames)
%%  Fetch Settle Date
ObjDB = connect_to_database;
SqlQuery = 'select settlement_date from valuation_date_table ' ;
% [~,Current_Settlement_Date] = read_from_database([],0,SqlQuery);
[~,Current_Settlement_Date] = Fetch_DB_Data(ObjDB,SqlQuery) ;

HolDates = {'2016-01-01','2017-01-01'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

OneDay_Before_Settlement_Date = datestr(busdate(today-3,-1,HolidayVec),'yyyy-mm-dd');
OneWeek_Before_Settlement_Date = datestr(busdate(today-6,-1,HolidayVec),'yyyy-mm-dd');

%% Fetch Data
for i = 1 : length(FieldNames)
    
    Data = PortfolioGroup.(FieldNames{i});
    
    TempOutData = cell2dataset([ColNames;Data]);
    
    %% Make ProductGroup and PortfolioGroup Information for Input Data
    ViewName = [char(InBUName),'_combined_report_mapping_table'];
    %     [ColNames1,Data1] = read_from_database(ViewName,0);
    [ColNames1,Data1] = Fetch_DB_Data(ObjDB,[],ViewName) ;
    DBRefPFGroupData = cell2dataset([ColNames1;Data1]);
    
    ViewName = [char(InBUName),'_product_group_table'];
    %     [ColNames1,Data1] = read_from_database(ViewName,0);
    [ColNames1,Data1] = Fetch_DB_Data(ObjDB,[],ViewName) ;
    DBRefPrdtGroupData = cell2dataset([ColNames1;Data1]);
    
    PortfolioGroup_Name = cell(size(TempOutData.subportfolio));
    ProductGroup_Name = cell(size(TempOutData.subportfolio));
    
    for iPF = 1:length(DBRefPFGroupData.subportfolio)
        IdxPF = strcmpi(DBRefPFGroupData.subportfolio{iPF},TempOutData.subportfolio);
        PortfolioGroup_Name(IdxPF) = DBRefPFGroupData.group(iPF);
    end
    
    for iPF = 1:length(DBRefPrdtGroupData.product_code)
        IdxPF = strcmpi(DBRefPrdtGroupData.product_code{iPF},TempOutData.product_code);
        ProductGroup_Name(IdxPF) = DBRefPrdtGroupData.product_group(iPF);
    end
    
    TempOutData.PortfolioGroup = PortfolioGroup_Name;
    TempOutData.ProductGroup = ProductGroup_Name;
    
    PortfolioGroup.(FieldNames{i}) = dataset2cell(TempOutData);
    
end


aa =1;
end





