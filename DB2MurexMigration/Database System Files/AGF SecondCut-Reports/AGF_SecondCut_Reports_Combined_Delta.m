function [OutErrorMsg,Header , FUT_MonthWise_Final] = AGF_SecondCut_Reports_Combined_Delta(InBUName, ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    if(~isempty(Data))
        
        GroupType_Index = strcmpi(ColNames,'group_type');
        GroupType_Data = Data(:,GroupType_Index);
        
        Matched_GroupType_Index = cellStrfind_Perfect(GroupType_Data,{'OFUT','FUT','FXFUT'});
        
        Data = Data(Matched_GroupType_Index,:); %% OFUT,FUT and FXFUT Data Only
        
        %%
        ProductCode_Index = strcmpi(ColNames,'product_code');
        ProductCode_Data = Data(:,ProductCode_Index);
        
        Unique_ProductCode_Data_Input  = unique(ProductCode_Data);
        %     ProductCode = Unique_ProductCode_Data ;  %%[~,ProductCode] = strtok(Unique_ProductCode_Data);
        %     ProductCode = strtrim(ProductCode);
        
        %     ObjDB = connect_to_database;
        RPO_TableName = [char(InBUName),'_reports_product_order_table'];
        SqlQuery = ['select * from ',char(RPO_TableName),' where product_type in (''COMMODITY'',''FXFUT'')'];
        Table_Data  = fetch(ObjDB,SqlQuery);
        
        Table_Data  = sortrows(Table_Data);
        Unique_ProductCode_Data = Table_Data(:,3) ; %% Only FUT and OFUT
        
        misMatch = setdiff(Unique_ProductCode_Data,Unique_ProductCode_Data_Input);
        misMatch_index = cellStrfind_exact(Unique_ProductCode_Data,misMatch);
        Unique_ProductCode_Data(misMatch_index) = [];
        
        ProductCode = strtrim(Unique_ProductCode_Data);
        
        %%
        ContractMonth_Index = strcmpi(ColNames,'contract_month'); %% Get Contract Month Data from Source Table
        ContractMonth_Data = Data(:,ContractMonth_Index);  %% FUT ContractMonth Only
        Unique_ContractMonth_Data = unique(ContractMonth_Data);
        
        % Maturity_Code = cellfun(@(x) x(end-1:end), Unique_ContractMonth_Data, 'UniformOutput', false);
        Maturity_Code = Unique_ContractMonth_Data;
        
        SettleDelta_Index = find(strcmpi(ColNames,'settle_delta_1'));
        
        FUT_MonthWise = cell(length(Unique_ContractMonth_Data),length(Unique_ProductCode_Data));
        
        for k = 1 : length(Unique_ContractMonth_Data)
            
            Current_ContractMonth = Unique_ContractMonth_Data{k};
            
            Matched_ContractMonth_Index = find(strcmpi(ContractMonth_Data,Current_ContractMonth));
            
            Temp_ProductCode_Data = ProductCode_Data(Matched_ContractMonth_Index);
            
            for i = 1 : length(Unique_ProductCode_Data)
                
                Current_ProductCode = Unique_ProductCode_Data{i};
                
                Matched_ProductCode_Index = Matched_ContractMonth_Index(find(strcmpi(Temp_ProductCode_Data,Current_ProductCode)));
                
                %         FUT_MonthWise{k,i} = length(Matched_ProductCode_Index) ;
                FUT_MonthWise{k,i} = sum(cell2mat(Data(Matched_ProductCode_Index,SettleDelta_Index)));
            end
            
        end
        
        [GrandTotal_Row,GrandTotal_Col] = cell2sum_Row_Col(FUT_MonthWise);
        FUT_MonthWise = [FUT_MonthWise;GrandTotal_Col];
        
        ProductCode = ProductCode';
        FUT_MonthWise_Final = [ProductCode;FUT_MonthWise];
        
        Maturity_Code = [{'Maturity'};Maturity_Code;{'Total'}];
        FUT_MonthWise_Final = [Maturity_Code,FUT_MonthWise_Final];
        
        Header = cell(1,size(FUT_MonthWise_Final,2));
        Header(1,2) = {'Pdt Code'};
        
        %%
        FUT_MonthWise_Total = [Header;FUT_MonthWise_Final];
        
        % xlswrite(OutXLSFileName,FUT_MonthWise_Total,'COMBINED DELTA REPORT');
        
    else
        Header  = {'No Data'};
        FUT_MonthWise_Final = {' '};
    end
catch ME
    Header  = {'No Data'};
    FUT_MonthWise_Final = {'Error'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end