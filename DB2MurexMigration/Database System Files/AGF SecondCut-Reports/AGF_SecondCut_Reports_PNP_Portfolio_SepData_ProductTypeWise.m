function [NewHeader,New_RowData] = AGF_SecondCut_Reports_PNP_Portfolio_SepData_ProductTypeWise(InBUName,Product_Category_Type,NewHeader,RowData,Uploading_2_DB,Selected_Portfolio)

[~,Overall_Col_Sum] = cell2sum_Row_Col(RowData(:,2:end));

Empty_Header = cell(1,size(RowData,2)-1);

global ObjDB;

%%
FUT_Category_Group_Index = sort(cellStrfind_Perfect(Product_Category_Type,{'FUT','OFUT'})); %% Sort for Product Order Wise
FUT_Category_Group_Data_1 = RowData(FUT_Category_Group_Index,:);
[~,FUT_Category_Col_Sum] = cell2sum_Row_Col(FUT_Category_Group_Data_1(:,2:end));
Commodity_Total_Row = ['COMMODITY TOTAL',FUT_Category_Col_Sum];
FUT_Category_Group_Data = [FUT_Category_Group_Data_1 ; Commodity_Total_Row];

FXFUT_Category_Group_Index = cellStrfind_Perfect(Product_Category_Type,{'FXFUT'});
FXFUT_Category_Group_Data = RowData(FXFUT_Category_Group_Index,:);
FXFUT_Header = ['FXFUT TRADES',Empty_Header];
FXFUT_Data = [FXFUT_Header ; FXFUT_Category_Group_Data];

FXD_Trades_Index = cellStrfind_Perfect(Product_Category_Type,{'FXD'});
FXD_Trades_Data = RowData(FXD_Trades_Index,:);
FXD_CASH_Flow_Trades_Index = cellStrfind(FXD_Trades_Data(:,1),{'CASH'});
FXD_CASH_Flow_Trades_Data = FXD_Trades_Data(FXD_CASH_Flow_Trades_Index,:);
FXD_CASH_Header = ['CASHFLOW TRADES',Empty_Header];
FXD_CASH_Data = [FXD_CASH_Header ; FXD_CASH_Flow_Trades_Data];

FXD_Trades_Data(FXD_CASH_Flow_Trades_Index,:) = [] ;  %% Without CASH FXD Product
FXD_Header = ['FXD TRADES',Empty_Header];
FXD_Data = [FXD_Header ; FXD_Trades_Data];

New_RowData = [FUT_Category_Group_Data ; FXFUT_Data ;  FXD_Data ; FXD_CASH_Data ];
Overall_Total_Row = ['TOTAL', Overall_Col_Sum] ;
New_RowData = [New_RowData ; Overall_Total_Row];


%% Make Uploading to DB Variable for 'productwise_combined_pnl_table'
% ObjDB = connect_to_database ;
DBValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));

DB_Uplodaing_Data = [];

DB_Data_Type  = cell(size(FUT_Category_Group_Data_1,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'COMMODITY'};
FUT_Category_Group_Data_1 = [DB_Data_Type , FUT_Category_Group_Data_1];

DB_Data_Type  = cell(size(Commodity_Total_Row,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'COMMODITY TOTAL'};
Commodity_Total_Row = [DB_Data_Type , Commodity_Total_Row] ;

DB_Data_Type  = cell(size(FXFUT_Category_Group_Data,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'FXFUT'};
FXFUT_Category_Group_Data = [DB_Data_Type , FXFUT_Category_Group_Data] ;

DB_Data_Type  = cell(size(FXD_CASH_Flow_Trades_Data,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'CASHFLOW'};
FXD_CASH_Flow_Trades_Data = [DB_Data_Type , FXD_CASH_Flow_Trades_Data] ;

DB_Data_Type  = cell(size(FXD_Trades_Data,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'FXD'};
FXD_Trades_Data = [DB_Data_Type , FXD_Trades_Data] ;

DB_Data_Type  = cell(size(Overall_Total_Row,1),2);
DB_Data_Type(:,1) = {DBValueDate};
DB_Data_Type(:,2) = {'TOTAL'};
Overall_Total_Row = [DB_Data_Type , Overall_Total_Row] ;

DB_Uplodaing_Data = [FUT_Category_Group_Data_1 ; Commodity_Total_Row ; FXFUT_Category_Group_Data ; FXD_Trades_Data ;... 
    FXD_CASH_Flow_Trades_Data ; Overall_Total_Row ];

DB_Uplodaing_Data = DB_Uplodaing_Data(:,1:end-2) ; %% No need Last Two Columns

%% Uploading to DB

if(Uploading_2_DB)
    
    TableName = [char(InBUName),'_productwise_combined_pnl_table_',char(Selected_Portfolio)];
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, DB_Uplodaing_Data);
    
end

end
