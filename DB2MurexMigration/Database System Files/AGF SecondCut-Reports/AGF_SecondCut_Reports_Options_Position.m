function [OutErrorMsg, Header , Total_Data] = AGF_SecondCut_Reports_Options_Position(InBUName,ColNames,Data,PortfolioGroup,FieldNames,OutXLSFileName)

OutErrorMsg = {'No errors'};
global ObjDB;

try
    
    GroupType_Index = strcmpi(ColNames,'group_type');
    GroupType_Data = Data(:,GroupType_Index);
    Matched_GroupType_Index = find(strcmpi('OFUT',GroupType_Data));
    
    if(~isempty(Matched_GroupType_Index)) %% Only OFUT Data
        
        ProductCode_Index = strcmpi(ColNames,'product_code');
        ProductCode_Data = Data(Matched_GroupType_Index,ProductCode_Index);  %% FUT Product Code Only
        %%
        Unique_ProductCode_Data_Input  = unique(ProductCode_Data);
        
        %     ObjDB = connect_to_database;
        RPO_TableName = [char(InBUName),'_reports_product_order_table'];
        SqlQuery = ['select * from ',char(RPO_TableName),' where product_type in (''COMMODITY'',''FXFUT'')'] ;
        Table_Data  = fetch(ObjDB,SqlQuery);
        
        Table_Data  = sortrows(Table_Data);
        Unique_ProductCode_Data = Table_Data(:,3) ; %% Only FUT and OFUT
        
        Unique_ProductCode_Data = Unique_ProductCode_Data(sort(cellStrfind_exact(Unique_ProductCode_Data,Unique_ProductCode_Data_Input)));
        
        %%
        ContractMonth_Index = strcmpi(ColNames,'contract_month'); %% Get Contract Month Data from Source Table
        ContractMonth_Data = Data(Matched_GroupType_Index,ContractMonth_Index);  %% FUT ContractMonth Only
        Unique_ContractMonth_Data = unique(ContractMonth_Data);
        
        CallPut_Index = strcmpi(ColNames,'call_put_id');
        Strike_Index = strcmpi(ColNames,'strike');
        ActiveLots_Index = strcmpi(ColNames,'active_lots');
        
        Strike_Data = Data(Matched_GroupType_Index,Strike_Index);
        
        count = 1;
        
        Total_Stike = [];
        Total_Call_Put = [];
        Overall_Sum_Particular_Product = [];
        
        for i = 1 : length(Unique_ProductCode_Data)
            
            Current_ProductCode = Unique_ProductCode_Data{i};
            ProdCode = Current_ProductCode; %%[~,ProdCode] = strtok(Current_ProductCode);
            Pdt_Code_OP{count} = strtrim(ProdCode);
            
            Matched_ProductCode_Index = find(strcmpi(ProductCode_Data,Current_ProductCode));
            
            Matched_ProductCode_Data = ProductCode_Data(Matched_ProductCode_Index);
            Matched_ContractMonth_Data = ContractMonth_Data(Matched_ProductCode_Index);
            Matched_Strike_Data = Strike_Data(Matched_ProductCode_Index);
            
            Unique_ContractMonth_Data = unique(Matched_ContractMonth_Data);
            
            for k = 1 : length(Unique_ContractMonth_Data)
                
                Current_ContractMonth = Unique_ContractMonth_Data{k};
                Maturity{count} = Current_ContractMonth;
                
                Matched_PC_CM = Matched_ProductCode_Index(find(strcmpi(Matched_ContractMonth_Data,Current_ContractMonth)));
                %         Strike = Strike_Data(Matched_PC_CM);
                %         Total_Stike = [Total_Stike;Strike];
                
                %         count = count + length(Strike);
                %         Maturity{count} = [Current_ContractMonth,' Total'];
                %         Total_Stike = [Total_Stike;{''}];
                
                %         aa = cell(length(Strike) + 1,12);
                bb =[];
                
                for ii = 1 : length(FieldNames)
                    
                    CurrentGroup_ProductCol = PortfolioGroup.(FieldNames{ii})(:,ProductCode_Index);
                    Matched_CurrentGroup__PC_Index = find(strcmpi(CurrentGroup_ProductCol,Current_ProductCode));
                    
                    CurrentGroup_ContractMonth_Data = PortfolioGroup.(FieldNames{ii})(Matched_CurrentGroup__PC_Index,ContractMonth_Index);
                    
                    %  Matched_CurrentGroup_CM = find(strcmpi(CurrentGroup_ContractMonth_Data,Current_ContractMonth));
                    Matched_CurrentGroup_CM = Matched_CurrentGroup__PC_Index(find(strcmpi(CurrentGroup_ContractMonth_Data,Current_ContractMonth)));
                    
                    Matched_CallPut = PortfolioGroup.(FieldNames{ii})(Matched_CurrentGroup_CM,CallPut_Index);
                    
                    Matched_Call =  Matched_CurrentGroup_CM(find(strcmpi('c',Matched_CallPut)));
                    Matched_Put =  Matched_CurrentGroup_CM(find(strcmpi('p',Matched_CallPut)));
                    
                    %%
                    Matched_Call_Put = [Matched_Call;Matched_Put];
                    
                    if(~isempty(Matched_Call_Put))
                        User_Want_Col = cellStrfind_Perfect(ColNames,{'product_code','contract_month','call_put_id','strike','active_lots'});
                        Temp_Matrix = PortfolioGroup.(FieldNames{ii})(Matched_Call_Put,User_Want_Col);
                        
                        UniqueFields = {'product_code','contract_month','call_put_id','strike'};
                        SumFields = {'active_lots'};
                        OutputFields = [UniqueFields,SumFields];
                        [OutputFields,ConsDeals] = consolidatedata(OutputFields, Temp_Matrix,UniqueFields,SumFields,OutputFields,[]);
                        
                        Strike = ConsDeals(:,4); %% strike col is 4
                        Total_Stike = [Total_Stike;Strike];
                        
                        aa = cell(size(Strike,1) ,12);
                        
                        Matched_Call = find(strcmpi('c',ConsDeals(:,3))) ; %% Call Put Column is 3
                        Matched_Put = find(strcmpi('p',ConsDeals(:,3))) ;
                        
                        Call_Data = ConsDeals(Matched_Call,5); %% active_lots Column is 5
                        Put_Data = ConsDeals(Matched_Put,5);
                        
                        
                        %%
                        if(~isempty(Call_Data) || ~isempty(Put_Data))
                            
                            if(ii==1)
                                aa(Matched_Call,ii) = Call_Data;
                                aa(Matched_Put,ii+1) = Put_Data ;
                            elseif(ii==2)
                                aa(Matched_Call,ii+1) = Call_Data;
                                aa(Matched_Put,ii+2) = Put_Data ;
                            elseif(ii==3)
                                aa(Matched_Call,ii+2) = Call_Data;
                                aa(Matched_Put,ii+3) = Put_Data ;
                            elseif(ii==4)
                                aa(Matched_Call,ii+3) = Call_Data;
                                aa(Matched_Put,ii+4) = Put_Data ;
                            elseif(ii==5)
                                aa(Matched_Call,ii+4) = Call_Data;
                                aa(Matched_Put,ii+5) = Put_Data ;
                            elseif(ii==6)
                                aa(Matched_Call,ii+5) = Call_Data;
                                aa(Matched_Put,ii+6) = Put_Data ;
                            end
                        end
                        
                        bb = [bb;aa];
                        
                    end
                    
                    
                    
                end
                
                bb = [bb ; cell(1,size(bb,2))]; %% Append Empty Row for Total
                
                Total_Call_Put = [Total_Call_Put;bb];
                
                count = size(Total_Call_Put,1);
                Maturity{count} = [Current_ContractMonth,' Total'];
                Total_Stike{count,1} = '';
                
                Pdt_Code_OP{count} = '';
                
                [~,Col_Sum] = cell2sum_Row_Col(bb);
                Total_Call_Put(end,:) = Col_Sum;
                
                Overall_Sum_Particular_Product = [Overall_Sum_Particular_Product ; Col_Sum];
                
                count = count + 1;
                
            end
            
            Pdt_Code_OP{count} = [Current_ProductCode,' Total'];
            Maturity{count} = '';
            Total_Stike{count,1} = '';
            
            [~,Col_Sum_OverAll] = cell2sum_Row_Col(Overall_Sum_Particular_Product);
            Total_Call_Put = [Total_Call_Put ; Col_Sum_OverAll] ;
            Overall_Sum_Particular_Product = [];
            
            count = count + 1 ;
            
        end
        
        [Grand_Total,~] = cell2sum_Row_Col(Total_Call_Put);
        
        %%
        Total_Data = [Pdt_Code_OP',Maturity',Total_Stike,Total_Call_Put,Grand_Total];
        
        Header = [{'','','','MM-FF1-FY','MM-FF1-FY','MM-FF2-FY','MM-FF2-FY','MM-FF3-FY','MM-FF3-FY','MM-FF4-FY','MM-FF4-FY','MM-FF5-FY','MM-FF5-FY','MM-FF6-FY','MM-FF6-FY','Grand Total'};...
            {'Product Code','Maturity','Strike','call','put','call','put','call','put','call','put','call','put','call','put',''}];
        
        
        % xlswrite(OutXLSFileName,[Header;Total_Data],'OPTIONS POSITIONS ');
        
    else
        Header = {'No OFUT Data'};
        Total_Data = {' '};
        
    end
    
catch ME
    Header = {'No Data'};
    Total_Data = {'No Data'};
    
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end

end