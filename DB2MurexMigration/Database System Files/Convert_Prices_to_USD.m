function [OutErrorMsg,OutFilename] =  Convert_Prices_to_USD(InBUName,Settle_Date)

%%%Upload Swap Price

% InBUName = 'qf5';
OutErrorMsg = {'No errors'};
OutFilename = '';

ObjDB = connect_to_database;

% format long;

%%

try
    %% Fetch Data From DB
    
    if(strcmpi(InBUName,'qf5'))
        
        SqlQuery_Currency_Map_Table = 'select * from equity_currency_mapping_table_qf5';
        [Currency_MapTbl_ColNames,Currency_MapTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Currency_Map_Table);
        Currency_Map_Tbl = cell2dataset([Currency_MapTbl_ColNames;Currency_MapTbl_Data]);
        
        %         settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table'); %% '2017-09-08' ;
        
        AssetClass_Matching = 'equity';
        SqlQuery_1 = ['select * from underlying_settle_value_table where asset_class = ''',AssetClass_Matching,''' and settlement_date = ''',char(Settle_Date),''' '];
        [Underlying_ColNames,Underlying_Data] = Fetch_DB_Data(ObjDB,SqlQuery_1);
        if(strcmpi(Underlying_Data,'No Data'))
            %             msgbox('No Data for Corresponding settlement_date in underlying_settle_value_table','Check DB') ;
            OutErrorMsg = {'No Data for Corresponding settlement_date in underlying_settle_value_table Check DB'};
            return;
        end
        
        spot_id = 'CR USDCNH Spot BGN';
        %         Counterparty = 'Morgan Stanley';
        %         SqlQuery_2 = ['select counterparty,subportfolio,settlement_spot_rate from equity_swaps_spot_table_qf5 where settlement_date = ''',char(settlement_date),''' and spot_id = ''',spot_id,''' and counterparty =''',Counterparty,''' '];
        SqlQuery_2 = ['select counterparty_parent,spot_id,settlement_spot_rate from equity_swaps_spot_table_qf5 where settlement_date = ''',char(Settle_Date),'''   ']; %% and spot_id = ''',spot_id,'''
        [Spot_ColNames,Spot_Data] = Fetch_DB_Data(ObjDB,SqlQuery_2);
        if(strcmpi(Spot_Data,'No Data'))
            %             msgbox(['No Spot_Data for Corresponding settlement_date and Spot_ID' ,char(settlement_date),{''},spot_id ],'Check DB') ;
            OutErrorMsg = {['No Spot_Data for Corresponding settlement_date and Spot_ID ' ,char(Settle_Date),' - ',spot_id ]};
            return;
            
        else
            %% Calculation
            
            
            
            AssetClass_Col = cellStrfind_exact(Underlying_ColNames,{'asset_class'});
            UnderlyingID_Col = cellStrfind_exact(Underlying_ColNames,{'underlying_id'});
            SettleValue_Col = cellStrfind_exact(Underlying_ColNames,{'settle_value'});
            
            CounterpartyParent_Col = cellStrfind_exact(Spot_ColNames,{'counterparty_parent'});
            SpotID_Col = cellStrfind_exact(Spot_ColNames,{'spot_id'});
            Settle_SpotRate_Col = cellStrfind_exact(Spot_ColNames,{'settlement_spot_rate'});
            
            
            %%% Newly added code(13-05-2019)
            SqlQuery_equity_upload_swaps_mapping = 'select * from equity_counterparty_mapping_table_qf5';
            [~,equity_upload_swaps_mapping_Data] = Fetch_DB_Data(ObjDB,SqlQuery_equity_upload_swaps_mapping);
            if(strcmpi(equity_upload_swaps_mapping_Data,'No Data'))
                OutErrorMsg = {'Data not Available in equity_upload_swaps_mapping_table'};
                return;
            end
            %%% End Code
            
            Overall_Underlying_Data = [];
            for i = 1 : size(Spot_Data,1)
                
                Current_CountetParty = Spot_Data(i,CounterpartyParent_Col);
                Current_Spot_ID = Spot_Data(i,SpotID_Col);
                Current_SpotRate = Spot_Data(i,Settle_SpotRate_Col);
                
                Split_Str = strsplit(char(Current_Spot_ID),' ');
                USD_Str_Check = char(Split_Str(2));
                USD_Str_Check = USD_Str_Check(1:3);
                
                Current_Currency = unique(Currency_Map_Tbl.currency(strcmpi(Currency_Map_Tbl.spot_id,Current_Spot_ID)));
                
                Underlying_Data_Temp = Underlying_Data; %% Copied

                %%% Newly added code (13-05-2019)
                Index_counterparty = contains(equity_upload_swaps_mapping_Data(:,2),Current_CountetParty,'IgnoreCase',true);
                equity_upload_swaps_Data = equity_upload_swaps_mapping_Data(Index_counterparty,1);
                Index_undelayind_id = ismember(Underlying_Data_Temp(:,2),equity_upload_swaps_Data);
                Underlying_Data_Temp = Underlying_Data_Temp(Index_undelayind_id,:);
                %%% End Code
                
                Temp_Cell = cell(size(Underlying_Data_Temp,1),2);
                Temp_Cell(:,1) = Current_CountetParty;
                Temp_Cell(:,2) = Current_Currency;
                
                Underlying_Data_Temp(:,AssetClass_Col) = {'equity swaps'};
                Underlying_Data_Temp(:,UnderlyingID_Col) = strrep(Underlying_Data_Temp(:,UnderlyingID_Col),'EQ','SW');
                SettleValue_NC = Underlying_Data_Temp(:,SettleValue_Col);
                
                %%%% Start
                %%%% Newly added code for roundup issue
                Roundup_UDP_Settle_Val_14 = round(cell2mat(SettleValue_NC),14);
                Roundup_Current_SpotRate_14 = (cell2mat(Current_SpotRate));
                %%% End
                
                
%                 if(strcmpi(USD_Str_Check,'USD'))
%                     Underlying_Data_Temp(:,SettleValue_Col) = num2cell(round(cell2mat(Underlying_Data_Temp(:,SettleValue_Col)) ./ cell2mat(Current_SpotRate),4));
%                 else
%                     Underlying_Data_Temp(:,SettleValue_Col) = num2cell(round(cell2mat(Underlying_Data_Temp(:,SettleValue_Col)) .* cell2mat(Current_SpotRate),4));
%                 end

                
                if(strcmpi(USD_Str_Check,'USD'))
                    Underlying_Data_Temp(:,SettleValue_Col) = num2cell(round((Roundup_UDP_Settle_Val_14) ./ (Roundup_Current_SpotRate_14),14));
                else
                    Underlying_Data_Temp(:,SettleValue_Col) = num2cell(round((Roundup_UDP_Settle_Val_14) .* (Roundup_Current_SpotRate_14),14));
                end
                
                Underlying_Data_Temp(:,SettleValue_Col) = num2cell(round(cell2mat(Underlying_Data_Temp(:,SettleValue_Col)),4));
                
                
                Underlying_Data_Temp = [Underlying_Data_Temp ,Temp_Cell,SettleValue_NC];
                Overall_Underlying_Data = [Overall_Underlying_Data ; Underlying_Data_Temp];
                
                
            end
        end
        
        %% Remove Cols & Re-arrange tha Col Order
        
        Underlying_ColNames = [Underlying_ColNames,{'counterparty_parent','currency','settle_value_nc'}];
        RemoveCol_Index = cellStrfind_exact(Underlying_ColNames,{'risk_free_rate','div_yield'});
        
        Underlying_ColNames(RemoveCol_Index) = [];
        Overall_Underlying_Data(:,RemoveCol_Index) = [];
        
        Col_Order = {'asset_class','underlying_id','value_date','counterparty_parent','currency','settle_value','settlement_date','settle_value_nc'};
        MatchedCol_Index = cellStrfind_exact(Underlying_ColNames,Col_Order);
        
        OutData = Overall_Underlying_Data(:,MatchedCol_Index);
        
        %% Excel Write
        
        OutFilename = getXLSFilename(['Convert_Price_to_USD_',char(InBUName)]);
        
        xlswrite(OutFilename,[Col_Order;OutData]);
        
        %     OutFilename = fullfile(pwd,OutFilename);
        
        %% Uploading Data into Table
        
        TableName = 'equity_swaps_underlying_prices_table_qf5';
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Settle_Date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(TableName, OutData);
        
        
        
    else
        %         warndlg('This Code not Implemented for Other than QF5','Warning');
        OutErrorMsg = {'This Code not Implemented for Other than QF5'};
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end