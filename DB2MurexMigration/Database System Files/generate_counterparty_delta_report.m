function [OutErrorMsg,OutFilename] = generate_counterparty_delta_report(InBUName)
% generate the OTC counterparty delta position report for compliance team 
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:52 $
%  $Revision: 1.2 $
%

OutErrorMsg = {'No errors'};

try    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
ViewName = 'helper_settle_pricing_subportfoliovalues_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

Filename = ['Counterparty_Delta_Report_',InBUName];
OutFilename = getXLSFilename(Filename);

% change the product code of Rubber to OR; since it is currently OR-EUR or
% OR-USD
PosProdCode = strcmpi('product_code',SettleColNames);
IdxOR = strncmpi(SettleData(:,PosProdCode),'OR',2);
SettleData(IdxOR,PosProdCode) = {'OR'};

SettleColNames = strrep(SettleColNames,'settle_delta_1','delta');
SettleColNames = strrep(SettleColNames,'original_lots','traded_lots');
SettleColNames = strrep(SettleColNames,'counterparty_parent','counterparty');

UniqueFields = {'product_code','counterparty','group_type','contract_month','market_action'};
SumFields = {'delta','traded_lots'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];

[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

xlswrite(OutFilename,[OutputFields;OutData],'Cumulative_Positions');

% To provide the intraday trading positions in the report
PsSettlementDate = strcmpi('settlement_date',SettleColNames);
SettlementDate = SettleData(1,PsSettlementDate);

PosTradeDate = strcmpi('transaction_date',SettleColNames);
IdxTradeDate = strcmpi(SettleData(:,PosTradeDate),SettlementDate);
SettleData(~IdxTradeDate,:)= [];

if ~isempty(SettleData)
UniqueFields = {'product_code','counterparty','group_type','contract_month','market_action'};
SumFields = {'delta','traded_lots'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];

[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

xlswrite(OutFilename,[OutputFields;OutData],'FTD_Positions');
end

catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end