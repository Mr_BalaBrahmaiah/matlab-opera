function [OutErrorMsg,OutFilename] = generate_net_fut_prodwise_report(InBUName)
%  Generate the options reports in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/28 11:00:27 $
%  $Revision: 1.9 $
%
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

ViewName = 'helper_funds_reports_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

Pos_GroupType = strcmpi('group_type',SettleColNames);

IdxFUT = strcmpi('FUT',SettleData(:,Pos_GroupType));
DBFutureData = SettleData(IdxFUT,:);


if ~isempty(DBFutureData)    
   
      
    OutputFields = {'product_code','settlement_date',...
        'contract_month','p1_name','p1_settleprice'...
        'settle_price','market_action','active_lots','original_lots',...
        'original_premium','mtm_nc','mult_factor','conv_factor','product_name','counterparty_parent'};% added counterparty parent
    WeightedAverageFields = {'original_premium','active_lots'};
    UniqueFields = {'product_code','contract_month','market_action','counterparty_parent'};% added counterparty parent
    SumFields = {'original_lots','active_lots','mtm_nc'};
    [OptionFields,OptionDeals] = consolidatedata(SettleColNames, DBFutureData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    ConsFutData = cell2dataset([OptionFields;OptionDeals]);
    
    IdxOptBot = strcmpi('bought', ConsFutData.market_action);
    IdxOptSold = strcmpi('sold', ConsFutData.market_action);
    
    % initialisation
    OpenBought = zeros(size(IdxOptBot));
    OpenSold = zeros(size(IdxOptSold));
    RealisedOpt = zeros(size(IdxOptBot));
    Open_Sold_Rate = zeros(size(IdxOptSold));
    Open_Bot_Rate = zeros(size(OpenBought));    
   
  
    MultFac = ConsFutData.mult_factor;
    ConvFac = ConsFutData.conv_factor;

    OpenBought(IdxOptBot) = abs(ConsFutData.active_lots(IdxOptBot));
    OpenSold(IdxOptSold) = abs(ConsFutData.active_lots(IdxOptSold));
    NetFutOpen = OpenBought - OpenSold;
                
    M2MOpt = ConsFutData.mtm_nc ;
    
    Mkt_Rate = ConsFutData.settle_price;
     
    Fut_Mkt_Val = ((OpenBought .* Mkt_Rate) - (OpenSold .* Mkt_Rate)) .* MultFac;
    Fut_Val = Fut_Mkt_Val - M2MOpt;
    
    Open_Sold_Rate(IdxOptSold) = ConsFutData.original_premium(IdxOptSold) ./ ConvFac(IdxOptSold);
    Open_Bot_Rate(IdxOptBot) =  ConsFutData.original_premium(IdxOptBot) ./ ConvFac(IdxOptBot);
    
    Mkt_Rate = Mkt_Rate ./ ConvFac;
    Price = ConsFutData.p1_settleprice ./ ConvFac;
    
    OutData = [ConsFutData.product_code,ConsFutData.product_name,....
        ConsFutData.contract_month,...
        num2cell(Price),num2cell(OpenSold),...
        num2cell(OpenBought),num2cell(NetFutOpen),...
        num2cell(RealisedOpt),num2cell(M2MOpt),num2cell(Fut_Val),...
        num2cell(Fut_Mkt_Val),num2cell(Mkt_Rate),num2cell(Open_Sold_Rate),...
        num2cell(Open_Bot_Rate),ConsFutData.settlement_date,ConsFutData.counterparty_parent];
    OutHeader = {'ProductCode','ProductName',...
        'ContractMonth','Price','Open_Sold',...
        'Open_Bought','Net_Open',...
        'Realised_PnL','M2M','Val','MktVal','Mkt_Rate',...
        'Open_Sold_Rate','Open_Bot_Rate','Market_ValueDate','counterparty_parent'};% added counterparty parent
    
    WeightedAverageFields = [];
    UniqueFields = {'ProductCode','ContractMonth','counterparty_parent'};% added counterparty parent
    SumFields = {'Open_Sold','Open_Bought','Net_Open','Realised_PnL','M2M',...
        'Val','MktVal','Open_Sold_Rate','Open_Bot_Rate'};
    [~,OutOptionData] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutHeader,WeightedAverageFields);

    
    try
        OutOptionData = sortrows(OutOptionData,[1 2 3 4 5 6]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    
    OutHeader = {'PRODUCT_CODE','PRODUCT_NAME','TMONTH',...
        'PRICE','OPEN_SOLD',...
        'OPEN_BOT','NET_OPEN','REALIZED_PL','MTM','VAL','MKTVAL','MKT_RATE',...
        'OPENSOLDRATE','OPENBOTRATE','MKT_DATE'};
    
    OutFilename = getXLSFilename('NetFutures_Productwise_Report');
    xlswrite(OutFilename,[OutHeader;OutOptionData(:,(1:end-1))]);
    
    %%% Newly added on 22-01-2020, for counterparty parent
    OutHeader = {'PRODUCT_CODE','PRODUCT_NAME','TMONTH',...
        'PRICE','OPEN_SOLD',...
        'OPEN_BOT','NET_OPEN','REALIZED_PL','MTM','VAL','MKTVAL','MKT_RATE',...
        'OPENSOLDRATE','OPENBOTRATE','MKT_DATE','COUNTERPARTY'};
    xlswrite(OutFilename,[OutHeader;OutOptionData],'Sheet2');
    
    xls_change_activesheet(fullfile(pwd,OutFilename) ,'Sheet1');
    
else
    OutErrorMsg = {'No option trades found!'};
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end