function [OutErrorMsg,PremiumPaidDump,CashBalanceReport] = generate_netting_premium_paid_report(InBUName)


OutErrorMsg = {'No errors'};

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
PremiumPaidDump = getCSVFilename('Netting_premium_paid_dump');

ViewName = 'helper_netting_deal_ticket_table_view';
[ColNames,Data] = read_from_database(ViewName,0,'',InBUName);

% OutXLSName = 'netting_premium_paid_report.xlsx';   %%% Newly added line
% xlswrite(OutXLSName,[ColNames;Data],'PremiumPaidDump');     %%% Newly added line

customcsvwrite(PremiumPaidDump,Data,ColNames);

UniqueFields = {'subportfolio','product_code','counterparty_parent','acc_group','group_type','contract_number'};
SumFields = {'realised_pnl','unrealised_pnl'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

CashBalanceReport = getCSVFilename('Cash_balance_report');

% xlswrite(OutXLSName,[OutputFields;OutData],'CashBalanceReport');  %%% Newly added line

customcsvwrite(CashBalanceReport,OutData,OutputFields);
catch ME
    OutErrorMsg = cellstr(ME.message);
    PremiumPaidDump = ''; CashBalanceReport = '';
end