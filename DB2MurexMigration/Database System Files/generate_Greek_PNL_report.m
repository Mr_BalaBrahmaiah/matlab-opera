clc
clear;

InBUName = 'agf';
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

ViewName = 'helper_greek_pandl_report_view' ;

[ColNames,Data] = read_from_database(ViewName,0,'',InBUName);

%%
if ~strcmpi(Data,'No Data')
    
    User_Want_Fields = {'value_date','settlement_date','subportfolio','instrument','contract_month','group_type','call_put_id','strike','active_lots',...
        'mult_factor','settle_vega_1','settle_theta','transaction_date','settle_price_today','settle_price_yest',...
        'und_st_price_today','und_st_price_yest','p1_vol_today','p1_vol_yest','vol_change','delta_lots','gamma_lots','pnl_today','pnl_yest','price_change'} ; %% 'settle_delta_1','settle_gamma_11'
    
    User_Want_Col = cellStrfind_exact(ColNames,User_Want_Fields) ;
    User_Data = Data(:,User_Want_Col) ;
    
    ValueDate_Col = find(cellfun(@(V) strcmpi('value_date',V), User_Want_Fields)) ;
    SettleDate_Col = find(cellfun(@(V) strcmpi('settlement_date',V), User_Want_Fields)) ;
    TransacDate_Col = find(cellfun(@(V) strcmpi('transaction_date',V), User_Want_Fields)) ;
    Instrument_Col = find(cellfun(@(V) strcmpi('instrument',V), User_Want_Fields)) ;
    SubPortfolio_Col = find(cellfun(@(V) strcmpi('subportfolio',V), User_Want_Fields)) ;
    ContractMonth_Col = find(cellfun(@(V) strcmpi('contract_month',V), User_Want_Fields)) ;
    GroupType_Col = find(cellfun(@(V) strcmpi('group_type',V), User_Want_Fields)) ;
    Strike_Col = find(cellfun(@(V) strcmpi('strike',V), User_Want_Fields)) ;
    ActiveLots_Col = find(cellfun(@(V) strcmpi('active_lots',V), User_Want_Fields)) ;
    MultFactor_Col = find(cellfun(@(V) strcmpi('mult_factor',V), User_Want_Fields)) ;
    SettleDelta_Col = find(cellfun(@(V) strcmpi('settle_delta_1',V), User_Want_Fields)) ;
    SettleGamma_Col = find(cellfun(@(V) strcmpi('settle_gamma_11',V), User_Want_Fields)) ;
    SettleVega_Col = find(cellfun(@(V) strcmpi('settle_vega_1',V), User_Want_Fields)) ;
    SettleTheta_Col = find(cellfun(@(V) strcmpi('settle_theta',V), User_Want_Fields)) ;
    ST_Today_Col = find(cellfun(@(V) strcmpi('settle_price_today',V), User_Want_Fields)) ;
    ST_Yest_Col = find(cellfun(@(V) strcmpi('settle_price_yest',V), User_Want_Fields)) ;
    UN_ST_Today_Col = find(cellfun(@(V) strcmpi('und_st_price_today',V), User_Want_Fields)) ;
    UN_ST_Yest_Col = find(cellfun(@(V) strcmpi('und_st_price_yest',V), User_Want_Fields)) ;
    Vol_Change_Col = find(cellfun(@(V) strcmpi('vol_change',V), User_Want_Fields)) ;
    TodayVol_Col =  find(cellfun(@(V) strcmpi('p1_vol_today',V), User_Want_Fields)) ;
    YestdayVol_Col =  find(cellfun(@(V) strcmpi('p1_vol_yest',V), User_Want_Fields)) ;
    
    DeltaLots_Col =  find(cellfun(@(V) strcmpi('delta_lots',V), User_Want_Fields)) ;
    GammaLots_Col =  find(cellfun(@(V) strcmpi('gamma_lots',V), User_Want_Fields)) ;
    PnL_Today_Col =  find(cellfun(@(V) strcmpi('pnl_today',V), User_Want_Fields)) ;
    PnL_Yest_Col =  find(cellfun(@(V) strcmpi('pnl_yest',V), User_Want_Fields)) ;
    PriceChange_Col =  find(cellfun(@(V) strcmpi('price_change',V), User_Want_Fields)) ;
    
    %     GroupType_Index = cellStrfind_exact(User_Data(:,GroupType_Col),{'FUT','OFUT'}) ;
    %     User_Data = User_Data(GroupType_Index,:) ; %% Only for FUT and OFUT
    
    
    %%
    Compute_Header = {'Sum of Daily PNL','Sum of Delta PNL','Sum of Gamma PNL','Sum of Theta PNL','Sum of Vega PNL','Sum of Intra Day PNL','Sum of Residual PNL'} ; %% {'Today VOL(T)','Yest Vols(T-1)'};
    Compute_Data = cell(size(User_Data,1) ,size(Compute_Header,2) ) ;
    
    %  Compute_Data(:,1) = num2cell(cell2mat(User_Data(:,ActiveLots_Col)).* (cell2mat( User_Data(:,ST_Today_Col)) - cell2mat(User_Data(:,ST_Yest_Col))).* cell2mat(User_Data(:,MultFactor_Col)));
    %  FUT_Index = cellStrfind_exact(User_Data(:,GroupType_Col),{'FUT'}) ;
    %  OFUT_Index = cellStrfind_exact(User_Data(:,GroupType_Col),{'OFUT'}) ;
    %  Compute_Data(FUT_Index,2) = num2cell(cell2mat(User_Data(FUT_Index,ActiveLots_Col)).* (cell2mat( User_Data(FUT_Index,ST_Today_Col)) - cell2mat(User_Data(FUT_Index,ST_Yest_Col))).* cell2mat(User_Data(FUT_Index,MultFactor_Col)));
    %  Compute_Data(OFUT_Index,2) = num2cell(cell2mat(User_Data(OFUT_Index,SettleDelta_Col)).* (cell2mat( User_Data(OFUT_Index,UN_ST_Today_Col)) - cell2mat(User_Data(OFUT_Index,UN_ST_Yest_Col))).* cell2mat(User_Data(OFUT_Index,MultFactor_Col)));
    
    Compute_Data(:,1) = num2cell( cell2mat(User_Data(:,PnL_Today_Col)) - cell2mat(User_Data(:,PnL_Yest_Col)) ) ; %% Daily PNL
    Settle_Date = unique(User_Data(:,SettleDate_Col)) ;
    IntraDay_Index = strcmpi(User_Data(:,TransacDate_Col) , Settle_Date) ;
    Compute_Data(IntraDay_Index,1) = User_Data(IntraDay_Index,PnL_Today_Col);
    
    Compute_Data(:,2) =  num2cell( cell2mat(User_Data(:,DeltaLots_Col)) .* cell2mat(User_Data(:,PriceChange_Col)) ) ; %% Delta PNL
    FUT_Index = strcmpi(User_Data(:,GroupType_Col),{'FUT'}) ;
    Compute_Data(FUT_Index,2) = Compute_Data(FUT_Index,1) ;
    
    Compute_Data(:,3) = num2cell( cell2mat(User_Data(:,GammaLots_Col)) .* cell2mat(User_Data(:,PriceChange_Col)) ) ; %% Gamma PNL
    
    Compute_Data(:,4) = num2cell( cell2mat(User_Data(:,SettleTheta_Col)) .* 7/5 ); %% Theta PNL
    Compute_Data(:,5) = num2cell(cell2mat(User_Data(:,SettleVega_Col)) .* cell2mat(User_Data(:,Vol_Change_Col)) ); %% Vega PNL
    
    %     Compute_Data(:,6) = num2cell(cell2mat(User_Data(:,ActiveLots_Col)).* (cell2mat( User_Data(:,ST_Today_Col)) - cell2mat(User_Data(:,ST_Yest_Col))).* cell2mat(User_Data(:,MultFactor_Col)));
    Compute_Data(IntraDay_Index,6) = Compute_Data(IntraDay_Index,1) ; %% Same as Daily PNL (TransactionDate == SettleDate)
    
    %     Compute_Data(:,7) = num2cell(cell2mat(Compute_Data(:,1)) - (cell2mat(Compute_Data(:,2)) + cell2mat(Compute_Data(:,3)) + cell2mat(Compute_Data(:,4)) + cell2mat(Compute_Data(:,5)) + cell2mat(Compute_Data(:,6)))) ;
    [Row_Sum,~] = cell2sum_Row_Col(Compute_Data(:,(2:6))) ; %% Sum of 'Delta PNL','Gamma PNL','Theta PNL','Vega PNL',' Intra Day PNL'
    Compute_Data(:,7) = num2cell(cell2mat(Compute_Data(:,1)) - cell2mat(Row_Sum) ) ;
    
    %     Compute_Data(:,8) = User_Data(:,TodayVol_Col) ;
    %     Compute_Data(:,9) = User_Data(:,YestdayVol_Col) ;
    
    %%  NaN to Zero
    %     Compute_Data(cellfun(@(x) any(isnan(x)),Compute_Data)) = {0};
    
    Overall_Header = [User_Want_Fields ,Compute_Header ];
    Overall_Data = [User_Data , Compute_Data] ;
    
    %% Consolidate Data
    UniqueFileds = {'subportfolio','instrument','contract_month','group_type','call_put_id','strike'} ;
    SumFields = {'Sum of Daily PNL','Sum of Delta PNL','Sum of Gamma PNL','Sum of Theta PNL','Sum of Vega PNL','Sum of Intra Day PNL','Sum of Residual PNL'};
    OutputFields = Overall_Header ; %% [UniqueFileds , SumFields] ;
    WeightedAverageFields = [];
    
    Dump_XLS_Name = getXLSFilename('DumpGreekPnlReport');
    xlswrite(Dump_XLS_Name,[Overall_Header;Overall_Data]);
    
    [Overall_Header,Overall_Data] = consolidatedata(Overall_Header, Overall_Data,UniqueFileds,SumFields,OutputFields,WeightedAverageFields);
    
    %%
    
    OutPut_Fields = [{'contract_month','group_type','call_put_id','strike'}, Compute_Header];
    OutPut_Col = cellStrfind_exact(Overall_Header,OutPut_Fields) ;
    
    Total_Data = [];
    count = 1;
    
    Unique_SubPortfolio = unique( Overall_Data(:,SubPortfolio_Col) );
    
    for i = 1 : size(Unique_SubPortfolio,1)
        Current_Portfolio = Unique_SubPortfolio(i) ;
        Portfolio{count,1} = char(Current_Portfolio) ;
        Matched_Portfolio_Index = cellStrfind_exact(Overall_Data(:,SubPortfolio_Col) , Current_Portfolio)  ;
        
        Temp_Matrix_1 = Overall_Data(Matched_Portfolio_Index,:) ;
        
        Unique_Instrument = unique( Temp_Matrix_1(:,Instrument_Col) );
        
        for ii = 1 : size(Unique_Instrument,1)
            Current_Instrument =  Unique_Instrument(ii);
            Portfolio{count,2} = char(Current_Instrument) ;
            Matched_Instrument_Index = cellStrfind_exact(Temp_Matrix_1(:,Instrument_Col) , Current_Instrument) ;
            
            Temp_Matrix_2 = Temp_Matrix_1(Matched_Instrument_Index,:) ;
            
            Unique_CM = unique(Temp_Matrix_2(:,ContractMonth_Col));
            
            for k = 1 : size(Unique_CM,1)
                Current_CM = Unique_CM(k);
                Matched_CM = cellStrfind_exact(Temp_Matrix_2(:,ContractMonth_Col) , Current_CM) ;
                
                Temp_Data = Temp_Matrix_2(Matched_CM ,OutPut_Col) ;
                
                count = count + size(Temp_Data,1) ;
                
                Total_Data = [Total_Data ; Temp_Data] ;
            end
            
        end
    end
    count = count - 1;
    Portfolio{count,1} = {''};
    Portfolio{count,2} = {''};
    
    %% EXCEL Sheet Write
    
    Header = [{'Subportfolio','Instrument'},OutPut_Fields];
    Row_Data = [Portfolio , Total_Data] ;
    
    aa = cell(1, size(Row_Data,2));
    [~,Col_Sum] = cell2sum_Row_Col(Row_Data(:,(7:end)))  ;
    aa(1,7:end) = Col_Sum;
    aa(1,1) = {'Grand Total'}  ;
    
    Row_Data = [Row_Data ; aa] ;
    
    OutXLSFileName = getXLSFilename('Greek_PNL_Report');
    xlswrite(OutXLSFileName,[Header ; Row_Data],'Greek PNL') ;
    
    OutXLSFileName = fullfile(pwd,OutXLSFileName);
    try
        xls_delete_sheets(OutXLSFileName);
    catch
    end
    
else
    warndlg('No Data in DB');
end


