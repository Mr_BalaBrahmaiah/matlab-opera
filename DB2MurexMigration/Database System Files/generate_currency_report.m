function [OutErrorMsg,OutFilename] = generate_currency_report(InBUName)
% generate the Risk office Fx report
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/14 07:53:07 $
%  $Revision: 1.3 $
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

ViewName = 'helper_risk_office_currency_report';
[SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);

if ~strcmpi(Data,'No Data')
    SettleData = cell2dataset([SettleColNames;Data]);
    clear Data;
    
    IdxDead = strcmpi('dead',SettleData.deal_status);
    IdxLive = strcmpi('live',SettleData.deal_status);
    
    Realized = zeros(size(IdxDead));
    MTM = zeros(size(IdxLive));
    CumulativeMTM = zeros(size(IdxLive));
    
    % TODO :
    % 1. assuming all vaues are needed from mtm-usd; if needed from mtm-nc, needs
    % to be changed
    % End of TODO
    Realized(IdxDead) = SettleData.cumulative_mtm_usd(IdxDead);
    MTM(IdxLive) = SettleData.cumulative_mtm_usd(IdxLive);
    CumulativeMTM(IdxLive) = SettleData.cumulative_mtm_usd(IdxLive);
    
%     IdxMultiplication = strcmpi('multiplication',SettleData.mult_type);
%     IdxDivision = strcmpi('division',SettleData.mult_type);
    
    USDExposure = SettleData.original_lots;
%     FxExposure = zeros(size(USDExposure));
    FxExposure = SettleData.original_lots .* SettleData.original_premium;
%     MktUSDExposure = USDExposure;
    
%     FxExposure(IdxMultiplication) = SettleData.original_lots(IdxMultiplication) .* SettleData.original_premium(IdxMultiplication);
%     FxExposure(IdxDivision) = SettleData.original_lots(IdxDivision) ./ SettleData.original_premium(IdxDivision);    
      
    % making the buy currency(USD Exposure) positive and sell currency(FX Exposure) as negative as per RMO
    % comments
    USDExposure = abs(USDExposure);
    FxExposure  = abs(FxExposure);
    
    buy_currency = SettleData.contract_name;
    sell_currency = SettleData.contract_name;
    
    IdxUSDSellCurr = strcmpi('sold',SettleData.market_action);
    IdxFxSellCurr  = ~IdxUSDSellCurr; % market_action = bought
    
    sell_currency(IdxUSDSellCurr) = {'USD'};
    buy_currency(IdxFxSellCurr) = {'USD'};
    
    SettleData.sell_currency = sell_currency;
    SettleData.buy_currency  = buy_currency;
    
    USDExposure(IdxUSDSellCurr) = USDExposure(IdxUSDSellCurr) * -1;
    FxExposure(IdxFxSellCurr)   = FxExposure(IdxFxSellCurr) * -1;
    
%     UniqueFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
%         'buy_currency','sell_currency','settlement_date'};
%     SumFields = {'USDExposure','FxExposure','Realized','MTM','MktUSDExposure','CumulativeMTM'};
%     OutputFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
%         'buy_currency','sell_currency','USDExposure','FxExposure',...
%         'original_premium','settle_price','Realized','MTM','settlement_date','MktUSDExposure','CumulativeMTM'};
%     WeightedAverageFields = [];
%     InData = [SettleData.counterparty,SettleData.contract_name,SettleData.maturity_date,SettleData.transaction_date,...
%         SettleData.trade_id,SettleData.buy_currency,SettleData.sell_currency,num2cell(USDExposure),...
%         num2cell(FxExposure),num2cell(SettleData.original_premium),num2cell(SettleData.settle_price),...
%         num2cell(Realized),num2cell(MTM),SettleData.settlement_date,num2cell(MktUSDExposure),num2cell(CumulativeMTM)];
%     [OutputFields,OutData] = consolidatedata(OutputFields, InData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
%     
%     OutputFields = strrep(OutputFields,'original_premium','traded_price');
%     OutputFields = strrep(OutputFields,'MktUSDExposure','MarketAction Basis USDExposure');
    
UniqueFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
        'buy_currency','sell_currency','settlement_date'};
    SumFields = {'USDExposure','FxExposure','Realized','MTM'};
    OutputFields = {'counterparty','contract_name','maturity_date','transaction_date','trade_id',...
        'buy_currency','sell_currency','USDExposure','FxExposure',...
        'original_premium','settle_price','Realized','MTM','currency','settlement_date'};
    WeightedAverageFields = [];
    InData = [SettleData.counterparty,SettleData.contract_name,SettleData.maturity_date,SettleData.transaction_date,...
        SettleData.trade_id,SettleData.buy_currency,SettleData.sell_currency,num2cell(USDExposure),...
        num2cell(FxExposure),num2cell(SettleData.original_premium),num2cell(SettleData.settle_price),...
        num2cell(Realized),num2cell(MTM),SettleData.currency,SettleData.settlement_date];
    [OutputFields,OutData] = consolidatedata(OutputFields, InData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    OutputFields = strrep(OutputFields,'original_premium','traded_price');
%     OutputFields = strrep(OutputFields,'MktUSDExposure','MarketAction Basis USDExposure');
    
%     OutFilename = getXLSFilename('Fx_Report');
    Filename = ['Fx_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    xlswrite(OutFilename,[OutputFields;OutData]);
else
    OutErrorMsg = {'No FX trades found!'};
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end