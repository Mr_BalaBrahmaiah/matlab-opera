function save_rms_pnp_dump

Filename = generate_rms_dump;

try
    DestinationFolder = '\\10.190.7.222\Share\DBSystem_Reports\RMS_PNP_Dump';
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'RMS Pnp Dump');
catch
end