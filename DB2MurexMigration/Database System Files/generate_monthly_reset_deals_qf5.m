function [OutErrorMsg,OutXLSfileName] = generate_monthly_reset_deals_qf5(InBUName,varargin)

% InBUName = 'qf5';
try
    OutErrorMsg = {'No Errors'};
    OutXLSfileName = '';
    
    ObjDB = connect_to_database ;
    
    ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %% equity_currency_mapping_table_qf5
    
    SqlQuery_Currency_Map_Table = 'select * from equity_currency_mapping_table_qf5';
    [Currency_MapTbl_ColNames,Currency_MapTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Currency_Map_Table);
    Currency_Map_Tbl = cell2dataset([Currency_MapTbl_ColNames;Currency_MapTbl_Data]);
    
    %% We need to store live deals into helper_evaluation_reports_store_table_qf5 Table for Borrowing Report Purpose
    % When monthly reset happen
    
    aa = 0;  %% Make it zero for debugging purpose to ignore uploading time ,now no need this table
    
    if(aa)
        
        Default_ViewName = strcat('helper_evaluation_reports_view_',InBUName) ;
        SqlQuery_Live = ['select * from ',char(Default_ViewName),' where deal_status = ''live'' '] ;
        
        [FieldNames ,Upload_Live_Data ] = Fetch_DB_Data(ObjDB,SqlQuery_Live) ;
        
        try
            
            TableName = strcat('helper_evaluation_reports_store_table_',InBUName);
            set(ObjDB,'AutoCommit','off');
            SqlQuery = ['truncate ',char(TableName),' '];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            upload_in_database(TableName, Upload_Live_Data);
        catch
            OutErrorMsg = {['Data upload got failed in ',char(TableName)]} ;
        end
        
    end
    
    %% Trade_Reset_Master File
    
    Trade_Master_Reset_Tbl_Name = ['trade_reset_master_table_',char(InBUName)];
    
    if isempty(varargin)
        SqlQuery_ResetTbl = ['select * from ',char(Trade_Master_Reset_Tbl_Name),' where reset_date = ''',char(ValueDate),''' '] ;
    else
        % this condition means for cancel reset for particular counterparty
        User_Passed_Counterparty = varargin(1);
        
        Current_Month = str2double(datestr(char(SettleDate),5));
        Current_Year = str2double(datestr(char(SettleDate),10));
        
        if(Current_Month==1)
            %         PrevEOM_Date = datestr( eomdate(Current_Year-1, 12) ,'yyyy-mm-dd'); %% If Jan Comes Shold be Dec
            PrevEOM_Date = datestr( lbusdate(Current_Year-1,12) ,'yyyy-mm-dd');
        else
            PrevEOM_Date = datestr( eomdate(Current_Year, Current_Month-1) ,'yyyy-mm-dd');
        end
        
        Previous_Reset_Month = datestr(PrevEOM_Date,'mmm-yy');
        
        SqlQuery_ResetTbl = ['select * from ',char(Trade_Master_Reset_Tbl_Name),' where reset_month = ''',char(Previous_Reset_Month),...
            ''' and counterparty_parent = ''',char(User_Passed_Counterparty),''' '] ;
    end
    
    [Trade_MasterTbl_Fields , Trade_MasterTbl_Data] =  Fetch_DB_Data(ObjDB,SqlQuery_ResetTbl);
    
    if( (size(Trade_MasterTbl_Data,2) <= 1) || sum(sum(strcmpi(Trade_MasterTbl_Data,'No Data'))) )
        OutErrorMsg = {['Corresponding Monthly Reset Date not Available in ',char(Trade_Master_Reset_Tbl_Name)]};
        return;
    end
    
    Trade_Master_ResetTbl_Data = cell2dataset([Trade_MasterTbl_Fields ; Trade_MasterTbl_Data]);
    
    
    Unique_CounterParty = unique(Trade_Master_ResetTbl_Data.counterparty_parent);
    Unique_CounterParty_Query = ['''',strrep(convertCell2Char(Unique_CounterParty),',',''','''),''''];
    
    Reset_Month = unique(Trade_Master_ResetTbl_Data.reset_month);
    
    if(size(Reset_Month,1) > 1)
        OutErrorMsg = {['We were getting more than one reset month for corresponding value date in ',char(Trade_Master_Reset_Tbl_Name)]};
        return;
    end
    
    %% monthly_reset_date_table_qf5 Reset Date Check
    
    Reset_Tbl_Name = ['monthly_reset_date_table_',char(InBUName)];
    
    % Reset_Month = datestr(SettleDate,'mmm-yy');
    % SqlQuery_ResetTbl = ['select * from ',char(Reset_Tbl_Name),' where reset_month = ''',char(Reset_Month),''' '] ;
    % [ResetTbl_Fields , ResetTbl_Data] =  Fetch_DB_Data(ObjDB,SqlQuery_ResetTbl);
    %
    % if( (size(ResetTbl_Data,2) <= 1) || sum(strcmpi(ResetTbl_Data,'No Data')) )
    %     OutErrorMsg = {'Reset Date Not Available for Corresponding Month'};
    %     return;
    % end
    
    %% Fetch Data
    SqlQuery = ['select * from equity_swaps_underlying_prices_table_qf5 where value_date=''',char(ValueDate),''' '];
    [EquitySwaps_Fields,EquitySwaps_UnderPriceTable] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    EquitySwaps_UnderPriceTable = cell2dataset([EquitySwaps_Fields;EquitySwaps_UnderPriceTable]);
    
    
    if(exist('User_Passed_Counterparty','var'))
        Last_Reset_Date = Trade_Master_ResetTbl_Data.reset_date;
        SqlQuery = ['select * from deal_ticket_table_with_latest_versions_active_lots_view_qf5 where counterparty_parent in (',Unique_CounterParty_Query,') and deal_status = ''live'' and is_suspended = 0 and subportfolio not like  ''RES-%'' and transaction_date < ''',char(Last_Reset_Date),''' '];
    else
        SqlQuery = ['select * from deal_ticket_table_with_latest_versions_active_lots_view_qf5 where counterparty_parent in (',Unique_CounterParty_Query,') and deal_status = ''live'' and is_suspended = 0 and subportfolio not like  ''RES-%'''];
    end
    
    
    [DealTRData_Fields , DealTRData] =  Fetch_DB_Data(ObjDB,SqlQuery);
    
    
    
    %%
    if(size(DealTRData,2) > 1)
        
        DealTRData = cell2dataset([DealTRData_Fields ;DealTRData]);
        
        %% Offset Deal TR Data
        OffsetDealTRData = DealTRData ;%% cell2dataset([DealTRData_Fields ; cell(size(DealTRData,1),size(DealTRData,2))]); %% Initialize
        
        OffsetDealTRData.trade_id = strcat('DUP-',DealTRData.trade_id);
        OffsetDealTRData.parent_trade_id = strcat('DUP-',DealTRData.parent_trade_id);
        OffsetDealTRData.transaction_date(:,:) = SettleDate ;
        
        Bought_Index = cellStrfind_exact(DealTRData.market_action,{'Bought'});
        Sold_Index = cellStrfind_exact(DealTRData.market_action,{'Sold'});
        OffsetDealTRData.market_action(Bought_Index) = {'Sold'};
        OffsetDealTRData.market_action(Sold_Index) = {'Bought'};
        
        OffsetDealTRData.original_lots = DealTRData.active_lots .* -1 ;
        OffsetDealTRData.lots_to_be_netted = DealTRData.active_lots .* -1 ;
        OffsetDealTRData.deal_status(:,:) = {'dead'};
        OffsetDealTRData.version_comments(:,:) = {'offset_deal_resetted'};
        
        
        %% Reset Deal TR Data
        ResetDealTRData = DealTRData ;%% cell2dataset([DealTRData_Fields ; cell(size(DealTRData,1),size(DealTRData,2))]); %% Initialize
        
        ResetDealTRData.trade_id = strcat('RST-',DealTRData.trade_id);
        ResetDealTRData.parent_trade_id = strcat('RST-',DealTRData.parent_trade_id);
        ResetDealTRData.transaction_date(:,:) = SettleDate ;
        
        ResetDealTRData.market_action = DealTRData.market_action;
        
        ResetDealTRData.original_lots = DealTRData.active_lots;
        ResetDealTRData.lots_to_be_netted = DealTRData.active_lots;
        
        ResetDealTRData.deal_status(:,:) = {'live'};
        ResetDealTRData.version_comments(:,:) = {'monthly_reset_deal'};
        
        if(exist('User_Passed_Counterparty','var'))
            ResetDealTRData.reset_date(:,:) = Last_Reset_Date;
        else
            ResetDealTRData.reset_date(:,:) = ValueDate;
        end
        
        %% Get
        
        for i = 1 : size(DealTRData)
            Current_SecutiyID = DealTRData.security_id(i);
            Current_CounterParty = DealTRData.counterparty_parent(i);
            
            [~,bb] = strtok(DealTRData.instrument(i),'.');
            [Current_StockType,~] = strtok(bb,'.');
            Current_CurrencyType = Currency_Map_Tbl.currency( strcmpi(Currency_Map_Tbl.counterparty_parent,Current_CounterParty) & strcmpi(Currency_Map_Tbl.stock_type,Current_StockType) ) ;
            
            Matched_Index = strcmpi(EquitySwaps_UnderPriceTable.underlying_id,Current_SecutiyID) & strcmpi(EquitySwaps_UnderPriceTable.counterparty_parent,Current_CounterParty) ...
                & strcmpi(EquitySwaps_UnderPriceTable.value_date,ValueDate) & strcmpi(EquitySwaps_UnderPriceTable.currency,Current_CurrencyType);
            
            if(~isempty(find(Matched_Index)))
                OffsetDealTRData.premium(i) = EquitySwaps_UnderPriceTable.settle_value(Matched_Index);
                OffsetDealTRData.premium_nc(i) = EquitySwaps_UnderPriceTable.settle_value_nc(Matched_Index);
                
                ResetDealTRData.premium(i) = EquitySwaps_UnderPriceTable.settle_value(Matched_Index);
                ResetDealTRData.premium_nc(i) = EquitySwaps_UnderPriceTable.settle_value_nc(Matched_Index);
            else
                OffsetDealTRData.premium(i) = NaN;
                OffsetDealTRData.premium_nc(i) = NaN;
                
                ResetDealTRData.premium(i) = NaN;
                ResetDealTRData.premium_nc(i) = NaN;
            end
            
        end
        
        
        %% Removed Headers
        DealTRData.original_lots = DealTRData.active_lots; %% newly added line(01-04-2019)
        DealTRData.active_lots = [];
        DealTRData.is_suspended = [];
        DealTRData.netting_status = [];
        DealTRData.portfolio = [];
        DealTRData.deal_status(:,:) = {'dead'};
        DealTRData.version_comments(:,:) = {'actual_deal_resetted'};
        
        OffsetDealTRData.active_lots = [];
        OffsetDealTRData.is_suspended = [];
        OffsetDealTRData.netting_status = [];
        OffsetDealTRData.portfolio = [];
        
        ResetDealTRData.active_lots = [];
        ResetDealTRData.is_suspended = [];
        ResetDealTRData.netting_status = [];
        ResetDealTRData.portfolio = [];
        
        % OffsetDealTRData = dataset2cell(OffsetDealTRData);
        % ResetDealTRData = dataset2cell(ResetDealTRData);
        % Overall_Reset_TRData = [OffsetDealTRData ; ResetDealTRData(2:end,:)]; %% Same Headers
        
        %% Consolidate for Reset Deals
        
        ResetDealTRData = dataset2cell(ResetDealTRData);
        ResetDealTRData_Header =  ResetDealTRData(1,:);
        ResetDealTRData = ResetDealTRData(2:end,:);
        
        UniqueFields = {'transaction_date','subportfolio', 'instrument','trade_group_type' ,'security_id','premium','counterparty_parent'}; %% {'market_action','equity_swaps_trade_type'}
        SumFields = {'original_lots','lots_to_be_netted'};
        OutputFields = ResetDealTRData_Header ;
        WeightedAverageFields = [];
        [ResetDealTRData_Header,ResetDealTRData_Consolidate] = consolidatedata(ResetDealTRData_Header, ResetDealTRData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        %% Market Action & Equity Swaps Trade Type
        % This Section Based on Counterparty if any new counterparty will
        % comes we nedd to handle that corresponding counterparty
        
        MarketAction_Col = cellStrfind_exact(ResetDealTRData_Header,{'market_action'});
        EQD_Swaps_Col = cellStrfind_exact(ResetDealTRData_Header,{'equity_swaps_trade_type'});
        CounterParty_Col = cellStrfind_exact(ResetDealTRData_Header,{'counterparty_parent'});
        OriginalLots_Col = cellStrfind_exact(ResetDealTRData_Header,{'original_lots'});
        
        Unique_CounterParty = unique(ResetDealTRData_Consolidate(:,CounterParty_Col));
        
        for k = 1 : length(Unique_CounterParty)
            
            Current_Counterparty_ResetDealTRData = Unique_CounterParty(k);
            
            Positive_Index = cell2mat(ResetDealTRData_Consolidate(:,OriginalLots_Col)) > 0 & strcmpi(ResetDealTRData_Consolidate(:,CounterParty_Col),Current_Counterparty_ResetDealTRData);
            Negative_Index = cell2mat(ResetDealTRData_Consolidate(:,OriginalLots_Col)) < 0 & strcmpi(ResetDealTRData_Consolidate(:,CounterParty_Col),Current_Counterparty_ResetDealTRData);
            Zero_Index = cell2mat(ResetDealTRData_Consolidate(:,OriginalLots_Col)) == 0 & strcmpi(ResetDealTRData_Consolidate(:,CounterParty_Col),Current_Counterparty_ResetDealTRData);
            
            ResetDealTRData_Consolidate(Positive_Index,MarketAction_Col) = {'BOUGHT'} ;
            ResetDealTRData_Consolidate(Negative_Index,MarketAction_Col) = {'SOLD'} ;
            
            if(strcmpi(Current_Counterparty_ResetDealTRData,'CICC'))
                ResetDealTRData_Consolidate(Positive_Index,EQD_Swaps_Col) = {'BUY'} ;
                ResetDealTRData_Consolidate(Negative_Index,EQD_Swaps_Col) = {'SELL'} ;
            else
                ResetDealTRData_Consolidate(Positive_Index,EQD_Swaps_Col) = {'BUY TO COVER'} ;
                ResetDealTRData_Consolidate(Negative_Index,EQD_Swaps_Col) = {'SELL TO OPEN'} ;
            end
            ResetDealTRData_Consolidate(Zero_Index,:) = [] ; %% No Need to Book these trades
            
        end
        
        %% Trade ID
        [TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(ResetDealTRData_Consolidate(:,1),InBUName);
        
        TraderName_Col = cellStrfind_exact(ResetDealTRData_Header,{'trader_name'});
        TradeID_Col = cellStrfind_exact(ResetDealTRData_Header,{'trade_id'});
        ParTradeID_Col = cellStrfind_exact(ResetDealTRData_Header,{'parent_trade_id'});
        
        ResetDealTRData_Consolidate(:,TraderName_Col) = {'RESET'};
        ResetDealTRData_Consolidate(:,TradeID_Col) = TradeId; %% Changing Trade ID
        ResetDealTRData_Consolidate(:,ParTradeID_Col) = TradeId;
        
        
        %% Excel Write
        
%       OutXLSfileName = getXLSFilename('Monthly_Reset_Deals');
        OutXLSfileName = getXLSFilename(['Monthly_Reset_Deals_',InBUName]);
        
        xlswrite(OutXLSfileName,dataset2cell(DealTRData),'Actual_deals_for_reset');
        xlswrite(OutXLSfileName,dataset2cell(OffsetDealTRData),'Offset_Deals');
        xlswrite(OutXLSfileName,[ResetDealTRData_Header ; ResetDealTRData_Consolidate],'Reset_Deals');
        
        xls_delete_sheets(fullfile(pwd,OutXLSfileName));
        
        %         msgbox('Process Finished','Finish') ;
        
        %% Reset Date Updated
        if(~exist('User_Passed_Counterparty','var'))
            try
                SqlQuery_Update = ['update ',char(Reset_Tbl_Name),' set reset_date=''',char(ValueDate),''' , settle_date=''',char(SettleDate),...
                    ''' , flag_monthly_reset=''YES'' where reset_month = ''',char(Reset_Month),''' and counterparty_parent in (',Unique_CounterParty_Query,') '];
                Curs = exec(ObjDB,SqlQuery_Update);
                
                if ~isempty(Curs.Message)
                    OutErrorMsg = {Curs.Message};
                end
                
                
                SqlQuery_Update_1 = ['update ',char(Trade_Master_Reset_Tbl_Name),' set reset_date=''',char(ValueDate),''' , settle_date=''',char(SettleDate),...
                    ''' where reset_month = ''',char(Reset_Month),''' and counterparty_parent in (',Unique_CounterParty_Query,') '];
                Curs = exec(ObjDB,SqlQuery_Update_1);
                
                if ~isempty(Curs.Message)
                    OutErrorMsg = {Curs.Message};
                end
                
            catch
                
            end
        end
        
        
        %         ResetDate_Col = cellStrfind_exact(ResetTbl_Fields,{'reset_date'});
        %         ResetTbl_Data(:,ResetDate_Col) = ValueDate;
        %
        %         TableName = Reset_Tbl_Name;
        %         set(ObjDB,'AutoCommit','off');
        %         SqlQuery = ['delete from ',TableName,' where reset_month = ''',char(Reset_Month),''''];
        %         curs = exec(ObjDB,SqlQuery);
        %         commit(ObjDB);
        %         if ~isempty(curs.Message)
        %             disp(curs.Message);
        %         end
        %         set(ObjDB,'AutoCommit','on');
        %
        %         upload_in_database(TableName, ResetTbl_Data);
        
    else
        OutErrorMsg = {'No Data Found'} ;
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end
