
%% {'CT-CC_PRICE','CT-CC_PR2', 'CT-USA_EQ16','CT-USA_PR'}

InBUName = 'cot';
InputFilterValues = {'subportfolio',{'CT-CC_PRICE','CT-USA_EQ16','CT-USA_EQ_UPS','CT-AUST_PR','CT-CC_PR2','CT-CHINA_PR','CT-CHINA_PR2','CT-TURKMEN_PR','CT-SECO_PR','CT-USA_PR','CT-BFASO_PR'},...
   'maturity','live'};
InputScenarios = [{'on',20,0.5,''};{'off',3,0.25,''};{'off',3,10,''}];
SummmaryParameters = {'product_code','instrument','subportfolio','contract_month','derivative_type','strike','exchange_name'};
PricingType = 'settle';
OutputFields = {'active_lots','price','pricemove','delta','gamma','vega','theta','m2m_usd'};


%% generate_sensitivity_report   %% generate_sensitivity_report_parfor

tic;
try
    [OutErrorMsg,OutFilename] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
    
    msgbox(['Without Parfor : ', num2str(toc) ]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

%% generate_sensitivity_report_parfor

% tic;
% try
%     [OutErrorMsg,OutFilename] = generate_sensitivity_report_parfor(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
%
%     msgbox(['With Parfor : ', num2str(toc) ]);
%
% catch ME
%     OutErrorMsg = cellstr(ME.message);
%
%     errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
%         ME.stack(1).name, ME.stack(1).line, ME.message);
%     fprintf(1, '%s\n', errorMessage);
%     %     uiwait(warndlg(errorMessage));
%
% end

%% Use this Function

% create_risk_matrix

%% CalcPrice Only  %% calcPriceForScenarios %% calcPriceForScenarios_updated

% load('sensitivity_CalcPrice_Input.mat')
%
% tic;
% OutFilename = calcPriceForScenarios_SubFunction(InBUName,SelData,InReportHeader,PricingType,...
%     PriceStepParameter,PriceNumSteps,PriceStepSize,VolNumSteps,VolStepSize,TimeNumSteps,TimeStepSize,...
%     SummmaryParameters,SumFields,OutputFields,WeightedAverageFields,OutFilename);
% fprintf('CalcPrice : %f\n',toc);

%%


% % RMOPTReport = 'settle_SensitivityReport_20-Oct-2016_17-31-23.xlsx';
%
% InBUName = 'agf';
% InputFilterValues = {'product_code',{'all'}};
% InputScenarios = [{'on',5,2,''};{'on',3,1,''};{'off',3,10,''}];
% SummmaryParameters = {'product_code','contract_month'};
% PricingType = 'settle';
% OutputFields = {'mtm-usd','delta','gamma','vega'};
%
% [OutErrorMsg,RMOPTReport] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
%
% [~,~,RawData] = xlsread(RMOPTReport, 'Summary Report');
% ReportHeader = RawData(1,:);
% ReportData   = RawData(2:end,:);
%
% Rounded_Header = {'mtm-usd','delta','delta_in_metric_tonnes','vega'};
% Two_Decimal_Header = {'gamma'};
% Unique_Header = 'product_code';
%
% [~,~,RMOPTReport] = xlswrite_sep_sheet_4_unique_fields(Rounded_Header,Two_Decimal_Header,Unique_Header,ReportHeader,ReportData,'CC_IV_RM_OPT',1);








