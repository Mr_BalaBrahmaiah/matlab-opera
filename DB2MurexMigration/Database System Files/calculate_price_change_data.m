function [OutErrorMsg,OutFilename] = calculate_price_change_data

OutErrorMsg = {'No errors'};
OutFilename = '';
try
    update_future_cont_series_table;
    
    % global ObjDB;
    ObjDB = connect_to_database;
    
    [ColNames,Data] = read_from_database('helper_calculate_price_vol_change_view',0);
    PriceVolData = cell2dataset([ColNames;Data]);
    %
    % ProductCode = strtrim(cellfun(@(x) x(1:2), PriceVolData.underlying_id, 'UniformOutput', false));
    % PriceVolData.ProductCode = ProductCode;
    
    VDate = fetch(ObjDB,'select value_date from valuation_date_table');
    Sys_SDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    % HolDates = {'2015-04-03';'2015-05-25'};
    % HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    % SDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    % SBusDay = datenum(SDate,'yyyy-mm-dd');
    % SSDate = cellstr(datestr(busdate(SBusDay,-1,HolidayVec),'yyyy-mm-dd'));
    % SqlQuery = ['select * from future_cont_series_table where settlement_date = ''',char(SDate),''''];
    % [ColNames,Data] = read_from_database('future_cont_series_table',0,SqlQuery);
    % FutureContSeriesData = cell2dataset([ColNames;Data]);
    % [ProductCode,MaturityCode] = strtok(FutureContSeriesData.underlying_id);
    % Unique_ProductCode = unique(strtrim(ProductCode)) ;
    
    % Unique_ProductCode = fetch(ObjDB,'select distinct(bbg_product_code) from product_master_table where invenio_product_code like ''CM%''');  %% 'select distinct(product_code) from var_aggregation_level_table_cfs'
    Unique_ProductCode = fetch(ObjDB,'select distinct(bbg_format_code) from var_product_code_mapping_table');
    % Unique_ProductCode(cellStrfind_exact(Unique_ProductCode,{'ccl'})) = [];
    
    %%
    [FutureContSeriesData_Cols,FutureContSeriesData_All] = Fetch_DB_Data(ObjDB,'select * from future_cont_series_table');
    FutureContSeriesData_All =  cell2dataset([FutureContSeriesData_Cols;FutureContSeriesData_All]);
    
    [AssetClass,Remain_ProductCode] = strtok(FutureContSeriesData_All.underlying_id,' ');
    [ProductCode,Remain_ProductCode] = strtok(Remain_ProductCode);
    ProductCode = strtrim(ProductCode);
    FutureContSeriesData_All.ProductCode = ProductCode;
    
    %%
    OutPriceChangeData = [];
    ProductCode_SDate = [];
    
    for i = 1 : length(Unique_ProductCode)
        Current_ProductCode = Unique_ProductCode(i);
        SqlQuery_SDate = ['select settle_date from product_busdays_table where product_code = ''',char(Current_ProductCode),''' and value_date = ''',char(VDate),''' '  ] ;
        [~,SDate] = Fetch_DB_Data(ObjDB,SqlQuery_SDate);
        
        SqlQuery_SSDate = ['select settle_date from product_busdays_table where product_code = ''',char(Current_ProductCode),''' and value_date = ''',char(SDate),''' '  ] ;
        [~,SSDate] = Fetch_DB_Data(ObjDB,SqlQuery_SSDate);
        
        IdxSDates = strcmpi(SDate,PriceVolData.settlement_date); % & strcmpi(Current_ProductCode,PriceVolData.ProductCode);
        IdxSSDates = strcmpi(SSDate,PriceVolData.settlement_date);  %& strcmpi(Current_ProductCode,PriceVolData.ProductCode);
        
        SDataPriceVol = PriceVolData(IdxSDates,:);
        SSDataPriceVol = PriceVolData(IdxSSDates,:);
        
        %      SqlQuery = ['select * from future_cont_series_table where settlement_date = ''',char(SDate),''''];
        %     %     [ColNames,Data] = read_from_database('future_cont_series_table',0,SqlQuery);
        %     [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
        %     FutureContSeriesData = cell2dataset([ColNames;Data]);
        
        Id = strcmpi(Current_ProductCode,FutureContSeriesData_All.ProductCode) & strcmpi(SDate,FutureContSeriesData_All.settlement_date);
        FutureContSeriesData  = FutureContSeriesData_All(Id,:);
        
        NumRecords = size(FutureContSeriesData,1);
        
        for iR = 1:NumRecords
            
            IdxUndId = strcmpi(FutureContSeriesData.underlying_id{iR},SSDataPriceVol.underlying_id);
            if any(IdxUndId)
                PrevPriceData = SSDataPriceVol.price(IdxUndId);
                if isnan(PrevPriceData)
                    PrevPriceData = 0;
                end
            else
                PrevPriceData = 0;
            end
            
            IdxUndId = strcmpi(FutureContSeriesData.underlying_id{iR},SDataPriceVol.underlying_id);
            if any(IdxUndId)
                CurrPriceData = SDataPriceVol.price(IdxUndId);
                if isnan(CurrPriceData)
                    CurrPriceData = 0;
                end
            else
                CurrPriceData = 0;
            end
            PriceChange = CurrPriceData - PrevPriceData;
            
            FutureContMonth = FutureContSeriesData.cont_month(iR);
            UnderlyingId    = FutureContSeriesData.underlying_id(iR);
            
            RowPriceChangeData = [SDate,FutureContMonth,PriceChange,UnderlyingId];  %% SDate
            OutPriceChangeData = [OutPriceChangeData;RowPriceChangeData];
            
        end
        
        ProductCode_SDate = [ProductCode_SDate ; [Current_ProductCode,SDate] ];
    end
    
    PriceFilename = getXLSFilename('Price_Change_Data');
    
    PriceHeader = {'settlement_date','cont_month','price_change','underlying_id'};
    
    OutPriceChangeData = [PriceHeader;OutPriceChangeData];
    
    [OutPriceChangeData] = Get_Exact_Product_SDate_Data(OutPriceChangeData,ProductCode_SDate,1) ; %% New
    
    CleanedPriceData = clean_price_vol_change_data(OutPriceChangeData);  %% cell2dataset(OutPriceChangeData)
    
    [OutErrorMsg,CleanedPriceData] = PriceChange_Mul_Div_Factor(ObjDB,CleanedPriceData,Sys_SDate); %% New
    
    if(~strcmpi(OutErrorMsg,'No Errors'))
        return;
    end
    
    CleanedPriceData = dataset2cell(CleanedPriceData);
    
    CleanedPriceData(:,1) = Sys_SDate;
    
    OutFilename = getXLSFilename('CleanedPriceData');
    xlswrite(OutFilename,CleanedPriceData);
    
    Data = CleanedPriceData(2:end,1:3);
    TableName = 'var_cont_price_change_table';
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(Sys_SDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    upload_in_database(TableName,Data);
    
    PriceHeader = {'settlement_date','cont_month','price_change'};
    xlswrite(PriceFilename,[PriceHeader;sortrows(Data)]);
    
    try
        SharedFolder = get_reports_foldername;
        DestinationFile = fullfile(SharedFolder,OutFilename);
        movefile(OutFilename,DestinationFile);
    catch ME
        OutErrorMsg = cellstr(ME.message);
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end