function [OutErrorMsg,TradersReportSummaryFilename,...
    PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_all_MO_reports_coc(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

%% Traders Report Summary

[OutErrorMsg1,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    TradersReportSummaryFilename = char(OutErrorMsg1);
end

cprintf('key','%s finished\n', 'Traders Report Summary');

%% PnP Report

[OutErrorMsg2,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    PnpOptionDumpFileName = char(OutErrorMsg2);
    PnpFutureDumpFileName = char(OutErrorMsg2);
    PnpMUsDumpFileName = char(OutErrorMsg2);
end

cprintf('key','%s finished\n', 'PnP Report');

%% Overall_ErrorMsg
Overall_ErrorMsg = [OutErrorMsg1;OutErrorMsg2];

end


