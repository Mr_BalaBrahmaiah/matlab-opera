% function generate_helper_VAR_dump

objDB = connect_to_database;

HolDates = {'2015-04-03';'2015-05-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2015-01-01'), datenum('2015-07-31'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

ConsolidatedDataDump = [];

for i = length(VBusDays):-1:1
    
    SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end

ViewName = 'temp_helper_var_report';
[ColNames,Data] = read_from_database(ViewName,0);

UniqueFields = {'settlement_date','subportfolio','counterparty','underlying_future_id','option_expiry','derivative_type','strike'};
SumFields = {'active_lots'};
OutputFields = {'settlement_date','subportfolio','counterparty','underlying_future_id','future_expiry','option_expiry','derivative_type','strike','active_lots','underlying_future_price','contract_settlement_price','contract_month'};
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

PosActiveLots = strcmpi('active_lots',OutputFields);
IdxZeroLots = isnumericequal(cell2mat(OutData(:,PosActiveLots)),0);

OutData(IdxZeroLots,:) = [];

try
    OutData = sortrows(OutData,[1,2,4,6,7,8]);
catch
end

OutFilename = getXLSFilename('helper_var_DBdump');
xlswrite(OutFilename,[OutputFields;OutData]);
 
ConsolidatedDataDump = [ConsolidatedDataDump;OutData];
    
end

OutFilename = getXLSFilename('Consolidated_VAR_DBdump');
xlswrite(OutFilename,[OutputFields;ConsolidatedDataDump]);