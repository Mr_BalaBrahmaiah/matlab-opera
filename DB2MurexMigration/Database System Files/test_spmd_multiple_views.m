disp('without par');
tic
ViewName = 'helper_5_1_traders_pricing_subportfoliovalues_view';
[TradersColNames,TradersData] = read_from_database(ViewName,0);

ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0);
toc

% start the parallel pool of matlab workers
parpool('local',2);

tic
spmd
    switch(labindex)
        case 1
            ViewName = 'helper_5_1_traders_pricing_subportfoliovalues_view';
            [ColNames,Data] = read_from_database(ViewName,0);
        case 2
            ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
            [ColNames,Data] = read_from_database(ViewName,0);            
    end
    
end

TradersColNames = ColNames{1}; TradersData = Data{1};
SettleColNames  = ColNames{2}; SettleData = Data{2};
toc

delete(gcp)