function [OutErrorMsg,OutFilename] = generate_ff_position_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('FF_Position_Report');
    
    ViewName = 'helper_funds_reports_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
        Get_CallPut = Data(:,Find_CallPut_Col);
        ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
        Data(ispresent_Call,Find_CallPut_Col) = {'call'};
        ispresent_Put = cellStrfind(Get_CallPut,'p');
        Data(ispresent_Put,Find_CallPut_Col) = {'put'};
        
        PosStrike = strcmpi('strike',ColNames);
        PosVol = strcmpi('p1_vol',ColNames);
        PosConvFactor = strcmpi('conv_factor',ColNames);
        
        Strike = cell2mat(Data(:,PosStrike)) ./ cell2mat(Data(:,PosConvFactor));
        Data(:,PosStrike) = num2cell(Strike);
        Vol = cell2mat(Data(:,PosVol)) .* 100;
        Data(:,PosVol) = num2cell(Vol);
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
%         ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FUT'});
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FUT','CSO'}); %%% newly added Group Type is CSO (2019-07-18)
        Data = Data(ispresent_FUT_OFUT,:);
        
        UniqueFields = {'portfolio','instrument','group_type','contract_month','call_put_id','strike','counterparty_parent'};% added counterparty parent
        %%% old code
        %SumFields = {'original_lots','settle_delta_1','cumulative_mtm_usd','settle_gamma_11','settle_theta','settle_vega_1','mtm_usd'};
        %OutputFields = {'portfolio','instrument','group_type','contract_month','call_put_id','strike',...
        %    'original_lots','settle_delta_1','cumulative_mtm_usd','settle_gamma_11','settle_theta','settle_vega_1','p1_vol','mtm_usd'};
        
        %%% Newly added line
        SumFields = {'original_lots','settle_delta_1','cumulative_mtm_usd','settle_gamma_11','gamma_lots','settle_theta','settle_vega_1','mtm_usd'};
        OutputFields = {'portfolio','instrument','group_type','contract_month','call_put_id','strike',...
            'original_lots','settle_delta_1','cumulative_mtm_usd','settle_gamma_11','gamma_lots','settle_theta','settle_vega_1','p1_vol','mtm_usd','counterparty_parent'};% added counterparty parent
        %%% End code
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        PosTheta = strcmpi('settle_theta',OutputFields);
        OutData(:,PosTheta) = num2cell(cell2mat(OutData(:,PosTheta)) .* (7/5));
        
        %%% old code
        %OutHeader = {'Portfolio','Underlying','Group','Maturity Label','Call/PutId','Strike','Nominal','Delta','Cummulative BV','Gamma','Theta','Vega','Theo.Volatility','M2M'};
        %%% newly added line
        OutHeader = {'Portfolio','Underlying','Group','Maturity Label','Call/PutId','Strike','Nominal','Delta','Cummulative BV','Gamma','Gamma_Lots','Theta','Vega','Theo.Volatility','M2M'};
        xlswrite(OutFilename,[OutHeader;OutData(:,(1:end-1))]);
        
        %%% Newly added on 22-01-2020, for counterparty parent
        OutHeader = {'Portfolio','Underlying','Group','Maturity Label','Call/PutId','Strike','Nominal','Delta','Cummulative BV','Gamma','Gamma_Lots','Theta','Vega','Theo.Volatility','M2M','Counterparty'};
        xlswrite(OutFilename,[OutHeader;OutData],'Sheet2');
    
        xls_change_activesheet(fullfile(pwd,OutFilename) ,'Sheet1');
        
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end