function startup()
if isdeployed && usejava('swing')
    [major, minor] = mcrversion;
    if major == 9 && minor == 0
        if ispc
            javax.swing.UIManager.setLookAndFeel('com.sun.java.swing.plaf.windows.WindowsLookAndFeel');
        elseif isunix
            javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');
        elseif ismac
            javax.swing.UIManager.setLookAndFeel('com.apple.laf.AquaLookAndFeel');
        end
    end
end
% javaaddpath('D:\MySQL_Connector\mysql-connector-java-5.1.25-bin.jar');
javaaddpath('mysql-connector-java-5.1.25-bin.jar');

% javaaddpath('D:\00_MUTHUKUMAR\WORKS\00_Office\DB2MurexMigration\Database System Files\jheapcl\MatlabGarbageCollector.jar');