function calculate_price_vol_change

update_cont_series_tables;

ObjDB = connect_to_database;

SqlQuery = 'select * from helper_calculate_price_vol_change_view';
Data = fetch(ObjDB,SqlQuery);
ColNames = columns(ObjDB,ObjDB.Instance,ObjDB.Instance,'helper_calculate_price_vol_change_view');
PriceVolData = cell2dataset([ColNames;Data]);

HolDates = {'2015-04-03';'2015-05-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

SDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
SBusDay = datenum(SDate,'yyyy-mm-dd');
SSDate = cellstr(datestr(busdate(SBusDay,-1,HolidayVec),'yyyy-mm-dd'));

OutPriceChangeData = [];
OutVolChangeData = [];

IdxSDates = strcmpi(SDate,PriceVolData.settlement_date);
IdxSSDates = strcmpi(SSDate,PriceVolData.settlement_date);

SDataPriceVol = PriceVolData(IdxSDates,:);
SSDataPriceVol = PriceVolData(IdxSSDates,:);

SqlQuery = ['select * from future_cont_series_table where settlement_date = ''',char(SDate),''''];
Data = fetch(ObjDB,SqlQuery);
ColNames = columns(ObjDB,ObjDB.Instance,ObjDB.Instance,'future_cont_series_table');
FutureContSeriesData = cell2dataset([ColNames;Data]);

NumRecords = size(FutureContSeriesData,1);

for iR = 1:NumRecords
    
    IdxUndId = strcmpi(FutureContSeriesData.underlying_id{iR},SSDataPriceVol.underlying_id);
    if any(IdxUndId)
        PrevPriceData = SSDataPriceVol.price(IdxUndId);
        if isnan(PrevPriceData)
            PrevPriceData = 0;
        end
    else
        PrevPriceData = 0;
    end
    
    IdxUndId = strcmpi(FutureContSeriesData.underlying_id{iR},SDataPriceVol.underlying_id);
    if any(IdxUndId)
        CurrPriceData = SDataPriceVol.price(IdxUndId);
        if isnan(CurrPriceData)
            CurrPriceData = 0;
        end
    else
        CurrPriceData = 0;
    end    
    PriceChange = CurrPriceData - PrevPriceData;
    
    FutureContMonth = FutureContSeriesData.cont_month(iR);
    UnderlyingId    = FutureContSeriesData.underlying_id(iR);
    
    RowPriceChangeData = [SDate,FutureContMonth,PriceChange,UnderlyingId];
    OutPriceChangeData = [OutPriceChangeData;RowPriceChangeData];
end

SqlQuery = ['select * from option_cont_series_table where settlement_date = ''',char(SDate),''''];
Data = fetch(ObjDB,SqlQuery);
ColNames = columns(ObjDB,ObjDB.Instance,ObjDB.Instance,'option_cont_series_table');
OptionContSeriesData = cell2dataset([ColNames;Data]);

NumRecords = size(OptionContSeriesData,1);

for iR = 1:NumRecords
    IdxVolId = strcmpi(OptionContSeriesData.underlying_vol_id{iR},SSDataPriceVol.vol_id);
    if any(IdxVolId)
        PrevVolData = SSDataPriceVol.vol(IdxVolId);
        if isnan(PrevVolData)
            PrevVolData = 0;
        end
    else
        PrevVolData = 0;
    end
    
    IdxVolId = strcmpi(OptionContSeriesData.underlying_vol_id{iR},SDataPriceVol.vol_id);
    if any(IdxVolId)
        CurrVolData = SDataPriceVol.vol(IdxVolId);
        if isnan(CurrVolData)
            CurrVolData = 0;
        end
    else
        CurrVolData = 0;
    end    
    
    VolChange = CurrVolData - PrevVolData;
    
    OptionContMonth = OptionContSeriesData.underlying_vol_id_cont_month(iR);  
    VolId   = OptionContSeriesData.underlying_vol_id(iR);
    
    RowVolChangeData = [SDate,OptionContMonth,VolChange,VolId];
    OutVolChangeData = [OutVolChangeData;RowVolChangeData];
    
end

PriceFilename = getXLSFilename('Price_Change_Data');
VolFilename = getXLSFilename('Vol_Change_Data');

PriceHeader = {'settlement_date','cont_month','price_change','underlying_id'};
VolHeader = {'settlement_date','cont_month','vol_change','vol_id'};

OutPriceChangeData = [PriceHeader;OutPriceChangeData];
OutVolChangeData   = [VolHeader;OutVolChangeData];

xlswrite(PriceFilename,OutPriceChangeData);
xlswrite(VolFilename,OutVolChangeData);

CleanedPriceData = clean_price_vol_change_data(cell2dataset(OutPriceChangeData));
CleanedVolData = clean_price_vol_change_data(cell2dataset(OutVolChangeData));

CleanedPriceData = dataset2cell(CleanedPriceData);
CleanedVolData   = dataset2cell(CleanedVolData);

OutFilename = getXLSFilename('CleanedPriceData');
xlswrite(OutFilename,CleanedPriceData);

OutFilename = getXLSFilename('CleanedVolData');
xlswrite(OutFilename,CleanedVolData);

Data = CleanedPriceData(2:end,1:3);
TableName = 'var_cont_price_change_table';
set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(SDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end
set(ObjDB,'AutoCommit','on');
upload_in_database(TableName,Data);

Data = CleanedVolData(2:end,1:3);
TableName = 'var_cont_vol_change_table';
set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',TableName,' where settlement_date = ''',char(SDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end
set(ObjDB,'AutoCommit','on');
upload_in_database(TableName,Data);

% StartDate = datestr(datenum(SDate) - 10,'yyyy-mm-dd');
% SqlQuery = ['select * from var_cont_price_change_table where settlement_date >= ''',StartDate,''''];
% PriceData = fetch(ObjDB,SqlQuery);
% PriceHeader = {'settlement_date','cont_month','price_change'};
% PriceChangeData = cell2dataset([PriceHeader;PriceData]);
% 
% SqlQuery = ['select * from var_cont_vol_change_table where settlement_date >= ''',StartDate,''''];
% VolData = fetch(ObjDB,SqlQuery);
% VolHeader = {'settlement_date','cont_month','vol_change'};
% VolChangeData = cell2dataset([VolHeader;VolData]);
% 
% SBusDays = busdays(datenum('2015-11-10'), datenum(SDate),'daily',HolidayVec);
% SDatesPlot = SBusDays(end-10):1:SBusDays(end);
% SDatesPlot = cellstr(datestr(SDatesPlot,'yyyy-mm-dd'));
% IdxPlot = ismember(PriceVolData.settlement_date,SDatesPlot);
% 
% PriceVolData = PriceVolData(IdxPlot,:);
% BBGUnderlyingId = unique(PriceVolData.bbg_underlying_id);
% SettlementDate = datetime(PriceVolData.settlement_date,'ConvertFrom','yyyy-MM-dd','format','dd-MMM');
% 
% for iC = 1:length(BBGUnderlyingId)
%     IdxContMonth = strcmpi(BBGUnderlyingId{iC},PriceVolData.bbg_underlying_id);
%     hFig(iC) = figure('units','normalized','outerposition',[0 0 1 1]);
%     plot(SettlementDate(IdxContMonth),PriceVolData.vol(IdxContMonth),'ro-');
%     hold on;
%     plot(SettlementDate(IdxContMonth),PriceVolData.price(IdxContMonth),'bx-');
%     datetick('x','dd-mmm');
%     grid on;
%     set(gca,'XMinorGrid','on');
%     LegendEntries = {'Vol Data(%)','Price Data'};
%     legend(LegendEntries);
%     
%     xlabel('Settlement Date');
%     ylabel('Price/Vol Data');
%     title(BBGUnderlyingId{iC});
%     
% %     set(gcf,'Name',BBGUnderlyingId{iC},'NumberTitle','off','MenuBar','none');
%     set(gcf,'Name',BBGUnderlyingId{iC});
% %     saveas(gcf,BBGUnderlyingId{iC} , 'bmp');
% %     close(gcf);
% end
%     


end