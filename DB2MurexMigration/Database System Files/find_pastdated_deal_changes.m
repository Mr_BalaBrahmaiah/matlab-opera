function find_pastdated_deal_changes

ObjDB = connect_to_database;
SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
ValueDate  = char(fetch(ObjDB,'select value_date from valuation_date_table'));

% deal changes done in trade_Date
SqlQuery = ['select * from deal_ticket_table where version_timestamp like ''',ValueDate,'%'' and transaction_date = ''',SettlementDate,''' and version_comments like ''change%''' ];
[DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery);
OutFilename = ['tradedate_deal_ticket_changes_',ValueDate,'.csv'];
customcsvwrite(OutFilename,DBData,DBFieldNames);

% past dated deal changes
SqlQuery = ['select * from deal_ticket_table where version_timestamp like ''',ValueDate,'%'' and transaction_date <> ''',SettlementDate,''''];
[DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery);
PastDatedOutFilename = ['pastdated_deal_ticket_changes_',ValueDate,'.csv'];
customcsvwrite(PastDatedOutFilename,DBData,DBFieldNames);

configure_mail_settings;
sendmail('Raghavendra.Sn@olamnet.com', 'Deal Ticket Changes', ['The attached files contains the changes done in deal ticket table on the ValueDate ''',ValueDate,''''],{OutFilename,PastDatedOutFilename});
