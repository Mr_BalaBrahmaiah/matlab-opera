function [OutErrorMsg,PortfolioDeltaFilename,Opt_Real_Unr_Filename,Net_Fut_PortfolioWise_Filename,Net_Fut_ProductWise_Filename,...
    FF_Portfolio_Filename,FF_Position_Filename,FF_Greeks_FileName,PandL_Monitoring_Filename,Greel_PnL_Filename,...
    VARFilename,MonthEnd_EOD_Filename,TradersReportSummaryFilename,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName,...
    AccountingTRReport,PnlReport]  = generate_all_MO_reports_agf(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

%% Portfolio Delta Report

[OutErrorMsg1,PortfolioDeltaFilename] = generate_portfolio_delta_report(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    PortfolioDeltaFilename = char(OutErrorMsg1);
end

cprintf('key','%s finished\n', 'Portfolio Delta Report');

%% Options Realized and Unrealized Report

[OutErrorMsg2,Opt_Real_Unr_Filename] = generate_opt_real_unr_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    Opt_Real_Unr_Filename = char(OutErrorMsg2);
end

cprintf('key','%s finished\n', 'Options Realized and Unrealized Report');

%% Net Futures Portfoliowise Report

[OutErrorMsg3,Net_Fut_PortfolioWise_Filename] = generate_net_fut_portfoliowise_report(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    Net_Fut_PortfolioWise_Filename = char(OutErrorMsg3);
end

cprintf('key','%s finished\n', 'Net Futures Portfoliowise Report');

%% Net Futures Productwise Report

[OutErrorMsg4,Net_Fut_ProductWise_Filename] = generate_net_fut_prodwise_report(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    Net_Fut_ProductWise_Filename = char(OutErrorMsg4);
end

cprintf('key','%s finished\n', 'Net Futures Productwise Report');

%% FF Portfolio Report

[OutErrorMsg5,FF_Portfolio_Filename] = generate_ff_portfolio_report(InBUName);
if ~strcmpi(OutErrorMsg5,'No errors')
    FF_Portfolio_Filename = char(OutErrorMsg5);
end

cprintf('key','%s finished\n', 'FF Portfolio Report');

%% FF Positions Report

[OutErrorMsg6,FF_Position_Filename] = generate_ff_position_report(InBUName);
if ~strcmpi(OutErrorMsg6,'No errors')
    FF_Position_Filename = char(OutErrorMsg6);
end

cprintf('key','%s finished\n', 'FF Positions Report');

%% FF Greeks Report

[OutErrorMsg7,FF_Greeks_FileName] = generate_ff_greeks_report(InBUName);
if ~strcmpi(OutErrorMsg7,'No errors')
    FF_Greeks_FileName = char(OutErrorMsg7);
end

cprintf('key','%s finished\n', 'FF Greeks Report');

%% PnL Monitoring Report

[OutErrorMsg8,PandL_Monitoring_Filename] = generate_pandl_monitoring_report(InBUName);
if ~strcmpi(OutErrorMsg8,'No errors')
    PandL_Monitoring_Filename = char(OutErrorMsg8);
end

cprintf('key','%s finished\n', 'PnL Monitoring Report');

%% Greek PnL Report

[OutErrorMsg9,Greel_PnL_Filename] = generate_greek_pandl_report(InBUName);
if ~strcmpi(OutErrorMsg9,'No errors')
    Greel_PnL_Filename = char(OutErrorMsg9);
end

cprintf('key','%s finished\n', 'Greek PnL Report');

%% VAR Report

[OutErrorMsg10,VARFilename] = generate_accounting_VAR_report(InBUName);
if ~strcmpi(OutErrorMsg10,'No errors')
    VARFilename = char(OutErrorMsg10);
end

cprintf('key','%s finished\n', 'VAR Report');

%% Monthend_EOD

[OutErrorMsg11,MonthEnd_EOD_Filename] = generate_monthend_eod_report(InBUName);
if ~strcmpi(OutErrorMsg11,'No errors')
    MonthEnd_EOD_Filename = char(OutErrorMsg11);
end

cprintf('key','%s finished\n', 'Monthend_EOD');

%% Traders Report Summary

[OutErrorMsg12,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
if ~strcmpi(OutErrorMsg12,'No errors')
    TradersReportSummaryFilename = char(OutErrorMsg12);
end

cprintf('key','%s finished\n', 'Traders Report Summary');

%% PnP Report

[OutErrorMsg13,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    PnpOptionDumpFileName = char(OutErrorMsg13);
    PnpFutureDumpFileName = char(OutErrorMsg13);
    PnpMUsDumpFileName = char(OutErrorMsg13);
end

cprintf('key','%s finished\n', 'PnP Report');

%% Previous Settle Accounting Traders Report

[OutErrorMsg14,AccountingTRReport] = generate_previous_settle_accounting_traders_report(InBUName);
if ~strcmpi(OutErrorMsg14,'No errors')
    AccountingTRReport = char(OutErrorMsg14);
end

cprintf('key','%s finished\n', 'Previous Settle Accounting Traders Report');

%% Prervious Settle Accounting PnL Report

[OutErrorMsg15,PnlReport] = generate_previous_accounting_tr_pnl_report(InBUName);
if ~strcmpi(OutErrorMsg15,'No errors')
    PnlReport = char(OutErrorMsg15);
end

cprintf('key','%s finished\n', 'Prervious Settle Accounting PnL Report');

%% Overall Error Message

Overall_ErrorMsg = [OutErrorMsg1;OutErrorMsg2;OutErrorMsg3;OutErrorMsg4;OutErrorMsg5;...
    OutErrorMsg6;OutErrorMsg7;OutErrorMsg8;OutErrorMsg9;OutErrorMsg10;...
    OutErrorMsg11;OutErrorMsg12;OutErrorMsg13;OutErrorMsg14;OutErrorMsg15];

end
