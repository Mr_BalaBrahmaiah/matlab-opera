function [OutErrorMsg,OutFilename] = generate_compliance_report_summary(InBUName)
%  Generate the compliance summary report
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:52 $
%  $Revision: 1.2 $
%

OutErrorMsg = {'No errors'};

try    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
ViewName = 'helper_settle_pricing_subportfoliovalues_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

[~,~,OptionTypeMapping] = xlsread('DB_Murex_Reports.xlsx','OptionType Mapping');
RefOptType = OptionTypeMapping(2:end,1);
RefOptGroup = OptionTypeMapping(2:end,3);

% since they need the breakup only for OPT or FUTURES and not based on
% individual derivative type
PosOptType = strcmpi('derivative_type',SettleColNames);
InOptType = SettleData(:,PosOptType);
for iType = 1:length(RefOptType)
    IdxType = strcmpi(RefOptType{iType},InOptType);
    SettleData(IdxType,PosOptType) = RefOptGroup(iType);    
end

% change the product code of Rubber to OR; since it is currently OR-EUR or
% OR-USD
PosProdCode = strcmpi('product_code',SettleColNames);
IdxOR = strncmpi(SettleData(:,PosProdCode),'OR',2);
SettleData(IdxOR,PosProdCode) = {'OR'};

UniqueFields = {'subportfolio','product_code','counterparty_parent','contract_month','derivative_type'};
SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11',...
             'settle_gamma_12','settle_gamma_21','settle_gamma_22',...
             'settle_vega_1','settle_vega_2','settle_theta','mtm-usd','mtm-nc'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

OutFilename = getXLSFilename('ComplianceReport_Summary');
xlswrite(OutFilename,[OutputFields;OutData]);

catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end