HolDates = {'2016-01-01';'2016-12-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2016-03-08'), datenum('2016-03-11'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

objDB = connect_to_database;

for i = 1:length(VBusDays)
    SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
    curs = exec(objDB,SqlQuery);
    if ~isempty(curs.Message)
        errordlg(curs.Message);
    end
    
%     derivativepricerinterface('settle',1,'ananthi');
%     
%     generate_detailed_accounting_traders_report;

[AccountingTRReport,PnlReport] = generate_previous_settle_accounting_traders_report;

end