ViewName = 'temp_helper_var_report';
[ColNames,Data] = read_from_database(ViewName,0);

UniqueFields = {'settlement_date','subportfolio','counterparty','underlying_future_id','option_expiry','derivative_type','strike'};
SumFields = {'active_lots'};
OutputFields = {'settlement_date','subportfolio','counterparty','underlying_future_id','future_expiry','option_expiry','derivative_type','strike','active_lots','underlying_future_price','contract_settlement_price','contract_month'};
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

try
    OutData = sortrows(OutData,[1,2,4,6,7]);
catch
end

OutFilename = getXLSFilename('helper_var_DBdump');
xlswrite(OutFilename,[OutputFields;OutData]);