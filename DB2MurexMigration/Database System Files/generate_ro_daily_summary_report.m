function [OutErrorMsg,OutFilename] = generate_ro_daily_summary_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('RO Daily Summary Report');
    
    ViewName = 'helper_funds_reports_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{''});  %% None
        Data = Data(ispresent_FUT_OFUT,:);
        
        %         Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
        %         Get_CallPut = Data(:,Find_CallPut_Col);
        %         ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
        %         Data(ispresent_Call,Find_CallPut_Col) = {'call'};
        %         ispresent_Put = cellStrfind(Get_CallPut,'p');
        %         Data(ispresent_Put,Find_CallPut_Col) = {'put'};        %% Get 'Call/Put' Row only
        
        %         ispresent_Call_Put = sort([ispresent_Call;ispresent_Put]);
        %         Data = Data(ispresent_Call_Put,:);         %% Get 'Call/Put' Row only
        
        UniqueFields = {'portfolio','asset_class','instrument','contract_month'};
        SumFields = {'cumulative_mtm_usd','0','cumulative_mtm_usd','settle_delta_1','curr_mult1','mkt_value_usd'};
        OutputFields = {'portfolio','asset_class','instrument','contract_month',...
            'cumulative_mtm_usd','0','cumulative_mtm_usd','settle_delta_1','curr_mult1','mkt_value_usd'};
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        OutHeader = {'Portfolio','Product Type','Underlying','Maturity Label','Book_Value','Brokerage','Net_PL','Delta',...
            'CurConvert','MKT_EXP_USD'};  %% No need 'Index_Currency' and check 'Brokerage' now its put '0'
        
        xlswrite(OutFilename,[OutHeader;OutData]);
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end