function clean_data_plot(OutDSData)

% Products = unique(OutDSData.product_code);
% for iP = 1:length(Products)
%     IdxProd = strcmpi(Products{iP},OutDSData.product_code);
%     figure;
%     ContMonth = unique(OutDSData.cont_month(IdxProd));
%     for iC = 1:length(ContMonth)
%         IdxContMonth = IdxProd & strcmpi(ContMonth{iC},OutDSData.cont_month);
%         plot(datetime(OutDSData.settlement_date(IdxContMonth),'ConvertFrom','excel'),OutDSData.vol_change(IdxContMonth),'-o');
%         hold on;
%     end
%     legend(ContMonth);
%     xlabel('Settlement Date');
%     ylabel('Vol Change');
%
% end

Products = unique(OutDSData.product_code);
SettlementDate = datetime(OutDSData.settlement_date,'ConvertFrom','excel','format','dd-MMM');
for iP = 1:length(Products)
    IdxProd = strcmpi(Products{iP},OutDSData.product_code);
    ContMonth = unique(OutDSData.cont_month(IdxProd));    
    for iC = 1:length(ContMonth)
        IdxContMonth = IdxProd & strcmpi(ContMonth{iC},OutDSData.cont_month);
        figure('units','normalized','outerposition',[0 0 1 1]);          
        plot(SettlementDate(IdxContMonth),OutDSData.vol_change(IdxContMonth),'-o');      
        datetick('x','dd-mmm');
        grid on;
        set(gca,'XMinorGrid','on');
        
        xlabel('Settlement Date');
        ylabel('Vol Change');
        title(ContMonth{iC});
        
        set(gcf,'Name',ContMonth{iC},'NumberTitle','off','MenuBar','none');
         saveas(gcf,ContMonth{iC} , 'bmp');
    end    
    
end