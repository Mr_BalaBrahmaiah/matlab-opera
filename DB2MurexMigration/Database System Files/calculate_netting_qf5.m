function [OutErrorMessage] = calculate_netting_qf5(InBUName,OutputHeader_PNL,Overall_OutData)

%% Temp

% InBUName = 'qf5';
% [~,~,RawData] = xlsread('Netting_Validation_25-Jun-2018_15-08-38.xlsx','Complete Data');
% OutputHeader_PNL = RawData(1,:);
% Overall_OutData = RawData(2:end,:);

disp('OutputHeader_PNL');
disp(OutputHeader_PNL);
disp('Overall_OutData');
disp(Overall_OutData);

%%
try
    
    OutErrorMessage  = {'No Errors'};
    ObjDB = connect_to_database;
    
    SecurityID_Col =  cellStrfind_exact(OutputHeader_PNL,{'SecurityID'});
    
    
    Unique_SecurityID = unique(Overall_OutData(:,cellStrfind_exact(OutputHeader_PNL,{'SecurityID'})));
    for k = 1 : size(Unique_SecurityID)
        Current_Security_ID = Unique_SecurityID(k);
        Matched_Index = strcmpi(Overall_OutData(:,SecurityID_Col),Current_Security_ID);
        
        Temp_Overall_Data = Overall_OutData(Matched_Index,:);
        
        %% Getting Information to Netting
        
        BuyTradeID = Temp_Overall_Data(:,cellStrfind_exact(OutputHeader_PNL,{'buy_trade_id'}));
        SellTradeID = Temp_Overall_Data(:,cellStrfind_exact(OutputHeader_PNL,{'sell_trade_id'}));
        BuyLotsToBeNetted =  Temp_Overall_Data(:,cellStrfind_exact(OutputHeader_PNL,{'buy_lots'}));
        SellLotsToBeNetted =  Temp_Overall_Data(:,cellStrfind_exact(OutputHeader_PNL,{'sell_lots'}));
        
        %% Remove Empty Rows
        BuyTradeID_Empty = cell2mat((cellfun(@(s) (isempty(s)),BuyTradeID,'UniformOutput',0))) | cell2mat((cellfun(@(s) (isnumeric(s)),BuyTradeID,'UniformOutput',0)));
        BuyTradeID(BuyTradeID_Empty) = [];
        BuyLotsToBeNetted(BuyTradeID_Empty) = [];
        SellTradeID_Empty = cell2mat((cellfun(@(s) (isempty(s)),SellTradeID,'UniformOutput',0))) | cell2mat((cellfun(@(s) (isnumeric(s)),SellTradeID,'UniformOutput',0)));
        SellTradeID(SellTradeID_Empty) = [];
        SellLotsToBeNetted(SellTradeID_Empty) = [];
        
        %% Calculate Netting
        UserName = 'system';
        
        [OutErrorMessage,NC_PnL,USD_PnL,Overall_Parfor_Errormsg] = calculate_netting(InBUName,BuyTradeID,BuyLotsToBeNetted,SellTradeID,SellLotsToBeNetted,UserName);
        
    end
    
    disp('Netting Finished');
catch ME
    OutErrorMessage = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
    %     delete(p);
    %     delete(gcp('nocreate'));
end

