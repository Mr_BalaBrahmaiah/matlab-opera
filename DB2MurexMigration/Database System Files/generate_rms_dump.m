function [OutErrorMsg,PnpRMSDumpFileName] = generate_rms_dump(InBUName)
%  generate the rms pnp dump from database
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:53 $
%  $Revision: 1.7 $
%

OutErrorMsg = {'No errors'};
PnpRMSDumpFileName = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    RMS_OutData=[];
    
    TableName = 'valuation_date_table';
    [ColNames,Data] =  read_from_database(TableName,0);
    DBData = cell2dataset([ColNames;Data]);
    
    ViewName = 'helper_rms_dump_barrier_view';
    [RMS_Fields,RMS_InData_Bar] = read_from_database(ViewName,0,'',InBUName);
    
    ViewName = 'helper_rms_dump_vanilla_view';
    [RMS_Fields,RMS_InData_CP] = read_from_database(ViewName,0,'',InBUName);
    
    IsBarDataNotFound = ~iscell(RMS_InData_Bar) && (isequal(RMS_InData_Bar,0) || ...
        strcmpi(RMS_InData_Bar,'No Data'));
    IsCPDataNotFound = ~iscell(RMS_InData_CP) && (isequal(RMS_InData_CP,0) || ...
        strcmpi(RMS_InData_CP,'No Data'));
    
    if IsBarDataNotFound && IsCPDataNotFound
        OutErrorMsg = {'No RMS data found for PNP report!'};
        return;
    end
        
    RMS_InData  = [RMS_InData_Bar;RMS_InData_CP];
    
    PosOptType  = find(strcmpi('derivative_type',RMS_Fields));
    PosBarHit   = find(strcmpi('barrier_hit',RMS_Fields));
    InOptType    = RMS_InData(:,PosOptType); %#ok<*FNDSB>
    InBarrierHit = cell2mat(RMS_InData(:,PosBarHit));
    
    IdxOut = zeros(size(InOptType));
    IdxRemove = cellStrfind(InOptType,{'up_out','down_out'});
    IdxOut(IdxRemove) = 1;
    
    IdxHit = (InBarrierHit==1);
    
    IdxOutHit = IdxHit & IdxOut;
    
    RMS_InData(IdxOutHit,:) = [];
    
    DS_RMS_Data = cell2dataset([RMS_Fields;RMS_InData]);
    clear RMS_InData;
    
    % Remove the rows with concat_unique_key as NULL
    IdxEmpty = cellfun(@isempty,DS_RMS_Data.concat_unique_key);
    DS_RMS_Data(IdxEmpty,:) = [];
    TableName = 'derivative_type_call_put_id_mapping_table';
    [ColNames,Data] =  read_from_database(TableName,0);
    MappingData = cell2dataset([ColNames;Data]);
    RefType = MappingData.derivative_type;
    MapType = MappingData.rmsdump_derivative_type;
    
    TableName = 'subportfolio_helper_data_table';
    [ColNames,Data] =  read_from_database(TableName,0,'',InBUName);
    DBHelperData = cell2dataset([ColNames;Data]);
    RefSubportfolio = DBHelperData.subportfolio;
    RefStrikeMult = DBHelperData.strike_factor;
    RefPremiumMult = DBHelperData.premium_factor;
    
    % changes for RMS dump
    % strike and premium conversion to adapt to TR format
    Strike       = DS_RMS_Data.strike;
    Premium      = DS_RMS_Data.premium;
    BarrierLevel = DS_RMS_Data.barrier_level;
    for iS = 1:length(RefSubportfolio)
        IdxSP = strcmp(RefSubportfolio{iS},DS_RMS_Data.subportfolio);
        if any(IdxSP)
            Strike(IdxSP)       = Strike(IdxSP) ./ RefStrikeMult(iS);
            Premium(IdxSP)      = Premium(IdxSP) ./ RefPremiumMult(iS);
            BarrierLevel(IdxSP) = BarrierLevel(IdxSP) ./ RefStrikeMult(iS);
        end
    end
    % derivative type conversion to adapt to RMS dump
    OptType = DS_RMS_Data.derivative_type;
    for iType = 1:length(RefType)
        IdxType = strcmpi(DS_RMS_Data.derivative_type,RefType{iType});
        OptType(IdxType) = MapType(iType);
    end
    % convert the NaN barrier level and barrier hit to zero
    BarrierHit = DS_RMS_Data.barrier_hit;
    BarrierHit(isnan(BarrierHit)) = 0;
    BarrierLevel(isnan(BarrierLevel))= 0;
    
    UniqueRows = unique(DS_RMS_Data.concat_unique_key,'stable');
    
    for iRows = 1:length(UniqueRows)
        Idx = strcmp(UniqueRows{iRows},DS_RMS_Data.concat_unique_key);
        
        AvgPrem = mean(Premium(Idx));
        Start = datestr(min(datenum(DS_RMS_Data.maturity_date(Idx),'yyyy-mm-dd')),'dd-mmm-yyyy');
        Expiry = datestr(max(datenum(DS_RMS_Data.maturity_date(Idx),'yyyy-mm-dd')),'dd-mmm-yyyy');
        
        IdxFound = find(Idx);
        i = IdxFound(1);
        
        try
            if isequal(length(find(Idx)),length(unique(DS_RMS_Data.maturity_date(Idx))))
                Lots = DS_RMS_Data.lots(i);
            else
                Lots = sum(DS_RMS_Data.lots(Idx))/length(unique(DS_RMS_Data.maturity_date(Idx)));
            end
        catch
            Lots = DS_RMS_Data.lots(i);
        end
        
        WeightedAvgPrem = sum(Premium(Idx).*DS_RMS_Data.lots(Idx)) ./ sum(DS_RMS_Data.lots(Idx));
        RowData = [DS_RMS_Data.instrument(i),DS_RMS_Data.subportfolio(i),Lots,...
            DS_RMS_Data.contract_month(i),Strike(i),DS_RMS_Data.opt_periodicity(i),...
            OptType(i),BarrierLevel(i),WeightedAvgPrem,cellstr(Start),cellstr(Expiry),...
            DS_RMS_Data.counterparty_parent(i),DS_RMS_Data.contract_number(i),BarrierHit(i)];
        
        RMS_OutData = [RMS_OutData;RowData];
        
    end
    
    PnpRMSDumpFileName = getXLSFilename('pnp_RMS_dump');
    
    if ~isempty(RMS_OutData)
        RMSDumpHeader = {'instrument','subportfolio','lots','contract_month','strike',...
            'opt_periodicity','option_type','barrier_level','premium','Start','Expiry',...
            'counterparty','contract_number','barrier_hit'};
        %     customcsvwrite(PnpRMSDumpFileName,RMS_OutData,RMSDumpHeader);
        xlswrite(PnpRMSDumpFileName,[RMSDumpHeader;RMS_OutData]);
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end