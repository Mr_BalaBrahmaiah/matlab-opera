function [OutErrorMessage,Overall_Parfor_Errormsg] = cancel_netting(InBUName,InNettingIDs,UserName)

OutErrorMessage = {'No Errors'};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %% Sort NettingID
    
    [aa,bb] =strtok(InNettingIDs,'-');
    [Split_NettedID,bb] =strtok(bb,'-');
    
    InNettingIDs = [InNettingIDs,Split_NettedID];
    
    InNettingIDs = sortrows(InNettingIDs,-2);
    InNettingIDs = InNettingIDs(:,1);
    
    %%
    
    %     ObjDB = connect_to_database;
    [DBName,DBUserName,DBPassword,DBServerIP,IsFromServer] = Read_DB_ConfigFile();
    javaaddpath('mysql-connector-java-5.1.25-bin.jar');
    %     p=parpool('local',4);
    c =parallel.pool.Constant(@()database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP,'PortNumber',3306),@close);
    
    DealTicketTableName = ['deal_ticket_table_',InBUName];
    ClonedDealTicketTableName = ['cloned_deal_ticket_table_',InBUName];
    NettingIdTableName = ['netting_id_table_',InBUName];
    
    
    ObjDB_1 = connect_to_database;
    ObjDB = connect_to_database;
    
    Overall_Parfor_Errormsg = {};
    
    for iD = 1:length(InNettingIDs)
        SqlQuery = ['select trade_id from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''''];
        TradeId = fetch(ObjDB_1,SqlQuery);
        Parfor_Errormsg = {};
        if ~isempty(TradeId)
            TradeId_sql_Query = ['''',strrep(convertCell2Char(TradeId),',',''','''),''''];
            % check if the latest needed information is available in
            % netting id table, before proceeding further with the
            % cancellation
            SqlQuery = ['select active_lots,netted_lots from ',char(NettingIdTableName),' where netting_id = ''',InNettingIDs{iD},''''];
            OutData = cell2mat(fetch(ObjDB_1,SqlQuery));
            if any(isnan(OutData(:,1))) || any(isnan(OutData(:,2)))
                Overall_Parfor_Errormsg = {['NettedLots info missing in netting_id_table.Please contact OPERA-IT to cancel ',InNettingIDs{iD}]};
                OutErrorMessage = {'Error Occured in Cancel Netting Process'} ;
                return;
            end
            
            %%% Newly added Logic (12-03-2019) to check if the netting ids chosen for cancellation is as per LIFO method
            SqlQuery_distinct_netting_id = ['select distinct(netting_id) from ',char(NettingIdTableName),' where trade_id  in (',TradeId_sql_Query,') '];
            distinct_netting_id = fetch(ObjDB_1,SqlQuery_distinct_netting_id);
            
            [~,netting_id_str2] =strtok(distinct_netting_id,'-');
            [Netting_id_Split1,~] =strtok(netting_id_str2,'-');
            
            %%% Check Condition
            Index_present = find(str2double(Netting_id_Split1) > str2double(Split_NettedID));
            
            if any(Index_present)
                Overall_Parfor_Errormsg = {['Cancellation should follow LIFO methodology.Please contact OPERA-IT to cancel ',InNettingIDs{iD}]};
                OutErrorMessage = {'Error Occured in Cancel Netting Process'} ;
                return;
            end
            %%%% End Code
            
            Parfor_Errormsg = {};
            parfor iTR = 1:length(TradeId)
                ObjDB = c.Value;
                %             for iTR = 1:length(TradeId)
                
                CurrentTradeId = [InNettingIDs{iD},'-',TradeId{iTR}];
                SqlQuery = ['delete from ',DealTicketTableName,' where trade_id = ''',CurrentTradeId,''''];
                Curs = exec(ObjDB,SqlQuery);
                if ~isempty(Curs.Message)
                    %  OutErrorMessage = {Curs.Message};
                    %                     Parfor_Errormsg = [Parfor_Errormsg ; [{CurrentTradeId} ,{DealTicketTableName},{Curs.Message}] ];
                    Parfor_Errormsg = [Parfor_Errormsg ; [CurrentTradeId,' : ' ,DealTicketTableName,' : ',Curs.Message] ];
                end
                
                SqlQuery = ['select max(version_no) from ',DealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
                MaxVersion = cell2mat(fetch(ObjDB,SqlQuery));
                SqlQuery = ['select * from deal_ticket_table where trade_id = ''',TradeId{iTR},''' and version_no = ',mat2str(MaxVersion)];
                [DBFieldNames,DBData] = read_from_database('deal_ticket_table',0,SqlQuery,InBUName);
                DealTicketData = cell2dataset([DBFieldNames;DBData]);
                
                %                 SqlQuery = ['select netting_status,netted_lots,original_lots from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
                %                 OutData = fetch(ObjDB,SqlQuery);
                %                 if strcmpi(OutData{1},'partially_netted')
                %                     DealTicketData.original_lots = OutData{2} + OutData{3};
                %                 end
                
                SqlQuery = ['select original_lots from ',ClonedDealTicketTableName,' where trade_id = ''',TradeId{iTR},''''];
                TempOriginalLots = cell2mat(fetch(ObjDB,SqlQuery));
                
                SqlQuery = ['select active_lots from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''''];
                TempActiveLots = cell2mat(fetch(ObjDB,SqlQuery));
                TempNettedLots = TempOriginalLots - TempActiveLots;
                
                DealTicketData.version_no = MaxVersion+1;
                DealTicketData.version_created_by = UserName;
                DealTicketData.version_timestamp = getCurrentDateTimestamp;
                DealTicketData.version_comments = 'netting_cancelled';
                DealTicketData.is_netting_done = 0;
                DealTicketData.lots_to_be_netted = TempActiveLots;
                DealTicketData.deal_status = 'live';
                
                DealTicketData = dataset2cell(DealTicketData);
                fastinsert(ObjDB,DealTicketTableName,DealTicketData(1,:),DealTicketData(2:end,:));
                
                if isnumericequal(TempOriginalLots,TempActiveLots) % cancellation of fully netted entries
                    update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
                        {TempActiveLots,'not_netted',0,'',''},{['where trade_id = ''',TradeId{iTR},'''']});
                else
                    update(ObjDB,ClonedDealTicketTableName,{'active_lots','netting_status','netted_lots','netting_id','netting_date'},...
                        {TempActiveLots,'partially_netted',TempNettedLots,'',''},{['where trade_id = ''',TradeId{iTR},'''']});
                end
                
                %                 SqlQuery = ['delete from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''''];
                %%% old one
                %SqlQuery = ['update ',NettingIdTableName,' set netting_status = ''cancelled'' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''''];
                
                %%% New one
                %%% Insert data into Netting_Id_Cancelled Table
                Netting_Id_cancelled_TableName = ['netting_id_cancelled_table_',InBUName];
                try
                    SqlQuery1 = ['insert into ',Netting_Id_cancelled_TableName,' select * from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''' '];
                    Curs = exec(ObjDB,SqlQuery1);
                    if ~isempty(Curs.Message)
                        Parfor_Errormsg = [Parfor_Errormsg ; [CurrentTradeId,' : ' ,Netting_Id_cancelled_TableName,' : ',Curs.Message] ];
                    end
                catch
                    continue;
                end
                
                %%% Delete data from Netting_Id table
                SqlQuery = ['delete from ',NettingIdTableName,' where netting_id = ''',InNettingIDs{iD},''' and trade_id = ''',TradeId{iTR},''''];
                Curs = exec(ObjDB,SqlQuery);
                if ~isempty(Curs.Message)
                    Parfor_Errormsg = [Parfor_Errormsg ; [CurrentTradeId,' : ' ,NettingIdTableName,' : ',Curs.Message] ];
                end
                %%% End Code
                
            end
        end
        
        Overall_Parfor_Errormsg = [ Overall_Parfor_Errormsg ; Parfor_Errormsg];
    end
    
    if(~isempty(Overall_Parfor_Errormsg))
        OutErrorMessage = {'Error Occured in Cancel Netting Process'} ;
    end
    
    %     delete(p);
    %     delete(gcp('nocreate'));
    
catch ME
    OutErrorMessage = {ME.message};
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
    %     delete(p);
    %     delete(gcp('nocreate'));
end