function [OutErrorMsg,OutXLSName,OutMissingClientCodes] = refresh_client_cash_acc_balances(InBUName)

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    LogFilename = getTimeStampedFilename('ML_CashAccBalaceRefresh_Log','.txt');
    
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['CashAccountBalance_',InBUName]);
    OutMissingClientCodes = {'No Client Codes missing!'};
    
    MissingClientCodes = [];
    
    % COB_DATE, CLIENT_CODE, FUT/FX CASH, OPTION CASH
    ObjDB = connect_to_database;
    ViewName = 'helper_cash_balance_report_view_';
    ViewName = [ViewName,InBUName];
    
    [SettleColNames,Data] = Fetch_DB_Data(ObjDB,[],ViewName);
    
    if(isempty(Data))
        OutErrorMsg = {'No data found for the COB!'};
        return;
    end
    
    if(strcmpi(Data,'No Data'))
        OutErrorMsg = {'No data found for the COB!'};
        return;
    end
    
    DBCashBalanceData = cell2table(Data);
    DBCashBalanceData.Properties.VariableNames = SettleColNames;
    
    % Fill the client code for all SIP avg_swap deals, as the SIP expiry processing
    % deals can contain empty contract numbers
    IdxSIPSwap = strcmpi(DBCashBalanceData.counterparty_parent,'SIPH') & ... 
        strcmpi(DBCashBalanceData.derivative_type,'avg_swap_cc');
    DBCashBalanceData.client_code(IdxSIPSwap) = {'SIP'};
    
    % Remove the lines with NULL client codes
    NullClientCode = cellfun(@isempty,DBCashBalanceData.client_code);
    DBCashBalanceData(NullClientCode,:) = [];
    IdxNullClientCode = strcmpi(DBCashBalanceData.client_code,'null');
    DBCashBalanceData(IdxNullClientCode,:) = [];
    
    FUT_FX_Cash = zeros(size(DBCashBalanceData.subportfolio,1),1);
    Option_Cash = zeros(size(DBCashBalanceData.subportfolio,1),1);
    
    IdxFUT = strcmpi(DBCashBalanceData.derivative_type,'future');
    IdxFX  = strncmpi(DBCashBalanceData.derivative_type,'fx_forward',length('fx_forward'));
    IdxFxOpt = strncmpi(DBCashBalanceData.derivative_type,'fx_vanilla',length('fx_vanilla'));
    IdxAvgSwap = strncmpi(DBCashBalanceData.derivative_type,'avg_swap',length('avg_swap'));
    IdxOPT = ~(IdxFUT | IdxFX | IdxFxOpt | IdxAvgSwap);
    
    COB_Date = DBCashBalanceData.settlement_date;
    Client_Code = DBCashBalanceData.client_code;
    Currency = DBCashBalanceData.currency;
    
    M2M_NC = (DBCashBalanceData.settle_price - DBCashBalanceData.original_premium)  .* DBCashBalanceData.active_lots .* DBCashBalanceData.mult_factor .* -1 ;
    M2M_USD = M2M_NC .* DBCashBalanceData.nc_factor .* DBCashBalanceData.fx_nc_factor ;
    
    %     M2M_USD_Spot = M2M_NC .* DBCashBalanceData.curr_mult ;
    
    PremiumPaidOrRecd_NC = DBCashBalanceData.original_premium .* DBCashBalanceData.original_lots .* DBCashBalanceData.mult_factor;
    PremiumPaidOrRecd_USD = PremiumPaidOrRecd_NC .* DBCashBalanceData.nc_factor .* DBCashBalanceData.fx_nc_factor ;
    
    %     FUT_FX_Cash(IdxFUT) = M2M_USD(IdxFUT);
    %     FUT_FX_Cash(IdxFX)  = M2M_USD(IdxFX);
    %     Option_Cash(IdxOPT) = PremiumPaidOrRecd_USD(IdxOPT);
    FUT_FX_Cash(IdxFUT)     = M2M_NC(IdxFUT);
    FUT_FX_Cash(IdxFX)      = M2M_USD(IdxFX);
    FUT_FX_Cash(IdxFxOpt)   = M2M_NC(IdxFxOpt);
    FUT_FX_Cash(IdxAvgSwap) = M2M_NC(IdxAvgSwap);
    
    Currency(IdxFX) = {'USD'};
    Option_Cash(IdxOPT) = PremiumPaidOrRecd_NC(IdxOPT);
    
    UniqueFields = {'COB_Date','Client_Code','Currency'};
    SumFields = {'FUT_FX_Cash','Option_Cash'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    OutData = [COB_Date,Client_Code,Currency,num2cell(FUT_FX_Cash),num2cell(Option_Cash)];
    [SummaryFields,SummaryData] = consolidatedata(OutputFields, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    %Identify the non-zero cash balance changes for the day 
    % end column signifies option_premium_paid_or_recd impacts
    % end-1 column signifies future_expired_or_liquidated_cash impacts
    IdxCOBChanges = ~isnumericequal(cell2mat(SummaryData(:,end)),0) | ~isnumericequal(cell2mat(SummaryData(:,end-1)),0);
    
    if ~isempty(find(IdxCOBChanges, 1))
        PosClientCode = strcmpi('client_code',SummaryFields);
        COBClientCode = SummaryData(IdxCOBChanges,PosClientCode);
        for iCode = 1:length(COBClientCode)
            IdxCOBData = strcmpi(COBClientCode(iCode),DBCashBalanceData.client_code);            
            xlswrite(OutXLSName, [SettleColNames;table2cell(DBCashBalanceData(IdxCOBData,:))],COBClientCode{iCode});
        end  
    end
    xlswrite(OutXLSName,[SettleColNames;table2cell(DBCashBalanceData)],'RawData');
    
    SettlementDate = COB_Date(1);
    
    SqlQuery = ['select settlement_date from vdate_sdate_table where value_date = ''',char(SettlementDate),''''];
    PrevSettlementDate = char(fetch(ObjDB,SqlQuery));
    
    SqlQuery = ['select * from client_cash_account_balance_table_cfs where cob_date = ''',PrevSettlementDate,''''];
    [DBFieldNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
    PrevDayCashBalanceData = cell2table(DBData);
    PrevDayCashBalanceData.Properties.VariableNames = DBFieldNames;
    PrevDayCashBalanceData.currency = strtrim(PrevDayCashBalanceData.currency);
    
    CashBalanceSummary = cell2table(SummaryData);
    CashBalanceSummary.Properties.VariableNames = SummaryFields;
    
    SqlQuery = 'SELECT client_code FROM client_master_data_cfs';
    ClientMasterCodes = fetch(ObjDB,SqlQuery);
    
    TodaysCashBalanceData = PrevDayCashBalanceData;
    % assign previous days closing balance to todays opening balance
    TodaysCashBalanceData.opening_balance = PrevDayCashBalanceData.closing_balance;
    TodaysCashBalanceData.cob_date(:) = SettlementDate;
    
    % initialise all the below variables for today
    TodaysCashBalanceData.closing_balance(:) = 0;
    TodaysCashBalanceData.commission(:) = 0;
    TodaysCashBalanceData.interest_charges(:) = 0;
    TodaysCashBalanceData.transfers_in(:) = 0;
    TodaysCashBalanceData.transfers_out(:) = 0;
    TodaysCashBalanceData.comments(:) = {''};
    TodaysCashBalanceData.expired_or_liquidated_cash(:) = 0;
    TodaysCashBalanceData.premium_paid_or_recd(:) = 0;
    
    NewCashBalanceRowData = TodaysCashBalanceData(end,:);
           
    format bank;
    NumClients = size(SummaryData,1);
    
    for iClient = 1:NumClients
        IdxFound = strcmpi(CashBalanceSummary.Client_Code(iClient),TodaysCashBalanceData.client_code) & ....
            strcmpi(CashBalanceSummary.Currency(iClient),TodaysCashBalanceData.currency);
        
        if isempty(find(IdxFound))
            % If the client code obtained from generated cash balance summary is missing in previous day's cash balance data
            if ~isempty(find(strcmpi(CashBalanceSummary.Client_Code(iClient),ClientMasterCodes))) % check if the code exists in master;
                % if the code exists in master, then we should add this data entry in cash balance table;
                % as we are entering data for this client as first time, we need to constrcut the structure fully
                NewCashBalanceRowData.client_code = CashBalanceSummary.Client_Code(iClient);
                NewCashBalanceRowData.currency = CashBalanceSummary.Currency(iClient);
                NewCashBalanceRowData.opening_balance = 0;
                NewCashBalanceRowData.expired_or_liquidated_cash = CashBalanceSummary.FUT_FX_Cash(iClient);
                NewCashBalanceRowData.premium_paid_or_recd = CashBalanceSummary.Option_Cash(iClient);
                
                TodaysCashBalanceData(end+1,:) = NewCashBalanceRowData;
            else
                if isempty(MissingClientCodes)
                    MissingClientCodes = [MissingClientCodes,CashBalanceSummary.Client_Code{iClient}];
                else
                    MissingClientCodes = [MissingClientCodes,',',CashBalanceSummary.Client_Code{iClient}];
                end
            end
        else
            % If the client code obtained from generated cash balance summary is available in previous day's cash balance data
            TodaysCashBalanceData.expired_or_liquidated_cash(IdxFound) = CashBalanceSummary.FUT_FX_Cash(iClient);
            TodaysCashBalanceData.premium_paid_or_recd(IdxFound) = CashBalanceSummary.Option_Cash(iClient);
        end
    end
    
    TodaysCashBalanceData.closing_balance = TodaysCashBalanceData.opening_balance + ...
        TodaysCashBalanceData.expired_or_liquidated_cash + TodaysCashBalanceData.premium_paid_or_recd + ...
        TodaysCashBalanceData.commission + TodaysCashBalanceData.interest_charges + ...
        TodaysCashBalanceData.transfers_in + TodaysCashBalanceData.transfers_out;
    OutData = table2cell(TodaysCashBalanceData);       
    
    TableName = ['client_cash_account_balance_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',TableName,' where cob_date = ''',char(SettlementDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutData);
    
    if ~isempty(MissingClientCodes)
        OutMissingClientCodes = ['Please add the client codes: ',MissingClientCodes,' in Client master!'];
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end

end