function [OutErrorMsg,OutFilename] = generate_ff_greeks_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('FF_Greeks_Report');
    
    ObjDB = connect_to_database;
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        %%
        
        Pos_ValueDate = strcmpi('value_date',ColNames);
        Pos_Maturity = strcmpi('maturity_date',ColNames);
        Pos_NettingStatus = strcmpi('netting_status',ColNames);
        Pos_OriginalLots = strcmpi('original_lots',ColNames);
        
        Current_ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
        MaturityDate_Empty = find(cell2mat(cellfun(@(s) (isempty(s)), Data(:,Pos_Maturity),'UniformOutput',0)));
        Data(MaturityDate_Empty,Pos_Maturity) = Current_ValueDate;
        
        IdxNetted = strcmpi('fully_netted',Data(:,Pos_NettingStatus)); % dead
        MaturityDate = datenum(Data(:,Pos_Maturity),'yyyy-mm-dd');
        ValueDate = datenum(Data(:,Pos_ValueDate),'yyyy-mm-dd');
        IdxDeadFut = (ValueDate > MaturityDate) ;  %% | IdxNetted
        % DBFutData(IdxDeadFut,:) = [];
        DealStatus = repmat({'live'},size(MaturityDate));
        DealStatus(IdxDeadFut) = {'dead'};
        
        ColNames = [ColNames,{'deal_status'}];
        Data = [Data,DealStatus];
        
        Data(IdxDeadFut,:) = []; % Removed Dead Trades
        
        %         Zero_Index = cell2mat(Data(:,Pos_OriginalLots)) == 0;
        %         Data(Zero_Index,:) = []; %% Remove Lots Zero Index
        
        %%
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'OFUT'});
        Data = Data(ispresent_FUT_OFUT,:);
        
        Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
        Get_CallPut = Data(:,Find_CallPut_Col);
        ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
        Data(ispresent_Call,Find_CallPut_Col) = {'call'};
        ispresent_Put = cellStrfind(Get_CallPut,'p');
        Data(ispresent_Put,Find_CallPut_Col) = {'put'};        %% Get 'Call/Put' Row only
        
        PosStrike = strcmpi('strike',ColNames);
        PosPrice = strcmpi('p1_settleprice',ColNames);
        PosConvFactor = strcmpi('conv_factor',ColNames);
        
        Strike = cell2mat(Data(:,PosStrike)) ./ cell2mat(Data(:,PosConvFactor));
        Data(:,PosStrike) = num2cell(Strike);
        Price = cell2mat(Data(:,PosPrice)) ./ cell2mat(Data(:,PosConvFactor));
        Data(:,PosPrice) = num2cell(Price);
        
        UniqueFields = {'subportfolio','instrument','contract_month','call_put_id','strike','counterparty_parent'};% added counterparty parent
        SumFields = {'original_lots','settle_delta_1','settle_gamma_11','settle_theta','settle_vega_1'};
        OutputFields = {'subportfolio','instrument','contract_month','call_put_id','strike','p1_settleprice',...
            'original_lots','settle_delta_1','settle_gamma_11','settle_theta','settle_vega_1','counterparty_parent'}; % added counterparty parent
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        %%%% Newly added code - 11-03-2019
        %%% Delete the entire Row, if Nominal = ZERO (sum of
        %%% original_lots is equal to ZERO)
        Index_Nominal_Zero = eq(cell2mat(OutData(:,7)),0);
        OutData(Index_Nominal_Zero,:) = [];
        %%% End code
        
        %%% Old Logic
        PosTheta = strcmpi('settle_theta',OutputFields); 
        %OutData(:,PosTheta) = num2cell(cell2mat(OutData(:,PosTheta)) .* (7/5));

        %%% Newly added Logic
        Index_Char = find(cellfun(@(v) ischar(v), OutData(:,PosTheta))); 
        if any(Index_Char)
            OutData(Index_Char,PosTheta) = {NaN};                           
        end
        %%% end logic
        
        OutData(:,PosTheta) = num2cell(cell2mat(OutData(:,PosTheta)) .* (7/5));
               
        OutHeader = {'subportfolio','PL Instrument','Maturity Label','Payout','Strike','Price','Nominal','Delta','Gamma','Theta','Vega'};
        xlswrite(OutFilename,[OutHeader;OutData(:,(1:end-1))]);
        
        %%% Newly added on 22-01-2020, for counterparty parent
        OutHeader_CP = {'subportfolio','PL Instrument','Maturity Label','Payout','Strike','Price','Nominal','Delta','Gamma','Theta','Vega','Counterparty'};
        xlswrite(OutFilename,[OutHeader_CP;OutData],'Sheet2');
        
       xls_change_activesheet(fullfile(pwd,OutFilename) ,'Sheet1');
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end