
ViewName = 'helper_traders_p_and_g_view';
[ColNames,Data] = read_from_database(ViewName,0);

PosValueDate = strcmpi('value_date',ColNames);
ValuationDate = datestr(Data{1,PosValueDate},'dd-mmm-yyyy');

Filename = [ViewName,'_',ValuationDate,'.xlsx'];
try
    Pathname = '\\10.190.7.51\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';    
    OutFilename = fullfile(Pathname,Filename);
    xlswrite(OutFilename,[ColNames;Data]);
catch
    OutFilename = fullfile(pwd,Filename);
    xlswrite(OutFilename,[ColNames;Data]);
end

ViewName = 'helper_settle_p_and_g_view';
[ColNames,Data] = read_from_database(ViewName,0);


Filename = [ViewName,'_',ValuationDate,'.xlsx'];
try
    Pathname = '\\10.190.7.51\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';    
    OutFilename = fullfile(Pathname,Filename);
    xlswrite(OutFilename,[ColNames;Data]);
catch
    OutFilename = fullfile(pwd,Filename);
    xlswrite(OutFilename,[ColNames;Data]);
end