function [OutErrorMsg,SummaryReportFilename,CounterpartyDeltaReportFilename,SummaryViewFilename,TradesViewFilename,DeltaReport1,DeltaReport2,DeltaReport3] = generate_all_compliance_reports(InBUName)

OutErrorMsg = {'No errors'};


if iscellstr(InBUName)
    InBUName = char(InBUName);
end

[OutErrorMsg1,SummaryReportFilename] = generate_compliance_report_summary(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    SummaryReportFilename = char(OutErrorMsg1);
end

[OutErrorMsg2,CounterpartyDeltaReportFilename] = generate_counterparty_delta_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    CounterpartyDeltaReportFilename = char(OutErrorMsg2);
end

[OutErrorMsg3,SummaryViewFilename] = generate_compliance_summary_view(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    SummaryViewFilename = char(OutErrorMsg3);
end

[OutErrorMsg4,TradesViewFilename] = generate_compliance_trade_view(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    TradesViewFilename = char(OutErrorMsg4);
end

[OutErrorMsg5,DeltaReport1,DeltaReport2,DeltaReport3] = generate_compliance_delta_reports(InBUName);
if ~strcmpi(OutErrorMsg5,'No errors')
    DeltaReport1 = char(OutErrorMsg5);
    DeltaReport2 = char(OutErrorMsg5);
    DeltaReport3 = char(OutErrorMsg5);
end

if ~strcmpi(OutErrorMsg1,'No errors') && ...
   ~strcmpi(OutErrorMsg2,'No errors') && ...
   ~strcmpi(OutErrorMsg3,'No errors') && ...
   ~strcmpi(OutErrorMsg4,'No errors') && ...
   ~strcmpi(OutErrorMsg5,'No errors') 
    OutErrorMsg = convertChar2Cell([char(OutErrorMsg1);char(OutErrorMsg2);...
        char(OutErrorMsg3);char(OutErrorMsg4);char(OutErrorMsg5)]);
end


end