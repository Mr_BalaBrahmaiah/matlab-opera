function [OutErrorMsg,OutFilename] = generate_options_report(InBUName)
%  Generate the options reports in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/28 11:00:27 $
%  $Revision: 1.9 $
%
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_risk_middle_office_reports';
    [SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);
    
    %% Remove  AGF Subportfolio in Counterparty Column (Directional Deals)
    %     if(strcmpi(InBUName,'agf'))
    ObjDB = connect_to_database;
    
    SqlQuery_1 = ['select distinct(subportfolio) from subportfolio_product_mapping_table_',char(InBUName)] ;
    [~,Subportfolio_1] = Fetch_DB_Data(ObjDB,SqlQuery_1);
    
    SqlQuery_2 = ['select distinct(directional_portfolio) from subportfolio_product_mapping_table_',char(InBUName)];
    [~,Subportfolio_2] = Fetch_DB_Data(ObjDB,SqlQuery_2);
    
    Overall_Subportfolio = [Subportfolio_1 ; Subportfolio_2];
    
    Counterparty_Col = cellStrfind_exact(SettleColNames,{'counterparty_parent'});
    Matched_Subportfolio_Index = cellStrfind_exact(SettleData(:,Counterparty_Col),Overall_Subportfolio);
    
    if(~isempty(Matched_Subportfolio_Index))
        SettleData(Matched_Subportfolio_Index,:) = [];
    end
    
    %     end
    
    %%
    if(~strcmpi(SettleData,'No Data'))
        
        Pos_OptType = strcmpi('derivative_type',SettleColNames);
        Pos_Maturity = strcmpi('maturity_date',SettleColNames);
        Pos_ValueDate = strcmpi('value_date',SettleColNames);
        Pos_Counterparty = strcmpi('counterparty_parent',SettleColNames);
        Pos_SettlementSource = strcmpi('settlement_source',SettleColNames);
        Pos_Subportfolio = strcmpi('subportfolio',SettleColNames);
        Pos_OptPeriodicity = strcmpi('opt_periodicity',SettleColNames);
        PosNettingStatus = strcmpi('netting_status',SettleColNames);
        PosPremiumPaid = strcmpi('is_premium_paid',SettleColNames);
        
        % remove the internal deals
        Counterparty = SettleData(:,Pos_Counterparty);
        IdxRemove = strncmpi(Counterparty,'MM',2) | strncmpi(Counterparty,'RMS',3);
        SettleData(IdxRemove,:) = [];
        
        % should consider all vanilla_call/vanilla_put in MM book and
        % vanilla_call/vanilla_put from RMS only with opt_periodicity = EOD
        IdxOTC = strcmpi('otc',SettleData(:,Pos_SettlementSource));
        SettleData(IdxOTC,:) = [];
        % IdxOTC = strncmpi(SettleData(:,Pos_Subportfolio),'RMS',3) & ~(strcmpi('EOD',SettleData(:,Pos_OptPeriodicity)));
        % SettleData(IdxOTC,:) = [];
        
        % find the option records
        IdxOpt = cellStrfind(lower(SettleData(:,Pos_OptType)),{'call','put'});
        
        % generate the options report
        DBOptionData = SettleData(IdxOpt,:);
        % To remove the USDBRL fx currency options from the report
        Pos_ProductCode = strcmpi('product_code',SettleColNames);
        ProductCode  = DBOptionData(:,Pos_ProductCode);
        DBOptionData(cellStrfind(ProductCode,'USD'),:) = [];
        
        if ~isempty(DBOptionData)
            
            IdxNetted = strcmpi('fully_netted',DBOptionData(:,PosNettingStatus)); % dead
            IdxPremiumPaid = (cell2mat(DBOptionData(:,PosPremiumPaid)) == 1);
            MaturityDate = datenum(DBOptionData(:,Pos_Maturity),'yyyy-mm-dd');
            ValueDate = datenum(DBOptionData(:,Pos_ValueDate),'yyyy-mm-dd');
            IdxDeadOpt = (ValueDate > MaturityDate) | IdxNetted;
            DBOptionData(IdxDeadOpt,:) = [];
            
            if isempty(DBOptionData) %% Error handling (2019-11-25)
                OutErrorMsg = {'No live option trades found!'};
                return;
            end
            
            IdxCall = cellStrfind(lower(DBOptionData(:,Pos_OptType)),'call');
            IdxPut = cellStrfind(lower(DBOptionData(:,Pos_OptType)),'put');
            DBOptionData(IdxCall,Pos_OptType) = {'call'};
            DBOptionData(IdxPut,Pos_OptType) = {'put'};
            
            Pos_SettlePrice = strcmpi('settle_price',SettleColNames);
            Pos_ActiveLots = strcmpi('active_lots',SettleColNames);
            Pos_Premium = strcmpi('original_premium',SettleColNames);
            Pos_OriginalLots  = strcmpi('original_lots',SettleColNames);
            
            SettlePrice = cell2mat(DBOptionData(:,Pos_SettlePrice));
            ActiveLots = cell2mat(DBOptionData(:,Pos_ActiveLots));
            Premium = cell2mat(DBOptionData(:,Pos_Premium));
            OriginalLots = cell2mat(DBOptionData(:,Pos_OriginalLots));
            SettlePrice(isnan(SettlePrice)) = 0;
            
            mtm_nc = (SettlePrice .* ActiveLots) - (Premium .* ActiveLots);
            mtm_nc = num2cell(mtm_nc);
            
            SettleColNames = [SettleColNames,'mtm_nc'];
            DBOptionData = [DBOptionData,mtm_nc];
            OutputFields = {'product_code','product_name','settlement_date','counterparty_parent',...
                'contract_month','p1_name','derivative_type','strike'...
                'settle_price','market_action','active_lots','original_lots',...
                'original_premium','mtm_nc','mult_factor','conv_factor','currency'};
            WeightedAverageFields = {'original_premium','active_lots'};
            UniqueFields = {'product_code','counterparty_parent','contract_month','derivative_type','strike','market_action'};
            SumFields = {'original_lots','active_lots','mtm_nc'};
            [OptionFields,OptionDeals] = consolidatedata(SettleColNames, DBOptionData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
            
            ConsOptData = cell2dataset([OptionFields;OptionDeals]);
            
            IdxOptBot = strcmpi('bought', ConsOptData.market_action);
            IdxOptSold = strcmpi('sold', ConsOptData.market_action);
            
            % initialisation
            OpenBought = zeros(size(IdxOptBot));
            OpenSold = zeros(size(IdxOptSold));
            RealisedOpt = zeros(size(IdxOptBot));
            Open_Sold_Rate = zeros(size(IdxOptSold));
            Open_Bot_Rate = zeros(size(OpenBought));
            
            % new fields added
            Sold_Lot = zeros(size(IdxOptSold));
            Adj_Sold = zeros(size(IdxOptSold));
            Bot_Lot = zeros(size(IdxOptBot));
            Adj_Bot = zeros(size(IdxOptBot));
            
            %     ConvFac = ConsOptData.conv_factor;
            MultFac = ConsOptData.mult_factor;
            
            OpenBought(IdxOptBot) = abs(ConsOptData.active_lots(IdxOptBot));
            OpenSold(IdxOptSold) = abs(ConsOptData.active_lots(IdxOptSold));
            NetFutOpen = OpenBought - OpenSold;
            
            Sold_Lot(IdxOptSold) = abs(ConsOptData.original_lots(IdxOptSold));
            Adj_Sold(IdxOptSold) = Sold_Lot(IdxOptSold) - OpenSold(IdxOptSold);
            
            Bot_Lot(IdxOptBot) = abs(ConsOptData.original_lots(IdxOptBot));
            Adj_Bot(IdxOptBot) = Bot_Lot(IdxOptBot) - OpenBought(IdxOptBot);
            
            
            M2MOpt = ConsOptData.mtm_nc .* MultFac;
            
            Mkt_Rate = ConsOptData.settle_price;
            %   Strike = ConsOptData.strike ./ ConvFac;
            Strike = ConsOptData.strike;
            
            Fut_Mkt_Val = ((OpenBought .* Mkt_Rate) - (OpenSold .* Mkt_Rate)) .* MultFac;
            Fut_Val = Fut_Mkt_Val - M2MOpt;
            
            %     Open_Sold_Rate(IdxOptSold) = ConsOptData.original_premium(IdxOptSold) ./ ConvFac(IdxOptSold);
            %     Open_Bot_Rate(IdxOptBot) =  ConsOptData.original_premium(IdxOptBot) ./ ConvFac(IdxOptBot);
            %
            %     Mkt_Rate = Mkt_Rate ./ ConvFac;
            
            Open_Sold_Rate(IdxOptSold) = ConsOptData.original_premium(IdxOptSold);
            Open_Bot_Rate(IdxOptBot) =  ConsOptData.original_premium(IdxOptBot);
            
            
            OutData = [ConsOptData.product_code,ConsOptData.product_name,ConsOptData.derivative_type,....
                ConsOptData.counterparty_parent,ConsOptData.contract_month,num2cell(Strike),...
                num2cell(Sold_Lot),num2cell(Adj_Sold),num2cell(OpenSold),...
                num2cell(Bot_Lot),num2cell(Adj_Bot),num2cell(OpenBought),num2cell(NetFutOpen),...
                num2cell(RealisedOpt),num2cell(M2MOpt),num2cell(Fut_Val),...
                num2cell(Fut_Mkt_Val),num2cell(Mkt_Rate),num2cell(Open_Sold_Rate),...
                num2cell(Open_Bot_Rate),ConsOptData.currency,ConsOptData.settlement_date];
            OutHeader = {'ProductCode','ProductName','Call_Put',...
                'Counterparty','ContractMonth','Strike',...
                'Sold_Lot','Adj_Sold','Open_Sold',...
                'Bot_Lot','Adj_Bot','Open_Bought','Net_Open',...
                'Realised_PnL','M2M','Val','MktVal','Mkt_Rate',...
                'Open_Sold_Rate','Open_Bot_Rate','Currency','Market_ValueDate'};
            
            WeightedAverageFields = [];
            UniqueFields = {'ProductCode','Counterparty','ContractMonth','Call_Put','Strike'};
            SumFields = {'Sold_Lot','Adj_Sold','Open_Sold','Bot_Lot','Adj_Bot','Open_Bought','Net_Open','Realised_PnL','M2M',...
                'Val','MktVal','Open_Sold_Rate','Open_Bot_Rate'};
            [~,OutOptionData] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutHeader,WeightedAverageFields);
            
            try
                OutOptionData = sortrows(OutOptionData,[1 2 3 4 5 6]);
            catch
                disp('Error while sorting rows: some NaN values are found');
            end
            
            OutHeader = {'PRODUCT_CODE','PRODUCT_NAME','CALL_PUT','BROKER','TMONTH',...
                'STRIKE','SOLD_LOT','ADJ_SOLD','OPEN_SOLD','BOT_LOT','ADJ_BOT',...
                'OPEN_BOT','NET_OPEN','REALIZED_PL','MTM','VAL','MKTVAL','MKT_RATE',...
                'OPENSOLDRATE','OPENBOTRATE','Currency','MKT_DATE'};
            
            %     OutFilename = getXLSFilename('OptionsReport');
            Filename = ['OptionsReport_',upper(InBUName)];
            OutFilename = getXLSFilename(Filename);
            
            xlswrite(OutFilename,[OutHeader;OutOptionData]);
            
            %% Upload into Database
            try
                
                ObjDB = connect_to_database;
                
                TradeDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
                
                TableName = strcat('rmo_option_report_table_',InBUName);
                set(ObjDB,'AutoCommit','off');
                SqlQuery = ['delete from ',TableName,' where mkt_date = ''',char(TradeDate),''' '];
                curs = exec(ObjDB,SqlQuery);
                commit(ObjDB);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB,'AutoCommit','on');
                
                upload_in_database(TableName, OutOptionData);
            catch
            end
            
        else
            OutErrorMsg = {'No live option trades found!'};
        end
        
    else
        OutErrorMsg = {['No Data for ',char(InBUName)]};
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
end