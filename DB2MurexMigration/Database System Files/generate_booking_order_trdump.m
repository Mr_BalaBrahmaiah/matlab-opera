function [OutErrorMsg,Clinet_Struct_Details,BO_TR_Dump] = generate_booking_order_trdump(InBUName,InXLSFilename)

%% Table Dependencies  Example

% booking_order_subportfolio_mapping_table_cfs;
% booking_order_derivative_type_mapping_table_cfs;
% client_master_data_cfs;


OutErrorMsg = {'No errors'};
Clinet_Struct_Details='';
BO_TR_Dump='';
%OutXLSFileName = '';

DB_Date_Format = 'yyyy-mm-dd';


Overall_ErrorMsg_Data = '';

%%

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    
    CombinationError_Index = 0;
    
    %%
    %     TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Strike',...
    %         'OptType','Prem','CounterParty','Initial Lots',...
    %         'CONTRACTNB','Maturity','Barrier Type',...
    %         'Barrier','Transaction Date','TRN.Number','Source',...
    %         'Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    %         'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp'};
    
    ObjDB = connect_to_database ;
    
    %% Vol_id table call.
    query_voli_id='select * from vol_id_table';
    [Header,Data]=Fetch_DB_Data(ObjDB,query_voli_id,[]);
    Commulative_data_vol_id=[Header;Data];
    Commulative_vol_id_dataset=cell2dataset(Commulative_data_vol_id);
    
    %% underlying List Table added rajashekhar 
%     query_underlying_id='select * from underlying_list_table';
%     [Header1,Data1]=Fetch_DB_Data(ObjDB,query_underlying_id,[]);
%     Commulative_data_underlying_id=[Header1;Data1];
%     Commulative_underlying_id_dataset=cell2dataset(Commulative_data_underlying_id);
    
    %% prod bus days table
    query_bus_days='select * from product_busdays_table';
    [Header_busdays,Data_busdays]=Fetch_DB_Data(ObjDB,query_bus_days,[]);
    commulative_data_bus_days=[Header_busdays;Data_busdays];
    commulative_data_bus_dataset=cell2dataset(commulative_data_bus_days);
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    sqlquery=['select column_name from tr_dump_column_format_table' '_' lower(InBUName) ' order by column_number asc'];
    
%     [~,TRHeader] = Fetch_DB_Data(ObjDB,'select column_name from tr_dump_column_format_table_cfs order by column_number asc');
    [~,TRHeader] = Fetch_DB_Data(ObjDB,sqlquery);
    TRHeader = TRHeader';
    
    DB_Date_Format = 'yyyy-mm-dd';
    [System_Date_Format,~] = get_System_Date_Format();
    
    [stHeader,stData] = Fetch_DB_Data(ObjDB,[],['client_european_struct_barrier_table_' lower(InBUName)]);
    % reading year mapping table
    [mapheader, mapdata]=Fetch_DB_Data(ObjDB,[],'year_mapping_table');
    Mapping_Tab_digit = cell2dataset([mapheader ; mapdata]);
    
    %% Get & Read Input Excel
%     if(isempty(InXLSFilename))
%         [Input_Filename, Input_Pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a Input File');
%         
%         if isnumeric(Input_Filename) && Input_Filename==0
%             OutErrorMsg = {'You did not selected the Input File Please Select Input File'} ;
%             return;
%         else
%             InXLSFilename = fullfile(Input_Pathname,Input_Filename);
%         end
%     end
    [~,~,RawData] = xlsread(InXLSFilename);
    
    DateIdx = find(strcmpi('Date',RawData));
    DateIdx = DateIdx(1);
    [RowNum,ColNum] = ind2sub(size(RawData),DateIdx);
    RawData = RawData(RowNum:end ,:); %% Entire data
    
    RawData_Header = RawData(1,:);  %% Headers of the input file name.
    RawData = RawData(2:end,:);  %% Data Except headers
    
    try
        %[~,Filename] = fileparts(InXLSFilename);
        Clinet_Struct_Details =getXLSFilename(['Client_Struct_Details','_',upper(InBUName)]);
        BO_TR_Dump=getXLSFilename(['BO_TR_Dump','_',upper(InBUName)]);
        
    catch
        Clinet_Struct_Details =getXLSFilename(['Client_Struct_Details','_',upper(InBUName)]);
        BO_TR_Dump=getXLSFilename(['BO_TR_Dump','_',upper(InBUName)]);
    end
    
    %% Validate the Input File
    
    Default_Header = {'Date','Product','Lots','Month','Strike','Put/Call/Fut','Prem/Rate','Client Code','Daily/EOD','Barrier','Expiry','TID','STR NAME'};
    
    %     if(length(Default_Header) == length(RawData_Header)) %% RawData_Header
    %
    %         Compare_Header = strcmpi(Default_Header,RawData_Header) ;
    %         if(find(Compare_Header == 0))
    %             Not_Match_Header =  char(RawData_Header(find(Compare_Header == 0))) ;
    %             Error_Str  = ['Input File Header does not Matched with Template Header : ',Not_Match_Header];
    %             OutErrorMsg = {Error_Str};
    %             return;
    %         end
    %
    %     else
    %         Not_Match_Header = char(setdiff(RawData_Header,Default_Header));
    %         if(isempty(Not_Match_Header))
    %             Not_Match_Header = char(setdiff(Default_Header,RawData_Header));
    %         end
    %         Error_Str = ['Input File Header length does not Matched with Template Header length : ', Not_Match_Header];
    %         OutErrorMsg = {Error_Str};
    %         return;
    %     end
    
    %% Empty and NaN Filed Checking
    UserWant_Header = {'Date','Product','Lots','Month','Strike','Put/Call/Fut','Prem/Rate','Client Code','Daily/EOD','Barrier','Expiry','Structure Description','TID','STR NAME'};
    Matched_UserWant_Col = cellStrfind_exact(RawData_Header,UserWant_Header);
    UserWant_Data = RawData(:,Matched_UserWant_Col);
    
    Date_Col = cellStrfind_exact(UserWant_Header,{'Date'});
    NaN_DateRow =  cellfun(@(V) any(isnan(V(:))), UserWant_Data(:,Date_Col));
    UserWant_Data(NaN_DateRow,:) = [];
    
    RawData_Header = UserWant_Header ;
    RawData = UserWant_Data;
    % rajashekhar added code 
    RawData_backup =RawData; 
    % end
    %     Put_Call_Fut_Col = find(cellfun(@(V) strcmpi('Put/Call/Fut',V), RawData_Header));
    %     Swap_Index = strcmpi(RawData(:,Put_Call_Fut_Col),'SWAP');
    %     RawData(Swap_Index,:) = [];
    
    %% Handle Merged Field structure Name
    
    STR_Name_Col = cellStrfind_exact(UserWant_Header,{'STR NAME'});
    TID_Col = cellStrfind_exact(UserWant_Header,{'TID'});
    Structure_Desc_Col = cellStrfind_exact(UserWant_Header,{'Structure Description'});
    
    Remove_Alphabet_Str = RawData(:,TID_Col);
    Temp_TID_AlphabetStr = cellfun(@(s) (s(end)),RawData(:,TID_Col),'UniformOutput',0); % We need remove Alphapet to match Structure Name
        
    % added to remove nan data from TID column rajashekhar 
    
    daatid=cellfun(@(V) any(isnan(V(:))), Temp_TID_AlphabetStr);
    Temp_TID_AlphabetStr(daatid,:) = [];
    Remove_Alphabet_Str(daatid,:) = [];
    RawData(daatid,:) = [];
    
    %  end 
    
    expr = {'[a-zA-Z]'};
    Alphapet_Index = ~cellfun(@isempty,regexp(Temp_TID_AlphabetStr,expr));
    Remove_Alphabet_Str(Alphapet_Index) = cellfun(@(s) (s(1:end-1)),Remove_Alphabet_Str(Alphapet_Index),'UniformOutput',0); %% Remove_Alphapet_Str
    
    Unique_TID = unique(Remove_Alphabet_Str);
    
    for c = 1 : length(Unique_TID)
        Current_Unique_TID = Unique_TID(c) ;
        Matched_Index = strcmpi(Remove_Alphabet_Str,Current_Unique_TID);
        if(~isempty(find(Matched_Index)))
            STR_Name_Index = find(Matched_Index,1);
            RawData(Matched_Index,STR_Name_Col) = RawData(STR_Name_Index,STR_Name_Col);
            RawData(Matched_Index,Structure_Desc_Col) = RawData(STR_Name_Index,Structure_Desc_Col);
        end
        
    end
    
    %% Derivative Mapping & Counterparty Mappping  Table
    
    DerivativeType_Mapping_Table = strcat('booking_order_derivative_type_mapping_table_',InBUName);
    [DerivativeType_Mapping_Table_ColNames,DerivativeType_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,[],DerivativeType_Mapping_Table);
    if(isempty(DerivativeType_Mapping_Table_Data))
        OutErrorMsg = {['Not getting data from ',char(DerivativeType_Mapping_Table)]};
        return;
    end
    
    if(strcmpi(DerivativeType_Mapping_Table_Data,'No Data'))
        OutErrorMsg = {['Not getting data from ',char(DerivativeType_Mapping_Table)]};
        return;
    end
    
    Counterparty_Mapping_Table = strcat('client_master_data_',InBUName);
    [Counterparty_Mapping_Table_ColNames,Counterparty_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,[],Counterparty_Mapping_Table);
    if(isempty(Counterparty_Mapping_Table_Data))
        OutErrorMsg = {['Not getting data from ',char(Counterparty_Mapping_Table)]};
        return;
    end
    if(strcmpi(Counterparty_Mapping_Table_Data,'No Data'))
        OutErrorMsg = {['Not getting data from ',char(Counterparty_Mapping_Table)]};
        return;
    end
    
    
    DerivativeType_Mapping_Table_Data = cell2dataset([DerivativeType_Mapping_Table_ColNames ;DerivativeType_Mapping_Table_Data]);
    Counterparty_Mapping_Table_Data = cell2dataset([Counterparty_Mapping_Table_ColNames ;Counterparty_Mapping_Table_Data]);
    
    %% Uploading Format
    % added rajashekhar to fill the expiry data if not given
    [ RawData,OutErrorMsg] = fillexpiry_date( RawData_Header ,RawData,Commulative_vol_id_dataset );
%     RawData(:,2)=strrep(RawData(:,2) ,'^',{' '});
    % end rajashekhar
    Input_RawData_previous=[RawData_Header;RawData];
    Input_RawData_previous(cell2mat(cellfun(@(x)any(isnan(x)),Input_RawData_previous,'UniformOutput',false))) = {[]};
    Input_RawData = cell2dataset(Input_RawData_previous);
  
    
    Unwound_Input_RawData_Index = find(~cellfun(@isempty, regexp(Input_RawData.StructureDescription,'unwound'))) ;
    Unwound_Input_RawData = Input_RawData(Unwound_Input_RawData_Index,:);
    Input_RawData(Unwound_Input_RawData_Index,:) = [];
    Input_RawData_backup=Input_RawData;
    
    %% New Section of code
  
    Input_RawData_cell=dataset2cell(Input_RawData);
    idx_product=find(strcmpi(Input_RawData_cell(1,:),'product'));
    month_idx=find(strcmpi(Input_RawData_cell(1,:),'Month'));
    Input_RawData_cell(1,:)=[];
    data_idx_product=Input_RawData_cell(:,idx_product);
    
    %% Detection of last element
    tf=find(endsWith( data_idx_product,'^'));
    if ~isempty(tf)
    required_data_idx=char(data_idx_product(tf,:));
    required_data_idx(:,end)=[];
    c={' '};
    required_data_idx_formatted=strcat(required_data_idx,c);
    % added rajashekhar
    RawData(:,2)=strrep(RawData(:,2) ,'^',{' '});
   %end
    %% Check analysis
    data_idx_product(tf)=cellstr(required_data_idx_formatted);
    Input_RawData_cell(:,2)=data_idx_product;
    end
    %% MOnth column required data fetch
    
    month_idx_data=Input_RawData_cell(:,month_idx);
    %% DEtection of decimal and removal of required data.
   [first,second]=strtok(month_idx_data,'.');
   Input_RawData_cell( :, month_idx)=second;
   
  %% Nan replacement with empty cell
%   index_nan=find(cell2mat(cellfun(@(x)any(isnan(x)),a,'UniformOutput',false)))
   a_header={'Date','Product','Lots','Months','Strike','Put_Call_Fut','Prem_Rate','ClientCode','Daily_EOD','Barrier','Expiry','StructureDiscription','TID','STRNAME'};
   Overall_set=[a_header;Input_RawData_cell];
   
   %% Nan Removal from cell
   Overall_set(cell2mat(cellfun(@(x)any(isnan(x)),Overall_set,'UniformOutput',false))) = {[]};
   Overall_set_used=cell2dataset( Overall_set);
 
    Uplodaing_Data_Header = {'Contract_Num','Trade_Date','Option_Expiry_Date','Contract_Month','Client_Code','Structure_Name','FUT_CALL_PUT',...
        'Daily_EOD','Notional_Lots','Traded_Price','Barrier_Type','Lots_Per_Day','ACC_Level_1','Acc_Level_2','Barrier_Level','future_expiry_date','underlying_future_id','Fx_value_date' };
    
    Uploading_Data = cell2dataset([Uplodaing_Data_Header ; cell(size(RawData,1),size(Uplodaing_Data_Header,2))] ) ;
    
    for k = 1: size(Input_RawData,1)
        if strcmpi(Input_RawData.Put_Call_Fut(k),'Future') || strcmpi(Input_RawData.Put_Call_Fut(k),'Put') || strcmpi(Input_RawData.Put_Call_Fut(k),'Call')
            z=char(Input_RawData_backup.Month(k));
            y=char(Input_RawData_backup.TID(k));
            index_power_one=strfind(char(y),'^');
            if (~isempty(index_power_one))
                y(index_power_one)=[];
            end
            Uploading_Data.Contract_Num(k)=cellstr(y);
        elseif strcmpi(Input_RawData.Put_Call_Fut(k),'SWAP')
%             z=char(Input_RawData_backup.Month(k));
              z=char(Input_RawData_backup.Strike(k));
            y=char(Input_RawData_backup.TID(k));
            index_power_one=strfind(char(y),'^');
            if (~isempty(index_power_one))
                y(index_power_one)=[];
            end
            Uploading_Data.Contract_Num(k)=cellstr(y);
        else
            z=char(Input_RawData_backup.Month(k));
            y=char(Input_RawData_backup.TID(k));
            index_power_one=strfind(char(y),'^');
            if (~isempty(index_power_one))
                y(index_power_one)=[];
            end
            Uploading_Data.Contract_Num(k)=cellstr(y);
        end
        %% Power index removal
       
        
        %% Underscore data access
         indexes_underscore=strfind( y,'_');
         used_data=y(indexes_underscore(1)+1:indexes_underscore(2)-1);
%         required_data=char(Uploading_Data.Contract_Num(k));
        %% 
%         if startsWith(z,'FD')
         if startsWith(z,'VD')
            index_FD=strfind(z,':');
           b= z(index_FD+1:end);
          required_date= upper(datestr(datenum(b,'dd-mm-yyyy'),'mmmyy'));
          Uploading_Data.Contract_Month(k)=strcat(cellstr(used_data),{' '},{''''},cellstr(required_date),{''''});
        else
            data_used=z(1);
            [month]= moth_mapping_opera(data_used);
            [fs,~]=strtok(z,'.');
            if length(strtrim(fs))==2
                year_f=strtrim(char(fs));
                year_number=str2double(year_f(end));
                map_pos= find(ismember(Mapping_Tab_digit.single_digit_year,year_number));
                if ~isempty(map_pos)
                    Pre_Year_ltr = Mapping_Tab_digit.double_digit_year(map_pos);
                    %                     year_f=strrep(year_f,num2str(number),num2str(Pre_Year_ltr));
                    Uploading_Data.Contract_Month(k)=strcat(cellstr(used_data) ,{' '},{''''},cellstr(month),{''''},num2str(Pre_Year_ltr));
                else
                    Uploading_Data.Contract_Month(k)=strcat(cellstr(used_data) ,{' '},{''''},cellstr(month),{''''},'1',num2str(year_number));
                end
            else
                Uploading_Data.Contract_Month(k)=strcat( cellstr(used_data),{' '},{''''},cellstr(month),{''''},fs(2:end));
            end
%             Uploading_Data.Contract_Month(k)=strcat( cellstr(used_data),{' '},{''''},cellstr(month),{''''},datestr(datenum(Input_RawData_backup.Date(k),'dd-mm-yyyy'),'yy'));
            
            
        end
        
            
%         index_power_one=strfind(char(z),'^');
%         if (~isempty(index_power_one))
%             
%             z(index_power_one)=[];
%         end
        
%         Uploading_Data.Contract_Num(k) = cellstr(z);
        Uploading_Data.Trade_Date(k) = cellstr(datestr(datenum(Input_RawData_backup.Date(k),'dd-mm-yyyy'),'yyyy-mm-dd'));
%         index_power=strfind( char(Uploading_Data.Contract_Num(k)),'^');
%         if (~isempty(index_power))
%             required_data(index_power)=[];
%         end
%         indexes_underscore=strfind( char(Uploading_Data.Contract_Num(k)),'_');
%         required_data=char(Uploading_Data.Contract_Num(k));
% %         if startswith
      
%         used_data=required_data(indexes_underscore(1)+1:indexes_underscore(2)-1);
%         data_function_input=used_data(1);
%         [month]= moth_mapping_opera(data_function_input);
%         Uploading_Data.Contract_Month(k)=strcat( cellstr(used_data),{''''},cellstr(month),{''''},datestr(datenum(Input_RawData_backup.Date(k),'dd-mm-yyyy'),'yy'));
        %Uploading_Data.Contract_Month(k) = Input_RawData_backup.Month(k);
        Uploading_Data.Client_Code(k) = Input_RawData_backup.ClientCode(k);
        
        Uploading_Data.underlying_future_id(k)=strcat(Overall_set_used.Product(k),Overall_set_used.Months(k));
        
        % added to replace underlying id sting '.' with ' '
        Uploading_Data.underlying_future_id(k)=strrep(Uploading_Data.underlying_future_id(k),'.',' ');
        %% Expiry date fix for upload Data format
%       if   endsWith(char(Overall_set_used.Product(k)),' ')
%           char(Overall_set_used.Product(k))
%           
        tf=endsWith(Overall_set_used.Product(k),' ');
        if tf==1
%             id_formatted_variable=char(strcat(Overall_set_used.Product(k),{' '},Overall_set_used.Months(k)));
            id_formatted_variable=char(strcat(Overall_set_used.Product(k),Overall_set_used.Months(k)));
        else
       
        [le_one]=length(char(Overall_set_used.Product(k)));  %% Change this logic.
        if le_one==1
               id_formatted_variable=char(strcat(Overall_set_used.Product(k),{' '},Overall_set_used.Months(k)));
        else
        id_formatted_variable=char(strcat(Overall_set_used.Product(k),Overall_set_used.Months(k)));
        end
        end
        
    %% Dot Index calculation.    
        bb=strfind(id_formatted_variable,'.');
        
     %% Exception handling of empty and  removal of dots.
        if(~isempty(bb))
        id_formatted_variable(bb)=[];
        id_formatted_used=cellstr(id_formatted_variable);
        end
        
        %% manipulated dataset comparison with Raw data which is been read.
        idx_prod_for_option_detection=find(strcmpi(Overall_set_used.Product(k),RawData(:,2))); %% Make dynamic
        
       if (~isempty(char(Overall_set_used.Expiry{k})))
           structure_detection=Overall_set_used.Put_Call_Fut(k);
           if strcmpi(structure_detection,'Future')
           Uploading_Data.future_expiry_date(k)=cellstr(datestr(datenum(Overall_set_used.Expiry(k),'dd-mm-yyyy'),'yyyy-mm-dd'));
           else
               if strcmpi(  structure_detection,'Put') || strcmpi(  structure_detection,'Call') || strcmpi(  structure_detection,'Cuo') || strcmpi(  structure_detection,'puo') ...
                       || strcmpi(  structure_detection,'pdo')|| strcmpi(  structure_detection,'Cdo')   || strcmpi(  structure_detection,'cdi')|| strcmpi(  structure_detection,'Cui') ...
                         || strcmpi(  structure_detection,'pdi')|| strcmpi(  structure_detection,'pui') || strcmpi(  structure_detection,'ecui')|| strcmpi(  structure_detection,'ecuo') ...
                        || strcmpi(  structure_detection,'epui')|| strcmpi(  structure_detection,'epuo') || strcmpi(  structure_detection,'epdi')|| strcmpi(  structure_detection,'epdo') ... 
                        || strcmpi(  structure_detection,'ecdi')|| strcmpi(  structure_detection,'ecdo') 
%                    Uploading_Data.Option_Expiry_Date(k)=cellstr(datestr(datenum(Overall_set_used.Expiry(k),'dd-mm-yyyy'),'yyyy-mm-dd'));
                    % added rajashekhar 
                    expery_call_put=cellstr(datestr(datenum(Overall_set_used.Expiry(k),'dd-mm-yyyy'),'yyyy-mm-dd'));
                    if size(expery_call_put,1)>0
                        Uploading_Data.Option_Expiry_Date(k)=expery_call_put;
                        required_expiry_date=datenum(expery_call_put);
                        date_manipulation=required_expiry_date+1; 
                        required_format_expiry =datestr(date_manipulation,'yyyy-mm-dd');
                        DB_Date_Format = 'yyyy-mm-dd';
                            HolDates = {'2012-01-01'};
                            HolidayVec = datenum(HolDates,'yyyy-mm-dd');
                            start_date={'2017-12-31'};
                            %startvector=datenum(start_date,'yyyy-mm-dd');
                            finish_date={'2040-12-31'};
                            finish_vector=datenum(finish_date,'yyyy-mm-dd');
                            VBusDays=busdays(min(datenum(start_date,DB_Date_Format)),max(finish_vector),'daily',HolidayVec);
                            Busdays_Array=cellstr(datestr(VBusDays,DB_Date_Format));
                            
                            %% Date manipulation
                            hh=find(strcmpi(required_format_expiry,Busdays_Array));
                            if isempty(hh)
                                for i=1:length(Busdays_Array)
                                    hh=find(strcmpi(datestr(date_manipulation+i,DB_Date_Format),Busdays_Array));
                                    if (~isempty(hh))
                                        %Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));
                                        Uploading_Data.future_expiry_date(k)= Busdays_Array(hh);
                                        break;
                                    end
                                end
                            else
                                %Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));  %% added rajashekhar
                                Uploading_Data.future_expiry_date(k)= Busdays_Array(hh);
                            end
                    else
                        stringdata=strcat(used_data,first(k));
                        ab_required= find(contains(Commulative_vol_id_dataset.bbg_opt_ticker,stringdata));
                        
                        if (~isempty(ab_required))
                            required_expiry_date=max(datenum(Commulative_vol_id_dataset_dataset.expiry_date(ab_required)));
                            required_format_expiry_date=datestr(required_expiry_date,'yyyy-mm-dd');
                            date_manipulation=required_expiry_date+1;
                            Uploading_Data.Option_Expiry_Date(k)=required_format_expiry_date;
                            %                         expirydate_id=find(strcmpi(Commulative_underlying_id_dataset.bbg_underlying_id,stringdata));
                            %                         Uploading_Data.Option_Expiry_Date(k)=Commulative_underlying_id_dataset.expiry_date(expirydate_id);
                            DB_Date_Format = 'yyyy-mm-dd';
                            HolDates = {'2012-01-01'};
                            HolidayVec = datenum(HolDates,'yyyy-mm-dd');
                            start_date={'2017-12-31'};
                            %startvector=datenum(start_date,'yyyy-mm-dd');
                            finish_date={'2040-12-31'};
                            finish_vector=datenum(finish_date,'yyyy-mm-dd');
                            VBusDays=busdays(min(datenum(start_date,DB_Date_Format)),max(finish_vector),'daily',HolidayVec);
                            Busdays_Array=cellstr(datestr(VBusDays,DB_Date_Format));
                            
                            %% Date manipulation
                            hh=find(strcmpi(required_format_expiry,Busdays_Array));
                            if isempty(hh)
                                for i=1:length(Busdays_Array)
                                    hh=find(strcmpi(datestr(date_manipulation+i,DB_Date_Format),Busdays_Array));
                                    if (~isempty(hh))
                                        Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));
                                        break;
                                    end
                                end
                            else
                                Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));  %% added rajashekhar
                            end
                        end
                    end
                      %end of edit rajashekhar
               end
           end
           
       else    
        if (~isempty(idx_prod_for_option_detection))
            fut_selection=Overall_set_used.Put_Call_Fut(k);
           % aa_required= find(endsWith(Commulative_vol_id_dataset.underlying_id,id_formatted_used));
           aa_required= find(contains(Commulative_vol_id_dataset.bbg_opt_ticker,id_formatted_used));
           %aa_required= find(strcmpi(Commulative_vol_id_dataset.bbg_opt_ticker,id_formatted_used));
                           
        end
         
        if (~isempty(aa_required))
            required_expiry_date=max(datenum(Commulative_vol_id_dataset.expiry_date(aa_required)));
            required_format_expiry_date=datestr(required_expiry_date,'yyyy-mm-dd');
            
            
            
            if  strcmpi(fut_selection,'Put') || strcmpi(fut_selection,'Call')                
                Uploading_Data.Option_Expiry_Date(k)=  cellstr(datestr(datenum(required_format_expiry_date,'dd-mm-yyyy'),'yyyy-mm-dd'));
                 
            else 
                if strcmpi(fut_selection,'Future')
                
                    date_manipulation=required_expiry_date+1;
                    required_format_expiry=datestr(date_manipulation,'yyyy-mm-dd');
                    
                    %% Business day calculation
                    DB_Date_Format = 'yyyy-mm-dd';
                    HolDates = {'2012-01-01'};
                    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
                    start_date={'2017-12-31'};
                    %startvector=datenum(start_date,'yyyy-mm-dd');
                    finish_date={'2040-12-31'};
                    finish_vector=datenum(finish_date,'yyyy-mm-dd');
                    VBusDays=busdays(min(datenum(start_date,DB_Date_Format)),max(finish_vector),'daily',HolidayVec);
                    Busdays_Array=cellstr(datestr(VBusDays,DB_Date_Format));
                    
                    %% Date manipulation
                    hh=find(strcmpi(required_format_expiry,Busdays_Array));
                    if isempty(hh)
                        for i=1:length(Busdays_Array)
                            hh=find(strcmpi(datestr(date_manipulation+i,DB_Date_Format),Busdays_Array));
                            if (~isempty(hh))
%                                   Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));
                                  Uploading_Data.future_expiry_date(k)= Busdays_Array(hh);
                                  break;
                            end
                        end
                    else
                      Uploading_Data.future_expiry_date(k)= Busdays_Array(hh);  %% added rajashekhar
%                       Uploading_Data.future_expiry_date(k)= cellstr((commulative_data_bus_dataset.value_date(hh)));

                    end
                
                else
                    if strcmpi(fut_selection,'SWAPS')
                     Uploading_Data.future_expiry_date(k)={' '};
                    end
                end
            end
        end
       end
   
 
        if(~strcmpi(Input_RawData.Product(k),'OR'))
           % Uploading_Data.Option_Expiry_Date(k) = Input_RawData.Expiry(k);
            Uploading_Data.Structure_Name(k) = Input_RawData.STRNAME(k);
            Uploading_Data.Daily_EOD(k) = Input_RawData.Daily_EOD(k);
        end
        
        % rajashekhar added this code 
        if strncmpi(Input_RawData.Put_Call_Fut(k),'P',1)|| strncmpi(Input_RawData.Put_Call_Fut(k),'EP',2)|| strncmpi(Input_RawData.Put_Call_Fut(k),'AP',2)
           Uploading_Data.FUT_CALL_PUT(k) = {'PUT'};
        elseif strncmpi(Input_RawData.Put_Call_Fut(k),'C',1)|| strncmpi(Input_RawData.Put_Call_Fut(k),'EC',2)|| strncmpi(Input_RawData.Put_Call_Fut(k),'AC',2)
             Uploading_Data.FUT_CALL_PUT(k) = {'CALL'};
        else
           Uploading_Data.FUT_CALL_PUT(k) = Input_RawData.Put_Call_Fut(k);
        end
        % end
          Uploading_Data.Notional_Lots(k) = num2cell(Input_RawData.Lots(k));
        try
            Uploading_Data.Traded_Price(k) = num2cell(round(Input_RawData.Prem_Rate(k),6));
        catch
            Uploading_Data.Traded_Price(k) = num2cell(round(Input_RawData.Prem_Rate{k},6));
        end
        % rajashekhar added 
%         Barrier_level=Input_RawData.Barrier{k};
        
        if ~isempty(Input_RawData.Barrier{k})
        Uploading_Data.Barrier_Level(k) =  num2cell(Input_RawData.Barrier{k});
        end

        Current_ContractNo = Input_RawData.TID(k);
        Current_StructureName =  Input_RawData.STRNAME(k);  
        Current_OptType = Input_RawData.Put_Call_Fut(k);
        
        if(isnumeric(cell2mat(Current_StructureName)) && sum(strcmpi({'Future','SWAP'},Current_OptType))>0 )
            if(strcmpi(Current_OptType,'SWAP'))
                Uploading_Data.Notional_Lots(k) =  num2cell(abs(round(Input_RawData.Lots(k),6))) ;                
%                 Split_Month = strsplit(char(Input_RawData.Month(k)),':');
                Split_Month = strsplit(char(Input_RawData.Strike(k)),':');
                TempDate = strtrim(Split_Month(end));
                Uploading_Data.Option_Expiry_Date(k) = cellstr(datestr(datenum(TempDate,'dd-mmm-yy'),'yyyy-mm-dd'));
               
                [va ,~]=strtok(Input_RawData.Strike(k),':');
                if strcmpi(va,'VD')
                Split_Strike = strsplit(char(Input_RawData.Strike(k)),':');
                TempDate1 = strtrim(Split_Strike(end));
                Uploading_Data.Fx_value_date(k) = cellstr(datestr(datenum(TempDate1,'dd-mmm-yy'),System_Date_Format));
                reqformat_date=cellstr(datestr(datenum(TempDate,'dd-mmm-yy'),'yyyymmdd'));
                Uploading_Data.underlying_future_id(k) =strcat(Input_RawData.Product(k),{' '}, reqformat_date ,' BGN');
                end
                Uploading_Data.Traded_Price(k) = num2cell(0);
                Uploading_Data.ACC_Level_1(k) = num2cell(round(Input_RawData.Prem_Rate(k),6));
            else
                Uploading_Data.Notional_Lots(k) =  num2cell(abs(round(Input_RawData.Lots(k),6))) ;
                Uploading_Data.ACC_Level_1(k) = num2cell(Input_RawData.Strike(k));
                
                Uploading_Data.Traded_Price(k) = num2cell(0);
                Uploading_Data.ACC_Level_1(k) = num2cell(round(Input_RawData.Prem_Rate(k),6));
            end
        elseif(~isnumeric(cell2mat(Current_StructureName)) && strcmpi({'Future'},Current_OptType) )
            Uploading_Data.Notional_Lots(k) =  num2cell(abs(round(Input_RawData.Lots(k),6))) ;
            Uploading_Data.ACC_Level_1(k) = num2cell(Input_RawData.Strike(k));
            
            Uploading_Data.Traded_Price(k) = num2cell(0);
            Uploading_Data.ACC_Level_1(k) = num2cell(round(Input_RawData.Prem_Rate(k),6));
            
        elseif(isnumeric(cell2mat(Current_StructureName)) && sum(strcmpi({'Call','Put'},Current_OptType))>0 )
            try
                Uploading_Data.Notional_Lots(k) =  num2cell( abs(Input_RawData.Lots(k)));
            catch
                Uploading_Data.Notional_Lots(k) =  num2cell( abs(Input_RawData.Lots{k}));
            end
            %Uploading_Data.ACC_Level_1(k) = num2cell(Input_RawData.Strike(k));
            try
            Uploading_Data.ACC_Level_1(k) = Input_RawData.Strike(k);
            catch
             Uploading_Data.ACC_Level_1(k) = num2cell(Input_RawData.Strike(k));
            end
            Uploading_Data.Traded_Price(k) = num2cell(Input_RawData.Prem_Rate(k));
        else
            Matched_ClientStatementType = DerivativeType_Mapping_Table_Data.client_statement_type(strcmpi(DerivativeType_Mapping_Table_Data.booking_order_type,Current_OptType));
            if(~isempty(char(Matched_ClientStatementType)))
                Uploading_Data.Barrier_Type(k) = Matched_ClientStatementType;                
            end
            
            Current_TradeDate = datenum(Input_RawData.Date(k),'dd-mm-yyyy');
            Current_ExpiryDate = datenum(Input_RawData.Expiry(k),'dd-mm-yyyy');
            Network_Days = size(busdays(Current_TradeDate,Current_ExpiryDate,'daily','2016-12-25'),1) ;
            
            if(strcmpi(Uploading_Data.Daily_EOD(k),'EOD'))
                Uploading_Data.Notional_Lots(k) =  num2cell(abs(round(Input_RawData.Lots(k),6))) ;
            else % Daily
                Uploading_Data.Notional_Lots(k) =  num2cell(abs(round( Input_RawData.Lots(k) * Network_Days ,6)));
                Uploading_Data.Lots_Per_Day(k) = num2cell(abs(round(Input_RawData.Lots(k),6))) ;
            end
            
            Acc_Level_Index = find( strcmpi(Input_RawData.STRNAME,Current_StructureName) & strcmpi(Input_RawData.TID,Current_ContractNo)); %% strcmpi(Input_RawData.Put_Call_Fut,Current_OptType)
            if(~isempty(Acc_Level_Index))
                % We need to show First Row of Lots & Notional ,becuse we
                % need to consolidte show it to single line
                Uploading_Data.Notional_Lots(Acc_Level_Index) = Uploading_Data.Notional_Lots(Acc_Level_Index(1)) ;
                Uploading_Data.Lots_Per_Day(Acc_Level_Index) = Uploading_Data.Lots_Per_Day(Acc_Level_Index(1)) ;
                Uploading_Data.Daily_EOD(Acc_Level_Index) = Uploading_Data.Daily_EOD(Acc_Level_Index(1)) ;
                
%                 STEED_Index = ~isempty( find(~cellfun(@isempty, regexp(Current_StructureName,'STEED'))) );
%                 STEEP_Index = ~isempty( find(~cellfun(@isempty, regexp(Current_StructureName,'STEEP'))) );
%                 STEP_Index = ~isempty( find(~cellfun(@isempty, regexp(Current_StructureName,'STEP'))) );
%                 if( STEED_Index | STEEP_Index | STEP_Index )
%                     Split_Description = strsplit(char(Input_RawData.StructureDescription(k)),' ');
%                     Required_Split_Str = strsplit(char(Split_Description(9)),'/') ;
%                     if(length(Required_Split_Str)==2)
%                         Uploading_Data.ACC_Level_1(k) = num2cell(str2double(char(Required_Split_Str(1))));
%                         Uploading_Data.Barrier_Level(k) = num2cell(str2double(char(Required_Split_Str(2))));
%                     else
%                         Uploading_Data.ACC_Level_1(k) = num2cell(str2double(char(Required_Split_Str(1))));
%                         Uploading_Data.Acc_Level_2(k) = num2cell(str2double(char(Required_Split_Str(2))));
%                         Uploading_Data.Barrier_Level(k) = num2cell(str2double(char(Required_Split_Str(3))));
%                     end
%                     
%                 else
                    try
                        Unique_Strike = unique(cell2mat(Input_RawData.Strike(Acc_Level_Index)),'stable');
                        Unique_Strike = Unique_Strike(~isnan(Unique_Strike));
                    catch
                        Unique_Strike = unique(Input_RawData.Strike(Acc_Level_Index),'stable');
                        Unique_Strike = Unique_Strike(~isnan(Unique_Strike));
                    end
                    if(length(Unique_Strike)==1)
                        Uploading_Data.ACC_Level_1(k) =  num2cell(Unique_Strike(1));
                    else
                        Uploading_Data.ACC_Level_1(k) =  num2cell(Unique_Strike(1));
                        Uploading_Data.Acc_Level_2(k) =  num2cell(Unique_Strike(2));
                    end
                %end
            end
            
        end
    end
    
    UniqueFields = Uplodaing_Data_Header;
    UniqueFields(strcmpi(Uplodaing_Data_Header,'FUT_CALL_PUT')) = [] ; %% Remove FUT_CALL_PUT column only
    SumFields = [];
    OutputFields = Uplodaing_Data_Header;
    WeightedAverageFields = [];
    
    Uploading_Data = dataset2cell(Uploading_Data);
    Uploading_Data(1,:) = [];
    % commented rajashekhar
%     Uploading_Data(:,15)={[]};
    
    %% get the barrier type and values from data base if the structure name matches excel and database   
    
    Unique_TID_1 = Uploading_Data(:,1);
    
    % added by rajashekhar to add barrier type from data base
     for r = 1 : length(Unique_TID_1)
        Current_Unique_TID_1 = Unique_TID_1(r) ;
        Matched_Index = strcmpi(Uploading_Data(:,1),Current_Unique_TID_1);        
        nonempty_string = RawData(Matched_Index ,STR_Name_Col);
         data_string= nonempty_string{1}; 
         if ischar (data_string)
             Structure_Desc_Col = cellStrfind_exact(stData(:,1),nonempty_string(1));
             if ~isempty(Structure_Desc_Col)
                 required_data_temp=stData(Structure_Desc_Col,:);
                  temp_requireddata=RawData(Matched_Index,:);
                  if ~isempty(required_data_temp{2})
%                   if  ~strcmpi(required_data_temp{2},'null')
                     Uploading_Data(Matched_Index,11)=required_data_temp(2);
%                      temp_requireddata=RawData(Matched_Index,:);
                  end
                 
%                  if  ~strcmpi(required_data_temp{3},'null')
                  if ~isempty(required_data_temp{3})
                     acclevel1=temp_requireddata(str2double(required_data_temp{3}),5);
                     Uploading_Data(Matched_Index,13)= acclevel1;
                 else
                     Uploading_Data(Matched_Index,13)={[]};
                 end
%                  if ~strcmpi(required_data_temp{4},'null')
                  if ~isempty(required_data_temp{4})
                      if strfind(required_data_temp{4},'/')
                          id=strfind(required_data_temp{4},'/');
                          acclevel2=cellstr([num2str(cell2mat(temp_requireddata(str2double(required_data_temp{4}(1:id-1)),5))) '/' num2str(cell2mat(temp_requireddata(str2double(required_data_temp{4}(id+1:end)),5)))]);
                          Uploading_Data(Matched_Index,14)=acclevel2;
                      else
                     acclevel2=temp_requireddata(str2double(required_data_temp{4}),5);
                     Uploading_Data(Matched_Index,14)=acclevel2;
                      end
                 else
                      Uploading_Data(Matched_Index,14)={[]};
                 end
%                  if ~strcmpi(required_data_temp{5},'null') 
                      
                  if ~isempty(required_data_temp{5}) || ~isempty(required_data_temp{6})
                      if ~isempty(required_data_temp{6})
                        barrierlevel1=temp_requireddata(str2double(required_data_temp{6}),10); 
                      else
                        barrierlevel1=[]; 
                      end
                        if ~isempty(required_data_temp{5})                            
                             barrierlevel=temp_requireddata(str2double(required_data_temp{5}),5);  
                        else
                               barrierlevel=[];  
                        end
                        if ~isempty(barrierlevel1) && ~isempty(barrierlevel) 
                             Uploading_Data(Matched_Index,15)=cellstr([num2str(cell2mat(barrierlevel1)) '/' num2str(cell2mat(barrierlevel))]);
                        elseif  isempty(barrierlevel1) || ~isempty(barrierlevel) 
                            Uploading_Data(Matched_Index,15)= barrierlevel;
                         elseif  ~isempty(barrierlevel1) || isempty(barrierlevel) 
                              Uploading_Data(Matched_Index,15)= barrierlevel1;
                        end
                 else
                       Uploading_Data(Matched_Index,15)={[]};
                 end
      
            end
        end
%         
     end
    
% end here
 %%   
    [Consolidate_Header,Consolidate_Uploading_Data] = consolidatedata(Uplodaing_Data_Header, Uploading_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    % rajashekhar added to change the value in underlying ID 
    

    undid_pos=find(strcmpi(Consolidate_Header,'underlying_future_id'));
    under_data=Consolidate_Uploading_Data(:,undid_pos);
    
    under_data1=[];
    
    for aa=1:length(under_data)
        current_unda=under_data{aa};
        tw=strfind(under_data{aa} ,'BGN');
        if tw==1
            under_data1{aa,1}=current_unda;
        else
            [us,ub]=strtok(current_unda,' ');
            if size(strtrim(char(ub)),2)==2
                year_f=strtrim(char(ub));
                number=str2double(year_f(end));
                
                map_pos= find(ismember(Mapping_Tab_digit.single_digit_year,number));
                if ~isempty(map_pos)
                    Pre_Year_ltr = Mapping_Tab_digit.double_digit_year(map_pos);
                    year_f=strrep(year_f,num2str(number),num2str(Pre_Year_ltr));
                    
                    % to avoid loop 
                    %                 for j=1:1:length(Mapping_Tab_digit)
                    %                     Mapping_Tab_digit_val=Mapping_Tab_digit.single_digit_year(j);
                    %                     if isequal(number,Mapping_Tab_digit_val)
                    %                         Pre_Year_ltr = Mapping_Tab_digit.double_digit_year(j);
                    %                         year_f=strrep(year_f,num2str(number),num2str(Pre_Year_ltr));
                    %                         break;
                    %                     end
                    %                 end
                    under_data1{aa,1}=cell2mat(strcat(us ,{' '},year_f));
                else
                    under_data1{aa,1}=cell2mat(strcat(us ,{' '},year_f));
                end
            else
                under_data1{aa,1}=current_unda;
            end
        end
        
    end

    Consolidate_Uploading_Data(:,undid_pos)=under_data1;
    % removing data from TRDUMP table
%     xlswrite(BO_TR_Dump,[Consolidate_Header;Consolidate_Uploading_Data],'Uploading Data');
    % end
    
    % Making Date Format column into DB uploading Format
    Trade_Date_Col = cellStrfind_exact(Consolidate_Header,{'Trade_Date'});
    Option_Expiry_Date_Col = cellStrfind_exact(Consolidate_Header,{'Option_Expiry_Date'});
    Consolidate_Uploading_Data(:,Trade_Date_Col) = cellstr(Consolidate_Uploading_Data(:,Trade_Date_Col));
    
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Consolidate_Uploading_Data(:,Option_Expiry_Date_Col));
    Consolidate_Uploading_Data(~Nan_Row,Option_Expiry_Date_Col) = Consolidate_Uploading_Data(~Nan_Row,Option_Expiry_Date_Col);
    Consolidate_Uploading_Data(Nan_Row,Option_Expiry_Date_Col) = {''};
    assign_check=nan;
    Consolidate_Uploading_Data(cell2mat(cellfun(@(x)any(isempty(x)), Consolidate_Uploading_Data,'UniformOutput',false))) = num2cell(assign_check);
        
    Consoildate_data_with_headers=[ Uplodaing_Data_Header;Consolidate_Uploading_Data];
    xlswrite(Clinet_Struct_Details, Consoildate_data_with_headers,'struct_data');
    xls_delete_sheets(fullfile(pwd,Clinet_Struct_Details));
%     xls_delete_namedsheet([pwd filesep Clinet_Struct_Details],'Sheet1')
    
    % Uploading Part
%         TableName = strcat('client_statement_booking_order_information_table_',char(InBUName));       
%         upload_in_database(TableName, Consolidate_Uploading_Data);
%         cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date :  ',char(Settle_Date)]);
    
    
    
    %% Get 'Instrument'    'Subportfolio'    'Strike Factor'    'Premium'  Informations
    
    Default_TableName = 'booking_order_subportfolio_mapping_table_';
    % InBUName = 'cot';
    Table_Name =  strcat(Default_TableName,InBUName); %% 'subportfolio_product_mapping_table' ; %% strcat(Default_TableName,InBUName) ;
    
    Product_Col = find(cellfun(@(V) strcmpi('Product',V), RawData_Header));
    Put_Call_Fut_Col = find(cellfun(@(V) strcmpi('Put/Call/Fut',V), RawData_Header));
    
    Modify_Map_Data = cell(size(RawData,1),7);
    Modify_Map_Data(:,1) = {InBUName};
     % rajashekhar added
      RawData =  RawData_backup;
      % end
    CombinationErrorMsg = cell(size(RawData,1),1);
    
    for i = 1 : size(RawData,1)
        
        if(strcmpi(RawData(i,Product_Col),'USDBRL'))
            Current_Product =  char(strcat({'CR '},RawData(i,Product_Col))) ;
        else
            Current_Product =  char(strcat({'CM '},RawData(i,Product_Col))) ;
        end
        Current_Product = strrep(Current_Product,'^','');
        SqlQuery = ['SELECT product_code,portfolio,instrument,subportfolio,strike_factor,premium_factor FROM ',Table_Name,' where subportfolio like ''RMS%'' and product_code = ''' Current_Product ''' ' ] ;
        
        [Map_ProdTbl_Fileds,Map_ProdTbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        if(~isempty(Map_ProdTbl_Data))
            if(size(Map_ProdTbl_Data,1)>1)
                OIR_Portfolio_Str = cellfun(@(s) (s(end-2:end)), Map_ProdTbl_Data(:,2),'UniformOutput',0) ; % We Need Only OIR Portfolio
                OIR_Index = strcmpi(OIR_Portfolio_Str,'OIR');
                Map_ProdTbl_Data = Map_ProdTbl_Data(OIR_Index,:);
                if(size(Map_ProdTbl_Data,1)>1)
                    Map_ProdTbl_Data = Map_ProdTbl_Data(1,:);
                end
            elseif(strcmpi(Map_ProdTbl_Data,'No Data'))
                CombinationErrorMsg(i,1) = cellstr(['Product Code could not be found for the combination with RMS and ',Current_Product,' in ',Table_Name]);
                CombinationError_Index = 1;
            else
                CombinationErrorMsg(i,1) = {''};
            end
        end
        
        Modify_Map_Data(i,2:end) = Map_ProdTbl_Data ;
    end
    
    Map_ProductData_Header = {'PortfolioType','Product','Portfolio','Instrument','Subportfolio','Strike Factor','Premium'};
    Modify_Map_Data(:,2) = strtrim(Modify_Map_Data(:,2)); %% Remove Spaces
    Map_ProductData = Modify_Map_Data;
    
    Portfolio_Data = Map_ProductData(:,3);
    Instrument_Data = Map_ProductData(:,4);
    Strike_Factor = Map_ProductData(:,6);
    Premium_Factor = Map_ProductData(:,7);
    
    %     CombinationError_Header = [{'ErrorMsg'},RawData_Header];
    %     CombinationErrorMsg = [CombinationErrorMsg,RawData];
    
    %% Get Buy Sell From Lots Field
    
    Lots_Col = find(cellfun(@(V) strcmpi('Lots',V), RawData_Header));  %% Actice Lots ans Initial Lots
    Lots_Data = RawData(:,Lots_Col);
    Index_Sell = find(cellNumber_2_Number(cellfun(@(x) x<0, Lots_Data, 'UniformOutput', false)));
    Index_Buy = find(cellNumber_2_Number(cellfun(@(x) x>0, Lots_Data, 'UniformOutput', false)));
    
    BuySell_Data = cell(length(Lots_Data),1);
    BuySell_Data(Index_Sell) = {'Sold'};
    BuySell_Data(Index_Buy) = {'Bought'};
    
    %% Get Month & Fixing Date
    USDBRL_Index = strcmpi(RawData(:,Product_Col),'USDBRL');
    
    Month_Col = find(cellfun(@(V) strcmpi('Month',V), RawData_Header));
    Month_Data = RawData(:,Month_Col);
    
    FixingDate_Data = cell(size(RawData,1),1);
    [~,MonthStr] = strtok(Month_Data(USDBRL_Index),' ');
    MonthStr = cellstr(datestr(strtrim(MonthStr),DB_Date_Format));
    FixingDate_Data(USDBRL_Index) = MonthStr;
    
    Month_Data(USDBRL_Index) = {''};
    
    %% Get Strike
    
    Strike_Col = find(cellfun(@(V) strcmpi('Strike',V), RawData_Header));
    Strike_Data = RawData(:,Strike_Col);
    
    Strike_Data(USDBRL_Index) = {''};
    
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Strike_Data);
    Strike_Data(Nan_Row(:),:) = num2cell(0);
    Strike_Data = num2cell (cellNumber_2_Number(Strike_Data) ./ cellNumber_2_Number(Strike_Factor));
    
    %% Premium
    Premium_Col = find(cellfun(@(V) strcmpi('Prem/Rate',V), RawData_Header));
    Premium_Data = RawData(:,Premium_Col);
    
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Premium_Data); %% Nan_Row = find(Nan_Row);
    Premium_Data(Nan_Row(:),:) = num2cell(0);
    
    Char_Row = cellfun(@(V) any(ischar(V(:))), Premium_Data);
    if(~isempty(find(Char_Row)))
        Premium_Data(Char_Row) = num2cell(str2num(cell2mat(Premium_Data(Char_Row))));
    end
    
    Premium_Data = num2cell (cellNumber_2_Number(Premium_Data) ./ cellNumber_2_Number(Premium_Factor));
    
    %% Get Counterparty and CONTRACTNB Data
    CounterParty_Col = find(cellfun(@(V) strcmpi('Client Code',V), RawData_Header)) ;
    CounterParty_Data = RawData(:,CounterParty_Col);
    
    CONTRACTNB_Col = find(cellfun(@(V) strcmpi('TID',V), RawData_Header)) ;
    CONTRACTNB_Data = RawData(:,CONTRACTNB_Col);
    
    %% Get Expiry(Maturity) Data
    Maturity_Col = find(cellfun(@(V) strcmpi('Expiry',V), RawData_Header)) ;
    Maturity_Data = RawData(:,Maturity_Col);
    
    [~,MonthStr] = strtok( RawData(USDBRL_Index,Strike_Col),' ');
    MonthStr = cellstr(datestr(strtrim(MonthStr),DB_Date_Format));
    Maturity_Data(USDBRL_Index) = MonthStr;
    
    %% Barrier
    Barrier_Col = find(cellfun(@(V) strcmpi('Barrier',V), RawData_Header)) ;
    Barrier_Data = RawData(:,Barrier_Col);
    
    Nan_Row =  cellfun(@(V) any(isnan(V(:))), Barrier_Data);
    Barrier_Data(Nan_Row(:),:) = num2cell(0);
    Char_Row = cellfun(@(V) any(ischar(V(:))), Barrier_Data);
    Barrier_Data(Char_Row(:),:) = num2cell(0);
    Barrier_Data = num2cell (cellNumber_2_Number(Barrier_Data) ./ cellNumber_2_Number(Strike_Factor));
    
    %% Get Exe-Broker , Trader Name , Traded DateTimestamp , Daily/EOD and Other Remarks
    ExeBroker_Col = find(cellfun(@(V) strcmpi('Exe-Broker',V), RawData_Header)) ;
    ExeBroker_Data = RawData(:,ExeBroker_Col);
    
    Other_Col = find(cellfun(@(V) strcmpi('Other Remarks',V), RawData_Header)) ;
    OtherRemarks_Data = RawData(:,Other_Col);
    
    OTC_EOD_Col = find(cellfun(@(V) strcmpi('Daily/EOD',V), RawData_Header)) ;
    OTC_EOD_Data = RawData(:,OTC_EOD_Col);
    
    TraderName_Col = find(cellfun(@(V) strcmpi('Trader Name',V), RawData_Header)) ;
    TraderName_Data = RawData(:,TraderName_Col);
    
    DateTime_Col = find(cellfun(@(V) strcmpi('Traded DateTimestamp',V), RawData_Header)) ;
    DateTime_Data = RawData(:,DateTime_Col);
    
    %% Transaction Date
    
    TransactionDate_Col = find(cellfun(@(V) strcmpi('Date',V), RawData_Header)) ;
    TransactionDate_Data = RawData(:,TransactionDate_Col);
    
    % SqlQuery = 'select settlement_date from valuation_date_table';
    % [~,Settlement_Date] = read_from_database('valuation_date_table',0,SqlQuery);
    %
    % TransactionDate_Data  = cell(length(Lots_Data),1);
    % TransactionDate_Data(:,:) = Settlement_Date;
    
    %% Source
    Source_Data  = cell(length(Lots_Data),1);
    Source_Data(:,:) = {'Booking Order'};
    
    ExeType_Data  = cell(length(Lots_Data),1);
    ExeType_Data(:,:) = {'Internal'};
    
    Rates_Data  = cell(length(Lots_Data),1);
    Rates_Data(:,:) = {'Normal'};
    
    %% Opt Type
    OptType_Col = find(cellfun(@(V) strcmpi('Put/Call/Fut',V), RawData_Header)) ;
    OptType_Data = RawData(:,OptType_Col);
    
    %% SIPH Handling in OptType & Prem
    
    ClientCode_Col = find(cellfun(@(V) strcmpi('Client Code',V), RawData_Header)) ; 
    SIPH_Index = strcmpi(RawData(:,ClientCode_Col),'SIP');
    
    OptType_Data(SIPH_Index) = {'avg_swap_cc'};
    Premium_Data(SIPH_Index) = num2cell(cell2mat(Premium_Data(SIPH_Index))./10);
    
    
    %% Make Temporary RowData
    Temp_Data = cell(length(Lots_Data),1);
    Temp_Data_NULL = cell(length(Lots_Data),1);
    Temp_Data(:,:) = num2cell(NaN);
    Temp_Data_NULL(:,:) = {'NULL'};
    
    RowData = [Instrument_Data,Portfolio_Data,BuySell_Data,Month_Data,Strike_Data,OptType_Data,Premium_Data,...
        CounterParty_Data,Lots_Data,CONTRACTNB_Data,Maturity_Data,Temp_Data,Barrier_Data,TransactionDate_Data,Temp_Data,Source_Data,Temp_Data,Temp_Data,...
        Temp_Data,CounterParty_Data,ExeType_Data,Rates_Data,Temp_Data,Temp_Data,OTC_EOD_Data,Temp_Data,Temp_Data,Temp_Data,FixingDate_Data];
    
    
    %% Use Derivative Mapping & Counterparty Mappping  Table
    
    OptType_Col = cellStrfind_exact(TRHeader,{'OptType'});
    Counterparty_Col = cellStrfind_exact(TRHeader,{'CounterParty'});
    ExeBroker_Col = cellStrfind_exact(TRHeader,{'Exe-Broker'});
    Maturity_Col = cellStrfind_exact(TRHeader,{'Maturity'});
    Fixing_Date_Col = cellStrfind_exact(TRHeader,{'Fixing_Date'});
    
    Error_Msg_Str = cell(size(RowData,1),2);
    for c = 1 : size(RowData,1)
        Current_OptType = RowData(c,OptType_Col);
        Current_ClientCode = RowData(c,Counterparty_Col);
        
        Matched_OptType = DerivativeType_Mapping_Table_Data.derivative_type(strcmpi(DerivativeType_Mapping_Table_Data.booking_order_type,Current_OptType));
        if(~isempty(Matched_OptType))
            RowData(c,OptType_Col) = Matched_OptType;
        else
            Error_Msg_Str(c,1) = {'Not getting derivative type from DB'};
        end
        
        Matched_ClientCode = Counterparty_Mapping_Table_Data.counterparty_parent(strcmpi(Counterparty_Mapping_Table_Data.client_code,Current_ClientCode)) ;
        if(~isempty(Matched_ClientCode))
            RowData(c,Counterparty_Col) = Matched_ClientCode;
            RowData(c,ExeBroker_Col) = Matched_ClientCode;
        else
            Error_Msg_Str(c,2) = {'Not Getting Client name from DB'};
        end
    end
    
    Overall_ErrorMsg_Header = [{'ProductCode Mapping Error','DerivativeType Error','ClientCode Error'},TRHeader];
    Overall_ErrorMsg_Data = [CombinationErrorMsg,Error_Msg_Str,RowData];
    
    %% Checking OptType and OTC_EOD & Making TR Dump
    
    Total_Data = [];
    Error_Data = [];  %% StartDate <= ExpiryDate
    
    OptType_Default = {'Future','Put','Call','Swap'};   %% {'Future','Put','Call'}; %% For make some calculation other  than three
    
    [System_Date_Format,System_Date] = get_System_Date_Format();
    
    HolDates = {'2016-01-01','2017-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    Maturity_Col = cellStrfind_exact(TRHeader,{'Maturity'});
    
    if(~isempty(OTC_EOD_Data))
        for i = 1 : size(RowData,1)
            
            Current_OptType = RowData(i,OptType_Col);
            Matched_Index = cellStrfind_exact(OptType_Default,Current_OptType); %% For make some calculation other  than three
            if(Matched_Index)
                Total_Data = [Total_Data ; RowData(i,:)];
            else
                
                if(strcmpi(OTC_EOD_Data{i},'Daily'))
                    StartDate = TransactionDate_Data{i};
                    ExpiryDate = Maturity_Data{i};
                    
                    if( datenum(StartDate,System_Date_Format) > datenum(ExpiryDate,System_Date_Format) )  %% datenum(StartDate,'dd-mm-yyyy') > datenum(ExpiryDate,'dd-mm-yyyy')
                        Error_Data = [Error_Data ; RowData(i,:)];      %% Error Sheet (StartDate <= ExpiryDate)
                        continue;
                    end
                    
                    
                    try
                        VBusDays = busdays(datenum(StartDate,System_Date_Format), datenum(ExpiryDate,System_Date_Format),'daily',HolidayVec); %% Where Start and Expiry are the input column names
                    catch
                        VBusDays = busdays(datenum(StartDate,System_Date_Format), datenum(ExpiryDate,System_Date_Format),'daily',HolidayVec);
                    end
                    
                    Maturity = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
                    
                    Temp = cell(length(Maturity),size(RowData,2));
                    %             Temp(:,:) =  RowData(i,:);
                    
                    for ii = 1 : length(Maturity)
                        
                        Temp(ii,:) = RowData(i,:);
                    end
                    
                    Temp(:,Maturity_Col) = Maturity;          %% Change Maturity Date to Business Date
                    Total_Data = [Total_Data ; Temp];
                    
                else
                    
                    Total_Data = [Total_Data ; RowData(i,:)];              %% EOD Line
                    
                end
            end
        end
        
        %% Except Future, Instrument Name Should be Add '-O'
        PosOptType = strcmpi('OptType',TRHeader);
        PosInstrument = strcmpi('Instrument',TRHeader);
        OptType_Data = Total_Data(:,PosOptType);
        
        Except_FUT_OptType = strcmpi('future',OptType_Data) | strcmpi('fx_forward_ndf',OptType_Data) | strcmpi('avg_swap_cc',OptType_Data);
        Total_Data(~Except_FUT_OptType,PosInstrument) = strcat(Total_Data(~Except_FUT_OptType,PosInstrument),'-O');
             
        %% Excel Write Delete Empty Excel Sheet
        
        if(~isempty(Overall_ErrorMsg_Data))
            ProdCode_Error_Col = cellStrfind_exact(Overall_ErrorMsg_Header,{'ProductCode Mapping Error'});
            DerivativeType_Error_Col = cellStrfind_exact(Overall_ErrorMsg_Header,{'DerivativeType Error'});
            ClientCode_Error_Col = cellStrfind_exact(Overall_ErrorMsg_Header,{'ClientCode Error'});
            
            Error_Line_Index = find(~cellfun(@isempty,Overall_ErrorMsg_Data(:,ProdCode_Error_Col)) | ~cellfun(@isempty,Overall_ErrorMsg_Data(:,DerivativeType_Error_Col)) | ~cellfun(@isempty,Overall_ErrorMsg_Data(:,ClientCode_Error_Col)));
            Overall_ErrorMsg_Data = Overall_ErrorMsg_Data(Error_Line_Index,:);
            
            xlswrite(BO_TR_Dump , [Overall_ErrorMsg_Header;Overall_ErrorMsg_Data] , 'ErrorSheet_RawData');
            
        end
        
        xlswrite(BO_TR_Dump , [TRHeader ; Total_Data],'OTC TR Format');
        
        if(~isempty(Error_Data))
            xlswrite(BO_TR_Dump , [TRHeader ; Error_Data],'OTC TR Format Error Sheet');
        end
        
        SheetName = 'OTC TR Format';
        try
%             xls_delete_sheets([pwd filesep Booking_Order_TR_Dump],'Sheet1');
            xls_delete_sheets(fullfile(pwd,BO_TR_Dump)); 
            xls_change_activesheet([pwd filesep Booking_Order_TR_Dump],SheetName);
        catch
            
        end
        
        
    else
        
        %         msgbox('Daily EOD Column is Empty','Finish');
        OutErrorMsg = {'Daily EOD Column is Empty'};
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end

