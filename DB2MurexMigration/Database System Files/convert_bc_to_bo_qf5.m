function [OutErrorMsg,OutXLSName] = convert_bc_to_bo_qf5(InBUName)

OutErrorMsg = {'No Errors'};

OutXLSName = getXLSFilename('BC_to_BO_Conversion');

ObjDB = connect_to_database ;

%%
try
    Settle_Date = fetch(ObjDB ,'select settlement_date from valuation_date_table');
    
    Default_Table = 'deal_ticket_table_with_latest_versions_active_lots_view_' ;
    Table_Name = strcat(Default_Table,char(InBUName)) ;
    
    SqlQuery = ['select * from ',Table_Name ,' where transaction_date = ''',char(Settle_Date),'''and  equity_swaps_trade_type like ''%Buy to Cover'' and active_lots <> 0  ' ];
    
    [DBFieldNames , DBData] = Fetch_DB_Data(ObjDB , SqlQuery);
    
    if(size(DBData,2) > 1)
        
        Equity_Swaps_Col = cellStrfind_exact(DBFieldNames,{'equity_swaps_trade_type'});
        
        DBData(:,Equity_Swaps_Col) = {'Buy to Open'} ;
        
        ActiveLots_Col = cellStrfind_exact(DBFieldNames,{'active_lots'});
        OriginalLots_Col = cellStrfind_exact(DBFieldNames,{'original_lots'});
        LotstoNetted_Col = cellStrfind_exact(DBFieldNames,{'lots_to_be_netted'});
        DBData(:,OriginalLots_Col) = DBData(:,ActiveLots_Col);
        DBData(:,LotstoNetted_Col) = DBData(:,ActiveLots_Col);
        
        
        %% Excel Write
        
        OverallData = cell2dataset([DBFieldNames ; DBData]);
        
        OverallData.active_lots = [];  %% No Need Active_Lots Col
        OverallData.is_suspended = [];
        OverallData.netting_status = [];
        OverallData.portfolio = [];
        
        xlswrite(OutXLSName ,dataset2cell(OverallData)) ;
        
    else
        OutErrorMsg = {'No Data Found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end



