function [OutErrorMsg,OutXLSName] = generate_futures_report_qf5(InBUName)
%  Generate the futures in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/14 07:53:07 $
%  $Revision: 1.7 $
%
%

OutErrorMsg = {'No errors'};
OutXLSName = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    %% Find Year Strat Date
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    Current_Year = str2double(datestr(char(Settle_Date),10));
    FTY_StartDate = datestr(lbusdate(Current_Year-1,12),'yyyy-mm-dd');
    
    %     FTY_StartDate = char(fetch(ObjDB,'select start_date from valuation_financial_year_view'));
    
    %% Fetch View Data
    Default_Name = 'helper_evaluation_reports_view_';
    ViewName = [Default_Name,InBUName];
    
    %     SqlQuery = ['select * from ',char(ViewName),' where transaction_date >= ''',FTY_StartDate,''' and version_comments in (''actual_deal_resetted'',''fully_netted'',''offset_deal_resetted'') ']; %% and deal_status = ''dead''
    SqlQuery = ['select * from ',char(ViewName),' where transaction_date >= ''',FTY_StartDate,''' ']; %% and deal_status = ''dead''
    
    [SettleColNames,SettleData] = Fetch_DB_Data(ObjDB,SqlQuery);
    %     [SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);
    
    %% Initialize Output Col
    
    Counterparty_Col = 1 ; %% counterparty_parent
    Subportfolio_Col = 2 ;
    StockNo_Col = 3; %% product_code
    StockName_Col =4; %% product_name
    PurchaseQuantity_Col = 5; %% active_lots
    PurchasePrice_Col = 6; %%original_premium
    SalesQuantity_Col = 7; %% active_lots
    SalesPrice_Col = 8; %% original_premium
    
    RealisedPnL_USD_Col = 9;
    UnRealised_USD_Col = 10 ;
    Market_Rate_Col = 11;
    Market_SettleDate_Col = 12;
    
    %%
    
    if(size(SettleData,2) > 1)
        
        Consolidate_Counterparty_Col = cellStrfind_exact(SettleColNames,{'counterparty_parent'});
        Consolidate_Subportfolio_Col  = cellStrfind_exact(SettleColNames,{'subportfolio'});
        
        Consolidate_ProdCode_Col = cellStrfind_exact(SettleColNames,{'product_code'});
        Consolidate_ProdName_Col = cellStrfind_exact(SettleColNames,{'product_name'});
        
        Consolidate_MarketAction_Col = cellStrfind_exact(SettleColNames,{'market_action'});
        Consolidate_ActiveLots_Col = cellStrfind_exact(SettleColNames,{'active_lots'});
        Consolidate_OriginalPremium_Col = cellStrfind_exact(SettleColNames,{'original_premium'});
        Consolidate_RealizedM2M_USD_Col = cellStrfind_exact(SettleColNames,{'realised_mtm_usd'});
        Consolidate_UnRealizedM2M_USD_Col = cellStrfind_exact(SettleColNames,{'unrealised_mtm_usd'});
        Consolidate_Mkt_Rate_Col = cellStrfind_exact(SettleColNames,{'p1_settleprice'});
        
        OutputData = cell(size(SettleData,1),12);
        for i = 1 : size(SettleData,1)
            
            OutputData(i,Counterparty_Col) = SettleData(i ,Consolidate_Counterparty_Col );
            OutputData(i,Subportfolio_Col) = SettleData(i , Consolidate_Subportfolio_Col);
            
            OutputData(i,StockNo_Col) = SettleData(i , Consolidate_ProdCode_Col);
            OutputData(i,StockName_Col) = SettleData(i ,Consolidate_ProdName_Col );
            
            if(strcmpi(SettleData(i , Consolidate_MarketAction_Col),'bought'))
                OutputData(i,PurchaseQuantity_Col) = SettleData(i ,Consolidate_ActiveLots_Col);
                OutputData(i,PurchasePrice_Col) = SettleData(i , Consolidate_OriginalPremium_Col);
            else
                OutputData(i,SalesQuantity_Col) = SettleData(i , Consolidate_ActiveLots_Col);
                OutputData(i,SalesPrice_Col) = SettleData(i , Consolidate_OriginalPremium_Col);
            end
            
            OutputData(i,RealisedPnL_USD_Col) = SettleData(i , Consolidate_RealizedM2M_USD_Col) ;
            OutputData(i,UnRealised_USD_Col) = SettleData(i , Consolidate_UnRealizedM2M_USD_Col) ;
            
            OutputData(i,Market_Rate_Col) = SettleData(i , Consolidate_Mkt_Rate_Col);
            OutputData(i,Market_SettleDate_Col) = Settle_Date;
            
            
        end
        
        %% Consolidate Data
        
        Output_Header = {'Counterparty','Subportfolio','ProductCode','ProductName','Open_Bought','Purchase_Price','Open_Sold','Sales_Price','Realised_PnL','UnRealised_PnL','Mkt_Rate','Market_SettleDate'};
        
        UniqueFields = {'Counterparty','Subportfolio','ProductCode','ProductName'};
        SumFields = {'Open_Bought','Open_Sold','Realised_PnL','UnRealised_PnL'};
        OutputFields = [[UniqueFields,SumFields],{'Mkt_Rate','Market_SettleDate'}];
        WeightedAverageFields = [];
        
        [NetHeader,NetData] = consolidatedata(Output_Header, OutputData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        %% Net Open Calculation
        
        OpenBought_Col = cellStrfind_exact(NetHeader,{'Open_Bought'});
        Open_Sold_Col = cellStrfind_exact(NetHeader,{'Open_Sold'});
        
        NetData(:,Open_Sold_Col) = num2cell( abs(cell2mat(NetData(:,Open_Sold_Col))) ) ;
        NetOpen = num2cell(  cell2mat(NetData(:,OpenBought_Col)) -  cell2mat(NetData(:,Open_Sold_Col)) );
        
        NetHeader = [NetHeader,{'Net Open'}];
        NetData = [NetData ,NetOpen] ;
        
        %% Excel Write
        
        OutXLSName = getXLSFilename('Futures_Report_QF5');
        
        Header_Order = {'Counterparty','Subportfolio','ProductCode','ProductName','Open_Bought','Open_Sold','Net Open',...
            'Realised_PnL','UnRealised_PnL','Mkt_Rate','Market_SettleDate'};
        Header_Order_Index = cellStrfind_exact(NetHeader,Header_Order);
        NetData_Order = NetData(:,Header_Order_Index);
        
        %       xlswrite(OutXLSName,[Output_Header ;OutputData ] );
        xlswrite(OutXLSName,[Header_Order ;NetData_Order ]);
        
    else
        OutErrorMsg = {['No Data ',ViewName]};
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end