function [OutErrorMsg] = Refresh_Broker_Open_Position_table(InBUName,DBValueDate)

%%% Project Name: Broker Module
%%% Module Name: Broker Open Position
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 08 Feb 2019 / 16:47:00
%
%%% [OutErrorMsg] = Refresh_Broker_Open_Position_table('qf1','2019-11-20')
try   
    OutErrorMsg = {'No Errors'};
    ObjDB = connect_to_database;

    DBValueDate = datestr(DBValueDate,'yyyy-mm-dd');

    %%% Delete data from Database (UAT)
    Open_TableName = strcat('broker_open_position_table_',char(InBUName));

    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',char(Open_TableName),' where st_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        %disp(curs.Message);
        OutErrorMsg = {curs.Message};
    end
    set(ObjDB,'AutoCommit','on');

    %%% Connect to Opera DB Channel
    ObjDB_Opera_Channel = connect_to_dbchannel;

    %%%% Fetch data fron OperaDBChannel
    DBVarchar_Date = datestr(DBValueDate,'dd-mmm-yy');

    TableName='broker_open_position_table';
    SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

    [Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

    if(isempty(Broker_Table_Data))
        OutErrorMsg = {['Data is not available in open position table for ',char(DBValueDate)]};
        return;
    end

    if(strcmpi(Broker_Table_Data,'No Data'))
        OutErrorMsg = {['Data is not available in open position table for ',char(DBValueDate)]};
        return;
    end

    %%% Newly added code on (15-11-2019)
    %%% display master product_display name insted of broker product name
    Broker_OpenPos_Data = cell2table(Broker_Table_Data,'VariableNames',Broker_Tab_ColNames);  %% Convert to table
    
    %%% Fetch data from broker product master table
    BProduct_master_Table = ['broker_product_master_table_',char(InBUName)];
    SqlQuery_BProduct_master_Table = ['select * from ',char(BProduct_master_Table)];
    [BProduct_master_Table_Cols,BProduct_master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BProduct_master_Table);

    if strcmpi(BProduct_master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BProduct_master_Table)]} ;
        return;
    end
    Broker_Product_master_Tbl = cell2table(BProduct_master_Table_Data,'VariableNames',BProduct_master_Table_Cols);  %% Convert to table
    
    %%% Broker and Master table mapping, get the product_display
    [row,~] = size(Broker_OpenPos_Data);
    for i = 1:1:row
        single_account = Broker_OpenPos_Data.account(i);
        single_broker = Broker_OpenPos_Data.broker_name(i);
        single_product = Broker_OpenPos_Data.product(i);
        
        Index_opera = strcmpi(single_account,Broker_Product_master_Tbl.account_number) & strcmpi(single_broker,Broker_Product_master_Tbl.broker_name) & strcmpi(single_product,Broker_Product_master_Tbl.broker_product_name);
        if any(Index_opera)
            Opera_product_display(i,1) =Broker_Product_master_Tbl.product_display(Index_opera);
        else
            Opera_product_display(i,1) = {''};
        end
    end
    Broker_OpenPos_Data.product = Opera_product_display;
    Broker_Table_Data = table2cell(Broker_OpenPos_Data);
    %%% end
    
    %%% date format changeing
    Pos_ST_Date = strcmpi(Broker_Tab_ColNames,'st_date');
    Broker_Table_Data(:,Pos_ST_Date) = cellstr(datestr(Broker_Table_Data(:,Pos_ST_Date),'yyyy-mm-dd'));
    
    %%% find the net_open_lots using bought_lots and sold_lots
    Pos_bought_lots = strcmpi(Broker_Tab_ColNames,'bought_lots');
    Pos_sold_lots = strcmpi(Broker_Tab_ColNames,'sold_lots');
    
    bought_lots = Broker_Table_Data(:,Pos_bought_lots);
    sold_lots = Broker_Table_Data(:,Pos_sold_lots);
    
    net_open_lots = num2cell(cell2mat(bought_lots) - cell2mat(sold_lots));
    
    Pos_net_open_lots = size(Broker_Tab_ColNames,2) + 1;
    Broker_Table_Data(:,Pos_net_open_lots) = net_open_lots;
    
    %%Featch data from Year Mapping Table
    Mapping_Tbl_Name = 'year_mapping_table';
    SqlQuery_Mapping_Table = ['select * from ',char(Mapping_Tbl_Name)] ;
    [Mapping_ColNames,Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Mapping_Table);
    Mapping_Tab_Data = cell2dataset([Mapping_ColNames;Mapping_Table_Data]);                 %%%% end code

    %%% Index for terminal_month
    terminal_month_Index = strcmpi(Broker_Tab_ColNames,'terminal_month');
    Month_data =Broker_Table_Data(:,terminal_month_Index);

    %%% Mapping process (terminal month and year mapping table)
    Mapping_Tab_digit=Mapping_Tab_Data.single_digit_year;
    Final_terminal_month = [];
    for i = 1:1:size(Month_data,1)
        Single_Month_data = char(Month_data(i));
        Pre_Last_ltr = Single_Month_data(end);               
        for j=1:1:length(Mapping_Tab_digit)
            Mapping_Tab_digit_val=Mapping_Tab_digit(j);
            if isequal(str2double(Pre_Last_ltr),Mapping_Tab_digit_val)
                Pre_Year_ltr = Mapping_Tab_Data.double_digit_year(j);
                Current_Month_data=strrep(Single_Month_data,Pre_Last_ltr,num2str(Pre_Year_ltr));
                break;
            end 
        end
        
        if(exist('Current_Month_data','var'))
            Final_terminal_month = vertcat(Final_terminal_month , {Current_Month_data});
            clear Current_Month_data;
        else
            Final_terminal_month = vertcat(Final_terminal_month , {Single_Month_data});
        end    
    end
    %%% After month mapping
    Broker_Table_Data(:,terminal_month_Index) = Final_terminal_month;
    
    Use_Broker_Table_Data = [Broker_Table_Data(:,1), Broker_Table_Data(:,2), Broker_Table_Data(:,3), Broker_Table_Data(:,4), Broker_Table_Data(:,5), ...];
                        Broker_Table_Data(:,6), Broker_Table_Data(:,7), Broker_Table_Data(:,8), Broker_Table_Data(:,9), Broker_Table_Data(:,10), ...
                        Broker_Table_Data(:,11), Broker_Table_Data(:,12), Broker_Table_Data(:,14), Broker_Table_Data(:,13)];
    
    %%% New Data
    Upload_Open_Data = Use_Broker_Table_Data;
    upload_in_database(Open_TableName,Upload_Open_Data,ObjDB);

    disp(['Refresh done successfully for ',Open_TableName,' for COB:',DBVarchar_Date]);

catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end
end
