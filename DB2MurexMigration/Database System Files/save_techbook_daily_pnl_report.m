function save_techbook_daily_pnl_report

SqlQuery =  'SELECT financial_year FROM valuation_financial_year_view';

ObjDB = connect_to_database;
FinYear = fetch(ObjDB,SqlQuery);

StartYear = strtok(strrep(FinYear,'FY-',''),'-');

SqlQuery = ['select * from tech_report_table where value_date like ''',char(StartYear),'%'''];
[ColNames,Data] = read_from_database('tech_report_table',0,SqlQuery);

SettlementDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));

UniqueFields = {'value_date'};
SumFields = {'delta','gamma','theta','vega','acc_value','pnl_for_the_day'};
OutputFields = [UniqueFields,SumFields];
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

NumRows = size(OutData,1);
Portfolio = repmat({'TECH BOOK'},NumRows,1);

OutputFields = ['Portfolio',OutputFields];
OutData      = [Portfolio,OutData];

Filename = 'TECH_Book_Daily_Pnl_Report.xlsx';

try
    if (exist(Filename,'file') == 2)
        delete(Filename);
    end
catch
end

xlswrite(Filename,[OutputFields;OutData]);

try
    configure_mail_settings;
    sendmail({'anthony.marques@olamnet.com','Raghavendra.Sn@olamnet.com'}, 'TECH Book Daily Pnl Report', ['Attached is the TECH Book Daily Pnl Report for COB ''',SettlementDate,''''],{Filename});
catch
end

try
    DestinationFolder = '\\10.190.7.222\Share\DBSystem_Reports\TECHBook_DailyPnl_Report';
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'TECH Book Daily Pnl Report');
catch
end



