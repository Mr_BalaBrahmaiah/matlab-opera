function [OutErrorMsg] = Refresh_Broker_Confirmed_Deal_table(InBUName,DBValueDate)

%%% Project Name: Broker Module
%%% Module Name: Broker Confirmed Deal
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 05 Mar 2019 / 17:00:00
%
%%% [OutErrorMsg] = Refresh_Broker_Confirmed_Deal_table('qf1','2019-11-18')

try
    OutErrorMsg = {'No Errors'};
    ObjDB = connect_to_database;

    DBValueDate = datestr(DBValueDate,'yyyy-mm-dd');

    %%% Delete data from Database (UAT)
    Confirmed_Deal_TableName = strcat('broker_confirmed_deal_table_',char(InBUName));

    set(ObjDB,'AutoCommit','off');
    SqlQuery = ['delete from ',char(Confirmed_Deal_TableName),' where st_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        %disp(curs.Message);
        OutErrorMsg = {curs.Message};
    end
    set(ObjDB,'AutoCommit','on');

    %%% Connect to Opera DB Channel
    ObjDB_Opera_Channel = connect_to_dbchannel;

    %%%% Fetch data fron OperaDB
    DBVarchar_Date = datestr(DBValueDate,'dd-mmm-yy');

    TableName='broker_confirmed_deal_table';
    SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

    [Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

    if(isempty(Broker_Table_Data))
        %OutErrorMsg = {['Data is not available in recon table for ',char(DBValueDate)]};
         OutErrorMsg = {['For ',char(DBValueDate),' Data is not available in ',char(Confirmed_Deal_TableName)]};
        return;
    end

    if(strcmpi(Broker_Table_Data,'No Data'))
        OutErrorMsg = {['For ',char(DBValueDate),' Data is not available in ',char(Confirmed_Deal_TableName)]};
        return;
    end

    %%% Newly added code on (15-11-2019)
    %%% display master product_display name insted of broker product name
    Broker_Confirmed_Data = cell2table(Broker_Table_Data,'VariableNames',Broker_Tab_ColNames);  %% Convert to table
    
    %%% Fetch data from broker product master table
    BProduct_master_Table = ['broker_product_master_table_',char(InBUName)];
    SqlQuery_BProduct_master_Table = ['select * from ',char(BProduct_master_Table)];
    [BProduct_master_Table_Cols,BProduct_master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BProduct_master_Table);

    if strcmpi(BProduct_master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BProduct_master_Table)]} ;
        return;
    end
    Broker_Product_master_Tbl = cell2table(BProduct_master_Table_Data,'VariableNames',BProduct_master_Table_Cols);  %% Convert to table
    
    %%% Broker and Master table mapping, get the product_display
    [row,~] = size(Broker_Confirmed_Data);
    for i = 1:1:row
        single_account = Broker_Confirmed_Data.account(i);
        single_broker = Broker_Confirmed_Data.broker_name(i);
        single_product = Broker_Confirmed_Data.product(i);
        
        Index_opera = strcmpi(single_account,Broker_Product_master_Tbl.account_number) & strcmpi(single_broker,Broker_Product_master_Tbl.broker_name) & strcmpi(single_product,Broker_Product_master_Tbl.broker_product_name);
        if any(Index_opera)
            Opera_product_display(i,1) =Broker_Product_master_Tbl.product_display(Index_opera);
        else
            Opera_product_display(i,1) = {''};
        end
    end
    Broker_Confirmed_Data.product = Opera_product_display;
    Broker_Table_Data = table2cell(Broker_Confirmed_Data);
    %%% end
    
    Pos_ST_Date = strcmpi(Broker_Tab_ColNames,'st_date');
    Broker_Table_Data(:,Pos_ST_Date) = cellstr(datestr(Broker_Table_Data(:,Pos_ST_Date),'yyyy-mm-dd'));
    
    Pos_trade_Date = strcmpi(Broker_Tab_ColNames,'trade_date');
    Broker_Table_Data(:,Pos_trade_Date) = cellstr(datestr(Broker_Table_Data(:,Pos_trade_Date),'yyyy-mm-dd'));

    %%% get last trade ID from broker_balance_report_table_(InBUName)
    [~,DBTradeId]= getLastTradeId_Confirmed(InBUName);

    %%% generate the fist col like rec_id
    [r,~] = size(Broker_Table_Data);
    for i=1:1:r
        NewTradeId = DBTradeId;
        deal_id(i,1) = cellstr(['DEAL-',num2str(NewTradeId)]);
        DBTradeId = DBTradeId+1;
    end

    %%% New Data
    Upload_Recon_Data = [deal_id Broker_Table_Data];
    upload_in_database(Confirmed_Deal_TableName,Upload_Recon_Data,ObjDB);

    disp(['Refresh done successfully for ',Confirmed_Deal_TableName,' for COB:',DBVarchar_Date]);
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end
end
