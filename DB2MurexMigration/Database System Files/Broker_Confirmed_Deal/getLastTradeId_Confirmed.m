function [OutErrorMsg,LastTradeId]= getLastTradeId_Confirmed(InBUName)

OutErrorMsg = {'No errors'};
LastTradeId = {''};

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ObjDB = connect_to_database;
    
    Balance_TableName = strcat('broker_confirmed_deal_table_',char(InBUName));
    SqlQuery = ['select mid(deal_id,6) from ',Balance_TableName,' where deal_id like ''','DEAL-','%'''];
    DBTradeId = fetch(ObjDB,SqlQuery);

    if (isempty(DBTradeId))
        DBTradeId = 0;
    end
    
    TempTradeNum = max(str2num(char(DBTradeId)));
    
    if(isempty(TempTradeNum))
        TempTradeNum = 0;      %% Default
    end

     if ~isempty(TempTradeNum)
            LastTradeId = (TempTradeNum+1)';
     end 
     
catch ME
    
    OutErrorMsg = cellstr(ME.message);
    
end
