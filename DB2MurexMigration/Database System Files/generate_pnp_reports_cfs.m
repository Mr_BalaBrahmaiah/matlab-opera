function [OutErrorMsg,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports_cfs(InBUName)
%  generate the pnp dumps - option-dump, future-dump, m-us dumps
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/03/18 10:56:07 $
%  $Revision: 1.14 $
%

OutErrorMsg = {'No errors'};
PnpOptionDumpFileName = ''; PnpFutureDumpFileName = ''; PnpMUsDumpFileName = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
% ViewName = 'helper_5_1_traders_pricing_subportfoliovalues_view';
ViewName = 'helper_traders_pricing_subportfoliovalues_view';
[TradersColNames,Data] = read_from_database(ViewName,0,'',InBUName);
TradersData = cell2dataset([TradersColNames;Data]);
clear Data;  

% ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
ViewName = 'helper_settle_pricing_subportfoliovalues_view';
[SettleColNames,Data] = read_from_database(ViewName,0,'',InBUName);
SettleData = cell2dataset([SettleColNames;Data]);
clear Data; 

% ValuationDate = TradersData.value_date{1};

TableName = 'subportfolio_helper_data_table';
[ColNames,Data] =  read_from_database(TableName,0,'',InBUName);
DBHelperData = cell2dataset([ColNames;Data]);
RefSubportfolio = DBHelperData.subportfolio;
RefStrikeMult = DBHelperData.strike_factor;
RefPremiumMult = DBHelperData.premium_factor;

% for pnp-dumps directional portfolios are not needed, hence removing them
IdxTR = zeros(size(TradersData.trade_id));
IdxS = zeros(size(SettleData.trade_id));
IdxFYD_TR = cellStrfind(TradersData.subportfolio,'OID');  IdxTR(IdxFYD_TR) = 1;
IdxFYD_S  = cellStrfind(SettleData.subportfolio,'OID');   IdxS(IdxFYD_S) = 1;

% the portfolios that are needed are already defined in master pnp file
% use only those portfolios in pnp reports and ignore the rest
IdxInPnp_TR     = cellStrfind(TradersData.subportfolio,RefSubportfolio);
IdxInPnp_Settle = cellStrfind(SettleData.subportfolio,RefSubportfolio);
IdxCSOTR     = ones(size(TradersData.trade_id));
IdxCSOSettle = ones(size(SettleData.trade_id));
IdxCSOTR(IdxInPnp_TR) = 0;
IdxCSOSettle(IdxInPnp_Settle) = 0;

% Ignore the out_barrier types which hit the barriers, since they consider
% to be dead
OptTypeTR = lower(TradersData.derivative_type);
BarHitTR = TradersData.barrier1_hit;
IdxOutTR = zeros(size(OptTypeTR));
IdxRemoveTR = cellStrfind(OptTypeTR,{'up_out','down_out'});
IdxOutTR(IdxRemoveTR) = 1;
IdxHitTR = (BarHitTR==1);
IdxOutHitTR = IdxHitTR & IdxOutTR;

OptTypeSettle = lower(SettleData.derivative_type);
BarHitSettle = SettleData.barrier1_hit;
IdxOutSettle = zeros(size(OptTypeSettle));
IdxRemoveSettle = cellStrfind(OptTypeSettle,{'up_out','down_out'});
IdxOutSettle(IdxRemoveSettle) = 1;
IdxHitSettle = (BarHitSettle==1);
IdxOutHitSettle = IdxHitSettle & IdxOutSettle;

% Identify the netted lines of trade
IdxTRNetted = strcmpi('fully_netted',TradersData.netting_status);
IdxSNetted = strcmpi('fully_netted',SettleData.netting_status);
IdxTR = IdxTR | IdxCSOTR | IdxOutHitTR | IdxTRNetted;
IdxS = IdxS | IdxCSOSettle | IdxOutHitSettle | IdxSNetted;

% IdxTR = strncmp('DUP',TradersData.trade_id,3);
TempTradersData = dataset2cell(TradersData(~IdxTR,:));
TempTradersData = TempTradersData(2:end,:);
% IdxS = strncmp('DUP',SettleData.trade_id,3);
TempSettleData = dataset2cell(SettleData(~IdxS,:));
TempSettleData = TempSettleData(2:end,:);

clear SettleData; clear TradersData;

% for pnp-dumps, we need only live deals; hence removing all dead deals
% remove dead deals from traders dump
PosValueDate = find(strcmpi('value_date',TradersColNames));
PosMaturity  = find(strcmpi('maturity_date',TradersColNames));
ValueDate    = datenum(TempTradersData(:,PosValueDate),'yyyy-mm-dd'); %#ok<*FNDSB>
TempMaturity = TempTradersData(:,PosMaturity);
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
IdxLive = ValueDate <= MaturityDate;
% IdxDead = ValueDate > MaturityDate;
DumpTradersData = cell2dataset([TradersColNames;TempTradersData(IdxLive,:)]);

% remove dead deals from settlement dump
PosValueDate = find(strcmpi('value_date',SettleColNames));
PosMaturity  = find(strcmpi('maturity_date',SettleColNames));
ValueDate    = datenum(TempSettleData(:,PosValueDate),'yyyy-mm-dd');
TempMaturity = TempSettleData(:,PosMaturity);
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');
IdxLive = ValueDate <= MaturityDate;
% IdxDead = ValueDate > MaturityDate;
DumpSettleData = cell2dataset([SettleColNames;TempSettleData(IdxLive,:)]);

clear TempSettleData; clear TempTradersData;

% iterate over each subportfolio for future and m-us dump
Subportfolio = unique(upper(DumpTradersData.subportfolio));

[~,Tok] = strtok(DumpTradersData.contract_month,'.');
FutureContractMonth = strrep(Tok,'.','');
[~,Tok1] = strtok(DumpSettleData.contract_month,'.');
SettleFutureContractMonth = strrep(Tok1,'.','');

FutLoopCntr = 1; MVLoopCntr = 1; TrMVLoopCntr = 1;

for iS = 1:length(Subportfolio)
    Portfolio = Subportfolio(iS);
    IdxTraders = strcmpi(Subportfolio{iS},DumpTradersData.subportfolio);
    IdxSettle  = strcmpi(Subportfolio{iS},DumpSettleData.subportfolio);
    
    %% generate the future dump
    % the below contrat months cannot be used for M-Us dump, since the
    % underlying months are considerd for future dump
    ContractMonth = unique(upper(FutureContractMonth(IdxTraders)));
    ContractMonth(cellfun(@isempty,ContractMonth)) = [];
    
    IdxMult = strcmpi(Subportfolio{iS},RefSubportfolio);
    PremiumMult = RefPremiumMult(IdxMult); StrikeMult = RefStrikeMult(IdxMult);
    
    for iCM = 1:length(ContractMonth)
        IdxTradersCM = zeros(size(IdxTraders)); IdxTradersCM = logical(IdxTradersCM); 
        IdxFuture    = zeros(size(IdxTraders)); IdxFuture = logical(IdxFuture);
        IdxOption    = zeros(size(IdxTraders)); IdxOption = logical(IdxOption);
        
        IdxSettleCM        = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM); 
        IdxSettleFuture    = zeros(size(IdxSettle)); IdxSettleFuture = logical(IdxSettleFuture);
        IdxSettleOption    = zeros(size(IdxSettle)); IdxSettleOption = logical(IdxSettleOption);
        
        IdxTradersCM(IdxTraders) = strcmpi(ContractMonth{iCM},FutureContractMonth(IdxTraders));        
        IdxFuture(IdxTradersCM) = strcmpi('future',DumpTradersData.derivative_type(IdxTradersCM));
        IdxOption(IdxTradersCM) = ~IdxFuture(IdxTradersCM);
        
        IdxSettleCM(IdxSettle) = strcmpi(ContractMonth{iCM},SettleFutureContractMonth(IdxSettle));        
        IdxSettleFuture(IdxSettleCM) = strcmpi('future',DumpSettleData.derivative_type(IdxSettleCM));
        IdxSettleOption(IdxSettleCM) = ~IdxSettleFuture(IdxSettleCM);
        
        Instrument      = unique(DumpTradersData.instrument(IdxFuture));
        if isempty(Instrument)
            Temp = char(unique(DumpTradersData.instrument(IdxOption)));
            Instrument = cellstr(Temp(1:end-2));
        end
        
        if length(Instrument) > 1
            Instrument  = Instrument(1);
        end
        FutureDump(FutLoopCntr,1) = Instrument;  %#ok<*AGROW>
        FutureDump(FutLoopCntr,2) = Portfolio;
        FutureDump(FutLoopCntr,3) = ContractMonth(iCM);
        FutureDump(FutLoopCntr,4) = num2cell(sum(DumpTradersData.cumulative_bv_usd(IdxOption)));
        FutureDump(FutLoopCntr,5) = num2cell(sum(DumpTradersData.traders_delta_1(IdxFuture))); % net pos
        FutureDump(FutLoopCntr,6) = num2cell(mean(DumpTradersData.traders_value(IdxFuture)) ./ PremiumMult); % future price
        if isnan(FutureDump{FutLoopCntr,6})
            try
            FutureDump(FutLoopCntr,6) = num2cell(unique(DumpTradersData.p1_settleprice(IdxOption)) ./ PremiumMult);
            catch
                FutureDump(FutLoopCntr,6) = {NaN};
            end
        end
        FutureDump(FutLoopCntr,7) = {' '};
        FutureDump(FutLoopCntr,8) = num2cell(sum(DumpTradersData.traders_delta_1(IdxOption)));
        FutureDump(FutLoopCntr,9) = num2cell(sum(DumpTradersData.gamma_lots(IdxOption)));
        FutureDump(FutLoopCntr,10) = num2cell(sum(DumpTradersData.traders_theta(IdxOption)));
        FutureDump(FutLoopCntr,11) = num2cell(sum(DumpTradersData.traders_vega_1(IdxOption)));
        FutureDump(FutLoopCntr,12) = num2cell(sum(DumpSettleData.cumulative_mtm_usd(IdxSettleOption)));
        FutureDump(FutLoopCntr,13) = num2cell(sum(DumpSettleData.settle_delta_1(IdxSettleOption)));
        FutureDump(FutLoopCntr,14) = num2cell(sum(DumpSettleData.gamma_lots(IdxSettleOption)));
        FutureDump(FutLoopCntr,15) = num2cell(sum(DumpSettleData.settle_theta(IdxSettleOption)));
        FutureDump(FutLoopCntr,16) = num2cell(sum(DumpSettleData.settle_vega_1(IdxSettleOption)));
        FutureDump(FutLoopCntr,17) = num2cell(sum(DumpTradersData.traders_gamma_11(IdxOption)));
        FutureDump(FutLoopCntr,18) = num2cell(sum(DumpSettleData.settle_gamma_11(IdxSettleOption)));
          
        FutLoopCntr = FutLoopCntr + 1;
        
    end
    
    %% generate the M-Us dump
    MV_ContractMonth = unique(upper(DumpSettleData.contract_month(IdxSettle)));
    MV_ContractMonth(cellfun(@isempty,MV_ContractMonth)) = [];
    
    %Instrument	Portfolio	Maturity	Strike	 Market Value Call	Market Value Put
    for iMVCM = 1:length(MV_ContractMonth)
        IdxSettleCM = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM);
        IdxSettleCM(IdxSettle)   = strcmpi(MV_ContractMonth{iMVCM},DumpSettleData.contract_month(IdxSettle));
        
        LotMult  = unique(DumpSettleData.lot_mult1(IdxSettleCM));
        CurrMult = unique(DumpSettleData.curr_mult1(IdxSettleCM));
        
        Strike = unique(DumpSettleData.strike(IdxSettleCM));
        Strike(isnan(Strike)) = [];
        
        for iStrike = 1:length(Strike)
            
            IdxSettleStrike  = zeros(size(IdxSettle));  IdxSettleStrike  = logical(IdxSettleStrike);
            IdxSettleCall    = zeros(size(IdxSettle));  IdxSettleCall    = logical(IdxSettleCall);
            IdxSettlePut     = zeros(size(IdxSettle));  IdxSettlePut     = logical(IdxSettlePut);
            
            IdxSettleStrike(IdxSettleCM)   = ismember(DumpSettleData.strike(IdxSettleCM),Strike(iStrike));
            
            TempCallIdx = strfind(lower(DumpSettleData.derivative_type(IdxSettleStrike)),'call');
            TempPutIdx  = strfind(lower(DumpSettleData.derivative_type(IdxSettleStrike)),'put');
            IdxSettleCall(IdxSettleStrike)   = ~(cellfun(@isempty,TempCallIdx));
            IdxSettlePut(IdxSettleStrike)    = ~(cellfun(@isempty,TempPutIdx));
            
            Market_value_call = sum(DumpSettleData.settle_price(IdxSettleCall) .* DumpSettleData.active_lots(IdxSettleCall) .* LotMult .* CurrMult);
            Market_value_put  = sum(DumpSettleData.settle_price(IdxSettlePut) .* DumpSettleData.active_lots(IdxSettlePut) .* LotMult .* CurrMult);
            Settle_Vega_call = sum(DumpSettleData.settle_vega_1(IdxSettleCall));
            Settle_Vega_put = sum(DumpSettleData.settle_vega_1(IdxSettlePut));
                        
            Instrument = unique(DumpSettleData.instrument(IdxSettleCall));
            if isempty(Instrument)
                Instrument = unique(DumpSettleData.instrument(IdxSettlePut));
            end
            if length(Instrument) > 1
                Instrument  = Instrument(1);
            end
            M_Us_Dump(MVLoopCntr,1) = Instrument;
            M_Us_Dump(MVLoopCntr,2) = Portfolio;
            M_Us_Dump(MVLoopCntr,3) = MV_ContractMonth(iMVCM);
            M_Us_Dump(MVLoopCntr,4) = num2cell(Strike(iStrike) / StrikeMult);
            M_Us_Dump(MVLoopCntr,5) = num2cell(Market_value_call);
            M_Us_Dump(MVLoopCntr,6) = num2cell(Market_value_put);
            M_Us_Dump(MVLoopCntr,7) = num2cell(Settle_Vega_call);
            M_Us_Dump(MVLoopCntr,8) = num2cell(Settle_Vega_put);
            
            MVLoopCntr = MVLoopCntr + 1;
        end
    end
    
    %% generate the Traders M-Us dump
    MV_ContractMonth = unique(upper(DumpTradersData.contract_month(IdxTraders)));
    MV_ContractMonth(cellfun(@isempty,MV_ContractMonth)) = [];
    
    %Instrument	Portfolio	Maturity	Strike	 Market Value Call	Market Value Put
    for iMVCM = 1:length(MV_ContractMonth)
        IdxTradersCM = zeros(size(IdxTraders)); IdxTradersCM = logical(IdxTradersCM);
        IdxTradersCM(IdxTraders)   = strcmpi(MV_ContractMonth{iMVCM},DumpTradersData.contract_month(IdxTraders));
        
        LotMult  = unique(DumpTradersData.lot_mult1(IdxTradersCM));
        CurrMult = unique(DumpTradersData.curr_mult1(IdxTradersCM));
        
        Strike = unique(DumpTradersData.strike(IdxTradersCM));
        Strike(isnan(Strike)) = [];
        
        for iStrike = 1:length(Strike)
            
            IdxTradersStrike  = zeros(size(IdxTraders));  IdxTradersStrike  = logical(IdxTradersStrike);
            IdxTradersCall    = zeros(size(IdxTraders));  IdxTradersCall    = logical(IdxTradersCall);
            IdxTradersPut     = zeros(size(IdxTraders));  IdxTradersPut     = logical(IdxTradersPut);
            
            IdxTradersStrike(IdxTradersCM)   = ismember(DumpTradersData.strike(IdxTradersCM),Strike(iStrike));
            
            TempCallIdx = strfind(lower(DumpTradersData.derivative_type(IdxTradersStrike)),'call');
            TempPutIdx  = strfind(lower(DumpTradersData.derivative_type(IdxTradersStrike)),'put');
            IdxTradersCall(IdxTradersStrike)   = ~(cellfun(@isempty,TempCallIdx));
            IdxTradersPut(IdxTradersStrike)    = ~(cellfun(@isempty,TempPutIdx));
            
            Market_value_call = sum(DumpTradersData.traders_value(IdxTradersCall) .* DumpTradersData.active_lots(IdxTradersCall) .* LotMult .* CurrMult);
            Market_value_put  = sum(DumpTradersData.traders_value(IdxTradersPut) .* DumpTradersData.active_lots(IdxTradersPut) .* LotMult .* CurrMult);
            Traders_Vega_call = sum(DumpTradersData.traders_vega_1(IdxTradersCall));
            Traders_Vega_put = sum(DumpTradersData.traders_vega_1(IdxTradersPut));
                        
            Instrument = unique(DumpTradersData.instrument(IdxTradersCall));
            if isempty(Instrument)
                Instrument = unique(DumpTradersData.instrument(IdxTradersPut));
            end
            if length(Instrument) > 1
                Instrument  = Instrument(1);
            end
            Traders_M_Us_Dump(TrMVLoopCntr,1) = Instrument;
            Traders_M_Us_Dump(TrMVLoopCntr,2) = Portfolio;
            Traders_M_Us_Dump(TrMVLoopCntr,3) = MV_ContractMonth(iMVCM);
            Traders_M_Us_Dump(TrMVLoopCntr,4) = num2cell(Strike(iStrike) / StrikeMult);
            Traders_M_Us_Dump(TrMVLoopCntr,5) = num2cell(Market_value_call);
            Traders_M_Us_Dump(TrMVLoopCntr,6) = num2cell(Market_value_put);
            Traders_M_Us_Dump(TrMVLoopCntr,7) = num2cell(Traders_Vega_call);
            Traders_M_Us_Dump(TrMVLoopCntr,8) = num2cell(Traders_Vega_put);
            
            TrMVLoopCntr = TrMVLoopCntr + 1;
        end
    end
end

if exist('M_Us_Dump','var')
Settle_M_Us_Dump = M_Us_Dump;
M_Us_Dump(:,[7,8]) = []; % Remove the vega columns from the normal M-Us dump
% remove RMS from the usual M-Us dump
IdxRMS = strncmpi('RMS-',M_Us_Dump(:,2),length('RMS-'));
M_Us_Dump(IdxRMS,:) = [];

% remove MM from the M-Us strikewise dump
IdxMM = strncmpi('MM-',Settle_M_Us_Dump(:,2),length('MM-'));
Settle_M_Us_Dump(IdxMM,:) = [];

% remove MM from the M-Us strikewise dump
IdxMM = strncmpi('MM-',Traders_M_Us_Dump(:,2),length('MM-'));
Traders_M_Us_Dump(IdxMM,:) = [];

end

%% generate the option dump
%%% TODO:28-Nov-2017 - VERIFY - Below lines are commented and modified on 28-Nov-2017, as Sri had mailed MO
% team that the below dump to be arrived from settlement surface
% IdxCallPut = cellStrfind(lower(DumpTradersData.derivative_type),{'put','call'});
% OptionData = DumpTradersData(IdxCallPut,:);
%%% END Of TODO:28-Nov-2017
IdxCallPut = cellStrfind(lower(DumpSettleData.derivative_type),{'put','call'});
OptionData = DumpSettleData(IdxCallPut,:);

% find and place the internal deals
IdxInt = strncmpi('MM',OptionData.counterparty_parent,2) | strncmpi('RMS',OptionData.counterparty_parent,3);
IsInternal = repmat(cellstr('N'),size(OptionData.trade_id));
IsInternal(IdxInt) = {'Y'};

Duplicated = repmat(cellstr('EXT'),size(OptionData.trade_id));
Duplicated(IdxInt) = {'INT-FYV'};

Call_Put_Id = repmat(cellstr('C'),size(OptionData.trade_id));
IdxCP = cellStrfind(lower(OptionData.derivative_type),'put');
Call_Put_Id(IdxCP) = {'P'};

BuySell = repmat(cellstr('B'),size(OptionData.trade_id));
IdxSell = strcmpi('sold',OptionData.market_action);
BuySell(IdxSell)  = {'S'};

InitialQty = abs(OptionData.original_lots);
LiveQty    = abs(OptionData.active_lots);

Strike = OptionData.strike;
Premium = OptionData.original_premium; 

for iS = 1:length(RefSubportfolio)
    IdxSP = strcmpi(RefSubportfolio{iS},OptionData.subportfolio);
    if any(IdxSP)
        Strike(IdxSP) = Strike(IdxSP) ./ RefStrikeMult(iS);
        Premium(IdxSP) = Premium(IdxSP) ./ RefPremiumMult(iS);
    end
end

OptionDumpHeader = {'DUPLICATED','Internal','Trn# (Internal)','Trn.Date','Instrument displayed label',...
    'Portfolio','BUY/SELL','InitialQty','CM: Contract maturity','Strike','Call/Put (C/P)',...
    'STL_PRM','Counterparty label','Live quantity','Settlement Premium'};
OptionDumpData = [Duplicated,IsInternal,OptionData.trade_id,OptionData.transaction_date,OptionData.instrument,...
    OptionData.subportfolio, BuySell, num2cell(InitialQty), OptionData.contract_month, num2cell(Strike), Call_Put_Id,...
    num2cell(Premium), OptionData.counterparty_parent, num2cell(LiveQty),num2cell(OptionData.settle_price)];
% PnpOptionDumpFileName = ['pnp_option_dump_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
% customcsvwrite(PnpOptionDumpFileName,OptionDumpData,OptionDumpHeader);
PnpOptionDumpFileName = getXLSFilename(['pnp_option_dump_',InBUName]);
xlswrite(PnpOptionDumpFileName,[OptionDumpHeader;OptionDumpData]);

if exist('FutureDump','var')
    FutDumpHeader = {'Instrument','Portfolio','Contract',' Book_Value ','Net Pos',...
        'FUT Price','FUT Settlement','Adapted delta','Gamma Lots','Theta','Vega',...
        'Accounting_Value','Accounting Delta','Accounting Gamma Lots','Accounting Theta','Accounting Vega','Book Gamma MTS','Accounting Gamma MTS'};    
%     PnpFutureDumpFileName = ['pnp_future_dump_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
%     customcsvwrite(PnpFutureDumpFileName,FutureDump,FutDumpHeader);
    PnpFutureDumpFileName = getXLSFilename(['pnp_future_dump_',InBUName]);
    xlswrite(PnpFutureDumpFileName,[FutDumpHeader;FutureDump]);

end

 PnpMUsDumpFileName = getXLSFilename(['pnp_M-Us_dump_',InBUName]);
if exist('M_Us_Dump','var')
    MUSDumpHeader = {'Instrument','Portfolio','Maturity','Strike',' Market Value Call','Market Value Put'};    
%     PnpMUsDumpFileName = ['pnp_M-Us_dump_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
%     customcsvwrite(PnpMUsDumpFileName,M_Us_Dump,MUSDumpHeader);
   
    xlswrite(PnpMUsDumpFileName,[MUSDumpHeader;M_Us_Dump],'Sheet1');
    
    SettleMUSDumpHeader = {'Instrument','Portfolio','Maturity','Strike',' Market Value Call','Market Value Put','Settle Vega Call','Settle Vega Put'};    
    xlswrite(PnpMUsDumpFileName,[SettleMUSDumpHeader;Settle_M_Us_Dump],'Sheet2');
    
    TradersMUSDumpHeader = {'Instrument','Portfolio','Maturity','Strike',' Market Value Call','Market Value Put','Traders Vega Call','Traders Vega Put'};    
    xlswrite(PnpMUsDumpFileName,[TradersMUSDumpHeader;Traders_M_Us_Dump],'Sheet3');
else
    MUSDumpHeader = {'Instrument','Portfolio','Maturity','Strike',' Market Value Call','Market Value Put'}; 
    xlswrite(PnpMUsDumpFileName,MUSDumpHeader,'Sheet1');
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end