function [OutErrorMsg,OutFilename] = generate_mo_summary_monthwise_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    Filename = ['Monthwise_Summary_Report_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        ObjDB = connect_to_database;
        
        Find_GroupType_Col = cellStrfind(ColNames,'asset_class');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_Curncy = cellfun(@(s) ~isempty(strfind('currency', s)),Get_GroupType);
        Data(ispresent_Curncy,:) = [];
        
        InData = cell2dataset([ColNames;Data]);
        
        [OutHeader,OutData] = compute_monthend_mtm_values(InBUName,InData,1);
        
        UniqueFields = {'ASSET_CLASS','PRODUCT_CODE','PRODUCT','PORTFOLIO','CONTRACT_MONTH','CURRENCY'};
        SumFields = {'Total_USD','Total_NC','Delta'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [OutputFields,ConsDeals] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        TempOutData = cell2dataset([OutputFields;ConsDeals]);
        
        TableName = 'currency_spot_mapping_table';
        [~,Data] = read_from_database(TableName,0);
        DBCurrency = Data(:,1);
        DBSpot = Data(:,2);
        
        ValueDate = fetch(ObjDB, 'select value_date from valuation_date_table');
        
        SpotRate = cell(size(TempOutData.CURRENCY,1),1);
        for iCurr = 1:length(DBCurrency)
            IdxCurr = strcmpi(DBCurrency{iCurr},TempOutData.CURRENCY);
            SqlQuery = ['select settle_value from underlying_settle_value_table where value_date  = ''',char(ValueDate),''' and underlying_id = ''',DBSpot{iCurr},''''];
            try
                SpotRate(IdxCurr) = fetch(ObjDB,SqlQuery);
            catch
                OutErrorMsg = [char(DBSpot{iCurr}),' Spot ID missing for corresponding value date '] ;
                return;
            end
        end
        
        TempOutData.SpotRate = SpotRate;
        TempOutData.CURRENCY = []; TempOutData = dataset2cell(TempOutData);
        
        OutHeader = {'Type','Product Code','Product','Portfolio','Maturity Label','Book_Value_USD','Book_Value_NC','Delta','Spot FX Rate'};
        OutData = TempOutData(2:end,:);
        
        xlswrite(OutFilename,[OutHeader;OutData]);
        
        %%% Newly added code
        %%% checking condition --> Maturity Label is empty or not
        Index_char = cellfun(@(v) ischar(v),OutData(:,5), 'UniformOutput',false);
        Index_nan = find(eq(0,cell2mat(Index_char)));
        OutData(Index_nan,5) = {'no contract_month'};
        %%% Newly added code
        settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
        settle_date = repmat(settlement_date,size(OutData,1),1);
        %%%% Upload the Final Data
        if strcmpi(InBUName,'qf1') || strcmpi(InBUName,'qf2') || strcmpi(InBUName,'qf3') || strcmpi(InBUName,'qf4')
        try
            Table_Name = strcat('mo_monthwise_summary_report_store_table_',InBUName);
            set(ObjDB,'AutoCommit','off');
            SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');

            upload_in_database(Table_Name,[settle_date,OutData],ObjDB);
            disp('Upload Done Successfully to Database Table');           
        catch
            OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
        end
        end
        %%% end code 
        
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end