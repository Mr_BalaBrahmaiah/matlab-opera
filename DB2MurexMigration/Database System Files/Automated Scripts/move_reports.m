function move_reports(SourceFile,DestinationFile,LogTitleMessage,LogFilename)

if ~exist('LogFilename','var')
 LogFilename = ['ML_automation_log_',datestr(today),'.txt'];
end

[SUCCESS,MESSAGE] = movefile(SourceFile,DestinationFile);

if SUCCESS
    write_log_file(LogFilename,[LogTitleMessage,': File created and saved successfully in ',DestinationFile]);
else
    write_log_file(LogFilename,[LogTitleMessage,': File created successfully and the following error occured while saving - ',MESSAGE]);
end

end