function run_recon_views

[~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
ReportNames = ConfigFileData(2:end,1);
OutputFolders = ConfigFileData(2:end,2);
IdxFound = strcmpi('ReconViews',ReportNames);
DestinationFolder = OutputFolders{IdxFound};

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try 
%% generate and write the traders recon dump
ViewName = 'helper_traders_p_and_g_view';
Filename = getXLSFilename(ViewName);
[ColNames,Data] = read_from_database(ViewName,0);
xlswrite(Filename,[ColNames;Data]);

% DestinationFolder = '\\Shared-pc\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';    
OutFilename = fullfile(DestinationFolder,Filename);
move_reports(Filename,OutFilename,'Traders Recon dump');


%% generate and write the settle recon dump
ViewName = 'helper_settle_p_and_g_view';
Filename = getXLSFilename(ViewName);
[ColNames,Data] = read_from_database(ViewName,0);
xlswrite(Filename,[ColNames;Data]);

% DestinationFolder = '\\Shared-pc\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';    
OutFilename = fullfile(DestinationFolder,Filename);
move_reports(Filename,OutFilename,'Settle Recon dump');


catch
    write_log_file(LogFilename,'Unknown error while saving recon dumps!');
end