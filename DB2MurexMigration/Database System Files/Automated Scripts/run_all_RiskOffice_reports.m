function run_all_RiskOffice_reports

[~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
ReportNames = ConfigFileData(2:end,1);
OutputFolders = ConfigFileData(2:end,2);
IdxFound = strcmpi('RiskOfficeReports',ReportNames);
DestinationFolder = OutputFolders{IdxFound};

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try     
    Filename = generate_futures_report;
    TempDestinationFolder = [DestinationFolder,'\Futures Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Risk Office Futures Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Futures Report under Risk Office Reports!');
end

try     
    Filename = generate_options_report;
    TempDestinationFolder = [DestinationFolder,'\Options Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Risk Office Options Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Options Report under Risk Office Reports!');
end

try     
    Filename = generate_currency_report;
    TempDestinationFolder = [DestinationFolder,'\FX Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Risk Office FX Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving FX Report under Risk Office Reports!');
end

try     
    Filename = generate_trade_query_report;
    TempDestinationFolder = [DestinationFolder,'\Trade Query Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Risk Office Trade Query Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Trade Query Report under Risk Office Reports!');
end

try  
    Filename = generate_riskoffice_rms_report;
    TempDestinationFolder = [DestinationFolder,'\OTC Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Risk Office OTC Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving OTC Report under Risk Office Reports!');
end