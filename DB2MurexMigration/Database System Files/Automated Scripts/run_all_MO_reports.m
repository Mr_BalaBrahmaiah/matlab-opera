function run_all_MO_reports

[~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
ReportNames = ConfigFileData(2:end,1);
OutputFolders = ConfigFileData(2:end,2);

IdxFound = strcmpi('MOReports',ReportNames);
DestinationFolder = OutputFolders{IdxFound};

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try     
    Filename = generate_traders_report_summary;
    TempDestinationFolder = [DestinationFolder,'\Traders Report Summary'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO Traders Report Summary');          
catch
    write_log_file(LogFilename,'Unknown error while saving Traders Report Summary under MO Reports!');
end

try     
    Filename = generate_rms_dump;
    TempDestinationFolder = [DestinationFolder,'\RMS Pnp Dump'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO RMS Pnp Dump');          
catch
    write_log_file(LogFilename,'Unknown error while saving RMS Pnp Dump under MO Reports!');
end

try
    [Filename1,Filename2,Filename3]  = generate_pnp_reports;
    TempDestinationFolder = [DestinationFolder,'\Pnp Reports'];
    OutFilename1 = fullfile(TempDestinationFolder,Filename1);
    move_reports(Filename1,OutFilename1,'MO Pnp Reports');
    OutFilename2 = fullfile(TempDestinationFolder,Filename2);
    move_reports(Filename2,OutFilename2,'MO Pnp Reports');
    OutFilename3 = fullfile(TempDestinationFolder,Filename3);
    move_reports(Filename3,OutFilename3,'MO Pnp Reports');
catch
    write_log_file(LogFilename,'Unknown error while saving Pnp Reports under MO Reports!');
end

try     
    Filename = generate_counterparty_report;
    TempDestinationFolder = [DestinationFolder,'\Counterparty Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO Counterparty Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Counterparty Report under MO Reports!');
end

try     
    Filename = generate_eod_report;
    TempDestinationFolder = [DestinationFolder,'\EOD Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO EOD Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving EOD Report under MO Reports!');
end

try     
    Filename = generate_dirbook_report;
    TempDestinationFolder = [DestinationFolder,'\DIR Book Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO DIR Book Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving DIR Book Report under MO Reports!');
end

try     
    Filename = generate_detailed_traders_report;
    TempDestinationFolder = [DestinationFolder,'\Traders Report Monthwise'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO Traders Report Monthwise');          
catch
    write_log_file(LogFilename,'Unknown error while saving Monthwise Traders Report under MO Reports!');
end

try     
    Filename = generate_monthend_eod_report;
    TempDestinationFolder = [DestinationFolder,'\Monthend EOD Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'MO Monthend EOD Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Monthend EOD Report under MO Reports!');
end


end
