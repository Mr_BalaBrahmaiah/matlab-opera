function run_all_Compliance_reports

[~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
ReportNames = ConfigFileData(2:end,1);
OutputFolders = ConfigFileData(2:end,2);
IdxFound = strcmpi('ComplianceReports',ReportNames);
DestinationFolder = OutputFolders{IdxFound};

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try     
    Filename = generate_compliance_report_summary;
    TempDestinationFolder = [DestinationFolder,'\Summary Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Compliance Summary Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Summary Report under Compliance Reports!');
end

try     
    Filename = generate_counterparty_delta_report;
    TempDestinationFolder = [DestinationFolder,'\Counterparty Delta Position Report'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Counterparty Delta Position Report');          
catch
    write_log_file(LogFilename,'Unknown error while saving Counterparty Delta Position Report under Compliance Reports!');
end

try     
    Filename = generate_compliance_summary_view;
    TempDestinationFolder = [DestinationFolder,'\Settlement Price & Delta Details'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Compliance Settlement Price & Delta Details');          
catch
    write_log_file(LogFilename,'Unknown error while saving Settlement Price & Delta Details under Compliance Reports!');
end

try     
    Filename = generate_compliance_trade_view;
    TempDestinationFolder = [DestinationFolder,'\Trade Details'];
    OutFilename = fullfile(TempDestinationFolder,Filename);
    move_reports(Filename,OutFilename,'Compliance Trade Details');          
catch
    write_log_file(LogFilename,'Unknown error while saving Trade Details under Compliance Reports!');
end

try   
    [Filename1,Filename2,Filename3]  = generate_compliance_delta_reports;
    TempDestinationFolder = [DestinationFolder,'\Delta Reports'];
    OutFilename1 = fullfile(TempDestinationFolder,Filename1);
    move_reports(Filename1,OutFilename1,'Compliance Delta Reports');
    OutFilename2 = fullfile(TempDestinationFolder,Filename2);
    move_reports(Filename2,OutFilename2,'Compliance Delta Reports');
    OutFilename3 = fullfile(TempDestinationFolder,Filename3);
    move_reports(Filename3,OutFilename3,'Compliance Delta Reports');
catch
    write_log_file(LogFilename,'Unknown error while saving Delta Reports under Compliance Reports!');
end