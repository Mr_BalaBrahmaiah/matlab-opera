function [OutErrorMsg,OutFilename] = generate_accounting_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_eod_monthend_report_view';
    [InputHeader,InputData] = read_from_database(ViewName,0,'',InBUName);
    
    if(strcmpi(InputData,'No Data'))
        OutErrorMsg = {'No Data from Database ,Check DB'};
        return;
    end
    
    InData = cell2dataset([InputHeader;InputData]);
    clear InputData;
    
    [OutputFields,ConsDeals] = compute_monthend_eod_values(InBUName,InData);
    
    Filename = ['AccountingReport_',upper(InBUName)];
    OutFilename = getXLSFilename(Filename);
    % OutFilename = getXLSFilename('AccountingReport');
    
    try
        ConsDeals = sortrows(ConsDeals,[5,3]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    
    xlswrite(OutFilename,[OutputFields;ConsDeals]);
    
%      %%% Old Code
%     if strcmpi(InBUName,'orx')
%         ObjDB = connect_to_database;
%         TableName = 'mc_accounting_report_table_orx';
%         set(ObjDB,'AutoCommit','off');
%         SqlQuery = ['delete from ',TableName];
%         curs = exec(ObjDB,SqlQuery);
%         commit(ObjDB);
%         if ~isempty(curs.Message)
%             disp(curs.Message);
%         end
%         set(ObjDB,'AutoCommit','on');
%         
%         upload_in_database(TableName, ConsDeals);
%     end
    
    %%% Newly added code
    ObjDB = connect_to_database;
    settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    settle_date = repmat(settlement_date,size(ConsDeals,1),1);
    %%%% Upload the Final Data
    if (strcmpi(InBUName,'qf1') || strcmpi(InBUName,'qf2') || strcmpi(InBUName,'qf3') || strcmpi(InBUName,'qf4') || strcmpi(InBUName,'cfs') || strcmpi(InBUName,'usg'))
    try
        Table_Name = strcat('accounting_report_store_table_',InBUName);
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Table_Name),' where settle_date = ''',char(settlement_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

        upload_in_database(Table_Name,[settle_date,ConsDeals],ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Table_Name)]};
    end
    end
    %%% end code
  
catch ME
    OutErrorMsg = cellstr(ME.message);
    OutFilename = '';
end