InputArray = round(linspace(1,1000000,100000))';
InputArray = (linspace(1,1000000,100000))';

OvearallSum = sum(InputArray);
Expected_Sum_Value = 213691;

if(Expected_Sum_Value < OvearallSum)
    SumResult = 0;
    OutputArray = [];
    for i = 1 : size(InputArray,1)
        
        Current_Element = InputArray(i);
        
        SumResult = SumResult + Current_Element;
        
        if(SumResult == Expected_Sum_Value)
            OutputArray = [OutputArray ; Current_Element];
            break;
        elseif(SumResult < Expected_Sum_Value)
            OutputArray = [OutputArray ; Current_Element];
            continue;
        elseif(SumResult > Expected_Sum_Value)
            if(Current_Element > 0)
                SumResult = SumResult - Current_Element;
                Diff_Expected_SumValue = abs(Expected_Sum_Value - SumResult);
                
                Current_Element_Remain = Current_Element - Diff_Expected_SumValue;
                Get_Diff_from_CurrentElement = Current_Element - Current_Element_Remain;
                
                SumResult =  SumResult + Get_Diff_from_CurrentElement;
                
                OutputArray = [OutputArray ; Get_Diff_from_CurrentElement];
                
                break;
                
            else
                continue;
            end
        else
            %Empty
        end
        
    end
    
    
else    
    %Warning
end