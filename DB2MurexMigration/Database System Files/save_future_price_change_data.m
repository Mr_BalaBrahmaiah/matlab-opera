function save_future_price_change_data

DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\Syed_Automation\FuturePriceChangeData';   

ViewName = 'fetch_future_price_change_data';
Filename = getXLSFilename('OPERA_Future_Price_Change_Data');
[ColNames,Data] = read_from_database(ViewName,0);
xlswrite(Filename,[ColNames;Data]);

try
    OutFilename = fullfile(DestinationFolder,Filename);
    move_reports(Filename,OutFilename,'FuturePriceChangeData');
catch
end