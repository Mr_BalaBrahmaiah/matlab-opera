function [OutErrorMsg , OutXLSName] = Refresh_RMO_Broker_Recon_Report(InBUName,Temp_Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: RMO Broker Recon Report
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 15 Nov 2019 / 11:45:00

%%% [OutErrorMsg , OutXLSName] = Refresh_RMO_Broker_Recon_Report('qf1','2019-11-27')

try 
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['rmo_broker_recon_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    ObjDB = connect_to_database;
    
    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = datestr(Temp_Settle_Date,'yyyy-mm-dd');
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    %%% Connect to Opera DB Channel
    ObjDB_Opera_Channel = connect_to_dbchannel;

    %%%% Fetch data fron OperaDB
    DBVarchar_Date = datestr(Settele_date,'dd-mmm-yy');

    TableName='broker_recon_report_table';
    SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

    [Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

    if(strcmpi(Broker_Table_Data,'No Data'))
        OutErrorMsg = {['For ',char(Settele_date),', Data is not available in ',char(TableName)]};
        return;
    end

    Broker_Recon_Table_Data = cell2table(Broker_Table_Data,'VariableNames',Broker_Tab_ColNames); %%% convert cell to table

    %%% Fetch data from Broker Cash Entry Portfolio_Counterparty_Master Table
    Broker_Currency_map_Table = 'broker_currency_mapping_table';
    SqlQuery_Broker_Currency_map_Table = ['select * from ',char(Broker_Currency_map_Table)] ;
    [Broker_Currency_map_Cols,Broker_Currency_map_Tab_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Currency_map_Table);

    if strcmpi(Broker_Currency_map_Tab_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Broker_Currency_map_Table)]} ;
        return;
    end 
    Broker_Currency_Map_Data = cell2table(Broker_Currency_map_Tab_Data,'VariableNames',Broker_Currency_map_Cols); %%% convert cell to dataset
    
    %%%% Broker Cash Entry and Broker Recon Mapping
    %%% Comparing Account Number and Currency and Counterparty
    [row,~] = size(Broker_Recon_Table_Data);
    for ii = 1:1:row
        BRC_currency = Broker_Recon_Table_Data.currency(ii);
        Matched_Index = strcmpi(BRC_currency,Broker_Currency_Map_Data.broker_currency);
        if any(Matched_Index)
            instrument = Broker_Currency_Map_Data.opera_currency(Matched_Index);
            %%% get the currency from instrument
            if contains(instrument,'USDUSD')
                final_currency(ii,1) = {'USD'};
            else
                final_currency(ii,1) = erase(strtok(instrument),'USD');
            end
        else
            OutErrorMsg = {[char(BRC_currency),' : Currency is not found in ',char(Broker_Currency_map_Table)]};
            return;
        end
    end
    Broker_Recon_Table_Data.currency = final_currency;
    
    IdxPut = strcmpi(Broker_Recon_Table_Data.trd_type,'PUT');
    IdxCall = strcmpi(Broker_Recon_Table_Data.trd_type,'CALL');
    Broker_Recon_Table_Data.trd_type(IdxPut) = {'OFUT'};
    Broker_Recon_Table_Data.trd_type(IdxCall) = {'OFUT'};
    
    %%% consolidation part for Broker_Opera_Master Data
    Consolidate_Data_Header =  Broker_Tab_ColNames;
    Consolidate_Data = table2cell(Broker_Recon_Table_Data);

    UniqueFields = {'account','st_date','currency','trd_type'}; 
    SumFields = {'commision','premium','fut_realised','margin_call','call_back','fx_realised','interest','loan','cash_adj','cash_tran_btw_act', ...
        'tenders','tender_fees''payment_tender','receipt_tender','give_up_comm','bank_chgs','fx_fixing_chg','platform_chg','currency_conv','gst'};
    OutputFields = {'account','st_date','currency','trd_type','commision','premium','fut_realised','margin_call','call_back','fx_realised','interest','loan', ...
        'cash_adj','cash_tran_btw_act', 'tenders','tender_fees','payment_tender','receipt_tender','give_up_comm','bank_chgs','fx_fixing_chg', ...
        'platform_chg','currency_conv','gst'};
    WeightedAverageFields = [];
    [Consolidate_Header,Data_Consolidate] = consolidatedata(Consolidate_Data_Header, Consolidate_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

    Broker_Recon_Data = cell2table(Data_Consolidate,'VariableNames',Consolidate_Header);  %% Convert to table
    
    %%% get account,currency and cash_balance(Close_Balance_statement value) data from
    %%% broker_balance_report_table @ Current Date
    Unique_Broker_Exp_Data = 'broker_balance_report_table';
    Unique_SqlQuery_Broker_Exp_Data = ['select account,currency,cash_balance from ',char(Unique_Broker_Exp_Data),' where desk = ''',char(InBUName),''' and st_date = ''',char(DBVarchar_Date),''' group by account,currency'];
    [Unique_Broker_Exp_Data_Cols,Unique_Broker_Exposure_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,Unique_SqlQuery_Broker_Exp_Data);

    if strcmpi(Unique_Broker_Exposure_Data,'No Data')
        OutErrorMsg = {['For ',char(DBVarchar_Date),', No Data found in ',char(Unique_Broker_Exp_Data)]} ;
        return;
    end
    
    Acco_Cur_Data = cell2table(Unique_Broker_Exposure_Data,'VariableNames',Unique_Broker_Exp_Data_Cols);  %% Convert to table
    
    %%% get account,currency and cash_balance(Opening_Balance value) data from
    %%% broker_balance_report_table @ Previous working Date
    Busday = busdate(Settele_date, -1);
    Last_wrkg_day = datestr(Busday,'dd-mmm-yy');
    Previous_SqlQuery_Broker_Exp_Data = ['select account,currency,cash_balance from ',char(Unique_Broker_Exp_Data),' where desk = ''',char(InBUName),''' and st_date = ''',char(Last_wrkg_day),''' group by account,currency'];
    [Previous_Broker_Exp_Data_Cols,Previous_Broker_Exposure_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,Previous_SqlQuery_Broker_Exp_Data);

    if strcmpi(Previous_Broker_Exposure_Data,'No Data')
        OutErrorMsg = {['For ',char(Last_wrkg_day),', No Data found in ',char(Unique_Broker_Exp_Data)]} ;
        return;
    end
    
    PreviousDate_OpeningBalance = cell2table(Previous_Broker_Exposure_Data,'VariableNames',Previous_Broker_Exp_Data_Cols);  %% Convert to table
    
    %%% Mapping Process
    [row,~] = size(Acco_Cur_Data);
    Final_User_Data = []; 
    for i = 1:1:row
        current_account = Acco_Cur_Data.account(i);
        current_currency = Acco_Cur_Data.currency(i);
        Close_Balance_statement = Acco_Cur_Data.cash_balance(i);

        %%% get the Opening_Balance
        Index_match = strcmpi(current_account,PreviousDate_OpeningBalance.account) & strcmpi(current_currency,PreviousDate_OpeningBalance.currency);
        if any(Index_match)
            Opening_Balance = PreviousDate_OpeningBalance.cash_balance(Index_match);
        else
            Opening_Balance = 0;
        end

        %%% Mapping process using account number and currency
        Match_Index = strcmpi(current_account,Broker_Recon_Data.account) & strcmpi(current_currency,Broker_Recon_Data.currency);
        if any(Match_Index)
            Matched_Broker_Recon_Data = Broker_Recon_Data(Match_Index,:);
            
            %%% FUT Commision and OFUT Commision
            IdxFut = strcmpi(Matched_Broker_Recon_Data.trd_type,'FUT');
            IdxOFut = strcmpi(Matched_Broker_Recon_Data.trd_type,'OFUT');
            Commission_Futures = sum(Matched_Broker_Recon_Data.commision(IdxFut));
            Commission_Options = sum(Matched_Broker_Recon_Data.commision(IdxOFut));
            
            %%% Remaining  Fields
            premium = sum(Matched_Broker_Recon_Data.premium);
            fut_realised = sum(Matched_Broker_Recon_Data.fut_realised);
            margin_call = sum(Matched_Broker_Recon_Data.margin_call);
            call_back = sum(Matched_Broker_Recon_Data.call_back);
            fx_realised = sum(Matched_Broker_Recon_Data.fx_realised);
            interest = sum(Matched_Broker_Recon_Data.interest);
            loan = sum(Matched_Broker_Recon_Data.loan);
            cash_adj = sum(Matched_Broker_Recon_Data.cash_adj);
            cash_tran_btw_act = sum(Matched_Broker_Recon_Data.cash_tran_btw_act);
            tenders = sum(Matched_Broker_Recon_Data.tenders);
            tender_fees = sum(Matched_Broker_Recon_Data.tender_fees);
            payment_tender = sum(Matched_Broker_Recon_Data.payment_tender);
            receipt_tender = sum(Matched_Broker_Recon_Data.receipt_tender);
            give_up_comm = sum(Matched_Broker_Recon_Data.give_up_comm);
            bank_chgs = sum(Matched_Broker_Recon_Data.bank_chgs);
            fx_fixing_chg = sum(Matched_Broker_Recon_Data.fx_fixing_chg);
            platform_chg = sum(Matched_Broker_Recon_Data.platform_chg);
            currency_conv = sum(Matched_Broker_Recon_Data.currency_conv);
            gst = sum(Matched_Broker_Recon_Data.gst);
            
            %%% Close Balance
            Close_Balance = Opening_Balance + Commission_Futures + Commission_Options + fut_realised + premium + payment_tender + receipt_tender + fx_fixing_chg + ...
                        gst + margin_call + call_back + fx_realised + interest + loan +cash_adj + cash_tran_btw_act + tenders + tender_fees + ...
                        give_up_comm + bank_chgs + platform_chg + currency_conv;
            
            Difference = Close_Balance - Close_Balance_statement;
            
            %%% Required Data
            User_Data = [current_account, cellstr(DBVarchar_Date), current_currency, num2cell(Opening_Balance), num2cell(Commission_Futures), num2cell(Commission_Options), ...
                    num2cell(fut_realised), num2cell(premium), num2cell(payment_tender), num2cell(receipt_tender), num2cell(fx_fixing_chg), ...
                    num2cell(gst), num2cell(margin_call), num2cell(call_back), num2cell(fx_realised), num2cell(interest), num2cell(loan), num2cell(cash_adj), ...
                    num2cell(cash_tran_btw_act), num2cell(tenders), num2cell(tender_fees), num2cell(give_up_comm), num2cell(bank_chgs), num2cell(platform_chg), ...
                    num2cell(currency_conv), num2cell(Close_Balance), num2cell(Close_Balance_statement), num2cell(Difference)];
        else
            %%% Required Data
            Difference = Opening_Balance - Close_Balance_statement;
            User_Data = [current_account, cellstr(DBVarchar_Date), current_currency, num2cell(Opening_Balance), {[]}, {[]}, ...
                    {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, ...
                    {[]}, num2cell(Opening_Balance), num2cell(Close_Balance_statement), num2cell(Difference)];
        end
        Final_User_Data = [Final_User_Data ; User_Data];
    end
    
    %%% Final data with Header and Data
    User_Haeder = {'Account','Statement Date','Currency','Opening Balance','Commission Futures','Commission Options','Future Realise','Option Premium', ...
                'Payment Tender','Receipt Tender','Fx Fixing Charges','GST','Margin Call','Call Back','Fx Realised','Interest','Loan','Cash Adjustment', ...
                'Cash Trans btw act','Tender','Tender Fees','Give Up Commission','Bank Charges','Platform Charges','Currency Conv','Close Balance', ...
                'Close Balance statement','Difference'};

    %%% write the data into excel sheet & Delete Empty sheets
    xlswrite(OutXLSName,[User_Haeder ; Final_User_Data],'Broker_Recon');
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end
end
