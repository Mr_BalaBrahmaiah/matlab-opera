function [OutErrorMsg , OutXLSName] = Refresh_RMO_Broker_Exposure_Report(InBUName,Temp_Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: RMO Broker Exposure Report or RMO Broker Balance Report
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 15 Nov 2019 / 11:45:00

%%% [OutErrorMsg , OutXLSName] = Refresh_RMO_Broker_Exposure_Report('qf1','2019-11-18')
try 
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['rmo_broker_exposure_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    ObjDB = connect_to_database;
    
    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = datestr(Temp_Settle_Date,'yyyy-mm-dd');
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end
    
    %%% Connect to Opera DB Channel
    ObjDB_Opera_Channel = connect_to_dbchannel;

    %%%% Fetch data fron OperaDBChannel
    DBVarchar_Date = datestr(Settele_date,'dd-mmm-yy');

    TableName='broker_balance_report_table';
    SqlQuery_Broker_Table = ['select * from ',char(TableName),' where st_date = ''',char(DBVarchar_Date),''' and desk = ''',char(InBUName),''' '] ;

    [Broker_Tab_ColNames,Broker_Table_Data] = Fetch_DB_Data(ObjDB_Opera_Channel,SqlQuery_Broker_Table);

    if(strcmpi(Broker_Table_Data,'No Data'))
        OutErrorMsg = {['For ',char(Settele_date),', Data is not available in ',char(TableName)]};
        return;
    end

    Broker_Exp_Data = cell2table(Broker_Table_Data,'VariableNames',Broker_Tab_ColNames);  %% Convert to table
     
    %%% Initial Margin & Date & Total_Exposure & Exposure Incl. Options
    Broker_Exp_Data.initial_margin = Broker_Exp_Data.initial_margin .* -1;
    Broker_Exp_Data.st_date = cellstr(datestr(Broker_Exp_Data.st_date,'yyyy-mm-dd'));
    Total_Exposure = num2cell(Broker_Exp_Data.cash_balance + Broker_Exp_Data.initial_margin + Broker_Exp_Data.variation_margin + Broker_Exp_Data.otc_m2m + Broker_Exp_Data.non_pass_through_m2m);
    Exposure_Incl_Options = num2cell(Broker_Exp_Data.cash_balance + Broker_Exp_Data.initial_margin + Broker_Exp_Data.variation_margin + Broker_Exp_Data.market_value_opt + Broker_Exp_Data.otc_m2m + Broker_Exp_Data.non_pass_through_m2m);
    
    %%% Final data with Header and Data
    User_Haeder = {'desk','broker','account','st_date','currency','cash_balance','initial_margin','variation_margin','market_value_opt','otc_m2m','non_pass_through_m2m','total_exposure','exposure_incl_options','curr_conv_rate'};
    User_Data = [Broker_Exp_Data.desk, Broker_Exp_Data.broker, Broker_Exp_Data.account, Broker_Exp_Data.st_date, Broker_Exp_Data.currency,num2cell(Broker_Exp_Data.cash_balance), ...
                num2cell(Broker_Exp_Data.initial_margin), num2cell(Broker_Exp_Data.variation_margin), num2cell(Broker_Exp_Data.market_value_opt), num2cell(Broker_Exp_Data.otc_m2m), num2cell(Broker_Exp_Data.non_pass_through_m2m), ...
                Total_Exposure, Exposure_Incl_Options, num2cell(Broker_Exp_Data.curr_conv_rate)];

    %%% write the data into excel sheet & Delete Empty sheets
    xlswrite(OutXLSName,[User_Haeder ; User_Data],'Broker_Exposure');
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end
end
