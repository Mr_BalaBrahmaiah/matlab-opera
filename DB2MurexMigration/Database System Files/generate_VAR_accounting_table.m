function generate_VAR_accounting_table

objDB = connect_to_database;

IsPriceUpdated = 0;

HolDates = {'2015-04-03';'2015-05-25'};
HolidayVec = datenum(HolDates,'yyyy-mm-dd');

VBusDays = busdays(datenum('2015-12-08'), datenum('2015-12-08'),'daily',HolidayVec);
VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));

for i = length(VBusDays):-1:1
    
%     SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];
%     curs = exec(objDB,SqlQuery);
%     if ~isempty(curs.Message)
%         errordlg(curs.Message);
%     end
%     SqlQuery = ['update valuation_date_table set settlement_date = ''',SDates{i},''''];
%     curs = exec(objDB,SqlQuery);
%     if ~isempty(curs.Message)
%         errordlg(curs.Message);
%     end
    
%     SqlQuery = ['select count(*) from security_settlement_value_table where value_date = ''',VDates{i},''''];
%     NumRecords = cell2mat(fetch(objDB,SqlQuery));
%     
%     if NumRecords == 0
%         derivativepricerinterface('settle',1,'ananthi');
%         IsPriceUpdated = 1;
%     end
    
%     ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    [SettleColNames,SettleData] = read_from_database(ViewName,0);
    
    UniqueFields = {'value_date','settlement_date','subportfolio','p1_name','contract_month','derivative_type','strike'};
    SumFields = {'settle_delta_1','settle_delta_2','settle_gamma_11',...
        'settle_gamma_12','settle_gamma_21','settle_gamma_22',...
        'settle_vega_1','settle_vega_2','settle_theta','mtm-usd','mtm-nc'};
    OutputFields = [UniqueFields,'p1_settleprice',SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(SettleColNames, SettleData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    Delta = cell2mat(OutData(:,9)) +  cell2mat(OutData(:,10));
    Gamma = cell2mat(OutData(:,11)) +  cell2mat(OutData(:,12)) + cell2mat(OutData(:,13)) +  cell2mat(OutData(:,14));
    Vega =  cell2mat(OutData(:,15)) +  cell2mat(OutData(:,16));
    
    AccTradersReportData = [OutData(:,1:8),num2cell(Delta),num2cell(Gamma),num2cell(Vega),OutData(:,17:end)];
    
    AccTradersReportHeader = {'value_date','settlement_date','subportfolio','p1_name','contract_month','derivative_type','strike','p1_settleprice','delta','gamma','vega','theta','mtm_usd','mtm_nc'};
    AccTradersReportFilename = getXLSFilename('Accounting_Traders_Report');
    xlswrite(AccTradersReportFilename,[AccTradersReportHeader;AccTradersReportData]);
    
    TableName = 'var_accounting_table';
    upload_in_database(TableName, AccTradersReportData);
    
    update_curr_mult_table;
    
%     if IsPriceUpdated
%         SqlQuery = ['delete from security_settlement_value_table where value_date = ''',VDates{i},''''];
%         curs = exec(objDB,SqlQuery);
%         if ~isempty(curs.Message)
%             errordlg(curs.Message);
%         end
%     end
    
end