function [OutErrorMsg,Summary_Filename,Monthwise_Filename,FXFuture_Filename,FXForwards_Filename] = generate_all_MO_reports_cof(InBUName)

OutErrorMsg = {'No errors'};

if iscellstr(InBUName)
    InBUName = char(InBUName);
end

%% Summary Report

[OutErrorMsg1,Summary_Filename] = generate_mo_summary_report(InBUName);
if ~strcmpi(OutErrorMsg1,'No errors')
    Summary_Filename = char(OutErrorMsg1);
end

cprintf('key','%s finished\n', 'Summary Report');

%% Monthwise Summary Report

[OutErrorMsg2,Monthwise_Filename] = generate_mo_summary_monthwise_report(InBUName);
if ~strcmpi(OutErrorMsg2,'No errors')
    Monthwise_Filename = char(OutErrorMsg2);
end

cprintf('key','%s finished\n', 'Monthwise Summary Report');


%% FXFutures Report

[OutErrorMsg3,FXFuture_Filename] = generate_mo_fxfutures_report(InBUName);
if ~strcmpi(OutErrorMsg3,'No errors')
    FXFuture_Filename = char(OutErrorMsg3);
end

cprintf('key','%s finished\n', 'FXFutures Report');


%% FXForwards Report

[OutErrorMsg4,FXForwards_Filename] = generate_mo_fxforwards_report(InBUName);
if ~strcmpi(OutErrorMsg4,'No errors')
    FXForwards_Filename = char(OutErrorMsg4);
end

cprintf('key','%s finished\n', 'FXFutures Report');

%% Overall ErrorMsg

Overall_ErrorMsg = [OutErrorMsg1;OutErrorMsg2;OutErrorMsg3;OutErrorMsg4] ;


end
