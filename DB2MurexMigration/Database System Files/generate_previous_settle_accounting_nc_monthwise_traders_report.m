function [OutErrorMsg,AccountingTRReport] = generate_previous_settle_accounting_nc_monthwise_traders_report(InBUName)

try
    OutErrorMsg = {'No errors'};
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
%     derivativepricerinterface_previous_settle_vols(InBUName,'settle',1,'system');
    
    ObjDB = connect_to_database;
    
    ViewName = 'helper_settle_pricing_previous_settle_vols_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    AccountingTRReport = getXLSFilename('Previous_Settle_Accounting_NC_Monthwise_Traders_Report');
    
    PosTradeDate   = strcmpi('transaction_date',ColNames);
    PosHedgeStatus = strcmpi('fut_hedge_status',ColNames);
    TradeClassification = cell(size(Data,1),1);
    FutHedgeStatus = cell(size(Data,1),1);
    
    SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    IdxFTDTrades = strcmpi(SettlementDate,Data(:,PosTradeDate));
    TradeClassification(IdxFTDTrades)  = {'FTD'};
    TradeClassification(~IdxFTDTrades) = {'NonFTD'};
    
    DBHedgeData = strtrim(Data(:,PosHedgeStatus));
    IdxHedge = strcmpi('H',DBHedgeData) | strcmpi('XH',DBHedgeData);
    FutHedgeStatus(IdxHedge) = {'H'};
    FutHedgeStatus(~IdxHedge) = {'NH'};
    IdxEXS = strcmpi('EXS',DBHedgeData);
    FutHedgeStatus(IdxEXS) = {'EXS'};
    
    Data     = [Data,TradeClassification,FutHedgeStatus];
    ColNames = [ColNames,'trade_classification','future_hedge_status'];
    
    UniqueFields = {'value_date','subportfolio','group_type','contract_month','trade_classification','future_hedge_status'};
    SumFields = {'settle_delta_1','gamma_lots','settle_theta','settle_vega_1','mtm-nc'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'PreviousVol Summary Report');
    
    TableName = ['dealprofit_monthwise_previous_settlevol_tr_summary_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutData);        

    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    PosTradeDate   = strcmpi('transaction_date',ColNames);
    PosHedgeStatus = strcmpi('fut_hedge_status',ColNames);
    TradeClassification = cell(size(Data,1),1);
    FutHedgeStatus = cell(size(Data,1),1);
    
    SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    IdxFTDTrades = strcmpi(SettlementDate,Data(:,PosTradeDate));
    TradeClassification(IdxFTDTrades)  = {'FTD'};
    TradeClassification(~IdxFTDTrades) = {'NonFTD'};
    
    DBHedgeData = strtrim(Data(:,PosHedgeStatus));
    IdxHedge = strcmpi('H',DBHedgeData) | strcmpi('XH',DBHedgeData);
    FutHedgeStatus(IdxHedge) = {'H'};
    FutHedgeStatus(~IdxHedge) = {'NH'};
    IdxEXS = strcmpi('EXS',DBHedgeData);
    FutHedgeStatus(IdxEXS) = {'EXS'};
    
    Data     = [Data,TradeClassification,FutHedgeStatus];
    ColNames = [ColNames,'trade_classification','future_hedge_status'];
    
    UniqueFields = {'value_date','subportfolio','group_type','contract_month','trade_classification','future_hedge_status'};
    SumFields = {'settle_delta_1','gamma_lots','settle_theta','settle_vega_1','mtm-nc'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    [OutputFields,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    xlswrite(AccountingTRReport,[OutputFields;OutData],'TodaysVol Summary Report');
    
    TableName = ['dealprofit_monthwise_todays_settlevol_tr_summary_table_',InBUName];
    set(ObjDB,'AutoCommit','off');
    DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
    curs = exec(ObjDB,SqlQuery);
    commit(ObjDB);
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(ObjDB,'AutoCommit','on');
    
    upload_in_database(TableName, OutData); 
           
    try
        xls_delete_sheets(fullfile(pwd,AccountingTRReport));
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    AccountingTRReport = '';
end