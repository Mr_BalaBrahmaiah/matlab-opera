function [OutErrorMsg,OutFilename] = generate_ff_portfolio_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

global ObjDB;
ObjDB = connect_to_database;

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %     ObjDB = connect_to_database;
    
    OutFilename = getXLSFilename('FF_Portfolio_Report');
    
    %% Fetch DB Data
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    ViewName = 'helper_eod_monthend_report_view';
    SqlQuery = 'select * from helper_eod_monthend_report_view where subportfolio not like ''conso%''';
    [ColNames,Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    
    Product_MapingTable = strcat('subportfolio_product_mapping_table_',char(InBUName));
    [Product_MapTable_ColNames,Product_MapTable_Data] = Fetch_DB_Data(ObjDB,[],Product_MapingTable);
    Product_MapTable_Data = cell2dataset([Product_MapTable_ColNames;Product_MapTable_Data]) ;
    
    %%
    
    if ~strcmpi(Data,'No Data')
        
        InData = cell2dataset([ColNames;Data]);
        
        ValueDate = InData.value_date(1);
        
        [OutHeader,OutData] = ff_compute_monthend_mtm_values(InBUName,InData,0);   %%% Newly added f
        
        UniqueFields = {'PORTFOLIO','UNDERLYING','GRP','COUNTERPARTY'}; % added counterparty parent
        SumFields={'Realized_USD','M2M_USD','Total_USD','Gamma','Theta','Vega','Delta'};
        OutputFields = [UniqueFields,SumFields];
        WeightedAverageFields = [];
        [OutHeader_cp,ConsDeals] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        ConsDeals1 = ConsDeals;%%% Newly added on 22-01-2020, for counterparty parent
        ConsDeals(:,4) = [];
        
        CumBV = cell2mat(ConsDeals(:,4)) +  cell2mat(ConsDeals(:,5));
        ConsDeals(:,5) = num2cell(CumBV);
        ConsDeals(:,4) =  [];
        
        OutHeader = {'Portfolio','Underlying','Group','CUMULATIVE_BV','Book_Value','Gamma','Theta','Vega','Delta'};
        xlswrite(OutFilename,[OutHeader;ConsDeals]);
        
        xlswrite(OutFilename,[OutHeader_cp;ConsDeals1],'Sheet2');%%% Newly added on 22-01-2020, for counterparty parent
        
        OutData = cell2dataset([OutHeader;ConsDeals]); % store these numbers in DB
        
        %%  Product Code Mapping & Uploading into DB for Breach VAR Report Purpose
        
        Temp_Date_ProdCode = cell(size(ConsDeals,1),2);
        Temp_Date_ProdCode(:,1) = Settle_Date;
        
        for i = 1 : size(ConsDeals,1)
            Current_Portfolio = ConsDeals(i,1);
            Current_Instrument = strrep(ConsDeals(i,2),'-O','');
            Current_DerivativeType = ConsDeals(i,3);
            
            Matched_Index = (strcmpi(Product_MapTable_Data.subportfolio,Current_Portfolio) |  strcmpi(Product_MapTable_Data.directional_portfolio,Current_Portfolio) )...
                & strcmpi(Product_MapTable_Data.instrument,Current_Instrument) ;
            
            if(~isempty(find(Matched_Index, 1)))
                if(isempty(find(strcmpi({'FXFUT','FXD','CSO'},Current_DerivativeType)))) %%% newly added Group Type is CSO (2019-07-18)
                    Temp_Date_ProdCode(i,2) = strtrim(strrep(Product_MapTable_Data.product_code(Matched_Index),'CM',''));
                else
                    % Breach Commodity Mapping Purpose for FXFUT & FXD
                    if(strcmpi(Current_Instrument,'USD/CNY'))
                        Temp_Date_ProdCode(i,2) = {'VV'};
                    elseif(strcmpi(Current_Instrument,'USDCNY CASH'))
                        if(strcmpi(Current_Portfolio,'BETA-FF1-OIL'))
                            Temp_Date_ProdCode(i,2) = {'PAL'};
                        else
                            Temp_Date_ProdCode(i,2) = {'VV'};
                        end
                    elseif(strcmpi(Current_Instrument,'USD/MYR'))
                        Temp_Date_ProdCode(i,2) = {'KO'};
                    elseif(strcmpi(Current_Instrument,'USDMYR CASH'))
                        Temp_Date_ProdCode(i,2) = {'KO'};
                    elseif(strcmpi(Current_Instrument,'USD/CNH'))
                        Temp_Date_ProdCode(i,2) = {'VV'};
                    elseif(strcmpi(Current_Instrument,'CME GBPUSD'))
                        Temp_Date_ProdCode(i,2) = {'QC'};
                    elseif(strcmpi(Current_Instrument,'CME USDCAD'))
                        Temp_Date_ProdCode(i,2) = {'RS'};
                    elseif(strcmpi(Current_Instrument,'USDUSD CASH'))
                        Temp_Date_ProdCode(i,2) = {'BO'};
                    elseif(strcmpi(Current_Instrument,'GBPUSD CASH'))
                        Temp_Date_ProdCode(i,2) = {'QC'};
                    elseif(strcmpi(Current_Instrument,'EURUSD CASH'))
                        Temp_Date_ProdCode(i,2) = {'CA'};
                    elseif(strcmpi(Current_Instrument,'USDZAR CASH'))
                        Temp_Date_ProdCode(i,2) = {'WZ'};
                    elseif(strcmpi(Current_Instrument,'USDCAD CASH'))
                        if(strcmpi(Current_Portfolio,'FUND-GROUP-OIL'))
                            Temp_Date_ProdCode(i,2) = {'RS'};
                        elseif(strcmpi(Current_Portfolio,'ALPHA-FF1-OIL'))
                            Temp_Date_ProdCode(i,2) = {'RS'};
                        elseif(strcmpi(Current_Portfolio,'BETA-FF1-OIL'))
                            Temp_Date_ProdCode(i,2) = {'RS'};
                        else
                            Temp_Date_ProdCode(i,2) = {'RS'}; %% Need to Change
                        end
                    elseif(strcmpi(Current_Instrument,'CME EURUSD'))
                        if(strcmpi(Current_Portfolio,'ALPHA-FF3-OIL'))
                            Temp_Date_ProdCode(i,2) = {'CA'};
                        elseif(strcmpi(Current_Portfolio,'ALCAW-FF3-OIL'))
                            Temp_Date_ProdCode(i,2) = {'CA'};
                        elseif(strcmpi(Current_Portfolio,'BETA-FF3-OIL'))
                            Temp_Date_ProdCode(i,2) = {'IJ'};
                        else
                            Temp_Date_ProdCode(i,2) = {'CA'}; %% Need to Change
                        end
                        
                    else
                        %                 Temp_Date_ProdCode(i,2) = strrep(cellstr( strcat('CR',{' '},char(Current_Instrument)) ),'/',''); %%
                        %                         aa = char(Current_Instrument);
                        %                         Temp_Date_ProdCode(i,2) = strtrim(cellstr(aa(4:5)));
                    end
                    
                end
            end
        end
        
        OutHeader_1 = {'COB_Date','Product_Code','Portfolio','Underlying','Group','CUMULATIVE_BV','Book_Value','Gamma','Theta','Vega','Delta'};
        Overall_Data = [Temp_Date_ProdCode,ConsDeals];
        
        %         xlswrite(OutFilename,[OutHeader_1;Overall_Data],'FF_ProductLevel_PortfolioData');
        
        % Uploading into FF Portfolio Table
        Table_Name = ['ff_portfolio_report_table_',char(InBUName)];
        sql_query = ['select distinct(portfolio) from ',char(Table_Name)];
        Unique_FF_Portfolio = fetch(ObjDB,sql_query); % Temp
        Required_Data = Overall_Data(cellStrfind_exact(Overall_Data(:,3),Unique_FF_Portfolio),:);
        
        TableName = strcat('ff_portfolio_report_table_',char(InBUName));
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',TableName,' where cob_date = ''',char(Settle_Date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        upload_in_database(TableName, Required_Data); %% Required_Data %% Overall_Data
        cprintf('key','%s finished\n', ['Data Uploaded into ',char(TableName),' for COB date : ',char(Settle_Date)]);
        
        %%
        CRM_Table_Name = [char(InBUName),'_combined_report_mapping_table'];
        [ColNames,Data] = read_from_database(CRM_Table_Name,0);
        DBData_All = cell2dataset([ColNames;Data]);
        PortfolioGroups = [{'ALL'};unique(DBData_All.group)];
        
        for i = 1 : length(PortfolioGroups)
            
            if(i==1)
                DBData = DBData_All;
                TableName = [char(InBUName),'_portfoliowise_combined_pnl_table'];
                FY_OpenBal_Table = [char(InBUName),'_portfoliowise_openbalance_table']; 
            else
                
                DBData = DBData_All(cellStrfind_exact(DBData_All.group,PortfolioGroups(i)),:);
                TableName = [char(InBUName),'_portfoliowise_combined_pnl_table_',char(PortfolioGroups(i))];
                FY_OpenBal_Table = [char(InBUName),'_portfoliowise_openbalance_table_',char(PortfolioGroups(i))];
            end
            
            NumSubPortfolios = length(DBData.subportfolio);
            
            CombinedPF = cell(size(OutData.Portfolio));
            for iPF = 1:NumSubPortfolios
                IdxPF = strcmpi(DBData.subportfolio{iPF},OutData.Portfolio);
                CombinedPF(IdxPF) = DBData.portfolio(iPF);
            end
            
            UniqueCombinedPF = unique(CombinedPF(~cellfun(@isempty,CombinedPF)));
            NumCombinedPF = length(UniqueCombinedPF);
            OutCombinedPFData = [];
            for iCPF = 1:NumCombinedPF
                IdxCPF = strcmpi(UniqueCombinedPF{iCPF},CombinedPF);
                CumBV = sum(OutData.CUMULATIVE_BV(IdxCPF));
                YtdBV = sum(OutData.Book_Value(IdxCPF));
                RowData = [ValueDate,UniqueCombinedPF(iCPF),num2cell(CumBV),num2cell(YtdBV)];
                OutCombinedPFData = [OutCombinedPFData;RowData];
            end
            
            RowData = [ValueDate,{'NET TOTAL'},sum(OutData.CUMULATIVE_BV),sum(OutData.Book_Value)];
            Header = {'value_date','portfolio','cumulative_bv','ytd_bv'};
            OutCombinedPFData = [OutCombinedPFData;RowData];
            
            %%  Make Summary Table
            FinancialYear = fetch(ObjDB,'select financial_year FROM valuation_financial_year_view');
            [FY_OpenBal_Header,FY_OpenBal_Table_Data] = Fetch_DB_Data(ObjDB,['select * from ',FY_OpenBal_Table,' where financial_year = ''',char(FinancialYear),''' ']);
            
            Porfolio_Col = cellStrfind_exact(FY_OpenBal_Header,{'portfolio'});
            
            OpenBal_Col = cellStrfind_exact(FY_OpenBal_Header,{'open_balance'});
            CumBV_Col = cellStrfind_exact(Header,{'cumulative_bv'});
            YTDBV_Col = cellStrfind_exact(Header,{'ytd_bv'});
            
            Final_Data = cell(size(FY_OpenBal_Table_Data,1),4);
            Final_Data(:,1) = ValueDate;
            Final_Data(:,2) = FY_OpenBal_Table_Data(:,Porfolio_Col);
            
            Matched_Index = cellStrfind_exact(FY_OpenBal_Table_Data(:,Porfolio_Col),OutCombinedPFData(:,Porfolio_Col)) ; %% porfolio Col 2
            
            Not_Matched_Portfolio = setdiff(FY_OpenBal_Table_Data(:,Porfolio_Col),OutCombinedPFData(:,Porfolio_Col));
            Not_Matched_Index = cellStrfind_exact(FY_OpenBal_Table_Data(:,Porfolio_Col),Not_Matched_Portfolio);
            
            Final_Data(Matched_Index,CumBV_Col) = num2cell( cell2mat(FY_OpenBal_Table_Data(Matched_Index,OpenBal_Col)) + cell2mat(OutCombinedPFData(:,YTDBV_Col)) );
            Final_Data(Not_Matched_Index,CumBV_Col) = FY_OpenBal_Table_Data(Not_Matched_Index,OpenBal_Col);
            Final_Data(Matched_Index,YTDBV_Col) = OutCombinedPFData(:,YTDBV_Col);
            Final_Data(Not_Matched_Index,YTDBV_Col) = {NaN};
            
            [~,ColSum] = cell2sum_Row_Col(Final_Data(1:end-1,3:4)) ;
            Final_Data(end,3:4) = ColSum; %% Sum without NET TOTAL Row and Replace to NET TOTAL
            
            OutCombinedPFData = Final_Data;
            
            xlswrite(OutFilename,[Header;OutCombinedPFData],char(PortfolioGroups(i)));
            
            %% Uploading Data
            
            % TableName = 'agf_portfoliowise_combined_pnl_table';
            set(ObjDB,'AutoCommit','off');
            DBValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
            SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
            curs = exec(ObjDB,SqlQuery);
            commit(ObjDB);
            if ~isempty(curs.Message)
                disp(curs.Message);
            end
            set(ObjDB,'AutoCommit','on');
            
            upload_in_database(TableName, OutCombinedPFData);
            
            %% Developer Purpose some times production uploading is failed , that time use it UAT and directly upload into production
            
            aa = 0; %% When its needed make it one
            if(aa)
                DBName = 'dbsystemuat';
                DBUserName = 'user';
                DBPassword = 'invenio@123';
                DBServerIP = '192.168.37.62';
                ObjDB_Prod = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
                
                set(ObjDB_Prod,'AutoCommit','off');
                DBValueDate = fetch(ObjDB_Prod,'select value_date from valuation_date_table');
                SqlQuery = ['delete from ',TableName,' where value_date = ''',char(DBValueDate),''''];
                curs = exec(ObjDB_Prod,SqlQuery);
                commit(ObjDB_Prod);
                if ~isempty(curs.Message)
                    disp(curs.Message);
                end
                set(ObjDB_Prod,'AutoCommit','on');
                
                upload_in_database(TableName, OutCombinedPFData,ObjDB_Prod);
                PC_pnl_TableName = [char(InBUName),'_productwise_combined_pnl_table'];
                cprintf('key','%s finished\n', ['Data Uploaded into production ',char(PC_pnl_TableName),' : ',char(DBValueDate)]);
            end
            
        end
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end