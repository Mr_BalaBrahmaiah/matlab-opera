function [OutErrorMsg,OutFilename] = generate_opt_real_unr_report(InBUName)
%  Generate the options reports in Murex format
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/28 11:00:27 $
%  $Revision: 1.9 $
%
%

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
if iscellstr(InBUName)
    InBUName = char(InBUName);
end

ViewName = 'helper_funds_reports_view';
[SettleColNames,SettleData] = read_from_database(ViewName,0,'',InBUName);

Pos_GroupType = strcmpi('group_type',SettleColNames);
Pos_OptType = strcmpi('derivative_type',SettleColNames);

%%% Old logic
% IdxNonOFUT = ~(strcmpi('OFUT',SettleData(:,Pos_GroupType)));
% SettleData(IdxNonOFUT,:) = [];

%%% New logic
IdxNonOFUT = strcmpi('OFUT',SettleData(:,Pos_GroupType));  
IdxNonCSO = strcmpi('CSO',SettleData(:,Pos_GroupType));  %%% newly added Group Type is CSO (2019-07-18)
IdxNon = ~(IdxNonOFUT | IdxNonCSO);
SettleData(IdxNon,:) = [];

% find the option records
IdxOpt = cellStrfind(lower(SettleData(:,Pos_OptType)),{'call','put'});

% generate the options report
DBOptionData = SettleData(IdxOpt,:);
% To remove the USDBRL fx currency options from the report
Pos_ProductCode = strcmpi('product_code',SettleColNames);
ProductCode  = DBOptionData(:,Pos_ProductCode);
DBOptionData(cellStrfind(ProductCode,'USD'),:) = [];

SettleColNames = [SettleColNames,'deal_status'];

Pos_Maturity = strcmpi('maturity_date',SettleColNames);
Pos_ValueDate = strcmpi('value_date',SettleColNames);
PosNettingStatus = strcmpi('netting_status',SettleColNames);
IdxNetted = strcmpi('fully_netted',DBOptionData(:,PosNettingStatus)); % dead
MaturityDate = datenum(DBOptionData(:,Pos_Maturity),'yyyy-mm-dd');
ValueDate = datenum(DBOptionData(:,Pos_ValueDate),'yyyy-mm-dd');
IdxDeadOpt = (ValueDate > MaturityDate) | IdxNetted;
DealStatus = repmat({'live'},size(MaturityDate));
DealStatus(IdxDeadOpt) = {'dead'};

DBOptionData = [DBOptionData,DealStatus];
    
if ~isempty(DBOptionData)
 
    IdxCall = cellStrfind(lower(DBOptionData(:,Pos_OptType)),'call');
    IdxPut = cellStrfind(lower(DBOptionData(:,Pos_OptType)),'put');
    DBOptionData(IdxCall,Pos_OptType) = {'call'};
    DBOptionData(IdxPut,Pos_OptType) = {'put'};
    
    OutputFields = {'product_code','subportfolio','settlement_date','counterparty_parent',...
        'contract_month','derivative_type','strike'...
        'settle_price','market_action','active_lots','original_lots',...
        'original_premium','mtm_nc','mult_factor','conv_factor','product_name','deal_status'};
    WeightedAverageFields = {'original_premium','active_lots'};
    UniqueFields = {'product_code','subportfolio','counterparty_parent','contract_month','derivative_type','strike','market_action','deal_status'};
    SumFields = {'original_lots','active_lots','mtm_nc'};
    [OptionFields,OptionDeals] = consolidatedata(SettleColNames, DBOptionData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    ConsOptData = cell2dataset([OptionFields;OptionDeals]);
    
      IdxLiveOpt = strcmpi('live', ConsOptData.deal_status);
    IdxDeadOpt = strcmpi('dead', ConsOptData.deal_status);
    
    IdxOptBot = strcmpi('bought', ConsOptData.market_action) & IdxLiveOpt;
    IdxOptSold = strcmpi('sold', ConsOptData.market_action) & IdxLiveOpt;
    
    % initialisation
    OpenBought = zeros(size(IdxOptBot));
    OpenSold = zeros(size(IdxOptSold));  
    M2MOpt = zeros(size(IdxOptSold));
    RealisedOpt = zeros(size(IdxOptSold));
  
    MultFac = ConsOptData.mult_factor;
    ConvFac = ConsOptData.conv_factor;
%     NCFactor = ConsOptData.nc_factor;

    OpenBought(IdxOptBot) = abs(ConsOptData.active_lots(IdxOptBot));
    OpenSold(IdxOptSold) = abs(ConsOptData.active_lots(IdxOptSold));
    NetFutOpen = OpenBought - OpenSold;
                
    M2MOpt(IdxLiveOpt) = ConsOptData.mtm_nc(IdxLiveOpt) ;
    RealisedOpt(IdxDeadOpt) =  ConsOptData.mtm_nc(IdxDeadOpt) ;
    Mkt_Rate = ConsOptData.settle_price;
    Strike = ConsOptData.strike ./ ConvFac;
    
    Fut_Mkt_Val = ((OpenBought .* Mkt_Rate) - (OpenSold .* Mkt_Rate)) .* MultFac ;
%     Fut_Val = Fut_Mkt_Val - M2MOpt;
    
%     RealisedOpt = Fut_Val .* -1;
    
    Mkt_Rate = Mkt_Rate ./ ConvFac;
    
    OutData = [ConsOptData.product_code,ConsOptData.product_name,ConsOptData.derivative_type,....
        ConsOptData.counterparty_parent,ConsOptData.subportfolio,ConsOptData.contract_month,...
        num2cell(Strike),...
        num2cell(OpenSold),...
        num2cell(OpenBought),num2cell(NetFutOpen),...
        num2cell(RealisedOpt),num2cell(M2MOpt),...
        num2cell(Fut_Mkt_Val),num2cell(Mkt_Rate),...
        ConsOptData.settlement_date];
    OutHeader = {'ProductCode','ProductName','Call_Put',...
        'Counterparty','Portfolio','ContractMonth','Strike',...
        'Open_Sold',...
        'Open_Bought','Net_Open',...
        'Realised_PnL','M2M','MktVal','Mkt_Rate',...
        'Market_ValueDate'};
    
    WeightedAverageFields = [];
    UniqueFields = {'ProductCode','Portfolio','Counterparty','ContractMonth','Call_Put','Strike'};
    SumFields = {'Open_Sold','Open_Bought','Net_Open','Realised_PnL','M2M',...
       'MktVal'};
    [~,OutOptionData] = consolidatedata(OutHeader, OutData,UniqueFields,SumFields,OutHeader,WeightedAverageFields);
 
    try
        OutOptionData = sortrows(OutOptionData,[1 2 3 4 5 6]);
    catch
        disp('Error while sorting rows: some NaN values are found');
    end
    
    OutHeader = {'PRODUCT_CODE','PRODUCT_NAME','CALL_PUT','BROKER','PORTFOLIO','TMONTH',...
        'STRIKE','OPEN_SOLD',...
        'OPEN_BOT','NET_OPEN','REALIZED_PL','MTM','MKTVAL','MKT_RATE',...
        'MKT_DATE'};
    
    OutFilename = getXLSFilename('Options_Realised_Unrealised_Report');
    xlswrite(OutFilename,[OutHeader;OutOptionData]);
else
    OutErrorMsg = {'No option trades found!'};
end
catch ME
    OutErrorMsg = cellstr(ME.message);
end