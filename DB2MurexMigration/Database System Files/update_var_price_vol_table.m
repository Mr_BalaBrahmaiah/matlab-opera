[filename, pathname] = uigetfile( ...
    {'*.xlsx';'*.xls';'*.*'}, ...
    'Pick the files','MultiSelect', 'on');


if pathname ~= 0
    objDB = connect_to_database;
    if ~iscellstr(filename)
        filename = cellstr(filename);
    end
    for iFile = 1:length(filename)
        Fullfilename = fullfile(pathname,filename{iFile});
        [~,~,RawData ] = xlsread(Fullfilename);
        Data = RawData(2:end,:);
        Data(:,2) = cellstr(datestr(datenum(Data(:,2),'dd-mm-yyyy'),'yyyy-mm-dd'));
        ColNames = {'bbg_underlying_id','settlement_date','price','vol'};
        fastinsert(objDB, 'var_price_vol_table', ColNames,Data);
    end
    
end