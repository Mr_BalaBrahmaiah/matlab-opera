function [OutErrorMsg,Filename] = process_option_expiry(InBUName)
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/16 08:29:36 $
%  $Revision: 1.10 $
%

OutErrorMsg = {'No errors'};
Filename = '';

try
if iscellstr(InBUName)
        InBUName = char(InBUName);
end

% TradeIdPrefix = 'TR-FY16-17-';
ObjDB = connect_to_database;
SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view'; 
TradeIdPrefix = char(fetch(ObjDB,SqlQuery));

ViewName = 'helper_expiry_processing_view';
[ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
if strcmpi(Data,'No Data')
    OutErrorMsg = {'No data found!'};
    return
end
ExpiryProcViewData = cell2dataset([ColNames;Data]);
SettlementDate = ExpiryProcViewData.settlement_date{1};

[ColNames,Data] = read_from_database('underlying_settle_value_table',1);
if strcmpi(Data,'No Data')
    OutErrorMsg = {'Prices are not updated in underlying_settle_value_table!'};
    return;
end
DBData = cell2dataset([ColNames;Data]);
UndId = DBData.underlying_id;
SettleValue = DBData.settle_value;

% SqlQuery = 'select trade_id from cloned_deal_ticket_table where transaction_date = (select max(transaction_date) from cloned_deal_ticket_table) order by trade_id DESC Limit 1';
% [~,DBTradeId] = read_from_database('cloned_deal_ticket_table',0,SqlQuery);
[OutErrorMsg,DBTradeId] = getLastTradeId(InBUName);
TempTradeId = str2num(strrep(char(DBTradeId),TradeIdPrefix,''));

SecUndId = unique(upper(ExpiryProcViewData.underlying1_id));

MarketPrice = zeros(size(SecUndId));
for iUndId = 1:length(SecUndId)
    IdxF = strcmpi(SecUndId{iUndId},UndId);    
    if isempty(find(IdxF))
        % TODO - replace the error message in a different variable and not
        % in filename
        OutErrorMsg = {['Underlying future price is missing for ',SecUndId{iUndId}]};
        return
    end
    MarketPrice(iUndId) = SettleValue(IdxF);
end

DBOptionData = []; DBFutureData = [];

ObjDB = connect_to_database;

PTI = ExpiryProcViewData.parent_trade_id;
CharPTI = convertCell2Char(PTI);
QueryID = ['(''',strrep(CharPTI,',',''','''),''')'];
TableName = ['cloned_deal_ticket_table_',InBUName];
SqlQuery = ['select parent_trade_id,active_lots from ',TableName,' where parent_trade_id in ',QueryID];
DBData = fetch(ObjDB,SqlQuery);

%%% Get settlement_date from valuation_date_table (23-12-2019)
settle_sql_query = 'select settlement_date from valuation_date_table';
settlement_date = fetch(ObjDB,settle_sql_query);

for i = 1:length(ExpiryProcViewData.trade_id)
       
    try
        IdxTID = strcmpi(ExpiryProcViewData.parent_trade_id{i},DBData(:,1));
        DBLots = cell2mat(DBData(IdxTID,2));
        if (length(DBLots) == 1) && (DBLots ==0)
            continue;
        end
    catch
        disp('Error while checking the active lots of parent trade id');
    end
%     try
%         SqlQuery = ['select distinct(active_lots) from cloned_deal_ticket_table where parent_trade_id = ''',ExpiryProcViewData.parent_trade_id{i},''''];
%         DBLots = cell2mat(fetch(ObjDB,SqlQuery));
%         if (length(DBLots) == 1) && (DBLots ==0)
%             continue;
%         end
%     catch
%         disp('Error while checking the active lots of parent trade id');
%     end
    
    IdxUndId = strcmpi(ExpiryProcViewData.underlying1_id{i},SecUndId);
    CurrMktPrice = MarketPrice(IdxUndId);
    
    %%% newly added logic (23-12-2019), to handle the Compo Option
    %%% Trades, get Market Price Value = today MktPrice * settle_value
    if strcmpi(ExpiryProcViewData.trade_group_type{i},{'CS_OPT'}) || strcmpi(ExpiryProcViewData.trade_group_type{i},{'CS_OTC'})
        currency = ExpiryProcViewData.other_remarks{i};
        %%% get spot From currency_spot_mapping_table
        Sql_query = ['select spot from currency_spot_mapping_table where currency = ''',char(currency),''' '];
        currency_spot = fetch(ObjDB,Sql_query); 
        
        %%% get settle_value From underlying_settle_value_table
        settle_value_query = ['select settle_value from underlying_settle_value_table where settlement_date= ''',char(settlement_date),''' and underlying_id = ''',char(currency_spot),''' '];
        settle_value = fetch(ObjDB,settle_value_query);
        
        %%% get currency from product_master_table
        [code_str1,code_str2] = strtok(ExpiryProcViewData.underlying1_id{i},' ');
        [code_str3,~] = strtok(code_str2,' ');
        product_code = strcat(code_str1,{' '},code_str3);
%         product_value_query = ['select invenio_product_code,currency,curr_mult_type from product_master_table where invenio_product_code= ''',char(product_code),''' '];
        product_value_query = ['select pnl_curr_exp_curr from product_master_table where invenio_product_code= ''',char(product_code),''' '];
        product_data = char(upper(fetch(ObjDB,product_value_query)));

        %%% Exact Market Price Value
        if strcmpi(product_data(1:3),'USD')
            CurrMktPrice = CurrMktPrice * cell2mat(settle_value);
        else
            CurrMktPrice = CurrMktPrice / cell2mat(settle_value);
        end
        
    end
  
    IsBarrierHitFromDB = ExpiryProcViewData.barrier1_hit(i);
    if isnan(IsBarrierHitFromDB)
        IsBarrierHitFromDB = 0;
    end
    if ~isempty(strfind(ExpiryProcViewData.derivative_type{i},'in')) % to handle only barriers of type "in"
        IsBarrierHit = ~IsBarrierHitFromDB;
    else
        IsBarrierHit = IsBarrierHitFromDB;
    end
    IsFutureNeeded =0; FutMarketAction= '';
    
    Lots = abs(ExpiryProcViewData.active_lots(i));
    if ~isempty(strfind(ExpiryProcViewData.derivative_type{i},'call')) % CALL option
        if ~IsBarrierHit && (ExpiryProcViewData.strike(i)  <= CurrMktPrice) % In The Money - Call
            if strcmpi(ExpiryProcViewData.market_action{i},'bought')
                MarketAction = 'exercised';
                FutMarketAction = 'bought';
                OptionLots = -(Lots);
                FutureLots = Lots;
            else
                MarketAction = 'gotassigned';
                FutMarketAction = 'sold';
                OptionLots = Lots;
                FutureLots = -(Lots);
            end
            if ~strcmpi(ExpiryProcViewData.netting_status(i),'fully_netted')
            IsFutureNeeded = 1;
            end
        else % Out of the money - Call
            if strcmpi(ExpiryProcViewData.market_action{i},'bought')
                MarketAction = 'abandoned';
                OptionLots = -(Lots);
            else
                MarketAction = 'gotabandoned';
                OptionLots = Lots;
            end
        end
    else % PUT Option
        if ~IsBarrierHit && (ExpiryProcViewData.strike(i)  >= CurrMktPrice) % In the Money - PUT option
            if strcmpi(ExpiryProcViewData.market_action{i},'bought')
                MarketAction = 'exercised';
                FutMarketAction = 'sold';
                OptionLots = -(Lots);
                FutureLots = -(Lots);
            else
                MarketAction = 'gotassigned';
                FutMarketAction = 'bought';
                OptionLots = Lots;
                FutureLots = Lots;
            end
            if ~strcmpi(ExpiryProcViewData.netting_status(i),'fully_netted')
            IsFutureNeeded = 1;
            end
        else % Out of the money - PUT option
            if strcmpi(ExpiryProcViewData.market_action{i},'bought')
                MarketAction = 'abandoned';
                OptionLots = -(Lots);
            else
                MarketAction = 'gotabandoned';
                OptionLots = Lots;
            end
        end
    end
    
%   TradeId = [ExpiryProcViewData.trade_id{i},'-EXP-OPT'];
    TempTradeId = TempTradeId + 1;
    TradeId = [TradeIdPrefix,num2str(TempTradeId)];
    Premium = 0;
    try
        GroupType = ExpiryProcViewData.trade_group_type(i);        
    catch
        GroupType = {'NULL'};
    end
%     RowOptionData = [ExpiryProcViewData.subportfolio(i),ExpiryProcViewData.trade_id(i),cellstr(TradeId),...
%         SettlementDate,ExpiryProcViewData.security_id(i),cellstr(MarketAction),OptionLots,...
%         Premium,ExpiryProcViewData.counterparty_parent(i), ExpiryProcViewData.counterparty_bu(i),...
%         ExpiryProcViewData.counterparty_entity(i),ExpiryProcViewData.execution_broker(i),ExpiryProcViewData.clearing_broker(i),...
%         ExpiryProcViewData.reconed_with_clearer(i),ExpiryProcViewData.reconed_completed_by(i),...
%         ExpiryProcViewData.recon_datetime_stamp(i),ExpiryProcViewData.contract_number(i),...
%         0,{'System'},{'NULL'},{'System generated expiry'},ExpiryProcViewData.exe_type(i),...
%         ExpiryProcViewData.brokerage_rate_type(i),ExpiryProcViewData.spcl_rate(i),...
%         ExpiryProcViewData.other_remarks(i),ExpiryProcViewData.instrument(i),{'NULL'},{'NULL'},0,OptionLots];
RowOptionData = [cellstr(TradeId),ExpiryProcViewData.trade_id(i),...
        SettlementDate,ExpiryProcViewData.subportfolio(i),ExpiryProcViewData.instrument(i),GroupType,...
        ExpiryProcViewData.security_id(i),cellstr(MarketAction),OptionLots,...
        Premium,ExpiryProcViewData.counterparty_parent(i), ExpiryProcViewData.counterparty_bu(i),...
        ExpiryProcViewData.counterparty_entity(i),ExpiryProcViewData.execution_broker(i),ExpiryProcViewData.clearing_broker(i),...
        ExpiryProcViewData.reconed_with_clearer(i),ExpiryProcViewData.reconed_completed_by(i),...
        ExpiryProcViewData.recon_datetimestamp(i),ExpiryProcViewData.contract_number(i),{'NULL'},{'NULL'},0,OptionLots,...
        {'NULL'},{'NULL'},{'NULL'},{'dead'},ExpiryProcViewData.exe_type(i),ExpiryProcViewData.brokerage_rate_type(i),ExpiryProcViewData.spcl_rate(i),ExpiryProcViewData.other_remarks(i),...
        0,{'System'},{'NULL'},{'System generated expiry'},...      
        {'NULL'},{'NULL'},{'NULL'}];
    DBOptionData = [DBOptionData;RowOptionData];
    if IsFutureNeeded 
%       TempTradeId = TempTradeId + 1;
%       TradeId = [TradeIdPrefix,num2str(TempTradeId)];
        FutPremium = ExpiryProcViewData.strike(i);
        Instrument = strrep(ExpiryProcViewData.instrument{i},'-O','');
        
        %%% newly added logic (23-12-2019), to handle the Compo Option
        %%% Trades, Chage the security_id and derivative_type
        if strcmpi(ExpiryProcViewData.trade_group_type{i},{'CS_OTC'}) || strcmpi(ExpiryProcViewData.trade_group_type{i},{'CS_OPT'})
            derivative_type = {'CS_FUT'};
            security_Str1 = strtok(ExpiryProcViewData.security_id(i),'.');
            [~,Str2] = strtok(ExpiryProcViewData.underlying1_id(i),' ');
            [~,security_Str2] = strtok(Str2,' ');
            security_id = strcat(security_Str1,{'.'},strtrim(security_Str2),{' '},ExpiryProcViewData.other_remarks{i});
        else
            derivative_type = {'FUT'};
            security_id = ExpiryProcViewData.underlying1_id(i);
        end
%         RowFutureData = [ExpiryProcViewData.subportfolio(i),cellstr(TradeId),cellstr(TradeId),...
%             SettlementDate,ExpiryProcViewData.underlying1_id(i),cellstr(FutMarketAction),FutureLots,...
%             FutPremium,ExpiryProcViewData.counterparty_parent(i), ExpiryProcViewData.counterparty_bu(i),...
%             ExpiryProcViewData.counterparty_entity(i),ExpiryProcViewData.execution_broker(i),ExpiryProcViewData.clearing_broker(i),...
%             ExpiryProcViewData.reconed_with_clearer(i),ExpiryProcViewData.reconed_completed_by(i),...
%             ExpiryProcViewData.recon_datetime_stamp(i),ExpiryProcViewData.contract_number(i),...
%             0,{'System'},{'NULL'},{'System generated expiry'},ExpiryProcViewData.exe_type(i),...
%             ExpiryProcViewData.brokerage_rate_type(i),ExpiryProcViewData.spcl_rate(i),...
%             ExpiryProcViewData.other_remarks(i),Instrument,{'NULL'},{'NULL'},0,FutureLots];
 RowFutureData = [cellstr(TradeId),cellstr(TradeId),SettlementDate,ExpiryProcViewData.subportfolio(i),Instrument,derivative_type,...
            security_id,cellstr(FutMarketAction),FutureLots,...
            FutPremium,ExpiryProcViewData.counterparty_parent(i), ExpiryProcViewData.counterparty_bu(i),...
            ExpiryProcViewData.counterparty_entity(i),ExpiryProcViewData.execution_broker(i),ExpiryProcViewData.clearing_broker(i),...
            ExpiryProcViewData.reconed_with_clearer(i),ExpiryProcViewData.reconed_completed_by(i),...
            ExpiryProcViewData.recon_datetimestamp(i),ExpiryProcViewData.contract_number(i),{'NULL'},{'NULL'},0,FutureLots,...
            {'NULL'},{'NULL'},{'NULL'},{'live'},ExpiryProcViewData.exe_type(i),ExpiryProcViewData.brokerage_rate_type(i),ExpiryProcViewData.spcl_rate(i),ExpiryProcViewData.other_remarks(i),...
            0,{'System'},{'NULL'},{'System generated expiry'}...
            {'NULL'},{'NULL'},{'NULL'}];
        DBFutureData = [DBFutureData;RowFutureData]; %#ok<*AGROW>
    end
    
    
end

PosParentTradeId = 2; PosTradeId = 1;
NumFutures = size(DBFutureData,1);
for iFut = 1:NumFutures
    TempTradeId = TempTradeId + 1;
    TradeId = [TradeIdPrefix,num2str(TempTradeId)];
    DBFutureData{iFut,PosTradeId} = TradeId;
    DBFutureData{iFut,PosParentTradeId} = TradeId;
end

try
DBFutureData = sortrows(DBFutureData,PosTradeId);
DBOptionData = sortrows(DBOptionData,PosTradeId);
catch
end
% OutExpiryData = [OutExpiryData;DBOptionData;DBFutureData];

% DBHeader = {'SubPortfolioID','ParentTradeID','TradeID','TransactionDate',...
%     'SecurityID','MarketAction','original_lots','Premium','CounterParty_Parent',...
%     'CounterParty_BU','CounterParty_Entity','ExecutionBroker','ClearingBroker',...
%     'ReconedWithClearer','ReconedCompletedBy','ReconDateTimeStamp','contract number',...
%     'version number','version created by','version comments','version timestamp',...
%     'exe_type','brokerage_rate_type','spcl_rate','other_remarks','instrument',...
%     'opt_periodicity','subcontract_number','is_netting_done','lots_to_be_netted'};
DBHeader = {'trade_id','parent_trade_id','transaction_date','subportfolio','instrument',...
    'trade_group_type','security_id','market_action','original_lots','premium',...
    'counterparty_parent','counterparty_bu','counterparty_entity','execution_broker',...
    'clearing_broker','reconed_with_clearer','reconed_completed_by','recon_datetimestamp',...
    'contract_number','subcontract_number','opt_periodicity','is_netting_done','lots_to_be_netted',...
    'date_to_be_fixed','trader_name','traded_timestamp','deal_status','exe_type','brokerage_rate_type',...
    'spcl_rate','other_remarks','version_no','version_created_by','version_timestamp','version_comments','ops_unique','broker_lots','ops_action'};
Filename  = getXLSFilename(['DBSystem_OptionExpiry_',InBUName]);

xlswrite(Filename,[DBHeader;DBOptionData],'Options');
xlswrite(Filename,[DBHeader;DBFutureData],'Futures');
OutXLSFileName = fullfile(pwd,Filename);
try
xls_delete_sheets(OutXLSFileName);
catch
end

try
    SharedFolder = get_reports_foldername;
    DestinationFile = fullfile(SharedFolder,Filename);
    movefile(Filename,DestinationFile);   
catch ME
    OutErrorMsg = cellstr(ME.message);
end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    Filename = '';
end
end