function [OutErrorMsg,PnlReport] = generate_previous_accounting_nc_monthwise_tr_pnl_report(InBUName)
try
    OutErrorMsg = {'No errors'};
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    ViewName = 'helper_previous_accounting_tr_pnl_dealprofit_monthwise_view';
%     [ColNames,Data] = read_from_database(ViewName,0);
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    TRData = cell2dataset([ColNames;Data]);
    
    PnlReport = getXLSFilename('Previous_Settle_Accounting_Pnl_Report_NC_Monthwise');
    
    Subportfolios = unique(TRData.subportfolio);
    NumPortfolios = length(Subportfolios);
    
    Header = {'ValueDate','Subportfolio','ContractMonth','delta','gamma','theta','vega',' Mtm - ABV-Usd ',' Mtm Prv Settle-Usd ',' Daily P&L ','Gamma P&L',' Vega P&L ','Deal Profit New Settle','Deal Profit Prv Settle'};
    
    HolDates = {'2016-01-01';'2016-12-26';'2017-01-02';'2017-01-16';'2017-04-14';'2017-12-25';'2018-03-30'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    SqlQuery = 'select value_date from valuation_date_table';
    [~,Data] = read_from_database('valuation_date_table',0,SqlQuery);
    TodayDate = char(Data);
    
    VBusDays = busdays(datenum('2018-01-01'), datenum(TodayDate),'daily',HolidayVec);
    VDates = cellstr(datestr(VBusDays,'yyyy-mm-dd'));
    SDates = cellstr(datestr(busdate(VBusDays,-1,HolidayVec),'yyyy-mm-dd'));
    
    for iPF = 1:NumPortfolios
        TempPF = regexprep(Subportfolios{iPF},'[^a-zA-Z0-9]','_');
        
        IdxPF = strcmpi(Subportfolios{iPF},TRData.subportfolio);
        ContractMonths = unique(TRData.contract_month(IdxPF));
        
        OutData = [];
        
        for iCM = 1:length(ContractMonths)
            IdxPortfolio = logical(zeros(size(IdxPF)));  %#ok<*LOGL>
            IdxPortfolio(IdxPF) =  strcmpi(ContractMonths{iCM},TRData.contract_month(IdxPF));
            
            IdxTR1 = logical(zeros(size(IdxPortfolio)));
            IdxTR2 = logical(zeros(size(IdxPortfolio)));
            IdxTR3 = logical(zeros(size(IdxPortfolio)));
            IdxTR4 = logical(zeros(size(IdxPortfolio)));
            
            % Trades which are existing in the portfolio before the day starts
            IdxTR1(IdxPortfolio) = strcmpi('NONFTD',TRData.trade_classification(IdxPortfolio));
            
            % New option trades done during the day; and not marked as EXSs
            IdxTR2(IdxPortfolio) = strcmpi('FTD',TRData.trade_classification(IdxPortfolio)) & ...
                ~strcmpi('FUT',TRData.group_type(IdxPortfolio)) & ...
                ~strcmpi('EXS',TRData.fut_hedge_status(IdxPortfolio));
            
            % Futures done as hedges against these trades and marked as H and XH in PNP
            IdxTR3(IdxPortfolio) = strcmpi('FTD',TRData.trade_classification(IdxPortfolio)) & ...
                strcmpi('FUT',TRData.group_type(IdxPortfolio)) & ...
                strcmpi('H',TRData.fut_hedge_status(IdxPortfolio));
            
            % Other futures trades
            IdxTR4(IdxPortfolio) = strcmpi('FTD',TRData.trade_classification(IdxPortfolio)) & ...
                ((strcmpi('FUT',TRData.group_type(IdxPortfolio)) & strcmpi('NH',TRData.fut_hedge_status(IdxPortfolio))) | ...
                (~strcmpi('FUT',TRData.group_type(IdxPortfolio)) & strcmpi('EXS',TRData.fut_hedge_status(IdxPortfolio))) | ...
                (strcmpi('FUT',TRData.group_type(IdxPortfolio)) & strcmpi('EXS',TRData.fut_hedge_status(IdxPortfolio))));
            
            IdxPFTrades23 = IdxTR2 | IdxTR3;
            IdxPFTrades14 = IdxTR1 | IdxTR4;
            
            Delta = zeros(size(IdxPortfolio));
            Gamma = zeros(size(IdxPortfolio));
            Theta = zeros(size(IdxPortfolio));
            Vega = zeros(size(IdxPortfolio));
            M2M = zeros(size(IdxPortfolio));
            PrevSettleM2M = zeros(size(IdxPortfolio));
            
            Delta(IdxPortfolio) = TRData.delta(IdxPortfolio);
            Gamma(IdxPortfolio) = TRData.gamma(IdxPortfolio);
            Theta(IdxPortfolio) = TRData.theta(IdxPortfolio);
            Vega(IdxPortfolio)  = TRData.vega(IdxPortfolio);
            M2M(IdxPortfolio) = TRData.mtm_abv_usd(IdxPortfolio);
            PrevSettleM2M(IdxPortfolio) = TRData.mtm_prv_settle_usd(IdxPortfolio);
            
            
            %     PortfolioData = [ValueDate,...
            %         TRData.subportfolio(IdxPortfolio),....
            %         num2cell(TRData.delta(IdxPortfolio)),...
            %         num2cell(TRData.gamma(IdxPortfolio)),...
            %         num2cell(TRData.theta(IdxPortfolio)),...
            %         num2cell(TRData.vega(IdxPortfolio)),...
            %         num2cell(M2M),...
            %         num2cell(PrevSettleM2M)];
            
            %     PortfolioData = sortrows(PortfolioData,1);
            
            DealProfitPrevSettle = zeros(size(M2M,1),1);
            DealProfitNewSettle  = zeros(size(M2M,1),1);
            GammaPnLComp = zeros(size(M2M,1),1);
            
            DealProfitPrevSettle(IdxPFTrades23) = PrevSettleM2M(IdxPFTrades23);
            DealProfitNewSettle(IdxPFTrades23)  = M2M(IdxPFTrades23);
            GammaPnLComp(IdxPFTrades14) = PrevSettleM2M(IdxPFTrades14);
            
            
            try
                
                for i = 1:length(VBusDays)
                    IdxToday = logical(zeros(size(IdxPortfolio)));
                    IdxYest  = logical(zeros(size(IdxPortfolio)));
                    
                    IdxToday(IdxPortfolio) = strcmpi(VDates{i},TRData.value_date(IdxPortfolio));
                    IdxYest(IdxPortfolio)   = strcmpi(SDates{i},TRData.value_date(IdxPortfolio));
                    
                    TodaysM2M  = sum(M2M(IdxToday));
                    YestM2M    = sum(M2M(IdxYest));
                    PrevSettleM2MToday = sum(PrevSettleM2M(IdxToday));
                    TempGammaPnL = sum(GammaPnLComp(IdxToday));
                    DPNewSettle = sum(DealProfitNewSettle(IdxToday));
                    DPPrevSettle = sum(DealProfitPrevSettle(IdxToday));
                    
                    if isempty(TodaysM2M) && isempty(YestM2M) && isempty(PrevSettleM2MToday)
                        continue;
                    end
                    
                    if isempty(TodaysM2M) && ~isempty(YestM2M)
                        continue;
                    end
                    if isempty(TodaysM2M) && ~isempty(PrevSettleM2MToday)
                        continue;
                    end
                    if isempty(PrevSettleM2MToday) && ~isempty(YestM2M)
                        PrevSettleM2MToday = 0;
                    end
                    if  isempty(PrevSettleM2MToday) && ~isempty(TodaysM2M)
                        PrevSettleM2MToday = 0;
                    end
                    if isempty(YestM2M) && ~isempty(PrevSettleM2MToday)
                        YestM2M = 0;
                    end
                    if isempty(YestM2M) && ~isempty(TodaysM2M)
                        YestM2M = 0;
                    end
                    
                    if i~=1
                        DailyPnL = TodaysM2M - YestM2M;
                        GammaPnL = TempGammaPnL - YestM2M;
                    else
                        DailyPnL = TodaysM2M;
                        GammaPnL = TempGammaPnL;
                    end
                    
                    VegaPnL  = DailyPnL - GammaPnL - DPNewSettle;
                    
                    PortfolioData = [VDates(i),Subportfolios(iPF),ContractMonths{iCM},...
                        num2cell(sum(Delta(IdxToday))),num2cell(sum(Gamma(IdxToday))),...
                        num2cell(sum(Theta(IdxToday)) * (7/5)),num2cell(sum(Vega(IdxToday))),...
                        num2cell(sum(M2M(IdxToday))),num2cell(sum(PrevSettleM2M(IdxToday))),...
                        num2cell(DailyPnL),num2cell(GammaPnL),num2cell(VegaPnL),num2cell(DPNewSettle),num2cell(DPPrevSettle)];
                    OutData = [OutData;PortfolioData];
                end
                
                %     OutData = [PortfolioData,num2cell(DailyPnL),num2cell(GammaPnL),num2cell(VegaPnL),num2cell(DPNewSettle),num2cell(DPPrevSettle)];
            catch
                disp(TempPF);
                continue;
            end
        end
        try
            OutData = sortrows(OutData,[1 3]);
        catch
            disp('Error while sorting rows: some NaN values are found');
        end
        xlswrite(PnlReport,[Header;OutData],TempPF);
        
    end
    try
        xls_delete_sheets(fullfile(pwd,PnlReport));
    catch
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    PnlReport = '';
end