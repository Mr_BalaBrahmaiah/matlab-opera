function [CPFilename,EODFilename] = generate_counterparty_eod_report
%  Generate the counterparty report and EOD reports
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:52 $
%  $Revision: 1.7 $
%

%% generate the counterparty report
ViewName = 'helper_counterparty_report_view';
[SettleColNames,Data] = read_from_database(ViewName,0);

% remove the internal counterparties
PosCP = find(strcmpi('counterparty_parent',SettleColNames));
IdxIntCP = strncmpi(Data(:,PosCP),'MM',2) | strncmpi(Data(:,PosCP),'RMS',3);
Data(IdxIntCP,:) = [];

% remove the derivative-type 'fx' 
PosOptType = find(strcmpi('derivative_type',SettleColNames));
IdxFx = strncmpi(Data(:,PosOptType),'fx',2);
Data(IdxFx,:) = [];

PosValueDate = find(strcmpi('value_date',SettleColNames));
PosMaturity  = find(strcmpi('maturity_date',SettleColNames));
ValueDate    = datenum(Data(:,PosValueDate),'yyyy-mm-dd'); %#ok<*FNDSB>
TempMaturity = Data(:,PosMaturity);
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');

SettleData = cell2dataset([SettleColNames;Data]);

clear Data;

ValuationDate = SettleData.value_date{1};

IdxLive = ValueDate <= MaturityDate;
IdxDead = ValueDate > MaturityDate;
IdxFut = strcmpi('future',SettleData.derivative_type);
IdxOpt = ~IdxFut;

Counterparty = unique(upper(SettleData.counterparty_parent));

CPOutData =[];

for iS = 1:length(Counterparty)
    CP = Counterparty(iS);
    IdxSettle  = strcmpi(CP,SettleData.counterparty_parent);
    
    ContractNo = unique(upper(SettleData.contract_number(IdxSettle)));
    ContractNo(cellfun(@isempty,ContractNo)) = [];
    for iCNo = 1:length(ContractNo)
        IdxSettleCM  = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM); %#ok<*LOGL>
        
        IdxDeadFut = zeros(size(IdxSettle)); IdxDeadFut = logical(IdxDeadFut);
        IdxLiveFut = zeros(size(IdxSettle)); IdxLiveFut = logical(IdxLiveFut);
        IdxLiveOpt = zeros(size(IdxSettle)); IdxLiveOpt = logical(IdxLiveOpt);
        IdxDeadOpt = zeros(size(IdxSettle)); IdxDeadOpt = logical(IdxDeadOpt);
               
        IdxSettleCM(IdxSettle)   = strcmpi(ContractNo{iCNo},SettleData.contract_number(IdxSettle));
        
        IdxDeadFut(IdxSettleCM) = IdxFut(IdxSettleCM) & IdxDead(IdxSettleCM);
        IdxLiveFut(IdxSettleCM) = IdxFut(IdxSettleCM) & IdxLive(IdxSettleCM);
        
        IdxLiveOpt(IdxSettleCM) = IdxOpt(IdxSettleCM) & IdxLive(IdxSettleCM);
        IdxDeadOpt(IdxSettleCM) = IdxOpt(IdxSettleCM) & IdxDead(IdxSettleCM);
        
        IdxOptAll = IdxLiveOpt | IdxDeadOpt;
        
        Subportfolio  = unique(upper(SettleData.subportfolio(IdxSettleCM)));
        LotMult  = unique(SettleData.lot_mult1(IdxSettleCM));
        CurrMult = unique(SettleData.curr_mult1(IdxSettleCM));
        Mult = LotMult * CurrMult;
        
        EquivLots = sum(SettleData.active_lots(IdxLiveFut));
        RealisedFut = sum(SettleData.mtm_usd(IdxDeadFut));
        UnrealisedFut = sum(SettleData.mtm_usd(IdxLiveFut));
        OptPremPay  = sum(SettleData.premium(IdxLiveOpt) .* SettleData.original_lots(IdxLiveOpt) .* Mult);
        OptMktValue = sum(SettleData.settle_price(IdxLiveOpt) .* SettleData.active_lots(IdxLiveOpt) .* Mult);        
        OptionM2M = sum(SettleData.mtm_usd(IdxOptAll));
        
        CPRowData = [Subportfolio,Counterparty(iS),ContractNo(iCNo),EquivLots,...
            RealisedFut,UnrealisedFut,OptPremPay,OptMktValue,OptionM2M];
        CPOutData = [CPOutData;CPRowData];       
       
    end
    
end

CPHeader = {'Subportfolio','Counterparty','ContractNo','Equivalent Lots',...
    'Realised Futures','Unrealised Futures','OFUT Premium Rec Pay','MV of Options','Option M2M'};
% CPFilename = ['CounterpartyReport_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
% customcsvwrite(CPFilename,CPOutData,CPHeader);

CPFilename = getXLSFilename('CounterpartyReport');
xlswrite(CPFilename,[CPHeader;CPOutData]);

clear SettleData;

%% generate the EOD report
ViewName = 'helper_daily_eod_report_view';
[SettleColNames,Data] = read_from_database(ViewName,0);

% remove the internal counterparties
PosCP = find(strcmpi('counterparty_parent',SettleColNames));
IdxIntCP = strncmpi(Data(:,PosCP),'MM',2) | strncmpi(Data(:,PosCP),'RMS',3);
Data(IdxIntCP,:) = [];

% remove the derivative-type 'fx' 
PosOptType = find(strcmpi('derivative_type',SettleColNames));
IdxFx = strncmpi(Data(:,PosOptType),'fx',2);
Data(IdxFx,:) = [];

IdxFuture = strcmpi('future',Data(:,PosOptType));
Data(IdxFuture,:) = [];

PosValueDate = find(strcmpi('value_date',SettleColNames));
PosMaturity  = find(strcmpi('maturity_date',SettleColNames));
ValueDate    = datenum(Data(:,PosValueDate),'yyyy-mm-dd'); %#ok<*FNDSB>
TempMaturity = Data(:,PosMaturity);
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');

SettleData = cell2dataset([SettleColNames;Data]);

clear Data;

ValuationDate = SettleData.value_date{1};

IdxLive = ValueDate <= MaturityDate;
IdxDead = ValueDate > MaturityDate;

Counterparty = unique(upper(SettleData.counterparty_parent));

EODOutData = [];
IdxSettleSource = strcmpi('otc',SettleData.settlement_source);

for iS = 1:length(Counterparty)
    CP = Counterparty(iS);
    IdxSettle  = strcmpi(CP,SettleData.counterparty_parent);
    
    ContractNo = unique(upper(SettleData.contract_number(IdxSettle)));
    ContractNo(cellfun(@isempty,ContractNo)) = [];
    for iCNo = 1:length(ContractNo)
        IdxSettleCM  = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM); %#ok<*LOGL>
        
        IdxLiveOpt = zeros(size(IdxSettle)); IdxLiveOpt = logical(IdxLiveOpt);
        IdxDeadOpt = zeros(size(IdxSettle)); IdxDeadOpt = logical(IdxDeadOpt);
               
        IdxSettleCM(IdxSettle)   = strcmpi(ContractNo{iCNo},SettleData.contract_number(IdxSettle));
                       
        IdxLiveOpt(IdxSettleCM) = IdxLive(IdxSettleCM);
        IdxDeadOpt(IdxSettleCM) = IdxDead(IdxSettleCM);
        
        IdxOptAll = IdxLiveOpt | IdxDeadOpt;
              
        Subportfolio  = unique(upper(SettleData.subportfolio(IdxSettleCM)));
        % for EOD dump        
        EOD_Subportfolio = unique(upper(SettleData.subportfolio(IdxOptAll)));
        if ~isempty(EOD_Subportfolio)       
            TradeType = 'OTC';
            IdxDeadOpt = IdxDeadOpt & IdxSettleSource;
            IdxLiveOpt = IdxLiveOpt & IdxSettleSource;
                       
            Currency = unique(SettleData.currency(IdxOptAll));
            if length(Currency) > 1
                Currency = Currency(1);
            end
            Instrument = unique(upper(SettleData.instrument(IdxOptAll)));
            if length(Instrument) > 1
                Instrument = Instrument(1);
            end
            Realised_USD = sum(SettleData.mtm_usd(IdxDeadOpt));
            Realised_NC = sum(SettleData.mtm_nc(IdxDeadOpt));
            Unrealised_USD = sum(SettleData.mtm_usd(IdxLiveOpt));
            Unrealised_NC = sum(SettleData.mtm_nc(IdxLiveOpt));
            TotalNC = Realised_NC + Unrealised_NC;
            TotalUSD = Realised_USD + Unrealised_USD;
            EODRowData = [Instrument,Subportfolio,Counterparty(iS),ContractNo(iCNo),TradeType,Currency,...
                Realised_NC,Realised_USD,Unrealised_NC,Unrealised_USD,TotalNC,TotalUSD];
            EODOutData = [EODOutData;EODRowData]; %#ok<*AGROW>
        end
    end
    
end

EODHeader = {'Instrument','Subportfolio','Counterparty','ContractNo','TradeType','Currency',...
    'Realised NC', 'Realised USD','Unrealised NC','Unrealised USD','Total NC','Total USD'};
% EODFilename = ['EODReport_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
% customcsvwrite(EODFilename,EODOutData,EODHeader);

EODFilename = getXLSFilename('EODReport');
xlswrite(EODFilename,[EODHeader;EODOutData]);

