function Opera_OTC_Filler_Central_Books()

try
    theCell = {'COT','COC','COF','GRN','RBR','SGR','PLM','DRY'};
    [User_SelectBU, theChosenIDX] = uicellect(theCell,'Multi', 0);
    
    if(isempty(theChosenIDX))
        warndlg('Please Selectt Anyone of the Business Unit');
        
    else
        hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');
        
        InBUName = char(User_SelectBU);
        
        Main_OTC_Filler_Central_Books(InBUName) ; %% Main_Function
        %  [OutErrorMsg,OutXLSFileName] =  Main_OTC_Filler_Central_Books_FrontEnd(InBUName);
        %  [OutErrorMsg,OutXLSFileName] =  generate_otcfills_trdump(InBUName,InXLSFilename);%% FrontEnd
        
        waitbar(1,hWaitbar,[InBUName ,' OTC Filler Finished']);
        
        msgbox('Process Finished','Finished');
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end
