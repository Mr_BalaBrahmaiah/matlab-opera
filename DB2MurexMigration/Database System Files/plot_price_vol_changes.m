% function plot_price_vol_changes


ViewName = 'helper_calculate_price_vol_change_view';
[ColNames,Data] = read_from_database(ViewName,0);
PriceVolData = cell2dataset([ColNames;Data]);


BBGUnderlyingId = unique(PriceVolData.bbg_underlying_id);
SettlementDate = datetime(PriceVolData.settlement_date,'ConvertFrom','yyyy-MM-dd','format','dd-MMM');

    for iC = 1:length(BBGUnderlyingId)
        IdxContMonth = strcmpi(BBGUnderlyingId{iC},PriceVolData.bbg_underlying_id);
        hFig(iC) = figure('units','normalized','outerposition',[0 0 1 1]);          
        plot(SettlementDate(IdxContMonth),PriceVolData.vol(IdxContMonth),'ro-');    
        hold on;
        plot(SettlementDate(IdxContMonth),PriceVolData.price(IdxContMonth),'bx-');  
        datetick('x','dd-mmm');
        grid on;
        set(gca,'XMinorGrid','on');
        LegendEntries = {'Vol Data(%)','Price Data'};
        legend(LegendEntries);
        
        xlabel('Settlement Date');
        ylabel('Price/Vol Data');
        title(BBGUnderlyingId{iC});
        
        set(gcf,'Name',BBGUnderlyingId{iC},'NumberTitle','off','MenuBar','none');
%         saveas(gcf,BBGUnderlyingId{iC} , 'bmp');
    end    
    
