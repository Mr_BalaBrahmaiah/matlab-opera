tic
SummmaryParameters = {'subportfolio','counterparty','contract_month','derivative_type'};
PricingType = 'settle';
InputScenarios = {'off',3,40,[]; 'off',3,1.00,[];'off',3,20,[]};
OutputFields = {'delta','gamma','vega','theta','m2m_usd','m2m_nc'};
InputFilterValues = {'subportfolio',{'all'}};

javaaddpath('mysql-connector-java-5.1.25-bin.jar');
objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.71');

BusDates = busdays(datenum('2014-11-01'), datenum('2014-12-03'),'daily','2014-12-25');
VDates = cellstr(datestr(BusDates,'yyyy-mm-dd'));

for i = 1:length(BusDates)

SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},'''']

curs = exec(objDB,SqlQuery);

OutFilename = generate_sensitivity_report(InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
NewFileName = ['ComplianceReport_AccSurface_',VDates{i},'.xlsx'];

NewFileName = fullfile('E:\Program Files\MATLAB\R2013a\work\Olam Analytics Library\Database System Files\Compliance Reports',NewFileName);

movefile(OutFilename,NewFileName);

end
toc