tic
F(1) = parfeval(MyPool,@test_datastore,1);
F(2)  = parfeval(MyPool,@test_sql_script,1);
F(3) = parfeval(MyPool,@test_exec_cursor,1);

N = 3; % since 3 diff executions are done

% Build a waitbar to track progress
h = waitbar(0,'Waiting for FevalFutures to complete...');
results = cell(1,N);

for idx = 1:N
    [completedIdx,thisResult] = fetchNext(F);
    % store the result
    results{completedIdx} = thisResult;
    % update waitbar
    waitbar(idx/N,h,sprintf(['Latest result completed from function: ', char(F(completedIdx).Function)]));
end
delete(h)
toc