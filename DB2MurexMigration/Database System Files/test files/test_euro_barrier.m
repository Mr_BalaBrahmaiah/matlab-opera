
%% Inputs for Euro barriers
AssetPrice            = (90:0.01:120)';
TickSize              = repmat(0.05,size(AssetPrice));
RFR                   = zeros(size(AssetPrice));
CostOfCarry           = zeros(size(AssetPrice));
Rebate                = zeros(size(AssetPrice));
StrikeVol             = repmat(0.00001,size(AssetPrice));
BarrierVol            = repmat(0.00001,size(AssetPrice));
TimeToMaturity        = zeros(size(AssetPrice));
Lots                  = ones(size(AssetPrice));
Lot_Mult              = ones(size(AssetPrice));
Curr_Mult             = ones(size(AssetPrice));
YearToDay_Mult        = ones(size(AssetPrice)) .* 365;
Currency              = repmat(cellstr('USD'),size(AssetPrice));
ExchangeATMConvention = repmat(cellstr('calendar'),size(AssetPrice));
IsExpiryProcessed     = zeros(size(AssetPrice));

%% Euro DI Call
OptionType            = repmat(cellstr('euro_down_in_call'),size(AssetPrice));
% invoke Euro DI call for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DI Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down In Call - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro DI call for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DI Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down In Call - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro DI Put
OptionType            = repmat(cellstr('euro_down_in_put'),size(AssetPrice));
% invoke Euro DI Put for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DI Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down In Put - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro DI Put for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DI Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down In Put - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro DO Call
OptionType            = repmat(cellstr('euro_down_out_call'),size(AssetPrice));
% invoke Euro DO call for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DO Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down Out Call - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro DO call for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DO Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down Out Call - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro DO Put
OptionType            = repmat(cellstr('euro_down_out_put'),size(AssetPrice));
% invoke Euro DO Put for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DO Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down Out Put - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro DO Put for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO DO Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Down Out Put - B < K , T<=0, Barrier = 100, Strike = 110');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Euro UI Call
OptionType            = repmat(cellstr('euro_up_in_call'),size(AssetPrice));
% invoke Euro UI call for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UI Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up In Call - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro UI call for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UI Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up In Call - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro UI Put
OptionType            = repmat(cellstr('euro_up_in_put'),size(AssetPrice));
% invoke Euro UI Put for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UI Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up In Put - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro UI Put for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UI Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up In Put - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro UO Call
OptionType            = repmat(cellstr('euro_up_out_call'),size(AssetPrice));
% invoke Euro UO call for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UO Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up Out Call - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro UO call for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UO Call
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up Out Call - B < K , T<=0, Barrier = 100, Strike = 110');

%% Euro UO Put
OptionType            = repmat(cellstr('euro_up_out_put'),size(AssetPrice));
% invoke Euro UO Put for B > K
Strike                = repmat(100,size(AssetPrice));
BarrierLevel          = repmat(110,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UO Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up Out Put - B > K , T<=0, Barrier = 110, Strike = 100');

% invoke Euro UO Put for B < K
Strike                = repmat(110,size(AssetPrice));
BarrierLevel          = repmat(100,size(AssetPrice));
obj = EuropeanOptionEuropeanBarrier(AssetPrice, Strike, BarrierLevel, Rebate,  RFR, CostOfCarry, StrikeVol,BarrierVol, ...
TimeToMaturity, OptionType, Lots, Lot_Mult, Curr_Mult, YearToDay_Mult, ExchangeATMConvention, IsExpiryProcessed,Currency,TickSize);
% figure,plot EURO UO Put
figure,plot(AssetPrice,obj.getprice); grid on;
xlabel('AssetPrice'); ylabel('Price');
title('Euro Up Out Put - B < K , T<=0, Barrier = 100, Strike = 110');