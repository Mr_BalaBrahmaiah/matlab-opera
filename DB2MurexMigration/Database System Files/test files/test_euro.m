% assignin('base','ATM1_Vol',ATM1_Vol)
% assignin('base','Call1_Skew',Call1_Skew)
% assignin('base','Put1_Skew',Put1_Skew)
% assignin('base','Skew1_Step',Skew1_Step)
% assignin('base','Strike',Strike)
% assignin('base','Barrier_1_Level',Barrier_1_Level)
% assignin('base','Barrier1_Rebate',Barrier1_Rebate)
% assignin('base','RiskFreeRate',RiskFreeRate)
% assignin('base','CostOfCarry',CostOfCarry)
% assignin('base','TTM',TTM)
% assignin('base','YearToDay_Mult',YearToDay_Mult)
% assignin('base','Opt_DayCount',Opt_DayCount)
% assignin('base','IsExpiryProcessed',IsExpiryProcessed)
% assignin('base','Currency',Currency)
% assignin('base','TickSize',TickSize)
% assignin('base','AssetPrice_1',AssetPrice_1)



StrikeVol =  floatvolcurvelinear(AssetPrice_1, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Strike);
            
            BarrierVol =  floatvolcurvelinear(AssetPrice_1, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Barrier_1_Level);
          
            obj = EuropeanOptionEuropeanBarrier(AssetPrice_1, Strike, Barrier_1_Level,Barrier1_Rebate,...
                RiskFreeRate, CostOfCarry,StrikeVol,BarrierVol, TTM,...
                OptionTypes, Lots, Lot_Mult, Curr_Mult, ...
                YearToDay_Mult,Opt_DayCount,  IsExpiryProcessed,Currency,TickSize);   
            OutPrice = obj.getprice()
            
            P11_Settle_Price = AssetPrice_1 - (TickSize.* CONST_SKEW_STEP_MULT);            
            StrikeVol1 =  floatvolcurvelinear(P11_Settle_Price, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Strike);            
            BarrierVol1 =  floatvolcurvelinear(P11_Settle_Price, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Barrier_1_Level);            
            obj1 = EuropeanOptionEuropeanBarrier(P11_Settle_Price, Strike, Barrier_1_Level,Barrier1_Rebate,...
                RiskFreeRate, CostOfCarry,StrikeVol1,BarrierVol1, TTM,...
                OptionTypes, Lots, Lot_Mult, Curr_Mult, ...
                YearToDay_Mult,Opt_DayCount,  IsExpiryProcessed,Currency,TickSize);   
            Price_1 =  obj1.getprice();
           
            P12_Settle_Price = AssetPrice_1 + (TickSize.* CONST_SKEW_STEP_MULT);            
            StrikeVol2 =  floatvolcurvelinear(P12_Settle_Price, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Strike);
            BarrierVol2 =  floatvolcurvelinear(P12_Settle_Price, ATM1_Vol, Call1_Skew,...
                            Put1_Skew, Skew1_Step, Barrier_1_Level);
            obj2 = EuropeanOptionEuropeanBarrier(P12_Settle_Price, Strike, Barrier_1_Level,Barrier1_Rebate,...
                RiskFreeRate, CostOfCarry,StrikeVol2,BarrierVol2, TTM,...
                OptionTypes, Lots, Lot_Mult, Curr_Mult, ...
                YearToDay_Mult,Opt_DayCount,  IsExpiryProcessed,Currency,TickSize);
            Price_2 =  obj2.getprice();
            
            OutDelta = (Price_2-Price_1)./(2.*TickSize.* CONST_SKEW_STEP_MULT)
      