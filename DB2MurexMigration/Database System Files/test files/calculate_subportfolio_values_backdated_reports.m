function [OutputHeader,Output] = calculate_subportfolio_values_backdated_reports(TradersData,SettleData)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:  
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2014/06/12 10:36:53 $
%  $Revision: 1.5 $
%

TableName = 'subportfolio_value_table';
[OutputHeader,Output] = read_from_database(TableName,1);

ValuationDate =  TradersData.value_date(1);

ValueDate    = datenum(TradersData.value_date,'yyyy-mm-dd');

TempMaturity = TradersData.maturity_date;
TempMaturity(cellfun(@isempty,TempMaturity)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
MaturityDate = datenum(TempMaturity,'yyyy-mm-dd');

% TradersData.maturity_date(cellfun(@isempty,TradersData.maturity_date)) = cellstr(datestr(today+1,'yyyy-mm-dd')); % happens only in fx_spot
% MaturityDate = datenum(TradersData.maturity_date,'yyyy-mm-dd');

Subportfolio = unique(TradersData.subportfolio);
Output = cell(length(Subportfolio),length(OutputHeader));

OutputHeader = lower(OutputHeader);
Pos_Subportfolio          = cellStrfind(OutputHeader, 'subportfolio');
Pos_ValueDate             = cellStrfind(OutputHeader, 'value_date');
Pos_PremiumValue          = cellStrfind(OutputHeader, 'premium_value');
Pos_DeadOptionPremium     = cellStrfind(OutputHeader, 'dead_option_premium');

Pos_SettleValue           = cellStrfind(OutputHeader, 'settle_value');
Pos_SettleDelta_1         = cellStrfind(OutputHeader, 'settle_delta_1');
Pos_SettleDelta_2         = cellStrfind(OutputHeader, 'settle_delta_2');
Pos_SettleGamma_11        = cellStrfind(OutputHeader, 'settle_gamma_11');
Pos_SettleGamma_12        = cellStrfind(OutputHeader, 'settle_gamma_12');
Pos_SettleGamma_21        = cellStrfind(OutputHeader, 'settle_gamma_21');
Pos_SettleGamma_22        = cellStrfind(OutputHeader, 'settle_gamma_22');
Pos_SettleVega_1          = cellStrfind(OutputHeader, 'settle_vega_1');
Pos_SettleVega_2          = cellStrfind(OutputHeader, 'settle_vega_2');
Pos_SettleTheta           = cellStrfind(OutputHeader, 'settle_theta');
Pos_SettleChi             = cellStrfind(OutputHeader, 'settle_chi');
Pos_Settle_DeltaCurrExp   = cellStrfind(OutputHeader, 'settle_delta_curr_exp');
Pos_Settle_PnlCurrExp     = cellStrfind(OutputHeader, 'settle_pnl_curr_exp');

Pos_TradersValue           = cellStrfind(OutputHeader, 'traders_value');
Pos_TradersDelta_1         = cellStrfind(OutputHeader, 'traders_delta_1');
Pos_TradersDelta_2         = cellStrfind(OutputHeader, 'traders_delta_2');
Pos_TradersGamma_11        = cellStrfind(OutputHeader, 'traders_gamma_11');
Pos_TradersGamma_12        = cellStrfind(OutputHeader, 'traders_gamma_12');
Pos_TradersGamma_21        = cellStrfind(OutputHeader, 'traders_gamma_21');
Pos_TradersGamma_22        = cellStrfind(OutputHeader, 'traders_gamma_22');
Pos_TradersVega_1          = cellStrfind(OutputHeader, 'traders_vega_1');
Pos_TradersVega_2          = cellStrfind(OutputHeader, 'traders_vega_2');
Pos_TradersTheta           = cellStrfind(OutputHeader, 'traders_theta');
Pos_TradersChi             = cellStrfind(OutputHeader, 'traders_chi');
Pos_Traders_DeltaCurrExp   = cellStrfind(OutputHeader, 'traders_delta_curr_exp');
Pos_Traders_PnlCurrExp     = cellStrfind(OutputHeader, 'traders_pnl_curr_exp');

Pos_MTM_USD                = cellStrfind(OutputHeader, 'mtm-usd');
Pos_MTM_NC                 = cellStrfind(OutputHeader, 'mtm-nc');
Pos_BV_USD                 = cellStrfind(OutputHeader, 'bv-usd');
Pos_BV_NC                  = cellStrfind(OutputHeader, 'bv-nc');
Pos_MKT_US_USD             = cellStrfind(OutputHeader, 'mkt-us-usd');
Pos_MKT_US_NC              = cellStrfind(OutputHeader, 'mkt-us-nc');

for iS = 1:length(Subportfolio)
    SubportfolioName = Subportfolio(iS);
    IdxTraders = ismember(TradersData.subportfolio,SubportfolioName);
    IdxSettle  = ismember(SettleData.subportfolio,SubportfolioName);
    
    Output(iS,Pos_Subportfolio) = SubportfolioName;
    Output(iS,Pos_ValueDate)    = ValuationDate;
    
    SumPremium = sum(TradersData.current_premium(IdxTraders) .* TradersData.original_lots(IdxTraders) .* ...
        TradersData.lot_mult1(IdxTraders) .* TradersData.curr_mult1(IdxTraders));
    IdxDeadOption = IdxTraders & (ValueDate > MaturityDate);
    DeadOptionPremium = sum(TradersData.current_premium(IdxDeadOption) .* TradersData.original_lots(IdxDeadOption) .* ...
        TradersData.lot_mult1(IdxDeadOption) .* TradersData.curr_mult1(IdxDeadOption));
    
    Output(iS,Pos_PremiumValue)      = num2cell(SumPremium - DeadOptionPremium);
    Output(iS,Pos_DeadOptionPremium) = num2cell(DeadOptionPremium);
    
    Output(iS,Pos_SettleValue)         = num2cell(sum(SettleData.settle_price(IdxSettle)));
    Output(iS,Pos_SettleDelta_1)       = num2cell(sum(SettleData.settle_delta_1(IdxSettle)));
    Output(iS,Pos_SettleDelta_2)       = num2cell(sum(SettleData.settle_delta_2(IdxSettle)));
    Output(iS,Pos_SettleGamma_11)      = num2cell(sum(SettleData.settle_gamma_11(IdxSettle)));
    Output(iS,Pos_SettleGamma_12)      = num2cell(sum(SettleData.settle_gamma_12(IdxSettle)));
    Output(iS,Pos_SettleGamma_21)      = num2cell(sum(SettleData.settle_gamma_21(IdxSettle)));
    Output(iS,Pos_SettleGamma_22)      = num2cell(sum(SettleData.settle_gamma_22(IdxSettle)));
    Output(iS,Pos_SettleVega_1)        = num2cell(sum(SettleData.settle_vega_1(IdxSettle)));
    Output(iS,Pos_SettleVega_2)        = num2cell(sum(SettleData.settle_vega_2(IdxSettle)));
    Output(iS,Pos_SettleTheta)         = num2cell(sum(SettleData.settle_theta(IdxSettle)));
    Output(iS,Pos_SettleChi)           = num2cell(sum(SettleData.settle_chi(IdxSettle)));
    Output(iS,Pos_Settle_DeltaCurrExp) = num2cell(sum(SettleData.settle_delta_curr_exp(IdxSettle)));
    Output(iS,Pos_Settle_PnlCurrExp)   = num2cell(sum(SettleData.settle_pnl_curr_exp(IdxSettle)));
    
    Output(iS,Pos_TradersValue)        = num2cell(sum(TradersData.traders_value(IdxTraders)));
    Output(iS,Pos_TradersDelta_1)      = num2cell(sum(TradersData.traders_delta_1(IdxTraders)));
    Output(iS,Pos_TradersDelta_2)      = num2cell(sum(TradersData.traders_delta_2(IdxTraders)));
    Output(iS,Pos_TradersGamma_11)     = num2cell(sum(TradersData.traders_gamma_11(IdxTraders)));
    Output(iS,Pos_TradersGamma_12)     = num2cell(sum(TradersData.traders_gamma_12(IdxTraders)));
    Output(iS,Pos_TradersGamma_21)     = num2cell(sum(TradersData.traders_gamma_21(IdxTraders)));
    Output(iS,Pos_TradersGamma_22)     = num2cell(sum(TradersData.traders_gamma_22(IdxTraders)));
    Output(iS,Pos_TradersVega_1)       = num2cell(sum(TradersData.traders_vega_1(IdxTraders)));
    Output(iS,Pos_TradersVega_2)       = num2cell(sum(TradersData.traders_vega_2(IdxTraders)));
    Output(iS,Pos_TradersTheta)        = num2cell(sum(TradersData.traders_theta(IdxTraders)));    
    Output(iS,Pos_TradersChi)          = num2cell(sum(TradersData.traders_chi(IdxTraders)));    
    Output(iS,Pos_Traders_DeltaCurrExp)= num2cell(sum(TradersData.traders_delta_curr_exp(IdxTraders)));    
    Output(iS,Pos_Traders_PnlCurrExp)  = num2cell(sum(TradersData.traders_pnl_curr_exp(IdxTraders)));    
    
    Output(iS,Pos_MTM_USD)             = num2cell(sum(SettleData.mtm_usd(IdxSettle)));
    Output(iS,Pos_MTM_NC)              = num2cell(sum(SettleData.mtm_nc(IdxSettle)));
    
    Output(iS,Pos_BV_USD)              = num2cell(sum(TradersData.bv_usd(IdxTraders)));
    Output(iS,Pos_BV_NC)               = num2cell(sum(TradersData.bv_nc(IdxTraders)));   
    
    Output(iS,Pos_MKT_US_USD)          = num2cell(sum(SettleData.mtm_usd(IdxSettle)) - sum(TradersData.bv_usd(IdxTraders)));
    Output(iS,Pos_MKT_US_NC)           = num2cell(sum(SettleData.mtm_nc(IdxSettle)) - sum(TradersData.bv_nc(IdxTraders)));
end

end