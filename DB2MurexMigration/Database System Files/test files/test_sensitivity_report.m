InputFilterValues = {'subportfolio',{'all'}};
SummmaryParameters = {'subportfolio'};
%% input section
% InputFilterValues = {'subportfolio',{'MM-C-CBT-OIV'}};
% SummmaryParameters = {'subportfolio','counterparty','contract_month'};
PricingType = 'traders';
% OutputFields = {'price','pricemove','vol','volmove','time','timemove','delta','gamma','vega','theta','m2m_usd','m2m_nc'};
OutputFields = {'time','delta','gamma','vega','theta','m2m_usd','m2m_nc'};
% IsPerformSensitivity, NumberOfSteps,StepSize
InputScenarios = {'on',0,4,[];... % IsPerformSensitivity, NumberOfSteps,StepSize - Price move
    'on',0,0.5,[];...% IsPerformSensitivity, NumberOfSteps,StepSize - vol move(in %)
    'on',0,5,[]}; % IsPerformSensitivity, NumberOfSteps,StepSize - time move(in days)

%% call matlab function
% matlab method to perform sensitivity analysis based on the given input
% scenarios
tic; OutFilename = generate_sensitivity_report(InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);toc;