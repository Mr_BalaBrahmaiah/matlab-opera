javaaddpath('mysql-connector-java-5.1.25-bin.jar');
objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.71');

BusDates = busdays(datenum('2014-10-13'), datenum('2014-10-14'),'daily','2014-12-25');
VDates = cellstr(datestr(BusDates,'yyyy-mm-dd'));

for i = 1:length(BusDates)
try
SqlQuery = ['update valuation_date_table set value_date = ''',VDates{i},''''];

curs = exec(objDB,SqlQuery);

ViewName = 'helper_generate_pricing_view';
[InputHeader,InputData] = read_from_database(ViewName,0);

PosTradeDate  = strcmpi('transaction_date',InputHeader);
TradeDates = datenum(InputData(:,PosTradeDate),'yyyy-mm-dd');
StartTradeDate = min(TradeDates);
EndTradeDate = max(TradeDates);

[InputData,InputHeader] = calcActiveLots(InputData,InputHeader,StartTradeDate,EndTradeDate);

TradeDates = datenum(InputData(:,PosTradeDate),'yyyy-mm-dd');
ValueDate = datenum(VDates{i},'yyyy-mm-dd');
IdxRemove = (TradeDates >= ValueDate) | (TradeDates < StartTradeDate) | (TradeDates > EndTradeDate);
InputData(IdxRemove,:) = [];

PricingType = 'traders';
TableName = 'security_traders_value_table';
% Read the values from database for derivative pricer
[PricerHeading,PricerData] = read_from_database(TableName,1);
IsSensitivityAnalysis = 0;
[TradersHeader,TradersData] = generate_pricing_views(PricingType,InputHeader,InputData,PricerHeading,PricerData,IsSensitivityAnalysis);
TradersHeader = strrep(TradersHeader,'delta','traders_delta_1');
TradersHeader = strrep(TradersHeader,'gamma','traders_gamma_11');
TradersHeader = strrep(TradersHeader,'vega','traders_vega_1');
TradersHeader = strrep(TradersHeader,'theta','traders_theta');
TradersHeader = strrep(TradersHeader,'calculated_price','traders_value');
TradersHeader = strrep(TradersHeader,'m2m_usd','bv-usd');
TradersHeader = strrep(TradersHeader,'m2m_nc','bv-nc');
DBTradersData = cell2dataset([TradersHeader;TradersData]);
clear PricerData; clear TradersData;
ZeroValues = zeros(size(DBTradersData.traders_value));
DBTradersData.traders_delta_2 = ZeroValues;
DBTradersData.traders_gamma_12 = ZeroValues;
DBTradersData.traders_gamma_21 = ZeroValues;
DBTradersData.traders_gamma_22 = ZeroValues;
DBTradersData.traders_vega_2 = ZeroValues;
DBTradersData.traders_chi = ZeroValues;
DBTradersData.traders_delta_curr_exp = ZeroValues;
DBTradersData.traders_pnl_curr_exp = ZeroValues;

PricingType = 'settle';
TableName = 'security_settlement_value_table';
[PricerHeading,PricerData] = read_from_database(TableName,1);
IsSensitivityAnalysis = 0;
[SettleHeader,SettleData] = generate_pricing_views(PricingType,InputHeader,InputData,PricerHeading,PricerData,IsSensitivityAnalysis);
SettleHeader = strrep(SettleHeader,'delta','settle_delta_1');
SettleHeader = strrep(SettleHeader,'gamma','settle_gamma_11');
SettleHeader = strrep(SettleHeader,'vega','settle_vega_1');
SettleHeader = strrep(SettleHeader,'theta','settle_theta');
SettleHeader = strrep(SettleHeader,'calculated_price','settle_price');
SettleHeader = strrep(SettleHeader,'m2m_usd','mtm-usd');
SettleHeader = strrep(SettleHeader,'m2m_nc','mtm-nc');
DBSettleData = cell2dataset([SettleHeader;SettleData]);
clear PricerData; clear SettleData;
ZeroValues = zeros(size(DBSettleData.settle_price));
DBSettleData.settle_delta_2 = ZeroValues;
DBSettleData.settle_gamma_12 = ZeroValues;
DBSettleData.settle_gamma_21 = ZeroValues;
DBSettleData.settle_gamma_22 = ZeroValues;
DBSettleData.settle_vega_2 = ZeroValues;
DBSettleData.settle_chi = ZeroValues;
DBSettleData.settle_delta_curr_exp = ZeroValues;
DBSettleData.settle_pnl_curr_exp = ZeroValues;

[OutputHeader,Output] = calculate_subportfolio_values_backdated_reports(DBTradersData,DBSettleData);
% update version info
Output(:,37) = num2cell(0);
Output(:,38) = {' '};
Output(:,39) = {' '};
Output(:,40) = {' '};

xlswrite(['Subportfolio_value_table_',Output{1,2},'.xlsx'],[OutputHeader;Output]);
catch ME
    disp(['In value_date',VDates{i},ME.message]);
    continue
end
end