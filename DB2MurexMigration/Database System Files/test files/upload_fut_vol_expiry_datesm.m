javaaddpath('mysql-connector-java-5.1.25-bin.jar');
objDB = database('inveniodbwebpagetest','ananthi','Olamnet@123','Vendor','MySQL','Server','10.190.7.71');

%% step-1, to add the expiry dates from the existing security id for the future id
[~,~,Data] = xlsread('C:\Users\DB System\Desktop\DB-Fut-Vol-SecId-Generaion\Step1-future_expiry_from_underlying_list.xlsx');
DsData = cell2dataset(Data);

NumIDs = length(DsData.underlying_id);

ExpiryDates = cellstr(datestr(datenum(DsData.expiry_date,'dd-mm-yyyy'),'yyyy-mm-dd'));
 
fid = fopen('dblog.txt','a');
for i = 1 : NumIDs
    SqlQuery = ['update underlying_list_table set expiry_date = ''',ExpiryDates{i},''' where underlying_id = ''',DsData.underlying_id{i},''''];
    curs = exec(objDB,SqlQuery);
    fprintf(fid,'%s\n',[TradeId{i},':',curs.Message]);

end
fclose(fid);
