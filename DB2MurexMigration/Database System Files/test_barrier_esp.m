% get the list of futures for all the barrier deals
ViewName = 'helper_barrier_expiry_view';
[ColNames,Data] = read_from_database(ViewName,0);
BarrierDBData = cell2dataset([ColNames;Data]);
BarrierFutId = unique(upper(BarrierDBData.underlying_id));
SettlementDate = BarrierDBData.settlement_date{1};
Barrier_Level = BarrierDBData.barrier_level;

[ColNames,Data] = read_from_database('underlying_list_table',0);
RefData = cell2dataset([ColNames;Data]);
UndId = RefData.underlying_id; 
BBG_UndId = RefData.bbg_underlying_id;

IdxEmpty = cellfun(@isempty,BBG_UndId);
BBG_UndId(IdxEmpty) = [];
UndId(IdxEmpty) = [];

[ColNames,Data] = read_from_database('valuation_date_table',0);
RefDate = cell2dataset([ColNames;Data]);
ValueDate = RefDate.value_date{1};

ErrorLogFile = ['BarrierExpiry_LogFile_',char(ValueDate),'.txt'];
fid = fopen(ErrorLogFile,'wt');

TickerOptions = {'PX_OPEN','PX_HIGH','PX_LOW','PX_LAST','PX_SETTLE'};

% javaaddpath('C:\bbgexe\blpapi3.jar');
% objBB = blp;
% 
% BBdata = [];
% for iCode = 1:length(BarrierFutId)
%     Idx = strcmp(BarrierFutId{iCode},UndId);
%     BarrierBBGId = char([BBG_UndId{Idx},' Comdty']);
%     Data = history(objBB, BarrierBBGId,TickerOptions,SettlementDate,SettlementDate);
%     if ~isempty(Data)
%         TempData = [BarrierFutId(iCode),cellstr(SettlementDate),num2cell(Data(:,2:end))];
%         BBdata = [BBdata;TempData];
%     end
% end
% Header = ['FutureName','settlement date',TickerOptions];
% Filename = ['BBG_Barrier_Future_Data_',SettlementDate,'.xlsx'];
% xlswrite(Filename,[Header;BBdata]);

BarrierExpData =[];
BarrierMissingPrices = [];

for iCode = 1:length(BarrierFutId)
    Idx = strcmpi(BarrierFutId{iCode},BarrierDBData.underlying_id);
    BBdataIdx = strcmpi(BarrierFutId{iCode},BBdata(:,1));
    if any(BBdataIdx)       
    Open  = BBdata{BBdataIdx,3};
    High  = BBdata{BBdataIdx,4};
    Low   = BBdata{BBdataIdx,5};
    Close = BBdata{BBdataIdx,6};
    
    True_Range_Low = repmat(Close,size(find(Idx)));
    True_Range_High = repmat(Close,size(find(Idx)));
    
    IdxBarrierType = strcmpi('american',BarrierDBData.barrier_type(Idx));
    if any(IdxBarrierType)
    True_Range_Low(IdxBarrierType) = min(Open,Low);
    True_Range_High(IdxBarrierType) = max(Open,High);
    end
       
    TRL_LTE_Barrier_Level = True_Range_Low <= Barrier_Level(Idx);
    TRH_GTE_Barrier_Level = True_Range_High >= Barrier_Level(Idx);
    
    BarrierHit = TRH_GTE_Barrier_Level;
    
    IdxDown = strcmpi('down',BarrierDBData.barrier_up_down(Idx)); 
    if any(IdxDown)
        BarrierHit(IdxDown) = TRL_LTE_Barrier_Level(IdxDown);
    end
    
    TempData = [BarrierDBData.contract_number(Idx),BarrierDBData.underlying_id(Idx),BarrierDBData.derivative_type(Idx)...
    BarrierDBData.settlement_date(Idx),BarrierDBData.barrier_type(Idx), BarrierDBData.barrier_up_down(Idx),...
    BarrierDBData.barrier_in_out(Idx),num2cell(BarrierDBData.barrier_level(Idx)),...    
    repmat(BBdata(BBdataIdx,3:end),size(find(Idx))),num2cell(BarrierHit)];

    BarrierExpData = [BarrierExpData;TempData]; % contract_no und_id bar_level open high low hit
    else
        BarrierMissingPrices = [BarrierMissingPrices;BarrierFutId(iCode)];
    end
end

if ~isempty(BarrierMissingPrices)
ErrorStr = ['Future prices for ''', convertCell2Char(BarrierMissingPrices), ''' are missing in BBG for the settlement_date ''',SettlementDate,'''. Hence barrier expiry cannot be processed for these futures!'];
warndlg(ErrorStr,'Missing Prices');
 fprintf(fid,'%s\n',ErrorStr);
end

Header = ['contract_number','future_id','derivative_type','settlement_date','barrier_type',...
    'barrier_up_down','barrier_in_out','barrier_level',TickerOptions,'is_barrier_hit'];

Filename = ['Barrier_Expiry_',ValueDate,'.xlsx'];
xlswrite(Filename,[Header;BarrierExpData]);

try
    upload_in_database('barrier_expiry_table', BarrierExpData);
catch ME
    errordlg(ME.message);
end

BarrierExpData = cell2dataset([Header;BarrierExpData]);

if any(BarrierExpData.is_barrier_hit)
ViewName = 'helper_barrier_expiry_security_info_view';
[ColNames,Data] = read_from_database(ViewName,0);
BarrierSecInfo = cell2dataset([ColNames;Data]);

NumRows= size(BarrierExpData,1);
OutBarrierMonData = [];
for iSec = 1:NumRows
if BarrierExpData.is_barrier_hit(iSec)
%     TempSettleDate =datestr(datenum(BarrierExpData.settlement_date{iSec}),'yyyy-mm-dd');
% IdxBarSec = strcmpi(BarrierExpData.contract_number(iSec),BarrierSecInfo.contract_number) & ...
% strcmpi(BarrierExpData.future_id(iSec),BarrierSecInfo.underlying_id) & ...
% strcmpi(BarrierExpData.derivative_type(iSec),BarrierSecInfo.derivative_type) & ...
% isnumericequal(BarrierExpData.barrier_level(iSec),BarrierSecInfo.barrier_level) & ...
% strcmpi(TempSettleDate,BarrierSecInfo.settlement_date);
IdxBarSec = strcmpi(BarrierExpData.contract_number(iSec),BarrierSecInfo.contract_number) & ...
strcmpi(BarrierExpData.future_id(iSec),BarrierSecInfo.underlying_id) & ...
strcmpi(BarrierExpData.derivative_type(iSec),BarrierSecInfo.derivative_type) & ...
isnumericequal(BarrierExpData.barrier_level(iSec),BarrierSecInfo.barrier_level) ;

if any(IdxBarSec)
    TempSize = size(find(IdxBarSec));
BarrierRowData = [BarrierSecInfo.contract_number(IdxBarSec),...
    BarrierSecInfo.value_date(IdxBarSec),BarrierSecInfo.security_id(IdxBarSec),...
    num2cell(repmat(1,TempSize)),repmat({'NULL'},TempSize)];
OutBarrierMonData = [OutBarrierMonData;BarrierRowData];
end

end
end

if ~isempty(OutBarrierMonData)
    MonHeader = {'contract_number','value_date','security_info','isbarrier1_hit','isbarrier2_hit'};
    OutFilename = ['Barrier_monitoring_table_',ValueDate,'.xlsx'];
    xlswrite(OutFilename,[MonHeader;OutBarrierMonData]);
end
end
fclose(fid);