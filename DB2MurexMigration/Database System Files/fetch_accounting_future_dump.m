[OutputFields,Data] = read_from_database('helper_var_accounting_future_dump_view',0);

UniqueFields = {'Date','Instrument','Portfolio','Contract'};
SumFields = {'Net Pos','Adapted delta','Gamma_SMT','Theta','Vega'};
WeightedAverageFields = [];
[OutputFields,OutData] = consolidatedata(OutputFields, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);

OutFilename = getXLSFilename('AccountingFutureDump');
xlswrite(OutFilename,[OutputFields;OutData])

configure_mail_settings;
