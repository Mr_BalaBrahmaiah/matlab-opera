function OutDrawdownData = PnL_Calculation_TR_Reports(InBUName)
% First Run " generate_traders_report_summary.m "

%%% Read from Database
ObjDB = connect_to_database;

ViewName = ['accounting_tr_drawdown_table_',char(InBUName)];
[RawData_Header,RawData] = read_from_database(ViewName,0);

%%% Get Value Date
Value_Settele_Date = fetch(ObjDB,'SELECT value_date,settlement_date FROM valuation_date_table');
Current_ValueDate = Value_Settele_Date(1);
Settlement_Date =  Value_Settele_Date(2);

%%% Get Last Five Business Day
[~,~,Last_Five_BusDay] = Get_User_Want_Business_Days();

%%%
ValueDate_Col = find(cellfun(@(V) strcmpi('value_date',V), RawData_Header)) ;

Subportfolio_Col = find(cellfun(@(V) strcmpi('tr_aggregation_level',V), RawData_Header)) ; %% subportfolio equivalent to tr_aggregation_level
Unique_Portfolio = unique(RawData(:,Subportfolio_Col));

Total_mtmUSD_Col = find(cellfun(@(V) strcmpi('total_mtm_usd',V), RawData_Header)) ;
Moving_Avg_Col = find(cellfun(@(V) strcmpi('moving_average',V), RawData_Header)) ;
Maximum_Col = find(cellfun(@(V) strcmpi('maximum',V), RawData_Header)) ;
DrawDown_Col = find(cellfun(@(V) strcmpi('drawdown',V), RawData_Header)) ;

%%% Get Year first 3 Business Dates and don't do any Drawdown calculations
%%% for these 3 days and just upload the data with zeros like 
%%% Moving_Avg_Value = 0, Maximum_Value = 0 and DrawDown_Value = 0
cur_year = datestr(today,'yyyy');
Busday = strcat(cur_year,'-01-01');
Index_weekday = weekday(Busday);
if Index_weekday == 7 || Index_weekday == 1
    Busday1 = datestr(busdate(Busday),'yyyy-mm-dd');
    Busday2 = datestr(busdate(Busday1),'yyyy-mm-dd');
    Busday3 = datestr(busdate(Busday2),'yyyy-mm-dd');
    First_3_days = [Busday1 ; Busday2 ; Busday3];
else
    Busday2 = datestr(busdate(Busday),'yyyy-mm-dd');
    Busday3 = datestr(busdate(Busday2),'yyyy-mm-dd');
    First_3_days = [Busday ; Busday2 ; Busday3];
end

% %%% Remove 1st Jan Data from Table
% Index = strcmpi(cellstr(Busday),RawData(:,1));
% RawData(Index,:) = [];

Index_match = strcmpi(Current_ValueDate,First_3_days);
if any(Index_match)
    Current_ValueDate_Index = cellStrfind_exact(RawData(:,ValueDate_Col),Current_ValueDate);
    Temp_Matrix = RawData(Current_ValueDate_Index,:); 
    Value_Zero = repmat({0},size(Temp_Matrix,1),1);
                
    Temp_Matrix(:,4) = Value_Zero; %% Moving_Avg
    Temp_Matrix(:,5) = Value_Zero; %% Maximum
    Temp_Matrix(:,6) = Value_Zero; %% DrawDown
    
    DrawdownData = Temp_Matrix;
else
    %%% Get Moving Average and Difference in current Value Date
    Total_Data_CurrentValue_Date = [];
    Error_Data_CurrentValue_Date = [];
    Current_ValueDate_Portfolio_Data = [];

    for i = 1 : size(Unique_Portfolio,1)
        Current_Portfolio = Unique_Portfolio(i);
        Matched_Portfolio_Index = cellStrfind_exact(RawData(:,Subportfolio_Col),Current_Portfolio);
        Temp_Matrix = RawData(Matched_Portfolio_Index,:);

        if(size(Temp_Matrix,1)>=3)
            Last_Five_BusDay_Index = cellStrfind_exact(Temp_Matrix(:,ValueDate_Col),Last_Five_BusDay);
            Current_ValueDate_Index = cellStrfind_exact(Temp_Matrix(:,ValueDate_Col),Current_ValueDate);
            Temp_Matrix(Current_ValueDate_Index,Moving_Avg_Col) = num2cell(mean(cell2mat(Temp_Matrix(Last_Five_BusDay_Index,Total_mtmUSD_Col)))); %% Calculate Moving Average
            %Temp_Matrix(Current_ValueDate_Index,Difference_mtm_Col) = num2cell( cell2mat(Temp_Matrix(Current_ValueDate_Index,Moving_Avg_Col)) - cell2mat(Temp_Matrix(Current_ValueDate_Index,Total_mtmUSD_Col))); %% Calculate  Difference
            %%%
            % NaN_Index = find(isnan(cell2mat(Temp_Matrix(:,Moving_Avg_Col))));
            % Temp_Matrix(NaN_Index,Moving_Avg_Col) = {0};

            Not_NaN_Index = find(~isnan(cell2mat(Temp_Matrix(:,Moving_Avg_Col))));
            Max_Value = num2cell(max(cell2mat(Temp_Matrix(Not_NaN_Index , Moving_Avg_Col))));
            Max_Value = num2cell(max(0,cell2mat(Max_Value)));

            Temp_Matrix(Current_ValueDate_Index,Maximum_Col) = Max_Value;  %% Not_NaN_Index %% Current_ValueDate_Index
    %         Temp_Matrix(Current_ValueDate_Index,DrawDown_Col) = num2cell(min(0, cell2mat(Temp_Matrix(Current_ValueDate_Index,Total_mtmUSD_Col))) - cell2mat(Max_Value));
            Temp_Matrix(Current_ValueDate_Index,DrawDown_Col) = num2cell(min(0,( cell2mat(Temp_Matrix(Current_ValueDate_Index,Total_mtmUSD_Col)) - cell2mat(Max_Value) )));
            %%%
            Total_Data_CurrentValue_Date = [Total_Data_CurrentValue_Date;Temp_Matrix];
            Current_ValueDate_Portfolio_Data = [Current_ValueDate_Portfolio_Data ; Temp_Matrix(Current_ValueDate_Index,:)];
        else
            Error_Data_CurrentValue_Date = [Error_Data_CurrentValue_Date;Temp_Matrix];
        end
        clear Temp_Matrix_Data;
    end  
    if(~isempty(Error_Data_CurrentValue_Date))
        DrawdownData = [Current_ValueDate_Portfolio_Data;Error_Data_CurrentValue_Date];
    else
        DrawdownData = Current_ValueDate_Portfolio_Data;
    end
end

%%Uploading to DB
TableName = ['accounting_tr_drawdown_table_',char(InBUName)];
set(ObjDB,'AutoCommit','off');
SqlQuery = ['delete from ',TableName,' where value_date = ''',char(Current_ValueDate),''''];
curs = exec(ObjDB,SqlQuery);
commit(ObjDB);
if ~isempty(curs.Message)
    disp(curs.Message);
end
set(ObjDB,'AutoCommit','on');

upload_in_database(TableName, DrawdownData);

OutDrawdownData = [RawData_Header;DrawdownData];

