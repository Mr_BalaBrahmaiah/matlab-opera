function run_all_recon_views_OPERA

% [~,ConfigFileData] = xlsread('AutomatedReportsConfig.xlsx');
% ReportNames = ConfigFileData(2:end,1);
% OutputFolders = ConfigFileData(2:end,2);
% IdxFound = strcmpi('ReconViews',ReportNames);
% DestinationFolder = OutputFolders{IdxFound};

% DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\OPERA RECON DUMPS';
DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\OPERA_RECON_DUMPS';
LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try
    % %% generate and write the traders recon dump
    % ViewName = 'helper_traders_p_and_g_view';
    % Filename = getXLSFilename(ViewName);
    % [ColNames,Data] = read_from_database(ViewName,0);
    % xlswrite(Filename,[ColNames;Data]);
    %
    % % DestinationFolder = '\\Shared-pc\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';
    % OutFilename = fullfile(DestinationFolder,Filename);
    % move_reports(Filename,OutFilename,'Traders Recon dump');
    
    InBUName = {'agf','qf1','qf2','qf3','qf4','orx'};
    
    for iBU = 1:length(InBUName)
        
        
        %% generate and write the settle recon dump
        ViewName = ['helper_settle_p_and_g_view_',InBUName{iBU}];
        Filename = getXLSFilename(ViewName);
        [ColNames,Data] = read_from_database(ViewName,0);
        xlswrite(Filename,[ColNames;Data]);
        
        % DestinationFolder = '\\Shared-pc\e\Invenio Database System\Database System Dump Files\automated-p&g-dumps-from-DBSystem';
        OutFilename = fullfile(DestinationFolder,upper(InBUName{iBU}),Filename);
        move_reports(Filename,OutFilename,['Settle Recon dump - ',upper(InBUName{iBU})]);
        
    end
    
catch
    write_log_file(LogFilename,'Unknown error while saving recon dumps!');
end