function [OutErrorMsg,OutXLSfileName] = generate_position_monitoring_report_SingleDay_QF(InBUName)

% Final
InBUName = 'QF';

try
    OutErrorMsg = {'No Errors'};
    OutXLSfileName = getXLSFilename(['Position_Monitoring_Report_',upper(char(InBUName))]);
    
    ObjDB = connect_to_database ;
    
    Current_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    FY_StartDate = fetch(ObjDB,'select start_date from valuation_financial_year_view');
    
    %% Overall Day
    %     SqlQuery_Overall_SettleDate_ValueDate =['select * from vdate_sdate_table where settlement_date >=''',char(FY_StartDate),''' and settlement_date <=''',char(Current_SettleDate),''' ' ];
    %     [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    %     Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    %     Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %% Single Day
    SqlQuery_Overall_SettleDate_ValueDate = 'select value_date,settlement_date from valuation_date_table' ;
    [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %% Product Name
    
    %     Unique_ProductName = unique(Overall_DBData.product_name);
    Unique_ProductName = fetch(ObjDB,'select product_name from position_monitoring_report_products_table') ;
    Unique_ProductName = sort(Unique_ProductName);
    
    
    %% Initialization
    
    Output_Header = {'COB Date','product_code','product_name','group_type','contract_month','Position','First Month Position','All month position','Net position','Gross Position'};
    First_MonthPosition_Col = cellStrfind_exact(Output_Header,{'First Month Position'});
    All_MonthPosition_Col = cellStrfind_exact(Output_Header,{'All month position'});
    Net_Position_Col = cellStrfind_exact(Output_Header,{'Net position'});
    Gross_Position_Col = cellStrfind_exact(Output_Header,{'Gross Position'});
    
    Summary_Header = {'Criteria','First Month Position','All month position','Net position','Gross Position'};
    Summary_RowIndex = {'Max','Min','Average','Median','5th Percentile','95th Percentile'};
    
    Overall_Summary_Header = [{'Product Summary'},Summary_Header];
    Summary_OutputArray = [];
    Overall_Summary_OutputArray = [];
    
    Output_Array = [];
    Overall_Uploading_Data = [];
    All_Product_Overall_Uploading_Data = [];
    
    %% Fetch Position Monitoring QF Table
    
    BU_Names = {'qf1','qf2','qf3','qf4'};
    Overall_DBData = [];
    for k = 1 : length(BU_Names)
        Current_TableName =  strcat('position_monitoring_report_',char(BU_Names(k)));
        
        SqlQuery = ['select * from ',char(Current_TableName),' where cob_date = ''',char(Overall_SettleDate),''' '];
        
        [ColNames,DBData] = Fetch_DB_Data(ObjDB,SqlQuery);
        if(isempty(DBData))
            OutErrorMsg = {['No getting data from ',char(Current_TableName)]};
            return;
        end
        if(strcmpi(DBData,'No Data'))
            OutErrorMsg = {['No getting data from ',char(Current_TableName)]};
            continue;
        end
        
        Overall_DBData = [Overall_DBData ; DBData ];
    end
    
    %% Consolidate Level
    UniqueFields = {'cob_date','product_code','product_name','group_type','contract_month'};
    SumFields = {'position'};
    OutputFields = {'product_code','product_name','group_type','contract_month','position'};
    WeightedAverageFields = [];
    
    ProductName_Col = cellStrfind_exact(OutputFields,{'product_name'});
    Positions_Col = cellStrfind_exact(OutputFields,{'position'});
    
    
    [Consolidate_Header,Consolidate_DBData] = consolidatedata(ColNames, Overall_DBData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    %% Zero Index Removal
    
    Zero_Index = cell2mat(Consolidate_DBData(:,Positions_Col)) ==  0;
    Consolidate_DBData(Zero_Index,:) = [];
    
    if(isempty(Consolidate_DBData))
        OutErrorMsg = {'All Month Positions are Zero'};
        return;
    end
    
    %% Financial Year Start Date
    Year_StartDate = fetch(ObjDB,'select start_date from valuation_financial_year_view');
    Year_StartDate_Number = datenum(Year_StartDate,'yyyy-mm-dd');
    
    %% Upload TableName
    
    Uploading_TableName = ['position_monitoring_report_',char(InBUName)];
    
    %%
    
    for ii = 1 : size(Unique_ProductName,1)
        
        Current_ProductName = Unique_ProductName(ii);
        
        Current_TransactionDate = Overall_SettleDate;
        
        Matched_ProductName_Index = strcmpi(Consolidate_DBData(:,ProductName_Col),Current_ProductName);
        if(isempty(find(Matched_ProductName_Index, 1)))
            continue;
        end
        
        Current_Product_Data = Consolidate_DBData(Matched_ProductName_Index,:);
        if(isempty(Current_Product_Data))
            continue;
        end
        
        try
            Month_YearNum = cellfun(@(s) (str2double(s(end))), Current_Product_Data(:,4),'UniformOutput',0);
            Find_Max_Length = find(cellfun(@(s) (length(s)>5), Current_Product_Data(:,4))) ;
            if(~isempty(Find_Max_Length))
                Month_YearNum(Find_Max_Length) = cellfun(@(s) (str2double(s(end-1:end))), Current_Product_Data(Find_Max_Length,4),'UniformOutput',0);
            end
            %                     Month_YearNum = cellstrNum_2_double(Month_YearNum) ; %% If Str Convert Double For Sorting purpose
            
            Current_Product_Data = [Current_Product_Data,Month_YearNum];
            Current_Product_Data = sortrows(Current_Product_Data,4);
            Current_Product_Data = sortrows(Current_Product_Data,6);
            Current_Product_Data(:,6) = [];
        catch
            Current_Product_Data = sortrows(Current_Product_Data,4);
        end
        
        %%
        Temp_COB_Array = cell(size(Current_Product_Data,1),1);
        Temp_COB_Array(:,:) =  Current_TransactionDate;
        
        Temp_Position_Array = cell(size(Current_Product_Data,1),4);
        
        Temp_Position_Array(end,1) = num2cell( abs(cell2mat(Current_Product_Data(1,5))) ); %% First Month Position
        Temp_Position_Array(end,2) = num2cell (max(abs(cell2mat(Current_Product_Data(:,5)))) ); %% ALL Month Position
        Temp_Position_Array(end,3) = num2cell ( abs(nansum(cell2mat(Current_Product_Data(:,5)))) ); %% Net Position
        Temp_Position_Array(end,4) = num2cell( nansum(abs(cell2mat(Current_Product_Data(:,5)))) ) ; %% Gross Position
        
        Output_Array = [Output_Array ; [Temp_COB_Array,Current_Product_Data,Temp_Position_Array] ];
        
        clear Temp_COB_Array  Temp_Position_Array;
        
        %% No Need Past Years Data
        if(isempty(Output_Array))
            continue;
        end
        
        Output_Array_YearNumber = datenum(Output_Array(:,1),'yyyy-mm-dd');
        
        PastYear_Index = Output_Array_YearNumber < Year_StartDate_Number;
        Output_Array(PastYear_Index,:) = [];
        
        %% Uploading into DB
        
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',Uploading_TableName,' where cob_date = ''',char(Overall_SettleDate),''' and product_name = ''',char(Current_ProductName),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(Uploading_TableName, Output_Array);
        
        cprintf('key','%s : %s : upload into %s finished\n',char(Current_TransactionDate),char(Current_ProductName),char(Uploading_TableName));
        
        %% Fetch TodayPosition with Previous Day Position for Calculation
        SqlQuery = ['select * from ',Uploading_TableName,' where product_name = ''',char(Current_ProductName),''' ' ];
        [~,Overall_Uploading_Data] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        %% Summary Sheet
        Position_On_Date = [{'Position_On_Date'},Output_Array(end,7:end)];
        
        Summary_OutputArray = cell(size(Summary_RowIndex,2),size(Summary_Header,2));
        
        for k = 1 : size(Summary_RowIndex,2)
            Current_RowIndex = Summary_RowIndex(k);
            if(strcmpi(Current_RowIndex,'max'))
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(max(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                Summary_OutputArray(k,3) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                Summary_OutputArray(k,4) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                Summary_OutputArray(k,5) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
            elseif(strcmpi(Current_RowIndex,'min'))
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(min(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                Summary_OutputArray(k,3) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                Summary_OutputArray(k,4) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                Summary_OutputArray(k,5) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
            elseif(strcmpi(Current_RowIndex,'Average'))
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                Summary_OutputArray(k,3) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                Summary_OutputArray(k,4) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                Summary_OutputArray(k,5) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
            elseif(strcmpi(Current_RowIndex,'Median'))
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                Summary_OutputArray(k,3) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                Summary_OutputArray(k,4) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                Summary_OutputArray(k,5) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
            elseif(strcmpi(Current_RowIndex,'5th Percentile'))
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col)),5));
                Summary_OutputArray(k,3) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col)),5));
                Summary_OutputArray(k,4) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Net_Position_Col)),5));
                Summary_OutputArray(k,5) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col)),5));
            else
                Summary_OutputArray(k,1) = Current_RowIndex;
                Summary_OutputArray(k,2) = num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col)),95));
                Summary_OutputArray(k,3) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col)),95));
                Summary_OutputArray(k,4) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Net_Position_Col)),95));
                Summary_OutputArray(k,5) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col)),95));
                
            end
            
        end
        
        Summary_OutputArray = [Position_On_Date ; Summary_OutputArray] ; %% Including Position On Date
        
        Current_Product_Summary = cell(size(Summary_OutputArray,1),1);
        Current_Product_Summary(:,:) = {[char(Current_ProductName),' Summary']};
        
        Summary_OutputArray = [ Current_Product_Summary, Summary_OutputArray ];
        
        % Need Product Wise Summary
        % xlswrite(OutXLSfileName,[Overall_Summary_Header;Summary_OutputArray],[char(Current_ProductName),' Summary']); %% Summary_Header
        
        
        Output_Array = []; %% Looping Instance
        
        %% Excel Write
        if(~isempty(Summary_OutputArray))
            Overall_Summary_OutputArray = [Overall_Summary_OutputArray ; Summary_OutputArray] ;
        end
        
        if(~isempty(Overall_Uploading_Data))
            xlswrite(OutXLSfileName,[Output_Header;Overall_Uploading_Data],char(Current_ProductName));
            All_Product_Overall_Uploading_Data = [All_Product_Overall_Uploading_Data ; Overall_Uploading_Data];
            Overall_Uploading_Data = []; %% Looping Instance
        end
        
        fprintf('%s : %d : %d finished\n',char(Current_ProductName),ii,length(Unique_ProductName));
        
    end
    
    %% Excel Sheet Delete
    
    xlswrite(OutXLSfileName,[Overall_Summary_Header ; Overall_Summary_OutputArray],'Overall Summary');
    
    BU_Array = cell(size(All_Product_Overall_Uploading_Data,1),1);
    BU_Array(:,:) = {upper(InBUName)};
    xlswrite(OutXLSfileName,[[Output_Header,{'BU'}] ;[All_Product_Overall_Uploading_Data,BU_Array]],'All_Products_Position');%% VBA Purpose
    
    try
        xls_delete_sheets(fullfile(pwd,OutXLSfileName));
    catch
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end




