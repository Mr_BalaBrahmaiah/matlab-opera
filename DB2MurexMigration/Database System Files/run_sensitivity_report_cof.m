
theCell = {'CF-Prop-','CF-TEND-','CF-RIM-'};
[User_SelectBU, theChosenIDX] = uicellect(theCell,'Multi', 0);

hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');

if(~isempty(User_SelectBU))
    
    if(strcmpi(User_SelectBU,'CF-Prop-'))
        %%% CF-Prop-
        InBUName = 'cof';
        InputFilterValues = {'subportfolio',{'CF-A_VIET_SC','CF-A_VIETNAM','CF-BVIET','CF-VOL','CF-VOL A','CF-VOL E'};'deal_status',{'live'}};
        InputScenarios = [{'on',10,2,'Percentage Move'};{'on',3,5,''};{'off',3,10,''}];
        SummmaryParameters = {'product_code','instrument','subportfolio','contract_month','derivative_type','strike','exchange_name'};
        PricingType = 'settle';
        OutputFields = {'active_lots','price','pricemove','vol','volmove','delta','gamma','vega','theta','m2m_usd'};
        
        OutFilename = 'CF-Prop-';
        
    elseif(strcmpi(User_SelectBU,'CF-TEND-'))
        %%% CF-TEND
        
        InBUName = 'cof';
        InputFilterValues = {'subportfolio',{'CF-TEND'};'deal_status',{'live'}};
        InputScenarios = [{'on',10,2,'Percentage Move'};{'on',3,5,''};{'off',3,10,''}];
        SummmaryParameters = {'product_code','instrument','subportfolio','contract_month','derivative_type','strike','exchange_name'};
        PricingType = 'settle';
        OutputFields = {'active_lots','price','pricemove','vol','volmove','delta','gamma','vega','theta','m2m_usd'};
        
        OutFilename = 'CF-TEND-';
        
        elseif(strcmpi(User_SelectBU,'CF-RIM-'))
        %%% CF-TEND
        
        InBUName = 'cof';
        InputFilterValues = {'subportfolio',{'CF-RIM'};'deal_status',{'live'}};
        InputScenarios = [{'on',10,2,'Percentage Move'};{'on',3,5,''};{'off',3,10,''}];
        SummmaryParameters = {'product_code','instrument','subportfolio','contract_month','derivative_type','strike','exchange_name'};
        PricingType = 'settle';
        OutputFields = {'active_lots','price','pricemove','vol','volmove','delta','gamma','vega','theta','m2m_usd'};
        
        OutFilename = 'CF-RIM-';
        
    else
        
        
    end
    
    %% generate_sensitivity_report   %% generate_sensitivity_report_parfor
    
    tic;
    try
        [OutErrorMsg,OutFilename] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields,OutFilename);
        
        msgbox(['Without Parfor : ', num2str(toc) ]);
        
    catch ME
        OutErrorMsg = cellstr(ME.message);
        
        errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
            ME.stack(1).name, ME.stack(1).line, ME.message);
        fprintf(1, '%s\n', errorMessage);
        %     uiwait(warndlg(errorMessage));
        
    end
    
    %% generate_sensitivity_report_parfor
%     parpool;
%     tic;
%     try
%         [OutErrorMsg,OutFilename] = generate_sensitivity_report_parfor(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields,OutFilename);
%         
%         msgbox(['With Parfor : ', num2str(toc) ]);
%         
%     catch ME
%         OutErrorMsg = cellstr(ME.message);
%         
%         errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
%             ME.stack(1).name, ME.stack(1).line, ME.message);
%         fprintf(1, '%s\n', errorMessage);
%         uiwait(warndlg(errorMessage));
%         
%     end
%     
%     delete(gcp('nocreate'));
    
else
    warndlg('Please Select appropriate check box','warning');
end

waitbar(1,hWaitbar,'Finished');




