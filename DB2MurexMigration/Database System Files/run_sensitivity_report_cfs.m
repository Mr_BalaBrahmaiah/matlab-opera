
InBUName = 'cfs';
InputFilterValues = {'subportfolio',{'RMS-KC-NYB-OIR','MM-KC-NYB-OIV','MM-DF-LIF-OIV','RMS-DF-LIF-OIR'};...
    'maturity',{'live'}};
InputScenarios = [{'on',20,50,'Percentage Move'};{'off',3,0.25,''};{'off',3,10,''}];
SummmaryParameters = {'product_code','instrument','subportfolio','contract_month','derivative_type','strike','exchange_name'};
PricingType = 'settle';
OutputFields = {'active_lots','price','pricemove','delta','gamma','vega','theta','m2m_usd'};


%% generate_sensitivity_report   %% generate_sensitivity_report_parfor

tic;
try
    [OutErrorMsg,OutFilename] = generate_sensitivity_report(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
    
    msgbox(['Without Parfor : ', num2str(toc) ]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end

%% generate_sensitivity_report_parfor

% tic;
% try
%     [OutErrorMsg,OutFilename] = generate_sensitivity_report_parfor(InBUName,InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);
% 
%     msgbox(['With Parfor : ', num2str(toc) ]);
% 
% catch ME
%     OutErrorMsg = cellstr(ME.message);
% 
%     errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
%         ME.stack(1).name, ME.stack(1).line, ME.message);
%     fprintf(1, '%s\n', errorMessage);
%     %     uiwait(warndlg(errorMessage));
% 
% end

