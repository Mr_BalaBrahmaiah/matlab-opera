clc ;

[DBName,DBUserName,DBPassword,DBServerIP,IsFromServer] = Read_DB_ConfigFile();
javaaddpath('mysql-connector-java-5.1.25-bin.jar');
p=parpool('local',4);
c =parallel.pool.Constant(@()database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP,'PortNumber',3306),@close);

Parfor_Error = [];
parfor i = 1 : 4
    ObjDB = c.Value;
    SqlQuery = 'select * from valuation_date_table1' ;
    
    Curs = exec(ObjDB,SqlQuery);
    if ~isempty(Curs.Message)
        OutErrorMessage = {Curs.Message};
        Parfor_Error = [Parfor_Error ; OutErrorMessage] ;
    end
    
end
Parfor_Error