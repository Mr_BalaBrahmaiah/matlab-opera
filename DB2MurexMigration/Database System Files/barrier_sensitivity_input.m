InputFilterValues = {'maturity',{'live'};...
    'derivative_type',{'amer_down_in_call',...
    'amer_down_in_put','amer_down_out_call','amer_down_out_put','amer_up_in_call',...
    'amer_up_in_put','amer_up_out_call','amer_up_out_put','euro_down_in_call',...
    'euro_down_in_put','euro_down_out_call','euro_down_out_put','euro_up_in_call','euro_up_in_put','euro_up_out_call','euro_up_out_put'}};

InputScenarios = {'on',5,10,'percentage move'; 'off',3,3.00,[];'off',3,20,[]};

SummmaryParameters = {'subportfolio','contract_month','contract_number','derivative_type','strike','barrier_level'};

PricingType = 'traders';

OutputFields = {'price','pricemove','active_lots','delta'};

OutFilename = generate_sensitivity_report(InputFilterValues,InputScenarios,SummmaryParameters,PricingType,OutputFields);