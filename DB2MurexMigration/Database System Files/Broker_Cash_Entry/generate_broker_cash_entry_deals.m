function [OutErrorMsg ,OutXLSName ] = generate_broker_cash_entry_deals(InBUName,Temp_Settle_Date)

%%% Project Name        :   Broker Module
%%% Module Name         :   Broker Cash Entry
%%% Matlab Version(s)   :   R2016b
%%% Company             :   Invenio Commodity Services Private Limited

%%% Author Name         :   INVENIO
%%% Date_Time           :   24 Jan 2019 / 12:12:12
%
%%% [OutErrorMsg ,OutXLSName ] = generate_broker_cash_entry_deals('qf1','2019-01-03')

try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['broker_cash_entry_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = {Temp_Settle_Date};
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    %%% Fetch data from Broker Cash Entry Recon Column Master Table
    Broker_Recon_Table = ['broker_recon_report_table_',char(InBUName)];
    SqlQuery_Broker_Recon_Table_Data = ['select * from ',char(Broker_Recon_Table),' where st_date = ''',char(Settele_date),''' '] ;
    [Broker_Recon_Table_Cols,Broker_Recon_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Recon_Table_Data);

    if strcmpi(Broker_Recon_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Broker_Recon_Table)]} ;
        return;
    end
    Broker_Recon_Column_Table_Data = cell2dataset([Broker_Recon_Table_Cols;Broker_Recon_Table_Data]); %%% convert cell to dataset
    
    %%% Fetch data from Broker Cash Entry Portfolio_Counterparty_Master Table
    Boker_Cash_Entry_Master_Table = ['broker_cashentry_master_table_',char(InBUName)];
    SqlQuery_Boker_Cash_Entry_Master_Table = ['select * from ',char(Boker_Cash_Entry_Master_Table)] ;
    [Boker_Cash_Entry_Master_Table_Cols,Boker_Cash_Entry_Master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Boker_Cash_Entry_Master_Table);

    if strcmpi(Boker_Cash_Entry_Master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Boker_Cash_Entry_Master_Table)]} ;
        return;
    end 
    Boker_Cash_Entry_Master_Data = cell2dataset([Boker_Cash_Entry_Master_Table_Cols;Boker_Cash_Entry_Master_Table_Data]); %%% convert cell to dataset
    
    %%% Fetch data from Broker Cash Entry Recon Column Master Table
    BCE_Recon_Master_Table = ['broker_cashentry_recon_columns_master_table_',char(InBUName)];
    SqlQuery_BCE_Recon_Master_Table_Data = ['select * from ',char(BCE_Recon_Master_Table)] ;
    [BCE_Recon_Master_Table_Cols,BCE_Recon_Master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_BCE_Recon_Master_Table_Data);

    if strcmpi(BCE_Recon_Master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(BCE_Recon_Master_Table)]} ;
        return;
    end
    BCE_Recon_Master_Data = cell2dataset([BCE_Recon_Master_Table_Cols;BCE_Recon_Master_Table_Data]); %%% convert cell to dataset
    
    %%% user required columns data from Recon Column Mapping Table and
    Index_ones = ismember(BCE_Recon_Master_Data.sum_consider,1);
    BCE_Recon_Master_Req_Col = BCE_Recon_Master_Data.broker_recon_column(Index_ones); 
    
    %%% Fetch data from Underlying Settle Value Table
    Underlying_settle_value_Table = 'underlying_settle_value_table';
    SqlQuery_Underlying_settle_value = ['select underlying_id, settle_value, settlement_date from ',char(Underlying_settle_value_Table),' where settlement_date=''',char(Settele_date),''' and underlying_id like ''%','spot','%'' '];
    [~,Underlying_settle_value_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Underlying_settle_value);

    if strcmpi(Underlying_settle_value_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Underlying_settle_value_Table),' for ',char(Settele_date)]} ;
        return;
    end
    
    %%%% Broker Cash Entry and Broker Recon Mapping
    %%% Comparing Account Number and Currency
    unique_account = unique(Broker_Recon_Column_Table_Data.account);
    unique_currency = unique(strtrim(Broker_Recon_Column_Table_Data.currency));

    [row,~] = size(unique_account);
    [row1,~] = size(unique_currency);
    Broker_Cash_Entry_Required_Data = []; Overall_ErrorMsg = [];
    for i = 1:1:row
        for j = 1:1:row1
        single_acc_number = unique_account(i);
        Single_currency = unique_currency(j);
        
        Matched_Index = find(strcmpi(single_acc_number,Boker_Cash_Entry_Master_Data.account_number) & strcmpi(Single_currency,Boker_Cash_Entry_Master_Data.currency));
        if ~isempty(Matched_Index)
            counterparty_parent = Boker_Cash_Entry_Master_Data.counterparty_parent(Matched_Index);
            subportfolio = Boker_Cash_Entry_Master_Data.portfolio(Matched_Index);
            instrument = Boker_Cash_Entry_Master_Data.instrument(Matched_Index);
            security_id = Boker_Cash_Entry_Master_Data.security_id(Matched_Index);
            underlying_spot_id = Boker_Cash_Entry_Master_Data.spot(Matched_Index);

            %%% comparing underlying_id
            Spot_Index = strcmpi(underlying_spot_id,Underlying_settle_value_Data(:,1));
            if ~isempty(Spot_Index)
                Spot_Value = Underlying_settle_value_Data(Spot_Index,2);
            else
                Overall_ErrorMsg = [Overall_ErrorMsg;{['underlying_id: ',char(underlying_spot_id),' is not found in ',char(Underlying_settle_value_Table)]}];
                continue;
            end

            Matched_data = Broker_Recon_Column_Table_Data(strcmpi(single_acc_number,Broker_Recon_Column_Table_Data.account) & strcmpi(Single_currency,Broker_Recon_Column_Table_Data.currency),:);

            if isempty(Matched_data)
                continue;
            end
            %%% required fields to sum at Broker Cash Entry Table
            Boker_cash_entry_Sum_fields = Matched_data(:,cellStrfind_exact(Broker_Recon_Table_Cols,BCE_Recon_Master_Req_Col));
            
            %%% Summation of all Broker Recon Column Mapping Table fields 
            Total_Cash_Sum_lots = sum(Boker_cash_entry_Sum_fields.bank_chgs) + sum(Boker_cash_entry_Sum_fields.call_back) + sum(Boker_cash_entry_Sum_fields.cash_adj) + ...
                        sum(Boker_cash_entry_Sum_fields.cash_tran_btw_act) + sum(Boker_cash_entry_Sum_fields.commision) + ...
                        sum(Boker_cash_entry_Sum_fields.fx_fixing_chg) + sum(Boker_cash_entry_Sum_fields.give_up_comm) + sum(Boker_cash_entry_Sum_fields.gst) + ...
                        sum(Boker_cash_entry_Sum_fields.interest) +  sum(Boker_cash_entry_Sum_fields.loan) + sum(Boker_cash_entry_Sum_fields.margin_call) + ...
                        sum(Boker_cash_entry_Sum_fields.payment_tender) + sum(Boker_cash_entry_Sum_fields.platform_chg) + sum(Boker_cash_entry_Sum_fields.receipt_tender) + ...
                        sum(Boker_cash_entry_Sum_fields.tenders) + sum(Boker_cash_entry_Sum_fields.tender_fees); 
                
                %%% Check the condition is first 3 char in instrument is USD or not
                current_instrument = char(instrument);
                if current_instrument(1:3) == 'USD'
                    %%% Check the condition is from 4th to 6th char in instrument is USD or not
                    if current_instrument(4:6) == 'USD'   
                        Broker_Cash_Entry_Lots = Total_Cash_Sum_lots;
                    else
                        Broker_Cash_Entry_Lots = Total_Cash_Sum_lots ./ (-1 .* cell2mat(Spot_Value));  
                    end
                else  
                    Broker_Cash_Entry_Lots = Total_Cash_Sum_lots;  
                end
            other_remarks = {' '};
            
            %%%% Required fields to consolidation part
            User_Required_BCE_Data = [counterparty_parent, subportfolio, instrument, security_id, Spot_Value, Broker_Cash_Entry_Lots, other_remarks];
            Required_BCE_Data = User_Required_BCE_Data;
            
            %%% Summation of all Currency_Conv column fields
            if any(Boker_cash_entry_Sum_fields.currency_conv)
                Currency_Conv_Sum_lots = sum(Boker_cash_entry_Sum_fields.currency_conv);
                Spot_Value_premium = {NaN};   %% or Spot_Value = {0};
                other_remarks = {'Currency conversion found'};
                Currency_Conv_BCE_Data = [counterparty_parent, subportfolio, instrument, security_id, Spot_Value_premium, Currency_Conv_Sum_lots, other_remarks];
                Required_BCE_Data = [User_Required_BCE_Data;Currency_Conv_BCE_Data];
            end

            %%% REquired Data to consolidation part
            Broker_Cash_Entry_Required_Data = vertcat(Broker_Cash_Entry_Required_Data,Required_BCE_Data);
        
            else
                Overall_ErrorMsg = [Overall_ErrorMsg;{['No mapping found with broker data for corresponding Account NUmber: ',char(single_acc_number),' and Currency: ',char(Single_currency)]}];
                continue;
        end
        end 
    end
    
    %%% consolidation part of Broker Cash Entry Data
    User_Required_BCE_Header = {'counterparty_parent', 'subportfolio', 'instrument', 'security_id', 'Spot_Value', 'Broker_Cash_Entry_Lots', 'other_remarks'};
    Consolidate_BCF_Header =  User_Required_BCE_Header;
    Consolidate_BCF_Data = Broker_Cash_Entry_Required_Data;

    UniqueFields = {'counterparty_parent', 'subportfolio', 'instrument', 'security_id', 'Spot_Value'}; 
    SumFields = {'Broker_Cash_Entry_Lots'};
    OutputFields = Consolidate_BCF_Header;
    WeightedAverageFields = [];
    [Consolidate_BCF_Data_Header,BCF_Data_Consolidate] = consolidatedata(Consolidate_BCF_Header, Consolidate_BCF_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    
    %%% delete the lots row, if original_lots value is zero
    Index_Zero = eq(cell2mat(BCF_Data_Consolidate(:,6)),0);
    BCF_Data_Consolidate(Index_Zero,:) = [];
    
    %%% BCE consolidation Header and Data
    Broker_Cash_Entry_Data = [Consolidate_BCF_Data_Header;BCF_Data_Consolidate];
    Broker_Cash_Entry_Data = cell2dataset(Broker_Cash_Entry_Data);      %%% convert cell to dataset
    
    %%% Based on +ve or -ve sign of lots,bought/sold need to be decided
    sign_lots = sign(Broker_Cash_Entry_Data.Broker_Cash_Entry_Lots);
    Index_positive =  eq(sign_lots,1);
    Index_negative =  eq(sign_lots,-1);
    
    BCE_market_action(Index_positive,1) = {'bought'};
    BCE_market_action(Index_negative,1) = {'sold'};
        
    %%% get last trade ID without prefix
    [~,~,DBTradeId]= getLastTradeId(InBUName);

    SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
    TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
    TempTradeId = DBTradeId;
    if(isempty(TempTradeId))
        TempTradeId = 0;      %% Default
    end
    NumRows = size(Broker_Cash_Entry_Data.counterparty_parent,1);
    NTradeId = (TempTradeId+1:1:TempTradeId+NumRows)';
    TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
    TradeId = strrep(TradeId,' ','');  %% Remove Spaces

    %%%% user required fields  
    trade_id = TradeId;
    parent_trade_id = TradeId;
    transaction_date = repmat(Settele_date,size(Broker_Cash_Entry_Data,1),1);
    subportfolio = Broker_Cash_Entry_Data.subportfolio;
    instrument = Broker_Cash_Entry_Data.instrument;
    trade_group_type = repmat({'FXD'},size(Broker_Cash_Entry_Data,1),1);
    security_id = Broker_Cash_Entry_Data.security_id;
    market_action = BCE_market_action;
    original_lots = num2cell(Broker_Cash_Entry_Data.Broker_Cash_Entry_Lots);
    premium = num2cell(Broker_Cash_Entry_Data.Spot_Value);
    counterparty_parent = Broker_Cash_Entry_Data.counterparty_parent; 
    is_netting_done = repmat({0},size(Broker_Cash_Entry_Data,1),1);
    lots_to_be_netted = num2cell(Broker_Cash_Entry_Data.Broker_Cash_Entry_Lots);
    deal_status = repmat({'live'},size(Broker_Cash_Entry_Data,1),1);
    brokerage_rate_type = repmat({'NORMAL'},size(Broker_Cash_Entry_Data,1),1);
    other_remarks = cellstr(char(Broker_Cash_Entry_Data.other_remarks));
    
    counterparty_bu = cell(size(Broker_Cash_Entry_Data,1),1);
    counterparty_entity = cell(size(Broker_Cash_Entry_Data,1),1);
    execution_broker = cell(size(Broker_Cash_Entry_Data,1),1);
    clearing_broker = cell(size(Broker_Cash_Entry_Data,1),1);
    reconed_with_clearer = cell(size(Broker_Cash_Entry_Data,1),1);
    reconed_completed_by = cell(size(Broker_Cash_Entry_Data,1),1);
    recon_datetimestamp = cell(size(Broker_Cash_Entry_Data,1),1);
    contract_number = cell(size(Broker_Cash_Entry_Data,1),1);
    subcontract_number = cell(size(Broker_Cash_Entry_Data,1),1);
    opt_periodicity = cell(size(Broker_Cash_Entry_Data,1),1);
    date_to_be_fixed = cell(size(Broker_Cash_Entry_Data,1),1);
    trader_name = cell(size(Broker_Cash_Entry_Data,1),1);
    traded_timestamp = cell(size(Broker_Cash_Entry_Data,1),1);
    exe_type = cell(size(Broker_Cash_Entry_Data,1),1);
    spcl_rate = cell(size(Broker_Cash_Entry_Data,1),1);
    version_no = cell(size(Broker_Cash_Entry_Data,1),1);
    version_created_by = cell(size(Broker_Cash_Entry_Data,1),1);
    version_timestamp = cell(size(Broker_Cash_Entry_Data,1),1);
    version_comments = cell(size(Broker_Cash_Entry_Data,1),1);
    ops_unique = cell(size(Broker_Cash_Entry_Data,1),1);
    broker_lots = cell(size(Broker_Cash_Entry_Data,1),1);
    ops_action = cell(size(Broker_Cash_Entry_Data,1),1);
 
    %%% User want Data (Final Broker Cash Entry Header)
    Broker_Cash_Entry_Final_Header = {'trade_id', 'parent_trade_id', 'transaction_date', 'subportfolio', 'instrument', 'trade_group_type', 'security_id', 'market_action', ...
                                'original_lots', 'premium', 'counterparty_parent', 'counterparty_bu', 'counterparty_entity', 'execution_broker', 'clearing_broker', ...
                                'reconed_with_clearer', 'reconed_completed_by', 'recon_datetimestamp', 'contract_number', 'subcontract_number', 'opt_periodicity', ...
                                'is_netting_done', 'lots_to_be_netted', 'date_to_be_fixed', 'trader_name', 'traded_timestamp', 'deal_status', 'exe_type', 'brokerage_rate_type', ...
                                'spcl_rate', 'other_remarks', 'version_no', 'version_created_by', 'version_timestamp', 'version_comments', 'ops_unique', 'broker_lots', 'ops_action'};
    
    %%% User want Data (Final Broker Cash Entry Data)
    Broker_Cash_Entry_Final_Data = [trade_id, parent_trade_id, transaction_date, subportfolio, instrument, trade_group_type, security_id, market_action, ...
                                original_lots, premium, counterparty_parent, counterparty_bu, counterparty_entity, execution_broker, clearing_broker, ...
                                reconed_with_clearer, reconed_completed_by, recon_datetimestamp, contract_number, subcontract_number, opt_periodicity, ...
                                is_netting_done, lots_to_be_netted, date_to_be_fixed, trader_name, traded_timestamp, deal_status, exe_type, brokerage_rate_type, ...
                                spcl_rate, other_remarks, version_no, version_created_by, version_timestamp, version_comments, ops_unique, broker_lots, ops_action];
    
    %%% Final cell data with Header and Data & write the data into excel sheet
    Total_Broker_Cash_Entry_Data = [Broker_Cash_Entry_Final_Header;Broker_Cash_Entry_Final_Data];
    xlswrite(OutXLSName,Total_Broker_Cash_Entry_Data,'Broker_Cash_Entry_Data');

    if ~isempty(Overall_ErrorMsg)
        xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Sheet');
        %%%% Active user sheet
        xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Error_Sheet'); 
    end

    %%%% Delete Empty sheets and active user sheet
    xls_delete_sheets(fullfile(pwd,OutXLSName));
 
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);

end
