function [OutErrorMsg,OutFilename] = generate_qf1_fx_forwards_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('QF1-FX Forwards');
    
    ViewName = 'helper_funds_reports_view' ; %% 'helper_settle_pricing_subportfoliovalues_view'
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')
        
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FXD'});
        Data = Data(ispresent_FUT_OFUT,:);
        
        %         Find_CallPut_Col = cellStrfind(ColNames,'call_put_id');
        %         Get_CallPut = Data(:,Find_CallPut_Col);
        %         ispresent_Call = cellStrfind(Get_CallPut,'c');     %% {'call','put'}
        %         Data(ispresent_Call,Find_CallPut_Col) = {'call'};
        %         ispresent_Put = cellStrfind(Get_CallPut,'p');
        %         Data(ispresent_Put,Find_CallPut_Col) = {'put'};        %% Get 'Call/Put' Row only
        
        %         ispresent_Call_Put = sort([ispresent_Call;ispresent_Put]);
        %         Data = Data(ispresent_Call_Put,:);         %% Get 'Call/Put' Row only
        
        UniqueFields = {'asset_class','product_code','product_name','portfolio','maturity_date'};
        SumFields = {'mtm_usd','settle_delta_1'};
        OutputFields = {'asset_class','product_code','product_name','portfolio','maturity_date',...
            'mtm_usd','settle_delta_1','nc_factor','settle_price'}; 
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        OutHeader = {'Type','Product Code','Product','Portfolio','Maturity','Book_Value','Delta','Spot FX Rate','FX Forward Rate'};
        
        xlswrite(OutFilename,[OutHeader;OutData]);
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end