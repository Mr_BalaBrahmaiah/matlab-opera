function [XLS_File] = Make_OPS_Cotton_Summary_Report(XLS_File,Delete_Sheet_Name,SummmaryParameters,InBUName)
%%
[~,~,RawData] = xlsread(XLS_File,Delete_Sheet_Name) ; %% 'Summary Report'

Header = RawData(1,:);
RawData = RawData(2:end,:);

%%

PriceMove_Col = cellStrfind_exact(Header,{'pricemove'}) ;
if(strcmpi(InBUName,'cot'))
PriceTick_Col = cellStrfind_exact(Header,{'pricemove_in_percentage'});
else
  PriceTick_Col = cellStrfind_exact(Header,{'pricemove_in_pricetick'});
end

M2M_Col = cellStrfind_exact(Header,{'m2m_usd'});

Unique_Col = cellStrfind_exact(Header,SummmaryParameters);
[Unique_CellArray,Index,~] = uniqueRowsCA(RawData(:,Unique_Col));

%%

Overall_Array = [];

for i = 1 : size(Index,1)
    
    if(i==1)
        Start_Index = 1;
        End_Index = Index(i);
    else
        Start_Index = Index(i-1) + 1;
        End_Index = Index(i);
    end
    
    Current_Array = RawData(Start_Index:End_Index , :) ;
    PriceMoveTick_Index = find(cellfun(@(x) x==0, Current_Array(:,PriceMove_Col), 'UniformOutput', 1) & cellfun(@(x) x==0, Current_Array(:,PriceTick_Col), 'UniformOutput', 1));
    
    Temp_Cell = cell(size(Current_Array,1),1) ;
    Temp_Cell(:,:) = Current_Array(PriceMoveTick_Index,M2M_Col); %% Default Value
    
    Calculate_Diff_M2M = num2cell( cell2mat(Current_Array(:,M2M_Col)) - cell2mat(Temp_Cell) );
    
    Temp_Array = [Current_Array , Calculate_Diff_M2M] ;
    Overall_Array = [Overall_Array ; Temp_Array];
end


%% Delete Exist Summary Report

xls_delete_sheets(XLS_File,{Delete_Sheet_Name}) ;

%% Output 
Header = [Header , {'diff_m2m_usd'}] ;

xlswrite(XLS_File,[Header;Overall_Array],Delete_Sheet_Name);




