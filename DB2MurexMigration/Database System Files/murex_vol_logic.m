SettleVolData = cell2dataset(SettleVolData);
uniqueComb = cellstr([char(SettleVolData.VolId),num2str(SettleVolData.Strike)]);
UniqueVolComb = unique(uniqueComb,'stable');

CalcSettleVols = SettleVolData.SettleVol;

for i = 1:length(UniqueVolComb)
IdxITM       = logical(zeros(size(uniqueComb)));   
IdxITMCallId = logical(zeros(size(uniqueComb)));
IdxITMPutId  = logical(zeros(size(uniqueComb)));

IdxOTM       = logical(zeros(size(uniqueComb)));   
IdxOTMCallId = logical(zeros(size(uniqueComb)));
IdxOTMPutId  = logical(zeros(size(uniqueComb)));

IdxComb = strcmpi(UniqueVolComb(i),uniqueComb);

% if strike > asset_price, use put_vol
IdxITM(IdxComb)       = SettleVolData.Strike(IdxComb) > SettleVolData.AssetPrice(IdxComb);
IdxITMPutId(IdxComb)  = IdxITM(IdxComb) & strcmpi('P',SettleVolData.Call_Put_Id(IdxComb));
IdxITMCallId(IdxComb) = IdxITM(IdxComb) & strcmpi('C',SettleVolData.Call_Put_Id(IdxComb));
if any(IdxITMCallId) && any(IdxITMPutId)
    CalcSettleVols(IdxITMCallId) = CalcSettleVols(IdxITMPutId);
end

% if strike <= asset_price, use call_vol
IdxOTM(IdxComb)       = SettleVolData.Strike(IdxComb) <= SettleVolData.AssetPrice(IdxComb);
IdxOTMPutId(IdxComb)  = IdxOTM(IdxComb) & strcmpi('P',SettleVolData.Call_Put_Id(IdxComb));
IdxOTMCallId(IdxComb) = IdxOTM(IdxComb) & strcmpi('C',SettleVolData.Call_Put_Id(IdxComb));
if any(IdxOTMPutId) && any(IdxOTMCallId)
    CalcSettleVols(IdxOTMPutId) = CalcSettleVols(IdxOTMCallId);
end

end

SettleVolData.CalcSettleVols = CalcSettleVols;


