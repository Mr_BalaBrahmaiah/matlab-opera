function [OutErrorMsg,TradersReportFilename] = generate_detailed_traders_report(InBUName)
% generate the detailed traders report format with counterparty and contract months
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/02/13 04:39:52 $
%  $Revision: 1.5 $
%

OutErrorMsg = {'No errors'};

try    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    %     ViewName = 'helper_5_1_traders_pricing_subportfoliovalues_view';
    ViewName = 'helper_traders_pricing_subportfoliovalues_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    TradersData = cell2dataset([ColNames;Data]);
    clear Data;
    
    %     ViewName = 'helper_4_1_settle_pricing_subportfoliovalues_view';
    ViewName = 'helper_settle_pricing_subportfoliovalues_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    SettleData = cell2dataset([ColNames;Data]);
    clear Data;
    
    ValuationDate = TradersData.value_date{1};
    
    % generate Traders report
    Subportfolio = unique(upper(TradersData.subportfolio));
    TradersReportData = cell(length(Subportfolio),10);
    
    NumCntr = 1;
    for iS = 1:length(Subportfolio)
        subportfolio_name = Subportfolio(iS);
        IdxTraders = strcmpi(subportfolio_name,TradersData.subportfolio);
        IdxSettle  = strcmpi(subportfolio_name,SettleData.subportfolio);
        
        ContractMonth = unique(upper(TradersData.contract_month(IdxTraders)));
        
        for iCM = 1:length(ContractMonth)
            
            IdxTradersCM = zeros(size(IdxTraders)); IdxTradersCM = logical(IdxTradersCM); %#ok<*LOGL>
            IdxSettleCM  = zeros(size(IdxSettle)); IdxSettleCM = logical(IdxSettleCM); %#ok<*LOGL>
            
            IdxTradersCM(IdxTraders) = strcmpi(ContractMonth{iCM},TradersData.contract_month(IdxTraders));
            IdxSettleCM(IdxSettle)   = strcmpi(ContractMonth{iCM},SettleData.contract_month(IdxSettle));
            
            Counterparty = unique(upper(TradersData.counterparty_parent(IdxTradersCM)));
            
            for iCP = 1:length(Counterparty)
                IdxTradersCP = zeros(size(IdxTraders)); IdxTradersCP = logical(IdxTradersCP); %#ok<*LOGL>
                IdxSettleCP = zeros(size(IdxSettle));   IdxSettleCP = logical(IdxSettleCP); %#ok<*LOGL>
                
                IdxTradersCP(IdxTradersCM) = strcmpi(Counterparty{iCP},TradersData.counterparty_parent(IdxTradersCM));
                IdxSettleCP(IdxSettleCM)   = strcmpi(Counterparty{iCP},SettleData.counterparty_parent(IdxSettleCM));
                
                TradersReportData(NumCntr,1) = subportfolio_name;
                TradersReportData(NumCntr,2) = ContractMonth(iCM);
                TradersReportData(NumCntr,3) = Counterparty(iCP);
                TradersReportData(NumCntr,4) = num2cell(sum(TradersData.traders_delta_1(IdxTradersCP)) + sum(TradersData.traders_delta_2(IdxTradersCP)));
                TradersReportData(NumCntr,5) = num2cell(sum(TradersData.traders_gamma_11(IdxTradersCP)) + sum(TradersData.traders_gamma_12(IdxTradersCP)) + ...
                    sum(TradersData.traders_gamma_21(IdxTradersCP)) + sum(TradersData.traders_gamma_22(IdxTradersCP)));
                TradersReportData(NumCntr,6) = num2cell(sum(TradersData.traders_theta(IdxTradersCP)) * (7/5));
                TradersReportData(NumCntr,7) = num2cell(sum(TradersData.traders_vega_1(IdxTradersCP)) + sum(TradersData.traders_vega_2(IdxTradersCP)));
                TradersReportData(NumCntr,8) = num2cell(sum(TradersData.bv_usd(IdxTradersCP)));
                TradersReportData(NumCntr,10) = num2cell(sum(SettleData.mtm_usd(IdxSettleCP)));
                TradersReportData(NumCntr,9) = num2cell(cell2mat(TradersReportData(NumCntr,10)) - cell2mat(TradersReportData(NumCntr,8)));
                TradersReportData(NumCntr,11) = num2cell(sum(TradersData.bv_nc(IdxTradersCP)));
                TradersReportData(NumCntr,12) = num2cell(sum(SettleData.mtm_nc(IdxSettleCP)));
                
                NumCntr = NumCntr + 1;
            end
        end
    end
    
    TradersReportHeader = {'subportfolio','contract_month','counterparty','delta','gamma','theta','vega','bv-usd','mkt-us','mtm-usd','bv-nc','mtm-nc'};
    % TradersReportFilename = ['Traders_Report_Monthwise_',datestr(ValuationDate,'dd-mmm-yyyy'),'.csv'];
    % customcsvwrite(TradersReportFilename,TradersReportData,TradersReportHeader);
    
    TradersReportFilename = getXLSFilename('Traders_Report_Monthwise');
    xlswrite(TradersReportFilename,[TradersReportHeader;TradersReportData]);
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    TradersReportFilename = '';
end