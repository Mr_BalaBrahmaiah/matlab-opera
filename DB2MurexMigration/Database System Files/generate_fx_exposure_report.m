function [OutErrorMsg ,OutXLSName ] = generate_fx_exposure_report(InBUName)

%%% Project Name        :   FX Exposure
%%% Module Name         :   FX Exp Report and CTRM FX Exposure
%%% Matlab Version(s)   :   R2016b
%%% Company             :   Invenio Commodity Services Private Limited

%%% Author Name         :   INVENIO
%%% Date_Time           :   16 Sep 2019 / 15:08:08
%
%%% [OutErrorMsg ,OutXLSName ] = generate_fx_exposure_report('usg')

try    
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['fx_exposure_report_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    Settlement_date = fetch(ObjDB,'select settlement_date from valuation_date_table');

    %%% Fetch data from helper_traders_p_and_g_view View 
    ViewName = 'helper_settle_p_and_g_view';
    SqlQuery = ['select * from ',char(ViewName),' where deal_status = ''','live',''' and currency not like ''','USD',''' '];
    [View_Header,View_Data] = read_from_database(ViewName,0,SqlQuery,InBUName);
    
    if strcmpi(View_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(ViewName)]} ;
        return;
    end

    %%% Fetch data from product master table
    Product_master_Table = 'product_master_table';
    SqlQuery_Product_master_Table = ['select asset_class,invenio_product_code,bbg_product_code,product_name,exchange_name from ',char(Product_master_Table)];
    [Product_master_Table_Cols,Product_master_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Product_master_Table);

    if strcmpi(Product_master_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Product_master_Table)]} ;
        return;
    end
    
    Product_master_Tbl = cell2table(Product_master_Table_Data,'VariableNames',Product_master_Table_Cols);  %% Convert to table
    
    %%% Fetch data from deal_ticket_table
    Deal_Ticket_Table = ['deal_ticket_table_with_latest_versions_view_',char(InBUName)];
    SqlQuery_Deal_Ticket_Table = ['select trade_id,trader_name from ',char(Deal_Ticket_Table)] ;
    [Deal_Ticket_Table_Cols,Deal_Ticket_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Deal_Ticket_Table);

    if strcmpi(Deal_Ticket_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Deal_Ticket_Table)]} ;
        return;
    end 
    
    Deal_Ticket_Tab_Data = cell2table(Deal_Ticket_Table_Data,'VariableNames',Deal_Ticket_Table_Cols);  %% Convert to table

    %%% Fetch data from fx_exposure_sap_master_table
    FX_Exposure_Sap = 'fx_exposure_sap_master_table';
    SqlQuery_FX_Exposure_Sap = ['select * from ',char(FX_Exposure_Sap),' where business_unit = ''',char(InBUName),''' '] ;
    [FX_Exposure_Sap_Cols,FX_Exposure_Sap_Data] = Fetch_DB_Data(ObjDB,SqlQuery_FX_Exposure_Sap);

    if strcmpi(FX_Exposure_Sap_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(FX_Exposure_Sap),' for BU: ',char(upper(InBUName))]} ;
        return;
    end 
    
    FX_Exposure_Sap_Tab_Data = cell2table(FX_Exposure_Sap_Data,'VariableNames',FX_Exposure_Sap_Cols);  %% Convert to table
    
    %%% Required output sheets headers
    Future_Header = {'Company_Code','Business_Area','Profit_Centre','Legal_Entity_Code','Legal_Entity','Product_Code','Product', ...
        'Buy_Sell','Trade_Date','Trade_Ref','Exchange','Broker_Code','Broker','Unit_Code','Unit_Name','Strategy','Terminal_Month', ...
        'Contract_Currency','Quantity_MT','Price','Price_MT','Trade_Value','Trader','Modified_Date','Date_of_Upload'};

%     Buy_Header = {'Company_Code','Business_Area','Profit_Centre','Legal_Entity_Code','Legal_Entity','Product_Code','Product', ...
%         'Buy_Sell','Trade_Date','Trade_Ref','Exchange','Broker_Code','Broker','Unit_Code','Unit_Name','Strategy','Terminal_Month', ...
%         'Contract_Currency','Quantity_MT','Price','Price_MT','Trade_Value','Trader','Modified_Date','Date_of_Upload'};
%    
    Option_Header = {'Company_Code','Business_Area','Profit_Centre','Legal_Entity_Code','Legal_Entity','Product_Code','Product','Buy_Sell', ...
        'Trade_Date','Trade_Ref','Exchange','Broker_Code','Broker','Unit_Code','Unit_Name','Strategy','Terminal_Month','Put_Call', ...
        'Strike','Contract_Currency','Quantity_MT','Delta','Delta_Quantity_MT','Premium','Premium_MT','Trade_Value', ...
        'Market_Rate','Market_Value','Trader','Modified_Date','Date_of_Upload'};
    
    %%% Get the column position value
    Pos_trade_id        = cellStrfind_exact(View_Header, {'trade_id'});
    Pos_asset_class     = cellStrfind_exact(View_Header, {'asset_class'});
    Pos_product_code    = cellStrfind_exact(View_Header, {'product_code'});
    Pos_market_action   = cellStrfind_exact(View_Header, {'market_action'});
    Pos_transaction_date = cellStrfind_exact(View_Header, {'transaction_date'});
    Pos_execution_broker = cellStrfind_exact(View_Header, {'execution_broker'});
    Pos_portfolio       = cellStrfind_exact(View_Header, {'portfolio'});
    Pos_group_type      = cellStrfind_exact(View_Header, {'group_type'});   
    Pos_currency        = cellStrfind_exact(View_Header, {'currency'});
    Pos_active_lots     = cellStrfind_exact(View_Header, {'active_lots'});
    Pos_lot_mult1       = cellStrfind_exact(View_Header, {'lot_mult1'});
    Pos_settle_price    = cellStrfind_exact(View_Header, {'settle_price'});
    Pos_contract_month   = cellStrfind_exact(View_Header, {'contract_month'});
    Pos_derivative_type   = cellStrfind_exact(View_Header, {'derivative_type'});
    Pos_Strike   = cellStrfind_exact(View_Header, {'Strike'});
    Pos_settle_delta_1   = cellStrfind_exact(View_Header, {'settle_delta_1'});
    Pos_current_premium   = cellStrfind_exact(View_Header, {'current_premium'});  
    
    %%% Required fileds from view
    trade_id_data = View_Data(:,Pos_trade_id);
    asset_class_data = View_Data(:,Pos_asset_class);
    product_code_data = View_Data(:,Pos_product_code);
    %Legal_Entity_Code = product_code_data;
    %Legal_Entity = repmat({'OLAM INT LTD'},size(View_Data,1),1);
    Product_Code = repmat(cellstr(upper(InBUName)),size(View_Data,1),1);
    
    %%% Buy Sell
    market_action = View_Data(:,Pos_market_action);
    Sell_index = strcmpi('sold',market_action);
    Buy_Sell(Sell_index,1) = {'Sell'};
    Buy_Sell(~Sell_index,1) = {'Buy'};
    %Trade_Date = cellstr(datestr(datenum(View_Data(:,Pos_transaction_date),'yyyy-mm-dd'),'dd-mmm-yy'));
    Trade_Date = View_Data(:,Pos_transaction_date);
    Trade_Ref = trade_id_data;
    Broker = View_Data(:,Pos_execution_broker);
    Unit_Name = View_Data(:,Pos_portfolio);
    Strategy = View_Data(:,Pos_group_type);

    %%% Currency
    Contract_Currency = View_Data(:,Pos_currency);
    Quantity_MT = num2cell(abs(cell2mat(View_Data(:,Pos_active_lots))) .* cell2mat(View_Data(:,Pos_lot_mult1)));
    Price = View_Data(:,Pos_settle_price);
    Price_MT = num2cell(cell2mat(Price) .* cell2mat(View_Data(:,Pos_lot_mult1)));
    Trade_Value = num2cell(cell2mat(Quantity_MT) .* cell2mat(Price_MT));
    
    %%% Option Trades need extra data
    Put_Call = View_Data(:,Pos_derivative_type);
    Strike = View_Data(:,Pos_Strike);
    Delta = View_Data(:,Pos_settle_delta_1);
    Delta_Quantity_MT = num2cell(cell2mat(Delta) .* cell2mat(View_Data(:,Pos_lot_mult1)));
    Premium = View_Data(:,Pos_current_premium);
    Premium_MT = num2cell(cell2mat(Premium) .* cell2mat(View_Data(:,Pos_lot_mult1)));
    Option_Value = num2cell(cell2mat(Quantity_MT) .* cell2mat(Premium_MT));
    Market_Rate = Price;
    Market_Value = num2cell(cell2mat(Quantity_MT) .* cell2mat(Market_Rate) .* cell2mat(View_Data(:,Pos_lot_mult1)));
    
    %%% View Data mapping with Master Table and deal ticket table and get
    %%% the trade_name, product_name and exchange_name
    Contract_month = strtok(View_Data(:,Pos_contract_month),'.');
    Month = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec'};
    MonthCode  = {'F';'G';'H';'J';'K';'M';'N';'Q';'U';'V';'X';'Z'};
    
    [row,~] = size(View_Data);
    for i = 1:1:row
        current_trade_id = trade_id_data(i);
        current_asset_class = asset_class_data(i);
        current_product_code = product_code_data(i);
        
        Index_tradeid = strcmpi(current_trade_id,Deal_Ticket_Tab_Data.trade_id);
        if any(Index_tradeid)
            trader_name(i,1) = Deal_Ticket_Tab_Data.trader_name(strcmpi(current_trade_id,Deal_Ticket_Tab_Data.trade_id)); 
        else
           trader_name(i,1) = {''};
        end

        Index_match = strcmpi(current_asset_class,Product_master_Tbl.asset_class) & strcmpi(current_product_code,Product_master_Tbl.bbg_product_code);
        if any(Index_match)
        product_name(i,1) = Product_master_Tbl.product_name(Index_match);
        exchange_name(i,1) = Product_master_Tbl.exchange_name(Index_match);
        else
            product_name(i,1) = {''};
            exchange_name(i,1) = {''};
        end
        
        %%% Month
        Con_month = char(Contract_month(i));
        if length(Con_month) == 2
            Terminal_Month(i,1) = cellstr([Month{strncmpi(Con_month,MonthCode,1)},strcat('1',Con_month(end))]); 
        else
            Terminal_Month(i,1) = cellstr([Month{strncmpi(Con_month,MonthCode,1)},Con_month(2:end)]); 
        end
    end
    Terminal_Month = cellstr(datestr(datenum(Terminal_Month,'mmmyy'),'mmmyy'));
    
    ProductName = product_name;
    Exchange = exchange_name;
    Trader = trader_name;
    
    %%% Empty field cells
    Broker_Code = cell(size(View_Data,1),1); 
    Unit_Code = cell(size(View_Data,1),1); 
    Modified_Date = repmat(cellstr(datestr(now,'mm/dd/yyyy HH:MM:SS PM')),size(View_Data,1),1);

    Profit_Centre = repmat(FX_Exposure_Sap_Tab_Data.profit_centre,size(View_Data,1),1);
    Business_Area = FX_Exposure_Sap_Tab_Data.sap_business_area;
    SAPBusiness_Area = strcat('''',Business_Area);
    SAP_Business_Area = repmat(SAPBusiness_Area,size(View_Data,1),1);
    %Legal_Entity_name = repmat(FX_Exposure_Sap_Tab_Data.entity_name,size(View_Data,1),1);
    
    Company_code = repmat({'SG00'},size(View_Data,1),1);
    Legal_Entity_Code1 = repmat({['''','001']},size(View_Data,1),1);
    Legal_Entity_name = repmat({'OLAM INT LTD'},size(View_Data,1),1);
    
    %%% Both Fut & Opt Data
    User_Header ={'Company_Code','Business_Area','Profit_Centre','Legal_Entity_Code','Legal_Entity','Product_Code','Product','Buy_Sell', ...
        'Trade_Date','Trade_Ref','Exchange','Broker_Code','Broker','Unit_Code','Unit_Name','Strategy','Terminal_Month','Put_Call', ...
        'Strike','Contract_Currency','Quantity_MT','Price','Price_MT','Trade_Value','Delta','Delta_Quantity_MT','Premium','Premium_MT', ...
        'Option_Value','Market_Rate','Market_Value','Trader','Modified_Date','Date_of_Upload'};
   
    Final_Data = [Company_code,SAP_Business_Area,Profit_Centre,Legal_Entity_Code1,Legal_Entity_name,Product_Code,ProductName,Buy_Sell,Trade_Date,Trade_Ref, ...
                    Exchange,Broker_Code,Broker,Unit_Code,Unit_Name,Strategy,Terminal_Month,Put_Call,Strike,Contract_Currency,Quantity_MT,Price, ...
                    Price_MT,Trade_Value,Delta,Delta_Quantity_MT,Premium,Premium_MT,Option_Value,Market_Rate,Market_Value,Trader,Modified_Date,Modified_Date];
                
    Final_User_Data = cell2table(Final_Data,'VariableNames',User_Header);  %% Convert to table

    Future_Data = Final_User_Data(strcmpi('FUT',Final_User_Data.Strategy),:);
    Option_Data = Final_User_Data(~strcmpi('FUT',Final_User_Data.Strategy),:);

    %%% Future Data
    Fut_Data=[Future_Data.Company_Code,Future_Data.Business_Area,Future_Data.Profit_Centre,Future_Data.Legal_Entity_Code,Future_Data.Legal_Entity, ...
            Future_Data.Product_Code,Future_Data.Product,Future_Data.Buy_Sell,Future_Data.Trade_Date,Future_Data.Trade_Ref,Future_Data.Exchange, ...
            Future_Data.Broker_Code,Future_Data.Broker,Future_Data.Unit_Code,Future_Data.Unit_Name,Future_Data.Strategy,Future_Data.Terminal_Month, ...
            Future_Data.Contract_Currency,num2cell(Future_Data.Quantity_MT),num2cell(Future_Data.Price),num2cell(Future_Data.Price_MT),num2cell(Future_Data.Trade_Value),...
            Future_Data.Trader,Future_Data.Modified_Date,Future_Data.Date_of_Upload]; 
    
    %%% write data into file and upload into Database
    Future_Table_Name = ['fx_exposure_future_table_',char(InBUName)];
    Option_Table_Name = ['fx_exposure_option_table_',char(InBUName)];
    
    if isempty(Fut_Data)
        xlswrite(OutXLSName,{'No Future Trades'},'Future');  
    else
        xlswrite(OutXLSName,[Future_Header ; Fut_Data],'Future');
        
        %%%% Upload the Data
        Settle_date = repmat(Settlement_date,size(Fut_Data,1),1);
    try
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Future_Table_Name),' where settle_date = ''',char(Settlement_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

        upload_in_database(Future_Table_Name,[Settle_date Fut_Data],ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Future_Table_Name)]};
    end
    
    end
    if isempty(Option_Data)
        xlswrite(OutXLSName,{'No Option Trades'},'Options');  
    else
        %%% Final Required Option Data
        Option_Data.Price = [];
        Option_Data.Price_MT = [];
        Option_Data.Trade_Value = [];
    
        xlswrite(OutXLSName,[Option_Header ; table2cell(Option_Data)],'Options');
        
        %%%% Upload the data
        Settle_date = repmat(Settlement_date,size(Option_Data,1),1);
    try
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Option_Table_Name),' where settle_date = ''',char(Settlement_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');

        upload_in_database(Option_Table_Name,[Settle_date table2cell(Option_Data)],ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Option_Table_Name)]};
    end
    
    end

    %%%% Delete empty sheets and Active user sheet
    xls_delete_sheets(fullfile(pwd,OutXLSName));
    xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Future'); 

catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);

end
