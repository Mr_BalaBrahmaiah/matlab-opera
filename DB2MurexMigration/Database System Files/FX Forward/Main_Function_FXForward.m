clc
clear


ObjDB = connect_to_database;

SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
SettleDate_Number = m2xdate(datenum(char(SettleDate),'yyyy-mm-dd'));

%% Get Input File

[filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
InXLSFilename = fullfile(pathname,filename);

[~,~,RawData] = xlsread(InXLSFilename);

RawHeader = RawData(1,:);
RawData(1,:) = [];

RawData_CurrencyPair_Col = cellStrfind_exact(RawHeader,{'Currency Pair'});
RawData_Source_Col = cellStrfind_exact(RawHeader,{'Source'});
RawData_Maturity_Col = cellStrfind_exact(RawHeader,{'Maturity'});
RawData_MaturityDate_Col = cellStrfind_exact(RawHeader,{'Maturity Date'});
RawData_HarCodedPrice_Col = cellStrfind_exact(RawHeader,{'Harcoded Price'});

Combination_for_Spot  = strcat(RawData(:,RawData_CurrencyPair_Col),RawData(:,RawData_Maturity_Col),RawData(:,RawData_Source_Col));

MaturiyDate_Number = cellfun(@(s) (m2xdate(datenum(s,'dd-mm-yyyy'))), RawData(:,RawData_MaturityDate_Col),'UniformOutput',0);
RawHeader = [RawHeader , 'MaturiyDate_Number' , 'Combination_for_Spot'];
RawData = [RawData , MaturiyDate_Number , Combination_for_Spot];

RawData_MaturiyDate_Number_Col = cellStrfind_exact(RawHeader,{'MaturiyDate_Number'});
RawData_Combination_for_Spot_Col = cellStrfind_exact(RawHeader,{'Combination_for_Spot'});



%% Forward SPot Mapping Sheet
% [~,~,RawData_SpotMapping] = xlsread('Forward_Spot_Mapping.xlsx');
% RawData_SpotMapping_Header =  RawData_SpotMapping(1,:);
% RawData_SpotMapping(1,:) = [];

Sql_Query_1 = 'select forward,spot from currency_forward_mapping_table';
[RawData_SpotMapping_Header,RawData_SpotMapping] = Fetch_DB_Data(ObjDB,Sql_Query_1);

Spot_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'Spot'});

Spot_MisMatch = setdiff( RawData_SpotMapping(:,Spot_Col), unique(RawData(:,RawData_CurrencyPair_Col)) );

try
    if(~isempty(Spot_MisMatch))
        errordlg(['CurrencyPair column in Input File and Spot in Spot_Mapping File is Mismatched the new Spot is ',{''},char(Spot_MisMatch)],'MisMatched');
    else
        
        %% Get Underlying List Table
        [Underlying_ColNames,Underlying_Data] = Fetch_DB_Data(ObjDB,'select * from underlying_list_table where underlying_id like ''%USD%'' ');
        
        UnderlyingID_Col = cellStrfind_exact(Underlying_ColNames,{'underlying_id'});
        UnderlyingID_Data_Split = cellfun(@(s) (strsplit(s,' ')), Underlying_Data(:,UnderlyingID_Col),'UniformOutput',0);
        
        %% Get Expiry Date
        Expiry_Date = cell(size(UnderlyingID_Data_Split,1),7);
        CurrencyPair = cell(size(UnderlyingID_Data_Split,1),1);
        for i = 1 : size(UnderlyingID_Data_Split,1)
            Expiry_Date(i,1) = { UnderlyingID_Data_Split{i,1}{1,3} };
            Expiry_Date(i,2) = {m2xdate(datenum(UnderlyingID_Data_Split{i,1}{1,3},'yyyymmdd'))};
            Expiry_Date(i,3) = {datestr(datenum(UnderlyingID_Data_Split{i,1}{1,3},'yyyymmdd'),1)};
            Expiry_Date(i,4) = SettleDate;
            Expiry_Date(i,5) = ValueDate;
            Expiry_Date(i,6) = {0};
            Expiry_Date(i,7) = {0};
            CurrencyPair(i,1) = { UnderlyingID_Data_Split{i,1}{1,2} };
        end
        
        Underlying_ColNames = [Underlying_ColNames , {'Expiry_Date_Str','Expiry_Date_Number','Expiry_Date(dd-mmm-yyyy)','Settlement_Date','Value_Date','Risk_Free_Rate','Div_Yield'}];
        Underlying_Data = [Underlying_Data ,Expiry_Date] ;
        
        %%
        % Invenio_ProdCode_Col = cellStrfind_exact(Underlying_ColNames,{'invenio_product_code'});
        % Invenio_ProdCode_Data = Underlying_Data(:,Invenio_ProdCode_Col);
        % [aa,CurrencyPair] = strtok(Invenio_ProdCode_Data);
        % CurrencyPair = strrep(CurrencyPair,' ','');
        
        Underlying_ColNames = [Underlying_ColNames , {'Currency_Pair'}];
        Underlying_Data = [Underlying_Data ,CurrencyPair] ;
        
        Expiry_Date_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Str'});
        Spot_Underlying_Index = cellStrfind_exact(Underlying_Data(:,Expiry_Date_Col),{'Spot'});
        
        Spot_Underlying_ColNames = Underlying_ColNames;
        Spot_Underlying_Data = Underlying_Data(Spot_Underlying_Index,:);
        
        Underlying_Data(Spot_Underlying_Index,:) =  [];
        
        %% Get Above Settlement Data
        ExpiryDate_Num_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Number'});
        Index = cellfun(@(s) (s >= SettleDate_Number), Underlying_Data(:,ExpiryDate_Num_Col));
        
        Underlying_Data = Underlying_Data(Index,:); %% Get Above Settlement Data
        
        CurrencyPair_Col = cellStrfind_exact(Underlying_ColNames,{'Currency_Pair'});
        Unique_CurrencyPair = unique(Underlying_Data(:,CurrencyPair_Col));
        
        %%
        ExpiryDate_Number_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Number'});
        ExpiryDate_Maturiy_Col =  cellStrfind_exact(Underlying_ColNames,{'Expiry_Date(dd-mmm-yyyy)'});
        
        Underlying_ColNames = [Underlying_ColNames , {'Settle_Value'}];
        Overall_UnderlyingData = [];
        for i = 1 : size(Unique_CurrencyPair,1)
            
            Current_CurrencyPair = Unique_CurrencyPair(i);
            
            SpotData = RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,1),Current_CurrencyPair),Spot_Col);
            Current_CurrencyPair = unique([Current_CurrencyPair ; SpotData]);
            
            Temp_UnderlyingData = Underlying_Data(cellStrfind_exact(Underlying_Data(:,CurrencyPair_Col),Current_CurrencyPair),:);%% From Underlying Table
            
            Temp_RawData = RawData(cellStrfind_exact(RawData(:,RawData_CurrencyPair_Col),Current_CurrencyPair),:);   %% From Input Excel
            
            Settle_Value = cell(size(Temp_UnderlyingData,1),1);
            for ii = 1 : size(Temp_UnderlyingData,1)
                
                Current_ExpiryDate = cell2mat(Temp_UnderlyingData(ii,ExpiryDate_Num_Col));
                
                if( SettleDate_Number == cell2mat(Temp_UnderlyingData(ii,ExpiryDate_Num_Col)) )
                    Matched_Index = find(cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))==SettleDate_Number);
                    Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                    
                elseif(find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))))
                    Matched_Index = find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col)));
                    Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                else
                    
                    DayDiff = cellfun(@(s) daysact(Current_ExpiryDate,s), Temp_RawData(:,RawData_MaturiyDate_Number_Col)); %% Day Difference
                    
                    if(isempty(find(DayDiff<0))) %% Min  and Maximum Diff
                        Sort_DayDiff = num2cell(sort(DayDiff));
                        Sort_DayDiff = Sort_DayDiff(1:2) ;
                        Min_Index =  find(DayDiff == cell2mat(Sort_DayDiff(1)));
                        Max_Index = find(DayDiff == cell2mat(Sort_DayDiff(2)));
                    else
                        Min_Index = find(DayDiff == max(DayDiff(find(DayDiff<0))));
                        Max_Index = find(DayDiff == min(DayDiff(find(DayDiff>0))));
                    end
                    
                    Min_Value = Temp_RawData(Min_Index,RawData_HarCodedPrice_Col);
                    Max_Value = Temp_RawData(Max_Index,RawData_HarCodedPrice_Col);
                    
                    Min_Date = Temp_RawData(Min_Index,RawData_MaturiyDate_Number_Col);
                    Max_Date = Temp_RawData(Max_Index,RawData_MaturiyDate_Number_Col);
                    
                    Interpolation = (cell2mat(Max_Value) - cell2mat(Min_Value))*(Current_ExpiryDate - cell2mat(Min_Date))/(cell2mat(Max_Date) - cell2mat(Min_Date))+ cell2mat(Min_Value);
                    
                    if(~isempty(Interpolation))
                        %                     Settle_Value  = [Settle_Value ; Interpolation];
                        Settle_Value(ii) = num2cell(Interpolation);
                    end
                end
            end
            
            Temp_UnderlyingData = [Temp_UnderlyingData ,Settle_Value];
            Overall_UnderlyingData = [Overall_UnderlyingData ; Temp_UnderlyingData];
        end
        
        %% Working Spot Underlying Data
        Spot_UnderlyingID_Col = cellStrfind_exact(Spot_Underlying_ColNames,{'underlying_id'});
        [S,Split_SpotUnderlyingID] = strtok(Spot_Underlying_Data(:,Spot_UnderlyingID_Col));
        Split_SpotUnderlyingID = strrep(Split_SpotUnderlyingID,' ','');
        
        Spot_SettleValue = cell(size(Split_SpotUnderlyingID,1),1);
        for j = 1 : size(Split_SpotUnderlyingID,1)
            Current_Spot = Split_SpotUnderlyingID(j);
            
            Matched_Index = cellStrfind_exact(RawData(:,RawData_Combination_for_Spot_Col),Current_Spot);
            if(~isempty(Matched_Index))
                Spot_SettleValue(j) = RawData(Matched_Index,RawData_HarCodedPrice_Col);
            else
                Spot_SettleValue(j) = {1};
            end
        end
        
        Spot_Underlying_ColNames = [Spot_Underlying_ColNames , {'Settle_Value'}];
        Spot_Underlying_Data = [Spot_Underlying_Data , Spot_SettleValue];
        
        %% Excel Write
        
        Overall_UnderlyingData = [Overall_UnderlyingData ;Spot_Underlying_Data];
        
        OutputHeader_Order = {'Asset_Class','Underlying_id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
        Order_Index = cellStrfind_exact(Underlying_ColNames,OutputHeader_Order);
        Output_Data = Overall_UnderlyingData(:,Order_Index);
        
        OutXLSName = getXLSFilename('FX_Forward_Output');
        xlswrite(OutXLSName,[OutputHeader_Order ; Output_Data]);
        
    end
    
    msgbox('Process Finished','Finish');

    
catch ME
    OutErrorMsg = cellstr(ME.message);
end


