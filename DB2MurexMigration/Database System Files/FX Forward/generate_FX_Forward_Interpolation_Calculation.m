function [OutErrorMsg,OutXLSName] = generate_FX_Forward_Interpolation_Calculation(UserPassed_SettleDate)

OutErrorMsg = {'No Errors'};
OutXLSName = '';

%%
ObjDB = connect_to_database;

if(exist('UserPassed_SettleDate','var'))
    SettleDate = cellstr(UserPassed_SettleDate);
    ValueDate = fetch(ObjDB,['select value_date from vdate_sdate_table where settlement_date = ''',char(SettleDate),''' ' ]);
else
    SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table'); %% For Debugging Purpose
    ValueDate = fetch(ObjDB,'select value_date from valuation_date_table');
    
    OutErrorMsg = {'User Should be Pass Settle Date'};
    return;
end

Previous_SettleDate = fetch(ObjDB,['select settlement_date from vdate_sdate_table where value_date = ''',char(SettleDate),''' ' ]);
SettleDate_Number = m2xdate(datenum(char(SettleDate),'yyyy-mm-dd'));

DB_Date_Format = 'yyyy-mm-dd';
StartDate_Str = fetch(ObjDB,'select start_date from valuation_financial_year_view');
StartDate = m2xdate(datenum(char(StartDate_Str),DB_Date_Format));

Overall_ErrorMsg = [];

DayForm = 'long';
language = 'en_US';

[SettleDate_DayNumber,SettleDate_DayName] = weekday(x2mdate(SettleDate_Number),DayForm,language);

%% Get Input File

% [filename, pathname] = uigetfile({'*.xlsx';'*.xls'},'Pick a file');
% InXLSFilename = fullfile(pathname,filename);
% [~,~,RawData] = xlsread(InXLSFilename);
% RawHeader = RawData(1,:);
% RawData(1,:) = [];

SqlQuery = ['select * from currency_forward_price_table where settlement_date = ''',char(SettleDate),''' '];
[RawHeader,RawData] = Fetch_DB_Data(ObjDB,SqlQuery);

if(strcmpi(RawData,'No Data'))
    OutErrorMsg = {['No Data Found in currency_forward_price_table for ',char(SettleDate)]};
    return;
end

%%
RawData_CurrencyPair_Col = cellStrfind_exact(RawHeader,{'currency_pair'});
RawData_Source_Col = cellStrfind_exact(RawHeader,{'source'});
RawData_Maturity_Col = cellStrfind_exact(RawHeader,{'forward_code'});
RawData_MaturityDate_Col = cellStrfind_exact(RawHeader,{'forward_date'});
RawData_HarCodedPrice_Col = cellStrfind_exact(RawHeader,{'price'});

Combination_for_Spot  = strcat(RawData(:,RawData_CurrencyPair_Col),RawData(:,RawData_Maturity_Col),RawData(:,RawData_Source_Col));

MaturiyDate_Number = cellfun(@(s) (m2xdate(datenum(s,DB_Date_Format))), RawData(:,RawData_MaturityDate_Col),'UniformOutput',0);
RawHeader = [RawHeader , 'MaturiyDate_Number' , 'Combination_for_Spot'];
RawData = [RawData , MaturiyDate_Number , Combination_for_Spot];

RawData_SettlementDate_Col = cellStrfind_exact(RawHeader,{'settlement_date'});
RawData_MaturiyDate_Number_Col = cellStrfind_exact(RawHeader,{'MaturiyDate_Number'});
RawData_Combination_for_Spot_Col = cellStrfind_exact(RawHeader,{'Combination_for_Spot'});


%% Forward SPot Mapping Sheet
% [~,~,RawData_SpotMapping] = xlsread('Forward_Spot_Mapping.xlsx');
% RawData_SpotMapping_Header =  RawData_SpotMapping(1,:);
% RawData_SpotMapping(1,:) = [];

Sql_Query_1 = 'select * from currency_forward_mapping_table';
[RawData_SpotMapping_Header,RawData_SpotMapping] = Fetch_DB_Data(ObjDB,Sql_Query_1);

Spot_ProductCode_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'product_code'});
Spot_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'spot'});
Forward_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'forward'});
Df_Ndf_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'df_or_ndf'});
Offset_Col = cellStrfind_exact(RawData_SpotMapping_Header,{'offset'});

Spot_MisMatch = setdiff( RawData_SpotMapping(:,Spot_Col), unique(RawData(:,RawData_CurrencyPair_Col)) );

try
    if(~isempty(Spot_MisMatch))
        errordlg(['CurrencyPair column in Input File and Spot in Spot_Mapping File is Mismatched the new Spot is ',{''},char(Spot_MisMatch)],'MisMatched');
    else
        
        %% Get Underlying List Table
        [Underlying_ColNames,Underlying_Data] = Fetch_DB_Data(ObjDB,'select * from underlying_list_table where underlying_id like ''%USD%'' ');
        
        UnderlyingID_Col = cellStrfind_exact(Underlying_ColNames,{'underlying_id'});
        UnderlyingID_Data_Split = cellfun(@(s) (strsplit(s,' ')), Underlying_Data(:,UnderlyingID_Col),'UniformOutput',0);
        
        %% Get Expiry Date
        Expiry_Date = cell(size(UnderlyingID_Data_Split,1),7);
        CurrencyPair = cell(size(UnderlyingID_Data_Split,1),1);
        for i = 1 : size(UnderlyingID_Data_Split,1)
            Expiry_Date(i,1) = { UnderlyingID_Data_Split{i,1}{1,3} };
            Expiry_Date(i,2) = {m2xdate(datenum(UnderlyingID_Data_Split{i,1}{1,3},'yyyymmdd'))};
            Expiry_Date(i,3) = {datestr(datenum(UnderlyingID_Data_Split{i,1}{1,3},'yyyymmdd'),1)};
            Expiry_Date(i,4) = SettleDate;
            Expiry_Date(i,5) = ValueDate;
            Expiry_Date(i,6) = {0};
            Expiry_Date(i,7) = {0};
            CurrencyPair(i,1) = { UnderlyingID_Data_Split{i,1}{1,2} };
        end
        
        Underlying_ColNames = [Underlying_ColNames , {'Expiry_Date_Str','Expiry_Date_Number','Expiry_Date(dd-mmm-yyyy)','Settlement_Date','Value_Date','Risk_Free_Rate','Div_Yield','Currency_Pair'}];
        Underlying_Data = [Underlying_Data ,Expiry_Date , CurrencyPair] ;
        
        %% Find SettleDate_ExpiryDate_DayDiff
        
        UnderlyingID_Col = cellStrfind_exact(Underlying_ColNames,{'underlying_id'});
        ExpiryDate_Num_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Number'});
        ExpiryDate_Maturiy_Col =  cellStrfind_exact(Underlying_ColNames,{'Expiry_Date(dd-mmm-yyyy)'});
        
        %         SettleDate_ExpiryDate_DayDiff = num2cell( cellfun(@(s) daysact(SettleDate_Number,s), Underlying_Data(:,ExpiryDate_Num_Col)) ); % Include Sat & Sun
        
        if(SettleDate_DayNumber >=3)
            SettleDate_ExpiryDate_DayDiff = num2cell( cellfun(@(s) wrkdydif(SettleDate_Number, s, 1), Underlying_Data(:,ExpiryDate_Num_Col)) );
        else
            SettleDate_ExpiryDate_DayDiff = num2cell( cellfun(@(s) wrkdydif(SettleDate_Number, s, 1)+1, Underlying_Data(:,ExpiryDate_Num_Col)) );
        end
        
        Underlying_ColNames  = [Underlying_ColNames , {'SettleDate_ExpiryDate_DayDiff'}]; %% Add New Header
        Underlying_Data = [Underlying_Data ,SettleDate_ExpiryDate_DayDiff];
        
        SettleDate_ExpiryDate_DayDiff_Col = cellStrfind_exact(Underlying_ColNames,{'SettleDate_ExpiryDate_DayDiff'});
        
        %%
        
        Expiry_Date_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Str'});
        Spot_Underlying_Index = cellStrfind_exact(Underlying_Data(:,Expiry_Date_Col),{'Spot'});
        
        Spot_Underlying_ColNames = Underlying_ColNames;
        Spot_Underlying_Data = Underlying_Data(Spot_Underlying_Index,:);
        
        Underlying_Data(Spot_Underlying_Index,:) =  [];
        
        
        %% Remove  Less Than Start Date
        
        LessThan_StartDate_Index = find(cell2mat(Underlying_Data(:,ExpiryDate_Num_Col)) < StartDate);
        LessThan_StartDate_Data = Underlying_Data(LessThan_StartDate_Index,:); %% For User Reference
        
        Underlying_Data(LessThan_StartDate_Index,:) =  []; %% Remove  Less Than Start Date
        
        %% Get Above Settlement Data (Live)
        ExpiryDate_Num_Col = cellStrfind_exact(Underlying_ColNames,{'Expiry_Date_Number'});
        Index = cellfun(@(s) (s >= SettleDate_Number), Underlying_Data(:,ExpiryDate_Num_Col)); %% (s >= SettleDate_Number)
        
        Underlying_Data_Live = Underlying_Data(Index,:); %% Get Live Data
        Underlying_Data_Dead = Underlying_Data((~Index),:); %% %% Get Dead Data
        
        CurrencyPair_Col = cellStrfind_exact(Underlying_ColNames,{'Currency_Pair'});
        Unique_CurrencyPair_Live = unique(Underlying_Data_Live(:,CurrencyPair_Col));
        Unique_CurrencyPair_Dead = unique(Underlying_Data_Dead(:,CurrencyPair_Col));
        
        %% Fetch Underlying Settle Value Table
        
        StartDate_for_SettleValue_Tbl = datestr(x2mdate(busdate(m2xdate(datenum(StartDate_Str,DB_Date_Format))-5)),DB_Date_Format);
        
        SqlQuery_2 = ['select * from underlying_settle_value_table where asset_class=''currency'' and settlement_date>=''',...
            char(StartDate_for_SettleValue_Tbl),''' and underlying_id like ''%spot%'' ' ];
        [SettleValue_Fields , SettleValue_DBData] = Fetch_DB_Data(ObjDB,SqlQuery_2);
        
        if(isempty(SettleValue_DBData))
            OutErrorMsg = {'Not Getting underlying_settle_value_table Data from DB'};
            return;
        end
        if(strcmpi(SettleValue_DBData,'No Data'))
            OutErrorMsg = {'Not Getting underlying_settle_value_table Data from DB'};
            return;
        end
        
        SqlQuery_3 = ['select * from underlying_settle_value_table where asset_class=''currency'' and settlement_date>=''',...
            char(StartDate_for_SettleValue_Tbl),''' '];
        [Overall_SettleValue_Cols,Overall_SettleValue_DBData] = Fetch_DB_Data(ObjDB,SqlQuery_3); %% For Dead Entries , Fetch SettleValue Using Expiry Date
        
        Underlying_SettleID_Col = cellStrfind_exact(SettleValue_Fields,{'underlying_id'});
        Underlying_SettleValue_Col = cellStrfind_exact(SettleValue_Fields,{'settle_value'});
        Underlying_SettleDate_Col = cellStrfind_exact(SettleValue_Fields,{'settlement_date'});
        
        %% Fetch Product_busdays_Table
        
        SqlQuery_3 = 'select * from product_busdays_table';
        [Product_BusDays_Fields , Product_BusDays_DBData] = Fetch_DB_Data(ObjDB,SqlQuery_3);
        
        if(isempty(Product_BusDays_DBData))
            OutErrorMsg = {'Not Getting product_busdays_table Data from DB'};
            return;
        end
        if(strcmpi(Product_BusDays_DBData,'No Data'))
            OutErrorMsg = {'Not Getting product_busdays_table Data from DB'};
            return;
        end
        
        Product_BusDays_DBData = cell2dataset([Product_BusDays_Fields ; Product_BusDays_DBData]);
        
        %%  Working For Live
        
        [DayNumber,DayName] = weekday(datenum(Underlying_Data_Live(:,Expiry_Date_Col),'yyyymmdd'),DayForm,language);
        DayName = cellstr(DayName) ;
        DayNumber = num2cell(DayNumber);
        Sunday_Saturday_Index = cellStrfind_exact(DayName,{'Sunday','Saturday'}) ;
        Sunday_Saturday_DataLive = Underlying_Data_Live(Sunday_Saturday_Index,:) ;
        Underlying_Data_Live(Sunday_Saturday_Index,:) = []; %% Remove Saturday Sunday Entries
        
        Underlying_ColNames = [Underlying_ColNames , {'Settle_Value'}];
        Overall_UnderlyingData_Live = [];
        for i = 1 : size(Unique_CurrencyPair_Live,1)
            
            Current_CurrencyPair = Unique_CurrencyPair_Live(i);
            
            SpotData = RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Spot_Col);
            if(isempty(SpotData))
                Error_Msg_1 = cellstr([char(Current_CurrencyPair),' not available in currency_forward_mapping_table']);
                Overall_ErrorMsg = [Overall_ErrorMsg ; Error_Msg_1];
                continue;
            end
            Current_CurrencyPair = unique([Current_CurrencyPair ; SpotData]);
            
            Offset_Data = cell2mat(RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Offset_Col));
            
            Temp_UnderlyingData_Live = Underlying_Data_Live(cellStrfind_exact(Underlying_Data_Live(:,CurrencyPair_Col),Current_CurrencyPair),:);%% From Underlying Table
            
            Temp_RawData = RawData(cellStrfind_exact(RawData(:,RawData_CurrencyPair_Col),Current_CurrencyPair),:);   %% From Input Excel
            
            Settle_Value = cell(size(Temp_UnderlyingData_Live,1),1);
            for ii = 1 : size(Temp_UnderlyingData_Live,1)
                
                Current_UnderlyinID =Temp_UnderlyingData_Live(ii,UnderlyingID_Col);
                Current_ExpiryDate = cell2mat(Temp_UnderlyingData_Live(ii,ExpiryDate_Num_Col));
                Current_ExpiryDate_Str = datestr(datenum(Temp_UnderlyingData_Live(ii,Expiry_Date_Col),'yyyymmdd'),DB_Date_Format);
                
                if(Offset_Data > 0 || ii == 1 )
                    Offset_StartLimit = Current_ExpiryDate - Offset_Data;
                    Offset_Date_Range = Offset_StartLimit : 1 : Current_ExpiryDate-1 ;
                end
                
                %% OFFSET Greater than Zero
                
                if(Offset_Data > 0)
                    if(cell2mat(Temp_UnderlyingData_Live(ii,SettleDate_ExpiryDate_DayDiff_Col)) > -1 && cell2mat(Temp_UnderlyingData_Live(ii,SettleDate_ExpiryDate_DayDiff_Col)) <=Offset_Data)
                        Matched_Index = find(strcmpi(Temp_RawData(:,RawData_Maturity_Col),'Spot')); %% For Offset Freeze
                        if(Matched_Index > 0 )
                            if(cell2mat(Temp_UnderlyingData_Live(ii,SettleDate_ExpiryDate_DayDiff_Col)) == Offset_Data)
                                Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col); %% Already Freeze
                            else
                                Date_Matched_Index = strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleID_Col),Current_UnderlyinID) & ...
                                    strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleDate_Col),Previous_SettleDate);
                                if(~isempty(find(Date_Matched_Index)))
                                    Temp_Matched_Data = Overall_SettleValue_DBData(Date_Matched_Index,:); %% For Reference
                                    Settle_Value(ii,1) = Overall_SettleValue_DBData(Date_Matched_Index,Underlying_SettleValue_Col) ;
                                else
                                    Settle_Value(ii,1) = {' '} ; %% Blank
                                end
                            end
                            
                        end
                        
                    elseif(cell2mat(Temp_UnderlyingData_Live(ii,SettleDate_ExpiryDate_DayDiff_Col)) == 0)
                        Matched_Index = find(strcmpi(Temp_RawData(:,RawData_Maturity_Col),'Spot')); %% For Offset Freeze
                        if(Matched_Index > 0 )
                            Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col); %% Already Freeze
                            
                            Date_Matched_Index = strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleID_Col),Current_UnderlyinID) & ...
                                strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleDate_Col),Previous_SettleDate);
                            if(~isempty(find(Date_Matched_Index)))
                                Temp_Matched_Data = Overall_SettleValue_DBData(Date_Matched_Index,:); %% For Reference
                                Settle_Value(ii,1) = Overall_SettleValue_DBData(Date_Matched_Index,Underlying_SettleValue_Col) ;
                            end
                            
                        end
                    elseif( SettleDate_Number == cell2mat(Temp_UnderlyingData_Live(ii,ExpiryDate_Num_Col)) )
                        Matched_Index = find(cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))==SettleDate_Number);
                        Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                        
                    elseif(find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))))
                        Matched_Index = find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col)));
                        Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                        
                    elseif(find(Offset_Date_Range == SettleDate_Number))
                        Matched_Index = find(cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))==SettleDate_Number);
                        Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                        
                    else
                        DayDiff = cellfun(@(s) daysact(Current_ExpiryDate,s), Temp_RawData(:,RawData_MaturiyDate_Number_Col)); %% Day Difference
                        
                        if(isempty(find(DayDiff<0))) %% Min  and Maximum Diff
                            Sort_DayDiff = num2cell(sort(DayDiff));
                            %                         Sort_DayDiff = Sort_DayDiff(1:2) ;
                            Min_Index =  find(DayDiff == cell2mat(Sort_DayDiff(1)));
                            Max_Index = find(DayDiff == cell2mat(Sort_DayDiff(end))); %% Sort_DayDiff(2)
                        else
                            try
                                Min_Index = find(DayDiff == max(DayDiff(find(DayDiff<0))));
                                Max_Index = find(DayDiff == min(DayDiff(find(DayDiff>0))));
                            catch
                                Sort_DayDiff = num2cell(sort(DayDiff));
                                Min_Index =  find(DayDiff == cell2mat(Sort_DayDiff(1)));
                                Max_Index = find(DayDiff == cell2mat(Sort_DayDiff(end)));
                            end
                        end
                        
                        Min_Value = Temp_RawData(Min_Index,RawData_HarCodedPrice_Col);
                        Max_Value = Temp_RawData(Max_Index,RawData_HarCodedPrice_Col);
                        
                        Min_Date = Temp_RawData(Min_Index,RawData_MaturiyDate_Number_Col);
                        Max_Date = Temp_RawData(Max_Index,RawData_MaturiyDate_Number_Col);
                        
                        Interpolation = (cell2mat(Max_Value) - cell2mat(Min_Value))*(Current_ExpiryDate - cell2mat(Min_Date))/(cell2mat(Max_Date) - cell2mat(Min_Date))+ cell2mat(Min_Value);
                        
                        if(~isempty(Interpolation))
                            %                     Settle_Value  = [Settle_Value ; Interpolation];
                            Settle_Value(ii) = num2cell(Interpolation);
                        end
                    end
                    
                    continue;
                end
                
                %% NO OFFSET
                
                if( SettleDate_Number == cell2mat(Temp_UnderlyingData_Live(ii,ExpiryDate_Num_Col)) )
                    Matched_Index = find(cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))==SettleDate_Number);
                    Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                    
                elseif(find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))))
                    Matched_Index = find(Current_ExpiryDate ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col)));
                    Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                    
                elseif(find(Offset_Date_Range == SettleDate_Number))
                    Matched_Index = find(cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col))==SettleDate_Number);
                    Settle_Value(ii) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                else
                    
                    DayDiff = cellfun(@(s) daysact(Current_ExpiryDate,s), Temp_RawData(:,RawData_MaturiyDate_Number_Col)); %% Day Difference
                    
                    if(isempty(find(DayDiff<0))) %% Min  and Maximum Diff
                        Sort_DayDiff = num2cell(sort(DayDiff));
                        %                         Sort_DayDiff = Sort_DayDiff(1:2) ;
                        Min_Index =  find(DayDiff == cell2mat(Sort_DayDiff(1)));
                        Max_Index = find(DayDiff == cell2mat(Sort_DayDiff(end))); %% Sort_DayDiff(2)
                    else
                        try
                            Min_Index = find(DayDiff == max(DayDiff(find(DayDiff<0))));
                            Max_Index = find(DayDiff == min(DayDiff(find(DayDiff>0))));
                        catch
                            Sort_DayDiff = num2cell(sort(DayDiff));
                            Min_Index =  find(DayDiff == cell2mat(Sort_DayDiff(1)));
                            Max_Index = find(DayDiff == cell2mat(Sort_DayDiff(end)));
                        end
                    end
                    
                    Min_Value = Temp_RawData(Min_Index,RawData_HarCodedPrice_Col);
                    Max_Value = Temp_RawData(Max_Index,RawData_HarCodedPrice_Col);
                    
                    Min_Date = Temp_RawData(Min_Index,RawData_MaturiyDate_Number_Col);
                    Max_Date = Temp_RawData(Max_Index,RawData_MaturiyDate_Number_Col);
                    
                    Interpolation = (cell2mat(Max_Value) - cell2mat(Min_Value))*(Current_ExpiryDate - cell2mat(Min_Date))/(cell2mat(Max_Date) - cell2mat(Min_Date))+ cell2mat(Min_Value);
                    
                    if(~isempty(Interpolation))
                        %                     Settle_Value  = [Settle_Value ; Interpolation];
                        Settle_Value(ii) = num2cell(Interpolation);
                    end
                end
            end
            
            Temp_UnderlyingData_Live = [Temp_UnderlyingData_Live ,Settle_Value];
            Overall_UnderlyingData_Live = [Overall_UnderlyingData_Live ; Temp_UnderlyingData_Live];
        end
        
        %% Working for Dead Entries
        
        [DayNumber,DayName] = weekday(datenum(Underlying_Data_Dead(:,Expiry_Date_Col),'yyyymmdd'),DayForm,language);
        DayName = cellstr(DayName) ;
        DayNumber = num2cell(DayNumber);
        Sunday_Saturday_Index = cellStrfind_exact(DayName,{'Sunday','Saturday'}) ;
        Sunday_Saturday_DataDead = Underlying_Data_Dead(Sunday_Saturday_Index,:) ;
        Underlying_Data_Dead(Sunday_Saturday_Index,:) = []; %% Remove Saturday Sunday Entries
        
        Overall_UnderlyingData_Dead = [];
        for i = 1 : size(Unique_CurrencyPair_Dead,1)
            
            Current_CurrencyPair = Unique_CurrencyPair_Dead(i);
            
            SpotData = RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Spot_Col);
            Current_CurrencyPair = unique([Current_CurrencyPair ; SpotData]);
            Temp_UnderlyingData_Dead = Underlying_Data_Dead(cellStrfind_exact(Underlying_Data_Dead(:,CurrencyPair_Col),Current_CurrencyPair),:);
            Temp_RawData = RawData(cellStrfind_exact(RawData(:,RawData_CurrencyPair_Col),Current_CurrencyPair),:);
            
            if(strcmpi(RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Df_Ndf_Col),'DF'))
                Settle_Value = cell(size(Temp_UnderlyingData_Dead,1),1);
                
                Matched_Index = find(SettleDate_Number ==  cell2mat(Temp_RawData(:,RawData_MaturiyDate_Number_Col)));
                Settle_Value(:,1) = Temp_RawData(Matched_Index,RawData_HarCodedPrice_Col);
                
                Temp_UnderlyingData_Dead = [Temp_UnderlyingData_Dead , Settle_Value] ;
                Overall_UnderlyingData_Dead = [Overall_UnderlyingData_Dead ; Temp_UnderlyingData_Dead];
                
            elseif((strcmpi(RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Df_Ndf_Col),'NDF')))
                
                Current_ProductCode = RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Spot_ProductCode_Col);
                Offset_Data = cell2mat(RawData_SpotMapping(cellStrfind_exact(RawData_SpotMapping(:,Forward_Col),Current_CurrencyPair),Offset_Col));
                
                Settle_Value = cell(size(Temp_UnderlyingData_Dead,1),1);
                for ii = 1 : size(Temp_UnderlyingData_Dead,1)
                    
                    Current_UnderlyinID =Temp_UnderlyingData_Dead(ii,UnderlyingID_Col);
                    Current_ExpiryDate_Str = datestr(datenum(Temp_UnderlyingData_Dead(ii,Expiry_Date_Col),'yyyymmdd'),DB_Date_Format) ;
                    
                    Date_Matched_Index = strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleID_Col),Current_UnderlyinID) & ...
                        strcmpi(Overall_SettleValue_DBData(:,Underlying_SettleDate_Col),{Current_ExpiryDate_Str});
                    if(~isempty(find(Date_Matched_Index)))
                        Temp_Matched_Data = Overall_SettleValue_DBData(Date_Matched_Index,:); %% For Reference
                        Settle_Value(ii,1) = Overall_SettleValue_DBData(Date_Matched_Index,Underlying_SettleValue_Col) ;
                    end
                    
                    
                    %% While Loop for Offset
                    %                     if(Offset_Data>0)
                    %                         Current_ExpiryDate_Str = datestr(datenum(Temp_UnderlyingData_Dead(ii,Expiry_Date_Col),'yyyymmdd'),DB_Date_Format) ;
                    %
                    %                         aa = 1;
                    %                         Offset_Temp = Offset_Data;  %% For Offset Logic
                    %                         while(aa)
                    %                             fprintf('Entering Dead Entries While Loop.........\n');
                    %
                    %                             try
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.value_date,Current_ExpiryDate_Str));
                    %                                 Get_Expiry_SettleDate = Product_BusDays_DBData.settle_date(GetDate_Index);
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.value_date,Get_Expiry_SettleDate));
                    %                             catch
                    %                                 % My Work
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.settle_date,Current_ExpiryDate_Str));
                    %                                 Get_Expiry_SettleDate = Product_BusDays_DBData.settle_date(GetDate_Index);
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.settle_date,Get_Expiry_SettleDate));
                    %                             end
                    %
                    %                             if(isempty(GetDate_Index)) %% My Work
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.settle_date,Current_ExpiryDate_Str));
                    %                                 Get_Expiry_SettleDate = Product_BusDays_DBData.settle_date(GetDate_Index);
                    %                                 GetDate_Index = find(strcmpi(Product_BusDays_DBData.product_code,Current_ProductCode) & strcmpi(Product_BusDays_DBData.settle_date,Get_Expiry_SettleDate));
                    %                             end
                    %
                    %
                    %                             Current_ExpiryDate_Str = Product_BusDays_DBData.settle_date(GetDate_Index);
                    %
                    %                             Offset_Temp = Offset_Temp-1;
                    %                             if(Offset_Temp <= 2)
                    %                                 aa = 0;
                    %                             end
                    %                         end
                    %
                    %                     else
                    %                         Current_ExpiryDate_Str =  datestr(datenum(Temp_UnderlyingData_Dead(ii,Expiry_Date_Col),'yyyymmdd'),DB_Date_Format) ;
                    %                     end
                    %
                    %                     Date_Matched_Index = cellStrfind_exact(SettleValue_DBData(:,Underlying_SettleDate_Col),{Current_ExpiryDate_Str});
                    %                     if(~isempty(Date_Matched_Index))
                    %                         Temp_Matched_Data = SettleValue_DBData(Date_Matched_Index,:);
                    %                         Matched_Index = cellStrfind(Temp_Matched_Data(:,Underlying_SettleID_Col),Current_CurrencyPair);
                    %                         Settle_Value(ii,1) = Temp_Matched_Data(Matched_Index,Underlying_SettleValue_Col) ;
                    %                     end
                    
                end
                
                Temp_UnderlyingData_Dead = [Temp_UnderlyingData_Dead , Settle_Value] ;
                Overall_UnderlyingData_Dead = [Overall_UnderlyingData_Dead ; Temp_UnderlyingData_Dead];
            else
                
            end
        end
        
        %% Working Spot Underlying Data
        Spot_UnderlyingID_Col = cellStrfind_exact(Spot_Underlying_ColNames,{'underlying_id'});
        [S,Split_SpotUnderlyingID] = strtok(Spot_Underlying_Data(:,Spot_UnderlyingID_Col));
        Split_SpotUnderlyingID = strrep(Split_SpotUnderlyingID,' ','');
        
        Spot_SettleValue = cell(size(Split_SpotUnderlyingID,1),1);
        for j = 1 : size(Split_SpotUnderlyingID,1)
            Current_Spot = Split_SpotUnderlyingID(j);
            
            Matched_Index = strcmpi(RawData(:,RawData_Combination_for_Spot_Col),Current_Spot) & strcmpi(RawData(:,RawData_SettlementDate_Col),SettleDate);
            if(~isempty(find(Matched_Index, 1)))
                Spot_SettleValue(j) = RawData(Matched_Index,RawData_HarCodedPrice_Col);
            else
                Spot_SettleValue(j) = {1};
            end
        end
        
        Spot_Underlying_ColNames = [Spot_Underlying_ColNames , {'Settle_Value'}];
        Spot_Underlying_Data = [Spot_Underlying_Data , Spot_SettleValue];
        
        
        %%  OnShore Holiday Check
        Onshore_Holiday_TableName  = 'currency_forward_holiday_table';
        SqlQuery_HolidayData = ['select * from ',Onshore_Holiday_TableName,' where settle_date=''',char(SettleDate),''' '];
        [aa,bb] = Fetch_DB_Data(ObjDB,SqlQuery_HolidayData);
        
        %         if(isempty(bb))
        %             OutErrorMsg = {'Not Getting currency_forward_holiday_table Data from DB'};
        %             return;
        %         end
        %         if(strcmpi(bb,'No Data'))
        %             OutErrorMsg = {'Not Getting currency_forward_holiday_table Data from DB'};
        %             return;
        %         end
        
        if(~strcmpi(bb,'No Data'))
            Onshore_Holiday_TableData = cell2dataset([aa;bb]);
            
            UnderlyingID_Col = cellStrfind_exact(Underlying_ColNames,{'Underlying_id'});
            SettleValue_Col = cellStrfind_exact(Underlying_ColNames,{'Settle_Value'});
            CurrencyPair_Col = cellStrfind_exact(Underlying_ColNames,{'Currency_Pair'});
            
            for k = 1 : size(Onshore_Holiday_TableData,1)
                Current_ProductCode = strcat({'CR '},Onshore_Holiday_TableData.product_code(k));
                Current_Holiday_Str = Onshore_Holiday_TableData.is_holiday(k);
                
                if(strcmpi(Current_Holiday_Str,'TRUE'))
                    
                    SqlQuery_PreviousData = ['select * from underlying_settle_value_table where asset_class=''currency'' and settlement_date=''',char(Previous_SettleDate),...
                        ''' and underlying_id like ''',[char(Current_ProductCode),'%'],''' and underlying_id not like ''%SPOT%'''];
                    [aa,bb] = Fetch_DB_Data(ObjDB,SqlQuery_PreviousData);
                    if(strcmpi(bb,'No Data'))
                        continue;
                    end
                    PreviousData_SettleValue = cell2dataset([aa ; bb]);
                    
                    for m = 1 : size(PreviousData_SettleValue,1)
                        Current_Underlying_ID =  PreviousData_SettleValue.underlying_id(m);
                        Current_Previous_SettleValue = PreviousData_SettleValue.settle_value(m);
                        
                        Matched_Index = strcmpi(Overall_UnderlyingData_Live(:,UnderlyingID_Col),Current_Underlying_ID);
                        if(~isempty(find(Matched_Index, 1)))
                            Overall_UnderlyingData_Live(Matched_Index,SettleValue_Col) = num2cell(Current_Previous_SettleValue); %% OverWrite the Price
                        end
                        
                    end
                    
                end
                
            end
            
        end
        %% Make Order
        
        Overall_UnderlyingData_Live_Dead = [Overall_UnderlyingData_Live ; Overall_UnderlyingData_Dead ; Spot_Underlying_Data];
        
        OutputHeader_Order = {'Asset_Class','Underlying_id','Value_Date','Settle_Value','Risk_Free_Rate','Div_Yield','Settlement_Date'};
        Order_Index = cellStrfind_exact(Underlying_ColNames,OutputHeader_Order);
        Output_Data = Overall_UnderlyingData_Live_Dead(:,Order_Index);
        
        %% Overwrite Prices
        OverWrite_PriceTbl = 'underlying_currency_forward_table';
        [Overwrite_TblCols,Overwrite_TblData] = Fetch_DB_Data(ObjDB,[],OverWrite_PriceTbl);
        
        if(isempty(Overwrite_TblData))
            Error_Msg_1 = cellstr(['Not Getting Data from ',OverWrite_PriceTbl,' for Overwrite the price']);
            Overall_ErrorMsg = [Overall_ErrorMsg ; Error_Msg_1];
        end
        if(strcmpi(Overwrite_TblData,'No Data'))
            Error_Msg_1 = cellstr(['Not Getting Data from ',OverWrite_PriceTbl,' for Overwrite the price']);
            Overall_ErrorMsg = [Overall_ErrorMsg ; Error_Msg_1];
        end
        
        Overwrite_TblData = cell2dataset([Overwrite_TblCols;Overwrite_TblData]);
        
        UnderlyingID_Col = cellStrfind_exact(OutputHeader_Order,{'Underlying_id'});
        SettleValue_Col = cellStrfind_exact(OutputHeader_Order,{'Settle_Value'});
        
        for i = 1 : size(Overwrite_TblData,1)
            Current_UnderlyingID = Overwrite_TblData.underlying_id(i);
            Current_SettleValue = Overwrite_TblData.settle_value(i);
            
            Matched_Index = strcmpi(Output_Data(:,UnderlyingID_Col),Current_UnderlyingID);
            if(~isempty(find(Matched_Index, 1)))
                Output_Data(Matched_Index,SettleValue_Col) = num2cell(Current_SettleValue); %% OverWrite the Price
            end
            
        end
        
        
        
        %% Excel Write
        
        OutXLSName = getXLSFilename('FX_Forward_Output');
        xlswrite(OutXLSName,[OutputHeader_Order ; Output_Data],'Output');
        
        xlswrite(OutXLSName,[Underlying_ColNames ; Overall_UnderlyingData_Live],'Live');
        xlswrite(OutXLSName,[Underlying_ColNames ; Overall_UnderlyingData_Dead],'Dead');
        xlswrite(OutXLSName,[Spot_Underlying_ColNames ; Spot_Underlying_Data],'Spot');
        if(exist('PreviousData_SettleValue','var'))
            xlswrite(OutXLSName,dataset2cell(PreviousData_SettleValue),'PreviousDate_SettleValue');
        end
        
        if(~isempty(Overall_ErrorMsg))
            xlswrite(OutXLSName,Overall_ErrorMsg,'Error_Msg');
        end
        
        if(~isempty(Sunday_Saturday_DataLive))
            xlswrite(OutXLSName,Sunday_Saturday_DataLive,'Sunday_Saturday_DataLive');
        end
        if(~isempty(Sunday_Saturday_DataDead))
            xlswrite(OutXLSName,Sunday_Saturday_DataDead,'Sunday_Saturday_DataDead');
        end
        
        try
            XLFileName = fullfile(pwd, OutXLSName);
            xls_delete_sheets(XLFileName);
            xls_change_activesheet(XLFileName,'Output');
        catch
        end
        
    end
    
    %     msgbox('Process Finished','Finish');
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end


