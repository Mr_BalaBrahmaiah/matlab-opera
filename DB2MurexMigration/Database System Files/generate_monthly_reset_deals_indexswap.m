function [OutErrorMsg,OutFileName]=generate_monthly_reset_deals_indexswap(InBUName)
try
    OutErrorMsg = {'No errors'};
    OutFilename = '';
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFileName = getXLSFilename(['Monthly_Reset_Deals_',InBUName]);
    
    %% Database Connection Object
    ObjDB=connect_to_database;
    
    %% Read data based on the valuation date
    [DateFields,DateData] = Fetch_DB_Data(ObjDB,'select * from valuation_date_table');
    DBDateData = cell2dataset([DateFields;DateData]);
    
    SqlQuery = ['select * from monthly_reset_date_table_',(char(InBUName)) ,' where reset_date= ''', char(DBDateData.value_date),''' '];
    [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
    
    if(isempty(Data))
        OutErrorMsg = {'Reset date is not available for corresponding value date'};
        return;
    end
    
    if(strcmpi(Data,'No Data'))
        OutErrorMsg = {'Reset date is not available for corresponding value date'};
        return;
    end
    
    ResetTableData= cell2dataset([ColNames;Data]);
    
    UniqueCP = unique(ResetTableData.counterparty_parent,'stable');
    
       
    Unique_CounterParty_Query = ['''',strrep(convertCell2Char(UniqueCP),',',''','''),''''];
    
    AllActualDealsData = [];
    AllOffsetDealsData = [];
    AllResetDealsData = [];
    
    for iCP = 1:length(UniqueCP)
        %% Function Condition
%         SqlQuery=['select * from deal_ticket_table_with_latest_versions_active_lots_view_',(char(InBUName)),...
%             ' where counterparty_parent = ''',UniqueCP{iCP},''' and trade_group_type=''IDX_SWAP''  and deal_status= ''','live',''' and transaction_date < ''',...
%             char(ResetTableData.reset_date),''''];
        
         % commented
%         SqlQuery=['select * from deal_ticket_table_with_latest_versions_active_lots_view_',(char(InBUName)),...
%             ' where counterparty_parent = ''',UniqueCP{iCP},''' and transaction_date < ''',...
%             char(ResetTableData.reset_date),''''];

     % rajashekhar added for two counter party case
         SqlQuery=['select * from deal_ticket_table_with_latest_versions_active_lots_view_',(char(InBUName)),...
           ' where counterparty_parent = ''',UniqueCP{iCP},''' and transaction_date < ''',...
            char(unique(ResetTableData.reset_date)),''''];
        [ColNames,Data] = Fetch_DB_Data(ObjDB,SqlQuery);
        
        if(isempty(Data))
%             OutErrorMsg = {'Index Swap deals not found for reset process'};
            continue;
        end
        
        if(strcmpi(Data,'No Data'))
%             OutErrorMsg = {'Index Swap deals not found for reset process'};
            continue;
        end        
               
        DealData=cell2table(Data);         
        DealData.Properties.VariableNames = ColNames;
        
        % remove the column names that are not needed in deal ticket table
        ActiveLots  = DealData.active_lots;
        SettleValue = DealData.settle_value;
        DealData.active_lots = [];
        DealData.settle_value = [];
        ColNames(end-1:end) = [];
        
        ActualDealsData = DealData;
        OffsetDealsData = DealData;
        ResetDealsData = DealData;
        DealHeader = ColNames;
        
        
        %% Sheet 1 Actual deal manipulations
        % Change deal status from live to dead.
        ActualDealsData.deal_status(:) = {'dead'};
        
        % commented
%         ActualDealsData.reset_date(:)= ResetTableData.reset_date;
        
     % added rajashekhar 
          ActualDealsData.reset_date(:)= unique(ResetTableData.reset_date);
        % Reset Status updation
%         ActualDealsData.reset_status(:) = {[char(unique(ResetTableData.reset_month)),' Reset']};
           ActualDealsData.reset_status(:) = {[char(ResetTableData.reset_month{iCP} ),' Reset']};
        % Version Comments actual_deal_resetted
        ActualDealsData.version_comments(:) = {'actual_deal_resetted'};
        
        ActualDealsData.version_no(:) = DealData.version_no(:) + 1;
        ActualDealsData.version_timestamp(:) = {''};
        ActualDealsData.version_created_by(:) = {''};
        
        %% Sheet 2 Offset_Deals sheet Manipulations
        % DUP-Concatenation
        
        OffsetDealsData.trade_id(:) = strcat('DUP-',DealData.trade_id);
        OffsetDealsData.parent_trade_id(:)=strcat('DUP-',DealData.parent_trade_id);
        
        Bought_Index = cellStrfind_exact(DealData.market_action,{'Bought'});
        Sold_Index   = cellStrfind_exact(DealData.market_action,{'Sold'});
        OffsetDealsData.market_action(Bought_Index) = {'Sold'};
        OffsetDealsData.market_action(Sold_Index) = {'Bought'};
       
%       OffsetDealsData.lots_to_be_netted(:) = (DealData.lots_to_be_netted .* -1);
%       OffsetDealsData.original_lots(:)     = (DealData.original_lots .* -1);
        OffsetDealsData.lots_to_be_netted(:) = ActiveLots .* -1;
        OffsetDealsData.original_lots(:)     = ActiveLots .* -1;
        
        OffsetDealsData.deal_status(:) = {'dead'};
        OffsetDealsData.version_comments(:) = {'offset_deal_resetted'};
                
        OffsetDealsData.reset_date(:)= unique(ResetTableData.reset_date);
        OffsetDealsData.reset_status(:) = {[char(ResetTableData.reset_month{iCP}),' Reset']};
        OffsetDealsData.transaction_date(:)=DBDateData.settlement_date;
        
        
        %% Sql query for Settlement price
        
%         SqlQuery= ['select settle_value from underlying_settle_value_table where asset_class= ''','commodity index swap',''' and value_date=''',char(DBDateData.value_date),''' '];
%         [~,SettleValue] = Fetch_DB_Data(ObjDB, SqlQuery);
        
        OffsetDealsData.premium(:)=SettleValue;
        
        OffsetDealsData.version_timestamp(:) = {''};
        OffsetDealsData.version_created_by(:) = {''};
        
        %% Reset Deal sheet
        DealLines = DealData.trade_id;
        TradeId  = getTradeID_4_Cell_Array(DealLines,InBUName);
        
        ResetDealsData.trade_id(:) = TradeId;
        ResetDealsData.parent_trade_id(:) = TradeId;
        ResetDealsData.version_comments(:) = {'MONTHLY_RESET_DEAL'};
        ResetDealsData.reset_date(:)= unique(ResetTableData.reset_date);
        
        ResetDealsData.transaction_date(:)=DBDateData.settlement_date;
        
        %% Original-lots and lots_to_be_netted Mathematical Calculation
%       New_Lots_U = round(((DealData.original_lots .* DealData.premium) ./ cell2mat(SettleValue)),8); % (original_lots * premium)/ Settleprice)
        New_Lots_U = round(((ActiveLots .* DealData.premium) ./ SettleValue),8); % (original_lots * premium)/ Settleprice)
        
%         ResetDealsData.original_lots(:)= New_Lots_U;
%         ResetDealsData.lots_to_be_netted(:)= New_Lots_U;
        ResetDealsData.original_lots(:)= ActiveLots;
        ResetDealsData.lots_to_be_netted(:)= ActiveLots;
        
        ResetDealsData.premium(:)=SettleValue;
        %   ResetDealsData.reset_status(:)={''};
        ResetDealsData.version_timestamp(:) = {''};
        ResetDealsData.version_created_by(:) = {''};
        
        AllActualDealsData = [AllActualDealsData;ActualDealsData];
        AllOffsetDealsData = [AllOffsetDealsData;OffsetDealsData];
        AllResetDealsData  = [AllResetDealsData;ResetDealsData];
        
        Reset_Tbl_Name = ['monthly_reset_date_table_',InBUName];
        SqlQuery_Update = ['update ',char(Reset_Tbl_Name),' set flag_monthly_reset=''YES'' where reset_month = ''',char(ResetTableData.reset_month{iCP}),''' and counterparty_parent = ''', UniqueCP{iCP},''' '];
        Curs = exec(ObjDB,SqlQuery_Update);
        
        if ~isempty(Curs.Message)
            OutErrorMsg = {Curs.Message};
        end
    end    

    %%% Newly added logic on 2019-12-02
    %%% Expired deals are only reflect in sheet �Actual_deals_for_reset�
    %%% not in �offset_deals� & �Reset_deals� sheets
        %%% offset_deals
        value_date = datenum(datestr(DBDateData.value_date,'yyyymmdd'),'yyyymmdd');

        [~,Offset_date] = strtok(AllOffsetDealsData.security_id,'.');
        [Offset_exp_date,~] = strtok(Offset_date,'.');
        Offset_expiry_date = datenum(Offset_exp_date,'yyyymmdd');
        Offset_Index = Offset_expiry_date < value_date;
        AllOffsetDealsData(Offset_Index,:) = [];

        %%% Reset_deals    
        [~,Reset_date] = strtok(AllResetDealsData.security_id,'.');
        [Reset_exp_date,~] = strtok(Reset_date,'.');
        Reset_expiry_date = datenum(Reset_exp_date,'yyyymmdd');
        Reset_Index = Reset_expiry_date < value_date;
        AllResetDealsData(Reset_Index,:) = [];
    %%% end code
    
        xlswrite(OutFileName,[DealHeader;table2cell(AllActualDealsData)],'Actual_deals_for_reset');
        xlswrite(OutFileName,[DealHeader;table2cell(AllOffsetDealsData)],'offset_deals');
        xlswrite(OutFileName,[DealHeader;table2cell(AllResetDealsData)],'Reset_deals');

        try
             xls_delete_sheets(fullfile(pwd,OutFileName));
             
            %% Reset Date Updated
      
%             try
%                 Reset_Tbl_Name = ['monthly_reset_date_table_',InBUName];
%                 SqlQuery_Update = ['update ',char(Reset_Tbl_Name),' set flag_monthly_reset=''YES'' where reset_month = ''',char(unique(ResetTableData.reset_month)),''' and counterparty_parent in (',Unique_CounterParty_Query,') '];
%                 Curs = exec(ObjDB,SqlQuery_Update);
%                 
%                 if ~isempty(Curs.Message)
%                     OutErrorMsg = {Curs.Message};
%                 end
%                 
%             catch
%                 
%             end
        
        
        
        catch
        end
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
    
end




