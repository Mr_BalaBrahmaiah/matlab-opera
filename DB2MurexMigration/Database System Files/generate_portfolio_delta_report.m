function [OutErrorMsg,OutFilename] = generate_portfolio_delta_report(InBUName)

OutErrorMsg = {'No errors'};
OutFilename = '';

try
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end
    
    OutFilename = getXLSFilename('Portfolio_Delta_Report');
    
    ViewName = 'helper_funds_reports_view';
    [ColNames,Data] = read_from_database(ViewName,0,'',InBUName);
    
    if ~strcmpi(Data,'No Data')        
       
        Find_GroupType_Col = cellStrfind(ColNames,'group_type');      
        Get_GroupType = Data(:,Find_GroupType_Col);
        ispresent_FUT_OFUT = cellStrfind(Get_GroupType,{'FUT'});
        Data = Data(ispresent_FUT_OFUT,:);        
         
        UniqueFields = {'portfolio','instrument','group_type','contract_month','counterparty_parent'};  % added counterparty parent
        SumFields = {'cumulative_mtm_usd','mtm_usd','settle_delta_1'};
        OutputFields = {'portfolio','instrument','group_type','contract_month',...   
            'cumulative_mtm_usd','mtm_usd','settle_delta_1','counterparty_parent'};% added counterparty parent
        WeightedAverageFields = [];
        [~,OutData] = consolidatedata(ColNames, Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
        
        OutHeader = {'portfolio','Underlying','Group','Maturity Label','CUMULATIVE_BV','Book_Value','Delta'}; 
        xlswrite(OutFilename,[OutHeader;OutData(:,(1:end-1))]);
        
        %%% Newly added on 22-01-2020, for counterparty parent
        OutHeader = {'portfolio','Underlying','Group','Maturity Label','CUMULATIVE_BV','Book_Value','Delta','Counterparty'}; 
        xlswrite(OutFilename,[OutHeader;OutData],'Sheet2');
        
        xls_change_activesheet(fullfile(pwd,OutFilename) ,'Sheet1');
        
    else
        OutErrorMsg = {'No data found'};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
end