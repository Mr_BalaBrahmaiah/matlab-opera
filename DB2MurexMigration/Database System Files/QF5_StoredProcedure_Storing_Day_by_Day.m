function [OutErrorMsg ,OutXLSName ] = QF5_StoredProcedure_Storing_Day_by_Day(InBUName,Trade_Date)

% Using Stored Procecdure

% InBUName = 'qf5';
% Trade_Date = '2018-04-30';
% End_Date = '2018-06-01';

%%

try
    OutErrorMsg = {'No Errors'};
    
    OutXLSName = getXLSFilename('QF5_Borrowing_InterestRate');
    
    %% Store Procedure Name & VDate_SDate Table
    
    ObjDB = connect_to_database;
    
%     StoreProcedure_Name = ['generate_position_monitoring_data_',char(InBUName)]; % Old
    StoreProcedure_Name = ['store_borrowing_report_data_',char(InBUName)]; % New for Testing
    
    SqlQuery_ValueDate = ['select * from vdate_sdate_table where settlement_date = ''',char(Trade_Date),''' '] ;
    %     SqlQuery_ValueDate = ['select * from vdate_sdate_table where settlement_date >=''',char(Trade_Date),''' and settlement_date <''',char(End_Date),''' '] ;
    [~,Overall_Value_Setttle_Date] = Fetch_DB_Data(ObjDB,SqlQuery_ValueDate);
    
    if(isempty(Overall_Value_Setttle_Date))
        OutErrorMsg = {'Not Getting Data from vdate_sdate_table'};
        return;
    end
    
    Overall_ValueDate = Overall_Value_Setttle_Date(:,1);
    Overall_SettleDate = Overall_Value_Setttle_Date(:,2);
    
    Uploading_TableName = ['helper_evaluation_reports_store_proc_table_',char(InBUName)];
    
    %%
    
    for i = 1 : size(Overall_ValueDate)
        Current_ValueDate = Overall_ValueDate(i);
        
        %% Delete Previous Data
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from helper_evaluation_reports_store_proc_table_qf5 where value_date = ''',char(Current_ValueDate),''' '];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        %%
        [ColNames,DBData_All,ObjDB_ErrorMsg] = Call_StoredProcedure(ObjDB,StoreProcedure_Name,Current_ValueDate); % Old
        
        SqlQuery_2 = 'insert into helper_evaluation_reports_store_proc_table_qf5 select * from temp_helper_evaluation_reports_store_proc_table_qf5';
        curs = exec(ObjDB,SqlQuery_2);
        
        cprintf('key','%d : %s : %d \n',i,char(Current_ValueDate),length(Overall_ValueDate));
        
        %%
        
        if(strcmpi(DBData_All,'No Data'))
            OutErrorMsg = {'Not Getting Data from Database ,Check DB'};
            continue;
        end
        
        if(isempty(DBData_All))
            OutErrorMsg = {'Not Getting Data from Database ,Check DB'};
            continue;
        end
        
        Temp_ValueDate = cell(size(DBData_All,1),1);
        Temp_ValueDate(:,:) = Current_ValueDate;
        
        Uploading_Data = [DBData_All ,Temp_ValueDate];
        
        %% For SQL Date Format NULL
        
        %         NettingDate_Empty_Index = cell2mat(cellfun(@(s) (isempty(s)), Uploading_Data(:,14),'UniformOutput',0));
        %         ResetDate_Empty_Index = cell2mat(cellfun(@(s) (isempty(s)), Uploading_Data(:,15),'UniformOutput',0));
        %         Uploading_Data(NettingDate_Empty_Index,14) = {'NULL'};
        %         Uploading_Data(ResetDate_Empty_Index,15) = {'NULL'};
        
        %% Uploading Table Name
        
        %         if(~isempty(Uploading_Data))
        %             set(ObjDB,'AutoCommit','off');
        %             SqlQuery = ['delete from ',Uploading_TableName,' where value_date = ''',char(Current_ValueDate),''' '];
        %             curs = exec(ObjDB,SqlQuery);
        %             commit(ObjDB);
        %             if ~isempty(curs.Message)
        %                 disp(curs.Message);
        %             end
        %             set(ObjDB,'AutoCommit','on');
        %
        %             upload_in_database(Uploading_TableName, Uploading_Data);
        %
        %             cprintf('key','%s : %d : upload into %s finished\n',char(Current_ValueDate),length(Overall_ValueDate),char(Uploading_TableName));
        %
        %         end
        
        
        
    end
    
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end