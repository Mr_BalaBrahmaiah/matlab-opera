function run_recon_views_network_cfs

% DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\DBSystem_Recon_dumps';    
DestinationFolder = '\\10.190.7.104\InvenioDatabaseSystem\OPERA_RECON_DUMPS\CFS';

LogFilename = ['DB_automation_log_',datestr(today),'.txt'];

try 
%% generate and write the settle recon dump
ViewName = 'helper_settle_p_and_g_view_cfs';
% % ViewName = 'helper_settle_p_and_g_view_cache_table_cfs';
[ColNames,Data] = read_from_database(ViewName,0);

PosSubportfolio = strcmpi(ColNames,'subportfolio');
IdxRMS = strncmpi('RMS-',Data(:,PosSubportfolio),length('RMS-'));
IdxMMVT = ~IdxRMS;
RMSData = Data(IdxRMS,:);
MMVTData = Data(IdxMMVT,:);

OutMMVTFilename = getXLSFilename('OPERA_CFS_MMVT_SettleRecon');
xlswrite(OutMMVTFilename,[ColNames;MMVTData]);
OutRMSFilename = getXLSFilename('OPERA_CFS_RMS_SettleRecon');
xlswrite(OutRMSFilename,[ColNames;RMSData]);

OutFilename = fullfile(DestinationFolder,OutMMVTFilename);
move_reports(OutMMVTFilename,OutFilename,'MMVT Settle Recon dump');
OutFilename = fullfile(DestinationFolder,OutRMSFilename);
move_reports(OutRMSFilename,OutFilename,'RMS Settle Recon dump');

%% generate and write the traders recon dump
ViewName = 'helper_traders_p_and_g_view_cfs';
% % ViewName = 'helper_traders_p_and_g_view_cache_table_cfs';
[ColNames,Data] = read_from_database(ViewName,0);

PosSubportfolio = strcmpi(ColNames,'subportfolio');
IdxRMS = strncmpi('RMS-',Data(:,PosSubportfolio),length('RMS-'));
IdxMMVT = ~IdxRMS;
RMSData = Data(IdxRMS,:);
MMVTData = Data(IdxMMVT,:);

OutMMVTFilename = getXLSFilename('OPERA_CFS_MMVT_TradersRecon');
xlswrite(OutMMVTFilename,[ColNames;MMVTData]);
OutRMSFilename = getXLSFilename('OPERA_CFS_RMS_TradersRecon');
xlswrite(OutRMSFilename,[ColNames;RMSData]);

OutFilename = fullfile(DestinationFolder,OutMMVTFilename);
move_reports(OutMMVTFilename,OutFilename,'MMVT Traders Recon dump');
OutFilename = fullfile(DestinationFolder,OutRMSFilename);
move_reports(OutRMSFilename,OutFilename,'RMS Traders Recon dump');


catch ME
    write_log_file(LogFilename,ME.message);
end