function  [OTC_Swaps_Data_Output_Total] = Find_Statement_ContractMonth_Wise_Swaps_Total(OTC_Swaps_Data_Output_Header,OTC_Swaps_Data_Output)

OTC_Swaps_Data_Output_Total = '';

try
    Fixed_Header_Col_Size = size(OTC_Swaps_Data_Output_Header,2);
    
    Ref_Month_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'REF_MONTH'});
    ExpiryDate_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'EXPIRY_DATE'});
    
    %% Introduce Total Based on these Columns & Average Columns
    Total_Sum_Required_Col = {'LONG_LOTS','AVG_LONG_PRICE','SHORT_LOTS','AVG_SHORT_PRICE','OR_EDSP_USD','EUR_USD_FORWARD','OR_EDSP_EUR ','EDSP','MTM_NC','MTM_USD','MTM_EUR'};
    Total_Sum_Required_Col_Index = cellStrfind_exact(OTC_Swaps_Data_Output_Header,Total_Sum_Required_Col);
    
    Avg_Sum_Required_Col = {'EDSP'};
    Avg_Sum_Required_Col_Index = cellStrfind_exact(OTC_Swaps_Data_Output_Header,Avg_Sum_Required_Col);
    
    LONG_LOTS_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'LONG_LOTS'});    
    AVG_LONG_PRICE_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'AVG_LONG_PRICE'});
    SHORT_LOTS_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'SHORT_LOTS'});
    AVG_SHORT_PRICE_Col = cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'AVG_SHORT_PRICE'});
    OR_EDSP_USD_Col=cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'OR_EDSP_USD'});
    EUR_EDSP_FORWARD_Col=cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'EUR_USD_FORWARD'});
    OR_EDSP_EUR_Col=cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'OR_EDSP_EUR '});
    MTM_EUR_Col=cellStrfind_exact(OTC_Swaps_Data_Output_Header,{'MTM_EUR'});
    
    
    
    %% Sorting Based On these Col
    
%     OTC_Swaps_Data_Output = sortrows(OTC_Swaps_Data_Output,Ref_Month_Col);
%     OTC_Swaps_Data_Output = sortrows(OTC_Swaps_Data_Output,ExpiryDate_Col);
    
    Unique_RefMonth = unique(OTC_Swaps_Data_Output.REF_MONTH,'stable');
    
    %%
    OTC_Swaps_Data_Output_CellArray = dataset2cell(OTC_Swaps_Data_Output);
    
    %     OTC_Swaps_Data_Output_Header = OTC_Swaps_Data_Output_CellArray(1,:);
    OTC_Swaps_Data_Output_CellArray(1,:) = [];
    
    Total_Overall_ColSum_Array = cell(2,Fixed_Header_Col_Size);
     [~,Overall_ColSum] = cell2sum_Row_Col(OTC_Swaps_Data_Output_CellArray(:,Total_Sum_Required_Col_Index));
     Total_Overall_ColSum_Array(2,Total_Sum_Required_Col_Index) = Overall_ColSum ;
     Total_Overall_ColSum_Array(2,1) = cellstr('TOTAL');
    
    %%
    
    OTC_Swaps_Data_Output_Total = [];
    
    for i  = 1 : length(Unique_RefMonth)
        Current_RefMonth = Unique_RefMonth(i) ;
        
        Matched_Ref_Month = strcmpi(OTC_Swaps_Data_Output_CellArray(:,Ref_Month_Col),Current_RefMonth) ;
        if(~isempty( find(Matched_Ref_Month,1)))
            Matched_Rows = OTC_Swaps_Data_Output_CellArray(Matched_Ref_Month,:);
            
            Num_of_Rows = size(Matched_Rows,1);
            
            Total_Empty_Array = cell(2,Fixed_Header_Col_Size);
            [~,ColSum] = cell2sum_Row_Col(Matched_Rows(:,Total_Sum_Required_Col_Index));            
                       
            Total_Empty_Array(2,Total_Sum_Required_Col_Index) = ColSum ;
            Total_Empty_Array(2,5) =  cellstr('TOTAL'); % strcat(Current_RefMonth,' TOTAL');
            
            Total_Empty_Array(2,AVG_LONG_PRICE_Col) = num2cell( nansum(cell2mat(Matched_Rows(:,LONG_LOTS_Col)) .*  cell2mat(Matched_Rows(:,AVG_LONG_PRICE_Col))) ./ cell2mat(Total_Empty_Array(2,LONG_LOTS_Col)) );
            Total_Empty_Array(2,AVG_SHORT_PRICE_Col) = num2cell( nansum(cell2mat(Matched_Rows(:,SHORT_LOTS_Col)) .*  cell2mat(Matched_Rows(:,AVG_SHORT_PRICE_Col))) ./ cell2mat(Total_Empty_Array(2,SHORT_LOTS_Col)) );
            
            Total_Empty_Array(2,Avg_Sum_Required_Col_Index) = num2cell(cell2mat(Total_Empty_Array(2,Avg_Sum_Required_Col_Index))./Num_of_Rows);
            
            OTC_Swaps_Data_Output_Total = [OTC_Swaps_Data_Output_Total ; [Matched_Rows ; Total_Empty_Array] ] ;
            
        end
    end
    
    OTC_Swaps_Data_Output_Total = [OTC_Swaps_Data_Output_Total ; Total_Overall_ColSum_Array];
    
    %%
    
    OTC_Swaps_Data_Output_Total = cell2dataset([OTC_Swaps_Data_Output_Header ; OTC_Swaps_Data_Output_Total]) ;
    
catch
    OTC_Swaps_Data_Output_Total = '';
end