[filename, pathname] = uigetfile( ...
    {'*.xlsx';'*.xls';'*.csv';'*.*'}, ...
    'Pick a file','MultiSelect', 'on');

OutFilename = 'Consolidated_Future_dump_reports_2015.xlsx';

if pathname ~= 0
    StrFilenames = strrep(filename,'pnp_future_dump_','');
    StrFilenames = StrFilenames';
    StrFilenames = strrep(StrFilenames,'.xlsx','');
    StrFilenames = strrep(StrFilenames,'.xls','');
    StrFilenames = strrep(StrFilenames,'.csv','');
    
    [Dates,Times] = strtok(StrFilenames,'_');
    Times = strrep(Times,'_','');
        
    OutData = [];
    for iFile = 1:length(filename)
        Fullfilename = fullfile(pathname,filename{iFile});
        [~,~,RawData ] = xlsread(Fullfilename);
        TRData = cell2dataset(RawData);
        ValueDate = repmat(Dates(iFile),size(TRData.Portfolio));
        UpdatedTimestamp = repmat(Times(iFile),size(TRData.Portfolio));
        TRData.ValueDate = ValueDate;
        TRData.UpdatedTimestamp = UpdatedTimestamp;        

        OutData = [OutData;TRData];
    end
    
    TempData = sortrows(OutData,[13,14]);
    
%     ConsolidatedData = unique(TempData,TempData.Properties.VarNames(1:end-1),'legacy');
    xlswrite(OutFilename,dataset2cell(TempData));
    
end