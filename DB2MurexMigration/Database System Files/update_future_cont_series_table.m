function update_future_cont_series_table

[ColNames,Data] = read_from_database('var_product_code_mapping_table',0);
ProductCodeMapping = cell2dataset([ColNames;Data]);

objDB = connect_to_database;
SDate = fetch(objDB,'select settlement_date from valuation_date_table');

FutureColNames = {'underlying_id','expiry_date','invenio_product_code'};
FutData = fetch(objDB,'select * from helper_get_underlying_id_for_var');
FutureData = cell2dataset([FutureColNames;FutData]);

IdxProductsNotSelected = ~ismember(FutureData.invenio_product_code,ProductCodeMapping.invenio_product_code);
FutureData(IdxProductsNotSelected,:) = [];

FutureContinuationSeries = cell(size(FutureData.invenio_product_code));
SettleDateUnderlyingId   = cell(size(FutureData.invenio_product_code));

% identify the future and option continuation series for this settlement date
Products = unique(FutureData.invenio_product_code);
for iProd = 1:length(Products)
    IdxMapping = strcmpi(Products{iProd},ProductCodeMapping.invenio_product_code);
    if isempty(find(IdxMapping))
        continue;
    end
    ContMonthFormatProduct = ProductCodeMapping.cont_month_format_code{IdxMapping};
    
    IdxProduct = strcmpi(Products{iProd},FutureData.invenio_product_code);
    
    % identify the future continuation series; used with price_change
    % data
    TempContracts = FutureData.underlying_id(IdxProduct);
    TempMaturity = FutureData.expiry_date(IdxProduct);
    
    %         [A,IA,IC] = unique(TempMaturity,'sorted');
    [A,IA] = sort(TempMaturity);
    FutureContracts = TempContracts(IA);
    
    TempCSeries = (1:length(FutureContracts))';
    
    CSeries = cell(size(TempCSeries));
    for iFut=1:length(TempCSeries)
        CSeries{iFut} = [ContMonthFormatProduct,num2str(TempCSeries(iFut))];
    end
    for iContract = 1:length(FutureContracts)
        IdxContract = IdxProduct & strcmpi(FutureContracts{iContract},FutureData.underlying_id);
        FutureContinuationSeries(IdxContract) = CSeries(iContract);
        SettleDateUnderlyingId(IdxContract) = SDate;
    end
        
end
%     OutFilename = getXLSFilename('future_cont_series_table');
FutHeader = {'settlement_date','underlying_id','cont_month'};
OutFutureData = [SettleDateUnderlyingId,FutureData.underlying_id,FutureContinuationSeries];
%     xlswrite(OutFilename,[FutHeader;OutFutureData]);
try
fastinsert(objDB, 'future_cont_series_table', FutHeader,OutFutureData);
catch
end




