function [OutputFields,ConsDeals] = compute_monthend_eod_values(InBUName,InData,Formula_Restrict_Str)

NumRows = length(InData.subportfolio);

Product = cell(NumRows,1);
OpeningBalanceUSD = zeros(NumRows,1);
OpeningBalanceNC = zeros(NumRows,1);
RealisedPremiumPaidUSD = zeros(NumRows,1);
RealisedPremiumPaidNC  = zeros(NumRows,1);
Brokerage = zeros(NumRows,1);
Instrument = cell(NumRows,1);

Realised_mtm_usd  = zeros(NumRows,1);
Unrealised_mtm_usd = zeros(NumRows,1);
Realised_mtm_nc = zeros(NumRows,1);
Unrealised_mtm_nc = zeros(NumRows,1);

Total_mtm_usd = zeros(NumRows,1);
Total_mtm_nc = zeros(NumRows,1);

DB_realised_cumulative_mtm_nc  = zeros(NumRows,1);
DB_unrealised_cumulative_mtm_nc = zeros(NumRows,1);
DB_realised_cumulative_mtm_usd = zeros(NumRows,1);
DB_unrealised_cumulative_mtm_usd = zeros(NumRows,1);

objDB = connect_to_database;

IdxDBDead = strcmpi('dead',InData.deal_status);
IdxDBLive = strcmpi('live',InData.deal_status);

DB_realised_cumulative_mtm_nc(IdxDBDead)   = InData.cumulative_mtm_nc(IdxDBDead);
DB_unrealised_cumulative_mtm_nc(IdxDBLive) = InData.cumulative_mtm_nc(IdxDBLive);
DB_realised_cumulative_mtm_usd(IdxDBDead) = InData.cumulative_mtm_usd(IdxDBDead);
DB_unrealised_cumulative_mtm_usd(IdxDBLive) = InData.cumulative_mtm_usd(IdxDBLive);

Group = InData.group_type;
AccGroup = InData.acc_group;
AssetClass = InData.asset_class;

IdxORSwap = strncmpi(InData.product_code,'OR',2);
InData.product_code(IdxORSwap) = {'OR'};

%  for iCode = 1:length(RefProductCode)  
%      IdxProd = strcmpi(strtrim(RefProductCode(iCode)),InData.product_code);
%      Product(IdxProd) = RefProduct(iCode);
%      Instrument(IdxProd) = RefInstrument(iCode);
%  end

IdxLondonProd = strcmpi('QC',InData.product_code) | strcmpi('DF',InData.product_code);
IdxOptions = contains(AccGroup,'options','IgnoreCase',true); %added 'contains' replacement of 'strcmpi' on 03-02-2020
IdxFutures = ~IdxOptions | IdxLondonProd;

TradeDates = datenum(InData.transaction_date,'yyyy-mm-dd');
% ExpDate = datenum('2016-01-01','yyyy-mm-dd');
SqlQuery = 'SELECT start_date FROM valuation_financial_year_view';
StartDate = char(fetch(objDB,SqlQuery));
ExpDate = datenum(StartDate,'yyyy-mm-dd');
IdxDateCheck = TradeDates >= ExpDate;

%added 'contains' replacement of 'strcmpi' on 03-02-2020
IdxDeadOpt = contains(AccGroup,'options','IgnoreCase',true) & IdxDateCheck;
IdxLiveOpt = contains(AccGroup,'options','IgnoreCase',true) & strcmpi('live',InData.deal_status);
IdxAllOpt = contains(AccGroup,'options','IgnoreCase',true);

% IdxRMS = strcmpi('options',AccGroup) & (strncmpi('RMS-SB',InData.subportfolio,length('RMS-SB')) | ...
%     strncmpi('RMS-CT',InData.subportfolio,length('RMS-CT')) | ...
%     strncmpi('RMS-C-',InData.subportfolio,length('RMS-C-')) | ...
% strncmpi('RMS-KC',InData.subportfolio,length('RMS-KC')));
IdxOID   = zeros(NumRows,1);   IdxOID   = logical(IdxOID);
IdxOID(cellStrfind(InData.subportfolio,'OID')) = 1;

IdxRMS = contains(AccGroup,'options','IgnoreCase',true) & ... %added 'contains' replacement of 'strcmpi' on 03-02-2020
    (strncmpi('RMS-',InData.subportfolio,length('RMS-')) | strncmpi('RMS-',InData.counterparty_parent,length('RMS-'))) & ~IdxOID;

% if strcmpi(InBUName,'cfs') %% as this is_premium_paid is applicable only for RMS BU
%     IdxLondonProdPremPaid = IdxLondonProd & strcmpi('options',AccGroup) & (InData.is_premium_paid == 1);
% else
    IdxLondonProdPremPaid = IdxLondonProd & contains(AccGroup,'options','IgnoreCase',true);%added 'contains' replacement of 'strcmpi' on 03-02-2020
% end

IdxFXD = strcmpi('FXD',Group);
IdxFXFUT = strcmpi('FXFUT',Group);

 % realised option value = premium_paid of the dead options
% unrealised option value = mkt value of the live options
Realised_mtm_usd(IdxDeadOpt) = InData.premium_paid_usd(IdxDeadOpt);
Unrealised_mtm_usd(IdxLiveOpt) = InData.mkt_value_usd(IdxLiveOpt);
Realised_mtm_nc(IdxDeadOpt) = InData.premium_paid(IdxDeadOpt);
Unrealised_mtm_nc(IdxLiveOpt) = InData.mkt_value(IdxLiveOpt);

Realised_mtm_usd(IdxRMS) = InData.realised_pnl_usd(IdxRMS);
Unrealised_mtm_usd(IdxRMS) = InData.unrealised_pnl_usd(IdxRMS);
Realised_mtm_nc(IdxRMS) = InData.realised_pnl(IdxRMS) ;
Unrealised_mtm_nc(IdxRMS) = InData.unrealised_pnl(IdxRMS) ;

% realised/unrealised values for futures = cumulative_mtm calculation
Realised_mtm_usd(IdxFutures) = DB_realised_cumulative_mtm_usd(IdxFutures);
Unrealised_mtm_usd(IdxFutures) = DB_unrealised_cumulative_mtm_usd(IdxFutures);
Realised_mtm_nc(IdxFutures) = DB_realised_cumulative_mtm_nc(IdxFutures);
Unrealised_mtm_nc(IdxFutures) =DB_unrealised_cumulative_mtm_nc(IdxFutures);

Realised_mtm_usd(IdxLondonProdPremPaid) = InData.realised_pnl_usd(IdxLondonProdPremPaid);
Unrealised_mtm_usd(IdxLondonProdPremPaid) = InData.unrealised_pnl_usd(IdxLondonProdPremPaid);
Realised_mtm_nc(IdxLondonProdPremPaid) = InData.realised_pnl(IdxLondonProdPremPaid);
Unrealised_mtm_nc(IdxLondonProdPremPaid) =InData.unrealised_pnl(IdxLondonProdPremPaid);

SqlQuery = 'SELECT financial_year FROM valuation_financial_year_view';
FinYear = char(fetch(objDB,SqlQuery));
TableName = ['subportfolio_fy_opening_balance_table_',InBUName];
SqlQuery = ['select asset_class,subportfolio,product_code,counterparty,option_acc_group,option_group_type,opening_balance_USD,opening_balance_NC,realised_premium_paid_usd,realised_premium_paid_nc from ',TableName,' where financial_year = ''',FinYear,''''];
DBSubportfolioOpenBalData = fetch(objDB,SqlQuery);
if ~isempty(DBSubportfolioOpenBalData)
    DBAssetClass = DBSubportfolioOpenBalData(:,1);
    DBSubportfolio = DBSubportfolioOpenBalData(:,2);
    DBProductCode = DBSubportfolioOpenBalData(:,3);
    DBCounterparty = DBSubportfolioOpenBalData(:,4);
    DBOptionGroup = DBSubportfolioOpenBalData(:,5);
    DBOptGroupType = DBSubportfolioOpenBalData(:,6);
    DBOpenBalanceUSD = cell2mat(DBSubportfolioOpenBalData(:,7));
    DBOpenBalanceNC = cell2mat(DBSubportfolioOpenBalData(:,8));
    DBRealisedPremiumPaidUSD = cell2mat(DBSubportfolioOpenBalData(:,9));
    DBRealisedPremiumPaidNC  = cell2mat(DBSubportfolioOpenBalData(:,10));
    
    for iBal = 1:length(DBSubportfolio)
        IdxBal = strcmpi(DBAssetClass(iBal),AssetClass) & ...
        strcmpi(DBSubportfolio(iBal),InData.subportfolio) & ...
            strcmpi(DBProductCode(iBal),InData.product_code) & ...
            strcmpi(DBCounterparty(iBal),InData.counterparty_parent) & ...
            strcmpi(DBOptionGroup(iBal),AccGroup) & ...
            strcmpi(DBOptGroupType(iBal),Group);
        OpeningBalanceUSD(IdxBal) = DBOpenBalanceUSD(iBal);
        OpeningBalanceNC(IdxBal) = DBOpenBalanceNC(iBal);
        RealisedPremiumPaidUSD(IdxBal) = DBRealisedPremiumPaidUSD(iBal);
        RealisedPremiumPaidNC(IdxBal)  = DBRealisedPremiumPaidNC(iBal);
    end
end

if ~strcmpi(InBUName,'cfs')
    Realised_mtm_nc(IdxFXD)   = DB_realised_cumulative_mtm_usd(IdxFXD);
    Unrealised_mtm_nc(IdxFXD) = DB_unrealised_cumulative_mtm_usd(IdxFXD);
    OpeningBalanceNC(IdxFXD)  = OpeningBalanceUSD(IdxFXD);
    InData.currency(IdxFXD)   = {'USD'};
    InData.currency(IdxFXFUT)   = {'USD'};
end

%%% Newly added code
if strcmpi(InBUName,'cfs')
    IdxFXOPT = strcmpi('FX_OPT',Group);
    %[~,Indxloc] = ismember(InData.maturity_date,InData.settlement_date);
    Indxloc = datetime(InData.maturity_date) <= datetime(InData.settlement_date);
    IdxMatCheck = IdxFXOPT & Indxloc;

    Realised_mtm_nc(IdxMatCheck,1) = InData.realised_pnl(IdxMatCheck,:) + InData.mkt_value_usd(IdxMatCheck,:);
    Unrealised_mtm_nc(IdxMatCheck,1) = InData.unrealised_pnl(IdxMatCheck,:) - InData.mkt_value_usd(IdxMatCheck,:);
    
    Realised_mtm_usd(IdxMatCheck,1) = InData.realised_pnl_usd(IdxMatCheck,:) + InData.mkt_value_usd(IdxMatCheck,:);
    Unrealised_mtm_usd(IdxMatCheck,1) = InData.unrealised_pnl_usd(IdxMatCheck,:) - InData.mkt_value_usd(IdxMatCheck,:);
end
%%% End Code

OutputHeader = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT','PORTFOLIO',...
    'UNDERLYING','CURRENCY','Opening_Balance_USD','Realized_USD','M2M_USD',...
    'Total_USD','BROKERAGE_USD','Net_Total_USD','Opening_Balance_NC',...
    'Realized_NC','M2M_NC','Total_NC','BROKERAGE_NC','Net_Total_NC',...
    'RealisedPremiumPaidUSD','RealisedPremiumPaidNC'};
OutData = [AssetClass,AccGroup,Group,upper(InData.counterparty_parent),InData.product_name,InData.subportfolio,...
    InData.instrument,upper(InData.currency),num2cell(OpeningBalanceUSD),num2cell(Realised_mtm_usd),num2cell(Unrealised_mtm_usd),...
    num2cell(Total_mtm_usd),num2cell(Brokerage),num2cell(Total_mtm_usd),num2cell(OpeningBalanceNC),...
    num2cell(Realised_mtm_nc),num2cell(Unrealised_mtm_nc),num2cell(Total_mtm_nc),num2cell(Brokerage),num2cell(Total_mtm_nc),...
    num2cell(RealisedPremiumPaidUSD),num2cell(RealisedPremiumPaidNC)];

% Do the netting
%if strcmpi(InBUName,'ogp') || strcmpi(InBUName,'usg') %%% Newly added oce(15-05-2019 --> unique field - UNDERLYING)
%    UniqueFields = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT','PORTFOLIO','UNDERLYING'};
%else
    UniqueFields = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT','PORTFOLIO'};
%end
SumFields={'Realized_USD','M2M_USD','Total_USD','Net_Total_USD','Realized_NC','M2M_NC','Total_NC','Net_Total_NC'};
OutputFields = OutputHeader;
WeightedAverageFields = [];
[OutputFields,ConsDeals] = consolidatedata(OutputHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
ConsDeals = cell2dataset([OutputFields;ConsDeals]);    

if(exist('Formula_Restrict_Str','var'))
    if(~isempty(cellStrfind_exact({'FTD Data','FTM Data'},{Formula_Restrict_Str}))) 
        % No  Need to Subtraction for the  FTD& FTM
    else
        ConsDeals.Realized_USD = ConsDeals.Realized_USD - ConsDeals.RealisedPremiumPaidUSD;
        ConsDeals.Realized_NC  = ConsDeals.Realized_NC - ConsDeals.RealisedPremiumPaidNC;
    end
else
    ConsDeals.Realized_USD = ConsDeals.Realized_USD - ConsDeals.RealisedPremiumPaidUSD;
    ConsDeals.Realized_NC  = ConsDeals.Realized_NC - ConsDeals.RealisedPremiumPaidNC;
end

Total_mtm_usd = ConsDeals.Opening_Balance_USD +  ConsDeals.Realized_USD + ConsDeals.M2M_USD ;
Total_mtm_nc = ConsDeals.Opening_Balance_NC +  ConsDeals.Realized_NC +  ConsDeals.M2M_NC ;

ConsDeals.Total_USD = num2cell(Total_mtm_usd);
ConsDeals.Net_Total_USD = num2cell(Total_mtm_usd);

ConsDeals.Total_NC = num2cell(Total_mtm_nc);
ConsDeals.Net_Total_NC = num2cell(Total_mtm_nc);

ConsDeals.RealisedPremiumPaidUSD = [];
ConsDeals.RealisedPremiumPaidNC = [];

TempOutData = dataset2cell(ConsDeals);

OutputFields = TempOutData(1,:);
ConsDeals = TempOutData(2:end,:);