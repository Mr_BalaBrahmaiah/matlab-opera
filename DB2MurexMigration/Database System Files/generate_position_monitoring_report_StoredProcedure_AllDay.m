function [OutErrorMsg,OutXLSfileName] = generate_position_monitoring_report_StoredProcedure_AllDay(InBUName)

% Final
% InBUName = 'agf';

try
    OutErrorMsg = {'No Errors'};
    OutXLSfileName = getXLSFilename(['Position_Monitoring_Report_',upper(char(InBUName))]);
    
    ObjDB = connect_to_database ;
    
    Current_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    FY_StartDate = fetch(ObjDB,'select start_date from valuation_financial_year_view');
    
    %% Overall Day
    SqlQuery_Overall_SettleDate_ValueDate =['select * from vdate_sdate_table where settlement_date >=''',char(FY_StartDate),''' and settlement_date <=''',char(Current_SettleDate),''' ' ];
    [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %% Product Name
    
    %     Unique_ProductName = unique(Overall_DBData.product_name);
    Unique_ProductName = fetch(ObjDB,'select product_name from position_monitoring_report_products_table') ;
    Unique_ProductName = sort(Unique_ProductName);
    
    
    %% Initialization
    
    Output_Header = {'COB Date','product_code','product_name','group_type','contract_month','Position','First Month Position','All month position','Net position','Gross Position'};
    First_MonthPosition_Col = cellStrfind_exact(Output_Header,{'First Month Position'});
    All_MonthPosition_Col = cellStrfind_exact(Output_Header,{'All month position'});
    Net_Position_Col = cellStrfind_exact(Output_Header,{'Net position'});
    Gross_Position_Col = cellStrfind_exact(Output_Header,{'Gross Position'});
    
    Summary_Header = {'Criteria','First Month Position','All month position','Net position','Gross Position'};
    Summary_RowIndex = {'Max','Min','Average','Median','5th Percentile','95th Percentile'};
    
    Overall_Summary_Header = [{'Product Summary'},Summary_Header];
    Summary_OutputArray = [];
    Overall_Summary_OutputArray = [];
    
    Output_Array = [];
    Overall_Uploading_Data = [];
    All_Product_Overall_Uploading_Data = [];
    
    %% Consolidate Level
    
    UniqueFields = {'bbg_product_code','product_name','derivative_type','contract_month'};
    SumFields = {'delta'};
    OutputFields = [UniqueFields,SumFields];
    WeightedAverageFields = [];
    
    ActiveLots_Col = cellStrfind_exact(OutputFields,{'delta'});
    
    %% Financial Year Start Date
    Year_StartDate = fetch(ObjDB,'select start_date from valuation_financial_year_view');
    Year_StartDate_Number = datenum(Year_StartDate,'yyyy-mm-dd');
    
    %% Stored Procedure Name
    StoreProcedure_Name = ['generate_position_monitoring_data_',char(InBUName)];
    
    for ii = 1 : size(Unique_ProductName,1)
        
        Current_ProductName = Unique_ProductName(ii);
        for i = 1 : length(Overall_ValueDate)
            
            Current_ValueDate = Overall_ValueDate(i);
            Current_SettleDate = Overall_SettleDate(i);
            
            %% Fetch Data
            
            [ColNames,DDBData] = Call_StoredProcedure(ObjDB,StoreProcedure_Name,Current_ValueDate);
            
            if(strcmpi(DDBData,'No Data'))
                OutErrorMsg = {'No Data from Database ,Check DB'};
                continue;
            end
            
            if(isempty(DDBData))
                OutErrorMsg = {'No Data from Database ,Check DB'};
                continue;
            end
            
            Overall_DBData = cell2dataset([ColNames ; DDBData]);
            
            if(strcmpi(InBUName,'cfs'))
                if(strcmpi(Current_ProductName,'SICOM RUBBER'))
                    Future_Index = (strcmpi(Overall_DBData.derivative_type,'future') | strcmpi(Overall_DBData.derivative_type,'vanilla_call') | strcmpi(Overall_DBData.derivative_type,'vanilla_put'))...
                        & strcmpi(Overall_DBData.counterparty_parent,'PHILIP FUTURES');
                else
                    Future_Index = (strcmpi(Overall_DBData.derivative_type,'future') | strcmpi(Overall_DBData.derivative_type,'vanilla_call') | strcmpi(Overall_DBData.derivative_type,'vanilla_put'))...
                        & strcmpi(Overall_DBData.counterparty_parent,'Wells Fargo RMS');
                end
                Changing_DerivativeType = {'FUT/OFUT'};
            elseif(strcmpi(InBUName,'agf'))
                Future_Index = strcmpi(Overall_DBData.derivative_type,'future') | strcmpi(Overall_DBData.derivative_type,'vanilla_call') | strcmpi(Overall_DBData.derivative_type,'vanilla_put');
                Changing_DerivativeType = {'FUT/OFUT'};
            else
                Future_Index = strcmpi(Overall_DBData.derivative_type,'future');
                Changing_DerivativeType = {'FUT'};
            end
            
            Overall_DBData.derivative_type(Future_Index,:) = Changing_DerivativeType; %% Consolidation Purpose
            
            Overall_DBData = Overall_DBData(Future_Index,:);
            
            %% Remove Zero Active Lots
            
            if(isempty(Overall_DBData))
                continue ;
            end
            
            Zero_Index = Overall_DBData.delta ==  0; %% Zero_Index = Overall_DBData.active_lots ==  0;
            Overall_DBData(Zero_Index,:) = [];
            
            Current_ProductName_Data = Overall_DBData(strcmpi(Overall_DBData.product_name,Current_ProductName),:);
            
            if(isempty(Current_ProductName_Data))
                continue ;
            end
            
            %%
            Unique_TransactionDate = Current_SettleDate;%% Overall_SettleDate ; %% unique(Current_ProductName_Data.transaction_date);
            
            for c = 1 : size(Unique_TransactionDate,1)
                Current_TransactionDate =  Unique_TransactionDate(c);
                
                Date_Index = datenum(Current_ProductName_Data.transaction_date,'yyyy-mm-dd') <= datenum(Current_TransactionDate,'yyyy-mm-dd');
                
                Current_TransactionDate_Data = Current_ProductName_Data(Date_Index,:);
                
                if(isempty(Current_TransactionDate_Data))
                    continue ;
                end
                
                %% Consolidate
                Current_Required_Data = dataset2cell(Current_TransactionDate_Data);
                Current_Required_Data(1,:) = [] ;
                
                [Consolidate_Header,Consolidate_DBData] = consolidatedata(ColNames, Current_Required_Data,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
                
                %% Zero Index Removal
                
                Zero_Index = cell2mat(Consolidate_DBData(:,ActiveLots_Col)) ==  0;
                Consolidate_DBData(Zero_Index,:) = [];
                
                if(isempty(Consolidate_DBData))
                    continue;
                end
                
                try
                    Month_YearNum = cellfun(@(s) (str2double(s(end))), Consolidate_DBData(:,4),'UniformOutput',0);
                    Find_Max_Length = find(cellfun(@(s) (length(s)>5), Consolidate_DBData(:,4))) ;
                    if(~isempty(Find_Max_Length))
                        Month_YearNum(Find_Max_Length) = cellfun(@(s) (str2double(s(end-1:end))), Consolidate_DBData(Find_Max_Length,4),'UniformOutput',0);
                    end
                    %                     Month_YearNum = cellstrNum_2_double(Month_YearNum) ; %% If Str Convert Double For Sorting purpose
                    
                    Consolidate_DBData = [Consolidate_DBData,Month_YearNum];
                    Consolidate_DBData = sortrows(Consolidate_DBData,4);
                    Consolidate_DBData = sortrows(Consolidate_DBData,6);
                    Consolidate_DBData(:,6) = [];
                catch
                    Consolidate_DBData = sortrows(Consolidate_DBData,4);
                end
                
                %%
                Temp_COB_Array = cell(size(Consolidate_DBData,1),1);
                Temp_COB_Array(:,:) =  Current_TransactionDate;
                
                Temp_Position_Array = cell(size(Consolidate_DBData,1),4);
                
                Temp_Position_Array(end,1) = num2cell( abs(cell2mat(Consolidate_DBData(1,5))) ); %% First Month Position
                Temp_Position_Array(end,2) = num2cell (max(abs(cell2mat(Consolidate_DBData(:,5)))) ); %% ALL Month Position
                Temp_Position_Array(end,3) = num2cell ( abs(nansum(cell2mat(Consolidate_DBData(:,5)))) ); %% Net Position
                Temp_Position_Array(end,4) = num2cell( nansum(abs(cell2mat(Consolidate_DBData(:,5)))) ) ; %% Gross Position
                
                Output_Array = [Output_Array ; [Temp_COB_Array,Consolidate_DBData,Temp_Position_Array] ];
                
                clear Temp_COB_Array  Temp_Position_Array;
                
            end
            %% No Need Past Years Data
            if(isempty(Output_Array))
                continue;
            end
            
            Output_Array_YearNumber = datenum(Output_Array(:,1),'yyyy-mm-dd');
            
            PastYear_Index = Output_Array_YearNumber < Year_StartDate_Number;
            Output_Array(PastYear_Index,:) = [];
            
            %%
            
            Overall_Uploading_Data = [Overall_Uploading_Data ; Output_Array];
            
            %% Summary Sheet
            
            %  Summary_Header = {'Criteria','First Month Position','All month position','Net position','Gross Position'};
            %  Summary_RowIndex = {'Max','Min','Average','Median','5th Percentile','95th Percentile'};
            
            Position_On_Date = [{'Position_On_Date'},Output_Array(end,7:end)];
            
            Summary_OutputArray = cell(size(Summary_RowIndex,2),size(Summary_Header,2));
            
            for k = 1 : size(Summary_RowIndex,2)
                Current_RowIndex = Summary_RowIndex(k);
                if(strcmpi(Current_RowIndex,'max'))
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(max(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                    Summary_OutputArray(k,3) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                    Summary_OutputArray(k,4) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                    Summary_OutputArray(k,5) =  num2cell(max(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
                elseif(strcmpi(Current_RowIndex,'min'))
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(min(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                    Summary_OutputArray(k,3) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                    Summary_OutputArray(k,4) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                    Summary_OutputArray(k,5) =  num2cell(min(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
                elseif(strcmpi(Current_RowIndex,'Average'))
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                    Summary_OutputArray(k,3) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                    Summary_OutputArray(k,4) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                    Summary_OutputArray(k,5) =  num2cell(nanmean(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
                elseif(strcmpi(Current_RowIndex,'Median'))
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col))));
                    Summary_OutputArray(k,3) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col))));
                    Summary_OutputArray(k,4) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,Net_Position_Col))));
                    Summary_OutputArray(k,5) =  num2cell(nanmedian(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col))));
                elseif(strcmpi(Current_RowIndex,'5th Percentile'))
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col)),5));
                    Summary_OutputArray(k,3) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col)),5));
                    Summary_OutputArray(k,4) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Net_Position_Col)),5));
                    Summary_OutputArray(k,5) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col)),5));
                else
                    Summary_OutputArray(k,1) = Current_RowIndex;
                    Summary_OutputArray(k,2) = num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,First_MonthPosition_Col)),95));
                    Summary_OutputArray(k,3) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,All_MonthPosition_Col)),95));
                    Summary_OutputArray(k,4) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Net_Position_Col)),95));
                    Summary_OutputArray(k,5) =  num2cell(calculate_xl_percentile(cell2mat(Overall_Uploading_Data(:,Gross_Position_Col)),95));
                    
                end
                
            end
            
            Summary_OutputArray = [Position_On_Date ; Summary_OutputArray] ; %% Including Position On Date
            
            Current_Product_Summary = cell(size(Summary_OutputArray,1),1);
            Current_Product_Summary(:,:) = {[char(Current_ProductName),' Summary']};
            
            Summary_OutputArray = [ Current_Product_Summary, Summary_OutputArray ];
            
            % Need Product Wise Summary
            % xlswrite(OutXLSfileName,[Overall_Summary_Header;Summary_OutputArray],[char(Current_ProductName),' Summary']); %% Summary_Header
            
            
            Output_Array = []; %% Looping Instance
            
            %             fprintf('%d : %d\n',i,length(Overall_ValueDate));
            cprintf('key','%s : Date : %d : %d finished\n',char(Current_ProductName),i,length(Overall_ValueDate));
            
        end
        
        %% Excel Write
        
        if(~isempty(Summary_OutputArray))
            Overall_Summary_OutputArray = [Overall_Summary_OutputArray ; Summary_OutputArray] ;
        end
        
        if(~isempty(Overall_Uploading_Data))
            xlswrite(OutXLSfileName,[Output_Header;Overall_Uploading_Data],char(Current_ProductName));
            All_Product_Overall_Uploading_Data = [All_Product_Overall_Uploading_Data ; Overall_Uploading_Data];
            Overall_Uploading_Data = []; %% Looping Instance
        end
        
        %         fprintf('%s : %d : %d finished\n',char(Current_ProductName),ii,length(Unique_ProductName));
        cprintf('key','%s : %d : %d finished\n',char(Current_ProductName),ii,length(Unique_ProductName));
        
    end
    
    %% Excel Sheet Delete
    
    %     xlswrite(OutXLSfileName,[Output_Header ; Overall_Uploading_Data],'Overall Uploading Data'); %% If Need to Upload into Database
    
    xlswrite(OutXLSfileName,[Overall_Summary_Header ; Overall_Summary_OutputArray],'Overall Summary');
    %     xlswrite(OutXLSfileName,[Output_Header;All_Product_Overall_Uploading_Data],'All_Products_Position');
    
    BU_Array = cell(size(All_Product_Overall_Uploading_Data,1),1);
    BU_Array(:,:) = {upper(InBUName)};
    xlswrite(OutXLSfileName,[[Output_Header,{'BU'}] ;[All_Product_Overall_Uploading_Data,BU_Array]],'All_Products_Position');%% VBA Purpose
    
    try
        xls_delete_sheets(fullfile(pwd,OutXLSfileName));
    catch
    end
    
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end
