function [OutputFields,ConsDeals] = compute_monthend_mtm_values(InBUName,InData,InType)

% InType = 0, exisitng data at portfolio level
% InType = 1, data at contract month level

NumRows = length(InData.subportfolio);

OpeningBalanceUSD = zeros(NumRows,1);
OpeningBalanceNC = zeros(NumRows,1);
RealisedPremiumPaidUSD = zeros(NumRows,1);
RealisedPremiumPaidNC  = zeros(NumRows,1);
Brokerage = zeros(NumRows,1);

Realised_mtm_usd  = zeros(NumRows,1);
Unrealised_mtm_usd = zeros(NumRows,1);
Realised_mtm_nc = zeros(NumRows,1);
Unrealised_mtm_nc = zeros(NumRows,1);

Total_mtm_usd = zeros(NumRows,1);
Total_mtm_nc = zeros(NumRows,1);

DB_realised_cumulative_mtm_nc  = zeros(NumRows,1);
DB_unrealised_cumulative_mtm_nc = zeros(NumRows,1);
DB_realised_cumulative_mtm_usd = zeros(NumRows,1);
DB_unrealised_cumulative_mtm_usd = zeros(NumRows,1);

objDB = connect_to_database;

IdxDBDead = strcmpi('dead',InData.deal_status);
IdxDBLive = strcmpi('live',InData.deal_status);

DB_realised_cumulative_mtm_nc(IdxDBDead)   = InData.cumulative_mtm_nc(IdxDBDead);
DB_unrealised_cumulative_mtm_nc(IdxDBLive) = InData.cumulative_mtm_nc(IdxDBLive);
DB_realised_cumulative_mtm_usd(IdxDBDead) = InData.cumulative_mtm_usd(IdxDBDead);
DB_unrealised_cumulative_mtm_usd(IdxDBLive) = InData.cumulative_mtm_usd(IdxDBLive);

Group = InData.group_type;
AccGroup = InData.acc_group;
AssetClass = InData.asset_class;

% IdxORSwap = strncmpi(InData.product_code,'OR',2);
% InData.product_code(IdxORSwap) = {'OR'};

IdxLondonProd = strcmpi('QC',InData.product_code) | strcmpi('DF',InData.product_code);
IdxOptions = strcmpi('options',AccGroup);
IdxFutures = ~IdxOptions | IdxLondonProd;

TradeDates = datenum(InData.transaction_date,'yyyy-mm-dd');
% ExpDate = datenum('2016-01-01','yyyy-mm-dd');
SqlQuery = 'SELECT start_date FROM valuation_financial_year_view';
StartDate = char(fetch(objDB,SqlQuery));
ExpDate = datenum(StartDate,'yyyy-mm-dd');
IdxDateCheck = TradeDates >= ExpDate;

IdxDeadOpt = strcmpi('options',AccGroup) & IdxDateCheck;
IdxLiveOpt = strcmpi('options',AccGroup) & strcmpi('live',InData.deal_status);
IdxAllOpt = strcmpi('options',AccGroup);

IdxOID = zeros(NumRows,1); IdxOID = logical(IdxOID);
IdxOID(cellStrfind(InData.subportfolio,'OID')) = 1;

IdxRMS = strcmpi('options',AccGroup) & strncmpi('RMS-',InData.subportfolio,length('RMS-')) & ~IdxOID;

% if strcmpi(InBUName,'cfs') %% as this is_premium_paid is applicable only for RMS BU
%     IdxLondonProdPremPaid = IdxLondonProd & strcmpi('options',AccGroup) & (InData.is_premium_paid == 1);
% else
    IdxLondonProdPremPaid = IdxLondonProd & strcmpi('options',AccGroup);
% end

IdxFXD = strcmpi('FXD',Group);
IdxFXFUT = strcmpi('FXFUT',Group);

% realised option value = premium_paid of the dead options
% unrealised option value = mkt value of the live options
Realised_mtm_usd(IdxDeadOpt) = InData.premium_paid_usd(IdxDeadOpt);
Unrealised_mtm_usd(IdxLiveOpt) = InData.mkt_value_usd(IdxLiveOpt);
Realised_mtm_nc(IdxDeadOpt) = InData.premium_paid(IdxDeadOpt);
Unrealised_mtm_nc(IdxLiveOpt) = InData.mkt_value(IdxLiveOpt);

Realised_mtm_usd(IdxRMS) = InData.realised_pnl_usd(IdxRMS);
Unrealised_mtm_usd(IdxRMS) = InData.unrealised_pnl_usd(IdxRMS);
Realised_mtm_nc(IdxRMS) = InData.realised_pnl(IdxRMS) ;
Unrealised_mtm_nc(IdxRMS) = InData.unrealised_pnl(IdxRMS) ;

% realised/unrealised values for futures = cumulative_mtm calculation
Realised_mtm_usd(IdxFutures) = DB_realised_cumulative_mtm_usd(IdxFutures);
Unrealised_mtm_usd(IdxFutures) = DB_unrealised_cumulative_mtm_usd(IdxFutures);
Realised_mtm_nc(IdxFutures) = DB_realised_cumulative_mtm_nc(IdxFutures);
Unrealised_mtm_nc(IdxFutures) =DB_unrealised_cumulative_mtm_nc(IdxFutures);

Realised_mtm_usd(IdxLondonProdPremPaid) = InData.realised_pnl_usd(IdxLondonProdPremPaid);
Unrealised_mtm_usd(IdxLondonProdPremPaid) = InData.unrealised_pnl_usd(IdxLondonProdPremPaid);
Realised_mtm_nc(IdxLondonProdPremPaid) = InData.realised_pnl(IdxLondonProdPremPaid);
Unrealised_mtm_nc(IdxLondonProdPremPaid) =InData.unrealised_pnl(IdxLondonProdPremPaid);

Delta = InData.settle_delta_1 + InData.settle_delta_2;
Gamma = InData.settle_gamma_11 + InData.settle_gamma_12 + InData.settle_gamma_21 + InData.settle_gamma_22;
Vega  = InData.settle_vega_1 + InData.settle_vega_2;
Theta = InData.settle_theta .* (7/5);
ActiveLots = InData.active_lots;
SettlePrice = InData.settle_price;

SqlQuery = 'SELECT financial_year FROM valuation_financial_year_view';
FinYear = char(fetch(objDB,SqlQuery));

if InType == 1 % compute at contract_month level
    TableName = ['subportfolio_fy_monthwise_opening_balance_table_',InBUName];
    SqlQuery = ['select asset_class,subportfolio,product_code,counterparty,option_acc_group,option_group_type,contract_month,opening_balance_USD,opening_balance_NC,realised_premium_paid_usd,realised_premium_paid_nc from ',TableName,' where financial_year = ''',FinYear,''''];
    DBSubportfolioOpenBalData = fetch(objDB,SqlQuery);
    if ~isempty(DBSubportfolioOpenBalData)
        DBAssetClass = DBSubportfolioOpenBalData(:,1);
        DBSubportfolio = DBSubportfolioOpenBalData(:,2);
        DBProductCode = DBSubportfolioOpenBalData(:,3);
        DBCounterparty = DBSubportfolioOpenBalData(:,4);
        DBOptionGroup = DBSubportfolioOpenBalData(:,5);
        DBOptGroupType = DBSubportfolioOpenBalData(:,6);
        DBContractMonth = DBSubportfolioOpenBalData(:,7);
        DBOpenBalanceUSD = cell2mat(DBSubportfolioOpenBalData(:,8));
        DBOpenBalanceNC = cell2mat(DBSubportfolioOpenBalData(:,9));
        DBRealisedPremiumPaidUSD = cell2mat(DBSubportfolioOpenBalData(:,10));
        DBRealisedPremiumPaidNC  = cell2mat(DBSubportfolioOpenBalData(:,11));
        
        for iBal = 1:length(DBSubportfolio)
            IdxBal = strcmpi(DBAssetClass(iBal),AssetClass) & ...
                strcmpi(DBSubportfolio(iBal),InData.subportfolio) & ...
                strcmpi(DBProductCode(iBal),InData.product_code) & ...
                strcmpi(DBCounterparty(iBal),InData.counterparty_parent) & ...
                strcmpi(DBOptionGroup(iBal),AccGroup) & ...
                strcmpi(DBOptGroupType(iBal),Group) & ...
                strcmpi(DBContractMonth(iBal),InData.contract_month) ;
            OpeningBalanceUSD(IdxBal) = DBOpenBalanceUSD(iBal);
            OpeningBalanceNC(IdxBal) = DBOpenBalanceNC(iBal);
            RealisedPremiumPaidUSD(IdxBal) = DBRealisedPremiumPaidUSD(iBal);
            RealisedPremiumPaidNC(IdxBal)  = DBRealisedPremiumPaidNC(iBal);
        end
    end    
    
    if ~strcmpi(InBUName,'cfs')
        Realised_mtm_nc(IdxFXD)   = DB_realised_cumulative_mtm_usd(IdxFXD);
        Unrealised_mtm_nc(IdxFXD) = DB_unrealised_cumulative_mtm_usd(IdxFXD);
        OpeningBalanceNC(IdxFXD)  = OpeningBalanceUSD(IdxFXD);
        InData.currency(IdxFXD)   = {'USD'};
        InData.currency(IdxFXFUT)   = {'USD'};
    end
    
    OutputHeader = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT','PRODUCT_CODE','PORTFOLIO','CONTRACT_MONTH',...
        'UNDERLYING','CURRENCY','Opening_Balance_USD','Realized_USD','M2M_USD',...
        'Total_USD','BROKERAGE_USD','Net_Total_USD','Opening_Balance_NC',...
        'Realized_NC','M2M_NC','Total_NC','BROKERAGE_NC','Net_Total_NC',...
        'RealisedPremiumPaidUSD','RealisedPremiumPaidNC',...
        'Gamma','Theta','Vega','Delta','ActiveLots','SettlePrice','future_price'...
       'mult_factor','nc_factor','fx_nc_factor','gamma_lots'};
    OutData = [AssetClass,AccGroup,Group,upper(InData.counterparty_parent),InData.product_name,...
        InData.product_code,InData.subportfolio,InData.contract_month,...
        InData.instrument,upper(InData.currency),num2cell(OpeningBalanceUSD),num2cell(Realised_mtm_usd),num2cell(Unrealised_mtm_usd),...
        num2cell(Total_mtm_usd),num2cell(Brokerage),num2cell(Total_mtm_usd),num2cell(OpeningBalanceNC),...
        num2cell(Realised_mtm_nc),num2cell(Unrealised_mtm_nc),num2cell(Total_mtm_nc),num2cell(Brokerage),num2cell(Total_mtm_nc),...
        num2cell(RealisedPremiumPaidUSD),num2cell(RealisedPremiumPaidNC),...
        num2cell(Gamma),num2cell(Theta),num2cell(Vega),num2cell(Delta),num2cell(ActiveLots),num2cell(SettlePrice),num2cell(InData.future_price),...
        num2cell(InData.mult_factor),num2cell(InData.nc_factor),num2cell(InData.fx_nc_factor),num2cell(InData.gamma_lots)];
    
    % Do the netting
    UniqueFields = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT_CODE','PORTFOLIO','CONTRACT_MONTH'};
    SumFields={'Realized_USD','M2M_USD','Total_USD','Net_Total_USD','Realized_NC','M2M_NC','Total_NC','Net_Total_NC','Gamma','Theta','Vega','Delta','ActiveLots','gamma_lots'};
    OutputFields = OutputHeader;
    WeightedAverageFields = [];
    [OutputFields,ConsDeals] = consolidatedata(OutputHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    ConsDeals = cell2dataset([OutputFields;ConsDeals]);  
else % compute at subportfolio level
    TableName = ['subportfolio_fy_opening_balance_table_',InBUName];
    SqlQuery = ['select asset_class,subportfolio,product_code,counterparty,option_acc_group,option_group_type,opening_balance_USD,opening_balance_NC,realised_premium_paid_usd,realised_premium_paid_nc from ',TableName,' where financial_year = ''',FinYear,''''];
    DBSubportfolioOpenBalData = fetch(objDB,SqlQuery);
    if ~isempty(DBSubportfolioOpenBalData)
        DBAssetClass = DBSubportfolioOpenBalData(:,1);
        DBSubportfolio = DBSubportfolioOpenBalData(:,2);
        DBProductCode = DBSubportfolioOpenBalData(:,3);
        DBCounterparty = DBSubportfolioOpenBalData(:,4);
        DBOptionGroup = DBSubportfolioOpenBalData(:,5);
        DBOptGroupType = DBSubportfolioOpenBalData(:,6);
        DBOpenBalanceUSD = cell2mat(DBSubportfolioOpenBalData(:,7));
        DBOpenBalanceNC = cell2mat(DBSubportfolioOpenBalData(:,8));
        DBRealisedPremiumPaidUSD = cell2mat(DBSubportfolioOpenBalData(:,9));
        DBRealisedPremiumPaidNC  = cell2mat(DBSubportfolioOpenBalData(:,10));
        
        for iBal = 1:length(DBSubportfolio)
            IdxBal = strcmpi(DBAssetClass(iBal),AssetClass) & ...
                strcmpi(DBSubportfolio(iBal),InData.subportfolio) & ...
                strcmpi(DBProductCode(iBal),InData.product_code) & ...
                strcmpi(DBCounterparty(iBal),InData.counterparty_parent) & ...
                strcmpi(DBOptionGroup(iBal),AccGroup) & ...
                strcmpi(DBOptGroupType(iBal),Group);
            OpeningBalanceUSD(IdxBal) = DBOpenBalanceUSD(iBal);
            OpeningBalanceNC(IdxBal) = DBOpenBalanceNC(iBal);
            RealisedPremiumPaidUSD(IdxBal) = DBRealisedPremiumPaidUSD(iBal);
            RealisedPremiumPaidNC(IdxBal)  = DBRealisedPremiumPaidNC(iBal);
        end
    end
    
    if ~strcmpi(InBUName,'cfs')
        Realised_mtm_nc(IdxFXD)   = DB_realised_cumulative_mtm_usd(IdxFXD);
        Unrealised_mtm_nc(IdxFXD) = DB_unrealised_cumulative_mtm_usd(IdxFXD);
        OpeningBalanceNC(IdxFXD)  = OpeningBalanceUSD(IdxFXD);
        InData.currency(IdxFXD)   = {'USD'};
        InData.currency(IdxFXFUT)   = {'USD'};
    end
    
    OutputHeader = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT','PRODUCT_CODE','PORTFOLIO',...
        'UNDERLYING','CURRENCY','Opening_Balance_USD','Realized_USD','M2M_USD',...
        'Total_USD','BROKERAGE_USD','Net_Total_USD','Opening_Balance_NC',...
        'Realized_NC','M2M_NC','Total_NC','BROKERAGE_NC','Net_Total_NC',...
        'RealisedPremiumPaidUSD','RealisedPremiumPaidNC','Gamma','Theta','Vega','Delta','ActiveLots'};
    OutData = [AssetClass,AccGroup,Group,upper(InData.counterparty_parent),InData.product_name,InData.product_code,InData.subportfolio,...
        InData.instrument,upper(InData.currency),num2cell(OpeningBalanceUSD),num2cell(Realised_mtm_usd),num2cell(Unrealised_mtm_usd),...
        num2cell(Total_mtm_usd),num2cell(Brokerage),num2cell(Total_mtm_usd),num2cell(OpeningBalanceNC),...
        num2cell(Realised_mtm_nc),num2cell(Unrealised_mtm_nc),num2cell(Total_mtm_nc),num2cell(Brokerage),num2cell(Total_mtm_nc),...
        num2cell(RealisedPremiumPaidUSD),num2cell(RealisedPremiumPaidNC),...
        num2cell(Gamma),num2cell(Theta),num2cell(Vega),num2cell(Delta),num2cell(ActiveLots)];
    
    % Do the netting
    UniqueFields = {'ASSET_CLASS','ACC_GRP','GRP','COUNTERPARTY','PRODUCT_CODE','PORTFOLIO'};
    SumFields={'Realized_USD','M2M_USD','Total_USD','Net_Total_USD','Realized_NC','M2M_NC','Total_NC','Net_Total_NC','Gamma','Theta','Vega','Delta','ActiveLots'};
    OutputFields = OutputHeader;
    WeightedAverageFields = [];
    [OutputFields,ConsDeals] = consolidatedata(OutputHeader, OutData,UniqueFields,SumFields,OutputFields,WeightedAverageFields);
    ConsDeals = cell2dataset([OutputFields;ConsDeals]);    
end


ConsDeals.Realized_USD = ConsDeals.Realized_USD - ConsDeals.RealisedPremiumPaidUSD;
ConsDeals.Realized_NC  = ConsDeals.Realized_NC - ConsDeals.RealisedPremiumPaidNC;

Total_mtm_usd = ConsDeals.Opening_Balance_USD +  ConsDeals.Realized_USD + ConsDeals.M2M_USD ;
Total_mtm_nc = ConsDeals.Opening_Balance_NC +  ConsDeals.Realized_NC +  ConsDeals.M2M_NC ;

ConsDeals.Total_USD = num2cell(Total_mtm_usd);
ConsDeals.Net_Total_USD = num2cell(Total_mtm_usd);

ConsDeals.Total_NC = num2cell(Total_mtm_nc);
ConsDeals.Net_Total_NC = num2cell(Total_mtm_nc);

ConsDeals.RealisedPremiumPaidUSD = [];
ConsDeals.RealisedPremiumPaidNC = [];

TempOutData = dataset2cell(ConsDeals);

OutputFields = TempOutData(1,:);
ConsDeals = TempOutData(2:end,:);