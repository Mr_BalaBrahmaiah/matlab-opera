function [ output_filleddate ,OutErrorMsg] = fillexpiry_date( RawData_Header ,RawData,Commulative_vol_id_dataset)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% fill the expirydate if not given in excel

% tid_pos=find(strcmpi(RawData_Header,'Tid'));

% inputs

% 1 . data headers
% 2. data
% 3. vol_id table

% output

% output_filleddate : if the date is empty fof the options read the date
% from volid and update to data

% OutErrorMsg : if the columns not shown it give erroe else noerror

try
    put_callpos = strcmpi(RawData_Header,'put/call/fut');
    prod_pos=find(strcmpi(RawData_Header,'product'));
    month_pos=find(strcmpi(RawData_Header,'month'));
    expiry_pos =find(strcmpi(RawData_Header,'Expiry'));
    
    
    if ~isempty(put_callpos) && ~isempty(prod_pos)&& ~isempty(prod_pos)&& ~isempty(prod_pos)
        
        optinon_data=RawData(:,put_callpos);
        % getting index of option type data and get the date for that option from
        % vol_id
        option_pos=find(strcmpi(optinon_data,'put') | strcmpi(optinon_data,'call') | strcmpi(optinon_data,'puo') | strcmpi(optinon_data,'cuo')...
            |strcmpi(optinon_data,'pdo') | strcmpi(optinon_data,'cdo') | strcmpi(optinon_data,'cdi') | strcmpi(optinon_data,'cui')...
            |strcmpi(optinon_data,'epui') | strcmpi(optinon_data,'epuo') | strcmpi(optinon_data,'epdi') | strcmpi(optinon_data,'epdo')...
            |strcmpi(optinon_data,'ecdi') | strcmpi(optinon_data,'ecdo') | strcmpi(optinon_data,'puo') | strcmpi(optinon_data,'cuo')...
            |strcmpi(optinon_data,'pdi') |strcmpi(optinon_data,'pui'));
        
        
        
        for k=1:length(option_pos)
            
            expiry_date=RawData{option_pos(k),expiry_pos};
            tf =isnan(expiry_date);
            if find(tf==1)
                produt_code=RawData{option_pos(k),prod_pos};
                index_power_one=strfind(produt_code,'^');
                if (~isempty(index_power_one))
                    produt_code = strrep(produt_code,'^',{' '});
                    %                 produt_code(index_power_one)=[];
                end
                month_code=RawData(option_pos(k), month_pos);
                % find the first half of the data and check the length of data
                % if data kength is >2 take first and last characters
                [first ,~] =strtok(month_code ,'.');
                pro_code=char(first);
                if size(pro_code,2)>2
                    first=strcat(pro_code(1) ,pro_code(3));
                end
                
                bgcode=strcat(produt_code ,first);
                
                aa_required= find(contains(Commulative_vol_id_dataset.bbg_opt_ticker,bgcode));
                % find the data from vol_id table and update to RawData
                if (~isempty(aa_required))
                    required_expiry_date=max(datenum(Commulative_vol_id_dataset.expiry_date(aa_required)));
                    required_format_expiry_date=datestr(required_expiry_date,'dd-mm-yyyy');
                    RawData{option_pos(k),expiry_pos}=required_format_expiry_date;
                end
            else
                continue ;
            end
        end
        
        output_filleddate =RawData;
        OutErrorMsg ={'No errors'};
    else
        OutErrorMsg ={'The following columns not found in the excel  put/call/fut / product /month / expiry'};
        output_filleddate =RawData;
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
     ME.stack(1).name, ME.stack(1).line, ME.message);
end
end

