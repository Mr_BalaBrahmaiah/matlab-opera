function [OutErrorMsg,OutDealWiseVaR,OutProductVaR,ProductWisePnL,TotalVaR] = generate_dynamic_var_report(TradeGroupType,ProductCode,Identifiers,CallPutID,Strike,Lots,VaRDays)

OutErrorMsg = {'No errors'};
OutDealWiseVaR = [];
OutProductVaR  = [];
ProductWisePnL = [];
TotalVaR = 0;

InBUName = 'agf';

% try
    
    %%  Securiy ID
    %     ProductCode = strtrim(ProductCode);
    
    FUT_Index = cellStrfind_exact(TradeGroupType , {'FUT'});
    OPT_Index = cellStrfind_exact(TradeGroupType , {'OPT'});
    
    IdxEmptyValue = cellfun(@isempty,ProductCode);
    if any(IdxEmptyValue)
        OutErrorMsg = {'Product Code cannot be empty!'};
        errordlg(OutErrorMsg);
        return;
    end
    
    IdxEmptyValue = cellfun(@isempty,Identifiers) | strcmpi(Identifiers,'--SELECT--') ;
    if any(IdxEmptyValue)
        OutErrorMsg = {'Identifiers cannot be empty!'};
        errordlg(OutErrorMsg);
        return;
    end
    
    IdxEmptyValue = cellfun(@isempty,Lots);
    if any(IdxEmptyValue)
        OutErrorMsg = {'Lots cannot be empty!'};
        errordlg(OutErrorMsg);
        return;
    end
    
    IdxEmptyValue = cellfun(@isempty,CallPutID(OPT_Index));
    if any(IdxEmptyValue)
        OutErrorMsg = {'Call Put ID cannot be empty for OPTIONS!'};
        errordlg(OutErrorMsg);
        return;
    end
    
    IdxEmptyValue = cellfun(@isempty,Strike(OPT_Index));
    if any(IdxEmptyValue)
        OutErrorMsg = {'Strike cannot be empty for OPTIONS!'};
        errordlg(OutErrorMsg);
        return;
    end
    
    SecurityId = cell(size(ProductCode)); %% Initialize length
    UnderlyingID = cell(size(ProductCode)); %% Initialize length
    VolID = cell(size(ProductCode)); %% Initialize length
    
    % construct the security and underlying ids for both futures and
    % options
    SecurityId(FUT_Index)   = Identifiers(FUT_Index);
    UnderlyingID(FUT_Index) = Identifiers(FUT_Index);
    SecurityId(OPT_Index)  = strcat(Identifiers(OPT_Index),CallPutID(OPT_Index),{' '}, strrep(cellstr(num2str(cell2mat(Strike(OPT_Index)))),' ',''));
%     UnderlyingID(OPT_Index) = strtok(Identifiers(OPT_Index),'.');
    OPT_Identifiers = Identifiers(OPT_Index);
    UnderlyingID(OPT_Index) = cellfun(@(s) [s(1:end-5),s(end-1:end)], OPT_Identifiers,'UniformOutput',0);
    VolID(OPT_Index)= Identifiers(OPT_Index);
    
    % product code to be trimmed as it contains the empty characters in the
    % end, which is needed only while constructing the security and
    % underlying ids
    ProductCode = strtrim(ProductCode);
    NumRows = size(ProductCode,1);
    
    OutHeader = {'TradeGroupType','ProductCode','Identifiers','CallPutID','Strike','Lots','UnderlyingID','SecurityId'};
    OutDealWiseVaR = [TradeGroupType,ProductCode,Identifiers,CallPutID,Strike,Lots,UnderlyingID,SecurityId] ;
    OutDealWiseVaR = cell2table(OutDealWiseVaR);
    OutDealWiseVaR.Properties.VariableNames = OutHeader;
    
    ObjDB = connect_to_database;
    
    ValueDate = char(fetch(ObjDB,'select value_date from valuation_date_table'));
    SettleDate = char(fetch(ObjDB,'select settlement_date from valuation_date_table'));
    
    % get all mult factor related data
    SqlQuery = ['select * from helper_dynamic_var_mult_factor_view where value_date = ''',ValueDate,''''];
    [ColNames,MultFactorData] = Fetch_DB_Data(ObjDB,SqlQuery);
    MultFactorData = cell2table(MultFactorData);
    MultFactorData.Properties.VariableNames = ColNames;
    
    LotMult  =  zeros(size(ProductCode));
    CurrMult =  zeros(size(ProductCode));
    MultFactor =  zeros(size(ProductCode));
    NCFactor = zeros(size(ProductCode));
    ProdCode = unique(strtrim(ProductCode));
    
    for iCode = 1:length(ProdCode)
        IdxProdCode = strcmpi(ProdCode(iCode),ProductCode);
        IdxMultFactorCode = strcmpi(ProdCode(iCode),MultFactorData.invenio_product_code);
        
        LotMult(IdxProdCode) = MultFactorData.lot_mult(IdxMultFactorCode);
        CurrMult(IdxProdCode) = MultFactorData.curr_mult_nc_factor(IdxMultFactorCode);
        MultFactor(IdxProdCode) =  MultFactorData.mult_factor(IdxMultFactorCode);
        
    end
    
    Delta = zeros(size(SecurityId));
    Gamma = zeros(size(SecurityId));
    Theta = zeros(size(SecurityId));
    Vega  = zeros(size(SecurityId));
    FuturePrice = zeros(size(SecurityId));
    SettlePrice = zeros(size(SecurityId));
    
    % iterate over the securities that were already priced for this BU
    for iSec = 1:length(SecurityId)
        % Evaluate futures prices and delta
        
        SqlQuery = ['select settle_value from underlying_settle_value_table where value_date = ''',ValueDate,''' and  underlying_id = ''',UnderlyingID{iSec},''''];
        try
            FuturePrice(iSec) = cell2mat(fetch(ObjDB,SqlQuery));
        catch
            if isempty(FuturePrice(iSec))
                OutErrorMsg = {'Price for UnderlyingId  ',UnderlyingID{iSec},' does not exist in table!'};
                errordlg(OutErrorMsg);
                return;
            end
        end
        
        if strcmpi(TradeGroupType{iSec},'FUT')
            SettlePrice(iSec) = FuturePrice(iSec);
            Delta(iSec) = 1 .* cell2mat(Lots(iSec));
        end
        
        % consider the data from the pricing table if it exists already for
        % OPTIONS
        if strcmpi(TradeGroupType{iSec},'OPT')
            % get the data from existing pricing table of given BU
            Tablename = ['security_settlement_value_table_',InBUName];
            
            SqlQuery = ['select * from ',Tablename,' where value_date = ''',ValueDate,''' and security_id = ''',SecurityId{iSec},''''];
            [PricingHeader,PricingData] = Fetch_DB_Data(ObjDB,SqlQuery);
            if strcmpi(PricingData,'No Data')
                [OutErrorMsg,InterpVol] = interpolate_voldata(ObjDB,ValueDate,VolID(iSec),CallPutID{iSec},cell2mat(Strike(iSec)));
                
                if strcmpi(OutErrorMsg,'No errors')
                    SqlQuery = ['select expiry_date from vol_id_table where vol_id = ''',VolID{iSec},''''];
                    Maturity = char(fetch(ObjDB,SqlQuery));
                    TTM               = ((datenum(Maturity,'yyyy-mm-dd') - datenum(ValueDate,'yyyy-mm-dd')) +1 )/365;
                    if strcmpi(CallPutID{iSec},'C')
                        OptionTypes = 'vanilla_call';
                    elseif strcmpi(CallPutID{iSec},'P')
                        OptionTypes = 'vanilla_put';
                    end
                    RiskFreeRate = 0; CostOfCarry =0;
                    Currency = {'USD'};
                    YearToDay_Mult = 365; IsExpiryProcessed =0;
                    
                    obj = VanillaOptionBlackScholesModel( FuturePrice(iSec), cell2mat(Strike(iSec)), RiskFreeRate, CostOfCarry,...
                        InterpVol, TTM, OptionTypes, 1, 1, 1,...
                        IsExpiryProcessed,YearToDay_Mult,Currency);
                    
                    SettlePrice(iSec) = obj.getprice;
                    Delta(iSec)       = obj.getdelta1 .* cell2mat(Lots(iSec));
                    Gamma(iSec)       = (obj.getgamma11 .* cell2mat(Lots(iSec)) .* LotMult(iSec)) ./ CurrMult(iSec);
                    Vega(iSec)        = obj.getvega1 .* cell2mat(Lots(iSec)) .* MultFactor(iSec);
                    Theta(iSec)       = obj.gettheta .* cell2mat(Lots(iSec)) .* MultFactor(iSec);
                else
                    errordlg(OutErrorMsg);
                    return;
                end
            else
                PricingData = cell2table(PricingData);
                PricingData.Properties.VariableNames = PricingHeader;
                
                SettlePrice(iSec) = PricingData.settle_price;
                Delta(iSec)       = PricingData.settle_delta_1 .* cell2mat(Lots(iSec));
                Gamma(iSec)       = (PricingData.settle_gamma_11 .* cell2mat(Lots(iSec)) .* LotMult(iSec)) ./ CurrMult(iSec);
                Vega(iSec)        = PricingData.settle_vega_1 .* cell2mat(Lots(iSec)) .* MultFactor(iSec);
                Theta(iSec)       = PricingData.settle_theta .* cell2mat(Lots(iSec)) .* MultFactor(iSec);
                
            end
        end
        
    end
    
    DeltaScreen = Delta .* MultFactor;
    GammaScreen = Gamma .* CurrMult.^2 ;
    VegaScreen  = Vega;
    ThetaScreen = Theta .* (5/7);
    
    % Fetch the data for continuation series
    SqlQuery = ['select * from future_cont_series_table where settlement_date = ''',SettleDate,''''];
    [ColNames,FutureContSeriesData] = Fetch_DB_Data(ObjDB,SqlQuery);
    FutureContSeriesData = cell2table(FutureContSeriesData);
    FutureContSeriesData.Properties.VariableNames = ColNames;
    FutureMonthSeries = cell(NumRows,1);
    
    SqlQuery = ['select * from option_cont_series_table where settlement_date = ''',SettleDate,''''];
    [ColNames,OptionContSeriesData] = Fetch_DB_Data(ObjDB,SqlQuery);
    OptionContSeriesData = cell2table(OptionContSeriesData);
    OptionContSeriesData.Properties.VariableNames = ColNames;
    OptionMonthSeries = cell(NumRows,1);
    
    for iCont = 1:NumRows
        IdxFutContMonth = strcmpi(UnderlyingID(iCont),FutureContSeriesData.underlying_id);
        FutureMonthSeries(iCont) = FutureContSeriesData.cont_month(IdxFutContMonth);
        
        if strcmpi(TradeGroupType{iCont},'OPT')
            IdxOptContMonth = strcmpi(VolID(iCont),OptionContSeriesData.vol_id);
            OptionMonthSeries(iCont) = OptionContSeriesData.underlying_vol_id_cont_month(IdxOptContMonth);
        end
    end
    
    DeltaPnL = cell(NumRows,1);
    GammaPnL = cell(NumRows,1);
    VegaPnL  = cell(NumRows,1);
    ThetaPnL = cell(NumRows,1);
    TotalPnL = cell(NumRows,1);
    VaR      = cell(NumRows,1);
    
    TmpVaR     = cell(NumRows,3);
%     StrTotalPnl = cell(NumRows,3);
    TmpTotalPnL = cell(NumRows,3);
      
          
    % To calculate the individual record's pnl values and its VaR
    for iRecord = 1:NumRows
        disp(iRecord); % just to track the iterations
        for iDays = 1:length(VaRDays)
            NumDays = VaRDays(iDays);
            disp(NumDays);
            
            SqlQuery = ['select price_change from var_cont_price_change_table where cont_month = ''',FutureMonthSeries{iRecord},...
                ''' and settlement_date <= ''',SettleDate,''' order by settlement_date desc limit 0,',num2str(NumDays)];
            PriceChangeVector = fetch(ObjDB,SqlQuery);
            PriceChangeVector = cell2mat(PriceChangeVector);
            PriceChangeVector = flipud(PriceChangeVector);
            % to check if any value is missing (to prevent NaN values)
            if ~isempty(find(isnan(PriceChangeVector)))
                disp([FutureMonthSeries{iRecord},':',SettleDate]);
            end
            
            if ~isequal(length(PriceChangeVector),NumDays)
                OutErrorMsg = ['PriceChange Data points for ',FutureMonthSeries{iRecord},' is not available for ',num2str(NumDays),' days! Please check to proceed further with VaR for this Identifier'];
                errordlg(OutErrorMsg);
                return;                
            end
            
            DeltaPnL{iRecord} = (DeltaScreen(iRecord) .* PriceChangeVector)';
            GammaPnL{iRecord} = (0.5 .* GammaScreen(iRecord) .* (PriceChangeVector.^2))';
            
            IsFutures = 0;
            if ~iscell(OptionMonthSeries) && all(isnan(OptionMonthSeries)) % if no live options are found
                IsFutures = 1;
            elseif isempty(OptionMonthSeries{iRecord}) || any(isnan(OptionMonthSeries{iRecord}))
                IsFutures = 1;
            end
            
            if IsFutures  % for futures
                VegaPnL{iRecord} = zeros(1,NumDays);
            else
                SqlQuery = ['select vol_change from var_cont_vol_change_table where cont_month = ''',OptionMonthSeries{iRecord},...
                    ''' and settlement_date <= ''',SettleDate,''' order by settlement_date desc limit 0,',num2str(NumDays)];
                VolChangeVector = fetch(ObjDB,SqlQuery);
                if isempty(VolChangeVector)
                    VolChangeVector = zeros(NumDays,1);
                else
                    VolChangeVector = cell2mat(VolChangeVector);
                    VolChangeVector = flipud(VolChangeVector);
                end
                
                if ~isequal(length(VolChangeVector),NumDays)
                    OutErrorMsg = ['VolChange Data points for ',OptionMonthSeries{iRecord},' is not available for ',num2str(NumDays),' days! Please check to proceed further with VaR for this Identifier'];
                    errordlg(OutErrorMsg);
                    return;
                end
                
                VegaPnL{iRecord} = (VegaScreen(iRecord) .* VolChangeVector)';
                % to check if any value is missing (to prevent NaN values)
                if ~isempty(find(isnan(VolChangeVector)))
                    disp([OptionMonthSeries{iRecord},':',SettleDate]);
                end
            end
            
            ThetaPnL{iRecord} = repmat(ThetaScreen(iRecord),1,NumDays);
            
            TotalPnL{iRecord} = DeltaPnL{iRecord} + GammaPnL{iRecord} + VegaPnL{iRecord} + ThetaPnL{iRecord};
            TmpVaR{iRecord,iDays} = calculate_xl_percentile(TotalPnL{iRecord},5);
            TmpTotalPnL{iRecord,iDays} = TotalPnL{iRecord};
%             StrTotalPnl{iRecord,iDays} = num2str(TotalPnL{iRecord});
        end
        VaR{iRecord} = mean(cell2mat(TmpVaR(iRecord,:)));
    end
     
    % To calculate the total VaR
    [NRows,~] = size(TmpTotalPnL);
    TmpVaR     = cell(1,length(VaRDays));
    for iDays = 1:length(VaRDays)    
            if NRows == 1
                TmpVaR{1,iDays} = calculate_xl_percentile(TmpTotalPnL{:,iDays},5);  
            else
                TmpVaR{1,iDays} = calculate_xl_percentile(sum(cell2mat(TmpTotalPnL(:,iDays))),5); 
            end           
    end
    TotalVaR = mean(cell2mat(TmpVaR)); 
        
    % To calculate the product wise VaR
    UniqProdCode = unique(ProductCode);
    NumUniqProds = length(UniqProdCode);
    TmpVaR     = cell(NumUniqProds,3);
    ProdVaR    = cell(NumUniqProds,1);
    StrTotalPnl = cell(NumUniqProds,3);
    for iProd = 1:length(UniqProdCode)
        IdxProd = strcmpi(UniqProdCode{iProd},ProductCode);
        for iDays = 1:length(VaRDays)    
            if length(find(IdxProd)) == 1
                TmpVaR{iProd,iDays} = calculate_xl_percentile(TmpTotalPnL{IdxProd,iDays},5);  
                StrTotalPnl{iProd,iDays} = num2str(TmpTotalPnL{IdxProd,iDays});  
            else
                TmpVaR{iProd,iDays} = calculate_xl_percentile(sum(cell2mat(TmpTotalPnL(IdxProd,iDays))),5); 
                StrTotalPnl{iProd,iDays} = num2str(sum(cell2mat(TmpTotalPnL(IdxProd,iDays))));  
            end
        end
        ProdVaR{iProd} = mean(cell2mat(TmpVaR(iProd,:)));
    end   
    AddToSubportfolio = logical(zeros(size(ProdVaR)));
    Subportfolio = repmat({'--Select--'},NumUniqProds,1);
    OutProductVaR = [UniqProdCode,ProdVaR,num2cell(AddToSubportfolio),Subportfolio];    
    
    for iDays = 1:length(VaRDays)    
        TempData = [repmat(cellstr(ValueDate),size(ProdVaR)),UniqProdCode,Subportfolio,repmat(num2cell(VaRDays(iDays)),size(ProdVaR)),StrTotalPnl(:,iDays)];
        ProductWisePnL = [TempData;ProductWisePnL];
    end
    Header = {'value_date','product_code','subportfolio','num_days','total_pnl'};
    ProductWisePnL = cell2table(ProductWisePnL);
    ProductWisePnL.Properties.VariableNames = Header;
    
%     TempOutData = [TradeGroupType,ProductCode,Identifiers,CallPutID,Strike,Lots,...
%         UnderlyingID,SecurityId,FutureMonthSeries,OptionMonthSeries,num2cell(DeltaScreen),...
%         num2cell(GammaScreen),num2cell(VegaScreen),num2cell(ThetaScreen),...
%         VaR,num2cell(AddToSubportfolio)];   
    OutDealWiseVaR = [ProductCode,TradeGroupType,SecurityId,...
        FutureMonthSeries,OptionMonthSeries,Lots,num2cell(DeltaScreen),...
        num2cell(GammaScreen),num2cell(VegaScreen),num2cell(ThetaScreen),...
        VaR]; 
    
% catch ME
%     OutErrorMsg = cellstr(ME.message);
%     
% end