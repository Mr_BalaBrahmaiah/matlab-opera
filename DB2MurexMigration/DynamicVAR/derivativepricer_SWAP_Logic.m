
DB_Date_Format = 'yyyy-mm-dd';

Security_id = {};

AvgStartDate = {};
AvgEndDate = {};
SwapValueDate = {};

AssetPrice_1 = {};
AssetPrice_2 = {};

OptionTypes = {};
Lots = {};
QuantoType = {};

%%
objDB = connect_to_database;
DBHolidaysData = fetch(objDB,'select exchange_code,invenio_product_code,holidays from holidays_list_table');
DBExchCode = DBHolidaysData(:,1);
DBProdCode = DBHolidaysData(:,2);
DBHolidays = DBHolidaysData(:,3);
IdxSGXHolidays = strcmpi('SGX',strtrim(DBExchCode)) & strcmpi('CM OR',strtrim(DBProdCode));
HolidaysList = DBHolidays(IdxSGXHolidays);
close(objDB);

HolidayVec = datenum(HolidaysList,'yyyy-mm-dd');

%%
TND         = length(busdays(datenum(AvgStartDate,DB_Date_Format),datenum(AvgEndDate,DB_Date_Format),'daily',HolidayVec));
ElapsedDays = length(busdays(datenum(AvgStartDate,DB_Date_Format),datenum(SwapValueDate,DB_Date_Format),'daily',HolidayVec));
NDE         = max(ElapsedDays,0);    % NDE = max(value_date - Avg_SD +1 ,0);

SwapAssetPrice_1 = AssetPrice_1 ./ 100;
% SwapAssetPrice_1(isnan(SwapAssetPrice_1)) = 0;
SwapAssetPrice_2 = AssetPrice_2;
% SwapAssetPrice_2(isnan(SwapAssetPrice_2)) = 0;
SwapAvgTillNow = AvgTillNow ./ 100;

obj = AverageSwapATDCrossCurrency(SwapAssetPrice_1 ,SwapAssetPrice_2, OptionTypes , SwapAvgTillNow , TND , NDE , Lots, QuantoType );
% outputhandling(obj, IdxTmpSwap);

