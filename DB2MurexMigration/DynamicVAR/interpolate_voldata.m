function [OutErrorMsg,InterpVol] = interpolate_voldata(ObjDB,ValueDate,InVolId,InCallPutID,InStrike)

OutErrorMsg = {'No Errors'};
InterpVol = 0;

try
    % assuming that single input entry is passed
    InterpVol = 0;
    InVolId = char(InVolId);
    InCallPutID = char(InCallPutID);    
    
    % InVolIDs = convertCell2Char(InVolId);
    % InVolIDs = strrep(InVolIDs,',',''',''');
    % InVolIDs = ['''',InVolIDs,''''];
    % [ColNames,Data] = Fetch_DB_Data(ObjDB,['select * from settlement_vol_surf_table where value_date = ''',ValueDate,''' and vol_id = ''',InVolIDs,'''']);
    [ColNames,Data] = Fetch_DB_Data(ObjDB,['select * from settlement_vol_surf_table where value_date = ''',ValueDate,''' and vol_id = ''',InVolId,'''']);
    
    if strcmpi(Data,'No Data')
        OutErrorMsg = {['Settlement vols are not available for the month ''',InVolId,'''']};
        return;
    end
    DbData = cell2table(Data);
    DbData.Properties.VariableNames = ColNames;
    
    DbIndex = find(ismember(upper(DbData.vol_id),upper(InVolId)) & ismember(upper(DbData.call_put_id),upper(InCallPutID)));
    if ~isempty(DbIndex)
        DBStrike = DbData.strike(DbIndex);
        DBVol =  DbData.settle_vol(DbIndex);
        if (length(DbIndex)==1) % if there is one value available in db, then interp1 cannot be invoked for the single value
            if abs(DBStrike - InStrike) < 0.000001 % to check if both strikes are equal
                InterpVol = DBVol;
            end
        else
            OutVol = interp1(DBStrike,DBVol,InStrike,'linear',NaN);
            % if the strikes are out of range, then assign the vol
            % corresponding to nearest strike
            MinStrike = min(DBStrike);
            MaxStrike = max(DBStrike);
            MinVol = DBVol(isnumericequal(DBStrike,MinStrike));
            MaxVol = DBVol(isnumericequal(DBStrike,MaxStrike));
            OutVol(InStrike > MaxStrike) = MaxVol;
            OutVol(InStrike < MinStrike) = MinVol;
            InterpVol = OutVol;
        end
    end
catch ME
    OutErrorMsg = cellstr(ME.message);
end
end
