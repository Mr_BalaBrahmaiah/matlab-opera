function varargout = DynamicVAR_Configurator(varargin)
% DYNAMICVAR_CONFIGURATOR MATLAB code for DynamicVAR_Configurator.fig
%      DYNAMICVAR_CONFIGURATOR, by itself, creates a new DYNAMICVAR_CONFIGURATOR or raises the existing
%      singleton*.
%
%      H = DYNAMICVAR_CONFIGURATOR returns the handle to a new DYNAMICVAR_CONFIGURATOR or the handle to
%      the existing singleton*.
%
%      DYNAMICVAR_CONFIGURATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DYNAMICVAR_CONFIGURATOR.M with the given input arguments.
%
%      DYNAMICVAR_CONFIGURATOR('Property','Value',...) creates a new DYNAMICVAR_CONFIGURATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DynamicVAR_Configurator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DynamicVAR_Configurator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DynamicVAR_Configurator

% Last Modified by GUIDE v2.5 12-Mar-2018 08:54:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @DynamicVAR_Configurator_OpeningFcn, ...
    'gui_OutputFcn',  @DynamicVAR_Configurator_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DynamicVAR_Configurator is made visible.
function DynamicVAR_Configurator_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DynamicVAR_Configurator (see VARARGIN)

% Choose default command line output for DynamicVAR_Configurator
handles.output = hObject;

% construct the input table structure
InTableColumnNames = {'TradeGroupType','ProductCode','Identifiers','CallPutID','Strike','Lots'};
set(handles.tblVaRInput,'ColumnName',InTableColumnNames);

Col_fmt = {'','char','char','','numeric','numeric'};
set(handles.tblVaRInput, 'ColumnFormat', Col_fmt);

InData = cell(1,length(InTableColumnNames));
InData(:,2) = {'--Select--'};
InData(:,3) = {'--Select--'};
set(handles.tblVaRInput,'Data',InData);

Edit_LogicalArray = logical( ones(1,length(InTableColumnNames)));
set(handles.tblVaRInput,'ColumnEditable', Edit_LogicalArray);

NoCols = size(InData,2);
ColWidth = repmat({'auto'},1,NoCols);
set(handles.tblVaRInput,'ColumnWidth',ColWidth);

% construct the dealwise var table structure
OutTableColumnNames = {'ProductCode','TradeGroupType','SecurityId',...
    'FutureMonthSeries','OptionMonthSeries','Lots',...
    'DeltaScreen','GammaScreen','VegaScreen','ThetaScreen','VaR'};
set(handles.tblVaROutput,'ColumnName',OutTableColumnNames);

Col_fmt = {'char','char','char','char','char','numeric',...
    'bank','bank','bank','bank','bank'};
set(handles.tblVaROutput, 'ColumnFormat', Col_fmt);

OutData = cell(1,length(OutTableColumnNames));
set(handles.tblVaROutput,'Data',OutData);

Edit_LogicalArray = logical( zeros(1,length(OutTableColumnNames)));
% Edit_LogicalArray(end-1:end) = 1;
set(handles.tblVaROutput,'ColumnEditable', Edit_LogicalArray);

% construct the product var table
OutProdVaRColumnNames = {'ProductCode','VaR','AddToSubportfolio','Subportfolio'};
set(handles.tblProductVaR,'ColumnName',OutProdVaRColumnNames);

Col_fmt = {'char','bank','logical','char'};
set(handles.tblProductVaR, 'ColumnFormat', Col_fmt);

OutProdVaR = cell(1,length(OutProdVaRColumnNames));
OutProdVaR(:,end) = {'--Select--'};
set(handles.tblProductVaR,'Data',OutProdVaR);

Edit_LogicalArray = logical( zeros(1,length(OutProdVaRColumnNames)));
Edit_LogicalArray(:,end-1:end) = 1;
set(handles.tblProductVaR,'ColumnEditable', Edit_LogicalArray);

%construct the consolidated var table
OutConsoVaRColumnNames = {'ProductCode','Subportfolio','OldVaR','NewVaR','IncrementalVaR'};
set(handles.tblConsoVaR,'ColumnName',OutConsoVaRColumnNames);

Col_fmt = {'char','char','bank','bank','bank'};
set(handles.tblConsoVaR, 'ColumnFormat', Col_fmt);

OutConsoVaR = cell(1,length(OutConsoVaRColumnNames));
set(handles.tblConsoVaR,'Data',OutConsoVaR);

Edit_LogicalArray = logical( zeros(1,length(OutConsoVaRColumnNames)));
set(handles.tblConsoVaR,'ColumnEditable', Edit_LogicalArray);

ObjDB = connect_to_database;

InBUName = 'agf';

Tablename = ['subportfolio_product_mapping_table_',InBUName];
SqlQuery = ['select distinct(subportfolio),product_code from ',Tablename];
[~,DBPortfolioData] = Fetch_DB_Data(ObjDB,SqlQuery);

DBSubportfolios = DBPortfolioData(:,1);
DBProductCode   = DBPortfolioData(:,2);

DBSubportfolios = ['--Select--';DBSubportfolios];

TradeGroupType = {'FUT';'OPT'};
CallPutID = {' ';'C';'P'};

SqlQuery = 'select settlement_date from valuation_date_table';
[~,SettlementDate] = Fetch_DB_Data(ObjDB,SqlQuery);
SettlementDate = char(SettlementDate);

% SqlQuery = ['select underlying_id,invenio_product_code from underlying_list_table where asset_class = ''commodity'' and expiry_date > ''',SettlementDate,''''];
SqlQuery = 'select * from helper_dynamic_var_extract_futures_view';
[~,FutureData] = Fetch_DB_Data(ObjDB,SqlQuery);
FutureId        = FutureData(:,1);
FutureProdCode  = FutureData(:,2);

% SqlQuery = ['select vol_id,product_code from vol_id_table where asset_class = ''commodity'' and expiry_date > ''',SettlementDate,''''];
SqlQuery = 'select * from helper_dynamic_var_extract_options_view';
[~,OptionData] = Fetch_DB_Data(ObjDB,SqlQuery);
OptionId        = OptionData(:,1);
OptionProdCode  = OptionData(:,2);

% Identifiers = unique([strtrim(FutureMonths);strtrim(OptionMonths)]);
Identifiers = [FutureId;OptionId];
ProductCodes = unique([FutureProdCode;OptionProdCode]);

ProductCodes = ['--Select--';ProductCodes];
Identifiers = ['--Select--';Identifiers];

handles.InTableColumnNames = InTableColumnNames;
handles.InitInData = InData;

handles.OutTableColumnNames = OutTableColumnNames;
handles.InitOutData = OutData;

% store all column number data
handles.TradeType_Col     = 1;
handles.ProductCode_Col   = 2;
handles.Identifiers_Col   = 3;
handles.CallPut_Col       = 4;
handles.Strike_Col        = 5;
handles.Lots_Col          = 6;

handles.InBUName = InBUName;

Col_fmt = get(handles.tblVaRInput, 'ColumnFormat');

Col_fmt{handles.TradeType_Col} = TradeGroupType';
Col_fmt{handles.CallPut_Col} = CallPutID';

set(handles.tblVaRInput, 'ColumnFormat', Col_fmt);

set(handles.tblVaROutput,'visible','off');
set(handles.pnlVaROutput,'visible','off');

% Col_fmt = get(handles.tblProductVaR, 'ColumnFormat');
% Col_fmt{end} = Subportfolios';
% set(handles.tblProductVaR, 'ColumnFormat', Col_fmt);

set(handles.tblProductVaR,'visible','off');
set(handles.pnlProductVaR,'visible','off');

set(handles.tblConsoVaR,'visible','off');
set(handles.pnlConsoVaR,'visible','off');
set(handles.tblTotalVaR,'visible','off');

set(handles.pbAddVaR,'visible','off');
set(handles.tlbrExportToExcel,'visible','on');
set(handles.pbAddTotalVaR,'visible','off');
set(handles.btnGrpAddTotalVaR,'visible','off');

set(handles.txtTotalVaR,'visible','off');
set(handles.edtTotalVaR,'visible','off');

handles.SettlementDate = SettlementDate;

handles.ProductCode    = ProductCodes;
handles.FutureProdCode = FutureProdCode;
handles.OptionProdCode = OptionProdCode;
handles.TradeGroupType = TradeGroupType;
handles.Identifiers    = Identifiers;
handles.FutureId       = FutureId;
handles.OptionId       = OptionId;
handles.CallPutID      = CallPutID;

handles.DBSubportfolios = DBSubportfolios;
handles.DBProductCode = DBProductCode;

InBUName = 'agf';
VarAggrTableName = ['var_aggregation_level_table_',InBUName];
 
handles.ObjDB = ObjDB;
handles.VarAggrTableName = VarAggrTableName;


set(handles.pbDeleteRows,'ButtonDownFcn',@tblVaRInput_CellSelectionCallback);
set(handles.tblProductVaR,'CellSelectionCallback',@tblProductVaR_CellSelectionCallback);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DynamicVAR_Configurator wait for user response (see UIRESUME)
% uiwait(handles.fgDynamicVARConfigurator);


% --- Outputs from this function are returned to the command line.
function varargout = DynamicVAR_Configurator_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbClearData.
function pbClearData_Callback(hObject, eventdata, handles)
% hObject    handle to pbClearData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
QueryResp = questdlg('Are you sure that you want to clear data in all rows?', 'Clear All Data!', 'Yes', 'No', 'No');
if strcmpi(QueryResp,'Yes')
    set(handles.tblVaRInput,'data',handles.InitInData);
    set(handles.tblVaROutput,'data',handles.InitOutData);
    % while clearing the input data table, hide all the output tables in
    % the screen
    set(handles.tblVaROutput,'visible','off');
    set(handles.pnlVaROutput,'visible','off');
    
    set(handles.tblProductVaR,'visible','off');
    set(handles.pnlProductVaR,'visible','off');
    
    set(handles.tblConsoVaR,'visible','off');
    set(handles.pnlConsoVaR,'visible','off');
    set(handles.tblTotalVaR,'visible','off');
    
    set(handles.pbAddVaR,'visible','off');
    set(handles.tlbrExportToExcel,'visible','on');
    set(handles.pbAddTotalVaR,'visible','off');
    set(handles.btnGrpAddTotalVaR,'visible','off');
    
    set(handles.txtTotalVaR,'visible','off');
    set(handles.edtTotalVaR,'visible','off');
    
    guidata(hObject, handles);
end

% --- Executes on button press in pbCalculateVaR.
function pbCalculateVaR_Callback(hObject, eventdata, handles)
% hObject    handle to pbCalculateVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.pnlConsoVaR,'visible','off');
set(handles.tblConsoVaR,'visible','off');
set(handles.tblTotalVaR,'visible','off');

ExData = get(handles.tblVaRInput,'data');

% Fetch the Trade Type data from input
TradeType = ExData(:,handles.TradeType_Col);
% Check if there are any lines without all the needed data and remove those
% lines
IdxEmpty = cellfun(@isempty,TradeType);
ExData(IdxEmpty,:) = [];

% Fetch all the needed parameters and pass it on to calculate dynamic var
TradeType = ExData(:,handles.TradeType_Col);
ProductCode = ExData(:,handles.ProductCode_Col);
Identifiers = ExData(:,handles.Identifiers_Col);
CallPutID = ExData(:,handles.CallPut_Col);
Strike = ExData(:,handles.Strike_Col);
Lots = ExData(:,handles.Lots_Col);

VaRDays  = [100;252;500];

drawnow;  [jObj] = Job_Start(); drawnow;

[OutErrorMsg,OutDealWiseVaR,OutProductVaR,ProductWisePnL,TotalVaR] = generate_dynamic_var_report(TradeType,ProductCode,Identifiers,CallPutID,Strike,Lots,VaRDays);

drawnow; Job_Done(jObj); drawnow;

if strcmpi(OutErrorMsg,'No Errors')
    % update data in Dealwise VaR output table
    set(handles.pnlVaROutput,'visible','on');
    set(handles.tblVaROutput,'visible','on');
    
    NoCols = size(OutDealWiseVaR,2);
    set(handles.tblVaROutput,'data',OutDealWiseVaR);
    
    ColWidth = repmat({'auto'},1,NoCols);
    set(handles.tblVaROutput,'ColumnWidth',ColWidth);
    
    % update data in Product VaR output table
    set(handles.pnlProductVaR,'visible','on');
    set(handles.tblProductVaR,'visible','on');
    set(handles.btnGrpAddTotalVaR,'visible','on');

    
    NoCols = size(OutProductVaR,2);
    set(handles.tblProductVaR,'data',OutProductVaR);
    
    ColWidth = repmat({'auto'},1,NoCols);
    set(handles.tblProductVaR,'ColumnWidth',ColWidth);
    
    set(handles.pbAddVaR,'visible','on');
    set(handles.pbAddTotalVaR,'visible','on');
    
    set(handles.txtTotalVaR,'visible','on');
    set(handles.edtTotalVaR,'visible','on');
    
    set(handles.edtTotalVaR,'string',num2str(TotalVaR));
else  
    % while clearing the input data table, hide all the output tables in
    % the screen
    set(handles.tblVaROutput,'visible','off');
    set(handles.pnlVaROutput,'visible','off');
    
    set(handles.tblProductVaR,'visible','off');
    set(handles.pnlProductVaR,'visible','off');
    
    set(handles.tblConsoVaR,'visible','off');
    set(handles.pnlConsoVaR,'visible','off');
    set(handles.tblTotalVaR,'visible','off');
    
    set(handles.pbAddVaR,'visible','off');
    set(handles.pbAddTotalVaR,'visible','off');
    set(handles.tlbrExportToExcel,'visible','on');
    set(handles.btnGrpAddTotalVaR,'visible','off');

    
    set(handles.txtTotalVaR,'visible','off');
    set(handles.edtTotalVaR,'visible','off');    
end

handles.VaRDays = VaRDays;
handles.OutDealWiseVaR = OutDealWiseVaR;
handles.OutProductVaR = OutProductVaR;
handles.ProductWisePnL = ProductWisePnL;
guidata(hObject, handles);



% --- Executes on button press in pbInsertRows.
function pbInsertRows_Callback(hObject, eventdata, handles)
% hObject    handle to pbInsertRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExData = get(handles.tblVaRInput,'data');
NewData = [ExData;handles.InitInData];
set(handles.tblVaRInput,'data',NewData);
guidata(hObject, handles);

% --- Executes on button press in pbDeleteRows.
function pbDeleteRows_Callback(hObject, eventdata, handles)
% hObject    handle to pbDeleteRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExData = get(handles.tblVaRInput,'data');
if ~isfield(handles,'SelectedRows') || isempty(handles.SelectedRows)
    SelectedRows = size(ExData,1);
else
    SelectedRows = handles.SelectedRows;
end
disp(SelectedRows);
QueryResp = questdlg(['Are you sure that you want to delete rows: ',num2str(SelectedRows'),'?'], 'Delete Rows!', 'Yes', 'No', 'No');
if strcmpi(QueryResp,'Yes')
    NewData = ExData;  NewData(SelectedRows,:) = [];
    set(handles.tblVaRInput,'data',NewData);
    guidata(hObject, handles);
end

% --- Executes when selected cell(s) is changed in tblVaRInput.
function tblVaRInput_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblVaRInput (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% disp('In CellSelection Callback of Input table');
handles.SelectedRows = eventdata.Indices(:,1);

SelRowIndex = eventdata.Indices(:,1);
SelColIndex = eventdata.Indices(:,2);

if isequal(SelColIndex,handles.ProductCode_Col)
    ExData = get(handles.tblVaRInput,'data');
    SelTradeType = ExData(SelRowIndex,handles.TradeType_Col);
    if strcmpi(SelTradeType,'FUT')
        ProductCodeStr = unique(handles.FutureProdCode);
    elseif strcmpi(SelTradeType,'OPT')
        ProductCodeStr = unique(handles.OptionProdCode);
    else
        ProductCodeStr = handles.ProductCode;
    end
    SelIdx = listdlg('PromptString','Select a Product Code:',...
        'SelectionMode','single',...
        'ListString',ProductCodeStr);
    if ~isempty(SelIdx)
        Selected_Str = ProductCodeStr(SelIdx);
        disp(Selected_Str);
        ExData(SelRowIndex,SelColIndex) = Selected_Str;
        set(handles.tblVaRInput,'data',ExData);
    end
end

if isequal(SelColIndex,handles.Identifiers_Col)
    ExData = get(handles.tblVaRInput,'data');
    SelTradeType = ExData(SelRowIndex,handles.TradeType_Col);
    SelProdCode = ExData(SelRowIndex,handles.ProductCode_Col);
    if strcmpi(SelTradeType,'FUT')
        IdxSelProducts = ismember(handles.FutureProdCode,SelProdCode);
        IdentifierStr  = handles.FutureId(IdxSelProducts);
        if isempty(IdentifierStr)
            errordlg(['No Future Identifiers found for the selected product ',SelProdCode]);
            return;
        end
    elseif strcmpi(SelTradeType,'OPT')
        IdxSelProducts = ismember(handles.OptionProdCode,SelProdCode);
        IdentifierStr  = handles.OptionId(IdxSelProducts);
        if isempty(IdentifierStr)
            errordlg(['No Option Identifiers found for the selected product ',SelProdCode]);
            return;
        end
    else
        IdentifierStr = handles.Identifiers;
        if isempty(IdentifierStr)
            errordlg(['No Identifiers found for the selected product ',SelProdCode]);
            return;
        end
    end
    %         [Selected_Str] = Customized_ListBox(IdentifierStr);
    SelIdx = listdlg('PromptString','Select an identifier:',...
        'SelectionMode','single',...
        'ListString',IdentifierStr);
    if ~isempty(SelIdx)
        Selected_Str = IdentifierStr(SelIdx);
        disp(Selected_Str);
        ExData(SelRowIndex,SelColIndex) = Selected_Str;
        set(handles.tblVaRInput,'data',ExData);
    end
end

guidata(hObject, handles);

% --- Executes when entered data in editable cell(s) in tblVaRInput.
function tblVaRInput_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tblVaRInput (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

% disp('In CellEdit Callback of Input table');
% SelRowIndex = eventdata.Indices(1);
% SelColIndex = eventdata.Indices(2);

% if isequal(SelColIndex,handles.Identifiers_Col)
%     %     if strcmpi(eventdata.EditData,'--Select--')
%     ExData = get(handles.tblVaRInput,'data');
%     SelTradeType = ExData(SelRowIndex,handles.TradeType_Col);
%     SelProdCode = ExData(SelRowIndex,handles.ProductCode_Col);
%     if strcmpi(SelTradeType,'FUT')
%         IdxSelProducts = ismember(handles.FutureProdCode,SelProdCode);
%         IdentifierStr  = handles.FutureId(IdxSelProducts);
%     elseif strcmpi(SelTradeType,'OPT')
%         IdxSelProducts = ismember(handles.OptionProdCode,SelProdCode);
%         IdentifierStr  = handles.OptionId(IdxSelProducts);
%     else
%         IdentifierStr = handles.Identifiers;
%     end
%     %         [Selected_Str] = Customized_ListBox(IdentifierStr);
%     SelIdx = listdlg('PromptString','Select an identifier:',...
%         'SelectionMode','single',...
%         'ListString',IdentifierStr);
%     Selected_Str = IdentifierStr(SelIdx);
%     disp(Selected_Str);
%     ExData(SelRowIndex,SelColIndex) = Selected_Str;
%     set(handles.tblVaRInput,'data',ExData);
%     %     end
% end


% guidata(hObject, handles);


% --- Executes when entered data in editable cell(s) in tblProductVaR.
function tblProductVaR_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tblProductVaR (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

% disp('In CellEdit Callback of Product VaR table');
% SelRowIndex = eventdata.Indices(1);
% SelColIndex = eventdata.Indices(2);
% 
% if isequal(SelColIndex,4)
%     ExData = get(handles.tblProductVaR,'data');
%     
%     SelIdx = listdlg('PromptString','Select an identifier:',...
%         'SelectionMode','single',...
%         'ListString',handles.DBSubportfolios);
%     Selected_Str = handles.DBSubportfolios(SelIdx);
%     disp(Selected_Str);
%     ExData(SelRowIndex,SelColIndex) = Selected_Str;
%     set(handles.tblProductVaR,'data',ExData);
% end


% guidata(hObject, handles);


% --- Executes on button press in pbAddVaR.
function pbAddVaR_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
OutProductVarData = get(handles.tblProductVaR,'data');
TblColNames = get(handles.tblProductVaR,'columnname');
OutProductVarData = cell2table(OutProductVarData);
OutProductVarData.Properties.VariableNames = TblColNames;

IdxSelectedPF = OutProductVarData.AddToSubportfolio;

SelProducts = OutProductVarData.ProductCode(IdxSelectedPF);
SelPortfolio = OutProductVarData.Subportfolio(IdxSelectedPF);

for iProd = 1:length(SelProducts)
    IdxProdCode = strcmpi(SelProducts{iProd},handles.ProductWisePnL.product_code);
    handles.ProductWisePnL.subportfolio(IdxProdCode) = SelPortfolio(iProd);
end
NewVaRData = handles.ProductWisePnL;

ObjDB = handles.ObjDB;
InBUName = lower(char(handles.InBUName));

SqlQuery = 'select value_date from valuation_date_table';
[~,ValueDate] = Fetch_DB_Data(ObjDB,SqlQuery);
ValueDate = char(ValueDate);
TableName = ['var_dynamic_report_',InBUName];

SqlQuery = ['select * from ',TableName,' where value_date = ''',ValueDate,''''];
[ColNames,ExistingVaRData] = Fetch_DB_Data(ObjDB,SqlQuery);
ExistingVaRData = cell2table(ExistingVaRData);
ExistingVaRData.Properties.VariableNames = ColNames;

% ColNames = get(handles.tblProductVaR,'ColumnName');
% PosProdCode = strcmpi('ProductCode',ColNames);
% PosSubportfolio = strcmpi('Subportfolio',ColNames);
% OutProductVaR = get(handles.tblProductVaR,'data');
% ProdCodes = OutProductVaR(:,PosProdCode);
% Subportfolios = OutProductVaR(:,PosSubportfolio);

% IdxLevelMatch = ismember(ExistingVaRData.product_code,ProdCodes) & ismember(ExistingVaRData.subportfolio,Subportfolios);
TempCurrVarData = [];
for iProd = 1:length(SelProducts)
    IdxLevelMatch = strcmpi(ExistingVaRData.product_code,SelProducts{iProd}) & strcmpi(ExistingVaRData.subportfolio,SelPortfolio{iProd});
    if ~isempty(find(IdxLevelMatch))
        TempCurrVarData = [TempCurrVarData;ExistingVaRData(IdxLevelMatch,:)];
    end
end
InData = [TempCurrVarData;NewVaRData];

ValueDate = char(ValueDate);
OutRowData= [];
for iProd = 1:length(SelProducts)
    OldVaR = 0; NewVaR = 0;
    if strcmpi(InBUName,'cfs')
        IdxAggrLevel = strcmpi(InData.subportfolio,SelPortfolio{iProd});
    else
        IdxAggrLevel = strcmpi(InData.subportfolio,SelPortfolio{iProd}) & ...
            strcmpi(InData.product_code,SelProducts{iProd});
    end
    
    NumIdx = find(IdxAggrLevel);
    if isempty(NumIdx)
        continue;
    end
    if length(NumIdx) == 1
        TempTotalPnL = InData.total_pnl(IdxAggrLevel,:);
        NewVaR = calculate_xl_percentile(TempTotalPnL,5);
    else
        TempVarNum = [];
        for iDays = 1:length(handles.VaRDays)
            CurDays = handles.VaRDays(iDays);
            IdxDays = ismember(InData.num_days(NumIdx),CurDays);
            IdxLines = NumIdx(IdxDays);
            TempNumPnL = [];
            for iN = 1:length(IdxLines)
                TempNumPnL = [TempNumPnL; str2num(cell2mat(InData.total_pnl(IdxLines(iN),:)))];
            end
            [nR,~] = size(TempNumPnL);
            if nR ==1
                TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
            else
                TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
            end
        end
        NewVaR = mean(TempVarNum);
    end
    
    if isempty(TempCurrVarData)
        OldVaR = 0;
    else
        if strcmpi(InBUName,'cfs')
            IdxCurrAggrLevel = strcmpi(TempCurrVarData.subportfolio,SelPortfolio{iProd});
        else
            IdxCurrAggrLevel = strcmpi(TempCurrVarData.subportfolio,SelPortfolio{iProd}) & ...
                strcmpi(TempCurrVarData.product_code,SelProducts{iProd});
        end
        
        NumIdx = find(IdxCurrAggrLevel);
        if isempty(NumIdx)
            OldVaR = 0;
        else
            if length(NumIdx) == 1
                TempTotalPnL = TempCurrVarData.total_pnl(IdxAggrLevel,:);
                OldVaR = calculate_xl_percentile(TempTotalPnL,5);
            else
                TempVarNum = [];
                for iDays = 1:length(handles.VaRDays)
                    CurDays = handles.VaRDays(iDays);
                    IdxDays = ismember(TempCurrVarData.num_days(NumIdx),CurDays);
                    IdxLines = NumIdx(IdxDays);
                    TempNumPnL = [];
                    for iN = 1:length(IdxLines)
                        TempNumPnL = [TempNumPnL; str2num(cell2mat(TempCurrVarData.total_pnl(IdxLines(iN),:)))];
                    end
                    [nR,~] = size(TempNumPnL);
                    if nR ==1
                        TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
                    else
                        TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
                    end
                end
                OldVaR = mean(TempVarNum);
            end
        end
    end
    %     RowData = [cellstr(ValueDate),SelProducts{iProd},SelPortfolio{iProd},num2cell(OldVaR),num2cell(NewVaR)];
    RowData = [SelProducts{iProd},SelPortfolio{iProd},num2cell(OldVaR),num2cell(NewVaR),num2cell(NewVaR-OldVaR)];
    OutRowData = [OutRowData;RowData];
end

if ~isempty(OutRowData)
    OutConsoVaRColumnNames = {'ProductCode','Subportfolio','OldVaR','NewVaR','IncrementalVaR'};
    set(handles.tblConsoVaR,'ColumnName',OutConsoVaRColumnNames);
    
    Col_fmt = {'char','char','bank','bank','bank'};
    set(handles.tblConsoVaR, 'ColumnFormat', Col_fmt);
    
    % update data in Dealwise VaR output table
    set(handles.pnlConsoVaR,'visible','on');
    set(handles.tblConsoVaR,'visible','on');
    
    NoCols = size(OutRowData,2);
    set(handles.tblConsoVaR,'data',OutRowData);
    
    ColWidth = repmat({'auto'},1,NoCols);
    set(handles.tblConsoVaR,'ColumnWidth',ColWidth);
end

set(handles.tlbrExportToExcel,'visible','on');

guidata(hObject, handles);


% --------------------------------------------------------------------
function tlbrNew_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

QueryResp = questdlg('Are you sure that you want to move to New screen?', 'New Screen!', 'Yes', 'No', 'No');
if strcmpi(QueryResp,'Yes')
    
    set(handles.tblVaRInput,'data',handles.InitInData);
    set(handles.tblVaROutput,'data',handles.InitOutData);
    % while clearing the input data table, hide all the output tables in
    % the screen
    set(handles.tblVaROutput,'visible','off');
    set(handles.pnlVaROutput,'visible','off');
    
    set(handles.tblProductVaR,'visible','off');
    set(handles.pnlProductVaR,'visible','off');
    
    set(handles.tblConsoVaR,'visible','off');
    set(handles.pnlConsoVaR,'visible','off');
    set(handles.tblTotalVaR,'visible','off');
    
    set(handles.pbAddVaR,'visible','off');
    set(handles.tlbrExportToExcel,'visible','on');    
    set(handles.pbAddTotalVaR,'visible','off');
    set(handles.btnGrpAddTotalVaR,'visible','off');    
    
    set(handles.txtTotalVaR,'visible','off');
    set(handles.edtTotalVaR,'visible','off');
end

guidata(hObject, handles);


% --------------------------------------------------------------------
function tlbrSaveConfig_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrSaveConfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uiputfile('*.mat', 'Save Configuration View as');
if filename == 0 
    
else
    OutFilename = fullfile(pathname,filename);
    
    InputConfigData = get(handles.tblVaRInput,'data');
    save(OutFilename,'InputConfigData');
end


% --------------------------------------------------------------------
function tlbrOpen_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrOpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat', 'Open Configuration View as');
if filename == 0 
    
else
    OutFilename = fullfile(pathname,filename);
    load(OutFilename);
    set(handles.tblVaRInput,'data',InputConfigData);
end


% --------------------------------------------------------------------
function tlbrUserManual_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrUserManual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    winopen('DynamicVaRGenerator_UserManual.docx');
catch
end


% --- Executes on button press in pbXLExport.
function pbXLExport_Callback(hObject, eventdata, handles)
% hObject    handle to pbXLExport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

OutFilename = getXLSFilename('Dynamic_VaR_Report');

InputVaRData      = get(handles.tblVaRInput,'data');
Header            = get(handles.tblVaRInput,'ColumnName')';
xlswrite(OutFilename,[Header;InputVaRData],'Input Configuration');

OutDealWiseVaR    = get(handles.tblVaROutput,'data');
Header            = get(handles.tblVaROutput,'ColumnName')';
xlswrite(OutFilename,[Header;OutDealWiseVaR],'DealWise VaR');

OutProductVaR     = get(handles.tblProductVaR,'data');
Header            = get(handles.tblProductVaR,'ColumnName')';
xlswrite(OutFilename,[Header;OutProductVaR],'ProductWise VaR');

OutConsoVaR       = get(handles.tblConsoVaR,'data');
Header            = get(handles.tblConsoVaR,'ColumnName')';
xlswrite(OutFilename,[Header;OutConsoVaR],'Product-Portfolio VaR');

try
xls_delete_sheets(fullfile(pwd,OutFilename));
catch
end

% QueryStr = ['XLFile is saved successfully in ',fullfile(pwd,OutFilename),'. Do you want to open the file?'];
% ButtonName = questdlg(QueryStr,'XLFile Save&Open','Yes','No','Yes');
% if strcmpi(ButtonName,'Yes')
%     winopen(OutFilename);
% end

msgbox(['Dynamic VaR Report is saved successfully in ',fullfile(pwd,OutFilename)]);


% --- Executes when selected cell(s) is changed in tblProductVaR.
function tblProductVaR_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tblProductVaR (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% disp('In Cell Selection Callback of Product VaR table');

handles = guidata(hObject);

SelRowIndex = eventdata.Indices(:,1);
SelColIndex = eventdata.Indices(:,2);

if isequal(SelColIndex,4)
    ExData = get(hObject,'data');
    
    SelIdx = listdlg('PromptString','Select a Subportfolio:',...
        'SelectionMode','single',...
        'ListString',handles.DBSubportfolios);
    if ~isempty(SelIdx)
        Selected_Str = handles.DBSubportfolios(SelIdx);
        disp(Selected_Str);
        ExData(SelRowIndex,SelColIndex) = Selected_Str;
        set(handles.tblProductVaR,'data',ExData);
    end
end



function edtTotalVaR_Callback(hObject, eventdata, handles)
% hObject    handle to edtTotalVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtTotalVaR as text
%        str2double(get(hObject,'String')) returns contents of edtTotalVaR as a double


% --- Executes during object creation, after setting all properties.
function edtTotalVaR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtTotalVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbAddTotalVaR.
function pbAddTotalVaR_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddTotalVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ObjDB = handles.ObjDB;

drawnow;  [jObj] = Job_Start(); drawnow;

TableName = handles.VarAggrTableName;
if get(handles.rbPortfolioVaR,'Value')
    SqlQuery = ['select distinct(aggregation_level),subportfolio from ',TableName,' where breach_var_group = ''PORTFOLIO VAR'''];
    Title = 'Portfolio VaR';
elseif get(handles.rbSuportfolioVaR,'Value')
    SqlQuery = ['select distinct(aggregation_level),subportfolio from ',TableName,' where breach_var_group like ''%SUBPORTFOLIO VAR%'''];
    Title = 'Subportfolio VaR';
else
    SqlQuery = ['select distinct(aggregation_level),subportfolio from ',TableName,' where breach_var_group = ''PORTFOLIO-GROUP VAR'''];
    Title = 'Portfolio-Group VaR';
end

VarAggrTableData = fetch(ObjDB,SqlQuery);
InVarAggrLevel = VarAggrTableData(:,1);
InSubportfolio = VarAggrTableData(:,2);

InBUName = lower(char(handles.InBUName));

SqlQuery = 'select value_date from valuation_date_table';
[~,ValueDate] = Fetch_DB_Data(ObjDB,SqlQuery);
ValueDate = char(ValueDate);
TableName = ['var_dynamic_report_',InBUName];

SqlQuery = ['select * from ',TableName,' where value_date = ''',ValueDate,''''];
[ColNames,ExistingVaRData] = Fetch_DB_Data(ObjDB,SqlQuery);
if strcmpi(ExistingVaRData{1},'No Data')
     errordlg('Agrifund VaR Report is not yet generated in OPERA!Could not add Total VaR to OPERA Agrifund VaR');
     drawnow; Job_Done(jObj); drawnow;
     return;
end
ExistingVaRData = cell2table(ExistingVaRData);
ExistingVaRData.Properties.VariableNames = ColNames;

NumLines = size(handles.ProductWisePnL,1);
CurrVaRData = [];

UniqAggrLevels = unique(InVarAggrLevel);
NumAggrLevels = length(UniqAggrLevels);

for iProd = 1:NumLines      
    % Current New VaR Data from Dynamic VaR
    TempCurrVarData = repmat(handles.ProductWisePnL(iProd,:),NumAggrLevels,1);
    TempCurrVarData.aggregation_level = UniqAggrLevels;   
       
    CurrVaRData = [CurrVaRData;TempCurrVarData];
end

if ~isempty(ExistingVaRData)
    NumLines = size(ExistingVaRData,1);
    for iLines = 1:NumLines
        IdxFound = strcmpi(ExistingVaRData.subportfolio(iLines),InSubportfolio);
        if ~isempty(find(IdxFound))
        ExistingVaRData.aggregation_level(iLines) = InVarAggrLevel(IdxFound);
        else
             disp(num2str(iLines));
        end
       
    end
end

InData = [CurrVaRData;ExistingVaRData];
IdxEmptyLevel = (cellfun(@isempty,InData.aggregation_level));
InData(IdxEmptyLevel,:) = [];

OutRowData= [];

for iProd = 1:NumAggrLevels
    OldVaR = 0; NewVaR = 0;
    IdxAggrLevel = strcmpi(UniqAggrLevels{iProd},InData.aggregation_level);
       
    NumIdx = find(IdxAggrLevel);
    if isempty(NumIdx)
        continue;
    end
    if length(NumIdx) == 1
        TempTotalPnL = InData.total_pnl(IdxAggrLevel,:);
        NewVaR = calculate_xl_percentile(TempTotalPnL,5);
    else
        TempVarNum = [];
        for iDays = 1:length(handles.VaRDays)
            CurDays = handles.VaRDays(iDays);
            IdxDays = ismember(InData.num_days(NumIdx),CurDays);
            IdxLines = NumIdx(IdxDays);
            TempNumPnL = [];
            for iN = 1:length(IdxLines)
                TempNumPnL = [TempNumPnL; str2num(cell2mat(InData.total_pnl(IdxLines(iN),:)))];
            end
            [nR,~] = size(TempNumPnL);
            if nR ==1
                TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
            else
                TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
            end
        end
        NewVaR = mean(TempVarNum);
    end
    
    if isempty(ExistingVaRData)
        OldVaR = 0;
    else        
        IdxCurrAggrLevel = strcmpi(UniqAggrLevels{iProd},ExistingVaRData.aggregation_level);        
        
        NumIdx = find(IdxCurrAggrLevel);
        if isempty(NumIdx)
            OldVaR = 0;
        else
            if length(NumIdx) == 1
                TempTotalPnL = ExistingVaRData.total_pnl(IdxAggrLevel,:);
                OldVaR = calculate_xl_percentile(TempTotalPnL,5);
            else
                TempVarNum = [];
                for iDays = 1:length(handles.VaRDays)
                    CurDays = handles.VaRDays(iDays);
                    IdxDays = ismember(ExistingVaRData.num_days(NumIdx),CurDays);
                    IdxLines = NumIdx(IdxDays);
                    TempNumPnL = [];
                    for iN = 1:length(IdxLines)
                        TempNumPnL = [TempNumPnL; str2num(cell2mat(ExistingVaRData.total_pnl(IdxLines(iN),:)))];
                    end
                    [nR,~] = size(TempNumPnL);
                    if nR ==1
                        TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
                    else
                        TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
                    end
                end
                OldVaR = mean(TempVarNum);
            end
        end
    end
    %     RowData = [cellstr(ValueDate),SelProducts{iProd},SelPortfolio{iProd},num2cell(OldVaR),num2cell(NewVaR)];
    RowData = [UniqAggrLevels{iProd},num2cell(OldVaR),num2cell(NewVaR),num2cell(NewVaR-OldVaR)];
    OutRowData = [OutRowData;RowData];
end

if ~isempty(OutRowData)
    OutConsoVaRColumnNames = {'AggregationLevel','OldVaR','NewVaR','IncrementalVaR'};
    set(handles.tblConsoVaR,'ColumnName',OutConsoVaRColumnNames);
    
    Col_fmt = {'char','bank','bank','bank'};
    set(handles.tblConsoVaR, 'ColumnFormat', Col_fmt);
    
    % update data in Dealwise VaR output table
    set(handles.pnlConsoVaR,'visible','on');
    set(handles.tblConsoVaR,'visible','on');
    
    set(handles.pnlConsoVaR,'title',Title);
        
    NoCols = size(OutRowData,2);
    set(handles.tblConsoVaR,'data',OutRowData);
    
    ColWidth = repmat({'auto'},1,NoCols);
    set(handles.tblConsoVaR,'ColumnWidth',ColWidth);
end

% % To generate the Total:Total VaR
% OldVaR = 0; NewVaR = 0;
% 
% TempVarNum = [];
% for iDays = 1:length(handles.VaRDays)
%     CurDays = handles.VaRDays(iDays);
%     IdxDays = ismember(InData.num_days,CurDays);
%     IdxLines = find(IdxDays);
%     TempNumPnL = [];
%     for iN = 1:length(IdxLines)
%         TempNumPnL = [TempNumPnL; str2num(cell2mat(InData.total_pnl(IdxLines(iN),:)))];
%     end
%     [nR,~] = size(TempNumPnL);
%     if nR ==1
%         TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
%     else
%         TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
%     end
% end
% NewVaR = mean(TempVarNum);
% 
% TempVarNum = [];
% for iDays = 1:length(handles.VaRDays)
%     CurDays = handles.VaRDays(iDays);
%     IdxDays = ismember(ExistingVaRData.num_days,CurDays);
%     IdxLines = find(IdxDays);
%     TempNumPnL = [];
%     for iN = 1:length(IdxLines)
%         TempNumPnL = [TempNumPnL; str2num(cell2mat(ExistingVaRData.total_pnl(IdxLines(iN),:)))];
%     end
%     [nR,~] = size(TempNumPnL);
%     if nR ==1
%         TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
%     else
%         TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
%     end
% end
% OldVaR = mean(TempVarNum);

TableName = handles.VarAggrTableName;
SqlQuery = ['select distinct(aggregation_level),subportfolio from ',TableName,' where breach_var_group = ''TOTAL VAR'''];

VarAggrTableData = fetch(ObjDB,SqlQuery);
InVarAggrLevel = VarAggrTableData(:,1);
InSubportfolio = VarAggrTableData(:,2);

SqlQuery = 'select value_date from valuation_date_table';
[~,ValueDate] = Fetch_DB_Data(ObjDB,SqlQuery);
ValueDate = char(ValueDate);

TableName = ['var_dynamic_report_',InBUName];
SqlQuery = ['select * from ',TableName,' where value_date = ''',ValueDate,''''];
[ColNames,ExistingVaRData] = Fetch_DB_Data(ObjDB,SqlQuery);
ExistingVaRData = cell2table(ExistingVaRData);
ExistingVaRData.Properties.VariableNames = ColNames;

NumLines = size(handles.ProductWisePnL,1);
CurrVaRData = [];

UniqAggrLevels = unique(InVarAggrLevel);
NumAggrLevels = length(UniqAggrLevels);

for iProd = 1:NumLines      
    % Current New VaR Data from Dynamic VaR
    TempCurrVarData = repmat(handles.ProductWisePnL(iProd,:),NumAggrLevels,1);
    TempCurrVarData.aggregation_level = UniqAggrLevels;   
       
    CurrVaRData = [CurrVaRData;TempCurrVarData];
end

if ~isempty(ExistingVaRData)
    NumLines = size(ExistingVaRData,1);
    AggregationLevel = cell(NumLines,1);
    
    for iLines = 1:NumLines
        IdxFound = strcmpi(ExistingVaRData.subportfolio(iLines),InSubportfolio);
        if ~isempty(find(IdxFound))
            AggregationLevel(iLines) = InVarAggrLevel(IdxFound);
        else
             disp(num2str(iLines));
        end
       
    end
end
ExistingVaRData.aggregation_level = AggregationLevel;
InData = [CurrVaRData;ExistingVaRData];
IdxEmptyLevel = (cellfun(@isempty,InData.aggregation_level));
InData(IdxEmptyLevel,:) = [];

OutRowData= [];

for iProd = 1:NumAggrLevels
    OldVaR = 0; NewVaR = 0;
    IdxAggrLevel = strcmpi(UniqAggrLevels{iProd},InData.aggregation_level);
       
    NumIdx = find(IdxAggrLevel);
    if isempty(NumIdx)
        continue;
    end
    if length(NumIdx) == 1
        TempTotalPnL = InData.total_pnl(IdxAggrLevel,:);
        NewVaR = calculate_xl_percentile(TempTotalPnL,5);
    else
        TempVarNum = [];
        for iDays = 1:length(handles.VaRDays)
            CurDays = handles.VaRDays(iDays);
            IdxDays = ismember(InData.num_days(NumIdx),CurDays);
            IdxLines = NumIdx(IdxDays);
            TempNumPnL = [];
            for iN = 1:length(IdxLines)
                TempNumPnL = [TempNumPnL; str2num(cell2mat(InData.total_pnl(IdxLines(iN),:)))];
            end
            [nR,~] = size(TempNumPnL);
            if nR ==1
                TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
            else
                TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
            end
        end
        NewVaR = mean(TempVarNum);
    end
    
    if isempty(ExistingVaRData)
        OldVaR = 0;
    else        
        IdxCurrAggrLevel = strcmpi(UniqAggrLevels{iProd},ExistingVaRData.aggregation_level);        
        
        NumIdx = find(IdxCurrAggrLevel);
        if isempty(NumIdx)
            OldVaR = 0;
        else
            if length(NumIdx) == 1
                TempTotalPnL = ExistingVaRData.total_pnl(IdxAggrLevel,:);
                OldVaR = calculate_xl_percentile(TempTotalPnL,5);
            else
                TempVarNum = [];
                for iDays = 1:length(handles.VaRDays)
                    CurDays = handles.VaRDays(iDays);
                    IdxDays = ismember(ExistingVaRData.num_days(NumIdx),CurDays);
                    IdxLines = NumIdx(IdxDays);
                    TempNumPnL = [];
                    for iN = 1:length(IdxLines)
                        TempNumPnL = [TempNumPnL; str2num(cell2mat(ExistingVaRData.total_pnl(IdxLines(iN),:)))];
                    end
                    [nR,~] = size(TempNumPnL);
                    if nR ==1
                        TempVarNum = [TempVarNum;calculate_xl_percentile(TempNumPnL,5)];
                    else
                        TempVarNum = [TempVarNum;calculate_xl_percentile(sum(TempNumPnL),5)];
                    end
                end
                OldVaR = mean(TempVarNum);
            end
        end
    end
    %     RowData = [cellstr(ValueDate),SelProducts{iProd},SelPortfolio{iProd},num2cell(OldVaR),num2cell(NewVaR)];
    RowData = [UniqAggrLevels{iProd},num2cell(OldVaR),num2cell(NewVaR),num2cell(NewVaR-OldVaR)];
    OutRowData = [OutRowData;RowData];
end


% OutTotalData = ['TOTAL PORTFOLIO VaR',num2cell(OldVaR),num2cell(NewVaR),num2cell(NewVaR-OldVaR)];

if ~isempty(OutRowData)
    OutConsoVaRColumnNames = {'AggregationLevel','OldVaR','NewVaR','IncrementalVaR'};
    set(handles.tblTotalVaR,'ColumnName',OutConsoVaRColumnNames);
    
    Col_fmt = {'char','bank','bank','bank'};
    set(handles.tblTotalVaR, 'ColumnFormat', Col_fmt);
    
    set(handles.tblTotalVaR,'visible','on');   
        
    NoCols = size(OutRowData,2);
    set(handles.tblTotalVaR,'data',OutRowData);
    
    ColWidth = repmat({'auto'},1,NoCols);
    set(handles.tblTotalVaR,'ColumnWidth',ColWidth);
end


set(handles.tlbrExportToExcel,'visible','on');

drawnow; Job_Done(jObj); drawnow;

guidata(hObject, handles);


% --- Executes on button press in rbPortfolioGroupVaR.
function rbPortfolioGroupVaR_Callback(hObject, eventdata, handles)
% hObject    handle to rbPortfolioGroupVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbPortfolioGroupVaR


% --- Executes on button press in rbPortfolioVaR.
function rbPortfolioVaR_Callback(hObject, eventdata, handles)
% hObject    handle to rbPortfolioVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbPortfolioVaR


% --- Executes on button press in rbSuportfolioVaR.
function rbSuportfolioVaR_Callback(hObject, eventdata, handles)
% hObject    handle to rbSuportfolioVaR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbSuportfolioVaR


% --------------------------------------------------------------------
function tlbrReferenceDoc_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrReferenceDoc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    winopen('VaR_Methodology_Reference.docx');
catch
end


% --------------------------------------------------------------------
function tlbrExportToExcel_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to tlbrExportToExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
OutFilename = getXLSFilename('Dynamic_VaR_Report');

% InputVaRData      = get(handles.tblVaRInput,'data');
% Header            = get(handles.tblVaRInput,'ColumnName')';
% if ~isempty(InputVaRData)
% xlswrite(OutFilename,[Header;InputVaRData],'Input Configuration');
% end

OutDealWiseVaR    = get(handles.tblVaROutput,'data');
Header            = get(handles.tblVaROutput,'ColumnName')';
if ~isempty(OutDealWiseVaR{1})
xlswrite(OutFilename,[Header;OutDealWiseVaR],'DealWise VaR');
end

OutProductVaR     = get(handles.tblProductVaR,'data');
Header            = get(handles.tblProductVaR,'ColumnName')';
if ~isempty(OutProductVaR{1})
xlswrite(OutFilename,[Header;OutProductVaR],'ProductWise VaR');
end

OutConsoVaR       = get(handles.tblConsoVaR,'data');
Header            = get(handles.tblConsoVaR,'ColumnName')';
OutTotalVaR       = get(handles.tblTotalVaR,'data');
if ~isempty(OutConsoVaR{1})
    OutConsoVaR = [OutTotalVaR;OutConsoVaR];
    xlswrite(OutFilename,[Header;OutConsoVaR],'Consolidated VaR');
end

% OutTotalVaR       = get(handles.tblTotalVaR,'data');
% Header            = get(handles.tblTotalVaR,'ColumnName')';
% if ~isempty(OutTotalVaR)
% xlswrite(OutFilename,[Header;OutTotalVaR],'Total Portfolio VaR');
% end

try
xls_delete_sheets(fullfile(pwd,OutFilename));
catch
end

QueryStr = ['Dynamic VaR Report is saved successfully in ',fullfile(pwd,OutFilename),'. Do you want to open the file?'];
ButtonName = questdlg(QueryStr,'XLFile Save&Open','Yes','No','Yes');
if strcmpi(ButtonName,'Yes')
    winopen(OutFilename);
end

% msgbox(['Dynamic VaR Report is saved successfully in ',fullfile(pwd,OutFilename)]);
