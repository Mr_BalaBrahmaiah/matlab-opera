function [OutErrorMsg,OutPDFFile]= generate_client_statement_pdf_report(Current_Client_Info ,Current_Sheet_Name,IncludeMtmNC)
try
    OutErrorMsg={'No Errors'};
    
    
    %% Removal of A,B,C as a postfix in contract number.
    idx_total_pf=find(strcmpi(Current_Client_Info(:,1),'TOTAL'));
    req_value_tot_pf=max(idx_total_pf);
    idx_trade_id=min(find(strcmpi(Current_Client_Info(:,2),'TRADE_ID')));
    req_data_trade_id_op=Current_Client_Info( idx_trade_id:req_value_tot_pf-2,2);
    
    %% Data Replacement
    
    TF =  find(endsWith(req_data_trade_id_op,{'A','B','C'}));
    if ~isempty(TF)
        %         T_string=char( req_data_trade_id_op(TF));
        T_string=cell(length(TF),1);
        for qq=1:size(TF,1)
            z=char( req_data_trade_id_op(TF(qq)));
            T_string(qq)=cellstr(z(1:end-1));
        end
        req_data_trade_id_op(TF) =T_string;
        %         req_data_trade_id_op(TF) =cellstr(T_string(1:end,1:end-1));
        Current_Client_Info( idx_trade_id:req_value_tot_pf-2,2)= req_data_trade_id_op;
    end
    
    %% Extra Empty Row addition in data to immplement Logic
    [~,b_Overall_Current]=size(Current_Client_Info);
    Overall_Current_Client_Data_Final=[Current_Client_Info;cell(1,b_Overall_Current)];
    
    
    %% To make DOMCompilable in Order to execute it...we have fixed it.
    makeDOMCompilable();
    
    %% DOM API Introduction (it will always be there in the function to access Dom pacakeges).
    import mlreportgen.dom.*;
    import mlreportgen.report.*;
    
    %% Document Creation Dynamically (Here to resolve one error 'permission denied' instead pdf we have generated .docx)
    OutPDFFile=Document(char(Current_Sheet_Name),'docx','Doc6');
    
    % DOC6 is a template created in MS Word.
    % open a new microsoft word file, go to Developers Section of it.
    % click on design mode and select rich text
    % add holes in this section with proper name and tag
    % after that refer from line number 81-93.
    
    
    %% Opening of Document
    open(OutPDFFile);
    
    %% CONFIRMATION DATE ISEMPTY STATUS
    CONFIRMATION_DATE=find(strcmpi('CONFIRMATION DATE:',Overall_Current_Client_Data_Final(:,1)));
    CONFIRMATION_DATE_CHECK=~isempty(CONFIRMATION_DATE);
    
    %% Manipulate Variable Analysis
    OTC_SWAPS_INDEX=find(strcmpi('OTC SWAPS',Overall_Current_Client_Data_Final(:,1)));
    OTC_SWAPS_CHECK=~isempty(OTC_SWAPS_INDEX);
    
    %% Checking Variable analysis
    OTC_OPTIONS_INDEX=find(strcmpi('OTC OPTIONS',Overall_Current_Client_Data_Final(:,1)));
    OTC_OPTIONS_CHECK=~isempty(OTC_OPTIONS_INDEX);
    
    %% OTC ACCUMULATORS CHECK
    OTC_ACCUMULATORS_INDEX=find(strcmpi('OTC ACCUMULATORS',Overall_Current_Client_Data_Final(:,1)));
    OTC_ACCUMULATORS_CHECK=~isempty(OTC_ACCUMULATORS_INDEX);
    
    %% OTC FX SWAPS CHECK
    OTC_FX_SWAPS_INDEX=find(strcmpi('OTC FX SWAPS',Overall_Current_Client_Data_Final(:,1)), 1);
    OTC_FX_SWAPS_CHECK=~isempty(OTC_FX_SWAPS_INDEX);
    
    %% OTC FX OPTIONS CHECK
    OTC_FX_OPTIONS_INDEX=find(strcmpi('OTC FX OPTIONS',Overall_Current_Client_Data_Final(:,1)), 1);
    OTC_FX_OPTIONS_CHECK=~isempty(OTC_FX_OPTIONS_INDEX);
    
    %% OTC KC/USDBRL Compo SWAPS
    KC_COMPOS_INDEX=find(strcmpi('KC/USDBRL Compo SWAPS',Overall_Current_Client_Data_Final(:,1)), 1);
    KC_COMPOS_CHECK=~isempty(KC_COMPOS_INDEX);
    
    %% CASH BALANCE CHECK
    CASH_BALANCE_INFO_INDEX=find(strcmpi('GRAND TOTAL',Overall_Current_Client_Data_Final(:,1)));
    CASH_BALANCE_INFO_CHECK=~isempty(CASH_BALANCE_INFO_INDEX);
    
    %% EMPTY SPACES CHECK
    EMPTY_SPACE_INDEX=find(cellfun(@isempty,Overall_Current_Client_Data_Final(:,1)));
    
    %% CLIENT INFORMATION SECTION
    if (CONFIRMATION_DATE_CHECK==1)
        a=find(strcmpi('CONFIRMATION DATE:',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        d=EMPTY_SPACE_INDEX-a;
        
        e=find(d>0, 1 );
        dynamic_space=a+d(e); %% Dynamic space
        
        %% OTC SWAPS SECTION Data Accession
        OTC_CONFIRMATION_SECTION=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end);
        
        %% Here we can remove some column from data as per requirement.
        %% APPEND FUNCTION BE CAREFUL
        % OTC_CONFIRMATION_SECTION(:,17:21)=[];  % No Need
        %% Sections to Manipulate Headers Using Template
        header_data = OTC_CONFIRMATION_SECTION(:,2); % Acessing second column data which is client information.
        page_layout = OutPDFFile.CurrentPageLayout; % Pagelayout settings
        header = page_layout.PageHeaders(1); %% To access page header
        header.moveToNextHole;
        text_ob=Text(header_data{1});
        text_ob.Color='#974706';
        append(header,text_ob);
        header.moveToNextHole;
        append(header,header_data{2});
        header.moveToNextHole;
        append(header,header_data{3});
        header.moveToNextHole;
        append(header,header_data{4});
        
        %% Page layout settings.
        pagesetup(OutPDFFile);
    end
    
    if (OTC_SWAPS_CHECK==1)
        a=find(strcmpi('OTC SWAPS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        %% MTM_MC introduction and removal as per the currency input.
        %% we are doing this by making a flag named 'IncludeMtmNC';
        
        OTC_SWAPS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end); % Replace 2 by 1
        if (IncludeMtmNC==1)
            OTC_SWAPS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        elseif (IncludeMtmNC==0)
            MTM_NC_INDEX=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'MTM_NC'));
            OTC_SWAPS_SECTION_EX_SP_T(:,MTM_NC_INDEX)=[];
        end
        
        %% highfen fix in OTC SWAPS section.
        idx_total_man=find(strcmpi( OTC_SWAPS_SECTION_EX_SP_T(:,5),'TOTAL'));
        max_total=max(idx_total_man);
        val_cell=char(OTC_SWAPS_SECTION_EX_SP_T(max_total,7));
        val_cell_eigth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total,8));
        val_cell_sixth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total,6));
        val_cell_ninth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total,9));
        val_cell_tenth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total,10));
        if isempty(val_cell)
            OTC_SWAPS_SECTION_EX_SP_T(max_total,7)={'-'};
        end
        if isempty( val_cell_eigth)
            OTC_SWAPS_SECTION_EX_SP_T(max_total,8)={'-'};
        end
        if isempty( val_cell_sixth)
            OTC_SWAPS_SECTION_EX_SP_T(max_total,6)={'-'};
        end
        if isempty( val_cell_ninth)
            OTC_SWAPS_SECTION_EX_SP_T(max_total,9)={'-'};
        end
        if isempty( val_cell_tenth)
            OTC_SWAPS_SECTION_EX_SP_T(max_total,10)={'-'};
        end
        
        %% Highfen fix
        val_up_row_sixth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total-2,6));
        val_up_row_seventh=char(OTC_SWAPS_SECTION_EX_SP_T(max_total-2,7));
        val_up_row_eight=char(OTC_SWAPS_SECTION_EX_SP_T(max_total-2,8));
        val_up_row_ninth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total-2,9));
        val_up_row_tenth=char(OTC_SWAPS_SECTION_EX_SP_T(max_total-2,10));
        if isempty(val_up_row_sixth)
            OTC_SWAPS_SECTION_EX_SP_T(max_total-2,6)={'-'};
        end
        if isempty( val_up_row_seventh)
            OTC_SWAPS_SECTION_EX_SP_T(max_total-2,7)={'-'};
        end
        if isempty(   val_up_row_eight)
            OTC_SWAPS_SECTION_EX_SP_T(max_total-2,8)={'-'};
        end
        if isempty(  val_up_row_ninth )
            OTC_SWAPS_SECTION_EX_SP_T(max_total-2,9)={'-'};
        end
        if isempty( val_up_row_tenth )
            OTC_SWAPS_SECTION_EX_SP_T(max_total-2,10)={'-'};
        end
        %% Highfen fix by loop manipulation...Please
        [len_a,~]=size( OTC_SWAPS_SECTION_EX_SP_T);
        for i=3:len_a-3
            for j=6
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=7
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=8
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=9
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=10
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=20
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        %% Empty replacement with '-'
        %% only line above total has been taken into coniseration.
        for i=len_a-1
            for j=6:10
                if isempty(OTC_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        %% LONG_LOTS Manipulation in six digits and nan replacement with empty karthiks requirement.
        LONG_LOTS_INDEX=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'LONG_LOTS'));
        %         required_data=str2double(OTC_SWAPS_SECTION_EX_SP_T(:,LONG_LOTS_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str( six_decimals);
        %         OTC_SWAPS_SECTION_EX_SP_T(index_numeric,LONG_LOTS_INDEX)=output;
        
        %% AVG_LONG_LOTS Manipulation
        %OTC_SWAPS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end); % Replace 2 by 1
        %         AVG_LONG_PRICE=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'AVG_LONG_PRICE'));
        %         required_data=str2double(OTC_SWAPS_SECTION_EX_SP_T(:,AVG_LONG_PRICE));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str( six_decimals);
        %         OTC_SWAPS_SECTION_EX_SP_T(index_numeric,AVG_LONG_PRICE)= output ;
        
        %% SHORT_LOTS Manipulation
        %         SHORT_LOTS=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'SHORT_LOTS'));
        %         required_data=str2double(OTC_SWAPS_SECTION_EX_SP_T(:,SHORT_LOTS));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str( six_decimals);
        %         OTC_SWAPS_SECTION_EX_SP_T(index_numeric,SHORT_LOTS)= output ;
        
        %% AVG_SHORT_PRICE Manipulation
        %         AVG_SHORT_PRICE=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'AVG_SHORT_PRICE'));
        %         required_data=str2double(OTC_SWAPS_SECTION_EX_SP_T(:,AVG_SHORT_PRICE));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str( six_decimals);
        %         OTC_SWAPS_SECTION_EX_SP_T(index_numeric,AVG_SHORT_PRICE)=output;
        %
        %% EDSP MANAIPULATION
        EDSP=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'EDSP'));
        %         required_data1=OTC_SWAPS_SECTION_EX_SP_T(3:EDSP:end);
        required_data=OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP:end);
        [required_data_format ,error]=format_cell(required_data,'%.2f');
        output = Sep1000Str( required_data_format);
        OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP:end)=output;
        
        %% Data Filtering for commas introduction
        MTM_USD_INDEX=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'MTM_USD'));
        MTM_EUR_INDEX=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'MTM_EUR'));
        
        %% if reports starts with name SIP format manipulation.
        if startsWith(Current_Sheet_Name,'SIP')
            EDSP_id=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'OR_EDSP_USD'));
            required_data=OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP_id:end);
            [required_data_format ,error]=format_cell(required_data,'%.2f');
            OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP_id:end)=required_data_format;
            Filtered_data_for_commas_in_swaps= OTC_SWAPS_SECTION_EX_SP_T(3:end,LONG_LOTS_INDEX:MTM_EUR_INDEX);
            %             EDSP_id=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'OR_EDSP_USD'));
            %             required_data=OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP_id:end);
            %             [required_data_format ,error]=format_cell(required_data,'%.2f');
            %             output = Sep1000Str( required_data_format);
            %             OTC_SWAPS_SECTION_EX_SP_T(3:end,EDSP_id:end)=output;
        else
            %dimen_reqdata=[];
            Filtered_data_for_commas_in_swaps= OTC_SWAPS_SECTION_EX_SP_T(3:end,LONG_LOTS_INDEX:MTM_USD_INDEX); %% Make this claus dynamic.
        end
        
        output = Sep1000Str(  Filtered_data_for_commas_in_swaps );
        %% Data Replacement with commas for section except rest total
        if startsWith(Current_Sheet_Name,'SIP')
            OTC_SWAPS_SECTION_EX_SP_T(3:end,LONG_LOTS_INDEX:MTM_EUR_INDEX)=output;
        else
            OTC_SWAPS_SECTION_EX_SP_T(3:end,LONG_LOTS_INDEX:MTM_USD_INDEX)=output;
        end
        
        
        %% Underscore removal from headers.
        
        underscore_remove_swaps=cellfun(@(v) strrep(v,'_',' '),  OTC_SWAPS_SECTION_EX_SP_T(2,:),'UniformOutput', false);
        OTC_SWAPS_SECTION_EX_SP_T(2,:)=underscore_remove_swaps;
        
        %% Bracket Introduction in Long lots
        index_long_lots=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'LONG LOTS'));
        LONG_LOTS_DATA= OTC_SWAPS_SECTION_EX_SP_T(2,index_long_lots);
        newstr=replace(LONG_LOTS_DATA,' LOTS',' (LOTS)');
        OTC_SWAPS_SECTION_EX_SP_T(2,index_long_lots)=newstr;
        
        %% Bracket Introduction in short lots
        index_short_lots=find(strcmpi(OTC_SWAPS_SECTION_EX_SP_T(2,:),'SHORT LOTS'));
        LONG_LOTS_DATA= OTC_SWAPS_SECTION_EX_SP_T(2,index_short_lots);
        newstr=replace(LONG_LOTS_DATA,' LOTS',' (LOTS)');
        OTC_SWAPS_SECTION_EX_SP_T(2,index_short_lots)=newstr;
        
        %% zeros replacement with '-'
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000'),  OTC_SWAPS_SECTION_EX_SP_T,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        OTC_SWAPS_SECTION_EX_SP_T(index)=cellstr(a);
        
        %% First Row Seperation From the rest of Data.
        title_row = OTC_SWAPS_SECTION_EX_SP_T(1,:);
        p = Table(title_row);
        p.Style={Width('100%'),FontSize('0.15in'),HAlign('left'),Bold,ResizeToFitContents(false)};
        p.Border='None';
        p.TableEntriesHAlign='Right';
        
        % calling table setting function
        if ~startsWith(Current_Sheet_Name,'SIP')
            da  = table_properties( p );
        else
            da = formatting_swapssipta(p);
        end
        %% Column spacing Manipulations (Seperation Between the Columns to attain a format).
        grp = TableColSpecGroup();
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false)};
        grp.Span = p.NCols; %% that span will implements on all 21 columns.
        %         grp.Style = {Width('1.5in')};  %% Doubts.........0.9
        p.ColSpecGroups = grp;
        append(OutPDFFile,p); %% First Table appension
        
        row_tobe_repeated = OTC_SWAPS_SECTION_EX_SP_T(2,:);
        new_cell_array = OTC_SWAPS_SECTION_EX_SP_T(2:end,:);
        
        %% New Section for Horizontal line Introduction after total.
        idx = strfind(new_cell_array,'TOTAL'); %Finds the string 'TOTAL' in the data and creates a cell array with '2' in place of all 'TOTALS' and empty for all other data
        idx = cellfun(@(x)isequal(x,1),idx);% Here i replaced 1 by 2 Find the index of all the 2s - create a logical matrix with 0 where dummy data is present and 1 where the 'TOTAL' string is present
        [row_idx,col_idx] = find(idx); %Find the position of all the ones.
        %row_idx=row_idx-1;
        %new_cell_array(row_idx(1:end-1)-1,:)=[];
        new_cell_array(row_idx-1,:)=[];
        idx = strfind(new_cell_array,'TOTAL');
        idx = cellfun(@(x)isequal(x,1),idx);
        [row_idx,col_idx] = find(idx);
        [num_rows,num_columns] = size(new_cell_array);
        t= Table(num_columns);   %num_columns...Table from scratch generally made up of number of columns.
        
        %% Table style manipulations
        t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
        t.Border='None';
        t.TableEntriesHAlign='Right';
        if ~startsWith(Current_Sheet_Name,'SIP')
            da  = table_properties( t );
        else
            da = formatting_swapssipta(t);
        end
        %         da  = table_properties( t );
        %% Page size manipulation (make page settings after every table)
        
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        % Importnat Section to achieve proper formating.
        grp = TableColSpecGroup();
        grp.Span = t.NCols;
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false)};
        %         grp.Span = t.NCols; %% Here the spacing between columns can be managed by
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
        t.ColSpecGroups = grp;
        i = 1; % Modified
        
        %% Here How to manage the required Format with Total.
        while(i<=num_rows) % Modified
            if(ismember(i,row_idx))
                append(OutPDFFile,t); % We are appending Just atable.
                if ~startsWith(Current_Sheet_Name,'SIP')
                    if t.NCols ==20
                        para = Paragraph('                                                                                                                                                                                                                      _______________________________________________________________________________________________________                                                                                                                                       _________________');
                    else
                        para = Paragraph('                                                                                                                                                                                                           ____________________________________________________________________________________________________                                                                                                                     _________________________________');
                    end
                    para.WhiteSpace = 'preserve';
                else
                    para = Paragraph('                                                                                                                                                                                               _______________________________________________________________________________________________________________________                                                                                       _________________________________');
                    para.WhiteSpace = 'preserve';
                end
                append(OutPDFFile,para); % Under Specified Columns we have added a line by making white space preserved hard coding
                
                %% We Have Fixed the Table formating using table column specification groups
                t = Table(num_columns);
                t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),BorderCollapse('on'),ResizeToFitContents(false),Bold}; %% This is table contents under OTC SWAPS
                t.Border='None';
                t.TableEntriesHAlign='Right';
                
                %% Table Column Seperation
                grp = TableColSpecGroup();
                grp.Span = t.NCols;
                grp.ColSpecs = da;
                grp.Style = {ResizeToFitContents(false)};
                %                 grp.Span = t.NCols; %% Here the spacing between columns can be managed by
                %                 grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
                
                t.ColSpecGroups = grp;
                
                %% Now in Table Having specified Columns we have added row this is the way of creating Table By scratch
                row = TableRow(); % We have created one row outside the loop.
                
                for k=1:num_columns % For one row it will access all number of columns.
                    entry = TableEntry(); % all Table entries will be created
                    % %                         entry.Style={HAlign('right')};
                    %
                    if(strcmp(new_cell_array{i,k},'TOTAL')) % wherever the Total is there remove that Total
                        new_cell_array{i,k} = '   ';
                        %  new_cell_array(i,:)=[];
                    end
                    %
                    append(entry,new_cell_array{i,k}); % Here we have appended entry
                    
                    
                    %% Red Colour OTC SWAPS MANIPULATION
                    if ((k==num_columns-1) || (k==num_columns))
                        if(str2double(new_cell_array{i,k})<0)
                            entry.Children.Color = 'red';
                            entry.Children.Content=strrep((entry.Children.Content),'-','');
                            
                            entry.Children.Content = strcat('(',entry.Children.Content,')');
                            
                        end
                    end
                    %
                    
                    append(row,entry);
                end
                append(t,row);
                
                append(OutPDFFile,t);
                
                if (i==num_rows)
                    break;
                end
                
                %% New section
                i = i+1;
                
                if(find((i-1)==row_idx) ~= length(row_idx))% Added
                    
                    t = Table(num_columns);
                    t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
                    t.Border='None';
                    t.TableEntriesHAlign='Right';
                    grp = TableColSpecGroup();
                    grp.ColSpecs = da;
                    grp.Style = {ResizeToFitContents(false)};
                    %% Append the row to be repeated
                    row = TableRow();
                    row.Style = {RepeatAsHeaderRow(true),Color('#974706')};
                    
                    
                    for k = 1:num_columns
                        entry = TableEntry();
                        %                         entry.Style={HAlign('right')};
                        append(entry,row_tobe_repeated{1,k});
                        
                        append(row,entry);
                    end
                    append(t,row);
                    
                    %                     t.entry(end,6).Style = [t.entry(end,6).Style {mlreportgen.dom.Width('0.5in')}];
                    
                    %% Page size manipulation
                    pagesetup(OutPDFFile);
                    
                    %% Width Control Code of columns
                    grp = TableColSpecGroup();
                    grp.ColSpecs = da;
                    grp.Span = t.NCols;
                    grp.Style = {ResizeToFitContents(false)};
                    %% Here the spacing between columns can be managed by
                    %                     grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
                    %                     %                     grp.TableEntriesHAlign='right';
                    t.ColSpecGroups = grp;
                    t.HAlign = 'left';
                    t.Width='100%';
                    %                     t.TableEntriesHAlign='right';
                    % t.entry(end,6).Style = [t.entry(end,7).Style {mlreportgen.dom.Width('0.08in')}];
                    
                end
            end
            row = TableRow();
            row.Style={HAlign('Right'),Width('100%'),ResizeToFitContents(false)};
            for j = 1:num_columns
                entry = TableEntry();
                %                                 entry.Style={HAlign('Right')};
                %                 entry.Style={TableEntriesHAlign('right')};
                %                 if (i==row_idx-1)
                %                         new_cell_array{i,:}=[];
                %                 end
                
                if str2double(new_cell_array{i,j})<0
                    
                    if(strcmp(new_cell_array{i,j},'TOTAL'))
                        new_cell_array{i,j} = '   ';
                        % new_cell_array(i-1,:)=[];
                    end
                    
                    append(entry, new_cell_array{i,j});
                    if j>(num_columns-2) % Added
                        entry.Children.Color='red';
                        entry.Children.Content(entry.Children.Content==')')=[];
                        %                         entry.Children.Content=num2str(abs(str2double(string(entry.Children.Content))));
                        entry.Children.Content=strrep((entry.Children.Content),'-','');
                        entry.Children.Content = strcat('(',entry.Children.Content,')');
                    end
                    
                else
                    
                    if(strcmp(new_cell_array{i,j},'TOTAL'))
                        new_cell_array{i,j} = '   ';
                        % new_cell_array(i,:)=[];
                    end
                    append(entry, new_cell_array{i,j});
                end
                
                %% Bold and Coloring Mechanism of OTC SWAPS
                if(i==1)
                    entry.Children.Color = '#974706';
                    %entry.Style={HAlign('Right'),Width('100%')};
                end
                
                append(row,entry);
                
            end
            append(t,row);
            i = i+1; % Modified
            
        end
        %% SIZE FIX
        %% Page size manipulation and coloring for proper formating
        t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
        t.Border='None';
        t.TableEntriesHAlign='Right';
        
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false)};
        
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
        %         grp.TableEntriesHAlign='right';
        t.ColSpecGroups = grp;
        %         t.HAlign = 'left';
        %         t.TableEntriesHAlign='right';
        
        
        %% Table appension Confirmation section
        if(find((i-1)==row_idx) ~= length(row_idx)) % Added
            grp = TableColSpecGroup();
            grp.Span = t.NCols;
            grp.ColSpecs = da;
            grp.Style = {ResizeToFitContents(false)};
            %             grp.Span = t.NCols; %% Here the spacing between columns can be managed by
            %             grp.Style = {Width('1.5in'),HAlign('right')}; %% From Here we can manage the width of a Table 0.7
            
            %             grp.TableEntriesHAlign='right';
            t.ColSpecGroups = grp;
            
            
        end
        % If user wants GRAND_TOTAL after individual total..uncopmment below section of code.
        %% Horizontal Line appension
        hr1 = HorizontalRule();
        hr1.Border='single';
        hr1.Style={Width('100%'),Bold};
        %hr1.Style={OuterMargin('3.2in','7.3in','0in','0in')};
        append(OutPDFFile,hr1);
        
        %% To Manage Total within lines......Requirement 6 Implementation.
        SIP_MAN=startsWith(Current_Sheet_Name,'SIP');
        
        if (SIP_MAN==1)
            MTM_EUR_TOTAL=str2double(OTC_SWAPS_SECTION_EX_SP_T(3:end,21));
            MTM_EUR_TOTAL(isnan(MTM_EUR_TOTAL))=0;
            % rajashekhar Added code
            idx_sip = strcmpi(OTC_SWAPS_SECTION_EX_SP_T(3:end,5),'TOTAL');
            MTM_EUR_TOTAL(idx_sip)=0;
            SUM_MTM_EUR_TOTAL=sum(MTM_EUR_TOTAL);
            SUM_MTM_EUR_TOTAL =sprintf('%.2f',SUM_MTM_EUR_TOTAL);
            % end of code
            OTC_SWAPS_SECTION_REST_FOR_TOTAL(1,21)= cellstr(num2str(SUM_MTM_EUR_TOTAL));
            output = Sep1000Str(OTC_SWAPS_SECTION_REST_FOR_TOTAL);
        else
            OTC_SWAPS_SECTION_REST_FOR_TOTAL=Overall_Current_Client_Data_Final(dynamic_space,1:end);
            output = Sep1000Str(   OTC_SWAPS_SECTION_REST_FOR_TOTAL);
        end
        % rajashekhar added end here
        %         end
        %% Fix to get Total
        output(1,1)={'TOTAL'};
        % rajashekhar Added
        OTC_SWAPS_SECTION_REST_FOR_TOTAL=output;
        % end
        %         OTC_SWAPS_SECTION_REST_FOR_TOTAL=req_data_rest_swaps;
        [len_Tot, Bre_Tot]=size(OTC_SWAPS_SECTION_REST_FOR_TOTAL);
        OTC_SWAPS_SECTION_REST=cell(1,Bre_Tot);
        OTC_SWAPS_SECTION_REST(1,1)= OTC_SWAPS_SECTION_REST_FOR_TOTAL(1,1);
        
        Empty_cell_indexes=find(cellfun(@isempty, OTC_SWAPS_SECTION_REST));
        OTC_SWAPS_SECTION_REST(Empty_cell_indexes)={' '};
        if (OTC_SWAPS_SECTION_REST_FOR_TOTAL{1,end-1}== '-')
            OTC_SWAPS_SECTION_REST(1,Bre_Tot)=OTC_SWAPS_SECTION_REST_FOR_TOTAL(1,end);
            %OTC_SWAPS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,end);
        else
            OTC_SWAPS_SECTION_REST(1,end-1)= OTC_SWAPS_SECTION_REST_FOR_TOTAL(1,end-1);
            OTC_SWAPS_SECTION_REST(1,end)= OTC_SWAPS_SECTION_REST_FOR_TOTAL(1,end);
        end
        
        [OTC_SWAPS_SECTION_REST,~] =format_cell(OTC_SWAPS_SECTION_REST,'%.2f');
        output = Sep1000Str(   OTC_SWAPS_SECTION_REST(5:end));
        OTC_SWAPS_SECTION_REST(5:end)= output;
        %% Column space management.
        t2=Table( OTC_SWAPS_SECTION_REST);
        t2.HAlign='left';
        
        %% Color Manipulation Section
        %% Red Color Manipulation
        for  i2=1:t2.NRows
            for j2=t2.NCols-3:t2.NCols
                if str2double(t2.entry(i2,j2).Children(1).Content)<0
                    t2.entry(i2,j2).Children(1).Color='red';
                    %                      t2.entry(i2,j2).Style={HAlign('Right')};
                    %                         t2.entry(i2,j2).Children.Content=num2str(abs(str2double(string(t2.entry(i2,j2).Children.Content))));
                    t2.entry(i2,j2).Children.Content=strrep((t2.entry(i2,j2).Children.Content),'-','');
                    t2.entry(i2,j2).Children.Content = strcat('(',t2.entry(i2,j2).Children.Content,')');
                    
                    
                end
            end
        end
        
        %%%
        t2.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(true),OuterMargin('0pt','0pt','0pt','0pt'),BorderCollapse('on'),Bold};
        t2.Border='None';
        t2.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t2.NCols; %% Here the spacing between columns can be managed by
        grp.Style = {Width('1.5in'),ResizeToFitContents(true)}; %% From Here we can manage the width of a Table. 0.7
        t2.ColSpecGroups = grp;
        t2.HAlign = 'left';
        %         t2.TableEntriesHAlign='right';
        %
        %% Total Bold Mechanism
        OTC_SWAPS_SECTION_REST_FOR_TOTAL{1,1}='TOTAL';
        Total_ROW_t=strcmpi('TOTAL', OTC_SWAPS_SECTION_REST_FOR_TOTAL(:,1));
        FIRST_ROW_Total=t2.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),Bold};
        
        
        %% Table appension Confirmation section
        append(OutPDFFile,t2);
        
        
        %         end
    end
    
    
    if (OTC_OPTIONS_CHECK==1)
        %% Edited Section for Page Break
        
        a=find(strcmpi('OTC OPTIONS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        %% Access Variable
        OTC_OPTIONS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        
        %% New Section added
        firstt_section=OTC_OPTIONS_SECTION_EX_SP_T(1:2,:);  %% First two rows of the data
        second_section=OTC_OPTIONS_SECTION_EX_SP_T(3:end,:); %% Third to second last row because last row is empty we dont want it.
        index_empty=find(cellfun(@isempty,second_section)); %% Index finding for empty data
        req='-'; %% The character to be replaced with empty values.
        second_section(index_empty)=cellstr(req); %% Replacement line
        OTC_OPTIONS_SECTION_EX_SP_T=[firstt_section;second_section]; %% Concatenation to get the proper data
        empty_index=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'')); %% to find the index of empty
        OTC_OPTIONS_SECTION_EX_SP_T(:,empty_index)={' '}; %% its replacement with blanks as per the report requireent
        
        %% MTM_NC Management
        %% New Code for MTM_NC Section
        
        %         OTC_OPTIONS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        if (IncludeMtmNC==1)
            OTC_OPTIONS_SECTION_EX_SP_T= OTC_OPTIONS_SECTION_EX_SP_T;
        elseif (IncludeMtmNC==1)
            MTM_NC_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'MTM_NC'));
            OTC_OPTIONS_SECTION_EX_SP_T(:,  MTM_NC_INDEX)=[];
        end
        
        %% LOTS SIX DECIMAL PLACE
        LOTS_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'LOTS'));
        %         required_data=str2double(OTC_OPTIONS_SECTION_EX_SP_T(:,LOTS_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str(six_decimals);
        %         OTC_OPTIONS_SECTION_EX_SP_T(index_numeric,LOTS_INDEX)=output;
        
        %% Barrier strike two decimal
        strike_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'STRIKE'));
        Barrier_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'BARRIER_STRIKE'));
        edsp_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'EDSP'));
        
        required_data=OTC_OPTIONS_SECTION_EX_SP_T(3:end,[strike_INDEX Barrier_INDEX edsp_INDEX]);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        %         required_data=str2double(OTC_OPTIONS_SECTION_EX_SP_T(:,Barrier_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.2f',Data_values), Data_values, 'uniform', 0);
        output = Sep1000Str(required_data_2decimal);
        OTC_OPTIONS_SECTION_EX_SP_T(3:end, [strike_INDEX Barrier_INDEX edsp_INDEX])=output;
        
        
        TRADE_PRICE_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'TRADE_PRICE'));
        required_data=OTC_OPTIONS_SECTION_EX_SP_T(3:end,TRADE_PRICE_INDEX+1:end);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal);
        OTC_OPTIONS_SECTION_EX_SP_T(3:end, TRADE_PRICE_INDEX+1:end)=output;
        %% INDEX DR_CR_USD
        DR_CR_USD=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'DR_CR_USD'));
        
        %% TRADE PRICE SIX DIGIT CONVERSION
        %         TRADE_PRICE_INDEX=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'TRADE_PRICE'));
        %         required_data=str2double(OTC_OPTIONS_SECTION_EX_SP_T(:,TRADE_PRICE_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %          output = Sep1000Str(six_decimals);
        %         OTC_OPTIONS_SECTION_EX_SP_T(index_numeric,TRADE_PRICE_INDEX)=  output;
        
        
        
        %% Underscore removal from headers.
        
        underscore_removal_options=cellfun(@(v) strrep(v,'_',' '), OTC_OPTIONS_SECTION_EX_SP_T(2,:),'UniformOutput', false);
        OTC_OPTIONS_SECTION_EX_SP_T(2,:)= underscore_removal_options;
        %% '/' slash introduction between b/s
        Index_B_S=find(strcmpi('B S',  OTC_OPTIONS_SECTION_EX_SP_T(2,:)));
        check=   OTC_OPTIONS_SECTION_EX_SP_T(2,Index_B_S);
        newStr = insertAfter(check,'B','/');
        OTC_OPTIONS_SECTION_EX_SP_T (2,Index_B_S)=  newStr;
        
        %% '/' introduction of call put
        Index_call_put=find(strcmpi('Call Put',  OTC_OPTIONS_SECTION_EX_SP_T(2,:)));
        check=  OTC_OPTIONS_SECTION_EX_SP_T (2,Index_call_put);
        newStr = upper(insertAfter(check,'Call','/'));
        OTC_OPTIONS_SECTION_EX_SP_T  (2,Index_call_put)=  newStr;
        OTC_OPTIONS_SECTION_EX_SP_T  (2,6)={'CALL / PUT'};
        %% Zero removals
        %% zeros replacement with '-'
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000')||strcmpi(v,'null')||startsWith(v,'0.00'), OTC_OPTIONS_SECTION_EX_SP_T  ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        aa='-';
        OTC_OPTIONS_SECTION_EX_SP_T  (index)=cellstr(aa);
        %% Zero replace with '-'
        remove_opt_rest=cellfun(@(v) strcmpi(v,'0'),       OTC_OPTIONS_SECTION_EX_SP_T ,'UniformOutput', false);
        index = find([remove_opt_rest{:}] == 1);
        opt_find='-';
        OTC_OPTIONS_SECTION_EX_SP_T(index)=cellstr(opt_find);
        title_row = OTC_OPTIONS_SECTION_EX_SP_T(1,:);
        title_table = Table(title_row);
        % calling table setting function
        da = formatting_optiontable(title_table);
        grp = TableColSpecGroup();
        grp.Span = title_table.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false),BorderCollapse('on'),HAlign('Right'),Bold,FontSize('0.15in')}; %% From Here we can manage the width of a Table. 0.7
        title_table.ColSpecGroups = grp;
        title_table.HAlign = 'Right';
        title_table.row(1).Style = {Color('Black'),Bold,FontSize('0.13in')};
        %append(OutPDFFile,title_table);
        h1 = Heading1('OTC OPTIONS');
        h1.Style = {PageBreakBefore,Color('Black'),FontSize('0.15in'),Bold};
        %h1.Style = [h1.Style,{PageBreakBefore,Bold}];
        append(OutPDFFile,h1);
        
        Filtered_OTC_OPTION_DATA= OTC_OPTIONS_SECTION_EX_SP_T(3:end,LOTS_INDEX:DR_CR_USD);
        output = Sep1000Str(Filtered_OTC_OPTION_DATA);
        
        
        %% Inbetween Comma fix.
        %         output(1,2)={'STRIKE'};
        %         output(1,13)={'MTM NC'};
        %         output(1,14)={'MTM USD'};
        %% Cell to char conversion
        
        idx_empty_eleven=find(cellfun(@isempty,output(:,11)));
        idx_empty_fourteen=find(cellfun(@isempty,output(:,14)));
        idx_empty_fifteen=find(cellfun(@isempty,output(:,15)));
        output(idx_empty_eleven,11)={'-'};
        output(idx_empty_fourteen,14)={'-'};
        output(idx_empty_fifteen,15)={'-'};
        %% Manipulated Data allocation.
        OTC_OPTIONS_SECTION_EX_SP_T(3:end,LOTS_INDEX:DR_CR_USD)  =output;
        OTC_OPTIONS_SECTION_EX_SP_T_FINAL=  OTC_OPTIONS_SECTION_EX_SP_T(2:end,:);
        t3=Table( OTC_OPTIONS_SECTION_EX_SP_T_FINAL );
        t3.row(1).Style = {Color('#974706'),RepeatAsHeaderRow(true),BorderCollapse('on'),HAlign('Right'),Bold,FontSize('0.13in')};
        t3.TableEntriesHAlign='right';
        for  i=1:t3.NRows
            for j=t3.NCols-5:t3.NCols
                if str2double(t3.entry(i,j).Children(1).Content)<0
                    t3.entry(i,j).Children(1).Color='red';
                    %                     t3.entry(i,j).Children.Content=num2str(abs(str2double(t3.entry(i,j).Children.Content)));
                    t3.entry(i,j).Children.Content=strrep((t3.entry(i,j).Children.Content),'-','');
                    t3.entry(i,j).Children.Content = strcat('(',t3.entry(i,j).Children.Content,')');
                    
                    
                    
                end
            end
        end
        
        t3.Style={Width('100%'),FontSize('0.13in'),HAlign('Right'),Bold};
        t3.Border='None';
        t3.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        
        grp.Span = t3.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false)}; %% From Here we can manage the width of a Table 0.7
        t3.ColSpecGroups = grp;
        t3.HAlign = 'right';
        t3.TableEntriesHAlign='right';
        
        %% Table appension Confirmation section
        append(OutPDFFile,t3);
        %% Formatting of the break lines depending upon various conditions.
        %% What happened if MTM_NC is there as well as else condition.
        MTM_NC=strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'MTM NC');
        MTM_NC_CHECK=find(strcmpi(OTC_OPTIONS_SECTION_EX_SP_T(2,:),'MTM NC'), 1);
        if (~isempty(MTM_NC_CHECK))
            %             if startsWith(Current_Sheet_Name,'ADP')|| startsWith(Current_Sheet_Name,'AAS')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             _________                                       ___________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para)
            %             elseif startsWith(Current_Sheet_Name,'AMJ')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  _________                                ___________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'VCC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'TEC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'SCH')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'OLC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'MIA')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'DPC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'MOM')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'TSE')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'VIT')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'COP')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'GFZ')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'AAC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            __________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'ABE')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'OCC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'OGC')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'SCA')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'TBM')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             elseif  startsWith(Current_Sheet_Name,'CIT')
            %                 para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            _________                                ______________________________________');
            %                 para.WhiteSpace = 'preserve';
            %                 append(OutPDFFile,para);
            %             else
            para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                 _______________                              _____________________________________________________');
            para.WhiteSpace = 'preserve';
            append(OutPDFFile,para);
            %             end
            
        else
            para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        _________                                           ____________________');
            para.WhiteSpace = 'preserve';
            append(OutPDFFile,para);
        end
        
        
        %% To Manage Total within lines
        OTC_OPTIONS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,1:end);
        OTC_OPTIONS_SECTION_REST=format_cell(OTC_OPTIONS_SECTION_REST,'%.2f');
        output = Sep1000Str( OTC_OPTIONS_SECTION_REST );
        
        OTC_OPTIONS_SECTION_REST=output;
        OTC_OPTIONS_SECTION_REST(1)=strrep(OTC_OPTIONS_SECTION_REST(1),',','');
        empty_cells_index= find(cellfun(@isempty,OTC_OPTIONS_SECTION_REST));
        OTC_OPTIONS_SECTION_REST(empty_cells_index)={' '};
        %         if(IncludeMtmNC==0)
        %             OTC_OPTIONS_SECTION_REST(1,19)=  OTC_OPTIONS_SECTION_REST(1,17);
        %             OTC_OPTIONS_SECTION_REST(1,17)={' '};
        %         end
        
        t4=Table(OTC_OPTIONS_SECTION_REST);
        %         t4.row(1).Style = {Width('100%'),FontSize('0.13in'),HAlign('Right'),Bold};
        %  p = Table(title_row);
        t4.Style={Width('100%'),FontSize('0.13in'),HAlign('Right'),Bold};
        t4.Border='None';
        t4.TableEntriesHAlign='Right';
        grp = TableColSpecGroup();
        grp.Span = t4.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = da;
        grp.Style = {ResizeToFitContents(false)}; %% From Here we can manage the width of a Table 0.7
        t4.ColSpecGroups = grp;
        t4.HAlign = 'right';
        t4.TableEntriesHAlign='right';
        %% Colours and other managements
        for  i=1:t4.NRows
            for j=t4.NCols-5:t4.NCols
                if str2double(t4.entry(i,j).Children(1).Content)<0
                    t4.entry(i,j).Children(1).Color='red';
                    %                     t4.entry(i,j).Children.Content=num2str(abs(str2double(t4.entry(i,j).Children.Content)));
                    t4.entry(i,j).Children.Content=strrep((t4.entry(i,j).Children.Content),'-','');
                    t4.entry(i,j).Children.Content = strcat('(',t4.entry(i,j).Children.Content,')');
                    
                    
                end
            end
        end
        
        %% Allignment management for AMJ
        
        
        
        
        
        
        %% AAS Management pick it by loop and adjust its size
        
        
        
        for  i=1:t4.NRows
            for j=t4.NCols-3:t4.NCols
                
                if str2double(t4.entry(i,j).Children(1).Content)<0
                    t4.entry(i,j).Children(1).Color='red';
                    t4.entry(i,j).Children.Content=num2str(abs(str2double(t4.entry(i,j).Children.Content)));
                    t4.entry(i,j).Children.Content=strrep((t4.entry(i,j).Children.Content),'-','');
                    t4.entry(i,j).Children.Content = strcat('(',t4.entry(i,j).Children.Content,')');
                    
                end
            end
        end
        
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %         %% Total Bold Mechanism
        Total_ROW_t=find(strcmpi('TOTAL',  OTC_OPTIONS_SECTION_REST(:,1)));
        FIRST_ROW_Total=t4.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),FontSize('0.13in'),Bold};
        
        append(OutPDFFile,t4);
        
        %% Horizontal Line appension
        hr4 = HorizontalRule();
        hr4.Border='single';
        hr4.Style={Width('100%')};
        %hr4.Style={OuterMargin('16.5in','0in','0in','0in')};
        append(OutPDFFile,hr4);
        %append(OutPDFFile,cell(1,1));
    end
    
    
    if (OTC_ACCUMULATORS_CHECK==1) % We can add this pagebreak object to other sections too as per the requirement.
        
        a=find(strcmpi('OTC ACCUMULATORS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        %% OTC_ACCUMULATORS_LOTS_PER_DAY_SIXDECIMAL.
        OTC_ACCUMULATORS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        
        %% New Code section
        firstt_section=  OTC_ACCUMULATORS_SECTION_EX_SP_T(1:2,:);
        index_long_lots=find(strcmpi( firstt_section(2,:),'LOTS_PER_DAY'));
        dr_cr_usd=find(strcmpi(firstt_section(2,:),'DR_CR_USD'));
        second_section=  OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end,:);
        filtered_data_acc=OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end,index_long_lots: dr_cr_usd);        
        output = Sep1000Str(   filtered_data_acc );
        
        OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end,index_long_lots:dr_cr_usd)=output;
        
        
        index_empty=find(cellfun(@isempty,second_section));
        req='-';
        second_section(index_empty)=cellstr(req);
        second_section_filter=second_section(1:end,7:end);
        barriertype_data=second_section_filter(:,6);
        %% OTC Accumulators fix of commas.
        second_section_ff=Sep1000Str(second_section_filter );
        second_section_ff(:,6)=barriertype_data;
        second_section(1:end,7:end)=  second_section_ff;
        OTC_ACCUMULATORS_SECTION_EX_SP_T    =[firstt_section;second_section];
        empty_index=find(strcmpi(    OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),''));
        OTC_ACCUMULATORS_SECTION_EX_SP_T  (:,empty_index)={' '};
        %% New Code for MTM_NC Section
        
        %         OTC_ACCUMULATORS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end);
        if (IncludeMtmNC==1)
            OTC_ACCUMULATORS_SECTION_EX_SP_T=  OTC_ACCUMULATORS_SECTION_EX_SP_T;
        elseif (IncludeMtmNC==1)
            MTM_NC_INDEX=find(strcmpi(  OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'MTM_NC'));
            OTC_ACCUMULATORS_SECTION_EX_SP_T(:,MTM_NC_INDEX)=[];
        end
        
        
        
        %% New functionality empty replacement with '-'
        %% BEW code FOR EMPTY CELL WITH '-' REPLACEMENT
        ACC1_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'ACC_LEVEL_1'));
        edsp_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'EDSP'));
        required_data=OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, [ACC1_INDEX edsp_INDEX] );
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal);
        OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, [ACC1_INDEX edsp_INDEX])=output;
        
        BARRIER_STRIKE_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'BARRIER_STRIKE'));
        required_data=OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, BARRIER_STRIKE_INDEX:end );
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal);
        OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, BARRIER_STRIKE_INDEX:end )=output;
        %         required_data=str2double(OTC_ACCUMULATORS_SECTION_EX_SP_T(:, NOTIONAL_QTY_LOTS_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str(  six_decimals );
        %         OTC_ACCUMULATORS_SECTION_EX_SP_T(index_numeric,NOTIONAL_QTY_LOTS_INDEX)=output;
        
        %         NOTIONAL_QTY_LOTS_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'NOTIONAL_QTY_LOTS'));
        %         required_data=str2double(OTC_ACCUMULATORS_SECTION_EX_SP_T(:, NOTIONAL_QTY_LOTS_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str(  six_decimals );
        %         OTC_ACCUMULATORS_SECTION_EX_SP_T(index_numeric,NOTIONAL_QTY_LOTS_INDEX)=output;
        
        %         ACC_LEFT_LOTS_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'ACC_LEFT_LOTS'));
        %         required_data=str2double(OTC_ACCUMULATORS_SECTION_EX_SP_T(:,  ACC_LEFT_LOTS_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str(  six_decimals );
        %         OTC_ACCUMULATORS_SECTION_EX_SP_T(index_numeric,  ACC_LEFT_LOTS_INDEX)=output;
        
        %         BARRIER_STRIKE_INDEX=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'BARRIER_STRIKE'));
        %          required_data=str2double(OTC_ACCUMULATORS_SECTION_EX_SP_T(:,BARRIER_STRIKE_INDEX));
        %          index_numeric=find(~isnan(required_data));
        %          Data_values=required_data(index_numeric);
        %          two_decimals=arrayfun(@(Data_values)  sprintf('%.2f',Data_values), Data_values, 'uniform', 0);
        %          output = Sep1000Str(   two_decimals );
        %          OTC_ACCUMULATORS_SECTION_EX_SP_T(index_numeric,  BARRIER_STRIKE_INDEX)=output;
        
        ACC_LEVEL_2_3_index=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'ACC_LEVEL_2_3'));
        required_data=OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, ACC_LEVEL_2_3_index );
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal );
        %           [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        %          ACC_LEVEL_2_3_index=find(strcmpi(OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'ACC_LEVEL_2_3'));
        %          required_data=str2double(OTC_ACCUMULATORS_SECTION_EX_SP_T(:,ACC_LEVEL_2_3_index));
        %          index_numeric=find(~isnan(required_data));
        %          Data_values=required_data(index_numeric);
        %          two_decimals=arrayfun(@(Data_values)  sprintf('%.2f',Data_values), Data_values, 'uniform', 0);
        %           output = Sep1000Str(   two_decimals );
        OTC_ACCUMULATORS_SECTION_EX_SP_T(3:end, ACC_LEVEL_2_3_index)=output;
        
        %% Underscore removal from headers
        underscore_removal_accumulators=cellfun(@(v) strrep(v,'_',' '),  OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:),'UniformOutput', false);
        underscore_removal_accumulators(10)={'ACC LEVEL 2/3'};
        underscore_removal_accumulators(7:8)={'LOTS (PER DAY)','NOTIONAL QTY (LOTS)'};
        underscore_removal_accumulators(14:15)={'ACC LEFT (LOTS)','ACC LEFT (DAYS)'};
        OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:)= underscore_removal_accumulators;
        
        %% '/' slash introduction between b/s
        Index_B_S=find(strcmpi('B S',  OTC_ACCUMULATORS_SECTION_EX_SP_T(2,:)));
        check=   OTC_ACCUMULATORS_SECTION_EX_SP_T(2,Index_B_S);
        newStr = insertAfter(check,'B','/');
        OTC_ACCUMULATORS_SECTION_EX_SP_T (2,Index_B_S)=  newStr;
        %% New section
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000')||startsWith(v,'0.00') ,   OTC_ACCUMULATORS_SECTION_EX_SP_T ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        OTC_ACCUMULATORS_SECTION_EX_SP_T (index)=cellstr(a);
        %% Manipulation
        remove_sed=cellfun(@(v) strcmpi(v,'0')|| strcmpi(v,'n,ull'),   OTC_ACCUMULATORS_SECTION_EX_SP_T ,'UniformOutput', false);
        index = find([remove_sed{:}] == 1);
        m_find='-';
        OTC_ACCUMULATORS_SECTION_EX_SP_T (index)=cellstr(m_find);
        %% '-' removal in OTC ACCUMULATORS
        %         udrs_rem=cellfun(@(v) strcmpi(v,'-'),   OTC_ACCUMULATORS_SECTION_EX_SP_T ,'UniformOutput', false);
        %         index = find([udrs_rem{:}] == 1);
        %         check_find={' '};
        %         OTC_ACCUMULATORS_SECTION_EX_SP_T (index)=cellstr(check_find);
        %% Here
        title_row = OTC_ACCUMULATORS_SECTION_EX_SP_T(1,:);
        title_table = Table(title_row);
        grp = TableColSpecGroup();
        grp.Span = title_table.NCols; %% Here the spacing between columns can be managed by
        % calling table setting function
        specs = fotmatting_acctable(title_table);
        grp.ColSpecs = specs;
        %         grp.Style = {Width('0.9in')}; %% From Here we can manage the width of a Table. 0.7
        title_table.ColSpecGroups = grp;
        title_table.HAlign = 'Right';
        title_table.row(1).Style = {Color('Black'),Bold,FontSize('0.15in') };
        %append(OutPDFFile,title_table);
        h1 = Heading1('OTC ACCUMULATORS');
        h1.Style = {PageBreakBefore,Color('Black'),FontSize('0.15in'),Bold};
        append(OutPDFFile,h1);
        %append(OutPDFFile,'');
        
        t5=Table(OTC_ACCUMULATORS_SECTION_EX_SP_T(2:end,:));
        %         t5.entry(5,:).Style = [t4.entry(5,:).Style {mlreportgen.dom.Width('0.75in')}];
        t5.row(1).Style = {Color('#974706'),RepeatAsHeaderRow(true),BorderCollapse('on'),HAlign('Right'),Bold,FontSize('0.13in')};
        t5.Style = {Bold,FontSize('0.13in') };
        for  i=1:t5.NRows
            for j=t5.NCols-3:t5.NCols
                if str2double(t5.entry(i,j).Children(1).Content)<0
                    t5.entry(i,j).Children(1).Color='red';
                    %                     t5.entry(i,j).Children.Content=num2str(abs(str2double(t5.entry(i,j).Children.Content)));
                    t5.entry(i,j).Children.Content=strrep((t5.entry(i,j).Children.Content),'-','');
                    t5.entry(i,j).Children.Content = strcat('(',t5.entry(i,j).Children.Content,')');
                    
                    
                    %% Red Color parenthesis implementation Code
                    %  t5.entry(i,j).Children.Content= strcat('(', t5.entry(i,j).Children.Content,')');
                end
            end
        end
        
        %% Structure Name Management
        %         for  i=5  %% Here
        %             for j=1:t5.NRows
        %                 t5.entry(j,i).Style = [t5.entry(j,i).Style {mlreportgen.dom.Width('4in')}];
        %             end
        %         end
        
        %% Table management for accumulators
        t5.Style={Width('100%'),FontSize('0.13in'),HAlign('Right'),ResizeToFitContents(false),Bold};
        t5.Border='None';
        t5.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t5.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs;
        %         grp.Style = {Width('1.98in')}; %% From Here we can manage the width of a Table. 0.9
        t5.ColSpecGroups = grp;
        t5.HAlign = 'right';
        
        %% Table appension Confirmation section
        append(OutPDFFile,t5)
        
        para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                        __________________________________           _________________________________');
        para.WhiteSpace = 'preserve';
        %         para.Style={Bold};
        append(OutPDFFile,para)
        
        
        %% To Manage Total within lines
        OTC_ACCUMULATORS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,1:end);
        
        required_data =format_cell(OTC_ACCUMULATORS_SECTION_REST(2:end),'%.2f');
        OTC_ACCUMULATORS_SECTION_REST(2:end) =required_data;
        output = Sep1000Str( OTC_ACCUMULATORS_SECTION_REST);
        
        
        output(1,1)={'TOTAL'};
        %
        
        
        
        OTC_ACCUMULATORS_SECTION_REST= output ;
        
        
        t6=Table(OTC_ACCUMULATORS_SECTION_REST);
        for  i=1:t6.NRows
            for j=t6.NCols-3:t6.NCols
                if str2double(t6.entry(i,j).Children(1).Content)<0
                    t6.entry(i,j).Children(1).Color='red';
                    %                     t6.entry(i,j).Children.Content=num2str(abs(str2double(t6.entry(i,j).Children.Content)));
                    t6.entry(i,j).Children.Content=strrep((t6.entry(i,j).Children.Content),'-','');
                    t6.entry(i,j).Children.Content = strcat('(',t6.entry(i,j).Children.Content,')');
                    
                    
                    %% red color commas fix
                    
                    %  t6.entry(i,j).Children.Content= strcat('(', t6.entry(i,j).Children.Content,')');
                end
            end
        end
        t6.Style={Width('100%'),FontSize('0.13in'),HAlign('Right'),ResizeToFitContents(false),Bold};
        t6.Border='None';
        t6.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t6.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs;
        %         grp.Style = {Width('1in')}; %% From Here we can manage the width of a Table. 0.7
        t6.ColSpecGroups = grp;
        t6.HAlign = 'right';
        %% Total Bold Mechanism
        Total_ROW_t=find(strcmpi('TOTAL', OTC_ACCUMULATORS_SECTION_REST(:,1)));
        FIRST_ROW_Total=t6.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),Bold};
        %% Table appension Confirmation section
        append(OutPDFFile,t6)
        
        %% Horizontal Line appension
        hr6 = HorizontalRule();
        hr6.Border='single';
        hr6.Style={Width('100%'),Bold};
        %hr6.Style={OuterMargin('16.5in','0in','0in','0in')};
        append(OutPDFFile,hr6);
        
    end
    
    
    if (OTC_FX_SWAPS_CHECK==1)
        
        a=find(strcmpi('OTC FX SWAPS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        OTC_FX_SWAPS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        a_replace = {'TRADE_DATE','TRADE_ID','VALUE_DATE','FX_PAIR','FIX_DATE','B/S','BUY_NOMINAL','BUY_PRICE',...
            'SELL_NOMINAL','SELL_PRICE','SETT_PRICE',' ',' ',' ',' ',' ',' ',' '...
            ,' ','MTM_NC','MTM_USD'};
        OTC_FX_SWAPS_SECTION_EX_SP_T(2,:)=  a_replace;
        %% highfen fix in OTC SWAPS section.
        idx_total_man=find(strcmpi( OTC_FX_SWAPS_SECTION_EX_SP_T(:,5),'TOTAL'));
        new_section_man= OTC_FX_SWAPS_SECTION_EX_SP_T(idx_total_man,6);
        idx_empty_new=find(cellfun(@isempty,new_section_man));
        new_section_man(idx_empty_new)={'-'};
        OTC_FX_SWAPS_SECTION_EX_SP_T(idx_total_man,6)= new_section_man;
        max_total=max(idx_total_man);
        val_cell=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,7));
        val_cell_eigth=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,8));
        val_cell_sixth=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,6));
        val_cell_ninth=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,9));
        val_cell_tenth=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,10));
        val_cell_eleven=char(OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,11));
        %% Fix did on 05-08-2019
        val_cell_miss=char(OTC_FX_SWAPS_SECTION_EX_SP_T(idx_total_man(1),6));
        
        if isempty(val_cell)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,7)={'-'};
        end
        if isempty( val_cell_eigth)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,8)={'-'};
        end
        if isempty( val_cell_sixth)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,6)={'-'};
        end
        if isempty( val_cell_ninth)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,9)={'-'};
        end
        if isempty( val_cell_tenth)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,10)={'-'};
        end
        if isempty( val_cell_miss)
            OTC_FX_SWAPS_SECTION_EX_SP_T(idx_total_man(1),idx_total_man(1)+1)={'-'};
        end
        if isempty( val_cell_eleven)
            OTC_FX_SWAPS_SECTION_EX_SP_T(max_total,11)={'-'};
        end
        %% BUY_NOMINAL_INDEX
        BUY_NOMINAL_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'BUY_NOMINAL'));
        
        %% MTM_USD_INDEX
        MTM_USD_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'MTM_USD'));
        
        %% REquired Data for commas introduction
        filtered_data=OTC_FX_SWAPS_SECTION_EX_SP_T(3:end,  BUY_NOMINAL_INDEX:MTM_USD_INDEX);
        % OTC_FX_SWAPS_SECTION_EX_SP_T(3:end, BUY_NOMINAL_INDEX:MTM_USD_INDEX)=  filtered_data;
        output = Sep1000Str( filtered_data);
        filtered_data= output;
        
        OTC_FX_SWAPS_SECTION_EX_SP_T(3:end,BUY_NOMINAL_INDEX:MTM_USD_INDEX)=  output;
        
        
        %% Later to be used
        OTC_FX_SWAPS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,:);
        output = Sep1000Str(OTC_FX_SWAPS_SECTION_REST );
        
        
        OTC_FX_SWAPS_SECTION_REST= output;
        OTC_FX_SWAPS_SECTION_REST(1,1)={'TOTAL'};
        %% To put commas in the section
        
        %% New Code for MTM_NC Section
        
        %         OTC_FX_SWAPS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-2,1:end);
        if (IncludeMtmNC==1)
            OTC_FX_SWAPS_SECTION_EX_SP_T=  OTC_FX_SWAPS_SECTION_EX_SP_T;
        elseif (IncludeMtmNC==1)
            MTM_NC_INDEX=find(strcmpi(  OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'MTM_NC'));
            OTC_FX_SWAPS_SECTION_EX_SP_T(:,  MTM_NC_INDEX)=[];
        end
        %% New Section addition
        [len_a,bre_a]=size( OTC_FX_SWAPS_SECTION_EX_SP_T );
        for i=3:len_a-3
            for j=7
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=8
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=9
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=10
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T{i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=11
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=20
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        
        for i=3:len_a-3
            for j=21
                if isempty( OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        %% Karthiks last mail fix.
        for i=len_a-2
            for j=7:8
                if isempty(OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        %% we have maintained a gap
        for i=len_a
            for j=6:1:8
                if isempty(OTC_FX_SWAPS_SECTION_EX_SP_T{i,j})
                    OTC_FX_SWAPS_SECTION_EX_SP_T {i,j}='-';
                end
            end
        end
        
        %% BUY PRICE SIX DECIMAL CREATOION.
        BUY_PRICE_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'BUY_PRICE')); %LONG(LOTS)        
   
        sell_PRICE_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'SELL_PRICE'));
        sett_PRICE_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'SEtt_PRICE'));
        
         required_data=OTC_FX_SWAPS_SECTION_EX_SP_T(3:end,[  BUY_PRICE_INDEX sell_PRICE_INDEX sett_PRICE_INDEX]);
        [required_data_4decimal,~]=format_cell(required_data,'%.4f');
        output = Sep1000Str(required_data_4decimal);
        OTC_FX_SWAPS_SECTION_EX_SP_T(3:end, [  BUY_PRICE_INDEX sell_PRICE_INDEX sett_PRICE_INDEX])=output;
        
        
          BUY_nominal_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'BUY_NOMINAL'));
          sell_nominal_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'SELL_NOMINAL'));
        required_data=OTC_FX_SWAPS_SECTION_EX_SP_T(3:end,[ BUY_nominal_INDEX  sell_nominal_INDEX]);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal);
        OTC_FX_SWAPS_SECTION_EX_SP_T(3:end, [  BUY_nominal_INDEX  sell_nominal_INDEX])=output;
        
        required_data=OTC_FX_SWAPS_SECTION_EX_SP_T(3:end,sett_PRICE_INDEX+1:end);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str(required_data_2decimal);
        OTC_FX_SWAPS_SECTION_EX_SP_T(3:end, sett_PRICE_INDEX+1:end)=output;
          
        %% SELL PRICE
        %
        SELL_PRICE_INDEX=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'SELL_PRICE'));
        
        %% Underscore removal from headers
        underscore_removal_FX_SWAPS=cellfun(@(v) strrep(v,'_',' '), OTC_FX_SWAPS_SECTION_EX_SP_T(2,:),'UniformOutput', false);
        OTC_FX_SWAPS_SECTION_EX_SP_T(2,:)=underscore_removal_FX_SWAPS;
        %% '/' slash introduction between b/s
        Index_B_S=find(strcmpi('B S',OTC_FX_SWAPS_SECTION_EX_SP_T(2,:)));
        check= OTC_FX_SWAPS_SECTION_EX_SP_T(2,Index_B_S);
        newStr = insertAfter(check,'B','/');
        OTC_FX_SWAPS_SECTION_EX_SP_T(2,Index_B_S)=  newStr;
        
        %% zeros removal
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000'),    OTC_FX_SWAPS_SECTION_EX_SP_T ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        OTC_FX_SWAPS_SECTION_EX_SP_T (index)=cellstr(a);
        
        %Here hedaer appension has been done
        title_row = OTC_FX_SWAPS_SECTION_EX_SP_T(1,:);
        title_table = Table(title_row);
        specs_fxswaps = formatting_fxswapstable(title_table);
        
        grp = TableColSpecGroup();
        grp.Span = title_table.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxswaps;
        grp.Style = {ResizeToFitContents(false),BorderCollapse('on')}; %% From Here we can manage the width of a Table. 0.7
        title_table.ColSpecGroups = grp;
        title_table.HAlign = 'left';
        title_table.row(1).Style = {Color('Black'),Bold,FontSize('0.15in'),Bold};
        %append(OutPDFFile,title_table);
        h1 = Heading1('OTC FX SWAPS');
        h1.Style = [h1.Style, {PageBreakBefore,Color('Black'),FontSize('0.13in'),Bold,ResizeToFitContents(false)}];
        append(OutPDFFile,h1);
        %append(OutPDFFile,'');
        %% Row to be repeated fetching
        row_tobe_repeated_fx_swaps = OTC_FX_SWAPS_SECTION_EX_SP_T(2,:);
        new_cell_array_fx_swaps = OTC_FX_SWAPS_SECTION_EX_SP_T(2:end,:);
        
        idx = strfind(new_cell_array_fx_swaps,'TOTAL'); %Finds the string 'TOTAL' in the data and creates a cell array with '2' in place of all 'TOTALS' and empty for all other data
        idx = cellfun(@(x)isequal(x,1),idx);% Here i replaced 1 by 2 Find the index of all the 2s - create a logical matrix with 0 where dummy data is present and 1 where the 'TOTAL' string is present
        [row_idx,col_idx] = find(idx); %Find the position of all the ones.
        %row_idx=row_idx-1;
        %new_cell_array(row_idx(1:end-1)-1,:)=[];
        new_cell_array_fx_swaps(row_idx-1,:)=[];
        idx = strfind(new_cell_array_fx_swaps,'TOTAL');
        idx = cellfun(@(x)isequal(x,1),idx);
        [row_idx,col_idx] = find(idx);
        % row_idx=row_idx-1;
        [num_rows,num_columns] = size(new_cell_array_fx_swaps);
        t= Table(num_columns);
        %% Table style manipulations
        t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),Bold};
        t.Border='None';
        t.TableEntriesHAlign='Right';
        
        %% Page size manipulation (make page settings after every table)
        
        pagesetup(OutPDFFile);
        
        grp = TableColSpecGroup();
        grp.Span = t.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxswaps;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
        %         grp.TableEntriesHAlign='right';
        t.ColSpecGroups = grp;
        
        i = 1; % Modified
        %% Here How to manage the required Format with Total.
        while(i<=num_rows) % Modified
            if(ismember(i,row_idx))
                %                 check=length(row_idx);
                %                 loop_length=(num_rows-check)+1;
                %                 new_cell_array(i-1,:)=[];
                append(OutPDFFile,t); % We are appending Just atable.
                para = Paragraph('                                                                                                                                                                                     _________________________________________________________________________________________________                                                                                                                                           _____________________________________');
                %                 new_cell_array(i-1,:)=[];
                
                para.WhiteSpace = 'preserve';
                %                 para.Bold=1;
                append(OutPDFFile,para); % Under Specified Columns we have added a line by making white space preserved hard coding
                
                %% We Have Fixed the Table formating using table column specification groups
                t = Table(num_columns);
                t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),BorderCollapse('on'),Bold}; %% This is table contents under OTC SWAPS
                t.Border='None';
                t.TableEntriesHAlign='Right';
                
                
                %% Table Column Seperation
                grp = TableColSpecGroup();
                grp.Span = t.NCols; %% Here the spacing between columns can be managed by
                grp.ColSpecs = specs_fxswaps;
                %                 grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
                %                 grp.TableEntriesHAlign='Right';
                t.ColSpecGroups = grp;
                %                 t.HAlign = 'left';
                %                 t.Width='100%';
                %                 t.TableEntriesHAlign='right';
                
                %% Now in Table Having specified Columns we have added row this is the way of creating Table By scratch
                row = TableRow(); % We have created one row outside the loop.
                for k=1:num_columns % For one row it will access all number of columns.
                    entry = TableEntry(); % all Table entries will be created
                    %                     entry.Style={HAlign('right')};
                    
                    %entry.Style={HAlign('Right')};
                    %                     if (i==row_idx-1)
                    %                         new_cell_array{i,:}=[];
                    %                     end
                    
                    if(strcmp(new_cell_array_fx_swaps{i,k},'TOTAL')) % wherever the Total is there remove that Total
                        new_cell_array_fx_swaps{i,k} = '   ';
                        %  new_cell_array(i,:)=[];
                    end
                    
                    append(entry,new_cell_array_fx_swaps{i,k}); % Here we have appended entry
                    
                    
                    
                    
                    %% Red Colour OTC SWAPS MANIPULATION
                    if ((k==num_columns-1) || (k==num_columns))
                        if(str2double(new_cell_array_fx_swaps{i,k})<0)
                            entry.Children.Color = 'red';
                            entry.Children.Content=strrep((entry.Children.Content),'-','');
                            entry.Children.Content = strcat('(',entry.Children.Content,')');
                            
                            
                            
                        end
                    end
                    
                    
                    append(row,entry);
                end
                append(t,row);
                
                append(OutPDFFile,t);
                
                %append(OutPDFFile,'');
                
                if (i==num_rows)
                    break;
                end
                %                   if (i==loop_length)
                %                     break;
                %                  end
                
                %% New section
                i = i+1;
                
                if(find((i-1)==row_idx) ~= length(row_idx))% Added
                    
                    t = Table(num_columns);
                    t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
                    t.Border='None';
                    t.TableEntriesHAlign='Right';
                    %                     t.HAlign = 'Right';
                    %                     t.Width='100%';
                    %                    % t.TableEntriesHAlign='Right';
                    %% Append the row to be repeated
                    row = TableRow();
                    row.Style = {RepeatAsHeaderRow(true),Color('#974706')};
                    % row.Entries(7).Style=[   row.Entries(7).Style {mlreportgen.dom.Width('1in')}];
                    
                    for k = 1:num_columns
                        entry = TableEntry();
                        %                         entry.Style={HAlign('right')};
                        append(entry,row_tobe_repeated_fx_swaps{1,k});
                        
                        append(row,entry);
                    end
                    append(t,row);
                    
                    %                     t.entry(end,6).Style = [t.entry(end,6).Style {mlreportgen.dom.Width('0.5in')}];
                    
                    %% Page size manipulation
                    pagesetup(OutPDFFile);
                    
                    %% Width Control Code of columns
                    grp = TableColSpecGroup();
                    grp.Span = t.NCols; %% Here the spacing between columns can be managed by
                    grp.ColSpecs = specs_fxswaps;
                    %                     grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
                    %                     grp.TableEntriesHAlign='right';
                    t.ColSpecGroups = grp;
                    t.HAlign = 'left';
                    t.Width='100%';
                    %                     t.TableEntriesHAlign='right';
                    % t.entry(end,6).Style = [t.entry(end,7).Style {mlreportgen.dom.Width('0.08in')}];
                    
                end
            end
            row = TableRow();
            %            row.Style={HAlign('Right'),Width('100%')};
            for j = 1:num_columns
                entry = TableEntry();
                
                if str2double(new_cell_array_fx_swaps{i,j})<0
                    
                    if(strcmp(new_cell_array_fx_swaps{i,j},'TOTAL'))
                        new_cell_array_fx_swaps{i,j} = '   ';
                        % new_cell_array(i-1,:)=[];
                    end
                    % rajashekhar added
                    append(entry, strrep(new_cell_array_fx_swaps{i,j},'-',''));
                    if j>(num_columns-2) % Added
                        entry.Children.Color='red';
                        %                         entry.Children.Content=num2str(abs(str2double(string(entry.Children.Content))));
                        
                        entry.Children.Content = strcat('(',entry.Children.Content,')');
                        
                        
                    end
                else
                    if(strcmp(new_cell_array_fx_swaps{i,j},'TOTAL'))
                        new_cell_array{i,j} = '   ';
                        % new_cell_array(i,:)=[];
                    end
                    append(entry, new_cell_array_fx_swaps{i,j});
                end
                
                %% Bold and Coloring Mechanism of OTC SWAPS
                if(i==1)
                    entry.Children.Color = '#974706';
                    
                end
                
                
                append(row,entry);
            end
            
            append(t,row);
            i = i+1; % Modified
        end
        
        
        %% New Section for subtotal manipulation.
        
        %% SIZE FIX
        %% Page size manipulation and coloring for proper formating
        t.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
        t.Border='None';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxswaps;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
        %         grp.TableEntriesHAlign='right';
        t.ColSpecGroups = grp;
        %         t.HAlign = 'left';
        %         t.TableEntriesHAlign='right';
        
        
        %% Table appension Confirmation section
        if(find((i-1)==row_idx) ~= length(row_idx)) % Added
            grp = TableColSpecGroup();
            grp.Span = t.NCols; %% Here the spacing between columns can be managed by
            grp.ColSpecs = specs_fxswaps;
            %             grp.Style = {Width('1.5in'),HAlign('right')}; %% From Here we can manage the width of a Table 0.7
            %             grp.TableEntriesHAlign='right';
            t.ColSpecGroups = grp;
            
        end
        % If user wants GRAND_TOTAL after individual total..uncopmment below section of code.
        %% Horizontal Line appension
        hr1 = HorizontalRule();
        hr1.Border='single';
        hr1.Style={Width('100%'),Bold};
        %hr1.Style={OuterMargin('3.2in','7.3in','0in','0in')};
        append(OutPDFFile,hr1);
        
        % calculating buy nominal total and sell nominal total
        %         TOTAL_IDX_TOTAL=find(strcmpi(OTC_FX_SWAPS_SECTION_EX_SP_T(:,5),'TOTAL'));
        otc_fx_swaps_idx=find(strcmpi(Current_Client_Info(:,1),'OTC FX SWAPS'));
        total_idx_req=find( Total_Index>otc_fx_swaps_idx,1);
        req_idx= Total_Index(total_idx_req);
        
        Data_fetch_for_sum=Current_Client_Info(req_idx,:);
        %         bb=strcmpi(Data_fetch_for_sum,'-');
        %         buy_nominal_sum=nansum(str2double(Data_fetch_for_sum(~bb)));
        buy_nominal_sum=Data_fetch_for_sum(7);
        if str2double( buy_nominal_sum)==0 || strcmp( buy_nominal_sum,'-')
            S_buy_nominal_sum={'-'};
        else
            buy_nominal_sum=sprintf('%.2f',str2double(buy_nominal_sum));
            S_buy_nominal_sum = Sep1000Str(cellstr(buy_nominal_sum));
        end
        
        Data_fetch_for_sum1=Current_Client_Info( req_idx,9);
        %         bb1=strcmpi(Data_fetch_for_sum1,'-');
        %         sell_nominal_sum1=nansum(str2double(Data_fetch_for_sum1(~bb1)));
        if str2double(Data_fetch_for_sum1)==0 || strcmp(Data_fetch_for_sum1,'-')
            S_sell_nominal_sum1={'-'};
        else
            sell_nominal_sum1=sprintf('%.2f',  str2double(Data_fetch_for_sum1));
            S_sell_nominal_sum1 = Sep1000Str(cellstr(sell_nominal_sum1));
            
        end
        Data_fetch_for_mtm_usd=Current_Client_Info(req_idx,21);
        if str2double(Data_fetch_for_mtm_usd)==0 ||  strcmp(Data_fetch_for_mtm_usd,'-')
            total_sell_mtm={'-'};
        else
            total_mtm_usd=sprintf('%.2f',  str2double( Data_fetch_for_mtm_usd));
            total_sell_mtm = Sep1000Str(cellstr(total_mtm_usd));
            
        end
        % end of section buy nominal total and sell nominal total
        
        [len_Tot Bre_Tot]=size(  Data_fetch_for_sum);
        Data_fetch_for_sum  =cell(1,Bre_Tot);
        Data_fetch_for_sum (1,1)=   Data_fetch_for_sum(1,1);
        Data_fetch_for_sum (1,7)=  cellstr(S_buy_nominal_sum);
        Data_fetch_for_sum(1,9)=  cellstr(S_sell_nominal_sum1);
        Data_fetch_for_sum(1,21)=  cellstr(  total_sell_mtm);
        Empty_cell_indexes=find(cellfun(@isempty, Data_fetch_for_sum ));
        Data_fetch_for_sum (Empty_cell_indexes)={' '};
        if ( Data_fetch_for_sum{1,end-1}== '-')
            Data_fetch_for_sum(1,Bre_Tot)= Data_fetch_for_sum(1,end);
            %OTC_SWAPS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,end);
        else
            Data_fetch_for_sum (1,end-1)=  Data_fetch_for_sum(1,end-1);
            Data_fetch_for_sum (1,end)= Data_fetch_for_sum (1,end);
        end
        
        Data_fetch_for_sum (1,1)={'TOTAL'};
        Empty_cell_indexes_gt=find(cell2mat(cellfun(@(x) strcmpi(x,'NaN') || strcmpi(x,'0.00')||strcmpi(x,'-0.00')  ,Data_fetch_for_sum,'uni',0)));
        Data_fetch_for_sum (Empty_cell_indexes_gt)={'-'};
        
        
        %% Column space management.
        t2=Table( Data_fetch_for_sum );
        t2.HAlign='left';
        
        %% BUY NOMINAL AND CELL NOMINAL MANIPULATIONS
        
        %% Color Manipulation Section
        %% Red Color Manipulation
        for  i2=1:t2.NRows
            for j2=t2.NCols-3:t2.NCols
                if str2double(t2.entry(i2,j2).Children(1).Content)<0
                    t2.entry(i2,j2).Children(1).Color='red';
                    %                      t2.entry(i2,j2).Style={HAlign('Right')};
                    %                     t2.entry(i2,j2).Children.Content=num2str(abs(str2double(string(t2.entry(i2,j2).Children.Content))));
                    %                        t2.entry(i2,j2).Children.Content=strrep(Data_fetch_for_sum{i2,j2},'-','');
                    t2.entry(i2,j2).Children.Content=strrep(t2.entry(i2,j2).Children.Content,'-','');
                    %                      t2.entry(i2,j2).Children.Content=strrep(string(t2.entry(i2,j2).Children.Content),'-','');
                    t2.entry(i2,j2).Children.Content = strcat('(',t2.entry(i2,j2).Children.Content,')');
                    
                end
            end
        end
        
        %%%
        t2.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),OuterMargin('0pt','0pt','0pt','0pt'),BorderCollapse('on'),Bold};
        t2.Border='None';
        t2.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t2.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxswaps;
        %         grp.Style = {Width('1.5in'),ResizeToFitContents(true)}; %% From Here we can manage the width of a Table. 0.7
        t2.ColSpecGroups = grp;
        t2.HAlign = 'left';
        %         t2.TableEntriesHAlign='right';
        %
        %% Total Bold Mechanism
        Total_ROW_t=strcmpi('TOTAL',  Data_fetch_for_sum(:,1));
        FIRST_ROW_Total=t2.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),Bold};
        
        
        %% Table appension Confirmation section
        append(OutPDFFile,t2);
        
    end
    
    %% OTC FX OPTIONS SECTION INTRODUCTION
    if (OTC_FX_OPTIONS_CHECK==1)
        
        a=find(strcmpi('OTC FX OPTIONS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        OTC_FX_OPTIONS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space,1:end);      
        backup= OTC_FX_OPTIONS_SECTION_EX_SP_T;
        Buy_nominal_idx=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'NOTIONAL (USD)'));
        idx_dr_cr_usd_fx_op=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'(DR.)/CR.USD'));
        
        Filtered_data_for_fx_options= OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end-1, Buy_nominal_idx: idx_dr_cr_usd_fx_op); %% Make this claus dynamic.
        output = Sep1000Str(  Filtered_data_for_fx_options);
        
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end-1,6:21)=output;
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end-1,7)= backup(3:end-1,7);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end-1,9)= backup(3:end-1,9);
        OTC_FX_OPTIONS_SECTION_EX_SP_T= OTC_FX_OPTIONS_SECTION_EX_SP_T(1:end-1,:);
        %% New code section
        index_empty=find(cellfun(@isempty,OTC_FX_OPTIONS_SECTION_EX_SP_T));
        req='-';
        OTC_FX_OPTIONS_SECTION_EX_SP_T(index_empty)=cellstr(req);
        Empty_cell_indexes_gt=find(cell2mat(cellfun(@(x) strcmpi(x,'NaN')||strcmpi(x,'0.00') , OTC_FX_OPTIONS_SECTION_EX_SP_T,'uni',0)));
        OTC_FX_OPTIONS_SECTION_EX_SP_T(Empty_cell_indexes_gt)={'-'};
        %% New Code for MTM_NC Section
        
        
        %         OTC_FX_OPTIONS_SECTION_EX_SP_T=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end);
        if (IncludeMtmNC==1)
            OTC_FX_OPTIONS_SECTION_EX_SP_T= OTC_FX_OPTIONS_SECTION_EX_SP_T;
        elseif (IncludeMtmNC==1)
            MTM_NC_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'MTM_NC'));
            OTC_FX_OPTIONS_SECTION_EX_SP_T(:,  MTM_NC_INDEX)=[];
        end
        
        %% Underscore removal Code
        underscore_removal_FX_OPTIONS=cellfun(@(v) strrep(v,'_',' '), OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'UniformOutput', false);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:)=underscore_removal_FX_OPTIONS;
        
        %% '/' slash introduction between b/s
        Index_B_S=find(strcmpi('B S', OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:)));
        check=  OTC_FX_OPTIONS_SECTION_EX_SP_T(2,Index_B_S);
        newStr = insertAfter(check,'B','/');
        OTC_FX_OPTIONS_SECTION_EX_SP_T (2,Index_B_S)=  newStr;
        
        %% '/' introduction of call put
        Index_call_put=find(strcmpi('Call Put', OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:)));
        check=  OTC_FX_OPTIONS_SECTION_EX_SP_T(2,Index_call_put);
        newStr = insertAfter(check,'Call','/');
        OTC_FX_OPTIONS_SECTION_EX_SP_T(2,Index_call_put)=  newStr;
        
        %% Zero removal
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000'),   OTC_FX_OPTIONS_SECTION_EX_SP_T  ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        OTC_FX_OPTIONS_SECTION_EX_SP_T  (index)=cellstr(a);
        
        %% TRADED PRICE IN SIX DECIMAL
        TRADE_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'TRADED PRICE (%)'));
        %         required_data=str2double(OTC_FX_OPTIONS_SECTION_EX_SP_T(:,TRADE_INDEX));
        %         index_numeric=find(~isnan(required_data));
        %         Data_values=required_data(index_numeric);
        %         four_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
        %         output = Sep1000Str( four_decimals);
        %         OTC_FX_OPTIONS_SECTION_EX_SP_T(index_numeric,TRADE_INDEX)=output;
        
        required_data=OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,TRADE_INDEX);
        [required_data_6decimal,~]=format_cell(required_data,'%.6f');
        output = Sep1000Str( required_data_6decimal);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,TRADE_INDEX)=output;
        
        usd_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'NOTIONAL (USD)'));
        required_data=OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,usd_INDEX);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str( required_data_2decimal);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,usd_INDEX)=output;
        %% TRADE PRICE
        %% STRIKE IN FOUR DECIMAL
        STRIKE_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'STRIKE'));
        BARRIER_STRIKE_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'BARRIER STRIKE'));
        PTAX_INDEX=find(strcmpi(OTC_FX_OPTIONS_SECTION_EX_SP_T(2,:),'PTAX ASK'));
        
        required_data=OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,[STRIKE_INDEX BARRIER_STRIKE_INDEX PTAX_INDEX]);
        [required_data_4decimal,~]=format_cell(required_data,'%.4f');
        output = Sep1000Str( required_data_4decimal);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,[STRIKE_INDEX BARRIER_STRIKE_INDEX PTAX_INDEX])=output;
        
        required_data=OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,PTAX_INDEX+4:end);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        output = Sep1000Str( required_data_2decimal);
        OTC_FX_OPTIONS_SECTION_EX_SP_T(3:end,PTAX_INDEX+4:end)=output;
        %% Formatting manipulation in terms of table
        title_row = OTC_FX_OPTIONS_SECTION_EX_SP_T(1,:);
        title_table = Table(title_row);
        % calling table settings
        specs_fxoption = formatting_fxoptiontable(title_table);
        grp = TableColSpecGroup();
        grp.Span = title_table.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxoption;
        grp.Style = {ResizeToFitContents(false),BorderCollapse('on')}; %% From Here we can manage the width of a Table. 0.7
        title_table.ColSpecGroups = grp;
        title_table.HAlign = 'left';
        title_table.row(1).Style = {Color('Black'),Bold,FontSize('0.15in'),Bold};
        %append(OutPDFFile,title_table);
        h1 = Heading1('OTC FX OPTIONS');
        h1.Style = [h1.Style, {PageBreakBefore,Color('Black'),FontSize('0.15in'),Bold,ResizeToFitContents(false)}];
        append(OutPDFFile,h1);
        
        
        OTC_FX_OPTION_EX_TITTLE_TOTAL=OTC_FX_OPTIONS_SECTION_EX_SP_T(2:end,:);
        
        t7=Table(OTC_FX_OPTION_EX_TITTLE_TOTAL);
        t7.row(1).Style = {Color('#974706'),RepeatAsHeaderRow(true),ResizeToFitContents(false),OuterMargin('0pt','0pt','0pt','0pt'),Bold,FontSize('0.13in')};
        for  i=1:t7.NRows
            for j=t7.NCols-2:t7.NCols
                if str2double(t7.entry(i,j).Children(1).Content)<0
                    t7.entry(i,j).Children(1).Color='red';
                    %                     t7.entry(i,j).Children(1).Content=num2str(abs(str2double(t7.entry(i,j).Children(1).Content)));
                    t7.entry(i,j).Children(1).Content=strrep((t7.entry(i,j).Children(1).Content),'-','');
                    t7.entry(i,j).Children(1).Content = strcat('(',t7.entry(i,j).Children(1).Content,')');
                    
                end
            end
        end
        
        t7.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),Bold};
        t7.Border='None';
        t7.TableEntriesHAlign='Right';
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t7.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxoption;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table. 0.7
        t7.ColSpecGroups = grp;
        t7.HAlign = 'left';
        
        %% Table appension Confirmation section
        append(OutPDFFile,t7);
        
        %% Break line appension in OTC FX OPTIONS
        if (IncludeMtmNC==1)
            para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ______________              __________________________________');
            para.WhiteSpace = 'preserve';
        else
            para = Paragraph('                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ______________                    _________________________________');
            para.WhiteSpace = 'preserve';
        end
        append(OutPDFFile,para)
        
        %% To Manage Total within lines
        OTC_FX_OPTIONS_SECTION_REST=Overall_Current_Client_Data_Final(dynamic_space,1:end);
        required_data = OTC_FX_OPTIONS_SECTION_REST(:,5:end);
        [required_data_2decimal,~]=format_cell(required_data,'%.2f');
        OTC_FX_OPTIONS_SECTION_REST(:,5:end)=required_data_2decimal;
        output = Sep1000Str(  OTC_FX_OPTIONS_SECTION_REST );
        
        output(1,1)={'TOTAL'};
        
        OTC_FX_OPTIONS_SECTION_REST=output;
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.00')||startsWith(v,'-0.00'),   OTC_FX_OPTIONS_SECTION_REST  ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        OTC_FX_OPTIONS_SECTION_REST  (index)=cellstr(a);
        
        t8=Table(OTC_FX_OPTIONS_SECTION_REST);
        for  i=1:t8.NRows
            for j=t8.NCols-3:t8.NCols
                if str2double(t8.entry(i,j).Children(1).Content)<0
                    t8.entry(i,j).Children(1).Color='red';
                    t8.entry(i,j).Children.Content=strrep((t8.entry(i,j).Children.Content),'-','');
                    %                     t8.entry(i,j).Children.Content=num2str(abs(str2double(t8.entry(i,j).Children.Content)));
                    t8.entry(i,j).Children.Content = strcat('(',t8.entry(i,j).Children.Content,')');
                    
                end
            end
        end
        t8.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),Bold};
        t8.Border='None';
        t8.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t8.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_fxoption;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table. 0.7
        t8.ColSpecGroups = grp;
        t8.HAlign = 'left';
        t8.TableEntriesHAlign='Right';
        % Table appension Confirmation section
        %% Total Bold Mechanism
        Total_ROW_t=find(strcmpi('TOTAL', OTC_FX_OPTIONS_SECTION_REST(:,1)));
        FIRST_ROW_Total=t8.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),Bold};
        append(OutPDFFile,t8)
        
        %% Horizontal Line appension
        hr8 = HorizontalRule();
        hr8.Border='single';
        hr8.Style={Width('100%')};
        %hr8.Style={OuterMargin('16.5in','0in','0in','0in')};
        
        append(OutPDFFile,hr8);
        %append(OutPDFFile,cell(1,1));
    end
    % New SEction addition
    if (KC_COMPOS_CHECK==1)
        
        a=find(strcmpi('KC/USDBRL Compo SWAPS',Overall_Current_Client_Data_Final(:,1)));  %% Position.
        Total_Index=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1))); %% Declare Globally
        d=Total_Index-a;
        e=find(d>0,1);
        dynamic_space=a+d(e);
        
        KC_COMPOS_SECTION_EX_SP_T_raw=Overall_Current_Client_Data_Final(a:dynamic_space,1:end);
        idx_long_kc_compos=find(strcmpi( KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'LONG(LOTS)'));
        idx_mtm_usd_kc_compos=find(strcmpi( KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'MTM_USD'));
        
        %% New Section introduction
        firstt_section=    KC_COMPOS_SECTION_EX_SP_T_raw(1:2,:);
        second_section=    KC_COMPOS_SECTION_EX_SP_T_raw(3:end,:); %% second section ka same dimension fetch karna hai.
        filtered_data_second_section_kc=second_section(1:end,idx_long_kc_compos:   idx_mtm_usd_kc_compos);
        output = Sep1000Str(   filtered_data_second_section_kc);
        
        %%
        second_section(1:end,idx_long_kc_compos:idx_mtm_usd_kc_compos)=output;
        index_empty=find(cellfun(@isempty,second_section));
        req='-';
        second_section(index_empty)=cellstr(req);
        KC_COMPOS_SECTION_EX_SP_T_raw =[firstt_section;second_section];
        empty_index=find(strcmpi(  KC_COMPOS_SECTION_EX_SP_T_raw (2,:),''));
        KC_COMPOS_SECTION_EX_SP_T_raw  (:,empty_index)={' '};
        %% New Code for MTM_NC Section
        
        if (IncludeMtmNC==1)
            KC_COMPOS_SECTION_EX_SP_T_raw=    KC_COMPOS_SECTION_EX_SP_T_raw;
        elseif (IncludeMtmNC==0)
            MTM_NC_INDEX=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'MTM_NC'));
            KC_COMPOS_SECTION_EX_SP_T_raw (:,  MTM_NC_INDEX)=[];
        end
        
        %% UNderscore removal section
        
        underscore_removal_options=cellfun(@(v) strrep(v,'_',' '),  KC_COMPOS_SECTION_EX_SP_T_raw (2,:),'UniformOutput', false);
        KC_COMPOS_SECTION_EX_SP_T_raw (2,:)= underscore_removal_options;
        
        %% SIX DIGIT LOTS
%         LONG_LOTS=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'LONG(LOTS)')); %LONG(LOTS) %SHORT(LOTS)
%         required_data=str2double(KC_COMPOS_SECTION_EX_SP_T_raw(:,LONG_LOTS));
%         index_numeric=find(~isnan(required_data));
%         Data_values=required_data(index_numeric);
%         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
%         output = Sep1000Str( six_decimals);
%         KC_COMPOS_SECTION_EX_SP_T_raw(index_numeric,BUY_PRICE_INDEX)=output;
        
         usdbrl_edspindex=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'USDBRL EDSP')); 
         required_data=KC_COMPOS_SECTION_EX_SP_T_raw(3:end,usdbrl_edspindex);
         [required_data_4decimal,~]=format_cell(required_data,'%.4f');
          output = Sep1000Str(required_data_4decimal);
          KC_COMPOS_SECTION_EX_SP_T_raw(3:end, usdbrl_edspindex)=output;

        
          KC_EDSP_index=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'KC EDSP US c/lb')); 
          array=KC_EDSP_index: 1:21;
          id_array=find(array==usdbrl_edspindex);
          array(id_array)=[];
          required_data=KC_COMPOS_SECTION_EX_SP_T_raw(3:end,array);
          [required_data_2decimal,~]=format_cell(required_data,'%.2f');
          output = Sep1000Str(required_data_2decimal);
          KC_COMPOS_SECTION_EX_SP_T_raw(3:end, array)=output;
          
          
       
        %% SHORT LOTS
%         SHORT_LOTS=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(2,:),'SHORT(LOTS)')); %LONG(LOTS) %SHORT(LOTS)
%         required_data=str2double(KC_COMPOS_SECTION_EX_SP_T_raw(:, SHORT_LOTS));
%         index_numeric=find(~isnan(required_data));
%         Data_values=required_data(index_numeric);
%         six_decimals=arrayfun(@(Data_values)  sprintf('%.6f',Data_values), Data_values, 'uniform', 0);
%         output = Sep1000Str( six_decimals);
%         KC_COMPOS_SECTION_EX_SP_T_raw(index_numeric,SHORT_LOTS)=  output;
        
        %% Zero removal
        zeros_remove_swaps=cellfun(@(v) startsWith(v,'0.000'),     KC_COMPOS_SECTION_EX_SP_T_raw ,'UniformOutput', false);
        index = find([ zeros_remove_swaps{:}] == 1);
        a='-';
        KC_COMPOS_SECTION_EX_SP_T_raw  (index)=cellstr(a);
        
        %% Table Format manipulation using Table commands
        title_row =  KC_COMPOS_SECTION_EX_SP_T_raw(1,:);
        title_table = Table(title_row);
        specs_kcta = formatting_kctable(title_table);
        grp = TableColSpecGroup();
        grp.Span = title_table.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_kcta ;
        grp.Style = {ResizeToFitContents(false)}; %% From Here we can manage the width of a Table. 0.7
        title_table.ColSpecGroups = grp;
        title_table.HAlign = 'left';
        title_table.row(1).Style = {Color('Black'),Bold,FontSize('0.15in')};
        %append(OutPDFFile,title_table);
        h1 = Heading1('KC/USDBRL Compo SWAPS');
        h1.Style = [h1.Style, {PageBreakBefore,Color('Black'),FontSize('0.15in'),Bold,ResizeToFitContents(false)}];
        append(OutPDFFile,h1);
        % append(OutPDFFile,'');  %% Focus on this space
        %% Removal of slashes
        Total_idx=find(strcmpi(KC_COMPOS_SECTION_EX_SP_T_raw(:,1),'TOTAL'));
        KC_COMPOS_SECTION_EX_SP_T_raw(Total_idx,2)={''};
        KC_COMPOS_SECTION_EX_SP_T_raw(Total_idx,3)={''};
        KC_COMPOS_SECTION_EX_SP_T_raw(Total_idx,4)={''};
        %% Red color implementation
        t3=Table( KC_COMPOS_SECTION_EX_SP_T_raw(2:end-2,:));
        %t3.Style=[t3.Style,{PageBreakBefore}]; %% Edited Section For Page Break.
        t3.row(1).Style = {Color('#974706'),RepeatAsHeaderRow(true),OuterMargin('0pt','0pt','0pt','0pt'),Bold,FontSize('0.13in')};
        for  i=1:t3.NRows
            for j=t3.NCols-2:t3.NCols
                if str2double(t3.entry(i,j).Children(1).Content)<0
                    t3.entry(i,j).Children(1).Color='red';
                    %                     t3.entry(i,j).Children.Content=num2str(abs(str2double(t3.entry(i,j).Children.Content)));
                    t3.entry(i,j).Children.Content=strrep((t3.entry(i,j).Children.Content),'-','');
                    t3.entry(i,j).Children.Content = strcat('(',t3.entry(i,j).Children.Content,')');
                    
                end
            end
        end
        
        t3.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),ResizeToFitContents(false),Bold};
        t3.Border='None';
        t3.TableEntriesHAlign='Right';
        
        
        %% Page Size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t3.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_kcta ;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
        t3.ColSpecGroups = grp;
        t3.HAlign = 'left';
        
        %% Table appension Confirmation section
        append(OutPDFFile,t3);
        
        %% Breaked Line Manipulation Section
        para = Paragraph('                                                                                                                                _________________________________________________________________________________________________________________________________                                                                                                                                                                   __________________');
        para.WhiteSpace = 'preserve';
        append(OutPDFFile,para)
        
        %% Sum allocation for KC Compos section karthiks requirement
        LONG_LOTS_INDEX=find(strcmpi('LONG(LOTS)',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        SUM_LONG_LOTS=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end,  LONG_LOTS_INDEX)));   
        
        if (SUM_LONG_LOTS==0)
            SUM_LONG_LOTS={'-'};
        else
         SUM_LONG_LOTS=arrayfun(@( SUM_LONG_LOTS)  sprintf('%.6f', SUM_LONG_LOTS),  SUM_LONG_LOTS, 'uniform', 0);
        end
        
        %% AVG LONG PRICE LOTS
        AVG_LONG_PRICE_INDEX=find(strcmpi('AVG LONG PRICE RS/BAG',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        AVG_LONG_PRICE_LOTS=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end, AVG_LONG_PRICE_INDEX )));
        
        
        if (AVG_LONG_PRICE_LOTS==0)
            AVG_LONG_PRICE_LOTS={'-'};
        else
           AVG_LONG_PRICE_LOTS=arrayfun(@( AVG_LONG_PRICE_LOTS)  sprintf('%.6f', AVG_LONG_PRICE_LOTS),  AVG_LONG_PRICE_LOTS, 'uniform', 0); 
        end
        
        
        
        %% SHORT(LOTS)
        
        SHORT_LOTS_PRICE_INDEX=find(strcmpi('SHORT(LOTS)',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        SHORT_PRICE_LOTS=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end, SHORT_LOTS_PRICE_INDEX )));
        
               
        if (  SHORT_PRICE_LOTS==0)
            SHORT_PRICE_LOTS_SIX_DIGITS  ={'-'};
        else
           SHORT_PRICE_LOTS_SIX_DIGITS=arrayfun(@( SHORT_PRICE_LOTS)  sprintf('%.6f', SHORT_PRICE_LOTS),  SHORT_PRICE_LOTS, 'uniform', 0); 
        end
        
        
        %% AVG SHORT PRICE RS/BAG
        AVG_SHORT_PRICE_RS_BAG_INDEX=find(strcmpi('AVG SHORT PRICE RS/BAG',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        AVG_SHORT_PRICE_BAG=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end,   AVG_SHORT_PRICE_RS_BAG_INDEX )));
          
        if (  AVG_SHORT_PRICE_BAG==0)
            AVG_SHORT_PRICE_BAG   ={'-'};
        else
            AVG_SHORT_PRICE_BAG=arrayfun(@( AVG_SHORT_PRICE_BAG)  sprintf('%.6f', AVG_SHORT_PRICE_BAG),  AVG_SHORT_PRICE_BAG, 'uniform', 0);
        end
        
        %% KC EDSP US c/lb
        KC_EDSP_INDEX=find(strcmpi('KC EDSP US c/lb',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        KC_EDSP_VALUE=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end, KC_EDSP_INDEX   )));
        
    
        
        if (   KC_EDSP_VALUE==0)
            KC_EDSP_VALUE  ={'-'};
        else
            KC_EDSP_VALUE=arrayfun(@( KC_EDSP_VALUE)  sprintf('%.2f', KC_EDSP_VALUE),  KC_EDSP_VALUE, 'uniform', 0);
        end
        
        %% KC EDSP BRL
        
        KC_EDSP_BRL_INDEX=find(strcmpi('KC EDSP BRL c/lb',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        KC_EDSP_BRL_VALUE=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end,      KC_EDSP_BRL_INDEX   )));
        
        
        if ( KC_EDSP_BRL_VALUE ==0)
            KC_EDSP_BRL_VALUE  ={'-'};
        else
            KC_EDSP_BRL_VALUE=arrayfun(@( KC_EDSP_BRL_VALUE)  sprintf('%.2f', KC_EDSP_BRL_VALUE),  KC_EDSP_BRL_VALUE, 'uniform', 0);
        end
        
        
        %% KC EDSP RS/BAG
        KC_EDSP_BRL_RS_INDEX=find(strcmpi('KC EDSP RS/BAG',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        KC_EDSP_BRL_VALUE_NEW=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end,      KC_EDSP_BRL_RS_INDEX   )));
        
        
        if ( KC_EDSP_BRL_VALUE_NEW ==0)
            KC_EDSP_BRL_VALUE_NEW ={'-'};
        else
             KC_EDSP_BRL_VALUE_NEW=arrayfun(@( KC_EDSP_BRL_VALUE_NEW)  sprintf('%.2f', KC_EDSP_BRL_VALUE_NEW),  KC_EDSP_BRL_VALUE_NEW, 'uniform', 0);
        end
        
        
        %% USDBRL EDSP
        
        USD_BRL_EDSP_INDEX=find(strcmpi('USDBRL EDSP',KC_COMPOS_SECTION_EX_SP_T_raw(2,:)));
        USD_BRL_EDSP_VALUE=nansum(str2double(KC_COMPOS_SECTION_EX_SP_T_raw(3:end,     USD_BRL_EDSP_INDEX  )));
        
        
        
        if ( USD_BRL_EDSP_VALUE ==0)
            USD_BRL_EDSP_VALUE  ={'-'};
        else
            USD_BRL_EDSP_VALUE=arrayfun(@( USD_BRL_EDSP_VALUE)  sprintf('%.4f', USD_BRL_EDSP_VALUE),  USD_BRL_EDSP_VALUE, 'uniform', 0);
        end
        
        %% Last line manipulation
        %% To Manage Total within lines
        KC_COMPOS_LAST_LINE=KC_COMPOS_SECTION_EX_SP_T_raw(end,1:end);
        KC_COMPOS_LAST_LINE(1,14)= USD_BRL_EDSP_VALUE;
        KC_COMPOS_LAST_LINE(1,13)=  KC_EDSP_BRL_VALUE_NEW;                                                    % cellstr(num2str((KC_EDSP_BRL_VALUE_NEW));
        KC_COMPOS_LAST_LINE(1,12)=  KC_EDSP_BRL_VALUE  ;
        KC_COMPOS_LAST_LINE(1,11)=  KC_EDSP_VALUE ;
        KC_COMPOS_LAST_LINE(1,10)=  AVG_SHORT_PRICE_BAG  ;
        KC_COMPOS_LAST_LINE(1,9)=  SHORT_PRICE_LOTS_SIX_DIGITS;
        KC_COMPOS_LAST_LINE(1,8)=    AVG_LONG_PRICE_LOTS ;
        KC_COMPOS_LAST_LINE(1,7)=  SUM_LONG_LOTS;
        t4=Table(KC_COMPOS_LAST_LINE);
        
        for  i=1:t4.NRows
            for j=t4.NCols-2:t4.NCols
                if str2double(t4.entry(i,j).Children(1).Content)<0
                    t4.entry(i,j).Children(1).Color='red';
                    %                     t4.entry(i,j).Children.Content=num2str(abs(str2double(t4.entry(i,j).Children.Content)));
                    t4.entry(i,j).Children.Content=strrep((t4.entry(i,j).Children.Content),'-','');
                    t4.entry(i,j).Children.Content = strcat('(',t4.entry(i,j).Children.Content,')');
                    
                end
            end
        end
        
        % New Section
        %% New SEction added
        %         for  i=6  %% Here
        %             for j=1: t4.NRows
        %                 t4  .entry(j,i).Style = [ t4.entry(j,i).Style {mlreportgen.dom.Width('2.8in')}];
        %             end
        %         end
        %         if  startsWith(Current_Sheet_Name,'ATC')
        %             for  i=8  %% Here
        %                 for j=1: t4.NRows
        %                     t4  .entry(j,i).Style = [ t4.entry(j,i).Style {mlreportgen.dom.Width('2.0in')}];
        %                 end
        %             end
        %         end
        
        t4.Style={Width('100%'),FontSize('0.13in'),HAlign('left'),Bold};
        t4.Border='None';
        t4.TableEntriesHAlign='Right';
        
        %% Page size manipulation
        pagesetup(OutPDFFile);
        
        %% Width Control Code of columns
        grp = TableColSpecGroup();
        grp.Span = t4.NCols; %% Here the spacing between columns can be managed by
        grp.ColSpecs = specs_kcta ;
        %         grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table. 0.7
        t4.ColSpecGroups = grp;
        t4.HAlign = 'left';
        %         t4.Style={Color('Black'),Bold,FontSize('0.13in'),ResizeToFitContents(false)};
        %append(OutPDFFile,title_table);
        %% Total Bold Mechanism
        Total_ROW_t=find(strcmpi('TOTAL',KC_COMPOS_LAST_LINE(:,1)));
        FIRST_ROW_Total=t4.row(Total_ROW_t);
        FIRST_ROW_Total.Style={Color('Black'),Bold,ResizeToFitContents(false)};
        %% Table appension Confirmation section
        append(OutPDFFile,t4);
    end
    
    %% Full Horizontal Line.
    hr8 = HorizontalRule();
    hr8.Border='solid';
    hr8.Style={Width('100%'),Bold};
    append(OutPDFFile,hr8);
    
    %% Smaller line section
    hr_s=HorizontalRule();
    hr_s.Border='solid';
    hr_s.Style={Width('100%')};
    hr_s.Style={OuterMargin('15.3in','0in','0in','0in')};
    append(OutPDFFile,hr_s);
    
    
    %% Total Management as per karthiks requirement.
    if (OTC_SWAPS_CHECK==1)
        MTM_USD_SWAPS=OTC_SWAPS_SECTION_REST_FOR_TOTAL(end-1:end);
        if iscellstr( MTM_USD_SWAPS)
            MTM_USD_SWAPS=str2double(MTM_USD_SWAPS);  %% MTM_USD for OTC SWAPS
        end
        hash_index=find(isnan(MTM_USD_SWAPS));
        MTM_USD_SWAPS(hash_index)=0;
    else
        MTM_USD_SWAPS=zeros(1,2);
    end
    
    if (OTC_OPTIONS_CHECK==1)
        REQUIRED_LAST_THREE_FIELD_OPTIONS=OTC_OPTIONS_SECTION_REST(end-4:end);%% PREMIUM_USD,MTM_USD,DR_CR_USD
        if iscellstr(REQUIRED_LAST_THREE_FIELD_OPTIONS)
            REQUIRED_LAST_THREE_FIELD_OPTIONS =str2double(REQUIRED_LAST_THREE_FIELD_OPTIONS);
        end
        hash_index=find(isnan( REQUIRED_LAST_THREE_FIELD_OPTIONS));
        REQUIRED_LAST_THREE_FIELD_OPTIONS(hash_index)=0;
        
    else
        REQUIRED_LAST_THREE_FIELD_OPTIONS=zeros(1,5);
    end
    
    if (OTC_ACCUMULATORS_CHECK==1) %% Not appropriate plese check
        REQUIRED_LAST_THREE_FIELD_ACC=OTC_ACCUMULATORS_SECTION_REST(end-3:end);
        if iscellstr(REQUIRED_LAST_THREE_FIELD_ACC)
            REQUIRED_LAST_THREE_FIELD_ACC =str2double(REQUIRED_LAST_THREE_FIELD_ACC );
        end
        
        hash_index=find(isnan( REQUIRED_LAST_THREE_FIELD_ACC));
        REQUIRED_LAST_THREE_FIELD_ACC(hash_index)=0;
    else
        REQUIRED_LAST_THREE_FIELD_ACC=zeros(1,4);
    end
    
    if (OTC_FX_SWAPS_CHECK==1)
        OTC_FX_SWAPS_LAST_TWO=OTC_FX_SWAPS_SECTION_REST(end-1:end);
        if iscellstr(  OTC_FX_SWAPS_LAST_TWO)
            OTC_FX_SWAPS_LAST_TWO  =str2double(  OTC_FX_SWAPS_LAST_TWO );
        end
        
        hash_index=find(isnan(OTC_FX_SWAPS_LAST_TWO));
        OTC_FX_SWAPS_LAST_TWO(hash_index)=0;
        
    else
        OTC_FX_SWAPS_LAST_TWO=zeros(1,2);
        
    end
    
    if (OTC_FX_OPTIONS_CHECK==1)
        OTC_FX_OPTIONS_LAST_THREE=OTC_FX_OPTIONS_SECTION_REST(end-2:end);
        if iscellstr(OTC_FX_OPTIONS_LAST_THREE)
            OTC_FX_OPTIONS_LAST_THREE =str2double(  OTC_FX_OPTIONS_LAST_THREE);
        end
        hash_index=find(isnan(OTC_FX_OPTIONS_LAST_THREE));
        OTC_FX_OPTIONS_LAST_THREE(hash_index)=0;
    else
        OTC_FX_OPTIONS_LAST_THREE =zeros(1,3);
        
    end
    
    
    if (KC_COMPOS_CHECK==1)
        KC_COMPOS_LAST_ONE=KC_COMPOS_LAST_LINE(end);
        if iscellstr( KC_COMPOS_LAST_ONE)
            KC_COMPOS_LAST_ONE   =str2double(KC_COMPOS_LAST_ONE );
        end
        hash_index=find(isnan( KC_COMPOS_LAST_ONE));
        KC_COMPOS_LAST_ONE(hash_index)=0;
    else
        KC_COMPOS_LAST_ONE =zeros(1,1);
        
    end
    
    %% Replacement of '0' by '-'
    
    %% MTM_USD_SUM
    MTM_USD_SUM=MTM_USD_SWAPS(end)+REQUIRED_LAST_THREE_FIELD_OPTIONS(end-1)+REQUIRED_LAST_THREE_FIELD_ACC(end-1)+OTC_FX_SWAPS_LAST_TWO(end)+OTC_FX_OPTIONS_LAST_THREE(end-1)+KC_COMPOS_LAST_ONE(end);
    % for testing rajashekhar commented
    %     MTM_USD_SUM=MTM_USD_SWAPS(end)+REQUIRED_LAST_THREE_FIELD_OPTIONS(end)+REQUIRED_LAST_THREE_FIELD_ACC(end)+OTC_FX_SWAPS_LAST_TWO(end)+OTC_FX_OPTIONS_LAST_THREE(end)+KC_COMPOS_LAST_ONE(end);
    MTM_USD_SUM=sprintf('%0.2f',MTM_USD_SUM);
    if (MTM_USD_SUM==0)
        MTM_USD_SUM='-';
    end
    if isnumeric(MTM_USD_SUM)
        MTM_USD_SUM=num2str(MTM_USD_SUM);
        MTM_USD_SUM=cellstr(MTM_USD_SUM);
    else
        if ischar(  MTM_USD_SUM )
            MTM_USD_SUM =cellstr( MTM_USD_SUM);
        end
    end
    
    
    %% MTM_DR_USD_SUM
    %     MTM_DR_USD_SUM=REQUIRED_LAST_THREE_FIELD_OPTIONS(end)+REQUIRED_LAST_THREE_FIELD_ACC(end)+ OTC_FX_OPTIONS_LAST_THREE(end);%% FX OPTIONS LAST
    MTM_DR_USD_SUM=MTM_USD_SWAPS(end)+REQUIRED_LAST_THREE_FIELD_OPTIONS(end-1)+REQUIRED_LAST_THREE_FIELD_ACC(end-1)+OTC_FX_SWAPS_LAST_TWO(end)+OTC_FX_OPTIONS_LAST_THREE(end-1)+KC_COMPOS_LAST_ONE(end);
    if (MTM_DR_USD_SUM==0)
        MTM_DR_USD_SUM='-';
    end
    if isnumeric(MTM_DR_USD_SUM)
        MTM_DR_USD_SUM=num2str(MTM_DR_USD_SUM);
        MTM_DR_USD_SUM=cellstr(MTM_DR_USD_SUM);
    else
        if ischar( MTM_DR_USD_SUM )
            MTM_DR_USD_SUM =cellstr( MTM_DR_USD_SUM  );
        end
    end
    
    
    %% MTM_PREMIUM_USD_SUM
    %     MTM_PREMIUM_USD_SUM= REQUIRED_LAST_THREE_FIELD_OPTIONS(end-3)+REQUIRED_LAST_THREE_FIELD_ACC(end-3)+OTC_FX_OPTIONS_LAST_THREE(end-2); %^% FX_OPTIONS
    MTM_PREMIUM_USD_SUM= REQUIRED_LAST_THREE_FIELD_OPTIONS(end-4)+REQUIRED_LAST_THREE_FIELD_ACC(end-3)+OTC_FX_OPTIONS_LAST_THREE(end-2); %^% FX_OPTIONS
    MTM_PREMIUM_USD_SUM=sprintf('%0.2f',MTM_PREMIUM_USD_SUM);
    
    if (MTM_PREMIUM_USD_SUM==0)
        MTM_PREMIUM_USD_SUM='-';
        
    end
    if isnumeric(MTM_PREMIUM_USD_SUM)
        MTM_PREMIUM_USD_SUM=num2str(MTM_PREMIUM_USD_SUM);
        MTM_PREMIUM_USD_SUM=cellstr(MTM_PREMIUM_USD_SUM);
    else
        if ischar(  MTM_PREMIUM_USD_SUM)
            MTM_PREMIUM_USD_SUM =cellstr(  MTM_PREMIUM_USD_SUM);
        end
    end
    %% MTM_PREMIUM_USD_COMMA_MANIPULATION
    %     MTM_PREMIUM_USD_SUM_char=char(MTM_PREMIUM_USD_SUM);
    % rajashekhar modified
    MTM_PREMIUM_USD_SUM_char=MTM_PREMIUM_USD_SUM;
    MTM_PREMIUM_USD_SUM = Sep1000Str(   MTM_PREMIUM_USD_SUM_char );
    
    
    %% To make zero replacement with underscore.
    MTM_PREMIUM_USD_SUM  =cellstr( MTM_PREMIUM_USD_SUM);
    if startsWith(MTM_PREMIUM_USD_SUM,'0')
        MTM_PREMIUM_USD_SUM={'-'};
    end
    
    %% MTM_NC_SUM
    %     MTM_NC_SUM= MTM_USD_SWAPS(end-1)+REQUIRED_LAST_THREE_FIELD_OPTIONS(end-2)+ REQUIRED_LAST_THREE_FIELD_ACC(end-2)+ OTC_FX_SWAPS_LAST_TWO(end-1);
    MTM_NC_SUM= MTM_USD_SWAPS(end-1)+REQUIRED_LAST_THREE_FIELD_OPTIONS(end-2)+ REQUIRED_LAST_THREE_FIELD_ACC(end-2)+ OTC_FX_SWAPS_LAST_TWO(end-1);
    MTM_NC_SUM=sprintf('%0.2f',MTM_NC_SUM);
    if (MTM_NC_SUM==0)
        MTM_NC_SUM='-';
        
    end
    if isnumeric(MTM_NC_SUM)
        MTM_NC_SUM=num2str(MTM_NC_SUM);
        MTM_NC_SUM=cellstr(MTM_NC_SUM);
    else
        if ischar(MTM_NC_SUM)
            MTM_NC_SUM=cellstr(MTM_NC_SUM);
        end
        
    end
    
    
    %% GRAND TOTAL CELL DECLARATION for trailing sections
    GRAND_TOTAL_INDEX=find(strcmpi(Overall_Current_Client_Data_Final(:,1),'GRAND TOTAL'));
    GRAND_TOTAL_DATA=Overall_Current_Client_Data_Final( GRAND_TOTAL_INDEX,:);
    output = Sep1000Str(  GRAND_TOTAL_DATA );
    %
    %
    output (1,1)={'GRAND TOTAL'};
    
    
    GRAND_TOTAL_DATA=output;
    
    GRAND_TOTAL_DOUBLE=cell2mat(GRAND_TOTAL_DATA(1,6));
    [~,col]=size(GRAND_TOTAL_DATA);
    
    
    %% grand total management
    GRAND_TOTAL_CELL=cell(1,col);
    GRAND_TOTAL_CELL(1,1)= GRAND_TOTAL_DATA(1);
    %     %% SIP GRAND TOTAL MANAGEMENT
    %     TOTAL_INDEX=find(strcmpi('TOTAL',Overall_Current_Client_Data_Final(:,1)));
    %     GRAND_TOTAL_VALUE=Overall_Current_Client_Data_Final(TOTAL_INDEX(end),end);
    
    %% GRAND TOTAL SECTION INTRODUCTION with MTM_USD and and MTM_PREMIUM
    if (OTC_SWAPS_CHECK==1)
        if(IncludeMtmNC==1)
            if isnumeric(GRAND_TOTAL_DOUBLE)
                GRAND_TOTAL_DOUBLE=num2str(GRAND_TOTAL_DOUBLE);
                GRAND_TOTAL_DOUBLE=cellstr(GRAND_TOTAL_DOUBLE);
            end
            
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)=MTM_USD_SUM;
        else
            if isnumeric(GRAND_TOTAL_DOUBLE)
                GRAND_TOTAL_DOUBLE=num2str(GRAND_TOTAL_DOUBLE);
                GRAND_TOTAL_DOUBLE=cellstr(GRAND_TOTAL_DOUBLE);
            end
            
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            
        end
    end
    
    if (OTC_OPTIONS_CHECK==1)
        %% MTM_USD_COMMA_HANDLING
        MTM_USD_SUM_char=char(MTM_USD_SUM);
        MTM_USD_SUM_char=MTM_USD_SUM;
        MTM_USD_SUM = Sep1000Str(   MTM_USD_SUM_char );
        if (IncludeMtmNC==1)
            if isnumeric(GRAND_TOTAL_DOUBLE)
                GRAND_TOTAL_DOUBLE=char(GRAND_TOTAL_DOUBLE);
            end
            
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            
            
            %             MTM_USD_SUM=cellstr( MTM_USD_SUM_char);
            GRAND_TOTAL_CELL(1,end-1)=MTM_USD_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_NC_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_PREMIUM_USD_SUM;
        else
            if isnumeric(GRAND_TOTAL_DOUBLE)
                GRAND_TOTAL_DOUBLE=num2str(GRAND_TOTAL_DOUBLE);
            end
            
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)= MTM_USD_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_NC_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_PREMIUM_USD_SUM;
        end
    end
    
    if (OTC_ACCUMULATORS_CHECK==1)
        if (IncludeMtmNC==1)
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)= MTM_USD_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_NC_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_PREMIUM_USD_SUM;
        else
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)= MTM_USD_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_NC_SUM;
            GRAND_TOTAL_CELL(1,end-2)= MTM_PREMIUM_USD_SUM;
        end
    end
    
    if (OTC_FX_SWAPS_CHECK==1)
        if (IncludeMtmNC==1)
            if isnumeric(GRAND_TOTAL_DOUBLE)
                GRAND_TOTAL_DOUBLE=num2str(GRAND_TOTAL_DOUBLE);
            end
            
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)= MTM_NC_SUM;
        else
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
        end
    end
    
    if (OTC_FX_SWAPS_CHECK==1)
        if (IncludeMtmNC==1)
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
            GRAND_TOTAL_CELL(1,end-1)= MTM_NC_SUM;
        else
            GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
        end
    end
    
    if (OTC_FX_OPTIONS_CHECK==1)
        if isnumeric(GRAND_TOTAL_DOUBLE)
            GRAND_TOTAL_DOUBLE=char(GRAND_TOTAL_DOUBLE);
        end
        GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
        GRAND_TOTAL_CELL(1,end-1)= MTM_USD_SUM;
        GRAND_TOTAL_CELL(1,end-2)= MTM_PREMIUM_USD_SUM;
    end
    
    if (KC_COMPOS_CHECK==1)
        if isnumeric(GRAND_TOTAL_DOUBLE)
            GRAND_TOTAL_DOUBLE=char(GRAND_TOTAL_DOUBLE);
        end
        GRAND_TOTAL_CELL(1,end)= cellstr( GRAND_TOTAL_DOUBLE) ;
    end
    
    
    
    Empty_cell_indexes_gt_=find(cellfun(@isempty, GRAND_TOTAL_CELL));
    %     GRAND_TOTAL_CELL( Empty_cell_indexes_gt_)={'-'};
    GRAND_TOTAL_CELL( Empty_cell_indexes_gt_)={''};
    output = Sep1000Str( GRAND_TOTAL_CELL);
    %% Grand total fix foran element where comma and decimal is not there.
    
    
    %% if decimal is there whilw no commas are there we have manage with the below function.
    output (1,1)={'GRAND TOTAL'};
    GRAND_TOTAL_CELL=output;
    
    %      GRAND_TOTAL_CELL=string_filtered_data_cell_grand_total;
    GRAND_TOTAL_CELL(1,6)={' '};
    
    TOTAL_INDEX_VIT_FIX=find(strcmpi(Current_Client_Info(:,1),'OTC OPTIONS'));
    TOTALIdx=find(strcmpi(Current_Client_Info(:,1),'TOTAL'));
    
    if (~isempty(TOTAL_INDEX_VIT_FIX))
        Req_total_index=find(TOTALIdx> TOTAL_INDEX_VIT_FIX,1);
    end
    
    check_req=exist(' Req_total_index');
    
    if (check_req==1)
        Premium_usd_value=Current_Client_Info(TOTALIdx(Req_total_index),17);
    end
    
    check_prem=exist('Premium_usd_value');
    
    if (check_prem==1)
        GRAND_TOTAL_CELL(1,end-3)=cellstr( Premium_usd_value);
    end
    
    
    %% SIP Assignment
    a=exist('SIP_MAN');
    if (a==1)
        if (SIP_MAN==1)
            Cash_Balance_Data(1,6)=OTC_SWAPS_SECTION_REST_FOR_TOTAL(end);
            GRAND_TOTAL_CELL=cell(1,col);
            GRAND_TOTAL_CELL(1,1)= GRAND_TOTAL_DATA(1);
            GRAND_TOTAL_CELL(1,end)=OTC_SWAPS_SECTION_REST_FOR_TOTAL(end);
        end
    end
    
    %% HERE we can add add replace '0' by '_'.
    EMPTY_GRAND_TOTAL_CELLS=cellfun(@isempty,GRAND_TOTAL_CELL);
    GRAND_TOTAL_CELL(EMPTY_GRAND_TOTAL_CELLS)={' '};
    % rajashekhar added
    Empty_cell_indexes_gt=find(cell2mat(cellfun(@(x) strcmpi(x,'NaN')||strcmpi(x,'0.00')||strcmpi(x,'-0.00')  , GRAND_TOTAL_CELL,'uni',0)));
    GRAND_TOTAL_CELL(Empty_cell_indexes_gt)={'-'};
    % end
    %% Space management in grand total suggested by Karthik.
    [length_gt,bredth_gt]=size(GRAND_TOTAL_CELL);
    emptty_cell_gt(length_gt:bredth_gt)={' '};
    
    GRAND_TOTAL_CELL=[ emptty_cell_gt;GRAND_TOTAL_CELL];
    
    %% GRAND TOTAL TABLE CREATION
    t_gt=Table(GRAND_TOTAL_CELL);
    t_gt.Style={Width('100%'),FontSize('0.13in'),HAlign('Right'),Bold,ResizeToFitContents(false)}; %
    t_gt.Border='None';
    t_gt.TableEntriesHAlign='Right';
    
    %% Page settings
    pagesetup(OutPDFFile);
    
    %% New Grand total formatting settings
    %% Width Control Code of columns
    grp = TableColSpecGroup();
    
    grp.Span = t_gt.NCols; %% Here the spacing between columns can be managed by
    da = formatting_optiontable( t_gt);
    grp.ColSpecs = da;
    %     grp.Style = {Width('1.5in')}; %% From Here we can manage the width of a Table 0.7
    t_gt.ColSpecGroups = grp;
    t_gt.HAlign = 'left';
    
    
    %% Red colour for nagative numbers in GRAND TOTAL SECTION
    
    for i=2:t_gt.NRows
        for j=1:t_gt.NCols
            if str2double(t_gt.entry(i,j).Children(1).Content)<0
                t_gt.entry(i,j).Children(1).Color='red';
                %                 t_gt.entry(i,j).Children.Content=num2str(abs(str2double(t_gt.entry(i,j).Children.Content)));
                t_gt.entry(i,j).Children.Content=strrep((t_gt.entry(i,j).Children.Content),'-','');
                t_gt.entry(i,j).Children.Content = strcat('(',t_gt.entry(i,j).Children.Content,')');
                
            end
            
        end
    end
    
    append(OutPDFFile,t_gt);
    
    %% Horizontal line after grand total.
    hr9 = HorizontalRule();
    hr9.Border='solid';
    hr9.Style={Width('100%'),Bold};
    hr9.Style={OuterMargin('15.3in','0in','0in','0in')};
    append(OutPDFFile,hr9);
    
    %% Pagebreak Object new edition.
    br = PageBreak();
    append( OutPDFFile,br);
    
    %% CLIENT INFORMATION SECTION
    if (CASH_BALANCE_INFO_CHECK==1)
        a=find(strcmpi('GRAND TOTAL',Overall_Current_Client_Data_Final(:,1)));  %% For position management we have did this
        d=EMPTY_SPACE_INDEX-a;
        e=find(d>0, 1 );
        dynamic_space=a+d(e); %% Dynamic space
        %% OTC SWAPS SECTION Data Accession
        Cash_Balance_Data=Overall_Current_Client_Data_Final(a:dynamic_space-1,1:end);
        output = Sep1000Str( Cash_Balance_Data);
        %% Simple way just come and replace
        Cash_Balance_Data=output;
        Cash_Balance_Data(:,1)=strrep(Cash_Balance_Data(:,1),',','');
        a=exist('SIP_MAN','var');
        if (a==1)
            if (SIP_MAN==1)
                Cash_Balance_Data(1,6)=OTC_SWAPS_SECTION_REST_FOR_TOTAL(end);
            end
        end
        %% Commas for red and non red both are there checked.
        Cash_Balance_Data(:,7:end,:)=[]; % analyse this for proper positioning.
        %Cash_Balance_Data(1:end,2:end)={' '};
        Empty_cell_indexes=find(cellfun(@isempty, Cash_Balance_Data));
        Cash_Balance_Data(Empty_cell_indexes)={' '};
        % rajashekhar added
        Empty_cell_indexes_gt=find(cell2mat(cellfun(@(x) strcmpi(x,'NaN')||strcmpi(x,'0.00')||strcmpi(x,'-0.00') , Cash_Balance_Data,'uni',0)));
        Cash_Balance_Data(Empty_cell_indexes_gt)={'-'};
        % end
        t=Table(Cash_Balance_Data);
        % Red Colour Mechanism of nagative numbers in footer section.
        for i=1:t.NRows
            for j=1:t.NCols
                if str2double(t.entry(i,j).Children(1).Content)<0
                    t.entry(i,j).Children(1).Color='red';
                    %                   t.entry(i,j).Children.Content=num2str(abs(str2double(t.entry(i,j).Children.Content)));
                    t.entry(i,j).Children.Content=strrep((t.entry(i,j).Children.Content),'-','');
                    t.entry(i,j).Children.Content = strcat('(',t.entry(i,j).Children.Content,')');
                    
                end
            end
        end
        
        t.Style = {FontSize('0.9in'),HAlign('right'),ResizeToFitContents(true)};
        t.Width='20%';
        t.Border='None';
        t.TableEntriesHAlign='left';
        t.Style={OuterMargin('13.0in','0in','0in','0in')};
        
        append(OutPDFFile,t);
        
    end
    br_l = PageBreak();
    append( OutPDFFile,br_l);
    
    %% Latest Edited Section Manipulations for last Note
    %p=paragraph('Hello how r u');
    %append(OutPDFFile,p);
    %tz=Text('NOTES AND DISCLAMIER');
    %tz.Bold=1;
    %append(OutPDFFile,tz);
    
    tz1=Text('1.A nagative figure means the client owes and a positive figure means Olam Owes the client');
    append(OutPDFFile,tz1);
    
    tz2=Text('2.Barrier Type:E/A/KL/KO-European/American/Knock-In/Knock-Out');
    append(OutPDFFile,tz2);
    
    t=Text('3.MTM Value reflected on statement is used for margining purposes only and generated by a computer estimate it is not a representation a tradable value');
    append(OutPDFFile,t);
    
    tz3=Text('4.Structure Name legend guide');
    append(OutPDFFile,tz3);
    
    t1=Text('Steed:usually Zero Cost Structure-Provides accumulations at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t1);
    
    t2=Text('Step Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t2);
    
    t3=Text('Buoy-Usually Zero Cost structure Provides accumulations at better than current market  level over a variable number of lots. Accumulators pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t3);
    
    t4=Text('DDU Daily Double Up:NOU NO Double UP:DUX Double UP at Expiry, DTU Daily Trips Up');
    append(OutPDFFile,t4);
    
    t5=Text('Long UpFront 50%-Usually Zero Cost.Provides swaps at better than current market upfront,with a chance of accelerated accumulations beyond a certain price level with a chance of increased accumulations at better than current market levels');
    append(OutPDFFile,t5);
    
    t6=Text('Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t6);
    
    t7=Text('Range Accural:NDU-Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t7);
    
    t8=Text('Avast-Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t8);
    
    t9=Text('Ladder-Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t9);
    
    t10=Text('Submarine-Usually Zero Cost Structure. Provides accumulation at better than current market levels over a variable number of lots. Accumulations pause beyond a certain price level with a chances of increased accumulations at better than current market levels');
    append(OutPDFFile,t10);
    
    t11=Text('Commodity Code Guide');
    append(OutPDFFile,t11);
    
    tc=Text('5.Commodity Code Guide');
    tc.Bold=1;
    append(OutPDFFile,tc);
    
    t12=Text('C-CBOT Corn futures (prices in c/bv');
    append(OutPDFFile,t12);
    
    t13=Text('CA-NYSE Milling Wheat futures (prices in euro/mt)');
    append(OutPDFFile,t13);
    
    t14=Text('CT-CE cotton futures(prices in c/)');
    append(OutPDFFile,t14);
    
    t15=Text('BO-CBOT Bean on futures(prices in c/lb');
    append(OutPDFFile,t15);
    
    t16=Text('KC-ICE NY Coffe futures(price in c/lb)');
    append(OutPDFFile,t16);
    
    t17=Text('KW-CBOT kansas Wheat futures (prices in c\lb)');
    append(OutPDFFile,t17);
    
    t18=Text('DF-NYSE London Coffe futures (prices in c/lb)');
    append(OutPDFFile,t18);
    
    t19=Text('S-CBOT Soyabean futures (prices in c/bu)');
    append(OutPDFFile,t19);
    
    t20=Text('SM-CBOT Soyabean futures (prices in USD/mt)');
    append(OutPDFFile,t20);
    
    t21=Text('W-CBOT Wheat futures(price in c/bw)');
    append(OutPDFFile,t21);
    
    t22=Text('OR-SGX Rubber futures');
    append(OutPDFFile,t22);
    
    t23=Text('SB-ICE NV Sugar No. 11 futures(prices in c/lb)');
    append(OutPDFFile,t23);
    
    t24=Text('QW-ICE NY White Sugar Futures(prices in USD/MT)');
    append(OutPDFFile,t24);
    
    t25=Text('QC-ICE EU London Cocoa futures(prices in USD/MT)');
    append(OutPDFFile,t25);
    
    t26=Text('CC-ICE NY Cocoa futures(prices in USD/MT)');
    append(OutPDFFile,t26);
    
    t27=Text('FX Code Guide');
    t27.Bold=1;
    append(OutPDFFile,t27);
    
    t28=Text('EUR/USD-EURUSD forward exchange rate (price of 1EUR in USD)');
    append(OutPDFFile,t28);
    
    t29=Text('GBP/USD-GBPUSD forward exchange rate (price of 1GBP in USD)');
    append(OutPDFFile,t29);
    
    t30=Text('USDBRL forward exchange rate(price of 1USD in BRL)');
    append(OutPDFFile,t30);
    
    t31=Text('7.EDSP:Exchange determined settlement price');
    t31.Bold=1;
    append(OutPDFFile,t31);
    
    t32=Text('8.Cash Account movement details reflect transaction for the day only');
    t32.Bold=1;
    append(OutPDFFile,t32);
    
    close(OutPDFFile);
    %% Document Conversion to pdf
    docview(char(Current_Sheet_Name),'convertdocxtopdf');
    [Pathname,Filename] = fileparts(OutPDFFile.OutputPath);
    Filename = [Filename,'.pdf'];
    OutPDFFile = fullfile(Pathname,Filename);
    %% if Option is available than total manipulation.
    %% SED steed structure fix.
    %% Call put logic.
catch ME
    OutErrorMsg = cellstr(ME.message);
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
end
end


