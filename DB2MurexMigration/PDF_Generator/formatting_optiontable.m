function [specs] = formatting_optiontable(p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
makeDOMCompilable();
import mlreportgen.dom.*;
import mlreportgen.report.*;
grp = TableColSpecGroup();

grp.Span = 21;
spec =TableColSpec;
spec.Style ={Width('1in')};
specs(1) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(2) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(3) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(4) =spec;

spec =TableColSpec;
spec.Style ={Width('0.6in')};
specs(5) =spec;

spec =TableColSpec;
spec.Style ={Width('0.8in')};
specs(6) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(7) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(8) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(9) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(10) =spec;

spec =TableColSpec;
spec.Style ={Width('0.8in')};
specs(11) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(12) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(13) =spec;

spec =TableColSpec;
spec.Style ={Width('0.8in')};
specs(14) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(15) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(16) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(17) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(18) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(19) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(20) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(21) =spec;
end

