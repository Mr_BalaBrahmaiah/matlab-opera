function [ specs ] = table_properties( p )

makeDOMCompilable();
import mlreportgen.dom.*;
import mlreportgen.report.*;
grp = TableColSpecGroup();
if p.NCols ==20
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
grp.Span = 20;
spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(1) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(2) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(3) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(4) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(5) =spec;

spec =TableColSpec;
spec.Style ={Width('2in')};
specs(6) =spec;

spec =TableColSpec;
spec.Style ={Width('2in')};
specs(7) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(8) =spec;

spec =TableColSpec;
spec.Style ={Width('1.8in')};
specs(9) =spec;

spec =TableColSpec;
spec.Style ={Width('0.8in')};
specs(10) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(11) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(12) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(13) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(14) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(15) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(16) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(17) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(18) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(19) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(20) =spec;
else
grp.Span = 21;
spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(1) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(2) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(3) =spec;

spec =TableColSpec;
spec.Style ={Width('1.2in')};
specs(4) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(5) =spec;

spec =TableColSpec;
spec.Style ={Width('2in')};
specs(6) =spec;

spec =TableColSpec;
spec.Style ={Width('2in')};
specs(7) =spec;

spec =TableColSpec;
spec.Style ={Width('1.6in')};
specs(8) =spec;

spec =TableColSpec;
spec.Style ={Width('1.8in')};
specs(9) =spec;

spec =TableColSpec;
spec.Style ={Width('1in')};
specs(10) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(11) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(12) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(13) =spec;

spec =TableColSpec;
spec.Style ={Width('0.2in')};
specs(14) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(15) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(16) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(17) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(18) =spec;

spec =TableColSpec;
spec.Style ={Width('0.5in')};
specs(19) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(20) =spec;

spec =TableColSpec;
spec.Style ={Width('1.5in')};
specs(21) =spec;

end
    
end

