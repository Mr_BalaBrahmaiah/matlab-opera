function [output ,error]=format_cell(input_array,formatspec)

  [row,col]=size(input_array);
  output=cell(row,col);
  for  k=1:row
      
      for h= 1:col
          
        s=str2double(input_array{k,h});
        
        if isnan(s)
           output{k,h} =input_array{k,h};
        elseif s==0
             output{k,h} ='-';
        else
        output{k,h}=sprintf(formatspec,s);
        end
      end
  end
  error ={''};
  
end