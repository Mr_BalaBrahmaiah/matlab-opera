function upload_price_data

try
    % connect to the database
    objDB = connect_to_database;
    
    % get the last date for which the data got updated in db price table
    SqlQuery = 'select max(date) from price_table';
    LastDatePriceTable = fetch(objDB,SqlQuery);
    % default start date - last updated date in db +1
    DefStartDate = datestr(busdate(datenum(LastDatePriceTable,'yyyy-mm-dd'),1),'yyyy-mm-dd');
    % default end date - today -1
    DefEndDate   = datestr(busdate(today,-1),'yyyy-mm-dd');
    
    % inform the user about the start and end settlement date for whcih the
    % data will get captured; also get the information about the time limit 
    % of contracts for which you want to capture the data
    Prompt        = {'Enter the Settlement Date - Start Date(yyyy-mm-dd format):','Enter the Settlement Date - End Date(yyyy-mm-dd format):','Enter the time limit of contracts for which you want to capture the data:(in days)'};
    Name          = 'Date Input';
    Numlines      = 1;
    Defaultanswer = {DefStartDate,DefEndDate,'540'};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Prices will not be fetched since the date input was not given!');
        return;
    end
    
    % capture and validate the needed inputs
    StartSettlementDate = InputDates{1};
    EndSettlementDate = InputDates{2};
    NumOfDays = str2num(InputDates{3});
    
    if isempty(StartSettlementDate)
        StartSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
    end
    
    if isempty(EndSettlementDate)
        EndSettlementDate = datestr(busdate(today,-1),'yyyy-mm-dd');
    end
    
    if isempty(NumOfDays)
        NumOfDays = 540;
    end
    
    % construct the date vector for whcih we need to capture the data from
    % bbg
    if isequal(StartSettlementDate,EndSettlementDate)
        SettlementDate = cellstr(StartSettlementDate);
    else
        BusDates = busdays(datenum(StartSettlementDate), datenum(EndSettlementDate),'daily','2014-12-25');
        SettlementDate = cellstr(datestr(BusDates,'yyyy-mm-dd'));
    end
    
    % read the data from Tickers file to get the list of tickers that need
    % to be read from bbg
    try
        TickerOptions =  textread('Tickers.txt','%s','delimiter','\n');
        if ~isrow(TickerOptions)
            TickerOptions = TickerOptions';
        end
    catch
        TickerOptions = {'px_open','px_high','px_low','px_last','px_settle','px_volume','open_int'};
    end
    
    % bbg related jar
    javaaddpath('C:\bbgexe\blpapi3.jar');
    objBB = blp;
    
    PriceTableColumns = ['contracts','date',TickerOptions];
    
    hWaitbar = waitbar(0,'Please wait, reading the Futures price data...');
    PriceTableData = [];
    for iDate = 1:length(SettlementDate)
        SettleDate = SettlementDate{iDate};
        % check the contracts that are not expired and less than 18 months
        % period
        EndDate   = datestr(datenum(SettleDate,'yyyy-mm-dd')+NumOfDays,'yyyy-mm-dd');
        SqlQuery = ['select bbg_ticker from future_expiry_table where last_trade between ''',SettleDate,''' and ''',EndDate,''''];
        FutureContracts = fetch(objDB,SqlQuery);
        
        OutFilename = ['FuturePriceTableData_',datestr(SettleDate,'yyyy-mm-dd'),'.xlsx'];
        OutData = [];
        % read all ticker options for every contract and for settlement
        % date
        for iContract = 1:length(FutureContracts)
            Data = history(objBB, FutureContracts{iContract},TickerOptions,SettleDate,SettleDate);
            if ~isempty(Data)
                RowData = [FutureContracts{iContract},SettleDate,num2cell(Data(:,2:end))];
                OutData = [OutData;RowData];
            end
            waitbar(iContract/length(FutureContracts),hWaitbar,['Please wait, reading the Futures price data for settlement date - ',SettleDate]);
        end
        
        % write the data for all contracts for a specific settlement date
        if ~isempty(OutData)
            xlswrite(OutFilename,[PriceTableColumns;OutData]);
            PriceTableData = [PriceTableData;OutData];
        end
    end
    delete(hWaitbar);
    
    % check with the user if he wants to upload the data to db and upload
    % it accordingly
    if ~isempty(PriceTableData)
        Reply = questdlg('Do you want to upload the data into price_table?','Upload PriceTable data','Yes','No','No');
        if strcmpi(Reply,'Yes')
            fastinsert(objDB,'price_table',PriceTableColumns,PriceTableData);
            msgbox('Data has been uploaded successfully!');
        end
    end
    
catch ME
    errordlg(ME.message);
end
