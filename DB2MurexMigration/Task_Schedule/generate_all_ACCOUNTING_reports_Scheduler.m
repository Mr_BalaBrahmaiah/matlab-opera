function [OutErrorMsg] = generate_all_ACCOUNTING_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('ACCOUNTING_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

All_BU = {'AGF','CFS','USG','QF1','QF2','QF3','QF4','QF5'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'Accounting_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);

%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    Accounting_Reports_Button = {'Accounting Report','FX ACC Report','FX Exposure Report'};
    
    for ii = 1 : length(Accounting_Reports_Button)
        
        Current_Report_Button =  Accounting_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(strtrim(Moving_StaticPath_Data.Web_page_Button),Current_Report_Button);
        Current_DestinationFolder = char(Moving_StaticPath_Data.Moving_Static_Path(Matched_MovingPath_Index));
        
        %%  Accounting Report
        
        if(strcmpi(Current_Report_Button,'Accounting Report'))
            try
                [OutErrorMsg1,Accounting_Report] = generate_accounting_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = ['Accounting Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Accounting_Report);
                    Overall_Reports = [Overall_Reports ; Accounting_Report];
                    
                     moving_report_to_corresponding_folder(Accounting_Report,Current_DestinationFolder,Current_Report_Button);
                    
                end
                continue;
            catch
                continue;
            end
        end
        
        %%  FX Accounting Report
        
        if(strcmpi(Current_Report_Button,'FX ACC Report'))
            try
                [OutErrorMsg1,FX_Accounting_Report] = generate_currency_accounting_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = ['FX Accounting Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FX_Accounting_Report);
                    Overall_Reports = [Overall_Reports ; FX_Accounting_Report];
                    
                     moving_report_to_corresponding_folder(FX_Accounting_Report,Current_DestinationFolder,Current_Report_Button);
                    
                end
                continue;
            catch
                
            end
        end
        
       %%  FX EXPOSURE Accounting Report
        
        if(strcmpi(Current_Report_Button,'FX Exposure Report') && strcmpi(InBUName,'USG')) 
            try
                [OutErrorMsg1,FX_Accounting_Report] = generate_fx_exposure_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = ['FX Exposure Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FX_Accounting_Report);
                    Overall_Reports = [Overall_Reports ; FX_Accounting_Report];
                    
                     moving_report_to_corresponding_folder(FX_Accounting_Report,Current_DestinationFolder,Current_Report_Button);
                    
                end
                continue;
            catch
                
            end
        end
        
    end % Second Loop
    
    %% Sending Mail
    
    %     Email_Subject = ['MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
    %
    %     for k = 1 : size(Overall_Reports,1)
    %
    %         sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{Overall_Reports(k,:)});
    %
    %     end
    
    %     Overall_Reports = [] ; %% For Next Loop
    
    %%
    
    
end % First Loop

if(~isempty(Overall_ErrorMsg))
    
    ACCOUNTING_Report_Status_File = getXLSFilename('Accounting_Report_Status_File');
    xlswrite(ACCOUNTING_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['Accounting Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com','balab.s@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],ACCOUNTING_Report_Status_File);
    
end

