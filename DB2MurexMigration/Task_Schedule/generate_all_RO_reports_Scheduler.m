function [OutErrorMsg] = generate_all_RO_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('RO_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

 All_BU = {'USG','QF1','QF2','QF3','QF4','CFS'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'RO_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);

%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    
    RO_Reports_Button = {'Daily Summary Report','Daily Greeks Report'};
 
    %%
    for ii = 1 : length(RO_Reports_Button)
        
        Current_Report_Button = RO_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(Moving_StaticPath_Data.Web_page_Button,Current_Report_Button);
        if(~isempty(find(Matched_MovingPath_Index)))
            Current_DestinationFolder = Moving_StaticPath_Data.Moving_Static_Path{Matched_MovingPath_Index};
            
            try
                if(iscell(Current_DestinationFolder))
                    Current_DestinationFolder = char(Current_DestinationFolder);
                end
                if(isempty(Current_DestinationFolder))
                    Current_DestinationFolder = []; % pwd;
                end
                if(isnan(Current_DestinationFolder))
                    Current_DestinationFolder = [] ; % pwd;
                end
            catch
                Current_DestinationFolder = [] ;
            end
        else
            OutErrorMsg1 = {'Not Available in Excel Sheet'};
            Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
            continue;
        end
        
        %% Daily Summary Report
        try
            if(strcmpi(Current_Report_Button,'Daily Summary Report'))
                [OutErrorMsg1,DailySummaryReport] = generate_daily_summary_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,DailySummaryReport);
                    Overall_Reports = [Overall_Reports ; cellstr(DailySummaryReport)];
                    
                    moving_report_to_corresponding_folder(DailySummaryReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Daily Greeks Report
        try
            if(strcmpi(Current_Report_Button,'Daily Greeks Report'))
                [OutErrorMsg1,DailyGreeksReport] = generate_daily_greeks_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,DailyGreeksReport);
                    Overall_Reports = [Overall_Reports ; cellstr(DailyGreeksReport)];
                    
                    moving_report_to_corresponding_folder(DailyGreeksReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end

    end % Second For Loop
    
    %% Sending Mail
    
    %     Email_Subject = ['RO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
    %
    %     for k = 1 : size(Overall_Reports,1)
    %
    %         sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{Overall_Reports(k,:)});
    %
    %     end
    
    %     Overall_Reports = [] ; %% For Next Loop
    
    %%
    
end % % First For Loop

%% Status Report

if(~isempty(Overall_ErrorMsg))
    
    RO_Report_Status_File = getXLSFilename('RO_Report_Status_File');
    xlswrite(RO_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['RO Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],RO_Report_Status_File);
    
end

