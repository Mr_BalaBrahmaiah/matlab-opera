function [OutErrorMsg] = generate_fx_exposure_report_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('FX_Exposure_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

All_BU = {'USG'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'Accounting_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);

%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    Accounting_Reports_Button = {'FX Exposure Report'};
    
    for ii = 1 : length(Accounting_Reports_Button)
        
        Current_Report_Button =  Accounting_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(strtrim(Moving_StaticPath_Data.Web_page_Button),Current_Report_Button);
        Current_DestinationFolder = char(Moving_StaticPath_Data.Moving_Static_Path(Matched_MovingPath_Index));

       %%  FX EXPOSURE Accounting Report
        
        if(strcmpi(Current_Report_Button,'FX Exposure Report'))
            try
                [OutErrorMsg,FX_Accounting_Report] = generate_fx_exposure_report(InBUName);
                if ~strcmpi(OutErrorMsg,'No errors')
                    OutErrorMsg = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg]];
                else
                    Email_Subject = ['FX Exposure Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FX_Accounting_Report);
                    Overall_Reports = [Overall_Reports ; FX_Accounting_Report];
                    
                     moving_report_to_corresponding_folder(FX_Accounting_Report,Current_DestinationFolder,Current_Report_Button);
                    
                end
                continue;
            catch
                
            end
        end
        
    end % Second Loop
    

    
end % First Loop

if(~isempty(Overall_ErrorMsg))
    
    ACCOUNTING_Report_Status_File = getXLSFilename('Fx_Exposure_Report_Status_File');
    xlswrite(ACCOUNTING_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['Fx Exposure Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com','balab.s@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],ACCOUNTING_Report_Status_File);
    
end

