
try
    Add_Folders_SubFoleders_Path;
    
    Overall_BU = {'agf','qf1','qf2','qf3','qf4','cfs','qf'} ;
    
    ObjDB =connect_to_database;
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %%
    
    Finished_Report_Status = [];
    Error_Report_Staus = [];
    
    for i = 1 : length(Overall_BU)
        Current_InBUName = char(Overall_BU(i));
        try
            
            if(strcmpi(Current_InBUName,'qf'))
                [OutErrorMsg,OutXLSfileName] = generate_position_monitoring_report_SingleDay_QF(Current_InBUName);
            else
                [OutErrorMsg,OutXLSfileName] = generate_position_monitoring_report_StoredProcedure_SingleDay(Current_InBUName);
            end
            
            Finished_Report_Status = [Finished_Report_Status ; {['Finished ',upper(char(Current_InBUName)),' Position Monitoring Report']} ];
            
            cprintf('key','%s finished\n', ['Finished ',upper(char(Current_InBUName)),' Position Monitoring Report']);
        catch
            Error_Report_Staus = [Error_Report_Staus ; {['Error in ',upper(char(Current_InBUName)),' Position Monitoring Report']}];
            continue;
        end
        
        %% Send Email to Corresponding Users
        
        System_Name = upper(getComputerName()) ;
        
        Email_Subject = [upper(char(Current_InBUName)),' Position Monitoring Scheduler Report for COB ''',char(Settle_Date),''' '] ;
        try
            sendmail({'raghavendra.sn@olamnet.com','funds.mo@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{OutXLSfileName});
            
        catch
            sendmail({'raghavendra.sn@olamnet.com','funds.mo@olamnet.com'},['Error in ',Email_Subject],[' No Attachement ',Email_Subject,' from ',System_Name]);
        end
        
    end
    
    %% Send Email for Status Verification
    
    EMail_Report_1 = getXLSFilename('Scheduler_Position_Monitoring_Report');
    
    if(~isempty(Finished_Report_Status))
        xlswrite(EMail_Report_1,Finished_Report_Status,'Finshed_Finished_Report_Status');
    end
    if(~isempty(Error_Report_Staus))
        xlswrite(EMail_Report_1,Error_Report_Staus,'Error_Report_Staus');
    end
    
    xls_delete_sheets(fullfile(pwd,EMail_Report_1),'Sheet1');
    
    Email_Subject_1 = ['Position Monitoring Scheduler Status Report for COB ''',char(Settle_Date),''' '] ;
    try
        sendmail({'raghavendra.sn@olamnet.com'},Email_Subject_1,['Attached ',Email_Subject_1,' from ',System_Name],{EMail_Report_1});
        
    catch
        sendmail({'raghavendra.sn@olamnet.com'},['Error in ',Email_Subject_1],[' No Attachement ',Email_Subject_1,' from ',System_Name]);
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end