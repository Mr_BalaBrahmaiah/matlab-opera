function [OutErrorMsg] = generate_all_MO_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('MO_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

% All_BU = {'CFS','AGF','COT','COC','COF','COP','ORX','QF1','QF2','QF3','QF4'};
All_BU = {'CFS','AGF','COT','COC','COF','COP','USG'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'MO_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);

%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    
    if(~isempty(cellStrfind_exact({'QF1','QF2','QF3','QF4','COT','COF'},{InBUName})))
        MO_Reports_Button = {'Summary Report','Monthwise Summary Report','FXFutures Report','FXForwards Report','Position Monitoring'};
    elseif(~isempty(cellStrfind_exact({'COC'},{InBUName})))
        MO_Reports_Button = {'Traders Report Summary','PnP Report'};
    else
        MO_Reports_Button = {'Portfolio Delta Report','Options Realized and Unrealized Report','Net Futures Portfoliowise Report','Net Futures Productwise Report',...
            'FF Portfolio Report','FF Positions Report','FF Greeks Report','PnL Monitoring Report','Greek PnL Report','VAR Report','Monthend EOD',...
            'Traders Report Summary','PnP Report','Previous Settle Accounting Traders Report','Prervious Settle Accounting PnL Report','OIL EOD Report',...
            'Breach Report','Position Monitoring','Counterparty Report','EOD Report'};
    end
    
    %%
    for ii = 1 : length(MO_Reports_Button)
        
        Current_Report_Button = MO_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(Moving_StaticPath_Data.Web_page_Button,Current_Report_Button);
        if(~isempty(find(Matched_MovingPath_Index)))
            Current_DestinationFolder = Moving_StaticPath_Data.Moving_Static_Path{Matched_MovingPath_Index};
            
            try
                if(iscell(Current_DestinationFolder))
                    Current_DestinationFolder = char(Current_DestinationFolder);
                end
                if(isempty(Current_DestinationFolder))
                    Current_DestinationFolder = []; % pwd;
                end
                if(isnan(Current_DestinationFolder))
                    Current_DestinationFolder = [] ; % pwd;
                end
            catch
                Current_DestinationFolder = [] ;
            end
        else
            OutErrorMsg1 = {'Not Available in Excel Sheet'};
            Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
            continue;
        end
        
        %% Portfolio Delta Report
        try
            if(strcmpi(Current_Report_Button,'Portfolio Delta Report'))
                [OutErrorMsg1,PortfolioDeltaFilename] = generate_portfolio_delta_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,PortfolioDeltaFilename);
                    Overall_Reports = [Overall_Reports ; cellstr(PortfolioDeltaFilename)];
                    
                    moving_report_to_corresponding_folder(PortfolioDeltaFilename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Net Futures Portfoliowise Report
        try
            if(strcmpi(Current_Report_Button,'Net Futures Portfoliowise Report'))
                [OutErrorMsg1,Net_Fut_PortfolioWise_Filename] = generate_net_fut_portfoliowise_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Net_Fut_PortfolioWise_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(Net_Fut_PortfolioWise_Filename)];
                    
                    moving_report_to_corresponding_folder(Net_Fut_PortfolioWise_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Net Futures Productwise Report
        try
            if(strcmpi(Current_Report_Button,'Net Futures Productwise Report'))
                [OutErrorMsg1,Net_Fut_ProductWise_Filename] = generate_net_fut_prodwise_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Net_Fut_ProductWise_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(Net_Fut_ProductWise_Filename)];
                    
                    moving_report_to_corresponding_folder(Net_Fut_ProductWise_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FF Portfolio Report
        try
            if(strcmpi(Current_Report_Button,'FF Portfolio Report'))
                [OutErrorMsg1,FF_Portfolio_Filename] = generate_ff_portfolio_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FF_Portfolio_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(FF_Portfolio_Filename)];
                    
                    moving_report_to_corresponding_folder(FF_Portfolio_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FF Positions Report
        try
            if(strcmpi(Current_Report_Button,'FF Positions Report'))
                [OutErrorMsg1,FF_Position_Filename] = generate_ff_position_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FF_Position_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(FF_Position_Filename)];
                    
                    moving_report_to_corresponding_folder(FF_Position_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FF Greeks Report
        try
            if(strcmpi(Current_Report_Button,'FF Greeks Report'))
                [OutErrorMsg1,FF_Greeks_FileName] = generate_ff_greeks_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FF_Greeks_FileName);
                    Overall_Reports = [Overall_Reports ; cellstr(FF_Greeks_FileName)];
                    
                    moving_report_to_corresponding_folder(FF_Greeks_FileName,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% PnL Monitoring Report
        try
            if(strcmpi(Current_Report_Button,'PnL Monitoring Report'))
                [OutErrorMsg1,PandL_Monitoring_Filename] = generate_pandl_monitoring_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,PandL_Monitoring_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(PandL_Monitoring_Filename)];
                    
                    moving_report_to_corresponding_folder(PandL_Monitoring_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Greek PnL Report
        try
            if(strcmpi(Current_Report_Button,'Greek PnL Report'))
                [OutErrorMsg1,Greel_PnL_Filename] = generate_greek_pandl_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Greel_PnL_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(Greel_PnL_Filename)];
                    
                    moving_report_to_corresponding_folder(Greel_PnL_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% VAR Report
        try
            if(strcmpi(Current_Report_Button,'VAR Report'))
                [OutErrorMsg1,VARFilename] = generate_accounting_VAR_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,VARFilename);
                    Overall_Reports = [Overall_Reports ; cellstr(VARFilename)];
                    
                    moving_report_to_corresponding_folder(VARFilename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Monthend EOD
        try
            if(strcmpi(Current_Report_Button,'Monthend EOD'))
                [OutErrorMsg1,MonthEnd_EOD_Filename] = generate_monthend_eod_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,MonthEnd_EOD_Filename);
                    Overall_Reports = [Overall_Reports ; cellstr(MonthEnd_EOD_Filename)];
                    
                    moving_report_to_corresponding_folder(MonthEnd_EOD_Filename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Traders Report Summary
        try
            if(strcmpi(Current_Report_Button,'Traders Report Summary'))
                [OutErrorMsg1,TradersReportSummaryFilename] = generate_traders_report_summary(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,TradersReportSummaryFilename);
                    Overall_Reports = [Overall_Reports ; cellstr(TradersReportSummaryFilename)];
                    
                    moving_report_to_corresponding_folder(TradersReportSummaryFilename,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% PnP Report
        try
            if(strcmpi(Current_Report_Button,'PnP Report'))
                [OutErrorMsg1,PnpOptionDumpFileName,PnpFutureDumpFileName,PnpMUsDumpFileName] = generate_pnp_reports(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,PnpOptionDumpFileName);
                    sendmail_to_corresponding_user(Email_Subject,PnpFutureDumpFileName);
                    sendmail_to_corresponding_user(Email_Subject,PnpMUsDumpFileName);
                    Overall_Reports = [Overall_Reports ; cellstr(PnpOptionDumpFileName)];
                    Overall_Reports = [Overall_Reports ; cellstr(PnpFutureDumpFileName)];
                    Overall_Reports = [Overall_Reports ; cellstr(PnpMUsDumpFileName)];
                    
                    moving_report_to_corresponding_folder(PnpOptionDumpFileName,Current_DestinationFolder,Current_Report_Button);
                    moving_report_to_corresponding_folder(PnpFutureDumpFileName,Current_DestinationFolder,Current_Report_Button);
                    moving_report_to_corresponding_folder(PnpMUsDumpFileName,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Previous Settle Accounting Traders Report
        try
            if(strcmpi(Current_Report_Button,'Previous Settle Accounting Traders Report'))
                [OutErrorMsg1,AccountingTRReport] = generate_previous_settle_accounting_traders_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,AccountingTRReport);
                    Overall_Reports = [Overall_Reports ; cellstr(AccountingTRReport)];
                    
                    moving_report_to_corresponding_folder(AccountingTRReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
        %% Prervious Settle Accounting PnL Report
        try
            if(strcmpi(Current_Report_Button,'Prervious Settle Accounting PnL Report'))
                [OutErrorMsg1,PnlReport] = generate_previous_accounting_tr_pnl_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,PnlReport);
                    Overall_Reports = [Overall_Reports ; cellstr(PnlReport)];
                    
                    moving_report_to_corresponding_folder(PnlReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% OIL EOD Report
        try
            if(strcmpi(Current_Report_Button,'OIL EOD Report'))
                [OutErrorMsg1,OIL_EOD_Report] = generate_OIL_eod_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,OIL_EOD_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(OIL_EOD_Report)];
                    
                    moving_report_to_corresponding_folder(OIL_EOD_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Summary Report (QF1,QF2,QF3,QF4)
        try
            if(strcmpi(Current_Report_Button,'Summary Report'))
                [OutErrorMsg1,Summary_Report] = generate_mo_summary_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Summary_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Summary_Report)];
                    
                    moving_report_to_corresponding_folder(Summary_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Monthwise Summary Report (QF1,QF2,QF3,QF4)
        try
            if(strcmpi(Current_Report_Button,'Monthwise Summary Report'))
                [OutErrorMsg1,MonthWise_Summary_Report] = generate_mo_summary_monthwise_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,MonthWise_Summary_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(MonthWise_Summary_Report)];
                    
                    moving_report_to_corresponding_folder(MonthWise_Summary_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FXFutures Report (QF1,QF2,QF3,QF4)
        try
            if(strcmpi(Current_Report_Button,'FXFutures Report'))
                [OutErrorMsg1,FxFutures_Report] = generate_mo_fxfutures_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FxFutures_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(FxFutures_Report)];
                    
                    moving_report_to_corresponding_folder(FxFutures_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FXForwards Report (QF1,QF2,QF3,QF4)
        try
            if(strcmpi(Current_Report_Button,'FXForwards Report'))
                [OutErrorMsg1,FxForward_Report] = generate_mo_fxforwards_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FxForward_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(FxForward_Report)];
                    
                    moving_report_to_corresponding_folder(FxForward_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
        
        %% Position Monitoring (AGF,CFS,QF1,QF2,QF3,QF4)
        try
            if(strcmpi(Current_Report_Button,'Position Monitoring'))
                [OutErrorMsg1,Position_Monitoring_Report] = generate_position_monitoring_report_StoredProcedure_SingleDay(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Position_Monitoring_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Position_Monitoring_Report)];
                    
                    moving_report_to_corresponding_folder(Position_Monitoring_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
        %% Breach Report (AGF)
        try
            if(strcmpi(Current_Report_Button,'Breach Report'))
                [OutErrorMsg1,Breach_Report] = generate_breach_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Breach_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Breach_Report)];
                    
                    moving_report_to_corresponding_folder(Breach_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
        %% Counterparty Report (CFS)
        try
            if(strcmpi(Current_Report_Button,'Counterparty Report'))
                [OutErrorMsg1,Counterparty_Report] = generate_counterparty_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Counterparty_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Counterparty_Report)];
                    
                    moving_report_to_corresponding_folder(Counterparty_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
        %% EOD Report (CFS)
        try
            if(strcmpi(Current_Report_Button,'EOD Report'))
                [OutErrorMsg1,EOD_Report] = generate_eod_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,EOD_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(EOD_Report)];
                    
                    moving_report_to_corresponding_folder(EOD_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
            
        catch
            continue;
        end
    end % Second For Loop
    
    %% Sending Mail
    
    %     Email_Subject = ['MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
    %
    %     for k = 1 : size(Overall_Reports,1)
    %
    %         sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{Overall_Reports(k,:)});
    %
    %     end
    
    %     Overall_Reports = [] ; %% For Next Loop
    
    %%
    
end % % First For Loop

%% Status Report

if(~isempty(Overall_ErrorMsg))
    
    MO_Report_Status_File = getXLSFilename('MO_Report_Status_File');
    xlswrite(MO_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['MO Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],MO_Report_Status_File);
    
end

