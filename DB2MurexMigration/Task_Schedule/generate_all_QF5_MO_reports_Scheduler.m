function [OutErrorMsg] = generate_all_QF5_MO_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

Settle_Date = SettlementDate; % For Input Parameter
Trade_Date = SettlementDate; % For Input Parameter

TextFile = strcat('QF5_MO_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

All_BU = {'QF5'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'QF5_MO_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);


%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    QF5_MO_Reports_Button = {'FTD Trades Report','Purchase Schedule Report','Sales Schedule Report','Unrealised Report','Realised Report - FTD','Realised Report Cumulative - FTM',...
        'Realised Report Cumulative - FTY','Commission Report','Borrowing Interest Report','Commission FTM/FTY Report','Borrowing FTM/FTY Report','QF5 FTD PnL Change'};
    
    for ii = 1 : length(QF5_MO_Reports_Button)
        
        Current_Report_Button = QF5_MO_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(Moving_StaticPath_Data.Web_page_Button,Current_Report_Button);
        if(~isempty(find(Matched_MovingPath_Index)))
            Current_DestinationFolder = Moving_StaticPath_Data.Moving_Static_Path(Matched_MovingPath_Index);
            
            try
                if(iscell(Current_DestinationFolder))
                    Current_DestinationFolder = char(Current_DestinationFolder);
                end
                if(isempty(Current_DestinationFolder))
                    Current_DestinationFolder = []; % pwd;
                end
                if(isnan(Current_DestinationFolder))
                    Current_DestinationFolder = [] ; % pwd;
                end
            catch
                Current_DestinationFolder = [] ;
            end
        else
            OutErrorMsg1 = {'Not Available in Excel Sheet'};
            Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
            continue;
        end
        
        %% FTD Trades Report
        try
            if(strcmpi(Current_Report_Button,'FTD Trades Report'))
                [OutErrorMsg1,FTD_Report] = QF5_FTD_Trades(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FTD_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(FTD_Report)];
                    
                    moving_report_to_corresponding_folder(FTD_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Purchase Schedule Report
        try
            if(strcmpi(Current_Report_Button,'Purchase Schedule Report'))
                [OutErrorMsg1,Purchase_Schedule_Report] = QF5_Purchase_Schedule(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Purchase_Schedule_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Purchase_Schedule_Report)];
                    
                    moving_report_to_corresponding_folder(Purchase_Schedule_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Sales Schedule Report
        try
            if(strcmpi(Current_Report_Button,'Sales Schedule Report'))
                [OutErrorMsg1,Sales_Schedule_Report] = QF5_Sales_Schedule(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Sales_Schedule_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Sales_Schedule_Report)];
                    
                    moving_report_to_corresponding_folder(Sales_Schedule_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Unrealised Report
        try
            if(strcmpi(Current_Report_Button,'Unrealised Report'))
                [OutErrorMsg1,UnRealised_Report] = QF5_UnRealised_Report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,UnRealised_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(UnRealised_Report)];
                    
                    moving_report_to_corresponding_folder(UnRealised_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Realised Report - FTD
        try
            if(strcmpi(Current_Report_Button,'Realised Report - FTD'))
                [OutErrorMsg1,QF5_Realised_Report_FTD] = QF5_Realised_Report_FTD(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_Realised_Report_FTD);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_Realised_Report_FTD)];
                    
                    moving_report_to_corresponding_folder(QF5_Realised_Report_FTD,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Realised Report Cumulative - FTM
        try
            if(strcmpi(Current_Report_Button,'Realised Report Cumulative - FTM'))
                [OutErrorMsg1,QF5_Realised_Report_Cumulative_FTM] = QF5_Realised_Report_Cumulative_FTM(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_Realised_Report_Cumulative_FTM);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_Realised_Report_Cumulative_FTM)];
                    
                    moving_report_to_corresponding_folder(QF5_Realised_Report_Cumulative_FTM,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Realised Report Cumulative - FTY
        try
            if(strcmpi(Current_Report_Button,'Realised Report Cumulative - FTY'))
                [OutErrorMsg1,QF5_Realised_Report_Cumulative_FTY_1] = QF5_Realised_Report_Cumulative_FTY(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_Realised_Report_Cumulative_FTY_1);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_Realised_Report_Cumulative_FTY_1)];
                    
                    moving_report_to_corresponding_folder(QF5_Realised_Report_Cumulative_FTY_1,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Commission Report
        try
            if(strcmpi(Current_Report_Button,'Commission Report'))
                [OutErrorMsg1,QF5_Commission_Calculation_Report] = QF5_Commission_Calculation(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_Commission_Calculation_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_Commission_Calculation_Report)];
                    
                    moving_report_to_corresponding_folder(QF5_Commission_Calculation_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Borrowing Interest Report
        try
            if(strcmpi(Current_Report_Button,'Borrowing Interest Report'))
                [OutErrorMsg1,QF5_Borrowing_InterestRate_Calculation_Report] = QF5_Borrowing_InterestRate_Calculation(InBUName,Trade_Date,Settle_Date);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_Borrowing_InterestRate_Calculation_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_Borrowing_InterestRate_Calculation_Report)];
                    
                    moving_report_to_corresponding_folder(QF5_Borrowing_InterestRate_Calculation_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Commission FTM/FTY Report
        %         try
        %             if(strcmpi(Current_Report_Button,'Commission FTM/FTY Report'))
        %                 [OutErrorMsg1,QF5_Commission_Calculation_FTM_FTY_Report] = QF5_Commission_Calculation_FTM_FTY(InBUName,From_Date,End_Date);
        %                 if ~strcmpi(OutErrorMsg1,'No errors')
        %                     OutErrorMsg1 = {'Not Successful'};
        %                     Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
        %                 else
        %                     Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
        %                     sendmail_to_corresponding_user(Email_Subject,QF5_Commission_Calculation_FTM_FTY_Report);
        %                     Overall_Reports = [Overall_Reports ; cellstr(QF5_Commission_Calculation_FTM_FTY_Report)];
        %
        %                     moving_report_to_corresponding_folder(QF5_Commission_Calculation_FTM_FTY_Report,Current_DestinationFolder,Current_Report_Button);
        %                 end
        %                 continue;
        %             end
        %         catch
        %             continue;
        %         end
        
        %% Borrowing FTM/FTY Report
        %         try
        %             if(strcmpi(Current_Report_Button,'Borrowing FTM/FTY Report'))
        %                 [OutErrorMsg1,QF5_Borrowing_InterestRate_Calculation_FTM_FTY_Report] = QF5_Borrowing_InterestRate_Calculation_FTM_FTY(InBUName,From_Date,End_Date);
        %                 if ~strcmpi(OutErrorMsg1,'No errors')
        %                     OutErrorMsg1 = {'Not Successful'};
        %                     Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
        %                 else
        %                     Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
        %                     sendmail_to_corresponding_user(Email_Subject,QF5_Borrowing_InterestRate_Calculation_FTM_FTY_Report);
        %                     Overall_Reports = [Overall_Reports ; cellstr(QF5_Borrowing_InterestRate_Calculation_FTM_FTY_Report)];
        %
        %                     moving_report_to_corresponding_folder(QF5_Borrowing_InterestRate_Calculation_FTM_FTY_Report,Current_DestinationFolder,Current_Report_Button);
        %                 end
        %                 continue;
        %             end
        %         catch
        %             continue;
        %         end
        
        %% QF5 FTD PnL Change
        try
            if(strcmpi(Current_Report_Button,'QF5 FTD PnL Change'))
                [OutErrorMsg1,QF5_FTD_PnL_Change_Report] = QF5_FTD_PnL_Change(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' QF5 MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,QF5_FTD_PnL_Change_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(QF5_FTD_PnL_Change_Report)];
                    
                    moving_report_to_corresponding_folder(QF5_FTD_PnL_Change_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        
    end %% second Loop
end %% First Loop


%% Status Report

if(~isempty(Overall_ErrorMsg))
    
    MCO_Report_Status_File = getXLSFilename('QF5_MO_Report_Status_File');
    xlswrite(MCO_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['QF5 MO Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],MCO_Report_Status_File);
    
end
















