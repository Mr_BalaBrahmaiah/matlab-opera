function [OutErrorMsg] = generate_all_MCO_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('MCO_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

All_BU = {'AGF','COT','COC','COF','COP','ORX','QF1','QF2','QF3','QF4'};

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'MCO_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);


%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    MCO_Reports_Button = {'Summary Report','Trade Details','Settlement Price & Delta Details','Counterparty Delta Position Report','Delta Reports'};
    
    for ii = 1 : length(MCO_Reports_Button)
        
        Current_Report_Button = MCO_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(Moving_StaticPath_Data.Web_page_Button,Current_Report_Button);
        if(~isempty(find(Matched_MovingPath_Index)))
            Current_DestinationFolder = Moving_StaticPath_Data.Moving_Static_Path(Matched_MovingPath_Index);
            
            try
                if(iscell(Current_DestinationFolder))
                    Current_DestinationFolder = char(Current_DestinationFolder);
                end
                if(isempty(Current_DestinationFolder))
                    Current_DestinationFolder = []; % pwd;
                end
                if(isnan(Current_DestinationFolder))
                    Current_DestinationFolder = [] ; % pwd;
                end
            catch
                Current_DestinationFolder = [] ;
            end
        else
            OutErrorMsg1 = {'Not Available in Excel Sheet'};
            Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
            continue;
        end
        
        %% Summary Report
        try
            if(strcmpi(Current_Report_Button,'Summary Report'))
                [OutErrorMsg1,Summary_Report] = generate_compliance_report_summary(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,Summary_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(Summary_Report)];
                    
                    moving_report_to_corresponding_folder(Summary_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Trade Details
        try
            if(strcmpi(Current_Report_Button,'Trade Details'))
                [OutErrorMsg1,TradeDetails_Report] = generate_compliance_trade_view(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,TradeDetails_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(TradeDetails_Report)];
                    
                    moving_report_to_corresponding_folder(TradeDetails_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Settlement Price & Delta Details
        try
            if(strcmpi(Current_Report_Button,'Settlement Price & Delta Details'))
                [OutErrorMsg1,SettlementPrice_Delta_Report] = generate_compliance_summary_view(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,SettlementPrice_Delta_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(SettlementPrice_Delta_Report)];
                    
                    moving_report_to_corresponding_folder(SettlementPrice_Delta_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Counterparty Delta Position Report
        try
            if(strcmpi(Current_Report_Button,'Counterparty Delta Position Report'))
                [OutErrorMsg1,CounterpartyDelta_Report] = generate_counterparty_delta_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,CounterpartyDelta_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(CounterpartyDelta_Report)];
                    
                    moving_report_to_corresponding_folder(CounterpartyDelta_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Delta Reports
        try
            if(strcmpi(Current_Report_Button,'Delta Reports'))
                [OutErrorMsg1,OutFilename1,OutFilename2,OutFilename3] = generate_compliance_delta_reports(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,OutFilename1);
                    sendmail_to_corresponding_user(Email_Subject,OutFilename2);
                    sendmail_to_corresponding_user(Email_Subject,OutFilename3);
                    
                    Overall_Reports = [Overall_Reports ; cellstr(OutFilename1)];
                    Overall_Reports = [Overall_Reports ; cellstr(OutFilename2)];
                    Overall_Reports = [Overall_Reports ; cellstr(OutFilename3)];
                    
                    moving_report_to_corresponding_folder(OutFilename1,Current_DestinationFolder,Current_Report_Button);
                    moving_report_to_corresponding_folder(OutFilename2,Current_DestinationFolder,Current_Report_Button);
                    moving_report_to_corresponding_folder(OutFilename3,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
    end % Second For Loop
    
    %% Sending Mail
    
    %     Email_Subject = ['MCO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
    %
    %     for k = 1 : size(Overall_Reports,1)
    %
    %         sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{Overall_Reports(k,:)});
    %
    %     end
    
    %     Overall_Reports = [] ; %% For Next Loop
    
    %%
    
end % First For Loop

%% Status Report

if(~isempty(Overall_ErrorMsg))
    
    MCO_Report_Status_File = getXLSFilename('MCO_Report_Status_File');
    xlswrite(MCO_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['MCO Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],MCO_Report_Status_File);
    
end



