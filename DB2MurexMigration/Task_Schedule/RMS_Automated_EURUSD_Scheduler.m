
try
    
    Add_Folders_SubFoleders_Path;
    
    ObjDB =connect_to_database;
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %% Main Function
    
    [OutErrorMsg ,OutXLSfileName ] = generate_EURUSD_report();
    
    if(~strcmpi(OutErrorMsg,'No Errors'))
        Error_Report_Staus = {'Error in generate_EURUSD_report'};
    else
        
        %% Send Email to Corresponding Users
        
        System_Name = upper(getComputerName()) ;
        
        Email_Subject = ['Automated EURUSD Report for COB ''',char(Settle_Date),''' '] ;
        try
            sendmail({'raghavendra.sn@olamnet.com',...
                'muthukarthik.g@olamnet.com','rms.clientservices@olamnet.com','rms.central@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{OutXLSfileName});
            
        catch
            sendmail({'raghavendra.sn@olamnet.com','muthukarthik.g@olamnet.com'},['Error in ',Email_Subject],[' No Attachement ',Email_Subject,' from ',System_Name]);
        end
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    %% Send Email to Corresponding Users
    
    System_Name = upper(getComputerName()) ;
    
    Email_Subject = ['Automated EURUSD Report for COB ''',char(Settle_Date),''' '] ;
    try
        sendmail({'raghavendra.sn@olamnet.com','muthukarthik.g@olamnet.com'},['Error in ',Email_Subject],[' No Attachement ',Email_Subject,' from ',System_Name]);
    catch
        
    end
    
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end