clc
clear

try
    Add_Folders_SubFoleders_Path;
    
    ObjDB = connect_to_database;
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    System_Name = upper(getComputerName()) ;
    
    %% Main Function
    
    run_settle_recon_view;
    
    %% Send Email to Corresponding Users
    Email_Subject = ['Automated SettleRecon View Report for COB ''',char(Settle_Date),''' '] ;
    
    try
        sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Finished ',Email_Subject,' from ',System_Name]);
        
    catch
        sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Finished ',Email_Subject,' from ',System_Name]);
    end
    
catch ME
    
    %% Send Email to Corresponding Users
    
    try
        sendmail({'raghavendra.sn@olamnet.com'},['Error in ',Email_Subject],['Not Finished ',Email_Subject,' from ',System_Name]);
        
    catch
        sendmail({'raghavendra.sn@olamnet.com'},['Error in ',Email_Subject],['Not Finished ',Email_Subject,' from ',System_Name]);
    end
    
    %%
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end