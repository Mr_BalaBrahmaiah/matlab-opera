function [OutErrorMsg] = generate_all_RMO_reports_Scheduler()

Add_Folders_SubFoleders_Path;

%%
OutErrorMsg = {'No errors'};

ObjDB = connect_to_database ;
SettlementDate = fetch(ObjDB,'select settlement_date from valuation_date_table');

TextFile = strcat('RMO_Report_LogFile_',strrep(char(strrep(datestr(now,0),':','_')),' ','_'),'.txt');
diary(TextFile) ;

System_Name = upper(getComputerName()) ;

% All_BU = {'CFS','QF1','QF2','QF3','QF4','USG','OGP'};
% 
% RMO_Reports_Button = {'Options Report','Futures Report','FX Report','OTC Report','Netting PnL Reports'};
% %'Trade Query Report',

%%% Newly added code on 03-12-2019
time = cellstr(datestr(now,'HH'));
schedule_time = cellstr('06'); %%% 11.30 for OGP Trade Query Report
schedule_time1 = cellstr('15');
if strcmpi(time,schedule_time1)
    All_BU = {'CFS','QF1','QF2','QF3','QF4','USG'};
    RMO_Reports_Button = {'Options Report','Futures Report','FX Report','Trade Query Report','OTC Report','Netting PnL Reports','Open Position Recon'};
else strcmpi(time,schedule_time)
    All_BU = {'OGP'};
    RMO_Reports_Button = {'Trade Query Report',};
end

%% Moving Static Path File

Excel_File_Location = fullfile(pwd,'Moving_Static_Path\Moving_to_Static_Path.xlsx');
[~,~,RawData] = xlsread(Excel_File_Location,'RMO_Reports');
Moving_StaticPath_Data = cell2dataset(RawData);

%%
Overall_ErrorMsg = [];
Overall_Reports = [];

for i = 1 : length(All_BU)
    
    InBUName = char(All_BU(i));
    
    for ii = 1 : length(RMO_Reports_Button)
        
        Current_Report_Button =  RMO_Reports_Button(ii);
        
        Matched_MovingPath_Index = strcmpi(Moving_StaticPath_Data.BUSINEES_UNIT,InBUName) & strcmpi(Moving_StaticPath_Data.Web_page_Button,Current_Report_Button);
        if(~isempty(find(Matched_MovingPath_Index)))
            Current_DestinationFolder = Moving_StaticPath_Data.Moving_Static_Path{Matched_MovingPath_Index};
            
            try
                if(iscell(Current_DestinationFolder))
                    Current_DestinationFolder = char(Current_DestinationFolder);
                end
                if(isempty(Current_DestinationFolder))
                    Current_DestinationFolder = []; % pwd;
                end
                if(isnan(Current_DestinationFolder))
                    Current_DestinationFolder = [] ; % pwd;
                end
            catch
                Current_DestinationFolder = [] ;
            end
        else
            OutErrorMsg1 = {'Not Available in Excel Sheet'};
            Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
            continue;
        end
        
        %% Options Report
        try
            if(strcmpi(Current_Report_Button,'Options Report'))
                [OutErrorMsg1,OptionReport] = generate_options_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,OptionReport);
                    Overall_Reports = [Overall_Reports ; cellstr(OptionReport)];
                    
                    moving_report_to_corresponding_folder(OptionReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Futures Report
        try
            if(strcmpi(Current_Report_Button,'Futures Report'))
                [OutErrorMsg1,FutureReport] = generate_futures_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FutureReport);
                    Overall_Reports = [Overall_Reports ; cellstr(FutureReport)];
                    
                    moving_report_to_corresponding_folder(FutureReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% FX Report
        try
            if(strcmpi(Current_Report_Button,'FX Report'))
                [OutErrorMsg1,FX_Report] = generate_currency_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,FX_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(FX_Report)];
                    
                    moving_report_to_corresponding_folder(FX_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        %% Trade Query Report
        try
            if(strcmpi(Current_Report_Button,'Trade Query Report'))
                [OutErrorMsg1,TradeQueryReport] = generate_trade_query_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = strcat('Not Successful - ',OutErrorMsg1);
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                    if strcmpi(InBUName,'OGP')
                        RMO_Report_Status_File = getXLSFilename('RMO_Report_Status_File');
                        xlswrite(RMO_Report_Status_File,Overall_ErrorMsg);
                        Email_Subject = ['OGP RMO Report Status for COB ''',char(SettlementDate),''' '] ;
                        sendmail({'usg.mo@olamnet.com','raghavendra.sn@olamnet.com','balab.s@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],RMO_Report_Status_File);
                    end
                else
                    if strcmpi(InBUName,'OGP')
                        Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                        sendmail({'carl.desjardins@olamnet.com','joyce.gan@olamnet.com','usg.mo@olamnet.com','raghavendra.sn@olamnet.com','balab.s@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{TradeQueryReport});
                    end
                    sendmail_to_corresponding_user(Email_Subject,TradeQueryReport);
                    Overall_Reports = [Overall_Reports ; cellstr(TradeQueryReport)];
                    
                    moving_report_to_corresponding_folder(TradeQueryReport,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% OTC Report
        try
            if(strcmpi(Current_Report_Button,'OTC Report'))
                [OutErrorMsg1,OTC_Report] = generate_riskoffice_rms_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,OTC_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(OTC_Report)];
                    
                    moving_report_to_corresponding_folder(OTC_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end
        
        %% Netting PnL Reports
        try
            if(strcmpi(Current_Report_Button,'Netting PnL Reports'))
                [OutErrorMsg1,NettingPNL_Report] = generate_netting_pnl_report(InBUName);
                if ~strcmpi(OutErrorMsg1,'No errors')
                    OutErrorMsg1 = {'Not Successful'};
                    Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                else
                    Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                    sendmail_to_corresponding_user(Email_Subject,NettingPNL_Report);
                    Overall_Reports = [Overall_Reports ; cellstr(NettingPNL_Report)];
                    
                    moving_report_to_corresponding_folder(NettingPNL_Report,Current_DestinationFolder,Current_Report_Button);
                end
                continue;
            end
        catch
            continue;
        end

        %% Open Position Recon Report
        try
            if ~strcmpi(InBUName,'QF3')
                if(strcmpi(Current_Report_Button,'Open Position Recon'))
                    [OutErrorMsg1,Open_Position_Recon_Report] = generate_broker_open_position_trade_recon(InBUName);
                    if ~strcmpi(OutErrorMsg1,'No errors')
                        OutErrorMsg1 = {'Not Successful'};
                        Overall_ErrorMsg = [Overall_ErrorMsg ;[{InBUName},Current_Report_Button ,OutErrorMsg1]];
                    else
                        Email_Subject = [char(InBUName),' RMO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
                        sendmail_to_corresponding_user(Email_Subject,Open_Position_Recon_Report);
                        Overall_Reports = [Overall_Reports ; cellstr(Open_Position_Recon_Report)];

                        moving_report_to_corresponding_folder(Open_Position_Recon_Report,Current_DestinationFolder,Current_Report_Button);
                    end
                    continue;
                end
            else
                continue;
            end
        catch
            continue;
        end
    end % Second Loop
    
    %% Sending Mail
    
    %     Email_Subject = ['MO Report ',char(Current_Report_Button),' for COB ''',char(SettlementDate),''' '] ;
    %
    %     for k = 1 : size(Overall_Reports,1)
    %
    %         sendmail({'raghavendra.sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{Overall_Reports(k,:)});
    %
    %     end
    
    %     Overall_Reports = [] ; %% For Next Loop
    
end %% First Loop


%% Status Report

if(~isempty(Overall_ErrorMsg))
    
    RMO_Report_Status_File = getXLSFilename('RMO_Report_Status_File');
    xlswrite(RMO_Report_Status_File,Overall_ErrorMsg);
    
    Email_Subject = ['RMO Report Status for COB ''',char(SettlementDate),''' '] ;
    sendmail({'raghavendra.sn@olamnet.com','balab.s@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],RMO_Report_Status_File);
    
end
