try
    
    Add_Folders_SubFoleders_Path;
    
    %%
    ObjDB =connect_to_database;
    
    InBUName = 'qf5';
    
    Settle_Date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    Trade_Date = Settle_Date;
    
    %%
    Finished_Report_Status = {'Finished_Report_Status'};
    Error_Report_Staus = {'Error_Report_Staus'};
    Overall_EMail_Report = [];
    
    %% FTD Report
    try
        [OutErrorMsg,OutXLSName] = QF5_FTD_Trades(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'FTD Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'FTD Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in FTD Report'}];
    end
    
    %% Purchase Schedule
    
    try
        [OutErrorMsg,OutXLSName] = QF5_Purchase_Schedule(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Purchase_Schedule Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Purchase Schedule Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Purchase_Schedule Report'}];
    end
    
    %% Sale Schedule
    
    try
        [OutErrorMsg,OutXLSName] = QF5_Sales_Schedule(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Sales_Schedule Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Sale Schedule Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in UnRealized Report'}];
    end
    
    %% Unrealized Report
    try
        [OutErrorMsg ,OutXLSName ] = QF5_UnRealised_Report(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished Unrealized Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Unrealized Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in UnRealized Report'}];
    end
    
    %% Realised Report  FTD
    try
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_FTD(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Realised_Report_FTD Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Realised FTD Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Realised_Report_FTD Report'}];
    end
    
    %% Realised Report Cumulative - FTM
    try
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTM(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Realised_Report_Cumulative_FTM Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Realised FTM Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Realised_Report_Cumulative_FTM Report'}];
    end
    
    %% Realised Report Cumulative - FTY
    try
        [OutErrorMsg , OutXLSName] = QF5_Realised_Report_Cumulative_FTY(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Realised_Report_Cumulative_FTY Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'Realised FT Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Realised_Report_Cumulative_FTY Report'}];
    end
    
    %% Commission Report
    try
        [OutErrorMsg ,OutXLSName ] = QF5_Commission_Calculation(InBUName);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Commission_Calculation Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'FTD Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Commission_Calculation Report'}];
    end
    
    %% Borrrowing Report
    try
        [OutErrorMsg ,OutXLSName ] = QF5_Borrowing_InterestRate_Calculation(InBUName,Trade_Date,Settle_Date);
        Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Borrowing_InterestRate_Calculation Report'}];
        Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
        cprintf('key','%s finished\n', 'FTD Report');
    catch
        Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Borrowing_InterestRate_Calculation Report'}];
    end
    
    %% Commission FTM/FTY
    %     try
    %         [OutErrorMsg ,OutXLSName ] = QF5_Commission_Calculation_FTM_FTY(InBUName,From_Date,End_Date);
    %         Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Commission_Calculation_FTM_FTY Report'}];
    %         Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
    %         cprintf('key','%s finished\n', 'Commission FTM/FTY Report');
    %     catch
    %         Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Commission_Calculation_FTM_FTY Report'}];
    %     end
    
    %% Borrowing FTM/FTY
    
    %     try
    %         [OutErrorMsg ,OutXLSName ] = QF5_Borrowing_InterestRate_Calculation_FTM_FTY(InBUName,From_Date,End_Date);
    %         Finished_Report_Status = [Finished_Report_Status ; {'Finished QF5_Borrowing_InterestRate_Calculation_FTM_FTY Report'}];
    %         Overall_EMail_Report = [Overall_EMail_Report ,{OutXLSName}];
    %         cprintf('key','%s finished\n', 'Borrowing FTM/FTY Report');
    %     catch
    %         Error_Report_Staus = [Error_Report_Staus ; {'Error in QF5_Borrowing_InterestRate_Calculation_FTM_FTY Report'}];
    %     end
    
    %% Send Email
    
    EMail_Report_1 = getXLSFilename('QF5_Scheduler_Status');
    xlswrite(EMail_Report_1,Finished_Report_Status,'Finshed_Finished_Report_Status');
    xlswrite(EMail_Report_1,Error_Report_Staus,'Error_Report_Staus');
    
    xls_delete_sheets(fullfile(pwd,EMail_Report_1));
    
    System_Name = upper(getComputerName()) ;
    
    Email_Subject = [upper(char(InBUName)),' Scheduler Report for COB ''',char(Settle_Date),''' '] ;
    try
        sendmail({'Raghavendra.Sn@olamnet.com'},Email_Subject,['Attached ',Email_Subject,' from ',System_Name],{EMail_Report_1}); %% 'raghavendra.sn@olamnet.com'
        
    catch
        sendmail({'Raghavendra.Sn@olamnet.com'},['Error in ',Email_Subject],[' No Attachement ',Email_Subject,' from ',System_Name]);
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    %     uiwait(warndlg(errorMessage));
    
end