function moving_report_to_corresponding_folder(Filename,DestinationFolder,Current_Report_Button)
try
    
    if iscell(DestinationFolder)
        DestinationFolder = char(DestinationFolder);
    end
    
    if(~isempty(DestinationFolder))
        
        OutFilename = fullfile(DestinationFolder,Filename);
        move_reports(Filename,OutFilename,char(Current_Report_Button));
        
    end
    
catch
    
end
end