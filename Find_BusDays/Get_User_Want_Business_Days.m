function [Start_Date,Expiry_Date,Bus_Days] = Get_User_Want_Business_Days(varargin) %% NoDaysShift

% NoDaysShift = 2;

if nargin == 1
    NoDaysShift = varargin{1};
    User_Want_Days = NoDaysShift;
    Today_Value_Date = today;
else
    NoDaysShift = varargin{1};
    User_Want_Days = NoDaysShift;
    
    if(~isempty(varargin{2}))
        Today_Value_Date = datenum(varargin{2});
    else
        Today_Value_Date = today;
    end
end



%%

% ObjDB = connect_to_database;
% Value_Settele_Date = fetch(ObjDB,'SELECT value_date,settlement_date FROM valuation_date_table');
% Current_ValueDate = Value_Settele_Date(1);
% Settlement_Date =  Value_Settele_Date(2);

%%

[System_Date_Format,System_Date] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

HolDates = {'2016-01-01','2017-01-01','2017-04-14'};
HolidayVec = datenum(HolDates,DB_Date_Format);

DayForm = 'long';
language = 'en_US';

%%

Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),System_Date_Format);
Expiry_Date = datestr(busdate(Today_Value_Date + 1  , -1, HolidayVec),System_Date_Format);

[DayNumber,DayName] = weekday(datenum(Expiry_Date,System_Date_Format),DayForm,language);
DayName = cellstr(DayName) ;
DayNumber = num2cell(DayNumber);

if(strcmpi(char(DayName),'Monday'))
    
    if(NoDaysShift>1)
        NoDaysShift = NoDaysShift + 1 ;
    end
    
    Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),System_Date_Format);
    Expiry_Date = datestr(busdate(Today_Value_Date + 1  , -1, HolidayVec),System_Date_Format);
    
    VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(Expiry_Date,System_Date_Format),'daily',HolidayVec);
    
    Bus_Days = cellstr(datestr(VBusDays,DB_Date_Format));
    
elseif(strcmpi(char(DayName),'Tuesday'))
    
    if(NoDaysShift==1)
        NoDaysShift = NoDaysShift - 1 ;
    else
        NoDaysShift = NoDaysShift + 1 ;
    end
    
    Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),System_Date_Format);
    Expiry_Date = datestr(busdate(Today_Value_Date + 1  , -1, HolidayVec),System_Date_Format);
    
    VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(Expiry_Date,System_Date_Format),'daily',HolidayVec);
    
    Bus_Days = cellstr(datestr(VBusDays,DB_Date_Format));
    
else
    if(cell2mat(DayNumber)>3)
        Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift+1 , -1, HolidayVec),System_Date_Format);
    else
        Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),System_Date_Format);
    end
    Expiry_Date = datestr(busdate(Today_Value_Date + 1  , -1, HolidayVec),System_Date_Format);
    
    %     Day_Diff = daysact(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),  busdate(Today_Value_Date + 1  , -1, HolidayVec));
    Day_Diff = days252bus(datestr(datenum(Start_Date,System_Date_Format),23), datestr(datenum(Expiry_Date,System_Date_Format),23));
    
    if(Day_Diff == User_Want_Days)
        
        VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(Expiry_Date,System_Date_Format),'daily',HolidayVec);
        
        Bus_Days = cellstr(datestr(VBusDays,DB_Date_Format));
    else
        aa = 1;
        
        while(aa)
            
            if(NoDaysShift <= 3)
                NoDaysShift = NoDaysShift - 1 ;
            else
                NoDaysShift = NoDaysShift + 1 ;
            end
            
            Start_Date = datestr(busdate(Today_Value_Date - NoDaysShift , -1, HolidayVec),System_Date_Format);
            Expiry_Date = datestr(busdate(Today_Value_Date + 1  , -1, HolidayVec),System_Date_Format);
            
            Day_Diff = days252bus(datestr(datenum(Start_Date,System_Date_Format),23), datestr(datenum(Expiry_Date,System_Date_Format),23));
            
            if(Day_Diff == User_Want_Days)
                aa =0;
                
                VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(Expiry_Date,System_Date_Format),'daily',HolidayVec);
                
                Bus_Days = cellstr(datestr(VBusDays,DB_Date_Format));
            end
            
        end
        
    end
    
end

%%

aa = 1;
while(aa)
    if(size(Bus_Days,1) == 3) %% Hard code
        aa = 0;
    elseif(size(Bus_Days,1) > 3)
        Bus_Days = Bus_Days(end-2:end) ;
        aa=0;
    else
        User_Want_Days = User_Want_Days + 1;
        [Start_Date,Expiry_Date,Bus_Days] = Get_User_Want_Business_Days(User_Want_Days) ;
    end
    
    
end


