clc
clear

global ObjDB;
ObjDB = connect_to_database;

%%
try
    hWaitbar = waitbar(0,'Please wait...','Name','Processing.....');
    
    ViewName = 'helper_fetch_holidays_for_products_view' ;
    
    [ColNames,Data] = read_from_database(ViewName,0) ;
    
    ProductCode_Col = find(cellfun(@(V) strcmpi('product_code',V), ColNames)) ;
    Holiday_Col = find(cellfun(@(V) strcmpi('holiday',V), ColNames)) ;
    
    ProductCode_Data = Data(:,ProductCode_Col);
    Holiday_Data = Data(:,Holiday_Col);
    
    Unique_ProductCode = unique(ProductCode_Data);
    
    % Unique_ProductCode = [Unique_ProductCode(22) ; Unique_ProductCode(27) ; Unique_ProductCode(23) ; Unique_ProductCode(18) ] ; %% For Testing
    
    %%
    
    [System_Date_Format,System_Date] = get_System_Date_Format();
    
%     Start_Date = datestr('2016-01-01' , System_Date_Format); % old logic
%     End_Date =  datestr('2020-12-31' , System_Date_Format) ;
 
    %%% Newly added logic on 29-01-2020, for getting automatic current year Start date
    %and End date fo
    Prompt        = {'Enter the Start Date(dd-mm-yyyy format):','Enter the End Date(dd-mm-yyyy format):'};
    Name          = 'Product BusDays Parser';
    Numlines      = 1;
    Defaultanswer = {char(strcat({'01-'},{'01-'},datestr(today,'yyyy'))),char(strcat({'31-'},{'12-'},datestr(today,'yyyy')))};
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);

    if isempty(InputDates) || strcmpi(InputDates(1),'') || strcmpi(InputDates(2),'')
        errordlg('Parser will not run since the date input was not given!');
        return;
    end
    
    Start_Date = InputDates(1);
    End_Date = InputDates(2);
    
    if isdeployed
        if(strcmpi(System_Date_Format,'dd-mm-yyyy'))
            DateNum_Conv = 'mm/dd/yyyy';
        else
            DateNum_Conv = 'dd-mm-yyyy';
        end
        if ~(datenum(Start_Date,DateNum_Conv) < datenum(End_Date,DateNum_Conv))
            errordlg('Start date must be less than End date,Kindly re-enter the dates') ;
            return;
        end
    else
        if ~(datenum(Start_Date,System_Date_Format) < datenum(End_Date,System_Date_Format))
            errordlg('Start date must be less than End date,Kindly re-enter the dates') ;
            return;
        end
    end
    % end code
    
    DB_Date_Format = 'yyyy-mm-dd';
    
    % Holiday_Data = cellstr( datestr( datenum(Data(:,Holiday_Col),System_Date_Format),System_Date_Format ) );
    
    %%
    
    DayForm = 'long';
    language = 'en_US';
    % [DayNumber,DayName] = weekday(D,DayForm,language);
    
    %%
    
    Total_Data = [];
    
    for i = 1 : size(Unique_ProductCode,1)
        
        Current_ProductCode = Unique_ProductCode(i) ;
        
        Matched_ProductCode_Index = cellStrfind_exact(ProductCode_Data , Current_ProductCode) ;
        
        Current_HodidayVec = char( Holiday_Data(Matched_ProductCode_Index) );
        
        VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(End_Date,System_Date_Format),'daily',Current_HodidayVec);
        
        Temp_BusDate = cellstr(datestr(VBusDays,DB_Date_Format)); %% DB_Date_Format
        
        Temp_ProductCode = cell(size(VBusDays,1),1) ;
        Temp_ProductCode(:,:) = Current_ProductCode ;
        
        [Ouput_Array] = NumDays_Diff_4_Two_Dates_2_MissingDates([Temp_ProductCode , Temp_BusDate]);  %% Find Missing Dates
        
        ValueDate_Array = datestr(Ouput_Array(:,2) ,System_Date_Format); %% Day Name of Value Date
        [DayNumber,DayName] = weekday(datenum(ValueDate_Array,System_Date_Format),DayForm,language);
        DayName = cellstr(DayName) ;
        DayNumber = num2cell(DayNumber);
        
        Temp_Data = [Ouput_Array , DayName, DayNumber] ;
        
        Total_Data = [Total_Data  ; Temp_Data ];
        
        fprintf('%d : %d\n',i,size(Unique_ProductCode,1));
        
        clear Ouput_Array Temp_Data ;
        
        waitbar(i/size(Unique_ProductCode,1),hWaitbar,'Processing.....');
        
    end
    
    %%
    
    OutXLSFileName = getXLSFilename('Product_BusDays');
    
    Header = {'product_code' , 'value_date' , 'settle_date', 'holiday' ,'business_day_name' , 'business_day_number'} ;
    xlswrite(OutXLSFileName, [Header ; Total_Data] , 'Product_BusDays');
    
    XLS_File_Path = [pwd filesep OutXLSFileName];
    xls_delete_sheets(XLS_File_Path);
    
    waitbar(1,hWaitbar,'Process Finished');
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end












%% Find Value Dates for two Years
% HodidayVec = {'2010-05-05'} ;
% HodidayVec = char(datestr(HodidayVec , System_Date_Format));
%
% VBusDays = busdays(datenum(Start_Date,System_Date_Format), datenum(End_Date,System_Date_Format),'daily',HodidayVec);
%
% Value_Date_Years = cellstr(datestr(VBusDays,DB_Date_Format));

% for i = 1 : size(Unique_ProductCode,1)
%
%     Current_ProductCode = Unique_ProductCode(i) ;
%
%     Matched_ProductCode_Index = cellStrfind_exact(ProductCode_Data , Current_ProductCode) ;
%
%     Current_HodidayVec = char( Holiday_Data(Matched_ProductCode_Index) );
%
%     Find_Holidays_ValueDate_Vector = setdiff(Current_HodidayVec,Value_Date_Years) ;
%
%     Remove_Holiday_Index = cellStrfind_exact(Value_Date_Years , Find_Holidays_ValueDate_Vector) ;
%
%     Value_Date_Years(Remove_Holiday_Index) = [];
% end

%%




