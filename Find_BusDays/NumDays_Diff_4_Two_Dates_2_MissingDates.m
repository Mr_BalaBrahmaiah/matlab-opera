function [Total_Data] = NumDays_Diff_4_Two_Dates_2_MissingDates(Cell_Array)

ProductCode_Array =  Cell_Array(:,1) ;
Date_Array = Cell_Array(:,2) ;

%%
global ObjDB;
Current_ProductCode = unique(ProductCode_Array);
SqlQuery_Holiday = ['select holiday from helper_fetch_holidays_for_products_view where product_code = ''',char(Current_ProductCode),''' '] ;
[~,Holiday_Tbl_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Holiday);

%%
[System_Date_Format,System_Date] = get_System_Date_Format();
DB_Date_Format = 'yyyy-mm-dd';

DayForm = 'long';
language = 'en_US';

%%
% Ouput_Array = cell(size(Date_Array,1) ,1);
% SettleDate_Array =  cell(size(Date_Array,1) ,1);
% datestr(datenum('2017-01-26') : datenum('2017-01-26')+8 )

ProductCode = {''};
DayDiff_Array = {''};
SettleDate_Array = {''};
ValueDate_Array = {''};


count = 1;
for i = 1 : size(Date_Array,1)
    
    Current_Product = ProductCode_Array(i,1);
    
    if( i==1 ||  i == size(Date_Array,1))
        
        ProductCode(count,1) = Current_Product;
        ValueDate_Array(count,1) = Date_Array(i,1);
        
        if(i==1)
            DayDiff_Array{count,1} = {''};
            SettleDate_Array{count,1} = {''};
        else
            DayDiff_Array{count,1} = {''};
            SettleDate_Array(count,1) = Date_Array(i-1,1);
            
        end
        
        
        count = count + 1;
        
    else
        
        First_Date  = char( Date_Array(i,1) ) ;
        Second_Date = char( Date_Array(i+1 , 1) ) ;
        SettleDate =  ValueDate_Array(end) ; %% Date_Array(i-1 , 1) ;
        
        Day_Diff = daysact(First_Date,  Second_Date) ;
        DayDiff_Array(count,1) = num2cell(Day_Diff) ;
        
        
        if(Day_Diff>=2)
            
            Current_Line_DayDiff = Day_Diff - 1 ;
            
            Missing_Dates = datestr(datenum(First_Date) : datenum(First_Date) + Current_Line_DayDiff ,System_Date_Format) ;
            
            [DayNumber,DayName] = weekday(datenum(Missing_Dates,System_Date_Format),DayForm,language);
            DayName = cellstr(DayName) ;
            DayNumber = num2cell(DayNumber);
            
            Temp_Matrix = [cellstr(Missing_Dates) , DayName , DayNumber ];
            
            Sunday_Saturday_Index = cellStrfind_exact(Temp_Matrix(:,2),{'Sunday','Saturday'}) ;
            Temp_Matrix(Sunday_Saturday_Index,:) = [] ; %% Remove Sunday and Saturday
            
            Current_ProductCode = cell(size(Temp_Matrix,1),1);
            Current_ProductCode(:,:) = Current_Product ;
            
            Value_Date_Cell = Temp_Matrix(:,1);             %% Find Missing Dates
            Value_Date_Cell = cellstr(datestr(datenum(Value_Date_Cell,System_Date_Format),DB_Date_Format));
            
            Matched_DateIdx = cellStrfind_exact(Holiday_Tbl_Data,SettleDate) ; %% If SettleDate Holiday change to the SettleDate-1 to SettleDate
            While_Count = 1;
            while(Matched_DateIdx)
                SettleDate = ValueDate_Array(end-While_Count);
                Matched_DateIdx = cellStrfind_exact(Holiday_Tbl_Data,SettleDate);
                While_Count = While_Count + 1;
                if(While_Count==7)
                    Matched_DateIdx = 0;
                end
            end
            
            Settle_Date_Cell = cell(size(Temp_Matrix,1),1);
            %             Settle_Date_Cell(:,:) = SettleDate ; %% {First_Date} ; %% SettleDate;
            Settle_Date_Cell(1,1) = SettleDate ;
            Settle_Date_Cell(2:end,:) = {First_Date} ;
            
            count = count + size(Settle_Date_Cell,1) ;
            
            ProductCode = [ProductCode ;Current_ProductCode];
            ValueDate_Array = [ValueDate_Array ; Value_Date_Cell];
            SettleDate_Array = [SettleDate_Array ; Settle_Date_Cell];
            
            
            clear Temp_Matrix;
            
        else
            Matched_DateIdx = cellStrfind_exact(Holiday_Tbl_Data,SettleDate);
            While_Count = 1;
            while(Matched_DateIdx)
                SettleDate = ValueDate_Array(end-While_Count);
                Matched_DateIdx = cellStrfind_exact(Holiday_Tbl_Data,SettleDate);
                While_Count = While_Count + 1;
                if(While_Count==7)
                    Matched_DateIdx = 0;
                end
            end
            
            ProductCode(count,1) = Current_Product;
            ValueDate_Array(count,1) = {First_Date};
            SettleDate_Array(count,1) = SettleDate;
            
            count = count + 1;
        end
        
        
    end
    
    
end

%%
Holiday_Array = cell(size(ValueDate_Array,1),1);
Holiday_Index = cellStrfind_exact(ValueDate_Array,Holiday_Tbl_Data);
Holiday_Array(Holiday_Index,1) = {'holiday'};

Total_Data  = [ProductCode , ValueDate_Array , SettleDate_Array , Holiday_Array] ;



