function [OutErrorMsg] =  Broker_Commission_Report_Exe()

try
    OutErrorMsg = {'No Errors'};
    
    ObjDB = connect_to_database ;
    Current_SettleDate = fetch(ObjDB,'select settlement_date from valuation_date_table');
    
    %%
    
    Prompt        = {'Enter the From Date(yyyy-mm-dd format):','Enter the BU Name:'};
    Name          = 'Inputs for Broker Commission Parser';
    Numlines      = 1;
    Defaultanswer = {char(Current_SettleDate),'CFS'};  %%% default BU is 'CFS"
    InputDates    = inputdlg(Prompt,Name,Numlines,Defaultanswer);
    
    if isempty(InputDates)
        errordlg('Parser will not run since the date & BU input was not given!');
        return;
    end
    
    FromDate = InputDates(1);
    InBUName = InputDates(2);
    
    %% Overall Day
    SqlQuery_Overall_SettleDate_ValueDate =['select * from vdate_sdate_table where settlement_date =''',char(FromDate),''' ' ];
    [~,Overall_SettleDate_ValueDate]  = Fetch_DB_Data(ObjDB,SqlQuery_Overall_SettleDate_ValueDate);
    Overall_ValueDate = Overall_SettleDate_ValueDate(:,1);
    Overall_SettleDate = Overall_SettleDate_ValueDate(:,2);
    
    %%
    hWaitbar = waitbar(0,'Please wait...');
    
    for i = 1 : length(Overall_SettleDate)
        Current_ValueDate = Overall_ValueDate(i);
        Current_SettleDate = Overall_SettleDate(i);
        
        [OutErrorMsg ,OutXLSName ] = broker_commission_report(InBUName,Current_SettleDate);
        
        if(strcmpi(OutErrorMsg,'No Errors'))
            TimeStamp = strsplit(datestr(now),' ');
            TimeStamp = strrep(TimeStamp(end),':','-');
            Date_TimeStamp = [char(datestr(Current_ValueDate)),'_',char(TimeStamp)];
            Final_XLS_FileName = strcat('broker_commission_',lower(char(InBUName)),'_',Date_TimeStamp,'.xlsx'); %%  We Need to move to Static Report Page
            
            copyfile(OutXLSName,fullfile(pwd,Final_XLS_FileName));
            
            delete(fullfile(pwd,OutXLSName));
            
        else
            errordlg(OutErrorMsg);
            continue;
        end
        
        waitbar(i/length(Overall_SettleDate),hWaitbar,[char(Current_SettleDate), ' Finished']);
        
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);
    
    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
        ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
end