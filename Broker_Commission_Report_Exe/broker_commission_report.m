function [OutErrorMsg ,OutXLSName ] = broker_commission_report(InBUName,Temp_Settle_Date)

%%% Project Name: Broker Module
%%% Module Name: Broker Commission
%%% Matlab Version(s): R2016b
%%% Company: Invenio Commodity Services Private Limited

%%% Author Name: INVENIO
%%% Date_Time: 08 Jan 2019 / 19:03:03

%%%% [OutErrorMsg ,OutXLSName ] = broker_commission_report('qf1')
%%%% [OutErrorMsg ,OutXLSName ] = broker_commission_report('cfs','2019-01-03')

try   
    OutErrorMsg = {'No Errors'};
    OutXLSName = getXLSFilename(['broker_commission_',lower(char(InBUName))]);  % output file name
    
    if iscellstr(InBUName)
        InBUName = char(InBUName);
    end

    %%% Connect to Database
    ObjDB = connect_to_database;

    %%Fetch the Settlement_date
    if(exist('Temp_Settle_Date','var'))
        Settele_date = Temp_Settle_Date;
    else
        Settele_date = fetch(ObjDB,'select settlement_date from valuation_date_table');
    end

    %%% Fetch data from Broker Counterparty Mapping Table
    Broker_counter_Mapping_Table = 'broker_counterparty_mapping_table';
    SqlQuery_Broker_counter_Mapping_Table = ['select sub_counterparty, counterparty_parent from ',char(Broker_counter_Mapping_Table),' where business_unit = ''',char(InBUName),''' and flag = ''YES'' '] ;
    [~,Broker_counter_Mapping_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_counter_Mapping_Table);

    if strcmpi(Broker_counter_Mapping_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Broker_counter_Mapping_Table)]} ;
        return;
    end
    %%% sub_counterparty field and counterparty_parent field data
    sub_counterparty = Broker_counter_Mapping_Table_Data(:,1);
    counterparty_parent = unique(Broker_counter_Mapping_Table_Data(:,2));
    
    %%% SQL query (pass multiple arguments at time)
    sub_counterparty_sql_Query = ['''',strrep(convertCell2Char(sub_counterparty),',',''','''),''''];
    counterparty_parent_sql_Query = ['''',strrep(convertCell2Char(counterparty_parent),',',''','''),''''];

    %%% Fetch data from Broker Commission Table
    Broker_Commision_Mapping_Table = ['broker_commission_master_table_',char(InBUName)];
    SqlQuery_Broker_Commision_Table = ['select * from ',char(Broker_Commision_Mapping_Table),' where counterparty_parent  in (',counterparty_parent_sql_Query,') '];
    [Broker_Commision_Table_Cols,Broker_Commision_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_Commision_Table);

    if strcmpi(Broker_Commision_Table_Data,'No Data') 
        OutErrorMsg = {['Data not Available in ',char(Broker_Commision_Mapping_Table)]} ;
        return;
    end
    
    Broker_Commision_Rate_Tbl = cell2dataset([Broker_Commision_Table_Cols;Broker_Commision_Table_Data]);  %% Convert to dataset
    
    %%% Fetch data from Opera Commission Table (deal_ticket_table_brokerage)
    TableName=['deal_ticket_table_brokerage_view_',char(InBUName)];
    SqlQuery_Opera_Table = ['select * from ',char(TableName),' where transaction_date = ''',char(Settele_date),''' and counterparty_parent in (',sub_counterparty_sql_Query,') '] ;
    [Opera_Table_Cols, Opera_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Opera_Table);

    if(isempty(Opera_Table_Data))
        OutErrorMsg = {['Data is not available in ',char(TableName),' for ',char(Settele_date)]};
        return;
    end

    if(strcmpi(Opera_Table_Data,'No Data'))
        OutErrorMsg = {['Data is not available in ',char(TableName),' for ',char(Settele_date)]};
        return;
    end
  
    Opera_Commision_Rate_Tbl = cell2dataset([Opera_Table_Cols; Opera_Table_Data]);  %% Convert to dataset
  
    Currency_sql_Query = ['''',strrep(convertCell2Char(unique(Broker_Commision_Rate_Tbl.currency)),',',''','''),''''];
    %%% Fetch data from Currency Spot Mapping Table
    Currency_Spot_Table_Name = 'currency_spot_mapping_table';
    SqlQuery_DVD_FX_Rate = ['select * from ',char(Currency_Spot_Table_Name),' where currency in (',Currency_sql_Query,') '] ;
    [~,Currency_Spot_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_DVD_FX_Rate);

    if strcmpi(Currency_Spot_Table_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Currency_Spot_Table_Name)]} ;
        return;
    end
    
    Spot_value_sql_Query = ['''',strrep(convertCell2Char(Currency_Spot_Table_Data(:,2)),',',''','''),''''];
    %%% Fetch data from Underlying Settle Value Table
    Underlying_settle_value_Table = 'underlying_settle_value_table';
    SqlQuery_Underlying_settle_value = ['select underlying_id, settle_value, settlement_date from ',char(Underlying_settle_value_Table),' where settlement_date=''',char(Settele_date),''' and underlying_id in (',Spot_value_sql_Query,') '] ;
    [~,Underlying_settle_value_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Underlying_settle_value);

    if strcmpi(Underlying_settle_value_Data,'No Data')
        OutErrorMsg = {['No Data found in ',char(Underlying_settle_value_Table),' for ',char(Settele_date)]} ;
        return;
    end
     
    %%% Get Column Names fromBroker Commission Store Table
    Broker_Commission_Store_Table = ['broker_commission_store_table_',char(InBUName)];
    Broker_Commission_Store_Columns = gettablecolumnnames(ObjDB, Broker_Commission_Store_Table);

    %%% Holidays List (Only 1st January dates), to calculate Bussiness days
    DB_Date_Format = 'yyyy-mm-dd';
    %HolDates = {'2016-01-01','2017-01-01','2018-01-01','2019-01-01','2020-01-01','2021-01-01'};
    HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
    HolidayVec = datenum(HolDates,'yyyy-mm-dd');
    
    %%%% Broker Commission and Opera Commission Mapping Process
    Broker_Commission_Store_Data = [];  Overall_ErrorMsg = [];
    for i = 1:1:size(Opera_Commision_Rate_Tbl,1)
        Opera_TradeDate = Opera_Commision_Rate_Tbl.transaction_date(i);
        Opera_original_lots = abs(Opera_Commision_Rate_Tbl.original_lots(i));
        Opera_counterparty_parent = Opera_Commision_Rate_Tbl.counterparty_parent(i);
        Opera_exe_type = Opera_Commision_Rate_Tbl.exe_type(i);
        Opera_instrument = Opera_Commision_Rate_Tbl.instrument(i);
        Opera_original_premium = Opera_Commision_Rate_Tbl.original_premium(i);
        Opera_murex_mult = Opera_Commision_Rate_Tbl.murex_mult(i);
        %%% Find business days from start_date to end_date
        VBusDays = busdays(min(datenum(Broker_Commision_Rate_Tbl.start_date,DB_Date_Format)), max(datenum(Broker_Commision_Rate_Tbl.end_date,DB_Date_Format)),'daily',HolidayVec);
        BusDays_Array = cellstr(datestr(VBusDays,DB_Date_Format));
        
        %%% Opera_TradeDate
        if isempty(cellStrfind_exact(BusDays_Array,Opera_TradeDate)) 
            OutErrorMsg = {[char(Opera_TradeDate),' date is not found in mapping table range']} ;
            return;
        else
            Index_trade_date = (datenum(Broker_Commision_Rate_Tbl.start_date,DB_Date_Format) <= datenum(Opera_TradeDate,DB_Date_Format) & datenum(Opera_TradeDate,DB_Date_Format) <= datenum(Broker_Commision_Rate_Tbl.end_date,DB_Date_Format));
        end
        %%% counterparty_parent
        Indx_Counterparty = regexpi(Opera_counterparty_parent,Broker_Commision_Rate_Tbl.counterparty_parent);
        Ind_empty = cellfun(@(v) isempty(v),Indx_Counterparty);
        Indx_Counterparty(Ind_empty) = {0};
        
        Matched_Index = find(strcmpi(Opera_instrument,Broker_Commision_Rate_Tbl.instrument) & strcmpi(Opera_exe_type,Broker_Commision_Rate_Tbl.exe_type) & cell2mat(Indx_Counterparty) & Index_trade_date);
        if ~isempty(Matched_Index)
            Broker_Commision_Rate_Data = Broker_Commision_Rate_Tbl(Matched_Index,:);  %%% Broker Data
            Opera_Commision_Rate_Data = Opera_Commision_Rate_Tbl(i,:);   %% Opera Data
         
            for j = 1:1:size(Broker_Commision_Rate_Data,1)
                
                Broker_GiveUp_Table = ['broker_giveup_commission_master_table_',char(InBUName)];
                SqlQuery_Broker_GiveUp = ['select * from ',char(Broker_GiveUp_Table),' where execution_broker = ''',char(Opera_Commision_Rate_Data.execution_broker),''' and instrument = ''',char(Opera_Commision_Rate_Data.instrument), ...
                                            ''' and invenio_product_code = ''',char(Opera_Commision_Rate_Data.invenio_product_code),''' and  broker_product_code = ''',char(Broker_Commision_Rate_Data.broker_product_code(j)),''' and exe_type = ''',char(Opera_Commision_Rate_Data.exe_type),''' '] ;
                [Broker_GiveUp_Table_Cols,Broker_GiveUp_Table_Data] = Fetch_DB_Data(ObjDB,SqlQuery_Broker_GiveUp);

                if strcmpi(Broker_GiveUp_Table_Data,'No Data')
                    Broker_GiveUp_Data.give_up_charge = 0;
                    Broker_GiveUp_Data.comm_calc_lots_notional = 'LOTS';
                    Broker_GiveUp_Data.currency = {' '};
                else
                    Broker_GiveUp_Data = cell2dataset([Broker_GiveUp_Table_Cols;Broker_GiveUp_Table_Data]);  %% Convert to dataset         
                end
                
                %%% Commissin Claculation using Opera lots
                Current_commission_amount = Broker_Commision_Rate_Data.commission_amount(j) .* Opera_original_lots .* -1;
                Current_clearing_fee = Broker_Commision_Rate_Data.clearing_fee(j) * Opera_original_lots .* -1;
                Current_exchange_fee = Broker_Commision_Rate_Data.exchange_fee(j) .* Opera_original_lots .* -1;
                Current_nfa_fee = Broker_Commision_Rate_Data.nfa_fee(j) .* Opera_original_lots .* -1;
                Current_pit_brokerage = Broker_Commision_Rate_Data.pit_brokerage(j) .* Opera_original_lots .* -1;
                Current_execution_charge = Broker_Commision_Rate_Data.execution_charge(j) .* Opera_original_lots .* -1;

                %%% added on 28-01-2020, convert negative zero into Positive zero
                Current_commission_amount(Current_commission_amount==0)=0;
                Current_clearing_fee(Current_clearing_fee==0)=0;
                Current_exchange_fee(Current_exchange_fee==0)=0;
                Current_nfa_fee(Current_nfa_fee==0)=0;
                Current_pit_brokerage(Current_pit_brokerage==0)=0;
                Current_execution_charge(Current_execution_charge==0)=0;
                
                %%% check Tax value and Spcl Rate is NaN or not
                if isnan(Broker_Commision_Rate_Data.tax(j))
                    Broker_Commision_Rate_Data.tax(j) = 0;
                end
                if isnan(Opera_Commision_Rate_Data.spcl_rate)
                   Opera_Commision_Rate_Data.spcl_rate = 0;
                end
                
                Current_adjustment = Opera_Commision_Rate_Data.spcl_rate;
                Current_Tax = (Current_commission_amount + Current_clearing_fee + Current_exchange_fee + Current_nfa_fee + Current_pit_brokerage + Current_execution_charge) .* (Broker_Commision_Rate_Data.tax(j) ./ 100);
                Current_Tax(Current_Tax==0)=0;%%% added on 28-01-2020, convert negative zero into Positive zero

                if strcmpi(Broker_Commision_Rate_Data.comm_calc_lots_notional(j),'LOTS') && strcmpi(Broker_GiveUp_Data.comm_calc_lots_notional,'LOTS')
                    %%% Total Brokerage Net Value Calculation
                    give_up_currency=Broker_GiveUp_Data.currency;
                    Give_up_charge = (Opera_original_lots .* Broker_GiveUp_Data.give_up_charge) .* -1;
                    Give_up_charge(Give_up_charge==0)=0;%%% added on 28-01-2020, convert negative zero into Positive zero
                    Brokerage_Sum_Net_Val = Current_commission_amount + Current_clearing_fee + Current_exchange_fee + Current_nfa_fee + Current_pit_brokerage + Current_execution_charge + Current_Tax + Current_adjustment;
                else  %%%% Notional logic
                    %%% Total Brokerage Net Value Calculation
                    give_up_currency=Broker_GiveUp_Data.currency;
                    Give_up_charge = (Opera_original_lots .* Broker_GiveUp_Data.give_up_charge) .* -1;
                    Give_up_charge(Give_up_charge==0)=0;%%% added on 28-01-2020, convert negative zero into Positive zero
                    Brokerage_Sum_Net_Val = (Current_commission_amount + Current_clearing_fee + Current_exchange_fee + Current_nfa_fee + Current_pit_brokerage + Current_execution_charge + ...
                                           Current_Tax ) .* (Opera_original_premium .* Opera_murex_mult) + Current_adjustment;
                    
                end

                %%% Get the quantotype from Currency Spot Table
                %%% Compare currency
                Index_currency = strcmpi(Broker_Commision_Rate_Data.currency(j),Currency_Spot_Table_Data(:,1));
               
                if ~isempty(Index_currency)
                    quantotype = Currency_Spot_Table_Data(Index_currency,3);
                    Spot_type = Currency_Spot_Table_Data(Index_currency,2);
                   
                else
                    Overall_ErrorMsg = [Overall_ErrorMsg;{[char(Broker_Commision_Rate_Data.currency(j)),' currency is not found in ',char(Currency_Spot_Table_Name)]}];
                    continue;
                end
                
                %%% added rajashekhar
                if strcmpi(Broker_GiveUp_Table_Data,'No Data')
                     give_Sum_Net_USD_Value =0;  
                       gSpot_Value=0;
                else
                    gIndex_currency = strcmpi(Broker_GiveUp_Data.currency(j),Currency_Spot_Table_Data(:,1));
                    if ~isempty(gIndex_currency)
                        gquantotype = Currency_Spot_Table_Data(gIndex_currency,3);
                        gSpot_type = Currency_Spot_Table_Data(gIndex_currency,2);
                        gIndex_Spot_Value = strcmpi(gSpot_type,Underlying_settle_value_Data(:,1)) & strcmpi(Settele_date,Underlying_settle_value_Data(:,3));
                        if ~isempty(gIndex_Spot_Value)
                            gSpot_Value = Underlying_settle_value_Data(gIndex_Spot_Value,2);
                            if (strcmpi(gquantotype,'division'))
                                give_Sum_Net_USD_Value = Give_up_charge ./ cell2mat(gSpot_Value);
                            elseif (strcmpi(gquantotype,'multiplication'))
                               give_Sum_Net_USD_Value = Give_up_charge .* cell2mat(gSpot_Value);
                            end
                        else
                            Overall_ErrorMsg = [Overall_ErrorMsg;{[char(Broker_Commision_Rate_Data.currency(j)),' currency is not found in ',char(Underlying_settle_value_Table)]}];
                            continue;
                        end
                    else
                        Overall_ErrorMsg = [Overall_ErrorMsg;{[char(Broker_GiveUp_Data.currency(j)),' currency is not found in ',char(Currency_Spot_Table_Name)]}];
                        continue;
                    end
                end
               % end
                %%% Get the Spot value from Underlying settle value Table
                %%% Compare currency and Settele_date
                Index_Spot_Value = strcmpi(Spot_type,Underlying_settle_value_Data(:,1)) & strcmpi(Settele_date,Underlying_settle_value_Data(:,3));
                if ~isempty(Index_Spot_Value)
                    Spot_Value = Underlying_settle_value_Data(Index_Spot_Value,2);
                else
                  Overall_ErrorMsg = [Overall_ErrorMsg;{[char(Broker_Commision_Rate_Data.currency(j)),' currency is not found in ',char(Underlying_settle_value_Table)]}];
                  continue;
                end
                
                %%% Overall Brokerage USD Net Value Calculation
                if (strcmpi(quantotype,'division'))
                    Brokerage_Sum_Net_USD_Value = Brokerage_Sum_Net_Val ./ cell2mat(Spot_Value);
                elseif (strcmpi(quantotype,'multiplication'))
                    Brokerage_Sum_Net_USD_Value = Brokerage_Sum_Net_Val .* cell2mat(Spot_Value);
                end
              
                %%%% user required fields        
                trade_id = Opera_Commision_Rate_Data.trade_id;
                transaction_date = Opera_Commision_Rate_Data.transaction_date;
                security_id = Opera_Commision_Rate_Data.security_id;
                portfolio = Opera_Commision_Rate_Data.portfolio;
                subportfolio = Opera_Commision_Rate_Data.subportfolio;
                instrument = Broker_Commision_Rate_Data.instrument(j);
                invenio_product_code = Broker_Commision_Rate_Data.invenio_product_code(j);
                broker_product_code = Broker_Commision_Rate_Data.broker_product_code(j);
                product_name = Opera_Commision_Rate_Data.product_name;
                counterparty_parent = Opera_Commision_Rate_Data.counterparty_parent;
                execution_broker = Opera_Commision_Rate_Data.execution_broker;
                exe_type = Broker_Commision_Rate_Data.exe_type(j);
                original_premium = Opera_Commision_Rate_Data.original_premium;
                market_action = Opera_Commision_Rate_Data.market_action;
                original_lots = abs(Opera_Commision_Rate_Data.original_lots);
                clearing_currency = Broker_Commision_Rate_Data.currency(j);
                cl_spot = Spot_Value;
                giveup_currency = give_up_currency;
                gu_spot = gSpot_Value;
                commission_amount = Current_commission_amount;
                clearing_fee = Current_clearing_fee;
                exchange_fee = Current_exchange_fee;
                nfa_fee = Current_nfa_fee;
                pit_brokerage = Current_pit_brokerage;
                execution_charge = Current_execution_charge;
                tax = Current_Tax;
                adjustment = Current_adjustment;
                clearing_brokerage_nc = Brokerage_Sum_Net_Val;
                give_up_charge_nc = Give_up_charge;
                clearing_brokerage_usd = Brokerage_Sum_Net_USD_Value; 
                give_up_brokerage_usd=give_Sum_Net_USD_Value;
                total_brokerage_nc = clearing_brokerage_nc + Give_up_charge;
                total_brokerage_usd_cg=give_up_brokerage_usd+clearing_brokerage_usd;
                
                %%% User want Data (Final Broker Commission Data)
                Broker_Commission_Store_Table_Data = [trade_id, transaction_date, security_id, portfolio, subportfolio, instrument, invenio_product_code, broker_product_code, product_name, ...
                                                counterparty_parent, execution_broker, exe_type, original_premium, market_action, original_lots, clearing_currency, cl_spot, giveup_currency,gu_spot, ...
                                                commission_amount, clearing_fee, exchange_fee, nfa_fee, pit_brokerage, execution_charge, tax,adjustment, ...
                                                clearing_brokerage_nc, give_up_charge_nc, clearing_brokerage_usd,give_up_brokerage_usd,total_brokerage_nc,total_brokerage_usd_cg];

                Broker_Commission_Store_Data = vertcat(Broker_Commission_Store_Data,Broker_Commission_Store_Table_Data);
           end
            
        else
            Overall_ErrorMsg = [Overall_ErrorMsg;{['No mapping found with broker data for corresponding ',char(Opera_TradeDate),',',char(Opera_instrument),',',char(Opera_counterparty_parent),',',char(Opera_exe_type)]}];
            continue;
        end       

    end 

    %%% Final data with Header and Data & write the data into excel sheet
    Total_Broker_Commission_Data = [Broker_Commission_Store_Columns;Broker_Commission_Store_Data];
    xlswrite(OutXLSName,Total_Broker_Commission_Data,'Broker_Commission_Data');
    if ~isempty(Overall_ErrorMsg)
        xlswrite(OutXLSName,[{'corresponding TradeDate, Instrument, Counterparty_parent, Exe_type'}; Overall_ErrorMsg],'Error_Sheet');
        %%%% Active user sheet
        xls_change_activesheet(fullfile(pwd,OutXLSName) ,'Error_Sheet'); 
    end
    
    %%%% Delete Empty sheets and active user sheet
    xls_delete_sheets(fullfile(pwd,OutXLSName));

    %%%% Upload the Final Broker Commission Data
    try
        set(ObjDB,'AutoCommit','off');
        SqlQuery = ['delete from ',char(Broker_Commission_Store_Table),' where transaction_date = ''',char(Settele_date),''''];
        curs = exec(ObjDB,SqlQuery);
        commit(ObjDB);
        if ~isempty(curs.Message)
            disp(curs.Message);
        end
        set(ObjDB,'AutoCommit','on');
        
        upload_in_database(Broker_Commission_Store_Table,Broker_Commission_Store_Data,ObjDB);
        disp('Upload Done Successfully to Database Table');           
    catch
        OutErrorMsg = {['Data upload got failed in ',char(Broker_Commission_Store_Table)]};
    end
    
catch ME
    OutErrorMsg = cellstr(ME.message);

    errorMessage = sprintf('Error in function %s() at line %d.\n\nError Message:\n%s', ...
    ME.stack(1).name, ME.stack(1).line, ME.message);
    fprintf(1, '%s\n', errorMessage);
    
end

