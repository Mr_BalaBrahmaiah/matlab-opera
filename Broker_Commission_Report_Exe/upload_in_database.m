function Status = upload_in_database(TableName, InData,objDB)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 06:15:07 $
%  $Revision: 1.5 $
%
Status = 0;

try
    javaaddpath('mysql-connector-java-5.1.25-bin.jar');
    
    
    if(exist('objDB','var'))
        %for Some time using UAT ,but we need same data in Production that
        %purposes only 
    else
        % Read the database configuration file to read the DB configuration
        % Default DB config values
        DBName = 'inveniodbwebpage';
        DBUserName = 'ananthi';
        DBPassword = 'Olamnet@123';
        IsFromServer = 1;
        DBServerIP = '10.190.7.194';
        try
            DBConfigData = textread('DatabaseConfiguration.txt','%s','delimiter','\n');
            IdxFound = cellStrfind(DBConfigData,'databasename');
            if ~isempty(IdxFound)
                [~,DBName] = strtok(DBConfigData(IdxFound),'=');
                DBName = char(strrep(DBName,'=',''));
            end
            IdxFound = cellStrfind(DBConfigData,'username');
            if ~isempty(IdxFound)
                [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
                DBUserName = char(strrep(DBUserName,'=',''));
            end
            IdxFound = cellStrfind(DBConfigData,'password');
            if ~isempty(IdxFound)
                [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
                DBPassword = char(strrep(DBPassword,'=',''));
            end
            IdxFound = cellStrfind(DBConfigData,'useserver');
            if ~isempty(IdxFound)
                [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
                IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
            end
            IdxFound = cellStrfind(DBConfigData,'serverip');
            if ~isempty(IdxFound)
                [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
                DBServerIP = char(strrep(DBServerIP,'=',''));
            end
            disp(['Using the database ',DBName]);
        catch
            disp('Error occured in reading the DatabaseConfiguration.txt, hence using the default configuration!');
        end
        
        if IsFromServer
            objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL','Server',DBServerIP);
        else
            objDB = database(DBName,DBUserName,DBPassword,'Vendor','MySQL');
        end
        
    end
    
    SqlQuery = ['select * from `',char(TableName),'` limit 2'];
    curs = exec(objDB,SqlQuery);
    curs = fetch(curs);
    attrobj = attr(curs);
    DataTypeValue = {attrobj.typeName};
    
    [NumRows,NumCols] = size(InData);
    
    % to upload the data into database quickly, it has to be stored in this
    % temp dir; saving this file in some other location increases the time to
    % upload in database
    mkdir C:\temp %% temp folder is not present in C drive, it will create automatically
    Path = 'C:\\temp\\';
    OutFileName =  [char(TableName),'_',datestr(today,'yyyymmdd'),'.txt'];
    OutFileName = [Path,OutFileName];
    
    tic
    fid = fopen(OutFileName,'wt');
    for iRow =1:NumRows
        for iCol = 1:NumCols
            switch lower(DataTypeValue{iCol})
                case {'varchar','date'}
                    if iCol == NumCols
                        fprintf(fid, '%s\n',InData{iRow,iCol});
                    else
                        fprintf(fid, '%s\t',InData{iRow,iCol});
                    end
                case {'double'}
                    if iCol == NumCols
                        fprintf(fid, '%f\n',InData{iRow,iCol});
                    else
                        fprintf(fid, '%f\t',InData{iRow,iCol});
                    end
                case {'int','bigint','tinyint'}
                    if iCol == NumCols
                        fprintf(fid, '%d\n',InData{iRow,iCol});
                    else
                        fprintf(fid, '%d\t',InData{iRow,iCol});
                    end
            end
        end
    end
    fclose(fid);
    toc
    
    tic
    set(objDB,'AutoCommit','off');
    SqlQuery = ['load data local infile ''',OutFileName,''' into table ',TableName,' fields terminated by ''\t'' lines terminated by ''\n'''];
    curs = exec(objDB,SqlQuery);
    commit(objDB);
    
    if ~isempty(curs.Message)
        disp(curs.Message);
    end
    set(objDB,'AutoCommit','on');
    toc
catch ME
    disp(ME.message);
end
end