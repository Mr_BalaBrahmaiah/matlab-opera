function TableColumns = gettablecolumnnames(objDB, TableName)
% Get the table names from the database object and get the column
% names of the selected table
% SqlTableCols = columns(objDB);
% TableNames = SqlTableCols(:,1);
% IdxTable = find(strcmpi(TableName,TableNames));
% TableColumns = SqlTableCols{IdxTable,2};  %#ok<*FNDSB>

SqlQuery = ['select * from `',char(TableName),'` limit 2'];
curs = exec(objDB,SqlQuery);
curs = fetch(curs);
attrobj = attr(curs);
TableColumns = {attrobj.fieldName};