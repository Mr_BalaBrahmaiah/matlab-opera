function [Min_Opera_Date] = Find_Minimum_PLUTO_ViewDate(Prod_Code,Start_Date,End_Date)

% Prod_Code = 'CT';

ObjDB_MS_SQL = connect_to_database_MS_SQL ;

%%
SqlQuery1 = ['select min(deal_dt) from VWGET_OPERA_FUTURE_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','NEW',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;
SqlQuery2 = ['select min(deal_dt) from VWGET_OPERA_FUTURE_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','MOD',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;
SqlQuery3 = ['select min(deal_dt) from VWGET_OPERA_OPTIONS_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','NEW',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;
SqlQuery4 = ['select min(deal_dt) from VWGET_OPERA_OPTIONS_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','MOD',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;
SqlQuery5 = ['select min(deal_dt) from VWGET_OPERA_FUTURE_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','DEL',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;
SqlQuery6 = ['select min(deal_dt) from VWGET_OPERA_OPTIONS_INTERFACE where pr_code in (''',Prod_Code,''') and RECORD_FLAG = ''','DEL',''' and ((deal_dt >= ''',Start_Date,''' and deal_dt <= ''',End_Date,''') or ( cast(upd_date as date) >= ''',Start_Date,''' and cast(upd_date as date) <= ''',End_Date,'''))'] ;

%%
FutNew_Date = fetch(ObjDB_MS_SQL,SqlQuery1) ;
FutMod_Date = fetch(ObjDB_MS_SQL,SqlQuery2) ;
OptNew_Date = fetch(ObjDB_MS_SQL,SqlQuery3) ;
OptMod_Date = fetch(ObjDB_MS_SQL,SqlQuery4) ;
FutDel_Date = fetch(ObjDB_MS_SQL,SqlQuery5) ;
OptDel_Date = fetch(ObjDB_MS_SQL,SqlQuery6) ;

All_Dates = [FutNew_Date ; FutMod_Date ; OptNew_Date ; OptMod_Date ; FutDel_Date ; OptDel_Date ] ;
All_Dates = All_Dates(cellfun(@(s) (length(s)>1), All_Dates)) ;  %% Remove Empty Date Fields

Min_All_Dates = min(datenum(All_Dates)) ;
Min_PLUTO_Date = datestr(Min_All_Dates,'dd-mmm-yyyy') ;

%%

[System_Date_Format,~] = get_System_Date_Format();

HolDates = {'2016-01-01','2017-01-01','2018-01-01'};
HolidayVec = datenum(datestr(HolDates,System_Date_Format));

Opera_Date_Str = datestr(busdate(today-90,-1,HolidayVec),1);
Opera_Date_Num = datenum(Opera_Date_Str);

if(Opera_Date_Num < Min_All_Dates)
    Min_Opera_Date = Opera_Date_Str ;
else
    Min_Opera_Date = Min_PLUTO_Date ;
end

%%

close(ObjDB_MS_SQL) ;
delete(ObjDB_MS_SQL) ;

end
