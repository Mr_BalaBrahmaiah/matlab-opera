function [DBFieldNames,DBData] = read_from_database_MS_SQL(InTableName,varargin)
%
%  Matlab Version(s):   R2013a
%  Matlab Toolboxes required:
%
%  Invenio Commodity Services Private Limited
%
%  CVS
%  $Author: invenio $
%  $Date: 2015/04/22 06:15:07 $
%  $Revision: 1.6 $
%

DBFieldNames = [];
DBData       = 'No Data' ;%%[];

try
    javaaddpath('sqljdbc4.jar');
    % Read the database configuration file to read the DB configuration
    % Default DB config values
    DBName = 'GS_ICSS_SB';
    DBUserName = 'sapappdev';
    DBPassword = 'CTRMdev@123#';
    IsFromServer = 1;
    DBServerIP = '10.230.0.72';
    DBServerPort = 1433;
    try
        DBConfigData = textread('DatabaseConfiguration_MS_SQL.txt','%s','delimiter','\n');
        IdxFound = cellStrfind(DBConfigData,'databasename');
        if ~isempty(IdxFound)
            [~,DBName] = strtok(DBConfigData(IdxFound),'=');
            DBName = char(strrep(DBName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'username');
        if ~isempty(IdxFound)
            [~,DBUserName] = strtok(DBConfigData(IdxFound),'=');
            DBUserName = char(strrep(DBUserName,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'password');
        if ~isempty(IdxFound)
            [~,DBPassword] = strtok(DBConfigData(IdxFound),'=');
            DBPassword = char(strrep(DBPassword,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'useserver');
        if ~isempty(IdxFound)
            [~,IsFromServer] = strtok(DBConfigData(IdxFound),'=');
            IsFromServer = str2double(cell2mat(strrep(IsFromServer,'=','')));
        end
        IdxFound = cellStrfind(DBConfigData,'serverip');
        if ~isempty(IdxFound)
            [~,DBServerIP] = strtok(DBConfigData(IdxFound),'=');
            DBServerIP = char(strrep(DBServerIP,'=',''));
        end
        IdxFound = cellStrfind(DBConfigData,'server_port');
        if ~isempty(IdxFound)
            DBServerPort = strsplit(char(DBConfigData(IdxFound)),'=');
            DBServerPort = char(DBServerPort(end));
            DBServerPort = str2double(DBServerPort);
        end
        %         disp(['Using the database ',DBName]);
        fprintf('Using the database : %s : IP : %s\n',DBName,DBServerIP);
    catch
        disp('Error occured in reading the DatabaseConfiguration_MS_SQL.txt, hence using the default configuration!');
    end
    
    if IsFromServer
        ObjDB = database(DBName,DBUserName,DBPassword,'Vendor','Microsoft SQL Server','Server',DBServerIP,'PortNumber',DBServerPort);
    else
        ObjDB = database(DBName,DBUserName,DBPassword,'Vendor','Microsoft SQL Server','DriverType','thin');
    end
    objDB = ObjDB;
    if ~isempty(varargin) % if sql query is passed as an input
        IsQueryInput = 1;
    else
        IsQueryInput = 0;
    end
    if isconnection(objDB)
        if IsQueryInput
            SqlQuery = varargin{1};
        else
            
            %             SqlQuery = ['select * from ',char(Schema_Name),'.',char(InTableName)];
            %             SqlQuery = ['select * from ',char(InTableName)];
            %             SqlQuery = ['select * from ',char(InTableName),' where DEAL_DT >= ''2017-01-01'''];
            SqlQuery = ['select * from ', char(InTableName),' where DEAL_DT >= ''2018-01-01'' '];
            
        end
        
        curs = exec(objDB,SqlQuery);
        curs = fetch(curs);
        AttrObj = attr(curs);
        DBFieldNames = {AttrObj.fieldName};
        DBData = curs.Data;
        
        close(objDB);delete(objDB);
    end
catch ME
    if isconnection(objDB)
        close(objDB);
        delete(objDB);
    end
    errordlg(ME.message);
end

end