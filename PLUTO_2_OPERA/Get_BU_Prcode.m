function [BU_Name,Business_Unit] = Get_BU_Prcode(handles)

CT = get(handles.checkbox1,'Value');
CC = get(handles.checkbox2,'Value');
CF = get(handles.checkbox3,'Value');
GR = get(handles.checkbox4,'Value');
MP = get(handles.checkbox5,'Value');
RB = get(handles.checkbox6,'Value');
SG = get(handles.checkbox7,'Value');

if(CT || CC || CF || GR || MP || RB || SG)
    
    ProductNames = {'CT','CC','CF','GR','MP','RB','SG'}; %% Hard Code
    BU_Names = {'cot','coc','cof','grn','dry','rbr','sgr'} ; %% Hard Code
    User_Select_Portfolio = {CT,CC,CF,GR,MP,RB,SG};
    BU_Name = BU_Names(logical(cell2mat(User_Select_Portfolio)));
    Business_Unit = ProductNames(logical(cell2mat(User_Select_Portfolio)));
        
end