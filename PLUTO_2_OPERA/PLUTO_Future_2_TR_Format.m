function [TRHeader , RowData ,Instrument_Error_Data ,Maturity_Error_Data] = PLUTO_Future_2_TR_Format(DBFieldNames , DBData , PLUTO_Action_Str,InBUName)

Commodity = 'FUT';


TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
    'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
    'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
    'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};

%     RowData = cell(1,size(TRHeader,2));
RowData = [];

Instrument_Error_Data = '';
Maturity_Error_Data = '';

%%

if(size(DBData,2) > 1)
    
    %%
    Pr_Code_Col = find(cellfun(@(V) strcmpi('pr_code',V), DBFieldNames)) ;
    Ex_Code_Col = find(cellfun(@(V) strcmpi('EX_CODE',V), DBFieldNames)) ;
    Prcat_Sd_Col = find(cellfun(@(V) strcmpi('prcat_sd',V), DBFieldNames)) ;
    Unit_Sd_Col = find(cellfun(@(V) strcmpi('unit_sd',V), DBFieldNames)) ;
    LotType_Col = find(cellfun(@(V) strcmpi('LOT_TYPE',V), DBFieldNames)) ;
    Month_Col = find(cellfun(@(V) strcmpi('TER_MONTH',V), DBFieldNames)) ;
    CounterParty_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
    TransactionDate_Col = find(cellfun(@(V) strcmpi('DEAL_DT',V), DBFieldNames)) ;
    ExeBroker_Col = find(cellfun(@(V) strcmpi('PARTY_SD',V), DBFieldNames)) ;
    Prem_Col = find(cellfun(@(V) strcmpi('RATE',V), DBFieldNames)) ;
    InitialLot_Col = find(cellfun(@(V) strcmpi('LOT',V), DBFieldNames)) ;
    EXPIRY_DT_Col = find(cellfun(@(V) strcmpi('EXPIRY_DT',V), DBFieldNames)) ;
    LYADJ_Col = find(cellfun(@(V) strcmpi('LYADJ_OT',V), DBFieldNames)) ; %% LYADJ_OT %% LYADJ_LOT
    
    if(isempty(LYADJ_Col))
        LYADJ_Col = find(cellfun(@(V) strcmpi('LYADJ_LOT',V), DBFieldNames)) ;
    end
    
    UniqueNumber_Col = find(cellfun(@(V) strcmpi('UNIQ_NO',V), DBFieldNames)) ;
    LYBROK_Col = find(cellfun(@(V) strcmpi('LYBROK_ADJ',V), DBFieldNames)) ;
    
    BBR_Code_Col = find(cellfun(@(V) strcmpi('BBR_CODE',V), DBFieldNames)) ;
    
    COMPANY_SD_Col = find(cellfun(@(V) strcmpi('COMPANY_SD',V), DBFieldNames));
    
    UPD_DATE_Col = find(cellfun(@(V) strcmpi('UPD_DATE',V), DBFieldNames));
    
    %%  Organize the Data  %% Read Mapping Sheet and Get Instrument
    
    Product_Exch_Data = strcat(DBData(:,Pr_Code_Col), {'-'} , DBData(:,Ex_Code_Col) , {'-'} , DBData(:,Prcat_Sd_Col)) ;
    
    [Instrument_Data , New_Instrument ,Price_Conversion , New_Instrument_Index] = Convert_Product_Exchange_Data_2_Instrument(Product_Exch_Data) ;
    
    if(~isempty(New_Instrument))
        
        warndlg([strcat('PLUTO_',Commodity,'_',PLUTO_Action_Str),' View Contain the Instrument ',New_Instrument',' which is not available in Mapping Master file. Please update this Instrument in master file(Instrument Mapping Sheet) to get these Instrument deals'],'PLUTO_2_OPERA');
        
        Instrument_Error_Data = DBData(New_Instrument_Index,:);
        % Instrument_Error_Data =  [DBFieldNames ; Instrument_Error_Data];
        
        DBData(New_Instrument_Index,:) = [];
        Instrument_Data(New_Instrument_Index,:) = [];
        Price_Conversion(New_Instrument_Index,:) = [];
        
    else
        Instrument_Error_Data = '';
    end
    
    if(~isempty(DBData))
        
        Portfolio = strcat(DBData(:,Pr_Code_Col) , {'-'}, DBData(:,Unit_Sd_Col));
        
        Buy_Sell = DBData(:,LotType_Col) ;
        Bounght_Index = cellStrfind_exact(Buy_Sell,{'bought'}) ;
        Sold_Index = cellStrfind_exact(Buy_Sell,{'Sold'}) ;
        Buy_Sell(Bounght_Index)  = {'Bought'} ;
        Buy_Sell(Sold_Index)  = {'Sold'} ;
        
        Month_DB =  DBData(:,Month_Col) ;
        BBR_Code_Data = DBData(:,BBR_Code_Col);
        Expiry_Date = DBData(:,EXPIRY_DT_Col);
        [Month_Maturity , Maturity_Code , Maturity_Error_Data] = Convert_Month_2_Maturity_Format(Month_DB,BBR_Code_Data,Expiry_Date,Commodity) ;
        %[Month_Maturity , Maturity_Code , Maturity_Error_Data] = Convert_Month_2_Maturity_Format(Month_DB,BBR_Code_Data,Commodity) ;
  
        Prem_Data = num2cell( cell2mat(DBData(:,Prem_Col)) .* cell2mat(Price_Conversion) );
        CounterParty_Data = DBData(:,CounterParty_Col);
        %InitialLot_Data = num2cell(cell2mat(DBData(:,InitialLot_Col)) - cell2mat(DBData(:,LYADJ_Col)));
        InitialLot_Data = DBData(:,InitialLot_Col);  % Change
        InitialLot_Data = cell2mat(DBData(:,InitialLot_Col));  % Change
        InitialLot_Data(Bounght_Index) = (InitialLot_Data(Bounght_Index) .* 1);   % Change
        InitialLot_Data(Sold_Index) = (InitialLot_Data(Sold_Index) .* -1);
        InitialLot_Data = num2cell(InitialLot_Data);
        
        TransactionDate_Data = DBData(:,TransactionDate_Col);
        ExeBroker_Data = DBData(:,ExeBroker_Col);
        
        UniqueNumber_Data = DBData(:,UniqueNumber_Col);
        %BrokerLot_Data = num2cell(cell2mat(DBData(:,InitialLot_Col)) - cell2mat(DBData(:,LYBROK_Col)));
        BrokerLot_Data = DBData(:,InitialLot_Col);  % Change
        BrokerLot_Data = cell2mat(DBData(:,InitialLot_Col));  % Change
        BrokerLot_Data(Bounght_Index) = (BrokerLot_Data(Bounght_Index) .* 1);   % Change
        BrokerLot_Data(Sold_Index) = (BrokerLot_Data(Sold_Index) .* -1);
        BrokerLot_Data = num2cell(BrokerLot_Data);
        
        CounterpartyEntity_Data = DBData(:,COMPANY_SD_Col);
        
        Traded_DateTime_Data = DBData(:,UPD_DATE_Col);
        
        %% Hard Code Field Names
        OptType = cell(size(DBData ,1) ,1) ;
        Source = cell(size(DBData ,1) ,1) ;
        ExeType = cell(size(DBData ,1) ,1) ;
        Rates = cell(size(DBData ,1) ,1) ;
        
        OptType(:,:) = {'Future'};
        %Source(:,:) = {'OPS'};
        Source(:,:) = {'PLUTO'};
        ExeType(:,:) = {'Elec'} ;
        Rates(:,:) = {'Normal'} ;
        
        
        %% Empty  Cell Array
        % Strike CONTRACTNB ,Maturity ,Barrier Type , Barrier , Averaging Start Date ,Averaging End Date , Parent Transaction Number ,Spcl. Rate /lot
        % Other Remarks , OTC DAILY EOD LEG , Trader Name , Traded DateTimestamp
        
        Temp_Cell_Array = cell(size(DBData ,1) ,1);
        
        PLUTO_Action_Data = cell(size(DBData ,1) ,1);
        PLUTO_Action_Data(:,:) = {PLUTO_Action_Str};
        
        %%  Getting Trade ID
        
        [TradeId , Last_TradeId] = getTradeID_4_Cell_Array(DBData,InBUName) ; %% Last Trade ID
        
        %%  Make Data
        
        TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike',...
            'OptType','Prem','CounterParty','Initial Lots','CONTRACTNB','Maturity','Barrier Type',...
            'Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
            'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
            'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
        
        RowData = [Instrument_Data,Portfolio,Buy_Sell,Maturity_Code,Temp_Cell_Array,Temp_Cell_Array,OptType,Prem_Data,CounterParty_Data,InitialLot_Data,Temp_Cell_Array,...
            Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,TransactionDate_Data,Temp_Cell_Array,Source,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,...
            ExeBroker_Data,ExeType,Rates,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,Temp_Cell_Array,Traded_DateTime_Data,...
            UniqueNumber_Data,BrokerLot_Data,PLUTO_Action_Data,CounterpartyEntity_Data] ;
        
        %     OutTRData = [TRHeader ;  RowData];
        
        %% Excel Sheet Write
        
        %     Date_Str = strrep(char(datetime),':','_');
        %
        %     OutXLSName = ['PLUTO_to_TR_Format_Future_',Date_Str,'.xlsx'];
        %
        %     OutTRData = [TRHeader ;  RowData];
        %
        %     xlswrite(OutXLSName , [TRHeader ;  RowData] , 'PLUTOI TO TR Format') ;
        %     xlswrite(OutXLSName , [DBFieldNames ;  DBData] , 'PLUTOI DB') ;
        %
        %     OutXLSName = fullfile(pwd,OutXLSName) ;
        %     xls_delete_sheets(OutXLSName) ;
    end
    
else
    
    TRHeader = {'Instrument','Portfolio','Buy Sell','Month','Month1','Strike','OptType','Prem','CounterParty','Initial Lots','CONTRACTNB',...
        'Maturity','Barrier Type','Barrier','Transaction Date','TRN.Number','Source','Averaging Start Date','Averaging End Date','Parent Transaction Number',...
        'Exe-Broker','Exe Type','Rates','Spcl. Rate /lot','Other Remarks','OTC DAILY EOD LEG','Trader Name','Traded DateTimestamp',...
        'OPS Unique','BROKER LOTS','OPS Action','Counterparty Entity'};
    
    %     RowData = cell(1,size(TRHeader,2));
    RowData = [];
    
end

end




