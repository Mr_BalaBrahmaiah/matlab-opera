function [DBFieldNames , DBData  ] = Update_2_Database(Del_Data,InBUName)

Default_TableName = 'deal_ticket_table_';
TableName = strcat(Default_TableName,InBUName) ;

ObjDB = connect_to_database;

%%

TransactionLogFile =  ['TransactionLog_',char(TableName),datestr(today,'yyyymmdd'),'.txt'];
% ErrorLogFile = ['ErrorLog_',char(TableName),datestr(today,'yyyymmdd'),'.txt'];

tic;
fid = fopen(TransactionLogFile,'wt');
% ferrid = fopen(ErrorLogFile,'wt');

%%
for i = 1 : size(Del_Data,1)
    
    Current_Trade_ID = char(Del_Data(i,1)) ; %% Trade ID
    Current_unique_No = char(Del_Data(i,36)) ; %% PLUTO Unique Number
    try
        MaxVersionQuery = ['select max(version_no) from ',TableName,' where trade_id = ''',Current_Trade_ID,''''];
        VersionNo = cell2mat(fetch(ObjDB,MaxVersionQuery));
        SqlQuery = ['update ',TableName,' set original_lots=0,broker_lots=0, lots_to_be_netted=0,premium=0,ops_action=''DEL'' where trade_id = ''',Current_Trade_ID,''' and ops_unique = ''',Current_unique_No,''' and version_no = ',num2str(VersionNo) ];
        
        fprintf(fid, '%s\n',SqlQuery);
        curs = exec(ObjDB,SqlQuery);
        
        %         delete(ErrorLogFile);
        
    catch ME
        %         fprintf(ferrid, '%s\n',SqlQuery);
        Error_Log_FileName_Line_Number(ME) ;
    end
    
    
end

fclose(fid);
% fclose(ferrid);
toc ;

pause(5);

%% Get Del_ID Info from Table

Full_ID = '';
for i = 1 : size(Del_Data,1)
    
    if(i==1)
        Full_ID = char(strcat({''''},Del_Data(i,1),{''''}));
        
    else
        Full_ID = [Full_ID ,  strcat(',',char(strcat({''''},Del_Data(i,1),{''''}))) ];
    end
    
end

SqlQuery = ['select * from ',TableName,' where trade_id  in (', Full_ID ,')'];
curs = exec(ObjDB,SqlQuery);
curs = fetch(curs);
AttrObj = attr(curs);
DBFieldNames = {AttrObj.fieldName};
DBData = curs.Data;


