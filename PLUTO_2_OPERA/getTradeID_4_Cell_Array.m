function [TradeId , Last_DBTradeId] = getTradeID_4_Cell_Array(Cell_Array,InBUName)

NumRows = size(Cell_Array,1) ;

%%
ObjDB = connect_to_database;

TradeId = '' ;
TempTradeId = '';                               %% Transaction Number
try
    [OutErrorMsg,Last_DBTradeId]= getLastTradeId(InBUName); %% 'agf'  %% 'cot'
    
    if strcmpi(OutErrorMsg,'No Errors')
        % TradeIdPrefix = 'TR-FY16-17-';
        SqlQuery = 'SELECT * FROM helper_fetch_tradeid_prefix_view';
        TradeIdPrefix = char(fetch(ObjDB,SqlQuery));
        TempTradeId = str2num(strrep(char(Last_DBTradeId),TradeIdPrefix,''));
        
        if(isempty(TempTradeId))
            TempTradeId = 0;      %% Default
        end
    end
    
    if ~isempty(TempTradeId)
        
        NTradeId = (TempTradeId+1 : 1 : TempTradeId+NumRows)';
        TradeId = cellstr([repmat(TradeIdPrefix,length(NTradeId),1),num2str(NTradeId)]);
        TradeId = strrep(TradeId,' ','');  %% Remove Spaces
        
    end
    
    
catch
%     TempTradeId = '';
    
    TradeId = '';
    Last_DBTradeId = '';
end

%%




